using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class Branch: IDisposable 
    {
        private string _BranchId;
        private string _CompanyName;
        private string _ProfName;
        private DataRow _referencedRow;

        public string BranchId
        {
            get { return _BranchId; }
        }

        public string CompanyName
        {
            get { return _CompanyName; }
        }

        public string ProfName
        {
            get { return _ProfName; }
        }

        public DataRow ReferencedRow
        {
            get { return _referencedRow; }
        }

        public Branch()
        {
            this.Initialize();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource
                _referencedRow = null;
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }

        private void Initialize()
        {
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                DataRow row;

                if (dm.GetCache("CurrentBranchRow") != null)
                {
                    row = (DataRow)dm.GetCache("CurrentBranchRow");

                    if (row != null)
                    {
                        this.Populate(row);
                    }
                }
                else // try to fetch the record from the database
                {
                    //TODO: Implement database fetch 
                    //throw (new Exception("Session CurrentCustomerRow not found.\r\nDirect database fetch is not yet implemented in AgilityCustomer class."));
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        private void Populate(DataRow row)
        {
            this._BranchId = row["branch_id"].ToString();
            this._CompanyName = row["company_name"].ToString();
            this._ProfName = row["prof_name"].ToString();
            this._referencedRow = row;
        }
    }
}
