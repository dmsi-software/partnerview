// Quote dataset wrapper

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;
using System.Threading;

namespace Dmsi.Agility.Data
{
    public class Quotes: IDisposable
    {
        #region Members

        //class-level members for Async Quote Release
        string _contextID;
        string _reference;
        string _job;
        string _quotedfor;
        string _custpo;
        string _acknowledgementEmailAddress;
        string _acknowledgementEmailAddress2;
        string _acknowledgementFaxNumber;
        dsQuoteDataSet _dsQuote;
        dsRetailAddressDataSet _dsRetailAddress;
        //class-level members for Async Quote Release

        string _quotesysid;
        int _quoteid;
        bool _copyWithRetail;
        bool _copyNonstock;

        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.Quote _subAppObject;

        #endregion

        #region Properties

        private DataTable _referencedHeaderTable;
        private DataTable _referencedDetailTable;
        private DataSet _referencedDataSet;
        private dsRetailAddressDataSet _referencedRetailAddressDataSet;

        /// <summary>
        /// Points to ttquote_header
        /// </summary>
        public DataTable ReferenceHeaderTable
        {
            get { return this._referencedHeaderTable; }
        }

        /// <summary>
        /// Points to ttquote_detail
        /// </summary>
        public DataTable ReferencedDetailTable
        {
            get { return this._referencedDetailTable; }
        }

        /// <summary>
        /// Points to dsQuoteDataSet
        /// </summary>
        public DataSet ReferencedDataSet
        {
            get { return this._referencedDataSet; }
        }

        public dsRetailAddressDataSet ReferencedRetailAddressDataSet
        {
            get { return this._referencedRetailAddressDataSet; }
            set { this._referencedRetailAddressDataSet = value; }
        }

        #endregion

        #region Contructors and Dispose
        
        /// <summary>
        /// Get the quote from the back-end
        /// </summary>
        public Quotes()
        {
            
        }

        /// <summary>
        /// Get the quote dateset from cache
        /// </summary>
        public Quotes(string sessionVariable, out int ReturnCode, out string MessageText)
        {
            this.Initialize(null, null, sessionVariable, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param>
        /// <param name="customer">Current customer, shiptos of the customer must be populated</param> 
        public Quotes(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet customer, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            this.Initialize(searchCriteria, customer, sessionVariable, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();

                if (System.Web.HttpContext.Current != null)
                {
                    _referencedHeaderTable = null;
                    _referencedDetailTable = null;
                    _referencedDataSet = null;
                }
                else
                {
                    this._referencedHeaderTable.Dispose();
                    this._referencedDetailTable.Dispose();
                    this._referencedDataSet.Dispose();
                }
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }
        #endregion

        #region Methods

        private void Initialize(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet customer, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet ds = null;
            Dmsi.Agility.EntryNET.StrongTypesNS.dsRetailAddressDataSet dsRetailAddress = null;
           
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                if (sessionVariable != null && 
                    (searchCriteria == null || customer == null) &&
                    dm.GetCache(sessionVariable) != null)
                {
                    ds = (dsQuoteDataSet)dm.GetCache(sessionVariable);
                    this.Populate(ds);

                    dsRetailAddress = (dsRetailAddressDataSet)dm.GetCache("dsRetailAddressForQuotes");
                }
                else // try to fetch the record from the database
                {
                    this.CreateSubAppObject();

                    if (this._subAppObject != null)
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();
                        ds = new dsQuoteDataSet();

                        try
                        {
                            string ipcContextId = dm.GetCache("opcContextId") as string;

                            this._subAppObject.siquotefetchlist(ipcContextId, searchCriteria, customer, out ds, out dsRetailAddress, out ReturnCode, out MessageText);
                            this.Populate(ds);
                            this.ReferencedRetailAddressDataSet = dsRetailAddress;
                            dm.SetCache("dsRetailAddressForQuotes", dsRetailAddress);
                            DisposeAppServerObjects();
                        }
                        catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                        {
                            el.LogError(EventTypeFlag.AppServerDBerror, oe);
                            em.Message = oe.Message;
                            em.EventType = EventTypeFlag.AppServerDBerror;
                        }
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        /// <summary>
        /// Assign values to properties
        /// </summary>
        /// <param name="ds"></param>
        private void Populate(Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet ds)
        {
            this._referencedDataSet = ds;
            this._referencedHeaderTable = ds.pvttquote_header;
            this._referencedDetailTable = ds.pvttquote_detail;
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject()
        {
            Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // Get ASP Session setting first
            if (System.Web.HttpContext.Current != null)
            {
                user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                if (this._appServerConnection != null)
                    this._isConnectionFromOutside = true;

                this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                if (this._appObject != null)
                    this._isAppObjectFromOutside = true;

                this._subAppObject = (Dmsi.Agility.EntryNET.Quote)dm.GetCache("QuoteSubAppObject");
                if (this._subAppObject != null)
                    this._isSubAppObjectFromOutside = true;
            }

            // if objects are still null then create them
            if (this._appServerConnection == null)
            {
                int loginStatus = -1;
                string result = "";

                _connection = new Dmsi.Agility.Security.Connection();

                if (user == null)
                {
                    throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.\r\nNon-ASP implementation is not yet supported.");
                }

                if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                    loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);

                if (loginStatus == 0)
                {
                    this._appServerConnection = _connection.ProgressConnection;
                    this._appObject = _connection.ProgressApplicationObject;
                    _connection.Dispose();
                }
                else
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")");
                    em.EventType = EventTypeFlag.AppServerDBerror;
                    em.Message = "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")";
                }
            }

            if (this._appObject == null)
                this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

            if (this._subAppObject == null)
                this._subAppObject = new Dmsi.Agility.EntryNET.Quote(this._appObject);
        }

        public string ValidateQuoteRelease(string reference, string job, string quotedfor, string custpo, dsQuoteDataSet dsQuote)
        {
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            string ipcContextId = dm.GetCache("opcContextId") as string;
            int ReturnCode = 0;
            string MessageText = "";

            if (_subAppObject == null)
                CreateSubAppObject();

            _subAppObject.siquoterelvalidate(ipcContextId,
                                             reference,
                                             job,
                                             quotedfor,
                                             custpo,
                                             dsQuote,
                                             out ReturnCode,
                                             out MessageText);

            return MessageText;
        }

        public void ReleaseQuote(object s)
        {
            EntryNET.Quote subAppObject = s as EntryNET.Quote;

            int ReturnCode = 0;
            int NewOrderID = 0;
            string MessageText = "";

            subAppObject.siquoterelexec(_contextID,
                                        _reference,
                                        _job,
                                        _quotedfor,
                                        _custpo,
                                        _acknowledgementEmailAddress,
                                        _acknowledgementEmailAddress2,
                                        _acknowledgementFaxNumber,
                                        _dsQuote,
                                        _dsRetailAddress,
                                        out NewOrderID,
                                        out ReturnCode,
                                        out MessageText);
        }

        public void BeginReleaseQuote(string reference, string job, string quotedfor, string custpo, string acknowledgementEmailAddress, string acknowledgementEmailAddress2, string acknowledgementFaxNumber, dsQuoteDataSet dsQuote, dsRetailAddressDataSet dsRetailAddress)
        {
            _reference = reference;
            _job = job;
            _quotedfor = quotedfor;
            _custpo = custpo;
            _acknowledgementEmailAddress = acknowledgementEmailAddress;
            _acknowledgementEmailAddress2 = acknowledgementEmailAddress2;
            _acknowledgementFaxNumber = acknowledgementFaxNumber;
            _dsQuote = dsQuote;
            _dsRetailAddress = dsRetailAddress;

            DataManager dm = new DataManager();
            _contextID = dm.GetCache("opcContextId") as string;

            if (_subAppObject == null)
                this.CreateSubAppObject();

            ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(ReleaseQuote), _subAppObject);
        }

        public string UpdateQuote(dsQuoteDataSet dsQuote, dsRetailAddressDataSet dsRetailAddress)
        {
            string returnMessage = "";

            int ReturnCode = 0;
            string MessageText = "";


            DataManager dm = new DataManager();
            PriceModeHelper pmh = new PriceModeHelper(dm);

            _contextID = dm.GetCache("opcContextId") as string;

            string cRetailMode = pmh.GetRetailMode();

            foreach (DataRow dr in dsQuote.pvttquote_detail.Rows)
            {
                dr["retail_mode"] = cRetailMode;
            }

            if (_subAppObject == null)
                this.CreateSubAppObject();

            _subAppObject.siquoteupdatepv(_contextID,
                                        dsQuote,
                                        dsRetailAddress,
                                        out ReturnCode,
                                        out MessageText);

            if (ReturnCode == 0)
                returnMessage = "0";
            else
                returnMessage = "ReturnCode: " + ReturnCode.ToString() + "; " + MessageText;

            return returnMessage;
        }

        public void BeginCopyQuote(string quotesysid, int quoteid, bool copyWithRetail, bool copyNonstock)
        {
            _quotesysid = quotesysid;
            _quoteid = quoteid;
            _copyWithRetail = copyWithRetail;
            _copyNonstock = copyNonstock;

            DataManager dm = new DataManager();
            _contextID = dm.GetCache("opcContextId") as string;

            if (_subAppObject == null)
                this.CreateSubAppObject();

            ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(CopyQuote), _subAppObject);
        }

        private void CopyQuote(object s)
        {
            EntryNET.Quote subAppObject = s as EntryNET.Quote;

            int ReturnCode = 0;
            string MessageText = "";

            Dmsi.Agility.EntryNET.EventLog el = new EventLog();
            Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

            try
            {
                subAppObject.siquotecopy(_contextID,
                                      _quotesysid,
                                      _quoteid,
                                      _copyWithRetail,
                                      _copyNonstock,
                                      out ReturnCode,
                                      out MessageText);

                if (ReturnCode != 0)
                {
                    el.LogError(EventTypeFlag.ApplicationEvent, DateTime.Now.ToString() + ": ReturnCode = " + ReturnCode + ": MessageText = " + MessageText);
                }

                DisposeAppServerObjects();
                this.Dispose();
            }
            catch (Progress.Open4GL.Exceptions.Open4GLException oe)
            {
                el.LogError(EventTypeFlag.AppServerDBerror, oe);
                em.Message = oe.Message;
                em.EventType = EventTypeFlag.AppServerDBerror;
            }
        }

        public string DeleteQuoteDetailItem(dsQuoteDataSet dsQuote)
        {
            string returnMessage = "";

            DataManager dm = new DataManager();
            _contextID = dm.GetCache("opcContextId") as string;

            int ReturnCode = 0;
            string MessageText = "";

            if (_subAppObject == null)
                this.CreateSubAppObject();

            _subAppObject.siquotedeleteitem(_contextID,
                                            dsQuote,
                                            out ReturnCode,
                                            out MessageText);

            if (ReturnCode == 0)
                returnMessage = "0";
            else
                returnMessage = "ReturnCode: " + ReturnCode.ToString() + "; " + MessageText;

            return returnMessage;
        }

        public string AddQuoteDetailItem(decimal markupFactor, dsQuoteDataSet dsQuote)
        {
            string returnMessage = "";

            DataManager dm = new DataManager();
            _contextID = dm.GetCache("opcContextId") as string;

            int ReturnCode = 0;
            string MessageText = "";

            if (_subAppObject == null)
                this.CreateSubAppObject();

            _subAppObject.siquoteadditempv(_contextID, markupFactor, dsQuote, out ReturnCode, out MessageText);

            if (ReturnCode == 0)
                returnMessage = "0";
            else
                returnMessage = "ReturnCode: " + ReturnCode.ToString() + "; " + MessageText;

            return returnMessage;
        }

        public string ApplyMultiplier(decimal multiplier, ref dsQuoteDataSet dsQuote)
        {
            string returnMessage = "";

            DataManager dm = new DataManager();
            PriceModeHelper pmh = new PriceModeHelper(dm);
            

            _contextID = dm.GetCache("opcContextId") as string;

            int ReturnCode = 0;
            string MessageText = "";

            if (_subAppObject == null)
                this.CreateSubAppObject();

            string cRetailMode = pmh.GetRetailMode();

            foreach (DataRow dr in dsQuote.pvttquote_detail.Rows)
            {
                dr["retail_mode"] = cRetailMode;
                dr["retail_price_calc_factor"] = multiplier;
            }

            _subAppObject.siquoteapplymultiplier(_contextID, "PV", cRetailMode, multiplier, ref dsQuote, out ReturnCode, out MessageText);

            if (ReturnCode == 0)
                returnMessage = "0";
            else
                returnMessage = "ReturnCode: " + ReturnCode.ToString() + "; " + MessageText;

            return returnMessage;
        }

        public dsQuoteDataSet GetQuoteDataset(int quoteId)
        {
            dsQuoteDataSet ds = new dsQuoteDataSet();
            int ReturnCode = 0;
            string MessageText = "";

            this.Initialize(null, null, "dsQuoteDataSet", out ReturnCode, out MessageText);

            DataRow[] QuoteHeaderRow = this.ReferenceHeaderTable.Select("quote_id='" + quoteId.ToString() + "'");
            DataRow[] QuoteDetailRows = this.ReferencedDetailTable.Select("quote_id='" + quoteId.ToString() + "'");

            ds.pvttquote_header.Rows.Add(QuoteHeaderRow[0].ItemArray);

            foreach (DataRow r in QuoteDetailRows)
            {
                ds.pvttquote_detail.Rows.Add(r.ItemArray);
            }

            foreach (DataRow byitem in this.ReferencedDataSet.Tables["pvttquote_byitem"].Rows)
            {
                ds.pvttquote_byitem.Rows.Add(byitem.ItemArray);
            }

            foreach (DataRow ttmisc in this.ReferencedDataSet.Tables["ttmisc"].Rows)
            {
                ds.ttmisc.Rows.Add(ttmisc.ItemArray);
            }

            ds.AcceptChanges();

            return ds;
        }

        #endregion
    }
}
