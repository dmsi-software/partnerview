// Customer dataset wrapper

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class Customers : IDisposable
    {
        #region Members
        // This object wraps both Connection and EntryNet. To be used when cached connection is not present.
        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.Customer _subAppObject;
        private System.Uri _Uri;
        private string _userName;
        private string _password;
        #endregion

        #region Properties
        private DataSet _referencedDataSet;

        /// <summary>
        /// Points to dsCustomerDataSet
        /// </summary>
        public DataSet ReferencedDataSet
        {
            get { return this._referencedDataSet; }
        }
        #endregion

        #region Contructors and Dispose
        /// <summary>
        /// Get the Customer dateset from cache
        /// </summary>
        public Customers(out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.Initialize("", null, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param>
        /// <param name="customer">Current customer, shiptos of the customer must be populated</param> 
        public Customers(string searchCriteria, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.Initialize("", searchCriteria, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Url">AppServer connection string</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="searchCriteria">Name-value pair for customer list fetch</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public Customers(string Url, string userName, string password, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._Uri = new System.Uri(Url);
            this.Initialize("", searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Uri">AppServer connetion Uri</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="searchCriteria">Name-value pair for customer list fetch</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public Customers(System.Uri Uri, string userName, string password, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._Uri = Uri;
            this.Initialize("", searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        public Customers(string ipcContextId, System.Uri Uri, string userName, string password, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._Uri = Uri;
            this.Initialize(ipcContextId, searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();

                if (System.Web.HttpContext.Current != null)
                {
                    _referencedDataSet = null;
                }
                else
                {
                    this._referencedDataSet.Dispose();
                }
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }
        #endregion

        #region Public Methods

        public List<string> FetchDeliveryList(string custKey, int shiptoSeq, string saleType, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            List<string> dateStrings = new List<string>();

            Dmsi.Agility.EntryNET.StrongTypesNS.dsRouteDatesDataSet dsRouteDates = null;

            this.CreateSubAppObject(checkCache);

            if (this._subAppObject != null)
            {
                Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                string ipcContextId = "";

                try
                {
                    if (System.Web.HttpContext.Current != null)
                    {
                        DataManager dm = new DataManager();
                        ipcContextId = dm.GetCache("opcContextId") as string;
                    }

                    this._subAppObject.sicustomerfetchdelivlist(ipcContextId, custKey, shiptoSeq, saleType, out dsRouteDates, out ReturnCode, out MessageText);

                    if (ReturnCode != 0)
                    {
                        el.LogError(EventTypeFlag.ApplicationEvent, MessageText);
                    }

                    if (dsRouteDates.ttRouteDates.Rows.Count > 0)
                    {
                        foreach (DataRow row in dsRouteDates.ttRouteDates.Rows)
                        {
                            if (row["deliv-date"] != null && row["deliv-date"].ToString().Length > 0)
                                dateStrings.Add(row["deliv-date"].ToString().Substring(0, row["deliv-date"].ToString().IndexOf(" ")));
                            else
                                dateStrings.Add("Next available");
                        }
                    }

                    DisposeAppServerObjects();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    el.LogError(EventTypeFlag.AppServerDBerror, oe);
                    em.Message = oe.Message;
                    em.EventType = EventTypeFlag.AppServerDBerror;
                }
            }

            return dateStrings;
        }

        #endregion

        #region Private Methods

        private void Initialize(string ipcContextId, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds = null;

            // check if in ASP mode 
            if (checkCache && System.Web.HttpContext.Current != null)
            {
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

                ds = (dsCustomerDataSet)dm.GetCache("dsCustomerDataSet");

                if (searchCriteria == null && dm.GetCache("dsCustomerDataSet") != null)
                    this.Populate(ds);
                else // try to fetch the record from the database
                {
                    ds = new dsCustomerDataSet();
                    this.FetchCustomers(ipcContextId, searchCriteria, ds, checkCache, out ReturnCode, out MessageText);
                }
            }
            else
            {
                ds = new dsCustomerDataSet();
                this.FetchCustomers(ipcContextId, searchCriteria, ds, checkCache, out ReturnCode, out MessageText);
            }
        }

        private void FetchCustomers(string ipcContextId, string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.CreateSubAppObject(checkCache);

            if (this._subAppObject != null)
            {
                Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                try
                {
                    if (System.Web.HttpContext.Current != null)
                    {
                        DataManager dm = new DataManager();
                        ipcContextId = dm.GetCache("opcContextId") as string;
                    }

                    this._subAppObject.sicustomerfetchlistsf(ipcContextId, searchCriteria, out ds, out ReturnCode, out MessageText);
                    DisposeAppServerObjects();
                    this.Populate(ds);
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    el.LogError(EventTypeFlag.AppServerDBerror, oe);
                    em.Message = oe.Message;
                    em.EventType = EventTypeFlag.AppServerDBerror;
                }
            }
        }

        /// <summary>
        /// Assign values to properties
        /// </summary>
        /// <param name="ds"></param>
        private void Populate(Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds)
        {
            this._referencedDataSet = ds;
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject(bool checkCache)
        {
            Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        
            // Get ASP Session setting first
            if (checkCache && System.Web.HttpContext.Current != null)
            {
                user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
                
                this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                if (this._appServerConnection != null)
                    this._isConnectionFromOutside = true;

                this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                if (this._appObject != null)
                    this._isAppObjectFromOutside = true;

                this._subAppObject = (Dmsi.Agility.EntryNET.Customer)dm.GetCache("CustomersubAppObject");
                if (this._subAppObject != null)
                    this._isSubAppObjectFromOutside = true;
            }

            // if objects are still null then create them
            if (this._appServerConnection == null)
            {
                _connection = new Dmsi.Agility.Security.Connection();

                int loginStatus = -1;
                string result = "";

                if (user == null && this._Uri != null && 
                    this._userName.Length != 0 && this._password.Length != 0)
                {
                    if (_connection.Connect(this._Uri, this._userName, this._password, out result))
                        loginStatus = _connection.LogIn(this._userName, this._password, out result);
                }
                else
                {
                    if (user == null)
                    {
                        throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.");
                    }
                                
                    if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                        loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);
                }

                if (loginStatus == 0)
                {
                    this._appServerConnection = _connection.ProgressConnection;
                    this._appObject = _connection.ProgressApplicationObject;
                }
                else
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.Customers: " + result + "(" + loginStatus.ToString() + ")");
                    em.EventType = EventTypeFlag.AppServerDBerror;
                    em.Message = "Dmsi.Agility.Data.Customers: " + result + "(" + loginStatus.ToString() + ")";
                }
            }

            if (this._appObject == null)
                this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

            if (this._subAppObject == null)
                this._subAppObject = new Dmsi.Agility.EntryNET.Customer(this._appObject);
        }

        #endregion
    }
}