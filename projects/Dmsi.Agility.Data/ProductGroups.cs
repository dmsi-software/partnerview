// Product Groups dataset wrapper

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class ProductGroups: IDisposable
    {
        #region Members
        private bool _isConnectionFromOutside;
        Dmsi.Agility.Security.Connection _connection = null;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.Inventory _subAppObject;
        #endregion

        #region Properties
        private DataSet _referencedDataSet;

        /// <summary>
        /// Points to dsInventoryDataSet
        /// </summary>
        public DataSet ReferencedDataSet
        {
            get { return this._referencedDataSet; }
        }
        #endregion

        #region Contructors and Dispose
        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param> 
        public ProductGroups(string searchCriteria, out int ReturnCode, out string MessageText)
        {
            this.Initialize(searchCriteria, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }
        #endregion

        #region Methods
        public dsProductGroupDataSet Initialize(string searchCriteria, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            Dmsi.Agility.EntryNET.StrongTypesNS.dsProductGroupDataSet dsPG = new dsProductGroupDataSet(); ;

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        this._subAppObject.siinventoryfetchlistprodgrp(ipcContextId, searchCriteria, out dsPG, out ReturnCode, out MessageText);
                        this.Populate(dsPG);
                        DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }                
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }

            return dsPG;
        }

        /// <summary>
        /// Assign values to properties
        /// </summary>
        /// <param name="ds"></param>
        private void Populate(Dmsi.Agility.EntryNET.StrongTypesNS.dsProductGroupDataSet ds)
        {
            this._referencedDataSet = ds;
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject()
        {
            Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // Get ASP Session setting first
            if (System.Web.HttpContext.Current != null)
            {
                user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                if (this._appServerConnection != null)
                    this._isConnectionFromOutside = true;

                this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                if (this._appObject != null)
                    this._isAppObjectFromOutside = true;

                this._subAppObject = (Dmsi.Agility.EntryNET.Inventory)dm.GetCache("InventorySubAppObject");
                if (this._subAppObject != null)
                    this._isSubAppObjectFromOutside = true;
            }

            // if objects are still null then create them
            if (this._appServerConnection == null)
            {
                int loginStatus = -1;
                string result = "";

                _connection = new Dmsi.Agility.Security.Connection();

                if (user == null)
                {
                    throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.\r\nNon-ASP implementation is not yet supported.");
                }

                if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                    loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);

                if (loginStatus == 0)
                {
                    this._appServerConnection = _connection.ProgressConnection;
                    this._appObject = _connection.ProgressApplicationObject;
                    _connection.Dispose();
                }
                else
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.Inventory: " + result + "(" + loginStatus.ToString() + ")");
                    em.EventType = EventTypeFlag.AppServerDBerror;
                    em.Message = "Dmsi.Agility.Data.Inventory: " + result + "(" + loginStatus.ToString() + ")";
                }
            }

            if (this._appObject == null)
                this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

            if (this._subAppObject == null)
                this._subAppObject = new Dmsi.Agility.EntryNET.Inventory(this._appObject);
        }
        
        #endregion
    }
}
