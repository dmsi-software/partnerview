/// Sales Order and Credit Memo dataset wrapper
/// Type={Sales Order|Credit Memo}

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class SalesOrders: IDisposable
    {
        #region Members
        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.SalesOrders _subAppObject;
        #endregion

        #region Properties
        private DataTable _referencedHeaderTable;
        private DataTable _referencedDetailTable;
        private DataSet _referencedDataSet;

        /// <summary>
        /// Points to ttso_header
        /// </summary>
        public DataTable ReferenceHeaderTable
        {
            get { return this._referencedHeaderTable; }
        }

        /// <summary>
        /// Points to ttso_detail
        /// </summary>
        public DataTable ReferencedDetailTable
        {
            get { return this._referencedDetailTable; }
        }

        /// <summary>
        /// Points to dsSalesOrderDataSet
        /// </summary>
        public DataSet ReferencedDataSet
        {
            get { return this._referencedDataSet; }
        }
        #endregion

        #region Contructors and Dispose
        /// <summary>
        /// Get the quote dateset from cache
        /// </summary>
        public SalesOrders(string sessionVariable, out int ReturnCode, out string MessageText)
        {
            this.Initialize(null, null, sessionVariable, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param>
        /// <param name="customer">Current customer, shiptos of the customer must be populated</param> 
        public SalesOrders(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet customer, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            this.Initialize(searchCriteria, customer, sessionVariable, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();

                if (System.Web.HttpContext.Current != null)
                {
                    _referencedHeaderTable = null;
                    _referencedDetailTable = null;
                    _referencedDataSet = null;
                }
                else
                {
                    this._referencedHeaderTable.Dispose();
                    this._referencedDetailTable.Dispose();
                    this._referencedDataSet.Dispose();
                }
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }
        #endregion

        #region Methods
        private void Initialize(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet customer, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet ds = null;
           
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                if ((searchCriteria == null || customer == null) && dm.GetCache(sessionVariable) != null)
                {
                    ds = (dsSalesOrderDataSet)dm.GetCache(sessionVariable);
                    this.Populate(ds);
                }
                else // try to fetch the record from the database
                {
                    this.CreateSubAppObject();

                    if (this._subAppObject != null)
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();
                        ds = new dsSalesOrderDataSet();

                        try
                        {
                            string ipcContextId = dm.GetCache("opcContextId") as string;

                            this._subAppObject.sisalesorderfetchlist(ipcContextId, searchCriteria, customer, out ds, out ReturnCode, out MessageText);
                            this.Populate(ds);
                            DisposeAppServerObjects();
                        }
                        catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                        {
                            el.LogError(EventTypeFlag.AppServerDBerror, oe);
                            em.Message = oe.Message;
                            em.EventType = EventTypeFlag.AppServerDBerror;
                        }
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        /// <summary>
        /// Assign values to properties
        /// </summary>
        /// <param name="ds"></param>
        private void Populate(Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet ds)
        {
            this._referencedDataSet = ds;
            this._referencedHeaderTable = ds.ttso_header;
            this._referencedDetailTable = ds.ttso_detail;
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject()
        {
            Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // Get ASP Session setting first
            if (System.Web.HttpContext.Current != null)
            {
                user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                if (this._appServerConnection != null)
                    this._isConnectionFromOutside = true;

                this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                if (this._appObject != null)
                    this._isAppObjectFromOutside = true;

                this._subAppObject = (Dmsi.Agility.EntryNET.SalesOrders)dm.GetCache("SalesOrderSubAppObject");
                if (this._subAppObject != null)
                    this._isSubAppObjectFromOutside = true;
            }

            // if objects are still null then create them
            if (this._appServerConnection == null)
            {
                int loginStatus = -1;
                string result = "";

                _connection = new Dmsi.Agility.Security.Connection();

                if (user == null)
                {
                    throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.\r\nNon-ASP implementation is not yet supported.");
                }

                if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                    loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);

                if (loginStatus == 0)
                {
                    this._appServerConnection = _connection.ProgressConnection;
                    this._appObject = _connection.ProgressApplicationObject;
                    _connection.Dispose();
                }
                else
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")");
                    em.EventType = EventTypeFlag.AppServerDBerror;
                    em.Message = "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")";
                }
            }

            if (this._appObject == null)
                this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

            if (this._subAppObject == null)
                this._subAppObject = new Dmsi.Agility.EntryNET.SalesOrders(this._appObject);
        }
        
        #endregion
    }
}
