// This object is just use for pushing data to the database

using System;
using System.Data;
using System.Threading;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

namespace Dmsi.Agility.Data
{
    public class SalesOrder : IDisposable
    {
        #region Members

        private Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.SalesOrders _subAppObject;
        private System.Uri _uri;
        private string _userName;
        private string _password;
        private string _branchID;
        private string _contextID;

        private dsRetailAddressDataSet _dsRetailAddress;

        #endregion

        #region Properties

        dsSalesOrderDataSet _CurrentSalesOrder;

        public dsSalesOrderDataSet CurrentSalesOrder
        {
            get { return _CurrentSalesOrder; }
        }

        #endregion

        #region Contructors and Dispose

        /// <summary>
        /// In ASP implementation, cache the object.
        /// </summary>
        /// <param name="branchID">Currently selected branch ID</param>
        /// <param name="headerRow">ttso_header which includes the user input from order capture</param>
        /// <param name="cart">Selected items and quantity</param>
        public SalesOrder(string branchID, dsSalesOrderDataSet.ttso_headerRow headerRow, dsOrderCartDataSet cart)
        {
            this._branchID = branchID;
            this.ConvertCartToOrder(headerRow, cart, true);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Url">AppServer connection string</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="branchID">Currently selected branch ID</param>
        /// <param name="headerRow">ttso_header which includes the user input from order capture</param>
        /// <param name="cart">Selected items and quantity</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public SalesOrder(string Url, string userName, string password, string branchID, dsSalesOrderDataSet.ttso_headerRow headerRow, dsOrderCartDataSet cart, bool checkCache)
        {
            this._branchID = branchID;
            this._userName = userName;
            this._password = password;
            this._uri = new System.Uri(Url);
            this.ConvertCartToOrder(headerRow, cart, checkCache);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Uri">AppServer connetion Uri</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="branchID">Currently selected branch ID</param>
        /// <param name="headerRow">ttso_header which includes the user input from order capture</param>
        /// <param name="cart">Selected items and quantity</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public SalesOrder(System.Uri Uri, string userName, string password, string branchID, dsSalesOrderDataSet.ttso_headerRow headerRow, dsOrderCartDataSet cart, bool checkCache)
        {
            this._branchID = branchID;
            this._userName = userName;
            this._password = password;
            this._uri = Uri;
            this.ConvertCartToOrder(headerRow, cart, checkCache);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }
        #endregion

        #region Public Methods

        public void BeginSaveSalesOrder()
        {
            DataManager dm = new DataManager();
            _contextID = dm.GetCache("opcContextId") as string;
            this.CreateSubAppObject(true);
            _CurrentSalesOrder.AcceptChanges();

            ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(SaveSalesOrder), _subAppObject);
        }

        #endregion

        #region Private Methods

        private void SaveSalesOrder(object s)
        {
            int ReturnCode = 0;
            string MessageText = "";

            EntryNET.SalesOrders subAppObject = s as EntryNET.SalesOrders;

            string message = "";
            string salesOrderID = "";

            Dmsi.Agility.EntryNET.EventLog el = new EventLog();
            Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

            try
            {

# if DEBUG
                    el.LogError(EventTypeFlag.ApplicationEvent, DateTime.Now.ToString() + ": Started .sisalesordersave");
                    //_dsRetailAddress.WriteXml("c:\\tmp\\dsRetailAddress.xml");
#endif

                subAppObject.sisalesordersave(_contextID, _branchID, _CurrentSalesOrder, _dsRetailAddress, out salesOrderID, out message, out ReturnCode, out MessageText);

# if DEBUG
                    el.LogError(EventTypeFlag.ApplicationEvent, DateTime.Now.ToString() + ": Ended .sisalesordersave");
#endif

                if (ReturnCode != 0)
                {
                    el.LogError(EventTypeFlag.ApplicationEvent, DateTime.Now.ToString() + ": ReturnCode = " + ReturnCode + ": MessageText = " + MessageText);
                }
                
                DisposeAppServerObjects();
                this.Dispose();
            }
            catch (Progress.Open4GL.Exceptions.Open4GLException oe)
            {
                el.LogError(EventTypeFlag.AppServerDBerror, oe);
                em.Message = oe.Message;
                em.EventType = EventTypeFlag.AppServerDBerror;
            }
        }

        /// <summary>
        /// Convert cart into sales order dataset.
        /// </summary>
        /// <param name="headerRow">ttso_header which includes the user input from order capture</param>
        /// <param name="cart"></param>
        /// <param name="salesOrder"></param>
        /// <param name="checkCache"></param>
        private void ConvertCartToOrder(dsSalesOrderDataSet.ttso_headerRow headerRow, dsOrderCartDataSet cart, bool checkCache)
        {
            //            Dmsi.Agility.EntryNET.EventLog el;

            //# if DEBUG
            //            el = new EventLog();
            //            el.LogError(EventTypeFlag.ApplicationEvent, DateTime.Now.ToString() + ": Started ConvertCartToOrder");
            //#endif

            // TODO: Add the conversion of the current header row and cart to the corresponding sales order dataset.
            //       If it make sense to just send the fully defined Sales Order dataset, modify the constructors
            //       and just let this be a simple assignment of _CurrentSalesOrder. This object is needed in 
            //       SaveSalesOrder.
            dsSalesOrderDataSet dsSO = new dsSalesOrderDataSet();
            DataManager dm = new DataManager();
            PriceModeHelper pmh = new PriceModeHelper(dm);

            dsSO.ttso_header.Rows.Add(headerRow.ItemArray);
            string cRetailMode = pmh.GetRetailMode();
            decimal dRetailMarkupFactor = pmh.GetRetailMarkupFactor();

            foreach (DataRow cartDetail in cart.ttorder_cart.Rows)
            {
                dsSalesOrderDataSet.ttso_detailRow soDetail = dsSO.ttso_detail.Newttso_detailRow();
                for (int i = 0; i < soDetail.ItemArray.Length; i++)
                {
                    try
                    {
                        soDetail[i] = "";
                    }
                    catch
                    {
                        try
                        {
                            soDetail[i] = DateTime.Now;
                        }
                        catch
                        {
                            try
                            {
                                soDetail[i] = 0;
                            }
                            catch
                            {
                                soDetail[i] = false;
                            }
                        }
                    }
                }

                soDetail.display_seq = (int)cartDetail["sequence"];
                soDetail.ITEM = cartDetail["ITEM"].ToString();
                soDetail.SIZE = cartDetail["SIZE"].ToString();
                soDetail.so_desc = cartDetail["DESCRIPTION"].ToString();
                soDetail.qty_ordered = Convert.ToDecimal(cartDetail["qty"]);
                soDetail.uom = cartDetail["uom"].ToString();
                soDetail.ordqty_uom_conv_ptr = Convert.ToInt32(cartDetail["qty_uom_conv_ptr"]);
                soDetail.ord_qty_uom_conv_ptr_sysid = cartDetail["qty_uom_conv_ptr_sysid"].ToString();
                soDetail.line_message = cartDetail["line_message"].ToString();
                soDetail.location_reference = cartDetail["location_reference"].ToString();
                soDetail.thickness = (decimal)cartDetail["thickness"];
                soDetail.WIDTH = (decimal)cartDetail["WIDTH"];
                soDetail.LENGTH = (decimal)cartDetail["LENGTH"];
                soDetail.price = (decimal)cartDetail["price"];
                soDetail.price_uom_code = cartDetail["price_uom"].ToString();

                soDetail.retail_listbased_price = (decimal)cartDetail["estimated_listbased_price"];

                dsSO.ttso_detail.Rows.Add(soDetail);

                if (dm.GetCache("UserPref_DisplaySetting") != null &&
                   ((string)dm.GetCache("UserPref_DisplaySetting") == "Retail price" ||
                    (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - markup on cost" ||
                    (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - discount from list"))
                {
                    soDetail.retail_price = (decimal)cartDetail["retail_price"];
                    soDetail.retail_extended_price = (decimal)cartDetail["retail_extension"];
                    soDetail.retail_price_uom_code = (string)cartDetail["price_uom"];
                    soDetail.retail_markup_factor = dRetailMarkupFactor;
                    soDetail.retail_mode = cRetailMode;

                    _dsRetailAddress = new dsRetailAddressDataSet();

                    if (dm.GetCache("AddInfo_txtCustomerId") != null &&
                        !((string)dm.GetCache("AddInfo_txtCustomerId") == "" &&
                        (string)dm.GetCache("AddInfo_txtShipTo") == "" &&
                        (string)dm.GetCache("AddInfo_txtCustomerName") == "" &&
                        (string)dm.GetCache("AddInfo_txtAddress1") == "" &&
                        (string)dm.GetCache("AddInfo_txtAddress2") == "" &&
                        (string)dm.GetCache("AddInfo_txtAddress3") == "" &&
                        (string)dm.GetCache("AddInfo_txtCity") == "" &&
                        (string)dm.GetCache("AddInfo_txtCity") == "" &&
                        (string)dm.GetCache("AddInfo_txtState") == "" &&
                        (string)dm.GetCache("AddInfo_txtZip") == "" &&
                        (string)dm.GetCache("AddInfo_txtCountry") == "" &&
                        (string)dm.GetCache("AddInfo_txtPhone") == "" &&
                        (string)dm.GetCache("AddInfo_txtContactName") == "" &&
                        (string)dm.GetCache("AddInfo_txtEmail") == "" &&
                        (string)dm.GetCache("AddInfo_txtReference") == "" &&
                        ((decimal)dm.GetCache("AddInfo_taxRate")).ToStringTrimmed() == "" &&
                        (string)dm.GetCache("AddInfo_taxableDescription") == "" &&
                        ((decimal)dm.GetCache("AddInfo_taxableAmount")).ToStringTrimmed() == "" &
                        (string)dm.GetCache("AddInfo_nonTaxableDescription") == "" &
                        ((decimal)dm.GetCache("AddInfo_nonTaxableAmount")).ToStringTrimmed() == ""))
                    {
                        DataRow newRow = _dsRetailAddress.tt_retail_address.NewRow();

                        foreach (DataColumn col in newRow.Table.Columns)
                        {
                            Type t = col.DataType;
                            switch (t.ToString().ToLower())
                            {
                                case "system.int32":
                                    newRow[col.Caption] = 0;
                                    break;

                                case "system.string":
                                    newRow[col.Caption] = "";
                                    break;

                                case "system.decimal":
                                    newRow[col.Caption] = 0;
                                    break;

                                case "system.boolean":
                                    newRow[col.Caption] = false;
                                    break;
                            }
                        }

                        newRow["misc_1"] = (string)dm.GetCache("AddInfo_txtCustomerId");
                        newRow["misc_2"] = (string)dm.GetCache("AddInfo_txtShipTo");
                        newRow["cust_name"] = (string)dm.GetCache("AddInfo_txtCustomerName");
                        newRow["address_1"] = (string)dm.GetCache("AddInfo_txtAddress1");
                        newRow["address_2"] = (string)dm.GetCache("AddInfo_txtAddress2");
                        newRow["address_3"] = (string)dm.GetCache("AddInfo_txtAddress3");
                        newRow["city"] = (string)dm.GetCache("AddInfo_txtCity");
                        newRow["state"] = (string)dm.GetCache("AddInfo_txtState");
                        newRow["zip"] = (string)dm.GetCache("AddInfo_txtZip");
                        newRow["country"] = (string)dm.GetCache("AddInfo_txtCountry");
                        newRow["phone"] = (string)dm.GetCache("AddInfo_txtPhone");
                        newRow["misc_3"] = (string)dm.GetCache("AddInfo_txtContactName");
                        newRow["email"] = (string)dm.GetCache("AddInfo_txtEmail");
                        newRow["misc_4"] = (string)dm.GetCache("AddInfo_txtReference");
                        newRow["misc_5"] = ((decimal)dm.GetCache("AddInfo_taxRate")).ToStringTrimmed();
                        newRow["misc_6"] = (string)dm.GetCache("AddInfo_taxableDescription");
                        newRow["misc_7"] = ((decimal)dm.GetCache("AddInfo_taxableAmount")).ToStringTrimmed();
                        newRow["misc_8"] = (string)dm.GetCache("AddInfo_nonTaxableDescription");
                        newRow["misc_9"] = ((decimal)dm.GetCache("AddInfo_nonTaxableAmount")).ToStringTrimmed();

                        newRow["created_date"] = DBNull.Value;
                        newRow["misc_date1"] = DBNull.Value;
                        newRow["misc_date2"] = DBNull.Value;
                        newRow["shipping_date"] = DBNull.Value;
                        newRow["update_date"] = DBNull.Value;
                        newRow["update_time"] = DBNull.Value;

                        newRow["tran_sysid"] = 0;
                        newRow["type"] = "PT";
                        newRow["tran_id"] = 0;
                        newRow["shipment_num"] = 0;
                        newRow["dispatch_tran_sysid"] = 0;
                        newRow["dispatch_tran_type"] = "";
                        newRow["dispatch_tran_id"] = 0;
                        newRow["dispatch_shipment_num"] = 0;

                        _dsRetailAddress.tt_retail_address.Rows.Add(newRow);
                        _dsRetailAddress.AcceptChanges();
                    }
                }
            }

            _CurrentSalesOrder = dsSO;

            // NOTE: Do not delete the cart rows inside here; eventhough, it's possible.

            //# if DEBUG
            //            el.LogError(EventTypeFlag.ApplicationEvent, DateTime.Now.ToString() + ": Ended ConvertCartToOrder");
            //#endif
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject(bool checkCache)
        {
            // Check if the object is already created
            if (this._subAppObject == null)
            {
                Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

                // Get ASP Session setting first
                if (checkCache && System.Web.HttpContext.Current != null)
                {
                    user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                    this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                    if (this._appServerConnection != null)
                        this._isConnectionFromOutside = true;

                    this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                    if (this._appObject != null)
                        this._isAppObjectFromOutside = true;

                    this._subAppObject = (Dmsi.Agility.EntryNET.SalesOrders)dm.GetCache("SalesOrderSubAppObject");
                    if (this._subAppObject != null)
                        this._isSubAppObjectFromOutside = true;
                }

                // if objects are still null then create them
                if (this._appServerConnection == null)
                {
                    _connection = new Dmsi.Agility.Security.Connection();

                    int loginStatus = -1;
                    string result = "";

                    if (user == null && this._uri != null &&
                        this._userName.Length != 0 && this._password.Length != 0)
                    {
                        if (_connection.Connect(this._uri, this._userName, this._password, out result))
                            loginStatus = _connection.LogIn(this._userName, this._password, out result);
                    }
                    else
                    {
                        if (user == null)
                        {
                            throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.");
                        }

                        if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                            loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);
                    }

                    if (loginStatus == 0)
                    {
                        this._appServerConnection = _connection.ProgressConnection;
                        this._appObject = _connection.ProgressApplicationObject;
                    }
                    else
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                        el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.SalesOrder: " + result + "(" + loginStatus.ToString() + ")");
                        em.EventType = EventTypeFlag.AppServerDBerror;
                        em.Message = "Dmsi.Agility.Data.SalesOrder: " + result + "(" + loginStatus.ToString() + ")";
                    }
                }

                if (this._appObject == null)
                    this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

                if (this._subAppObject == null)
                    this._subAppObject = new Dmsi.Agility.EntryNET.SalesOrders(this._appObject);
            }
        }

        #endregion
    }
}
