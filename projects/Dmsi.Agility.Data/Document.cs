// Single Document object from a DataRow
// When specifying the searchCriteria for back-end fetch, make sure it's for only one Document.

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class Document: IDisposable
    {
        #region Members
        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.Document _subAppObject;
        private System.Uri _uri;
        private string _userName;
        private string _password;
        #endregion

        #region Properties

        private string _documentUrl = "";
        private DataRow _referencedRow = null;
        private string _ContextID = "";
        private string _TranID;
        private string _Type;

        public string DocumentUrl
        {
            get { return _documentUrl; }
        }

        public DataRow ReferencedRow
        {
            get { return _referencedRow; }
        }

        public string ContextID
        {
            get { return _ContextID; }
            set { _ContextID = value; }
        }

        public string TranID
        {
            get { return _TranID; }
            set { _TranID = value; }
        }

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        #endregion

        #region Constructor and Dispose

        public Document() //Default ctor...
        {
        }

        /// <summary>
        /// Get initial data from cache else database
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param>
        public Document(string searchCriteria, out int ReturnCode, out string MessageText)
        {
            if (searchCriteria == null)
                throw (new Exception("Search criteria for detail document can not be null."));
            else
                this.Initialize(searchCriteria, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// This is use to fill the object from an existing row
        /// </summary>
        /// <param name="row"></param>
        public Document(DataRow row)
        {
            if (row == null)
                throw (new Exception("Source DataRow can not be null."));
            else
                this.Populate(row, "");
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Url">AppServer connection string</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="custObj">cust_obj</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public Document(string Url, string userName, string password, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            this._userName = userName;
            this._password = password;
            this._uri = new System.Uri(Url);
            this.Initialize(searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Uri">AppServer connetion Uri</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="custObj">cust_obj</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public Document(System.Uri Uri, string userName, string password, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            this._userName = userName;
            this._password = password;
            this._uri = Uri;
            this.Initialize(searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource


                // Make sure the object refernece are created inside the class and not from cache
                DisposeAppServerObjects();

                // this will be cached outside of this context, so dispose is not really advisable

                if (System.Web.HttpContext.Current != null)
                {
                    this._referencedRow = null;
                }
                else
                {
                    this._referencedRow = null;
                }
            }
            // free native resources
        }

        public void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }

        #endregion

        #region Public Methods

        public byte[] GetCQFileBytes(string ipcContextId, string retailFormLogoURL)
        {
            byte[] returnBytes = null;

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            if (this._subAppObject != null)
            {
                Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                try
                {
                    string convertedType = "";
                    if (this.Type == "Quote")
                        convertedType = "QU";
                   
                    int ReturnCode;
                    string MessageText;
                    string BinaryData = "";

                    this._subAppObject.PrintCustomerEstimate(ipcContextId, convertedType, Convert.ToInt32(this.TranID), retailFormLogoURL, out BinaryData, out ReturnCode, out MessageText);

                    if (BinaryData != null)
                    {
                        returnBytes = new byte[BinaryData.Length];
                        returnBytes = Convert.FromBase64String(BinaryData);
                    }

                    DisposeAppServerObjects();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    el.LogError(EventTypeFlag.AppServerDBerror, oe);
                    em.Message = oe.Message;
                    em.EventType = EventTypeFlag.AppServerDBerror;
                }
            }

            return returnBytes;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get the data either from the cache or from the database
        /// </summary>
        /// <param name="searchCriteria">For ASP string cust_obj is enough else use the name-value pairs for back-end retrieval</param>
        /// <param name="checkCache">Check session cache first</param>
        private void Initialize(string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            string opcConfigstr = "";
            string documentUrl = "";
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            Dmsi.Agility.EntryNET.StrongTypesNS.dsDetailDocDataSet ds = null;
           
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null || !checkCache)
            {
                this.CreateSubAppObject(checkCache);

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();
                    ds = new dsDetailDocDataSet();

                    try
                    {
                        string ipcContextId = dm.GetCache("opcContextId") as string;
                        this.ContextID = ipcContextId;
                   
                        this._subAppObject.sifetchdetaildocument(ipcContextId, searchCriteria, out ds, out documentUrl, out opcConfigstr, out ReturnCode, out MessageText);

                        if (ds.ttDetailDoc.Rows.Count > 0)
                            this.Populate(ds.ttDetailDoc.Rows[0], documentUrl);
                        else
                            this.Populate(null, documentUrl);

                        //DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        /// <summary>
        /// This is the assignment of all commonly used fields
        /// </summary>
        /// <param name="row"></param>
        private void Populate(DataRow row, string documentUrl)
        {
            this._documentUrl = documentUrl;

            if (row != null)
                this._referencedRow = row;
        }


        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject(bool checkCache)
        {
            // Check if the object is already created
            if (this._subAppObject == null)
            {
                Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            
                // Get ASP Session setting first
                if (checkCache && System.Web.HttpContext.Current != null)
                {
                    user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                    this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                    if (this._appServerConnection != null)
                        this._isConnectionFromOutside = true;

                    this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                    if (this._appObject != null)
                        this._isAppObjectFromOutside = true;

                    this._subAppObject = (Dmsi.Agility.EntryNET.Document)dm.GetCache("DocumentSubAppObject");
                    if (this._subAppObject != null)
                        this._isSubAppObjectFromOutside = true;
                }

                // if objects are still null then create them
                if (this._appServerConnection == null)
                {
                    _connection = new Dmsi.Agility.Security.Connection();

                    int loginStatus = -1;
                    string result = "";

                    if (user == null && this._uri != null &&
                        this._userName.Length != 0 && this._password.Length != 0)
                    {
                        if (_connection.Connect(this._uri, this._userName, this._password, out result))
                            loginStatus = _connection.LogIn(this._userName, this._password, out result);
                    }
                    else
                    {
                        if (user == null)
                        {
                            //throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.");

                            System.Web.HttpContext.Current.Response.Redirect("~/default.aspx", true);

                            Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                            Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                            el.LogError(EventTypeFlag.ApplicationEvent, "Cached user name and password not found. If this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.");
                            em.EventType = EventTypeFlag.ApplicationEvent;
                            em.Message = "Cached user name and password not found. If this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.";
                        }

                        if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                            loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);
                    }

                    if (loginStatus == 0)
                    {
                        this._appServerConnection = _connection.ProgressConnection;
                        this._appObject = _connection.ProgressApplicationObject;
                    }
                    else
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                        el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.Document: " + result + "(" + loginStatus.ToString() + ")");
                        em.EventType = EventTypeFlag.AppServerDBerror;
                        em.Message = "Dmsi.Agility.Data.Document: " + result + "(" + loginStatus.ToString() + ")";
                    }
                }

                if (this._appObject == null)
                    this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

                if (this._subAppObject == null)
                    this._subAppObject = new Dmsi.Agility.EntryNET.Document(this._appObject);
            }
        }
#endregion
    }
}
