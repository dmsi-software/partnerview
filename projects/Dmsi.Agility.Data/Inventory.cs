using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class Inventory: IDisposable
    {
        #region Members

        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.Inventory _subAppObject;

        #endregion

        #region Properties

        private DataSet _referencedDataSet;
        private DataTable _referencedHeaderTable;
        private DataTable _referencedPriceTable;
        private DataTable _referencedAvailableTotalTable;
        private DataTable _referencedAvailableBranchTable;
        private DataTable _referencedAvailableTallyHeaderTable;
        private DataTable _referencedAvailableTallyDetailTable;
        private DataTable _referencedOnOrderTotalTable;
        private DataTable _referencedOnOrderBranchTable;
        private DataTable _referencedOnOrderTranHeaderTable;
        private DataTable _referencedOnOrderTranDetailTable;

        private dsPVItemXrefDataSet _XrefDataSet;

        dsCategoryDataSet _ReferencedCategoryDataSet;
        dsBannerDataSet _ReferencedBannerDataSet;
        dsGroupDataSet _ReferencedGroupDataSet;
        dsAttributeDataSet _ReferencedAttributeDataSet;
        dsItemImageDocDataSet _ReferencedItemDocDataSet;
        dsItemImageDocDataSet _ReferencedItemImageDataSet;


        /// <summary>
        /// Points to ttiventory
        /// </summary>
        public DataTable ReferenceHeaderTable
        {
            get { return this._referencedHeaderTable; }
        }

        /// <summary>
        /// Points to ttPriceInfo
        /// </summary>
        public DataTable ReferencedPriceTable
        {
            get { return this._referencedPriceTable; }
        }

        /// <summary>
        /// Points to ttavail
        /// </summary>
        public DataTable ReferencedAvailableTotalTable
        {
            get { return this._referencedAvailableTotalTable; }
        }

        /// <summary>
        /// Points to ttavail_branch
        /// </summary>
        public DataTable ReferencedAvailableBranchTable
        {
            get { return this._referencedAvailableBranchTable; }
        }

        /// <summary>
        /// Points to ttavail_header
        /// </summary>
        public DataTable ReferencedAvailableTallyHeaderTable
        {
            get { return this._referencedAvailableTallyHeaderTable; }
        }

        /// <summary>
        /// Points to ttavail_detail
        /// </summary>
        public DataTable ReferencedAvailableTallyDetailTable
        {
            get { return this._referencedAvailableTallyDetailTable; }
        }

        /// <summary>
        /// Points to tton_order
        /// </summary>
        public DataTable ReferencedOnOrderTotalTable
        {
            get { return this._referencedOnOrderTotalTable; }
        }

        /// <summary>
        /// Points to tton_order_branch
        /// </summary>
        public DataTable ReferencedOnOrderBranchTable
        {
            get { return this._referencedOnOrderBranchTable; }
        }

        /// <summary>
        /// Points to tton_order_header
        /// </summary>
        public DataTable ReferencedOnOrderTranHeaderTable
        {
            get { return this._referencedOnOrderTranHeaderTable; }
        }

        /// <summary>
        /// Points to tton_order_detail
        /// </summary>
        public DataTable ReferencedOnOrderTranDetailTable
        {
            get { return this._referencedOnOrderTranDetailTable; }
        }

        /// <summary>
        /// Points to dsInventoryDataSet
        /// </summary>
        public DataSet ReferencedDataSet
        {
            get { return this._referencedDataSet; }
        }

        public dsPVItemXrefDataSet XrefDataSet
        {
            get { return _XrefDataSet; }
            set { _XrefDataSet = value; }
        }

        public dsCategoryDataSet ReferencedCategoryDataSet
        {
            get { return _ReferencedCategoryDataSet; }
        }

        public dsBannerDataSet ReferencedBannerDataSet
        {
            get { return _ReferencedBannerDataSet; }
        }

        public dsGroupDataSet ReferencedGroupDataSet
        {
            get { return _ReferencedGroupDataSet; }
        }

        public dsAttributeDataSet ReferencedAttributeDataSet
        {
            get { return _ReferencedAttributeDataSet; }
        }

        public dsItemImageDocDataSet ReferencedItemDocDataSet
        {
            get { return _ReferencedItemDocDataSet; }
        }

        public dsItemImageDocDataSet ReferencedItemImageDataSet
        {
            get { return _ReferencedItemImageDataSet; }
        }
        
        #endregion

        #region Contructors and Dispose

        /// <summary>
        /// Get the quote dateset from cache
        /// </summary>
        public Inventory(out int ReturnCode, out string MessageText)
        {
            this.Initialize(null, null, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param>
        /// <param name="customer">Current customer, shiptos of the customer must be populated</param> 
        public Inventory(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsPartnerVuDataSet dsPV, out int ReturnCode, out string MessageText)
        {
            this.Initialize(searchCriteria, dsPV, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();

                if (System.Web.HttpContext.Current != null)
                {
                    _referencedHeaderTable = null;
                    _referencedPriceTable = null;
                    _referencedDataSet = null;
                }
                else
                {
                    this._referencedHeaderTable.Dispose();
                    this._referencedPriceTable.Dispose();
                    this._referencedDataSet.Dispose();
                }
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }

        #endregion

        #region Methods

        #region Public Methods

        public void FetchUOM(string branch, decimal markupfactor, ref dsInventoryDataSet ds, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {

                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        string ipcContextId = dm.GetCache("opcContextId") as string;
                        this._subAppObject.sipvselectuom(ipcContextId, branch, markupfactor, ref ds, out ReturnCode, out MessageText);
                        DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void FetchItemImagesOrDocuments(string FetchType, string ItemCode, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        dsItemImageDocDataSet dsItemImageDoc = new dsItemImageDocDataSet();

                        this._subAppObject.siitemimagedocfetchpv(ipcContextId, FetchType, ItemCode, out dsItemImageDoc, out ReturnCode, out MessageText);

                        if (FetchType.ToLower() == "document")
                        {
                            foreach (DataRow row in dsItemImageDoc.ttItemImageDoc.Rows)
                            {
                                if (row["display_text"].ToString().Length == 0)
                                {
                                    string sb = row["image_file"].ToString();

                                    if (sb.Length > 100)
                                        sb = sb.Substring(0, 100) + "...";

                                    row["display_text"] = sb;
                                }
                            }

                            this._ReferencedItemDocDataSet = dsItemImageDoc;
                        }

                        if (FetchType.ToLower() == "image")
                            this._ReferencedItemImageDataSet = dsItemImageDoc;

                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void FetchHomeCategories(out int ReturnCode, out string MessageText)
        {
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                ReturnCode = 0;
                MessageText = "";

                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        dsCategoryDataSet dsCategory = new dsCategoryDataSet();
                        dsBannerDataSet dsBanner = new dsBannerDataSet();                        

                        this._subAppObject.sicategoryfetchhome(ipcContextId, out dsCategory, out dsBanner, out ReturnCode, out MessageText);

                        this._ReferencedCategoryDataSet = dsCategory;
                        this._ReferencedBannerDataSet = dsBanner;

                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void FetchGroups(string CategoryGuid, out int ReturnCode, out string MessageText)
        {
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                ReturnCode = 0;
                MessageText = "";

                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        dsCategoryDataSet dsCategory = new dsCategoryDataSet();
                        dsBannerDataSet dsBanner = new dsBannerDataSet();
                        dsGroupDataSet dsGroup = new dsGroupDataSet();

                        this._subAppObject.sigroupfetchlist(ipcContextId, CategoryGuid, out dsCategory, out dsBanner, out dsGroup, out ReturnCode, out MessageText);

                        this._ReferencedCategoryDataSet = dsCategory;
                        this._ReferencedBannerDataSet = dsBanner;
                        this._ReferencedGroupDataSet = dsGroup;

                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void FetchAttributes(string CategoryGuid, string GroupGuid, out int ReturnCode, out string MessageText)
        {
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                ReturnCode = 0;
                MessageText = "";

                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        dsAttributeDataSet dsAttribute = new dsAttributeDataSet();
                        dsBannerDataSet dsBanner = new dsBannerDataSet();

                        this._subAppObject.siattributefetchlist(ipcContextId, CategoryGuid, GroupGuid, out dsAttribute, out dsBanner, out ReturnCode, out MessageText);

                        this._ReferencedAttributeDataSet = dsAttribute;
                        this._ReferencedBannerDataSet = dsBanner;

                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void FetchItemsByAttribute(string GroupGuid, string CustomerID, int CustomerShipToSeq, string saleType, string retailMode, decimal MarkupFactor, dsSelectedAttributesDataSet dsSelectedAttributes, out int ReturnCode, out string MessageText)
        {
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                ReturnCode = 0;
                MessageText = "";

                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();

                        string ipcContextId = dm.GetCache("opcContextId") as string;
                        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

                        dsInventoryDataSet dsInv = new dsInventoryDataSet();

                        this._subAppObject.siinventoryfetchlistbyattribute(ipcContextId, GroupGuid, CustomerID, CustomerShipToSeq, saleType, retailMode, dsPV, dsSelectedAttributes, MarkupFactor, out dsInv, out ReturnCode, out MessageText);
                        this.Populate(dsInv);
                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void FetchItemsComplementaryItems(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsPartnerVuDataSet dsPV, ref Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet ds, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        if (searchCriteria != null && searchCriteria.Length > 0)
                        {
                            if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
                                dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - discount from list")
                            {
                                searchCriteria += "\x0003RetailMode=List";
                            }

                            if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
                                dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - markup on cost")
                            {
                                searchCriteria += "\x0003RetailMode=Net";
                            }

                            this._subAppObject.siinventoryfetchcomplist(ipcContextId, searchCriteria, dsPV, ref ds, out ReturnCode, out MessageText);
                            dm.SetCache("CompItemsHaveBeenFetched", true); //only way to keep the ComplementaryItems.ascx.cs from rebinding data in Page_PreRender event.
                            //This makes it a "Singleton" load.
                        }

                        DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void UpdateQuickList(string branchID, string userID, decimal shiptoObj, Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet dsInventory, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        this._subAppObject.sisavequicklist(ipcContextId, branchID, userID, "User", shiptoObj, dsInventory, out ReturnCode, out MessageText);
                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public void FetchItemXrefs(string ipcSelectionCriteria, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        dsPVItemXrefDataSet dsPVItemXref = null;

                        this._subAppObject.siinventoryfetchlistxref(ipcContextId, ipcSelectionCriteria, out dsPVItemXref, out ReturnCode, out MessageText);
                        this.XrefDataSet = dsPVItemXref;
                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        public string AddItemXref(string ipcSelectionCriteria, string itemxref, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        dsPVItemXrefDataSet dsPVItemXref = new dsPVItemXrefDataSet();
                        dsPVItemXref.ttPVItem_xref.AddttPVItem_xrefRow("", itemxref, "", "", "", "", "");
                        dsPVItemXref.AcceptChanges();

                        this._subAppObject.siinventoryaddxref(ipcContextId, ipcSelectionCriteria, ref dsPVItemXref, out ReturnCode, out MessageText);
                        this.XrefDataSet = dsPVItemXref;
                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }

            return MessageText;
        }

        public string DeleteItemXref(string ipcSelectionCriteria, string itemxref, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        DataManager dm = new DataManager();
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        dsPVItemXrefDataSet dsPVItemXref = new dsPVItemXrefDataSet();
                        dsPVItemXref.ttPVItem_xref.AddttPVItem_xrefRow("", itemxref, "", "", "", "", "");
                        dsPVItemXref.AcceptChanges();

                        this._subAppObject.siinventorydeletexref(ipcContextId, ipcSelectionCriteria, ref dsPVItemXref, out ReturnCode, out MessageText);
                        this.XrefDataSet = dsPVItemXref;
                        this.DisposeAppServerObjects();
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }

            return MessageText;
        }

        #endregion

        private void Initialize(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsPartnerVuDataSet dsPV, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet ds = null;
           
            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                if ((searchCriteria == null || dsPV == null) && dm.GetCache("dsInventoryDataSet") != null)
                {
                    ds = (dsInventoryDataSet)dm.GetCache("dsInventoryDataSet");
                    this.Populate(ds);
                }
                else // try to fetch the record from the database
                {
                    this.CreateSubAppObject();

                    if (this._subAppObject != null)
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();
                        ds = new dsInventoryDataSet();

                        try
                        {
                            string ipcContextId = dm.GetCache("opcContextId") as string;

                            if (searchCriteria != null)
                            {
                                if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
                                    dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - discount from list")
                                {
                                    searchCriteria += "\x0003RetailMode=List";
                                }

                                if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
                                    dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - markup on cost")
                                {
                                    searchCriteria += "\x0003RetailMode=Net";
                                }

                                this._subAppObject.siinventoryfetchlist(ipcContextId, searchCriteria, dsPV, out ds, out ReturnCode, out MessageText);
                                this.Populate(ds);
                            }

                            DisposeAppServerObjects();
                        }
                        catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                        {
                            el.LogError(EventTypeFlag.AppServerDBerror, oe);
                            em.Message = oe.Message;
                            em.EventType = EventTypeFlag.AppServerDBerror;
                        }
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        /// <summary>
        /// Assign values to properties
        /// </summary>
        /// <param name="ds"></param>
        private void Populate(Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet ds)
        {
            this._referencedDataSet = ds;
            this._referencedHeaderTable = ds.ttinventory;
            this._referencedPriceTable = ds.ttPriceInfo;
            this._referencedAvailableTotalTable = ds.ttavail;
            this._referencedAvailableBranchTable = ds.ttavail_branch;
            this._referencedAvailableTallyHeaderTable = ds.ttavail_header;
            this._referencedAvailableTallyDetailTable = ds.ttavail_detail;
            this._referencedOnOrderTotalTable = ds.tton_order;
            this._referencedOnOrderBranchTable = ds.tton_order_branch;
            this._referencedOnOrderTranHeaderTable = ds.tton_order_header;
            this._referencedOnOrderTranDetailTable = ds.tton_order_detail;
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject()
        {
            Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // Get ASP Session setting first
            if (System.Web.HttpContext.Current != null)
            {
                user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                if (this._appServerConnection != null)
                    this._isConnectionFromOutside = true;

                this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                if (this._appObject != null)
                    this._isAppObjectFromOutside = true;

                this._subAppObject = (Dmsi.Agility.EntryNET.Inventory)dm.GetCache("InventorySubAppObject");
                if (this._subAppObject != null)
                    this._isSubAppObjectFromOutside = true;
            }

            // if objects are still null then create them
            if (this._appServerConnection == null)
            {
                int loginStatus = -1;
                string result = "";

                _connection = new Dmsi.Agility.Security.Connection();

                if (user == null)
                {
                    throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.\r\nNon-ASP implementation is not yet supported.");
                }

                if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                    loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);

                if (loginStatus == 0)
                {
                    this._appServerConnection = _connection.ProgressConnection;
                    this._appObject = _connection.ProgressApplicationObject;
                    _connection.Dispose();
                }
                else
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.Inventory: " + result + "(" + loginStatus.ToString() + ")");
                    em.EventType = EventTypeFlag.AppServerDBerror;
                    em.Message = "Dmsi.Agility.Data.Inventory: " + result + "(" + loginStatus.ToString() + ")";
                }
            }

            if (this._appObject == null)
                this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

            if (this._subAppObject == null)
                this._subAppObject = new Dmsi.Agility.EntryNET.Inventory(this._appObject);
        }

        #endregion
    }
}
