// Single customer object from a DataRow
// When specifying the searchCriteria for back-end fetch, make sure it's for only one customer.

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class AgilityCustomer : IDisposable
    {
        #region Members

        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.Customer _subAppObject;
        private System.Uri _uri;
        private string _userName;
        private string _password;

        #endregion

        #region Properties

        private DataTable _BranchShipTos;
        private DataTable _ShipTos;

        /// <summary>
        /// ASP application SHOULD cache this
        /// </summary>
        public DataTable BranchShipTos
        {
            get { return _BranchShipTos; }
        }

        /// <summary>
        /// ASP application SHOULD cache this
        /// </summary>
        public DataTable ShipTos
        {
            get { return _ShipTos; }
        }

        #endregion

        #region Commonly Used Fields

        private string _customerName;
        private string _customerCode;
        private decimal _customerObj;
        private string _customerKey;
        private string _customerSystemID;
        private string _city;
        private string _state;
        private string _zip;
        private string _address1;
        private string _address2;
        private string _address3;

        private DataRow _referencedRow;

        public decimal CustomerObj
        {
            get { return _customerObj; }
        }

        public string CustomerCode
        {
            get { return _customerCode; }
        }

        public string CustomerKey
        {
            get { return _customerKey; }
        }

        public string CustomerSystemID
        {
            get { return _customerSystemID; }
        }

        public string CustomerName
        {
            get { return _customerName; }
        }

        public DataRow ReferencedRow
        {
            get { return _referencedRow; }
        }

        public string City
        {
            get { return _city; }
        }

        public string State
        {
            get { return _state; }
        }

        public string Zip
        {
            get { return _zip; }
        }

        public string Address1
        {
            get { return _address1; }
        }

        public string Address2
        {
            get { return _address2; }
        }

        public string Address3
        {
            get { return _address3; }
        }

        #endregion

        #region Constructor and Dispose

        /// <summary>
        /// Get initial data from cache
        /// </summary>
        public AgilityCustomer(string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.Initialize("", null, true, sessionVariable, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get initial data from cache else database
        /// </summary>
        /// <param name="custObj">cust_obj</param>
        public AgilityCustomer(string custObj, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (custObj == null)
                throw (new Exception("Customer key can not be null."));
            else
                this.Initialize("", custObj, true, sessionVariable, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// This is use to fill the object from an existing row
        /// </summary>
        /// <param name="row"></param>
        public AgilityCustomer(DataRow row)
        {
            this.Populate(row);
        }

        /// <summary>
        /// Get initial data from cache and retrieve associated shipt-tos from cache else database
        /// </summary>
        /// <param name="retrieveShipTos">Retrieve ship-tos</param>
        public AgilityCustomer(bool retrieveShipTos, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.Initialize("", null, true, sessionVariable, out ReturnCode, out MessageText);

            if (this.ReferencedRow != null && retrieveShipTos)
                this.RetrieveShipTos(this.CustomerObj.ToString(), true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get initial data and retrieve associated shipt-tos from cache else database
        /// </summary>
        /// <param name="custObj">cust_obj</param>
        /// <param name="retrieveShipTos">Retrieve ship-tos</param>
        public AgilityCustomer(string custObj, bool retrieveShipTos, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.Initialize("", custObj, true, sessionVariable, out ReturnCode, out MessageText);

            if (this.ReferencedRow != null && retrieveShipTos)
                this.RetrieveShipTos(this.CustomerObj.ToString(), true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get initial data a given DataRow and retrieve associated shipt-tos from cache else database
        /// </summary>
        /// <param name="row"></param>
        /// <param name="retrieveShipTos"></param>
        public AgilityCustomer(DataRow row, bool retrieveShipTos, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.Populate(row);

            if (this.ReferencedRow != null && retrieveShipTos)
                this.RetrieveShipTos(this.CustomerObj.ToString(), true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Url">AppServer connection string</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="custObj">cust_obj</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        /// <param name="retrieveShipTos">Retrieve ship-tos</param>
        public AgilityCustomer(string Url, string userName, string password, string searchCriteria, bool checkCache, bool retrieveShipTos, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._uri = new System.Uri(Url);
            this.Initialize("", searchCriteria, checkCache, sessionVariable, out ReturnCode, out MessageText);

            if (this.ReferencedRow != null && retrieveShipTos)
                this.RetrieveShipTos(this.CustomerObj.ToString(), true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Uri">AppServer connetion Uri</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="custObj">cust_obj</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        /// <param name="retrieveShipTos">Retrieve ship-tos</param>
        public AgilityCustomer(System.Uri Uri, string userName, string password, string searchCriteria, bool checkCache, bool retrieveShipTos, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._uri = Uri;
            this.Initialize("", searchCriteria, checkCache, sessionVariable, out ReturnCode, out MessageText);

            if (this.ReferencedRow != null && retrieveShipTos)
                this.RetrieveShipTos(this.CustomerObj.ToString(), true, out ReturnCode, out MessageText);
        }

        public AgilityCustomer(string ipcContextId, System.Uri Uri, string userName, string password, string searchCriteria, bool checkCache, bool retrieveShipTos, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._uri = Uri;
            this.Initialize(ipcContextId, searchCriteria, checkCache, sessionVariable, out ReturnCode, out MessageText);

            if (this.ReferencedRow != null && retrieveShipTos)
                this.RetrieveShipTos(this.CustomerObj.ToString(), true, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource


                // Make sure the object refernece are created inside the class and not from cache
                DisposeAppServerObjects();

                // this will be cached outside of this context, so dispose is not really advisable

                if (System.Web.HttpContext.Current != null)
                {
                    this._ShipTos = null;
                    this._referencedRow = null;
                    this._BranchShipTos = null;
                }
                else
                {
                    if (this._ShipTos != null)
                        this._ShipTos.Dispose();

                    this._referencedRow = null;

                    if (this._BranchShipTos != null)
                        this._BranchShipTos.Dispose();
                }
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Check if the current cutomer instance belongs to the given branch
        /// </summary>
        /// <param name="branchID"></param>
        /// <returns></returns>
        public bool IsBranchSharedForCustomer(string branchID, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            // retrieve cached dsCustomerDataSet
            Dmsi.Agility.Data.Customers custList = new Customers(out ReturnCode, out MessageText);
            DataRow[] rows = null;

            if (this._referencedRow != null)
            {
                rows = ((dsCustomerDataSet)custList.ReferencedDataSet).ttbranch_cust.Select("cust_system_id='" + this._referencedRow["system_id"].ToString() + "' AND branch_id='" + branchID + "'");
            }

            custList.Dispose();

            bool customersAvailable = false;

            if (rows != null && rows.Length > 0)
                customersAvailable = true;

            return customersAvailable;
        }

        /// <summary>
        /// Must first call a constructor that also retrieves shipto list.
        /// </summary>
        /// <param name="sequence"></param>
        /// <returns></returns>
        public DataRow GetShiptoRow(int sequence)
        {
            DataRow row = null;

            if (this.BranchShipTos != null && this.BranchShipTos.Rows.Count > 0)
            {
                DataRow[] rows = this.BranchShipTos.Select("seq_num=" + sequence.ToString());
                if (rows.Length > 0)
                    row = rows[0];
            }

            return row;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get the data either from the cache or from the database
        /// </summary>
        /// <param name="searchCriteria">For ASP string cust_obj is enough else use the name-value pairs for back-end retrieval</param>
        /// <param name="checkCache">Check session cache first</param>
        private void Initialize(string ipcContextId, string searchCriteria, bool checkCache, string sessionVariable, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds = null;

            // check if in ASP mode 
            if (checkCache && System.Web.HttpContext.Current != null)
            {
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
                DataRow row;

                if (dm.GetCache(sessionVariable) != null)
                {
                    row = (DataRow)dm.GetCache(sessionVariable);

                    if (row != null &&
                        ((searchCriteria != null && row["cust_obj"].ToString().Equals(searchCriteria, StringComparison.OrdinalIgnoreCase)) || searchCriteria == null))
                    {
                        this.Populate(row);
                    }
                }
                else if (searchCriteria != null) // try to fetch the record from the database
                {
                    // re-format searCriteria for back-end call,
                    // most of the time the original value of the searchCriteria should be cust_obj
                    string custObj = searchCriteria;
                    searchCriteria = "Tables=cust";
                    searchCriteria += "\x0003Branch=<ALL>";
                    searchCriteria += "\x0003CustQueryField=cust_obj";
                    searchCriteria += "\x0003CustCompareMode=EQ";
                    searchCriteria += "\x0003CustCompareValue=" + custObj;

                    ds = new dsCustomerDataSet();
                    this.FetchCustomers(ipcContextId, searchCriteria, ds, checkCache, out ReturnCode, out MessageText);
                }
            }
            else
            {
                ds = new dsCustomerDataSet();
                this.FetchCustomers(ipcContextId, searchCriteria, ds, checkCache, out ReturnCode, out MessageText);
            }
        }

        private void FetchCustomers(string ipcContextId, string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.CreateSubAppObject(checkCache);

            if (this._subAppObject != null)
            {
                Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                try
                {
                    this._subAppObject.sicustomerfetchlistsf(ipcContextId, searchCriteria, out ds, out ReturnCode, out MessageText);

                    if (ds.ttCustomer.Rows.Count > 0)
                        this.Populate(ds.ttCustomer.Rows[0]);
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    el.LogError(EventTypeFlag.AppServerDBerror, oe);
                    em.Message = oe.Message;
                    em.EventType = EventTypeFlag.AppServerDBerror;
                }
                finally
                {
                    DisposeAppServerObjects();
                }
            }
        }

        /// <summary>
        /// This is the assignment of all commonly used fields
        /// </summary>
        /// <param name="row"></param>
        private void Populate(DataRow row)
        {
            if (row == null)
                throw (new Exception("DataRow can not be null."));
            else
            {
                _customerObj = Convert.ToDecimal(row["cust_obj"].ToString());
                _customerName = row["cust_name"].ToString();
                _customerCode = row["cust_code"].ToString();
                _customerKey = row["cust_key"].ToString();
                _customerSystemID = row["system_id"].ToString();
                _city = row["city"].ToString();
                _state = row["state"].ToString();
                _zip = row["zip"].ToString();
                _address1 = row["address_1"].ToString();
                _address2 = row["address_2"].ToString();
                _address3 = row["address_3"].ToString();
                _referencedRow = row;
            }
        }

        private void RetrieveShipTos(string custObj, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            bool isShiptoPresent = false;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            Dmsi.Agility.EntryNET.EventLog el = new EventLog();
            Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();
            Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet dsShipto = new dsCustomerDataSet();

            // fetch shipto's
            if (!isShiptoPresent)
            {
                this.CreateSubAppObject(checkCache);

                if (this._subAppObject != null)
                {
                    try
                    {
                        string ipcContextId = dm.GetCache("opcContextId") as string;
                        string branchinput = "";
                        
                        if (dm.GetCache("newBranchIDforShiptoFetch") != null)
                        {
                            branchinput = (string)dm.GetCache("newBranchIDforShiptoFetch");
                        }
                        else
                        {
                            branchinput = dm.GetCache("currentBranchID") as string;
                        }

                        // This si method needs at least the customer and the branch you are interested in
                        dsShipto.ttCustomer.ImportRow(this.ReferencedRow);
                        dsShipto.ttbranch_cust.ImportRow(new Branch().ReferencedRow);
                        this._subAppObject.sicustomerfetchshiptolist(ipcContextId, "Cust=" + custObj + "\x0003Branch_id=" + branchinput, ref dsShipto, out ReturnCode, out MessageText);
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                    finally
                    {
                        this._ShipTos = dsShipto.ttShipto; // All ship-tos for this customer

                        dm.SetCache("AllCurrentCustomerShiptos", _ShipTos);

                        // build branch specific ship-tos using the ttShipto table as template
                        this._BranchShipTos = dsShipto.ttShipto.Clone();

                        for (int i = 0; i < dsShipto.ttShipto.Rows.Count; i++)
                        {
                            DataRow[] rows = dsShipto.ttCustShiptoBranch.Select("cust_shipto_obj=" + dsShipto.ttShipto.Rows[i]["cust_shipto_obj"].ToString());

                            if (rows.Length > 0)
                            {
                                dsShipto.ttShipto.Rows[i]["nonsalable"] = rows[0]["nonsalable"];
                                this._BranchShipTos.ImportRow(dsShipto.ttShipto.Rows[i]);
                            }
                        }

                        DisposeAppServerObjects();
                    }
                }
            }
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject(bool checkCache)
        {
            // Check if the object is already created
            if (this._subAppObject == null)
            {
                Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

                // Get ASP Session setting first
                if (checkCache && System.Web.HttpContext.Current != null)
                {
                    user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                    this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                    if (this._appServerConnection != null)
                        this._isConnectionFromOutside = true;

                    this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                    if (this._appObject != null)
                        this._isAppObjectFromOutside = true;

                    this._subAppObject = (Dmsi.Agility.EntryNET.Customer)dm.GetCache("CustomerSubAppObject");
                    if (this._subAppObject != null)
                        this._isSubAppObjectFromOutside = true;
                }

                // if objects are still null then create them
                if (this._appServerConnection == null)
                {
                    _connection = new Dmsi.Agility.Security.Connection();

                    int loginStatus = -1;
                    string result = "";

                    if (user == null && this._uri != null &&
                        this._userName.Length != 0 && this._password.Length != 0)
                    {
                        if (_connection.Connect(this._uri, this._userName, this._password, out result))
                            loginStatus = _connection.LogIn(this._userName, this._password, out result);
                    }
                    else
                    {
                        if (user == null)
                        {
                            throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.");
                        }

                        if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                            loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);
                    }

                    if (loginStatus == 0)
                    {
                        this._appServerConnection = _connection.ProgressConnection;
                        this._appObject = _connection.ProgressApplicationObject;
                    }
                    else
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                        el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")");
                        em.EventType = EventTypeFlag.AppServerDBerror;
                        em.Message = "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")";
                    }
                }

                if (this._appObject == null)
                    this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

                if (this._subAppObject == null)
                    this._subAppObject = new Dmsi.Agility.EntryNET.Customer(this._appObject);
            }
        }
        #endregion
    }
}
