﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public class AccountsReceivable: IDisposable
    {
        #region Members
        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.AcctsRcv _subAppObject;
        #endregion

        #region Properties

        private DataTable _referencedAgingTable;
        private DataTable _referencedARTable;
        private DataSet _referencedDataSet;

        private Dmsi.Agility.EntryNET.StrongTypesNS.dsCustBankDataSet _referencedCustBankDataSet;

        private bool _showPay;
        private bool _showACH;

        /// <summary>
        /// Points to ttinvoice_header
        /// </summary>
        public DataTable ReferencedAgingTable
        {
            get { return this._referencedAgingTable; }
        }

        /// <summary>
        /// Points to ttinvoice_detail
        /// </summary>
        public DataTable ReferencedARTable
        {
            get { return this._referencedARTable; }
        }

        public DataSet ReferencedDataSet
        {
            get { return this._referencedDataSet; }
        }

        public DataSet ReferencedCustBankDataSet
        {
            get { return this._referencedCustBankDataSet; }
        }

        public bool ShowPay
        {
            get { return this._showPay; }
        }

        public bool ShowACH
        {
            get { return this._showACH; }
        }

        #endregion

        #region Contructors and Dispose
        /// <summary>
        /// Get the quote dateset from cache
        /// </summary>
        public AccountsReceivable(out int ReturnCode, out string MessageText)
        {
            this.Initialize(null, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param>
        /// <param name="customer">Current customer, shiptos of the customer must be populated</param> 
        public AccountsReceivable(string searchCriteria, out int ReturnCode, out string MessageText)
        {
            this.Initialize(searchCriteria, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();

                if (System.Web.HttpContext.Current != null)
                {
                    _referencedAgingTable = null;
                    _referencedARTable = null;
                    _referencedDataSet = null;
                }
                else
                {
                    this._referencedAgingTable.Dispose();
                    this._referencedARTable.Dispose();
                    this._referencedDataSet.Dispose();
                }
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }

        #endregion

        #region Methods

        public bool ProcessPayment(string custCode, 
                                   int shiptoSeq, 
                                   string guid, 
                                   string CCNumber, 
                                   string approvalNumber, 
                                   string transactionType, 
                                   string transactionID, 
                                   decimal transactionAmount, 
                                   string mode, 
                                   string branch, 
                                   string transactionXML, 
                                   string pvURL,  
                                   dspvarDataSet ds, 
                                   dsCustBankDataSet dsCustBank, 
                                   decimal ccSurchargeAmount,
                                   string ccSurchargeBasisType,
                                   decimal ccSurchargeBasisAmount,
                                   out int ReturnCode, 
                                   out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            //transactionAmount contains the cc surcharge amount
            //subtract it out so it gets reconciled properly on the backend
            transactionAmount -= ccSurchargeAmount;

            bool success = false;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    try
                    {
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        this._subAppObject.siapplypayments(ipcContextId,
                                                           mode,
                                                           branch,
                                                           custCode,
                                                           shiptoSeq,
                                                           guid,
                                                           CCNumber,
                                                           approvalNumber,
                                                           transactionType,
                                                           transactionID,
                                                           transactionAmount,
                                                           ccSurchargeAmount,
                                                           ccSurchargeBasisType,
                                                           ccSurchargeBasisAmount,
                                                           transactionXML,
                                                           pvURL,
                                                           ds,
                                                           dsCustBank,
                                                           out ReturnCode,
                                                           out MessageText);

                        DisposeAppServerObjects();

                        if (ReturnCode != 0)
                        {
                            el.LogError(EventTypeFlag.ApplicationEvent, "Error was returned from .siapplypayments. Error message text: " + MessageText +
                                "; Input parameters were: mode=" + mode + 
                                "; branch=" + branch + 
                                "; custCode=" + custCode + 
                                "; shiptoSeq=" + shiptoSeq.ToString() + 
                                "; guid=" + guid + 
                                "; transActionID=" + transactionID + 
                                "; transactionAmount=" + transactionAmount.ToString() +
                                "; ccSurchargeAmount=" + ccSurchargeAmount.ToString() +
                                "; ccSurchargeBasisType=" + ccSurchargeBasisType.ToString() +
                                "; ccSurchargeBasisAmount=" + ccSurchargeBasisAmount.ToString() +
                                ".");
                        }
                        else
                            success = true;
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                    catch (System.Exception ex)
                    {
                        el.LogError(EventTypeFlag.UnhandledException, ex);
                        em.Message = ex.Message;
                        em.EventType = EventTypeFlag.UnhandledException;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }

            return success;
        }

        private void Initialize(string searchCriteria, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            Dmsi.Agility.EntryNET.StrongTypesNS.dspvarDataSet ds = null;
            Dmsi.Agility.EntryNET.StrongTypesNS.dsCustBankDataSet dsCustBank;

            // check if in ASP mode 
            if (System.Web.HttpContext.Current != null)
            {
                this.CreateSubAppObject();

                if (this._subAppObject != null)
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();
                    ds = new dspvarDataSet();

                    _showPay = false;

                    try
                    {
                        string ipcContextId = dm.GetCache("opcContextId") as string;

                        if (searchCriteria != null)
                        {
                            this._subAppObject.sifetcharbalance(ipcContextId, searchCriteria, out _showPay, out _showACH, out ds, out dsCustBank, out ReturnCode, out MessageText);
                            this.Populate(ds, dsCustBank);
                            DisposeAppServerObjects();

                            if (ReturnCode != 0)
                            {
                                el.LogError(EventTypeFlag.ApplicationEvent, MessageText);
                            }
                        }
                    }
                    catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                    {
                        el.LogError(EventTypeFlag.AppServerDBerror, oe);
                        em.Message = oe.Message;
                        em.EventType = EventTypeFlag.AppServerDBerror;
                    }
                }
            }
            else
            {
                throw (new Exception("Non-ASP implementation is not yet supported."));
            }
        }

        /// <summary>
        /// Assign values to properties
        /// </summary>
        /// <param name="ds"></param>
        private void Populate(Dmsi.Agility.EntryNET.StrongTypesNS.dspvarDataSet ds, Dmsi.Agility.EntryNET.StrongTypesNS.dsCustBankDataSet dsCustBank)
        {
            this._referencedDataSet = ds;
            this._referencedAgingTable = ds.ttaginginfo;
            this._referencedARTable = ds.ttarbalance;

            _referencedCustBankDataSet = dsCustBank;
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject()
        {
            Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            // Get ASP Session setting first
            if (System.Web.HttpContext.Current != null)
            {
                user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                if (this._appServerConnection != null)
                    this._isConnectionFromOutside = true;

                this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                if (this._appObject != null)
                    this._isAppObjectFromOutside = true;

                this._subAppObject = (Dmsi.Agility.EntryNET.AcctsRcv)dm.GetCache("InvoicesubAppObject");
                if (this._subAppObject != null)
                    this._isSubAppObjectFromOutside = true;
            }

            // if objects are still null then create them
            if (this._appServerConnection == null)
            {
                int loginStatus = -1;
                string result = "";

                _connection = new Dmsi.Agility.Security.Connection();

                if (user == null)
                {
                    throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.\r\nNon-ASP implementation is not yet supported.");
                }

                if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                    loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);

                if (loginStatus == 0)
                {
                    this._appServerConnection = _connection.ProgressConnection;
                    this._appObject = _connection.ProgressApplicationObject;
                    _connection.Dispose();
                }
                else
                {
                    Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                    Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                    el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")");
                    em.EventType = EventTypeFlag.AppServerDBerror;
                    em.Message = "Dmsi.Agility.Data.AgilityCustomer: " + result + "(" + loginStatus.ToString() + ")";
                }
            }

            if (this._appObject == null)
                this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

            if (this._subAppObject == null)
                this._subAppObject = new Dmsi.Agility.EntryNET.AcctsRcv(this._appObject);
        }

        #endregion
    }
}
