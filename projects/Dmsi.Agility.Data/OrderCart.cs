using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;
using System.Web.Security;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Security;

namespace Dmsi.Agility.Data
{
    public struct Violation
    {
        public string Item;
        public decimal Multiple;
        public decimal CurrentValue;
        public decimal Suggested;
    }

    public class OrderCart: IDisposable
    {
        public delegate void AddToCartDelegate();
        public event AddToCartDelegate OnAddToCart;

        #region Members
        private Collection<Violation> _violations;
        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private System.Uri _Uri;
        private string _userName; // This is the currently logon user
        private string _password;
        #endregion

        #region Properties
        private Dmsi.Agility.EntryNET.SalesOrders _subAppObject;
        private string _defaultSaleType;
        private string _saleTypeDescription;
        private string _pricingSaleType;
        private string _branchID;
        private string _userID; // This is the user ID of the desired order cart
        private DataSet _referencedDataSet;
        private decimal _CartTotal;
        private decimal _MarkupFactor;
        private decimal _RetailCartTotal;

        public Collection<Violation> Violations
        {
            get { return _violations; }
        }

        public decimal MarkupFactor
        {
            get { return _MarkupFactor; }
            set { _MarkupFactor = value; }
        }
        
        public decimal CartTotal
        {
            get { return _CartTotal; }
            set { _CartTotal = value; }
        }

        public decimal RetailCartTotal
        {
            get { return _RetailCartTotal; }
            set { _RetailCartTotal = value; }
        }

        public DataSet ReferencedDataSet
        {
            get { return _referencedDataSet; }
        }

        public string DefaultSaleType
        {
            get { return _defaultSaleType; }
        }

        public string SaleTypeDescription
        {
            get { return _saleTypeDescription; }
        }

        public string PricingSaleType
        {
            get { return _pricingSaleType; }
            set { _pricingSaleType = value; }
        }

        public string BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        public Dmsi.Agility.EntryNET.SalesOrders SubAppObject
        {
            get { return _subAppObject; }
            set { _subAppObject = value; }
        }

        #endregion

        #region Contructors and Dispose
        /// <summary>
        /// Get the Customer dateset from cache. In ASP implementation, cache the object.
        /// </summary>
        public OrderCart(out int ReturnCode, out string MessageText)
        {
            this.Initialize(null, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="searchCriteria">Name-value pair</param>
        /// <param name="customer">Current customer, shiptos of the customer must be populated</param> 
        public OrderCart(string searchCriteria, out int ReturnCode, out string MessageText)
        {
            this._branchID = this.ParseSearchCriteria(searchCriteria, "BranchID");
            this._userID = this.ParseSearchCriteria(searchCriteria, "UserID");
            this._pricingSaleType = this.ParseSearchCriteria(searchCriteria, "SaleType");
            this.MarkupFactor = Convert.ToDecimal(this.ParseSearchCriteria(searchCriteria, "MarkupFactor"));

            this.Initialize(searchCriteria, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get dataset from cache else fetch from the back-end
        /// </summary>
        /// <param name="branchID"></param>
        /// <param name="userID"></param>
        /// <param name="shipToObj"></param>
        public OrderCart(string branchID, string userID, string shipToObj, string saleType, decimal markupFactorValue, out int ReturnCode, out string MessageText)
        {
            string searchCriteria = "";

            searchCriteria = "Branch=" + branchID; // Branch=<ALL> to get the full list
            searchCriteria += "\x0003UserID=" + userID;
            searchCriteria += "\x0003ShipToObj=" + shipToObj;
            searchCriteria += "\x0003SaleType=" + saleType;
            searchCriteria += "\x0003MarkupFactor=" + markupFactorValue.ToString();

            this._branchID = branchID;
            this._userID = userID;
            this._pricingSaleType = saleType;

            this.Initialize(searchCriteria, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Url">AppServer connection string</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="searchCriteria">Name-value pair for customer list fetch</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public OrderCart(string Url, string userName, string password, string searchCriteria, string saleType, bool checkCache, out int ReturnCode, out string MessageText)
        {

            this._userName = userName;
            this._password = password;
            this._pricingSaleType = saleType;
            this._Uri = new System.Uri(Url);

            this._branchID = this.ParseSearchCriteria(searchCriteria, "BranchID");
            this._userID = this.ParseSearchCriteria(searchCriteria, "UserID");
            this.MarkupFactor = Convert.ToDecimal(this.ParseSearchCriteria(searchCriteria, "MarkupFactor"));

            this.Initialize(searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Uri">AppServer connetion Uri</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="searchCriteria">Name-value pair for customer list fetch</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public OrderCart(System.Uri Uri, string userName, string password, string searchCriteria, string saleType, bool checkCache, out int ReturnCode, out string MessageText)
        {
            this._userName = userName;
            this._password = password;
            this._pricingSaleType = saleType;
            this._Uri = Uri;

            this._branchID = this.ParseSearchCriteria(searchCriteria, "BranchID");
            this._userID = this.ParseSearchCriteria(searchCriteria, "UserID");
            this.MarkupFactor = Convert.ToDecimal(this.ParseSearchCriteria(searchCriteria, "MarkupFactor"));

            this.Initialize(searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource

                // Make sure the object references are created inside the class and not from cache
                DisposeAppServerObjects();

                if (System.Web.HttpContext.Current != null)
                {
                    _referencedDataSet = null;
                }
                else
                {
                    this._referencedDataSet.Dispose();
                }
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }
        #endregion

        #region Public Methods

        public void CheckQuantity()
        {
            bool minimumQtyViolation = false;

            if (_violations != null)
                _violations.Clear();
            else
                _violations = new Collection<Violation>();

            if (_referencedDataSet != null)
            {
                foreach(DataRow row in _referencedDataSet.Tables["ttorder_cart"].Rows)
                {
                    if (row["min_pak_disp"].ToString().Length > 0)
                    {
                        if ((decimal)row["min_pak_disp"] > 0)
                        {
                            if (Convert.ToDecimal(row["min_pak_disp"]) > 0 && Convert.ToDecimal(row["min_pak_disp"]) < 1)
                            {
                                if (!row["qty"].ToString().Contains("."))
                                {
                                    minimumQtyViolation = false;
                                }
                                else
                                {
                                    decimal decimalPortion = Convert.ToDecimal(row["qty"].ToString().Substring(row["qty"].ToString().IndexOf(".")));
                                    minimumQtyViolation = decimalPortion % Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(row["min_pak_disp"]), 0), 4) > 0;
                                }
                            }
                            else
                            {
                                if (!row["qty"].ToString().Contains("."))
                                {
                                    minimumQtyViolation = Convert.ToDecimal(row["qty"]) % Convert.ToDecimal(row["min_pak_disp"]) > 0;
                                }
                                else
                                {
                                    if (Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(row["qty"]), 0) == 0)
                                        minimumQtyViolation = Convert.ToDecimal(row["qty"]) % Convert.ToDecimal(row["min_pak_disp"]) > 0;
                                    else
                                        minimumQtyViolation = Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(row["qty"]), 0), 4) % Decimal.Round(Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(row["min_pak_disp"]), 0), 4) > 0;
                                }
                            }

                            decimal minpack = Math.Round((decimal)row["min_pak_disp"], 4, MidpointRounding.AwayFromZero);
                            Int64 whole = (Int64)Math.Floor((decimal)row["qty"] / minpack);
                            decimal remainder = (decimal)row["qty"] - (whole * minpack);

                            if (minimumQtyViolation)
                            {
                                Violation temp;
                                if ((string)row["SIZE"] != "")
                                    temp.Item = (string)row["ITEM"] + " - " + (string)row["SIZE"];
                                else
                                    temp.Item = (string)row["ITEM"];

                                temp.Multiple = minpack;
                                temp.CurrentValue = (decimal)row["qty"];

                                decimal nearest = 0;

                                if ((double)remainder / (double)temp.Multiple >= .5)
                                    nearest = (whole + 1) * temp.Multiple;
                                else
                                    nearest = whole * temp.Multiple;

                                if (nearest == 0)
                                    nearest = temp.Multiple;

                                temp.Suggested = (decimal)nearest;

                                _violations.Add(temp);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Save the current contents of the Order Cart to the database. This will not create the Sales Order.
        /// </summary>
        public void SaveCart(decimal shiptoObj, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (this._branchID.Length == 0)
                throw (new Exception("Branch ID must be supplied."));

            if (this._userID.Length == 0)
                throw (new Exception("User ID must be supplied."));

            if (_referencedDataSet != null)
            {
                this.CreateSubAppObject(true);

                dsOrderCartDataSet cart = (dsOrderCartDataSet)_referencedDataSet;

                cart.AcceptChanges();

                // use current shipto if not supplied
                if (shiptoObj == 0)
                {
                    Dmsi.Agility.Data.Shipto shipto = new Shipto(out ReturnCode, out MessageText);
                    shiptoObj = shipto.ShiptoObj;
                    shipto.Dispose();
                }

                DataManager dm = new DataManager();
                string ipcContextId = dm.GetCache("opcContextId") as string;

                this._subAppObject.sisaveordercart(ipcContextId, this._branchID, shiptoObj, this._userID, "", 0, cart, out ReturnCode, out MessageText);
                CalculateTotal();
                DisposeAppServerObjects();
            }
        }

        private void CalculateTotal()
        {
            this.CartTotal = 0;
            this.RetailCartTotal = 0;

            foreach (DataRow row in _referencedDataSet.Tables["ttorder_cart"].Rows)
            {
                if (row["extension"] != DBNull.Value)
                    this.CartTotal += Math.Round(Convert.ToDecimal(row["extension"]), 2);

                if (row["retail_extension"] != DBNull.Value)
                    this.RetailCartTotal += Math.Round(Convert.ToDecimal(row["retail_extension"]), 2);
            }
        }

        public void UpdateCart(string item, decimal thickness, decimal width, decimal length, int sequence, string newQty, string newMessage, string newLocationReference, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (_referencedDataSet != null)
            {
                dsOrderCartDataSet ds = (dsOrderCartDataSet)_referencedDataSet;

                // check if the item is already in the cart
                dsOrderCartDataSet.ttorder_cartRow[] rows = (dsOrderCartDataSet.ttorder_cartRow[])ds.ttorder_cart.Select("ITEM='" + item.Replace("'", "''") + "' AND thickness=" + thickness.ToString() + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString() + " AND sequence=" + sequence.ToString());
                if (rows.Length > 0)
                {
                    rows[0].qty = Convert.ToDecimal(newQty); // save the qty passed in
                    rows[0].extension = Math.Round(rows[0].qty * rows[0].price, 2);
                    rows[0].line_message = newMessage; // the line_messagae passed in
                    rows[0].location_reference = newLocationReference; // the location_reference passed in
                }

                this.SaveCart(0, out ReturnCode, out MessageText); 
            }
        }

        public void AddToCart(dsOrderCartDataSet.ttorder_cartRow row, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (_referencedDataSet != null)
            {
                dsOrderCartDataSet ds = (dsOrderCartDataSet)_referencedDataSet;

                // check if the item is already in the cart
                //dsOrderCartDataSet.ttorder_cartRow[] rows = (dsOrderCartDataSet.ttorder_cartRow[])ds.ttorder_cart.Select("ITEM='" + row.ITEM.Replace("'","''") + "' AND thickness=" + row.thickness.ToString() + " AND WIDTH=" + row.WIDTH.ToString() + " AND LENGTH=" + row.LENGTH.ToString());

                // Get the highest sequence number and add 1.
                // The Sort/Save process will remove the holes. This is just for uniqueness.
                int sequence = 0;

                foreach (dsOrderCartDataSet.ttorder_cartRow line in ds.ttorder_cart.Rows)
                {
                    if (sequence < (int)line["sequence"])
                        sequence = (int)line["sequence"];
                }

                sequence++;

                row.sequence = sequence;
                ds.ttorder_cart.LoadDataRow(row.ItemArray, true);
                
                this.SaveCart(0, out ReturnCode, out MessageText);

                if (this.OnAddToCart != null)
                    this.OnAddToCart();
            }
        }

        public void DeleteCartEntry(string item, decimal thickness, decimal width, decimal length, int sequence, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (_referencedDataSet != null)
            {
                dsOrderCartDataSet ds = (dsOrderCartDataSet)_referencedDataSet;

                // check if the item is already in the cart
                dsOrderCartDataSet.ttorder_cartRow[] rows = (dsOrderCartDataSet.ttorder_cartRow[])ds.ttorder_cart.Select("ITEM='" + item.Replace("'", "''") + "' AND thickness=" + thickness.ToString() + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString() + " AND sequence=" + sequence.ToString());
                if (rows.Length > 0)
                {
                    rows[0].Delete(); // delete the row
                }

                this.SaveCart(0, out ReturnCode, out MessageText);
            }
        }

        public void DeleteAllItems()
        {
            if (_referencedDataSet != null)
            {
                dsOrderCartDataSet ds = (dsOrderCartDataSet)_referencedDataSet;

                ds.ttorder_cart.Rows.Clear();

                int ReturnCode;
                string MessageText;

                this.SaveCart(0, out ReturnCode, out MessageText);
                    
                
            }
        }

        #endregion

        #region Private Methods

        private string ParseSearchCriteria(string searchCriteria, string key)
        {
            string value = "";

            if(searchCriteria.Length > 0)
            {
                string[] pairs = searchCriteria.Split(new char[] { '\x0003' });

                if (pairs.Length > 0)
                {
                    for (int i = 0; i < pairs.Length; i++)
                    {
                        string[] nameValues = pairs[i].Split(new char[] { '=' });

                        if (nameValues.Length == 2 && nameValues[0] == key)
                            value = nameValues[1];
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Get the data either from the cache or from the database
        /// </summary>
        /// <param name="searchCriteria">Name-value pairs for back-end retrieval</param>
        /// <param name="checkCache">Check session cache first</param>
        private void Initialize(string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.StrongTypesNS.dsOrderCartDataSet ds = null;

            // check if in ASP mode 
            if (checkCache && System.Web.HttpContext.Current != null)
            {
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

                if (dm.GetCache("OrderCart") != null)
                {
                    ds = (dsOrderCartDataSet)((OrderCart)dm.GetCache("OrderCart")).ReferencedDataSet;

                    if (ds != null)
                    {
                        this._branchID = ((OrderCart)dm.GetCache("OrderCart")).BranchID;
                        this._userID = ((OrderCart)dm.GetCache("OrderCart")).UserID;
                        this._pricingSaleType = ((OrderCart)dm.GetCache("OrderCart")).PricingSaleType;
                        this._subAppObject = ((OrderCart)dm.GetCache("OrderCart")).SubAppObject;

                        this.Populate(ds);
                    }
                }
                else if (searchCriteria != null) // try to fetch the record from the database
                    this.FetchOrderCart(searchCriteria, ds, checkCache, out ReturnCode, out MessageText);
            }
            else
                this.FetchOrderCart(searchCriteria, ds, checkCache, out ReturnCode, out MessageText);
        }

        private void FetchOrderCart(string searchCriteria, Dmsi.Agility.EntryNET.StrongTypesNS.dsOrderCartDataSet ds, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.CreateSubAppObject(checkCache);

            if (this._subAppObject != null)
            {
                Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                try
                {
                    DataManager dm = new DataManager();
                    string ipcContextId = dm.GetCache("opcContextId") as string;

                    //Add RecordType="" and ControlNo=0 to SearchCriteria...
                    searchCriteria += "\x0003ControlNo=0\x0003RecordType=";

                    if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
                        dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - discount from list")
                    {
                        searchCriteria += "\x0003RetailMode=List";
                    }

                    if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
                        dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - markup on cost")
                    {
                        searchCriteria += "\x0003RetailMode=Net";
                    }

                    this._subAppObject.sifetchordercart(ipcContextId, searchCriteria, out ds, out ReturnCode, out MessageText);

                    if (ds != null)
                        this.Populate(ds);

                    DisposeAppServerObjects();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    el.LogError(EventTypeFlag.AppServerDBerror, oe);
                    em.Message = oe.Message;
                    em.EventType = EventTypeFlag.AppServerDBerror;
                }
            }
        }

        /// <summary>
        /// This is the assignment of all commonly used fields
        /// </summary>
        /// <param name="row"></param>
        private void Populate(DataSet ds)
        {
            if (ds == null)
                throw (new Exception("DataSet can not be null."));
            else
            {
                _referencedDataSet = ds;
                this.CalculateTotal();

                // This should only be one record per fetch, unless the fetch is changed
                // then cust_key and cust_key_sysid has to be added to the Select statement
                if (ds.Tables["ttsales_type"].Rows.Count > 0)
                {
                    _defaultSaleType = ds.Tables["ttcust_default"].Rows[0]["sale_type"].ToString();

                    DataRow[] rows = ds.Tables["ttsales_type"].Select("sale_type='" + _defaultSaleType + "'");

                    if (rows.Length > 0)
                        _saleTypeDescription = rows[0]["description"].ToString();
                    else
                        _saleTypeDescription = _defaultSaleType;

                    if (_defaultSaleType == "") 
                    {
                        _defaultSaleType = "<All>";
                        _saleTypeDescription = "All Sale Types";
                    }
                }
                else
                {
                    _defaultSaleType = "<All>";
                    _saleTypeDescription = "All Sale Types";
                }
            }
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject(bool checkCache)
        {
            // Check if the object is already created
            if (this._subAppObject == null)
            {
                Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            
                // Get ASP Session setting first
                if (checkCache && System.Web.HttpContext.Current != null)
                {
                    user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                    // Check if the Progress objects are coming from cache data.
                    // We can't dispose them during the Dispose call.
                    this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                    if (this._appServerConnection != null)
                        this._isConnectionFromOutside = true;

                    this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                    if (this._appObject != null)
                        this._isAppObjectFromOutside = true;

                    this._subAppObject = (Dmsi.Agility.EntryNET.SalesOrders)dm.GetCache("OrderCartSubAppObject");
                    if (this._subAppObject != null)
                        this._isSubAppObjectFromOutside = true;
                }

                // if objects are still null then create them
                if (this._appServerConnection == null)
                {
                    _connection = new Dmsi.Agility.Security.Connection();

                    int loginStatus = -1;
                    string result = "";

                    if (user == null && this._Uri != null &&
                        this._userName.Length != 0 && this._password.Length != 0)
                    {
                        if (_connection.Connect(this._Uri, this._userName, this._password, out result))
                            loginStatus = _connection.LogIn(this._userName, this._password, out result);
                    }
                    else
                    {
                        if (user == null)
                        {
                            throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.");
                        }

                        if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                            loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);
                    }

                    if (loginStatus == 0)
                    {
                        this._appServerConnection = _connection.ProgressConnection;
                        this._appObject = _connection.ProgressApplicationObject;
                    }
                    else
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                        el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.OrderCart: " + result + "(" + loginStatus.ToString() + ")");
                        em.EventType = EventTypeFlag.AppServerDBerror;
                        em.Message = "Dmsi.Agility.Data.OrderCart: " + result + "(" + loginStatus.ToString() + ")";
                    }
                }

                if (this._appObject == null)
                    this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

                if (this._subAppObject == null)
                    this._subAppObject = new Dmsi.Agility.EntryNET.SalesOrders(this._appObject);
            }
        }
#endregion
    }
}
