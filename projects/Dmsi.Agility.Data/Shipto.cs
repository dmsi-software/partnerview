// Single customer object from a DataRow
// When specifying the searchCriteria for back-end fetch, make sure it's for only one customer.

using System;
using System.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

namespace Dmsi.Agility.Data
{
    public class Shipto : IDisposable
    {
        #region Members
        Dmsi.Agility.Security.Connection _connection = null;
        private bool _isConnectionFromOutside;
        private Progress.Open4GL.Proxy.Connection _appServerConnection;
        private bool _isAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.EntryNET _appObject;
        private bool _isSubAppObjectFromOutside;
        private Dmsi.Agility.EntryNET.Customer _subAppObject;
        private System.Uri _uri;
        private string _userName;
        private string _password;
        #endregion

        #region Properties

        #endregion

        #region Commonly Used Fields

        private string _ShiptoName;
        private string _ShiptoSystemID;
        private int _ShiptoSequence;
        private decimal _ShiptoObj;
        private string _CustomerKey;
        private string _CustomerSystemID;
        private decimal _CustomerObj;
        private string _City;
        private string _State;
        private string _Zip;
        private string _Address1;
        private string _Address2;
        private string _Address3;
        private bool _Nonsalable;

        private DataRow _referencedRow;

        public decimal ShiptoObj
        {
            get { return _ShiptoObj; }
        }

        public string ShiptoSystemID
        {
            get { return _ShiptoSystemID; }
        }

        public int ShiptoSequence
        {
            get { return _ShiptoSequence; }
        }

        public string CustomerKey
        {
            get { return _CustomerKey; }
        }

        public string CustomerSystemID
        {
            get { return _CustomerSystemID; }
        }

        public decimal CustomerObj
        {
            get { return _CustomerObj; }
        }

        public string ShiptoName
        {
            get { return _ShiptoName; }
        }

        public DataRow ReferencedRow
        {
            get { return _referencedRow; }
        }

        public string City
        {
            get { return _City; }
        }

        public string State
        {
            get { return _State; }
        }

        public string Zip
        {
            get { return _Zip; }
        }

        public string Address1
        {
            get { return _Address1; }
        }

        public string Address2
        {
            get { return _Address2; }
        }

        public string Address3
        {
            get { return _Address3; }
        }

        public bool Nonsalable
        {
            get { return _Nonsalable; }
        }

        #endregion

        #region Constructor and Dispose

        /// <summary>
        /// Get initial data from cache
        /// </summary>
        public Shipto(out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.Initialize(null, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Get initial data from cache else database
        /// </summary>
        /// <param name="shiptoObj">cust_shipto_obj</param>
        public Shipto(string shiptoObj, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (shiptoObj == null)
                throw (new Exception("Ship-to object can not be null."));
            else
                this.Initialize(shiptoObj, true, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// This is use to fill the object from an existing row
        /// </summary>
        /// <param name="row"></param>
        public Shipto(DataRow row)
        {
            this.Populate(row);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Url">AppServer connection string</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="searchCriteria">Name value pair</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public Shipto(string Url, string userName, string password, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._uri = new System.Uri(Url);
            this.Initialize(searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        /// <summary>
        /// Use this constructor if you expect a full Progress Proxy instantiation
        /// </summary>
        /// <param name="Uri">AppServer connetion Uri</param>
        /// <param name="userName">Login Name</param>
        /// <param name="password">Password</param>
        /// <param name="searchCriteria">Name-value pair</param>
        /// <param name="checkCache">Set this to false if the object should use its own Progress proxy related objects</param>
        public Shipto(System.Uri Uri, string userName, string password, string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this._userName = userName;
            this._password = password;
            this._uri = Uri;
            this.Initialize(searchCriteria, checkCache, out ReturnCode, out MessageText);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resource


                // Make sure the object refernece are created inside the class and not from cache
                DisposeAppServerObjects();

                this._referencedRow = null;
            }
            // free native resources
        }

        private void DisposeAppServerObjects()
        {
            if (this._subAppObject != null)
            {
                if (this._isSubAppObjectFromOutside)
                    this._subAppObject = null;
                else
                {
                    this._subAppObject.Dispose();
                    this._subAppObject = null;
                }
            }

            if (this._appObject != null)
            {
                if (this._isAppObjectFromOutside)
                    this._appObject = null;
                else
                {
                    this._appObject.Dispose();
                    this._appObject = null;
                }
            }

            if (this._appServerConnection != null)
            {
                if (this._isConnectionFromOutside)
                    this._appServerConnection = null;
                else
                {
                    this._appServerConnection.Dispose();
                    this._connection.Dispose();
                    this._appServerConnection = null;
                    this._connection = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Check if the current customer instance belongs to the given branch
        /// </summary>
        /// <param name="branchID"></param>
        /// <returns></returns>
        public bool IsBranchSharedForShipto(string branchID, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            // retrieve cached dsCU
            Dmsi.Agility.Data.Customers custList = new Customers(out ReturnCode, out MessageText);
            DataRow[] rows = null;

            if (this._referencedRow != null)
            {
                rows = ((dsCustomerDataSet)custList.ReferencedDataSet).ttbranch_cust.Select("cust_system_id='" + this._referencedRow["system_id"].ToString() + "' AND branch_id='" + branchID + "'");
            }

            custList.Dispose();

            bool customersAvailable = false;

            if (rows != null && rows.Length > 0)
                customersAvailable = true;

            return customersAvailable;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get the data either from the cache or from the database
        /// </summary>
        /// <param name="searchCriteria">For ASP string cust_shipto_obj is enough else use the name-value pairs for back-end retrieval</param>
        /// <param name="checkCache">Check session cache first</param>
        private void Initialize(string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds = null;

            // check if in ASP mode 
            if (checkCache && System.Web.HttpContext.Current != null)
            {
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
                DataRow row;
                
                if (dm.GetCache("ShiptoRowForInformationPanel") != null)
                {
                    row = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

                    if (row != null &&
                        ((searchCriteria != null && row["cust_shipto_key"].ToString().Equals(searchCriteria, StringComparison.OrdinalIgnoreCase)) || searchCriteria == null))
                    {
                        this.Populate(row);
                    }
                }
                else if (searchCriteria != null) // try to fetch the record from the database
                {
                    // Verify if the Customer Dataset is already cached
                    if (dm.GetCache("dsCustomerDataSet") != null)
                    {
                        ds = (dsCustomerDataSet)dm.GetCache("dsCustomerDataSet");
                        DataRow[] rows = ds.ttShipto.Select("cust_shipto_obj=" + searchCriteria);

                        if (rows.Length > 0)
                            this.Populate(rows[0]);
                    }
                    else
                    {
                        // re-format searCriteria for back-end call,
                        // most of the time the original value of the searchCriteria should be cust_obj
                        string custShiptoObj = searchCriteria;

                        searchCriteria = "Shipto=" + custShiptoObj; // obj is unique
                        this.FetchShiptos(searchCriteria, checkCache, out ReturnCode, out MessageText);
                    }
                }
            }
            else
            {
                this.FetchShiptos(searchCriteria, checkCache, out ReturnCode, out MessageText);
            }
        }

        private void FetchShiptos(string searchCriteria, bool checkCache, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            this.CreateSubAppObject(checkCache);

            if (this._subAppObject != null)
            {
                Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                try
                {
                    DataManager dm = new DataManager();
                    string ipcContextId = dm.GetCache("opcContextId") as string;

                    dsCustomerDataSet dsCustomer = new dsCustomerDataSet();
                    this._subAppObject.sicustomerfetchshipto(ipcContextId, searchCriteria, ref dsCustomer, out ReturnCode, out MessageText);

                    if (dsCustomer.ttShipto.Rows.Count > 0)
                        this.Populate(dsCustomer.ttShipto.Rows[0]);

                    DisposeAppServerObjects();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    el.LogError(EventTypeFlag.AppServerDBerror, oe);
                    em.Message = oe.Message;
                    em.EventType = EventTypeFlag.AppServerDBerror;
                }
            }
        }

        /// <summary>
        /// This is the assignment of all commonly used fields
        /// </summary>
        /// <param name="row"></param>
        private void Populate(DataRow row)
        {
            if (row == null)
                throw (new Exception("DataRow can not be null."));
            else
            {
                _ShiptoObj = Convert.ToDecimal(row["cust_shipto_obj"].ToString());
                _ShiptoName = row["shipto_name"].ToString();
                _ShiptoSequence = (int)row["seq_num"];
                _ShiptoSystemID = row["system_id"].ToString();
                _CustomerKey = row["cust_key"].ToString();
                _CustomerObj = (decimal)row["cust_obj"];
                _CustomerSystemID = row["cust_key_sysid"].ToString();
                _City = row["city"].ToString();
                _State = row["state"].ToString();
                _Zip = row["zip"].ToString();
                _Address1 = row["address_1"].ToString();
                _Address2 = row["address_2"].ToString();
                _Address3 = row["address_3"].ToString();
                _Nonsalable = (bool)row["nonsalable"];
                _referencedRow = row;
            }
        }

        /// <summary>
        /// Generic Application Object initializer
        /// </summary>
        private void CreateSubAppObject(bool checkCache)
        {
            // Check if the object is already created
            if (this._subAppObject == null)
            {
                Dmsi.Agility.EntryNET.AgilityEntryNetUser user = null;
                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

                // Get ASP Session setting first
                if (checkCache && System.Web.HttpContext.Current != null)
                {
                    user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                    this._appServerConnection = (Progress.Open4GL.Proxy.Connection)dm.GetCache("ProgressAgilityLibraryConnection");
                    if (this._appServerConnection != null)
                        this._isConnectionFromOutside = true;

                    this._appObject = (Dmsi.Agility.EntryNET.EntryNET)dm.GetCache("ProgressAppObject");
                    if (this._appObject != null)
                        this._isAppObjectFromOutside = true;

                    this._subAppObject = (Dmsi.Agility.EntryNET.Customer)dm.GetCache("CustomerSubAppObject");
                    if (this._subAppObject != null)
                        this._isSubAppObjectFromOutside = true;
                }

                // if objects are still null then create them
                if (this._appServerConnection == null)
                {
                    _connection = new Dmsi.Agility.Security.Connection();

                    int loginStatus = -1;
                    string result = "";

                    if (user == null && this._uri != null &&
                        this._userName.Length != 0 && this._password.Length != 0)
                    {
                        if (_connection.Connect(this._uri, this._userName, this._password, out result))
                            loginStatus = _connection.LogIn(this._userName, this._password, out result);
                    }
                    else
                    {
                        if (user == null)
                        {
                            throw new Exception("Cached user name and password not found.\r\nIf this is an ASP implementation, make sure the AgilityEntryNetUser has been cached.");
                        }

                        if (_connection.SessionStart(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword")))
                            loginStatus = _connection.LogIn(user.GetContextValue("currentUserLogin"), user.GetContextValue("currentUserPassword"), out result);
                    }

                    if (loginStatus == 0)
                    {
                        this._appServerConnection = _connection.ProgressConnection;
                        this._appObject = _connection.ProgressApplicationObject;
                    }
                    else
                    {
                        Dmsi.Agility.EntryNET.EventLog el = new EventLog();
                        Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

                        el.LogError(EventTypeFlag.AppServerDBerror, "Dmsi.Agility.Data.Shipto: " + result + "(" + loginStatus.ToString() + ")");
                        em.EventType = EventTypeFlag.AppServerDBerror;
                        em.Message = "Dmsi.Agility.Data.Shipto: " + result + "(" + loginStatus.ToString() + ")";
                    }
                }

                if (this._appObject == null)
                    this._appObject = new Dmsi.Agility.EntryNET.EntryNET(this._appServerConnection);

                if (this._subAppObject == null)
                    this._subAppObject = new Dmsi.Agility.EntryNET.Customer(this._appObject);
            }
        }
        #endregion
    }
}
