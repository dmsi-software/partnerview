﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PVDevel.Tests
{
    [TestClass]
    public class DecimalExtensionsTests
    {
        /// <summary>
        /// ToTrimmedStringTest.
        /// Tests to verify the output of the ToTrimmedString function return the proper results
        /// </summary>
        [TestMethod]
        public void ToTrimmedStringTest()
        {
            Assert.AreEqual("10.5", (new decimal(10.5)).ToTrimmedString());
            Assert.AreEqual("2.1875", (new decimal(2.1875)).ToTrimmedString());
            Assert.AreEqual("10.5", (new decimal(10.50)).ToTrimmedString());
            Assert.AreEqual("10.5", (new decimal(10.5000)).ToTrimmedString());
            Assert.AreEqual("10", (new decimal(10.0000)).ToTrimmedString());
            Assert.AreEqual("1", (new decimal(1.0000)).ToTrimmedString());
            Assert.AreEqual("10,000", (new decimal(10000)).ToTrimmedString());
            Assert.AreEqual("10,000", (new decimal(10000.000)).ToTrimmedString());
            //Assert.AreEqual("", (new decimal()).ToTrimmedString());

            Assert.AreNotEqual("10.50", (new decimal(10.50).ToTrimmedString()));
            //Assert.AreNotEqual("", (new decimal().ToTrimmedString()));
        }
    }
}
