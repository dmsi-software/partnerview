using System;
using System.IO;
using System.Xml;
using System.ComponentModel;

namespace Dmsi.Agility.EntryNET
{

	public enum EventTypeFlag 
	{
		AgilityLibraryConnection,
		AppServerDBerror,
		BadURL,
		DB_Message,
		DB_MessageTable,
		ApplicationEvent,
		UnhandledException
	}

	
	#region ExceptionManager class

	/// <summary>
	/// Summary description for ExceptionManager.
	/// </summary>
	public class ExceptionManager
	{
//		private string _sessID;

		#region Constructor

		public ExceptionManager()
		{
			//Add constructor logic here
		}

//		public ExceptionManager(string sessID)
//		{
//			_sessID = sessID;
//		}

		#endregion

		#region Properties

//		public string sessID
//		{
//			get
//			{
//				return _sessID;
//			}
//
//			set
//			{
//				_sessID = value;
//			}
//		}

		#endregion

		#region Public Constants, Variables, and Enumerations


		#endregion

		#region Properties

		#region Private Variables

		private string _message;
		private EventTypeFlag _errortype;

		#endregion

		#region Public Properties

		/// <summary>
		/// Read/Write for simple error message.
		/// </summary>
		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}

		
		/// <summary>
		/// Boolean to check if an error message exists.
		/// </summary>
		public bool MessageExists
		{
			get
			{
				if (_message == "" || _message == null)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}


		public EventTypeFlag EventType
		{
			get
			{
				return _errortype;
			}
			set
			{
				_errortype = value;
			}
		}


		#endregion

		#endregion

		#region Methods

		public void CheckErrorCarryover()
		{
			Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
			try
			{
//				this.ErrorType = (ErrorTypeFlag)DataManager.GetCache("ErrorTypeFlag" + userName);
//				this.Message = (string)DataManager.GetCache("ErrorType" + userName);
				EventType = (EventTypeFlag)myDataManager.GetCache("ErrorTypeFlag");
				Message = (string)myDataManager.GetCache("ErrorType");
			}
			catch
			{
				//this.ErrorType = ErrorTypeFlag.NoError;
				//this.Message = "";
				EventType = EventTypeFlag.ApplicationEvent;
				Message = "";
			}
			myDataManager.RemoveCache("ErrorTypeFlag");
			myDataManager.RemoveCache("ErrorType");
		}

		public void ClearErrors()
		{
			//this.Message = "";
			//this.ErrorType = ErrorTypeFlag.NoError;
			Message = "";
			EventType = EventTypeFlag.ApplicationEvent;
		}

		#endregion

	}
	#endregion

	#region EventLog class

	public class EventLog
	{

		#region Constructor

		public EventLog()
		{
		}

		#endregion

		#region Methods

        public void LogEvent(EventTypeFlag eventType, string message)
        {
            LogError(eventType, new Exception(message));
        }

		public void LogError(EventTypeFlag eventType, string message)
		{
			LogError(eventType, new Exception(message));
		}

		public void LogError(EventTypeFlag eventType, Exception ex)
		{
			
			// if AppLog.dtd does not exist, create it.
			// validation by DTD or Schema is needed by LogViewer.ascx
			if (!File.Exists(DocManager.DocFolder + "AppLog.dtd"))
			{
				StreamWriter w = new StreamWriter(DocManager.DocFolder + "AppLog.dtd");

				//XmlTextWriter w = new XmlTextWriter(DocManager.DocFolder + "AppLog.dtd", System.Text.Encoding.UTF8);
				w.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				w.WriteLine("<!ELEMENT logfile (fields+, events+)>");
				w.WriteLine("<!ELEMENT fields (field*)>");
				w.WriteLine("<!ELEMENT field (name+, value+)>");
				w.WriteLine("<!ELEMENT name (#PCDATA)>");
				w.WriteLine("<!ELEMENT value (#PCDATA)>");
				w.WriteLine("<!ELEMENT events (event*)>");
				w.WriteLine("<!ELEMENT event (eventid, type, time, remoteip, remotehost, remoteuser, unmappedremoteuser, authuser, baseurl, querystring, http_method, description, innerexception, source, method, trace)>");
				w.WriteLine("<!ELEMENT eventid (#PCDATA)>");
				w.WriteLine("<!ELEMENT type (#PCDATA)>");
				w.WriteLine("<!ELEMENT time (#PCDATA)>");
				w.WriteLine("<!ELEMENT remoteip (#PCDATA)>");
				w.WriteLine("<!ELEMENT remotehost (#PCDATA)>");
				w.WriteLine("<!ELEMENT remoteuser (#PCDATA)>");
				w.WriteLine("<!ELEMENT unmappedremoteuser (#PCDATA)>");
				w.WriteLine("<!ELEMENT authuser (#PCDATA)>");
				w.WriteLine("<!ELEMENT baseurl (#PCDATA)>");
				w.WriteLine("<!ELEMENT querystring (#PCDATA)>");
				w.WriteLine("<!ELEMENT http_method (#PCDATA)>");
				w.WriteLine("<!ELEMENT description (#PCDATA)>");
				w.WriteLine("<!ELEMENT innerexception (#PCDATA)>");
				w.WriteLine("<!ELEMENT source (#PCDATA)>");
				w.WriteLine("<!ELEMENT method (#PCDATA)>");
				w.WriteLine("<!ELEMENT trace (#PCDATA)>");
				w.WriteLine("<!ENTITY events SYSTEM \"AppLog-Entries.xml\">");
				w.Flush();
				w.Close();
			}
		

			// if AppLog.xml does not exist, create it.
			if (!File.Exists(DocManager.DocFolder + "AppLog.xml"))	
			{
				XmlTextWriter w = new XmlTextWriter(DocManager.DocFolder + "AppLog.xml", System.Text.Encoding.UTF8);
				w.Formatting = Formatting.Indented;
				w.WriteStartDocument();
				w.WriteDocType("logfile", null, DocManager.DocFolder + "AppLog.dtd", null); // "<!ENTITY events SYSTEM \"AppLog-Entries.xml\">");
				w.WriteStartElement("logfile");
				w.WriteStartElement(null, "fields", null);

				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "ID");
				w.WriteElementString("value", "eventid");
				w.WriteEndElement();

				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Error Type");
				w.WriteElementString("value", "type");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Date/Time");
				w.WriteElementString("value", "time");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Remote IP");
				w.WriteElementString("value", "remoteip");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Remote Host");
				w.WriteElementString("value", "remotehost");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Remote User");
				w.WriteElementString("value", "remoteuser");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Unmapped User");
				w.WriteElementString("value", "unmappedremoteuser");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Authenticated User");
				w.WriteElementString("value", "authuser");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Base URL");
				w.WriteElementString("value", "baseurl");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Query String");
				w.WriteElementString("value", "querystring");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "HTTP Method");
				w.WriteElementString("value", "http_method");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Error Message");
				w.WriteElementString("value", "description");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Inner Error Message");
				w.WriteElementString("value", "innerexception");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Error Source");
				w.WriteElementString("value", "source");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Error in Method");
				w.WriteElementString("value", "method");
				w.WriteEndElement();
				
				w.WriteStartElement(null, "field", null);
				w.WriteElementString("name", "Stack Trace");
				w.WriteElementString("value", "trace");
				w.WriteEndElement();

				w.WriteEndElement();

				w.WriteStartElement(null, "events", null);
				w.WriteEntityRef("events");
				w.WriteEndElement();

				w.WriteEndElement();

				w.WriteEndDocument();
				w.Flush();
				w.Close();
			}

			// append event data to Applog-Entries.xml.
			// if file does not exist, it is created.
			object m_obj = new object();
			int counter = 0;
Start:
			try
			{
				if(counter == 200)
				{
					goto End;
				}

				FileStream fs = new FileStream(DocManager.DocFolder + "AppLog-Entries.xml", FileMode.Append, FileAccess.Write, FileShare.None);

				StreamWriter sw = new StreamWriter(fs);
				XmlTextWriter xtw =  new XmlTextWriter(sw); 
				xtw.Formatting = Formatting.Indented;

				System.Web.HttpContext context = System.Web.HttpContext.Current;
                xtw.WriteWhitespace(" ");
				xtw.WriteStartElement("event");

				xtw.WriteElementString("eventid", DateTime.Now.Ticks.ToString());
				xtw.WriteElementString("type", eventType.ToString());
				xtw.WriteElementString("time", formattedTimeStamp(DateTime.Now));
				xtw.WriteElementString("remoteip",  context.Request.ServerVariables["REMOTE_ADDR"]);
				xtw.WriteElementString("remotehost", context.Request.ServerVariables["REMOTE_HOST"]);
				xtw.WriteElementString("remoteuser", context.Request.ServerVariables["REMOTE_USER"]);
				xtw.WriteElementString("unmappedremoteuser", context.Request.ServerVariables["UNMAPPED_REMOTE_USER"]);
				xtw.WriteElementString("authuser", context.Request.ServerVariables["AUTH_USER"]);
				xtw.WriteElementString("baseurl", context.Request.ServerVariables["URL"]);
				xtw.WriteElementString("querystring", context.Request.QueryString.ToString());
				xtw.WriteElementString("http_method", context.Request.ServerVariables["CONTENT_TYPE"]);

				xtw.WriteElementString("description", ex.Message == null ? "" : ex.Message);
				xtw.WriteElementString("innerexception", ex.InnerException == null ? "" : ex.InnerException.Message);
				xtw.WriteElementString("source", ex.Source == null ? "" : ex.Source);
				xtw.WriteElementString("method", ex.TargetSite == null ? "" : ex.TargetSite.ToString());
				xtw.WriteElementString("trace", ex.StackTrace == null ? "" : ex.StackTrace);
                
				xtw.Flush();
				xtw.Close();
				sw.Close();
				fs.Close();
			}
			catch(System.Exception se)
			{
				m_obj = se;
				counter++;
				goto Start;
			}
End:
			counter = 0;
		}

        protected string formattedTimeStamp(DateTime myTime)
        {
            string myDateTimeStamp;
            string myTimeStamp = " ";
            int myDateStamp;

            myDateStamp = (myTime.Year * 10000) + (myTime.Month * 100) + (myTime.Day);
            
            myTimeStamp += (myTime.TimeOfDay.ToString());
            myDateTimeStamp = myDateStamp.ToString() + myTimeStamp.ToString();
            return myDateTimeStamp;
        }
		#endregion

	}
	#endregion

}
