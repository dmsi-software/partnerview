using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using Dmsi.Agility.EntryNET.StrongTypesNS;

namespace Dmsi.Agility.EntryNET
{

    #region Public Constants, Variables, and Enumerations

    /// <summary>
    /// The FilterFlag enumeration determines which filters are applied by the InputFilter
    /// function.  The Flags attribute allows the user to include multiple 
    /// enumerated values in a single variable by OR'ing the individual values
    /// together.
    /// </summary>
    public enum FilterFlag
    {
        TestAll = 0,
        Multiline = 1,
        NoMarkup = 2,
        NoScripting = 4,
        NoSQL = 8
    }

    #endregion

    #region SecurityManager Object

    public class SecurityManager
    {
        const bool DEBUG = false;

        #region Constructor

        public SecurityManager()
        {
            //Add constructor logic here
        }

        //		public SecurityManager(string sessID)
        //		{
        //			_sessID = sessID;
        //		}

        #endregion

        #region Properties

        //		public string sessID
        //		{
        //			get
        //			{
        //				return _sessID;
        //			}
        //
        //			set
        //			{
        //				_sessID = value;
        //			}
        //		}

        #endregion

        #region Methods

        #region Decrypt

        /// <summary>
        /// Decrypts 128-bit encrypted string data, using an encryption key.
        /// </summary>
        /// <param name="strKey">String encryption key.</param>
        /// <param name="strData">Encrypted string data to be decrypted.</param>
        /// <returns></returns>
        public string Decrypt(string strKey, string strData)
        {
            string strValue = "";

            if (strKey.Length > 0)
            {
                // convert strKey to 16-characters for simplicity
                if (strKey.Length < 16) { strKey.PadRight(16, Convert.ToChar("x")); }
                else { strKey = strKey.Substring(0, 16); }

                // create encryption strKeys
                byte[] bytestrKey = Encoding.UTF8.GetBytes(strKey.Substring(0, 8));
                byte[] byteVector = Encoding.UTF8.GetBytes(strKey.Substring(8));

                // convert data to byte array and Base64 decode
                byte[] byteData = new byte[strData.Length];
                try
                {
                    byteData = Convert.FromBase64String(strData);
                }
                catch
                {
                    strValue = strData;
                }

                if (strValue.Length == 0)
                {
                    try
                    {
                        // decrypt
                        DESCryptoServiceProvider objDES = new DESCryptoServiceProvider();
                        MemoryStream objMemoryStream = new MemoryStream();
                        CryptoStream objCryptoStream = new CryptoStream(objMemoryStream, objDES.CreateEncryptor(bytestrKey, byteVector), CryptoStreamMode.Write);
                        objCryptoStream.Write(byteData, 0, byteData.Length);
                        objCryptoStream.FlushFinalBlock();

                        // convert to string
                        System.Text.Encoding objEncoding = System.Text.Encoding.UTF8;
                        strValue = objEncoding.GetString(objMemoryStream.ToArray());
                    }
                    catch
                    {
                        strValue = "";
                    }
                }
            }
            else
            {
                strValue = strData;
            }

            return strValue;
        }

        #endregion

        #region Encrypt

        /// <summary>
        /// 128-bit encrypts any string data, using an encryption key.
        /// </summary>
        /// <param name="strKey">String encryption key.</param>
        /// <param name="strData">String data to be encrypted.</param>
        /// <returns></returns>
        public string Encrypt(string strKey, string strData)
        {
            string strValue = "";

            if (strKey.Length != 0)
            {
                // convert strKey to 16-characters for simplicity
                if (strKey.Length < 16) { strKey.PadRight(16, Convert.ToChar("x")); }
                else { strKey = strKey.Substring(0, 16); }

                // create encryption strKeys
                byte[] bytestrKey = Encoding.UTF8.GetBytes(strKey.Substring(0, 8));
                byte[] byteVector = Encoding.UTF8.GetBytes(strKey.Substring(8));

                // convert strData to byte array
                byte[] byteData = Encoding.UTF8.GetBytes(strData);

                // encrypt
                DESCryptoServiceProvider objDES = new DESCryptoServiceProvider();
                MemoryStream objMemoryStream = new MemoryStream();
                CryptoStream objCryptoStream = new CryptoStream(objMemoryStream, objDES.CreateEncryptor(bytestrKey, byteVector), CryptoStreamMode.Write);
                objCryptoStream.Write(byteData, 0, byteData.Length);
                objCryptoStream.FlushFinalBlock();

                // convert to string and Base64 encode
                strValue = Convert.ToBase64String(objMemoryStream.ToArray());
            }
            else
            {
                strValue = strData;
            }

            return strValue;
        }

        #endregion

        #region FormatDisableScripting

        /// <summary>
        /// Removes HTML tags which are targeted in Cross-site scripting (XSS) attacks.
        /// </summary>
        /// <param name="strInput">This is the string to be filtered</param>
        /// <returns>Filtered UserInput</returns>
        /// <remarks>
        /// This function will evolve to provide more robust checking as additional holes are found.
        /// </remarks>
        public string FormatDisableScripting(string strInput)
        {
            string TempInput = strInput;

            RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            string strReplacement = " ";
            //string strPattern = "<script[^>]*>.*?</script[^><]*>";

            TempInput = Regex.Replace(TempInput, "<script[^>]*>.*?</script[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<input[^>]*>.*?</input[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<object[^>]*>.*?</object[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<applet[^>]*>.*?</applet[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<form[^>]*>.*?</form[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<option[^>]*>.*?</option[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<select[^>]*>.*?</select[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<iframe[^>]*>.*?</iframe[^><]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "<form[^>]*>", strReplacement, options);
            TempInput = Regex.Replace(TempInput, "</form[^><]*>", strReplacement, options);

            return TempInput;
        }

        #endregion

        #region FormatMultiLine

        /// <summary>
        /// Removes CrLf characters and inserts br markup.
        /// </summary>
        /// <param name="strInput">String to be filtered</param>
        /// <returns>Filtered UserInput</returns>
        public string FormatMultiLine(string strInput)
        {
            string TempInput = strInput.Replace('\x000D'.ToString() + '\x000A'.ToString(), "<br>");
            return TempInput.Replace('\x000D'.ToString(), "<br>");
        }

        #endregion

        #region FormatRemoveSQL

        /// <summary>
        /// Verifies raw SQL statements to prevent SQL injection attacks
        /// </summary>
        /// <param name="strSQL">String to be filtered</param>
        /// <returns>Filtered UserInput</returns>
        public string FormatRemoveSQL(string strSQL)
        {
            string strCleanSQL = strSQL;
            if (strSQL != null)
            {
                string[] BadCommands = new string[] { ";", "--", "create", "drop", "select", "insert", "delete", "update", "union", "sp_", "xp_", "," };
                // strip dangerous SQL commands
                for (int intCommand = 0; intCommand < BadCommands.Length; intCommand++)
                {
                    strCleanSQL = Regex.Replace(strCleanSQL, Convert.ToString(BadCommands.GetValue(intCommand)), " ", RegexOptions.IgnoreCase);
                }
                // convert single quotes
                strCleanSQL = strCleanSQL.Replace("'", "''");
            }

            return strCleanSQL;
        }

        #endregion

        #region includesMarkup

        /// <summary>
        /// Determines if an Input string contains any markup.
        /// </summary>
        /// <param name="strInput">String to be checked</param>
        /// <returns>True if string contains Markup tag(s)</returns>
        public bool IncludesMarkup(string strInput)
        {
            RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            string strPattern = "<[^<>]*>";
            return Regex.IsMatch(strInput, strPattern, options);
        }

        #endregion

        #region InputFilter

        /// <summary>
        /// Applies security filtering to the UserInput string.
        /// </summary>
        /// <param name="UserInput">String to be filtered</param>
        /// <param name="FilterType">Flags which designate the filters to be applied</param>
        /// <returns>Filtered UserInput</returns>
        public string InputFilter(string userInput, FilterFlag filterType)
        {
            string tempInput = userInput;

            if (filterType == 0)
            {
                if (IncludesMarkup(tempInput)) { tempInput = HttpUtility.HtmlEncode(tempInput); }
                tempInput = FormatRemoveSQL(tempInput);
                tempInput = FormatDisableScripting(tempInput);
                tempInput = FormatMultiLine(tempInput);
            }

            if ((filterType & FilterFlag.NoSQL) == FilterFlag.NoSQL)
            {
                tempInput = FormatRemoveSQL(tempInput);
            }
            else
            {
                if ((filterType & FilterFlag.NoMarkup) == FilterFlag.NoMarkup)
                {
                    if (IncludesMarkup(tempInput)) { tempInput = HttpUtility.HtmlEncode(tempInput); }
                }
                else if ((filterType & FilterFlag.NoScripting) == FilterFlag.NoScripting)
                {
                    tempInput = FormatDisableScripting(tempInput);
                }

                if ((filterType & FilterFlag.Multiline) == FilterFlag.Multiline)
                {
                    tempInput = FormatMultiLine(tempInput);
                }
            }

            return tempInput;
        }

        #endregion

        #region UserLogin

        /// <summary>
        /// UserLogin accepts username and password from login control. If login is accepted, it sets
        /// up the user context so roles and permissions can be set and enforced.
        /// </summary>
        /// <param name="commandString">Miscellaneous information that will be passed to the back-end, ex. Product=PrtnrView</param>
        /// <param name="userName">Username of the login.</param>
        /// <param name="password">Password of the login.</param>
        /// <returns>Boolean value 'loginAccepted'</returns>
        public int UserLogin(string commandString, string userName, string password, ref string errorMessage_Param, ref string errorMessage_Catch)
        {
            Dmsi.Agility.EntryNET.DataManager dataManager = new DataManager();
            Dmsi.Agility.EntryNET.ExceptionManager exceptionManager = new ExceptionManager();
            Dmsi.Agility.EntryNET.EventLog eventLog = new EventLog();
            Dmsi.Agility.EntryNET.Session sessionSubAppObject = null;

            int loginAccepted = -1;

            dsSessionMgrDataSet dsSessionManager = new dsSessionMgrDataSet();
            dsCustomerDataSet dsCU = new dsCustomerDataSet();
            dsPartnerVuDataSet dsPV = new dsPartnerVuDataSet();
            dsPVParamDataSet dsPVParam = new dsPVParamDataSet();

            EventManager em = new EventManager(); 

            Connection connection = em.SessionStart(ref userName, ref password);

            dataManager.SetCache("AgilityLibraryConnection", connection);

            // Dmsi.Agility.Data usage
            dataManager.SetCache("ProgressAgilityLibraryConnection", connection.AppServerConnection);
            dataManager.SetCache("ProgressAppObject", connection.EntryNetProxy);

            string opcContextId = "";

            try
            {
                Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

                sessionSubAppObject = new Dmsi.Agility.EntryNET.Session(connection.EntryNetProxy);
                sessionSubAppObject.sipartnervuestablishcontext(userName, password, commandString, out opcContextId, out dsSessionManager, out dsCU, out dsPV, out dsPVParam, out loginAccepted, out errorMessage_Param);
                sessionSubAppObject.Dispose();
                sessionSubAppObject = null;
            }
            catch (Progress.Open4GL.Exceptions.Open4GLException oe)
            {
                eventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                exceptionManager.Message = oe.Message;
                exceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                errorMessage_Catch = oe.Message;

                if (sessionSubAppObject != null)
                    sessionSubAppObject.Dispose();

                connection.Disconnect();
                throw new Exception(errorMessage_Catch);
            }

            if (opcContextId.Length > 0)
                dataManager.SetCache("opcContextId", opcContextId);

            ////HACK: turn dsSessionManager, dsCU and dsPV on and off here...
            ////next lines "dump" DataSets to XML...
            //#if DEBUG       
            //    dsSessionManager.WriteXml(DocManager.DocFolder + "dsSessionManager.xml");
            //    dsCU.WriteXml(DocManager.DocFolder + "dsCU.xml");
            //    dsPV.WriteXml(DocManager.DocFolder + "dsPV.xml");		
            //#endif

            // Cache the SessionManager, CU and PV datasets
            dataManager.SetCache("dsSessionManager", dsSessionManager);
            dataManager.SetCache("dsCU", dsCU);
            dataManager.SetCache("dsPV", dsPV);
            dataManager.SetCache("dsPVParam", dsPVParam);

            DataRow[] rows = dsPVParam.ttparam_pv.Select("property_name='use_route_dates'");

            if (rows.Length > 0)
                dataManager.SetCache("use_route_dates", rows[0]["property_value"].ToString());

            rows = dsPVParam.ttparam_pv.Select("property_name='purechat_account_key'");

            if (rows.Length > 0 && rows[0]["property_value"].ToString().Trim().Length > 0)
                dataManager.SetCache("ChatScript", "<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '" + rows[0]["property_value"].ToString() + "', f: true }); done = true; } }; })();</script>");

            return loginAccepted;
            //			errorMessage_Param = "Your password has expired and must be changed.";
            //			return -40;
        }

        #endregion

        #region SaveCustomConfigurations

        public bool SaveCustomConfigurations(dsSessionMgrDataSet ds, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            bool returnValue = false;

            Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
            Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
            Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
            Dmsi.Agility.EntryNET.Session sessionSubAppObject = null;

            Connection connection = (Connection)myDataManager.GetCache("AgilityLibraryConnection");

            try
            {
                DataManager dm = new DataManager();
                string ipcContextId = dm.GetCache("opcContextId") as string;

                sessionSubAppObject = new Dmsi.Agility.EntryNET.Session(connection.EntryNetProxy);
                sessionSubAppObject.sisavepvcustomconfigurations(ipcContextId, ds, out ReturnCode, out MessageText);
                sessionSubAppObject.Dispose();
                returnValue = true;
            }
            catch (Progress.Open4GL.Exceptions.Open4GLException oe)
            {
                myEventLog.LogError(EventTypeFlag.ApplicationEvent, oe);
                myExceptionManager.Message = oe.Message;
                myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;

                if (sessionSubAppObject != null)
                    sessionSubAppObject.Dispose();

                connection.Disconnect();
            }

            return returnValue;
        }

        #endregion

        #region UserLogout

        /// <summary>
        /// Logs out the user, and destroys the cached dataset with key "dsSessionManager."
        /// </summary>
        public void UserLogout()
        {
            FormsAuthentication.SignOut();
        }

        #endregion

        #region ChangePassword

        /// <summary>
        /// Change Password method in m-layer calls siSessionMgrChangeUserPwd method in entrynet.dll
        /// </summary>
        /// <param name="ipcUser">Current User Name</param>
        /// <param name="ipcOldPassword">Old Password</param>
        /// <param name="ipcNewPassword">New Password</param>
        /// <param name="oplSuccess"> returns boolean true if successful</param>
        /// <param name="opcMessage">message string returned</param>
        public void ChangePassword(string ipcUser, string ipcOldPassword, string ipcNewPassword, ref bool oplSuccess, ref string opcMessage, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
            Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
            Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
            Dmsi.Agility.EntryNET.Session sessionSubAppObject = null;

            Connection connection = (Connection)myDataManager.GetCache("AgilityLibraryConnection");

            try
            {
                DataManager dm = new DataManager();
                string ipcContextId = dm.GetCache("opcContextId") as string;

                sessionSubAppObject = new Dmsi.Agility.EntryNET.Session(connection.EntryNetProxy);
                sessionSubAppObject.sisessionmgrchangeuserpwdsf(ipcContextId, ipcUser, ipcOldPassword, ipcNewPassword, out oplSuccess, out opcMessage, out ReturnCode, out MessageText);
                sessionSubAppObject.Dispose();
            }
            catch (Progress.Open4GL.Exceptions.Open4GLException oe)
            {
                myEventLog.LogError(EventTypeFlag.ApplicationEvent, oe);
                myExceptionManager.Message = oe.Message;
                myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;

                if (sessionSubAppObject != null)
                    sessionSubAppObject.Dispose();

                connection.Disconnect();
            }
        }

        #endregion

        #region ResetPassword

        /// <summary>
        /// Reset Password method in m-layer calls siSessionMgrChangeUserPwd method in entrynet.dll
        /// </summary>
        /// <param name="ipcUser">Current User Name</param>
        /// <param name="oplSuccess"> returns boolean true if successful</param>
        /// <param name="opcMessage">message string returned</param>
        public void ResetPassword(string ipcUser, ref bool oplSuccess, ref string opcMessage, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
            Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
            Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
            Dmsi.Agility.EntryNET.Session sessionSubAppObject;

            EventManager em = new EventManager();

            string userLogin = "Default";
            string password = "Default";

            Connection AgilityLibraryConnection = em.SessionStart(ref userLogin, ref password);

            try
            {
                DataManager dm = new DataManager();
                string ipcContextId = dm.GetCache("opcContextId") as string;

                sessionSubAppObject = new Dmsi.Agility.EntryNET.Session(AgilityLibraryConnection.EntryNetProxy);
                sessionSubAppObject.sisessionmgrresetuserpwdsf(ipcContextId, ipcUser, out oplSuccess, out opcMessage, out ReturnCode, out MessageText);
            }
            catch (Progress.Open4GL.Exceptions.Open4GLException oe)
            {
                myEventLog.LogError(EventTypeFlag.ApplicationEvent, oe);
                myExceptionManager.Message = oe.Message;
                myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                AgilityLibraryConnection.Disconnect();
            }
        }

        #endregion

        #endregion

    }

    #endregion

    #region User object

    public class AgilityEntryNetUser
    {
        private dsSessionMgrDataSet.ttSelectedPropertyDataTable _UserContext;
        private string _ContextID = "";

        public string ContextID
        {
            get { return _ContextID; }
            set
            {
                _ContextID = value;

                // MDM foreach (dsSessionMgrDataSet.ttSelectedPropertyRow row in UserContext.Rows)
                //{
                //    row.contextID = value;
                //}
            }
        }

        public dsSessionMgrDataSet.ttSelectedPropertyDataTable UserContext
        {
            get { return _UserContext; }
        }

        public AgilityEntryNetUser()
        {
            _UserContext = new dsSessionMgrDataSet.ttSelectedPropertyDataTable();
        }

        public void AddContext(string name, string value, string contextID)
        {
            DataRow[] rows = UserContext.Select("propertyName='" + name + "'");

            if (rows.Length > 0)
                rows[0]["propertyValue"] = value;
            else
            {
                if (ContextID == "")
                    ContextID = contextID;
                else if (ContextID != contextID)
                    throw new Exception("Context ID of property '" + name + "' is different from current.");

                // MDM UserContext.AddttSelectedPropertyRow(name, value, contextID, "", "", "", "", "");
                UserContext.AddttSelectedPropertyRow(name, value, "", "", "", "", "");
            }
        }

        public void AddContext(string name, string value)
        {
            DataRow[] rows = UserContext.Select("propertyName='" + name + "'");

            if (rows.Length > 0)
                rows[0]["propertyValue"] = value;
            else
                // MDM UserContext.AddttSelectedPropertyRow(name, value, ContextID, "", "", "", "", "");
                UserContext.AddttSelectedPropertyRow(name, value, "", "", "", "", "");
        }

        public string GetContextValue(string name)
        {
            string value = "";

            DataRow[] rows = UserContext.Select("propertyName='" + name + "'");

            if (rows.Length > 0)
                value = rows[0]["propertyValue"].ToString();
            else
                throw new Exception("Property name '" + name + "' not found.");

            return value;
        }
    }

    #endregion

}