﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Helper class to determine which pricing mode a user is in or has access to
/// based on their action allocations and the PV parameters set in Agility
/// </summary>
public class PriceModeHelper
{
    private Dmsi.Agility.EntryNET.DataManager _dm = null;

    const string USERPREFDISPLAYSETTING = "UserPref_DisplaySetting";

    public readonly string ERROR_ACTIONALLOCATIONMISMATCH = "PartnerView pricing access is not set up for this User ID.";

    //quote_detail.retail_price_mode values
    public readonly string MARKUPONCOSTMODE = "cost";
    public readonly string DISCOUNTFROMLISTMODE = "list";
    public readonly string NETMODE = "net";

    //session and display values
    public readonly string NETPRICE = "Net price";
    public readonly string RETAILPRICE = "Retail price";
    public readonly string RETAILPRICEMARKUPONCOST = "Retail price - markup on cost";
    public readonly string RETAILPRICEDISCOUNTFROMLIST = "Retail price - discount from list";

    //PV params
    const string PVPARAM_ALLOWRETAILPRICEDISPLAY = "allow_retail_pricing";
    const string PVPARAM_ALLOWLISTBASEDRETAILPRICE = "allow_list_based_retail_price";

    //action allocations
    const string ACTIONALLOCATION_NETMODE = "enable_net_price_display";
    const string ACTIONALLOCATION_RETAILPRICEMODE = "enable_retail_price_display";
    const string ACTIONALLOCATION_RETAILMARKUPMODE = "retail_allow_markup_from_cost";
    const string ACTIONALLOCATION_RETAILLISTMODE = "retail_allow_discount_from_list";

    public PriceModeHelper(Dmsi.Agility.EntryNET.DataManager dm)
    {
        _dm = dm;
    }

    public bool InAModeUserDoesntHaveAccessTo()
    {
        return ((InNetMode() && !HaveNetModeEnabled()) ||
                (InRetailMode() && !HaveRetailPricingEnabled()) ||
                (InRetailMode() && HaveRetailDiscountFromListModeEnabled() && !HaveRetailMarkupModeEnabled()) ||
                (InRetailMarkupMode() && !HaveRetailMarkupModeEnabled()) ||
                (InRetailListMode() && !HaveRetailDiscountFromListModeEnabled()));

    }
    /// <summary>
    /// If a user does not have a mode enabled and it's been saved previously, 
    /// default to the first mode in the following order:
    /// Net
    /// Markup on cost
    /// Discount from list
    /// </summary>
    /// <returns>string value stored in Session["UserPref_DisplaySetting"]</returns>
    public string DefaultUserPreferenceDisplaySetting()
    {
        string cReturnVal = NETPRICE;

        if (!HaveNetModeEnabled())
        {
            cReturnVal = RETAILPRICE;

            if (HaveRetailListBasedPricingEnabled())
            {
                if (HaveRetailMarkupModeEnabled())
                {
                    cReturnVal = RETAILPRICEMARKUPONCOST;
                }
                else if (HaveRetailDiscountFromListModeEnabled())
                {
                    cReturnVal = RETAILPRICEDISCOUNTFROMLIST;
                }
            }
        }

        return cReturnVal;
    }

    #region Retail Mode
    public bool InAnyRetailMode()
    {
        string cDisplaySetting = _dm.SafelyGetCacheString(USERPREFDISPLAYSETTING);

        if (cDisplaySetting == RETAILPRICE ||
            cDisplaySetting == RETAILPRICEDISCOUNTFROMLIST ||
            cDisplaySetting == RETAILPRICEMARKUPONCOST)
        {
            return true;
        }

        return false;
    }
    public bool InRetailMode()
    {
        return (_dm.SafelyGetCacheString(USERPREFDISPLAYSETTING) == RETAILPRICE);
    }
    public bool InRetailListMode()
    {
        return (_dm.SafelyGetCacheString(USERPREFDISPLAYSETTING) == RETAILPRICEDISCOUNTFROMLIST);
    }
    public bool InRetailMarkupMode()
    {
        return (_dm.SafelyGetCacheString(USERPREFDISPLAYSETTING) == RETAILPRICEMARKUPONCOST);
    }

    public string GetRetailMode()
    {
        if (_dm.SafelyGetCacheString("AllowListBasedRetailPrice").ToLower() == "yes")
        {
            if (_dm.SafelyGetCacheString(USERPREFDISPLAYSETTING) == RETAILPRICEDISCOUNTFROMLIST) return DISCOUNTFROMLISTMODE;
            if (_dm.SafelyGetCacheString(USERPREFDISPLAYSETTING) == RETAILPRICEMARKUPONCOST) return MARKUPONCOSTMODE;
        }

        return "";
    }
    public decimal GetRetailMarkupFactor()
    {
        string cRetailMode = GetRetailMode();

        if (cRetailMode == MARKUPONCOSTMODE ||
            cRetailMode == DISCOUNTFROMLISTMODE)
        {
            return _dm.SafelyGetCacheDecimal("MarkupFactor");
        }

        return 0;
    }

    public bool HaveRetailPricingEnabled()
    {
        return _dm.PVParameter(PVPARAM_ALLOWRETAILPRICEDISPLAY, "yes") &&
               _dm.ActionAllocationExists(ACTIONALLOCATION_RETAILPRICEMODE);
    }

    private bool HaveRetailListBasedPricingEnabled()
    {
        return HaveRetailPricingEnabled() &&
               _dm.PVParameter(PVPARAM_ALLOWLISTBASEDRETAILPRICE, "yes");
    }

    public bool HaveRetailMarkupModeEnabled()
    {
        return HaveRetailPricingEnabled() &&
               _dm.ActionAllocationExists(ACTIONALLOCATION_RETAILMARKUPMODE);
    }
    public bool HaveRetailDiscountFromListModeEnabled()
    {
        return HaveRetailListBasedPricingEnabled() &&
               _dm.ActionAllocationExists(ACTIONALLOCATION_RETAILLISTMODE);
    }
    #endregion

    #region Net Mode
    public bool InNetMode()
    {
        return (_dm.SafelyGetCacheString(USERPREFDISPLAYSETTING) == NETPRICE);
    }
    public bool OnlyNetModeEnabled()
    {
        return HaveNetModeEnabled() &&
              !HaveRetailPricingEnabled() &&
              !HaveRetailDiscountFromListModeEnabled() &&
              !HaveRetailMarkupModeEnabled();
    }
    public bool HaveNetModeEnabled()
    {
        return _dm.ActionAllocationExists(ACTIONALLOCATION_NETMODE);
    }
    #endregion

    #region General
    public string CurrentDisplaySetting()
    {
        return _dm.SafelyGetCacheString(USERPREFDISPLAYSETTING);
    }
    public bool OnlyOnePriceModeEnabled(bool lNetModeEnabled,
                                        bool lRetailPricingEnabled,
                                        bool lMarkupModeEnabled,
                                        bool lListModeEnabled)
    {
        int iCount = 0;

        if (lRetailPricingEnabled &&
           !lNetModeEnabled &&
           !lMarkupModeEnabled &&
           !lListModeEnabled)
        {
            return true;
        }

        if (lNetModeEnabled)
        {
            iCount++;
            if (lRetailPricingEnabled) { return false; }
        }
        if (lMarkupModeEnabled) { iCount++; }
        if (lListModeEnabled) { iCount++; }

        return (iCount == 1);
    }
    #endregion
}