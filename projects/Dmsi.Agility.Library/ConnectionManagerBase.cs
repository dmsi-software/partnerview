using System;
using System.IO;
using System.Data;

namespace Dmsi.Agility.EntryNET
{
	/// <summary>
	/// Summary description for ConnectionBase.
	/// </summary>
	public class ConnectionBase
	{
		public ConnectionBase()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		#region Connection Methods
		/// <summary>
		/// Methods for the Connection Base class
		/// </summary>

		public string ProcessError(string errorCode)
		{
			return errorCode;
		}


		public void SaveDataSetCache(DataSet ds)
		{
			if (ds != null)
			{
				try
				{
					ds.WriteXml(ds.DataSetName+".xml");
				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message.ToString());
				}
			}
		}

		public void ReadDataSetCache(DataSet ds)
		{
			if (ds != null)
			{
				try
				{
					ds.ReadXml(ds.DataSetName+".xml");
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message.ToString());
				}
			}
		}
		#endregion


		#region Connection Properties
		/// <summary>
		/// Properties for the Connection Base class 
		/// </summary>

		public bool HasContext
		{
			get
			{
				return true;
			}
		}
		#endregion
	}
}
