using System;
using System.Data;
using System.Web;
using System.Text;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.EntryNET;
using System.Web.SessionState;

namespace Dmsi.Agility.EntryNET
{
	/// <summary>
	/// Summary description for EventManager.
	/// </summary>
	public class EventManager
	{
//		private string _sessID;

		#region Constructor

		public EventManager()
		{
			// Add constructor logic here
		}

//		public EventManager(string sessID)
//		{
//			_sessID = sessID;
//		}

		#endregion

		#region Properties

//		public string sessID
//		{
//			get
//			{
//				return _sessID;
//			}
//
//			set
//			{
//				_sessID = value;
//			}
//		}

		#endregion
		
		#region Methods

		#region Session

		/// <summary>
		/// Establishes a connection to the AppServer
		/// </summary>
		public Connection SessionStart(ref string userid, ref string password)
		{
			string errorMessage;
			string connection;
			Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
			
			// get the AppServer connection string
            
            connection = System.Configuration.ConfigurationManager.AppSettings["AgilityLibraryConnection"].ToString();

			Agility.EntryNET.Connection AgilityLibraryConnection = new Connection();

			// connect to the AppServer...
			if (AgilityLibraryConnection.Connect(connection, ref userid, ref password, out errorMessage))
			{
				if(errorMessage.Length != 0)
				{
					myExceptionManager.Message = "Connection to Agility AppServer was made, but AppServer returned an error message.<br>Please contact your System Administrator for assistance.";
					myExceptionManager.EventType = EventTypeFlag.AgilityLibraryConnection;
					throw new Exception(myExceptionManager.Message);
				}
				return AgilityLibraryConnection;
			}
			else
			{
				myExceptionManager.Message = "No connection can be made to the Agility AppServer.<br>Please contact your System Administrator for assistance.";
				myExceptionManager.EventType = EventTypeFlag.AgilityLibraryConnection;
				throw new Exception(myExceptionManager.Message);
			}
		}

		#endregion

		#endregion

	}
}
