﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace Dmsi.Agility.EntryNET
{
    public class AnalyticsManager
    {
        private AnalyticsInput _aInput;
        private Page _parentPage;

        #region Constructor
        public AnalyticsManager(Page parentPage, AnalyticsInput ai)
        {
            // Add constructor logic here
            ai.Category = (parentPage.Request.Url.Host + parentPage.Request.ApplicationPath).TrimEnd('/');
            ai.Host = parentPage.Request.Url.ToString();
            ai.IPAddress = parentPage.Request.UserHostAddress;

            _aInput = ai;
            _parentPage = parentPage;
        }

        #endregion

        public void BeginTrackEvent()
        {
            //We want the call to timeout in 1 seconds...
            _parentPage.AsyncTimeout = new TimeSpan(0, 0, 1);

            // create the asynchronous task instance
            PageAsyncTask asyncTask = new PageAsyncTask(
              new BeginEventHandler(this.beginAsyncRequest),
              new EndEventHandler(this.endAsyncRequest),
              new EndEventHandler(this.asyncRequestTimeout),
             _aInput, true);

            // register the asynchronous task instance with the page
            _parentPage.RegisterAsyncTask(asyncTask);

            _parentPage.ExecuteRegisteredAsyncTasks();
        }

        private IAsyncResult beginAsyncRequest(object sender, EventArgs e, AsyncCallback callback, object state)
        {
            ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(TrackEvent), state);

            return new EmptyAsyncResult();
        }

        private void TrackEvent(object ai)
        {
            try
            {
                AnalyticsInput input = ai as AnalyticsInput;

                ASCIIEncoding encoding = new ASCIIEncoding();
                string postData = "v=1&tid=UA-99923896-1&cid=" + _parentPage.Session.SessionID + 
                                  "&t="         + HttpUtility.UrlEncode(input.Type) + 
                                  "&ec="        + HttpUtility.UrlEncode(input.Category) + 
                                  "&ea="        + HttpUtility.UrlEncode(input.Action) + 
                                  "&el="        + HttpUtility.UrlEncode(input.Label) + 
                                  "&ev="        + HttpUtility.UrlEncode(input.Value) + 
                                  "&dh="        + HttpUtility.UrlEncode(input.Host) + 
                                  "&aip=1&uip=" + HttpUtility.UrlEncode(input.IPAddress);

                //string pathfile = _parentPage.Server.MapPath("~/Documents") + "\\pvanalytics.txt";
                //File.AppendAllText(pathfile, postData + "\r\n");

                byte[] data = encoding.GetBytes(postData);
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://www.google-analytics.com/collect");
                myRequest.Method = "POST";
                myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentLength = data.Length;
                Stream newStream = myRequest.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
            }
            catch { }
        }

        private void endAsyncRequest(IAsyncResult result)
        {
            //Do Nothing...
        }

        void asyncRequestTimeout(IAsyncResult result)
        {
            //Do Nothing...
        }
    }

    public class AnalyticsInput
    {
        public AnalyticsInput()
        {

        }

        private string _Type;
        private string _Category;
        private string _Action;
        private string _Label;
        private string _Value;
        private string _Host;
        private string _IPAddress;


        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }

        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }

        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        public string Host
        {
            get { return _Host; }
            set { _Host = value; }
        }

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
    }

    public class EmptyAsyncResult : IAsyncResult
    {
        public EmptyAsyncResult() //Default ctor...
        {
        }

        public object AsyncState
        {
            get { return new object(); }
        }

        public System.Threading.WaitHandle AsyncWaitHandle
        {
            get { return null; }
        }

        public bool CompletedSynchronously
        {
            get { return true; }
        }

        public bool IsCompleted
        {
            get { return true; }
        }
    }
}
