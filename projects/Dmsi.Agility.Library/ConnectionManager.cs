using System;
using System.Data;
using System.IO;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Progress.Open4GL;

namespace Dmsi.Agility.EntryNET
{
	/// <summary>
	/// Summary description for Connection...
	/// </summary>
	public class Connection : ConnectionBase
	{
		//Version 1.3 created 11/07/2006 MDM
		// Connection objects for the Agility proxies
		private Progress.Open4GL.Proxy.Connection _agilityConnection;
		private Dmsi.Agility.EntryNET.EntryNET _entryNetProxy;
		private Dmsi.Agility.EntryNET.ExceptionManager _exceptionManager;
		private Dmsi.Agility.EntryNET.EventLog _eventLog;
		private bool _isConnected;
		private string _sessionID;
		private string _url;

		#region Constructor
		
		public Connection()
		{
			_isConnected = false;
		}

		public Connection(string sessID)
		{
			_isConnected = false;
			_sessionID = sessID;
		}

		#endregion

		#region Properties of the Connection

		public string SessionID
		{
			get
			{
				return _sessionID;
			}

			set
			{
				_sessionID = value;
			}
		}

		public string URL
		{
			get
			{
				return _url;
			}
		}

		public Progress.Open4GL.Proxy.Connection AppServerConnection
		{
			get
			{
				return _agilityConnection;
			}
		}

		public Dmsi.Agility.EntryNET.EntryNET EntryNetProxy
		{
			get
			{
				return _entryNetProxy;
			}
		}

		public bool IsConnected
		{
			get
			{
				return _isConnected;
			}
		}

		#endregion
		
		#region Base Methods for the Connection

		#region Connect

		/// <summary>
		/// Connect to the AppServer
		/// </summary>
		/// <param name="Url">URI-based AppServer connection string.</param>
		/// <param name="errorMessage">String error message (output). Empty if connection is successful.</param>
		/// <returns>Boolean. "True" if connection is successful.</returns>
		public bool Connect(string Url, ref string userid, ref string password, out string errorMessage)
		{
			//set local object-scoped variable so 
			_url = Url;
			_exceptionManager = new ExceptionManager();
			_eventLog = new EventLog();

			// Create the Agility Connection Object
			string Userid = userid;
			string Password = password;
			string AppServerInfo = "Agility EntryNET";
			errorMessage = "";

			_agilityConnection = new Progress.Open4GL.Proxy.Connection(Url, "Default", "Default", AppServerInfo);

			if(_agilityConnection == null)
			{
				_isConnected = false;
				return false;
			}

			if (HasContext)  // HasContext comes from the ConnectionBase class
			{
				try
				{
					if((this.URL.IndexOf("https") != -1) || (this.URL.IndexOf("AppServerDCS") != -1))
					{
                        AcceptAllCertificatePolicy policy = new AcceptAllCertificatePolicy();
                        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(policy.CheckValidationResult);
                    }

                    RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);
					_entryNetProxy = new Dmsi.Agility.EntryNET.EntryNET(_agilityConnection);
                    
					//HACK: grab Progress side Connection ID...
//					string id = EntryNETProxy.ConnectionId.ToString();
//
//					//Open an existing file, or create a new one.
//					FileInfo fi = new FileInfo("C:\\connections.txt");
//
//					// Create a writer, ready to add entries to the file.
//					StreamWriter sw = fi.AppendText();
//					sw.WriteLine("Connection ID: " + id  + " " + System.DateTime.Now.ToString());
//					sw.Flush();
//					sw.Close();

					_isConnected = true;
					return true;
				}
				catch(Progress.Open4GL.Exceptions.ConnectException ce)
				{
					errorMessage = ce.Message.ToString();

					_eventLog.LogError(EventTypeFlag.AppServerDBerror, ce);
					_exceptionManager.Message = ce.Message;
					_exceptionManager.EventType = EventTypeFlag.AppServerDBerror;
					errorMessage = ce.Message;
					_isConnected = false;
					return false;
				}
			}
			else
				return false;
		}

		#endregion

		#region Disconnect

		/// <summary>
		/// Disconnects from AppServer. Sets isConnected property.
		/// </summary>
		public void Disconnect()
		{
			if (_isConnected)
			{
                Type type;
                int count = System.Web.HttpContext.Current.Session.Count;

                // Clear all session variables except for Progress related Objects
                for (int i = count - 1; i >= 0; i--)
                {
                    try
                    {
						if (System.Web.HttpContext.Current.Session[i] != null)
						{
							type = System.Web.HttpContext.Current.Session[i].GetType();

							if (type.BaseType != typeof(Progress.Open4GL.Proxy.SubAppObject) &&
								type.BaseType != typeof(Progress.Open4GL.Proxy.AppObject) &&
								type != typeof(Progress.Open4GL.Proxy.Connection) &&
								System.Web.HttpContext.Current.Session[i] is IDisposable)
								DisposeSessionVariable(i);
						}
                    }
                    catch 
                    {
                        System.Web.HttpContext.Current.Session.RemoveAt(i);
                    }
                }

                // first, Release SubAppObjects
                count = System.Web.HttpContext.Current.Session.Count;
                for (int i = count - 1; i >= 0; i--)
                {
					if (System.Web.HttpContext.Current.Session[i] != null)
					{
						type = System.Web.HttpContext.Current.Session[i].GetType();

						if (type.BaseType == typeof(Progress.Open4GL.Proxy.SubAppObject))
							DisposeSessionVariable(i);
					}
                }

                _entryNetProxy = null;

                // then, Release AppObjects
                count = System.Web.HttpContext.Current.Session.Count;
                for (int i = count - 1; i >= 0; i--)
                {
					if (System.Web.HttpContext.Current.Session[i] != null)
					{
						type = System.Web.HttpContext.Current.Session[i].GetType();

						if (type.BaseType == typeof(Progress.Open4GL.Proxy.AppObject))
							DisposeSessionVariable(i);
					}
                }

                _agilityConnection = null;
                
                // then, Release Connection
                count = System.Web.HttpContext.Current.Session.Count;
                for (int i = count - 1; i >= 0; i--)
                {
					if (System.Web.HttpContext.Current.Session[i] != null)
					{
						type = System.Web.HttpContext.Current.Session[i].GetType();

						if (type == typeof(Progress.Open4GL.Proxy.Connection))
							DisposeSessionVariable(i);
					}
                }
			}

            //System.Web.HttpContext.Current.Session.Clear(); MDM - removed to keep Session["PureChat"] in scope on failed login.
            
			_isConnected = false;
		}

        private static void DisposeSessionVariable(int i)
        {
            try
            {
                IDisposable disposable;
                disposable = (IDisposable)System.Web.HttpContext.Current.Session[i];
                disposable.Dispose();
                disposable = null;
                System.Web.HttpContext.Current.Session.RemoveAt(i);
            }
            catch { }
        }

		#endregion

		#endregion

	}

    internal class AcceptAllCertificatePolicy
    {
        public AcceptAllCertificatePolicy()
        {
        }

        public bool CheckValidationResult(object obj, X509Certificate cert, X509Chain chain, SslPolicyErrors err)
        {
            // Always accept
            return true;
        }
    }
}
