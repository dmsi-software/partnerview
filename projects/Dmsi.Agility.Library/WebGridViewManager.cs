// DataManager.cs will be broken down later on, and there is no need to add new code
// to the class unless it's a bug fix.
//
// This is will make the class name self descriptive of it's specific contents.
//
// When adding statis method, make sure that the method is self contained or the related
// are static in nature.

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Dmsi.Agility.EntryNET
{
    public class WebGridViewManager
    {
        /// <summary>
        /// Method to show only specific columns of the header row.
        /// </summary>
        /// <param name="gv">System.Web.UI.WebControls.GridView</param>
        /// <param name="cols">String of cols ex. #col1#col2#col3#</param>
        public static void ShowHeaderColumns(System.Web.UI.WebControls.GridView gv, string cols)
        {
            for (int i = 0; i < gv.HeaderRow.Cells.Count; i++)
            {
                gv.HeaderRow.Cells[i].Wrap = false;

                if (cols.Contains(gv.HeaderRow.Cells[i].Text))
                {
                    gv.HeaderRow.Cells[i].Visible = true;              
                }
                else
                {
                    gv.HeaderRow.Cells[i].Visible = false;
                }
            }
        }

        /// <summary>
        /// Method to show only specific columns of the data rows. Call this inside DataRowBound event.
        /// </summary>
        /// <param name="gv">System.Web.UI.WebControls.GridView</param>
        /// <param name="row">Current GridViewRow</param>
        /// <param name="cols">String of cols ex. #col1#col2#col3#</param>
        public static void ShowRowColumns(System.Web.UI.WebControls.GridView gv, System.Web.UI.WebControls.GridViewRow row, string cols)
        {
            if (cols != null &&
                gv.HeaderRow != null &&
                row != null &&
                gv.HeaderRow.Cells.Count == row.Cells.Count)
            for (int i = 0; i < gv.HeaderRow.Cells.Count; i++)
            {
                if (cols.Contains(gv.HeaderRow.Cells[i].Text))
                    row.Cells[i].Visible = true;
                else
                    row.Cells[i].Visible = false;

                row.Cells[i].Wrap = false;
            }
        }

        /// <summary>
        /// Create an empty GridView based on a given DataTable. 
        /// This is a workaround to GridView not showing up when empty.
        /// </summary>
        /// <param name="table">System.Data.DataTable</param>
        /// <param name="gv">System.Web.UI.WebControls.GridView</param>
        public static void CreateEmptyGridView(System.Data.DataTable table, System.Web.UI.WebControls.GridView gv)
        {
            System.Data.DataColumn col;
            string gvName = gv.ID.ToString();

            for (int i = 0; i < table.Columns.Count; i++)
            {
                col = table.Columns[i];
                col.AllowDBNull = true;
            }

            table.Rows.Add(table.NewRow());
            gv.DataSource = table;
            gv.DataBind();

            if (!(gv.ID == "gvInventoryPricing" ||
                gv.ID == "gvInventoryAvailableTotal" ||
                gv.ID == "gvInventoryAvailableBranch" ||
                gv.ID == "gvInventoryAvailableTally" ||
                gv.ID == "gvInventoryAvailableTallyDetail" ||
                gv.ID == "gvOnOrderTotal" ||
                gv.ID == "gvOnOrderBranch" ||
                gv.ID == "gvOnOrderTran" ||
                gv.ID == "gvOnOrderTranDetail"))
            {
                // Clean the grid
                int columnCount = gv.Columns.Count;

                gv.Rows[0].Cells.Clear();
                gv.Rows[0].Cells.Add(new TableCell());
                gv.Rows[0].Cells[0].ColumnSpan = columnCount;
            }

            if (gv.ID == "gvOnOrderTran")
            {
                gv.Rows[0].Cells[8].Text = "&nbsp;";
                gv.Rows[0].Cells[4].Text = "&nbsp;";
            }
        }

        /// <summary>
        /// Assign column names to the grid.
        /// </summary>
        /// <param name="gv">System.Web.UI.WebControls.GridView</param>
        /// <param name="colFields">Comma delimited list of field names</param>
        /// <param name="colLabels">Comma delimited list of column names</param>
        public static void ColumnHeaderText(System.Web.UI.WebControls.GridView gv, string colFields, string colLabels)
        {
            ColumnHeaderText(gv, colFields, colLabels, false, true);
        }

        /// <summary>
        /// Assign column names to the grid.
        /// </summary>
        /// <param name="gv">System.Web.UI.WebControls.GridView</param>
        /// <param name="colFields">Comma delimited list of field names</param>
        /// <param name="colLabels">Comma delimited list of column names</param>
        /// <param name="ignoreBlank">Flag to ignore blank column names</param>
        public static void ColumnHeaderText(System.Web.UI.WebControls.GridView gv, string colFields, string colLabels, bool ignoreBlank)
        {
            ColumnHeaderText(gv, colFields, colLabels, ignoreBlank, true);
        }

        /// <summary>
        /// Assign column names to the grid.
        /// </summary>
        /// <param name="gv">System.Web.UI.WebControls.GridView</param>
        /// <param name="colFields">Comma delimited list of field names</param>
        /// <param name="colLabels">Comma delimited list of column names</param>
        /// <param name="ignoreBlank">Flag to ignore blank column names</param>
        /// <param name="respectVisibility">Flag to assign only visible columns</param>
        public static void ColumnHeaderText(System.Web.UI.WebControls.GridView gv, string colFields, string colLabels, bool ignoreBlank, bool respectVisibility)
        {
            if (colLabels.Length > 0)
            {
                string[] fields = colFields.Split(new char[] { ',' });
                string[] cols = colLabels.Split(new char[] { ',' });
                
                for (int j = 0; j < cols.Length; j++)
                {
                    for (int i = 0; i < gv.HeaderRow.Cells.Count; i++)
                    {
                        if (gv.HeaderRow.Cells[i].Text == fields[j] &&
                            (gv.HeaderRow.Cells[i].Visible || (!gv.HeaderRow.Cells[i].Visible && !respectVisibility)) &&
                            (cols[j].Length > 0 || (cols[j].Length == 0 && !ignoreBlank)))
                        {
                            gv.HeaderRow.Cells[i].Text = cols[j];
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get the value of a cell using a the row and the column name
        /// </summary>
        /// <param name="gv">System.Web.UI.WebControls.GridView</param>
        /// <param name="row">Row</param>
        /// <param name="colName">Column name</param>
        /// <returns>String value of the cell</returns>
        public static string GetValueAt(System.Web.UI.WebControls.GridView gv, int row, string colName)
        {
            string result = null;

            if (gv != null && gv.HeaderRow.Cells.Count > 0)
            for (int i = 0; i < gv.HeaderRow.Cells.Count; i++ )
            {
                if (gv.HeaderRow.Cells[i].Text == colName)
                {
                    result = gv.Rows[row].Cells[i].Text;
                    break;
                }
            }

            return result;
        }
    }
}
