using System;
using System.Data;
using System.Web;
using System.Web.Caching;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;
using Dmsi.Agility.EntryNET.StrongTypesNS;

namespace Dmsi.Agility.EntryNET
{

	#region DocManager class

	/// <summary>
	/// Summary description for DataManager.
	/// </summary>
	public class DocManager
	{

		#region Constructor
		public DocManager()
		{
			// Add constructor logic here
		}
		#endregion

		#region Properties

		private static string _docfolder;

		public static string DocFolder
		{
			get
			{
				return _docfolder;
			}
			set
			{
				_docfolder = value;
			}
		}

		#endregion

	}

	#endregion

}
