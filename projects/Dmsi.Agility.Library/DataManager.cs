using System;
using System.Data;
using System.Web;
using System.Web.Caching;
using System.Collections;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using Dmsi.Agility.EntryNET.StrongTypesNS;

namespace Dmsi.Agility.EntryNET
{

	#region DataManager class

	/// <summary>
	/// Summary description for DataManager.
	/// </summary>
	public class DataManager
	{
		//const bool DEBUG = true;
		bool DEBUG = false;

        #region Constructor

        public DataManager()
		{
            string debugtag = System.Configuration.ConfigurationManager.AppSettings["WriteXML"];
			if( debugtag == "true")
			{
				DEBUG = true;
			}
			// Add constructor logic here
		}

		#endregion

		#region Methods

		#region Cache

		#region ClearApplicationCache / RemoveCache

		/// <summary>
		/// Removes the specified item from the application's Cache object.
		/// </summary>
		/// <param name="CacheKey">A String identifier for the cache item to remove.</param>
		public void RemoveCache(string CacheKey)
		{
			if(CacheKey == null)
			{
				throw (new ArgumentNullException());
			}
			else
			{
				System.Web.HttpContext.Current.Session.Remove(CacheKey);
			}
		}

		#endregion

		#region GetCache

		/// <summary>
		/// Retrieves the specified item from the Cache object.
		/// </summary>
		/// <param name="CacheKey">The identifier for the cache item to retrieve.</param>
		/// <returns>The retrieved cache item, or null if the key is not found.</returns>
		public object GetCache(string CacheKey)
		{	
			if(CacheKey == null)
			{
				throw (new ArgumentNullException());
			}
			else
			{
				return System.Web.HttpContext.Current.Session[CacheKey];
			}
		}

        public string SafelyGetCacheString(string CacheKey, string DefaultReturnValue = "")
        {
            if (CacheKey == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                return (System.Web.HttpContext.Current.Session[CacheKey] == null ? DefaultReturnValue : (string)System.Web.HttpContext.Current.Session[CacheKey]) ;
            }
        }
        public decimal SafelyGetCacheDecimal(string CacheKey, int DefaultReturnValue = -1)
        {
            if (CacheKey == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                return (System.Web.HttpContext.Current.Session[CacheKey] == null ? DefaultReturnValue : (decimal)System.Web.HttpContext.Current.Session[CacheKey]);
            }
        }
        #endregion

        #region SetCache

        /// <summary>
        /// Inserts an item into the Cache object with a cache key to reference its location and using default values provided by the CacheItemPriority enumeration.
        /// </summary>
        /// <param name="CacheKey">The cache key used to reference the object.</param>
        /// <param name="objObject">The object to be inserted in the cache.</param>
        public void SetCache(string CacheKey, object objObject)
		{
			System.Web.HttpContext.Current.Session[CacheKey] = objObject;
		}

		#endregion

		#endregion

		#region Data

		#region Detail Documents

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsDetailDocDataSet of a PDF or Doc Storage URL
        /// </summary>
        /// <param name="SearchCriteria">Search string; name/value pairs.</param>
        /// <param name="docpopURL">URL to fetch Doc Storage document.</param>
        /// <param name="CacheDataset">Caches dataset if it is for a paging grid or multi-control use.</param>
        /// <returns></returns>
        public dsDetailDocDataSet GetDetailDocument(string SearchCriteria, ref string docpopURL, bool CacheDataset, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            string opcConfigstr = "";
            string l_docpopURL = "";
            if (SearchCriteria == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                Dmsi.Agility.EntryNET.Document myDetailDocumentObject;
                Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
                Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
                dsDetailDocDataSet ds = new dsDetailDocDataSet();

                // get new dataset
                Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

                try
                {
                    string ipcContextId = this.GetCache("opcContextId") as string;

                    myDetailDocumentObject = new Document(AgilityLibraryConnection.EntryNetProxy);
                    myDetailDocumentObject.sifetchdetaildocument(ipcContextId, SearchCriteria, out ds, out l_docpopURL, out opcConfigstr, out ReturnCode, out MessageText);
                    docpopURL = l_docpopURL;
                    myDetailDocumentObject.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                    myExceptionManager.Message = oe.Message;
                    myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                    AgilityLibraryConnection.Disconnect();
                }

                // uncomment next line to dump full DataSet to XML file.
                if (DEBUG)
                {
                    AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                    ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsDetailDocument.xml");
                }

                // cache if multiple controls will use it, or if paging is used in a grid that uses it,
                if (CacheDataset) { SetCache("dsDetailDocument", ds); }

                return ds;
            }
        }

		#endregion

		#region Sales Orders

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsSalesOrderDataSet of Sales Orders
        /// </summary>
        /// <param name="SearchCriteria">Sales Order Search string</param>
        /// <param name="CacheDataset">Caches dataset if it is for a paging grid or multi-control use.</param>
        /// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsSalesOrderDataSet</returns>
        public dsSalesOrderDataSet GetSalesOrders(string SearchCriteria, bool CacheDataset, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (SearchCriteria == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                Dmsi.Agility.EntryNET.SalesOrders mySalesOrdersObject;
                Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
                Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
                dsSalesOrderDataSet ds = new dsSalesOrderDataSet();
                dsCustomerDataSet dsCU = (dsCustomerDataSet)GetCache("dsCUforSIcalls");

                string errorMessage = "";

                // get new dataset
                Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

                try
                {
                    string ipcContextId = GetCache("opcContextId") as string;

                    mySalesOrdersObject = new SalesOrders(AgilityLibraryConnection.EntryNetProxy);
                    mySalesOrdersObject.sisalesorderfetchlist(ipcContextId, SearchCriteria, dsCU, out ds, out ReturnCode, out MessageText);
                    mySalesOrdersObject.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                    myExceptionManager.Message = oe.Message;
                    myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                    errorMessage = oe.Message.ToString();
                    AgilityLibraryConnection.Disconnect();
                }

                if (errorMessage.Length != 0)
                {
                    myExceptionManager.Message = errorMessage;
                }

                //			if(ds.HasErrors)
                //			{
                //				if(ds.Tables["ttso_header"].HasErrors)
                //				{
                //			DataRow[] rowArray = ds.Tables["ttso_header"].GetErrors();
                //				}
                //			}

                // uncomment next line to dump full DataSet to XML file.
                if (DEBUG)
                {
                    AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                    ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsSalesOrders.xml");
                }

                // create parent/child relationship 
                ds.Relations.Add("ttso_header", ds.Tables["ttso_header"].Columns["so_id"], ds.Tables["ttso_detail"].Columns["so_id"]);

                // cache if multiple controls will use it, or if paging is used in a grid that uses it,
                if (CacheDataset) { SetCache("dsSalesOrders", ds); }

                return ds;
            }
        }

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsSalesOrderDataSet of Sales Orders from cache.
        /// </summary>
        /// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsSalesORderDataSet</returns>
        public dsSalesOrderDataSet GetSalesOrders()
        {
            return (dsSalesOrderDataSet)GetCache("dsSalesOrders");
        }

        public dsSalesOrderDataSet GetSalesOrderDetail(string SearchCriteria, bool CacheDataset, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";
            
            if (SearchCriteria == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                Dmsi.Agility.EntryNET.SalesOrders mySOFetchSingleObject;
                Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
                Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
                dsSalesOrderDataSet ds = new dsSalesOrderDataSet();

                string errorMessage = "";

                // get new dataset
                Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

                try
                {
                    string ipcContextId = GetCache("opcContextId") as string;

                    mySOFetchSingleObject = new Dmsi.Agility.EntryNET.SalesOrders(AgilityLibraryConnection.EntryNetProxy);
                    mySOFetchSingleObject.sisalesorderfetchsingle(ipcContextId, SearchCriteria, out ds, out ReturnCode, out MessageText);
                    mySOFetchSingleObject.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                    myExceptionManager.Message = oe.Message;
                    myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                    errorMessage = oe.Message.ToString();
                    AgilityLibraryConnection.Disconnect();
                }

                if (errorMessage.Length != 0)
                {
                    myExceptionManager.Message = errorMessage;
                }

                // uncomment next line to dump full DataSet to XML file.
                if (DEBUG)
                {
                    AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                    ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsSalesOrderSingle.xml");
                }

                // create parent/child relationship 
                ds.Relations.Add("ttso_header", ds.Tables["ttso_header"].Columns["so_id"], ds.Tables["ttso_detail"].Columns["so_id"]);

                // cache if multiple controls will use it, or if paging is used in a grid that uses it,
                if (CacheDataset) { SetCache("dsSalesOrderSingle", ds); }

                return ds;
            }
        }

		#endregion

		#region Quotes

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsQuoteDataSet of Quotes
        /// </summary>
        /// <param name="Status">Quote Status string</param>
        /// <param name="CacheDataset">Caches dataset if it is for a paging grid or multi-control use.</param>
        /// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsQuoteDataSet</returns>
        public dsQuoteDataSet GetQuotes(string SearchCriteria, bool CacheDataset)
        {
            if (SearchCriteria == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                Dmsi.Agility.EntryNET.Quote myQuoteObject;
                Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
                dsQuoteDataSet ds = new dsQuoteDataSet();
                dsRetailAddressDataSet dsRetailAddress = new dsRetailAddressDataSet();
                dsCustomerDataSet dsCU = (dsCustomerDataSet)GetCache("dsCUforSIcalls");

                // get new dataset
                Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

                string errorMessage = "";

                try
                {
                    string ipcContextId = GetCache("opcContextId") as string;

                    myQuoteObject = new Quote(AgilityLibraryConnection.EntryNetProxy);

                    int ReturnCode;
                    string MessageText;

                    myQuoteObject.siquotefetchlist(ipcContextId, SearchCriteria, dsCU, out ds, out dsRetailAddress, out ReturnCode, out MessageText);
                    myQuoteObject.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                    myExceptionManager.Message = oe.Message;
                    myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                    errorMessage = oe.Message.ToString();
                    AgilityLibraryConnection.Disconnect();
                }

                if (errorMessage.Length != 0)
                {
                    myExceptionManager.Message = errorMessage;
                }

                // uncomment next line to dump full DataSet to XML file.
                if (DEBUG)
                {
                    AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                    ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsQuotes.xml");
                }

                // create parent/child relationship 
                ds.Relations.Add("pvttquote_header", ds.Tables["pvttquote_header"].Columns["quote_id"], ds.Tables["pvttquote_detail"].Columns["quote_id"]);

                // cache if multiple controls will use it, or if paging is used in a grid that uses it,
                if (CacheDataset) { SetCache("dsQuotes", ds); }

                return ds;
            }
        }

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsQuoteDataSet of Quotes from cache.
        /// </summary>
        /// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsQuoteDataSet</returns>
        public dsQuoteDataSet GetQuotes()
        {
            return (dsQuoteDataSet)GetCache("dsQuotes");
        }

        public dsQuoteDataSet GetQuoteDetail(string SearchCriteria, bool CacheDataset)
        {
            if (SearchCriteria == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                Dmsi.Agility.EntryNET.Quote myQOFetchSingleObject;
                Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
                dsQuoteDataSet ds = new dsQuoteDataSet();

                // get new dataset
                Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

                string errorMessage = "";

                try
                {
                    string ipcContextId = GetCache("opcContextId") as string;

                    myQOFetchSingleObject = new Quote(AgilityLibraryConnection.EntryNetProxy);

                    int ReturnCode;
                    string MessageText;

                    myQOFetchSingleObject.siquotefetchsingle(ipcContextId, SearchCriteria, out ds, out ReturnCode, out MessageText);
                    myQOFetchSingleObject.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                    myExceptionManager.Message = oe.Message;
                    myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                    errorMessage = oe.Message.ToString();
                    AgilityLibraryConnection.Disconnect();
                }

                if (errorMessage.Length != 0)
                {
                    myExceptionManager.Message = errorMessage;
                }

                // uncomment next line to dump full DataSet to XML file.
                if (DEBUG)
                {
                    AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                    ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsQuoteSingle.xml");
                }

                // create parent/child relationship 
                ds.Relations.Add("pvttquote_header", ds.Tables["pvttquote_header"].Columns["quote_id"], ds.Tables["pvttquote_detail"].Columns["quote_id"]);

                // cache if multiple controls will use it, or if paging is used in a grid that uses it,
                if (CacheDataset) { SetCache("dsQuoteSingle", ds); }

                return ds;
            }
        }

		#endregion

		#region Invoices

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsInvoiceDataSet of Acct Receivable Invoices...
        /// </summary>
        /// <param name="SearchCriteria">(string)SearchCriteria built from screen scrape...</param>
        /// <param name="CacheDataset">send true, if you want to cache the dataset...</param>
        /// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsInvoiceDataSet</returns>
        public dsInvoiceDataSet GetInvoices(string SearchCriteria, bool CacheDataset)
        {
            Dmsi.Agility.EntryNET.AcctsRcv myInvoiceObject;
            Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
            Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
            dsInvoiceDataSet ds = new dsInvoiceDataSet();
            dsCustomerDataSet dsCU = (dsCustomerDataSet)GetCache("dsCUforSIcalls");

            // get new dataset
            Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

            string errorMessage = "";

            try
            {
                string ipcContextId = GetCache("opcContextId") as string;

                myInvoiceObject = new AcctsRcv(AgilityLibraryConnection.EntryNetProxy);

                int ReturnCode;
                string MessageText;

                myInvoiceObject.siacctsrcvfetchlistinv(ipcContextId, SearchCriteria, dsCU, out ds, out ReturnCode, out MessageText);
                myInvoiceObject.Dispose();
            }
            catch (Progress.Open4GL.Exceptions.Open4GLException oe)
            {
                myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                myExceptionManager.Message = oe.Message;
                myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                errorMessage = oe.Message.ToString();
                AgilityLibraryConnection.Disconnect();
            }

            if (errorMessage.Length != 0)
            {
                myExceptionManager.Message = errorMessage;
            }

            // uncomment next line to dump full DataSet to XML file.
            if (DEBUG)
            {
                AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsInvoices.xml");
            }

            // create parent/child relationship
            DataRelation refNum = new DataRelation("ttinvoice_header1", ds.Tables["ttinvoice_header"].Columns["ref_num"], ds.Tables["ttinvoice_detail"].Columns["ref_num"], false);
            DataRelation shipmentNum = new DataRelation("ttinvoice_header2", ds.Tables["ttinvoice_header"].Columns["shipment_num"], ds.Tables["ttinvoice_detail"].Columns["shipment_num"], false);
            DataRelation[] relations = new DataRelation[] { refNum, shipmentNum };

            ds.Relations.AddRange(relations);

            // cache if multiple controls will use it, or if paging is used in a grid that uses it,
            if (CacheDataset) { SetCache("dsInvoices", ds); }

            return ds;
        }

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsInvoiceDataSet of Invoices from cache.
        /// </summary>
        /// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsInvoiceDataSet</returns>
        public dsInvoiceDataSet GetInvoices()
        {
            return (dsInvoiceDataSet)GetCache("dsInvoices");
        }

        public dsInvoiceDataSet GetInvoiceDetail(string SearchCriteria, bool CacheDataset)
        {
            if (SearchCriteria == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                Dmsi.Agility.EntryNET.AcctsRcv myINFetchSingleObject;
                Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
                dsInvoiceDataSet ds = new dsInvoiceDataSet();
                dsCustomerDataSet dsCU = (dsCustomerDataSet)GetCache("dsCUforSIcalls");

                // get new dataset
                Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

                string errorMessage = "";

                try
                {
                    string ipcContextId = GetCache("opcContextId") as string;

                    myINFetchSingleObject = new AcctsRcv(AgilityLibraryConnection.EntryNetProxy);

                    int ReturnCode;
                    string MessageText;

                    myINFetchSingleObject.siacctsrcvfetchlistinv(ipcContextId, SearchCriteria, dsCU, out ds, out ReturnCode, out MessageText);
                    myINFetchSingleObject.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                    myExceptionManager.Message = oe.Message;
                    myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                    errorMessage = oe.Message.ToString();
                    AgilityLibraryConnection.Disconnect();
                }

                if (errorMessage.Length != 0)
                {
                    myExceptionManager.Message = errorMessage;
                }

                // uncomment next line to dump full DataSet to XML file.
                if (DEBUG)
                {
                    AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                    ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsInvoiceSingle.xml");
                }

                // create parent/child relationship 
                DataRelation refNum = new DataRelation("ttinvoice_header1", ds.Tables["ttinvoice_header"].Columns["ref_num"], ds.Tables["ttinvoice_detail"].Columns["ref_num"], false);
                DataRelation shipmentNum = new DataRelation("ttinvoice_header2", ds.Tables["ttinvoice_header"].Columns["shipment_num"], ds.Tables["ttinvoice_detail"].Columns["shipment_num"], false);
                DataRelation[] relations = new DataRelation[] { refNum, shipmentNum };

                ds.Relations.AddRange(relations);

                // cache if multiple controls will use it, or if paging is used in a grid that uses it,
                if (CacheDataset) { SetCache("dsInvoiceSingle", ds); }

                return ds;
            }
        }

		#endregion

		#region Inventory

        /// <summary>
        /// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsInventoryDataSet of Inventory Items...
        /// </summary>
        /// <param name="SearchCriteria">(string)SearchCriteria built from screen scrape...</param>
        /// <param name="CacheDataset">send true, if you want to cache the dataset...</param>
        /// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsInventoryDataSet</returns>
        public dsInventoryDataSet GetInventory(string SearchCriteria, dsPartnerVuDataSet dsPV, bool CacheDataset, out int ReturnCode, out string MessageText)
        {
            ReturnCode = 0;
            MessageText = "";

            if (SearchCriteria == null)
            {
                throw (new ArgumentNullException());
            }
            else
            {
                Dmsi.Agility.EntryNET.Inventory myInventoryObject;
                Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
                Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
                dsInventoryDataSet ds = new dsInventoryDataSet();

                // get new dataset
                Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

                string errorMessage = "";

                try
                {
                    string ipcContextId = GetCache("opcContextId") as string;

                    myInventoryObject = new Inventory(AgilityLibraryConnection.EntryNetProxy);
                    myInventoryObject.siinventoryfetchlist(ipcContextId, SearchCriteria, dsPV, out ds, out ReturnCode, out MessageText);
                    myInventoryObject.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
                    myExceptionManager.Message = oe.Message;
                    myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
                    errorMessage = oe.Message.ToString();
                    AgilityLibraryConnection.Disconnect();
                }

                if (errorMessage.Length != 0)
                {
                    myExceptionManager.Message = errorMessage;
                }

                // uncomment next line to dump full DataSet to XML file.
                if (DEBUG)
                {
                    AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
                    ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsInventory.xml");
                }

                // cache if multiple controls will use it, or if paging is used in a grid that uses it,
                if (CacheDataset) { SetCache("dsInventory", ds); }

                return ds;
            }
        }

		/// <summary>
		/// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsInventoryDataSet of one Inventory Item with Extended Price...
		/// </summary>
		/// <param name="SearchCriteria">(string)SearchCriteria built from screen scrape...</param>
		/// <param name="CacheDataset">send true, if you want to cache the dataset...</param>
		/// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsInventoryDataSet</returns>
		public dsInventoryDataSet GetInventoryDetail(string SearchCriteria, dsPartnerVuDataSet dsPV, bool CacheDataset, out int ReturnCode, out string MessageText)
		{
            ReturnCode = 0;
            MessageText = "";

			if (SearchCriteria == null)
			{
				throw (new ArgumentNullException());
			}
			else
			{
				Dmsi.Agility.EntryNET.Inventory myInventoryObject;
				Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
				Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
				dsInventoryDataSet ds = new dsInventoryDataSet();

				// get new dataset
				Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

				string errorMessage = "";

				try
				{
					myInventoryObject = new Inventory(AgilityLibraryConnection.EntryNetProxy);

//					//HACK: log start of procedure...
//					myEventLog.LogEvent(EventTypeFlag.ApplicationEvent, "Start of Invt. Price, Avail. or On Order fetch.");

                    string ipcContextId = GetCache("opcContextId") as string;

                    myInventoryObject.siinventoryfetchlist(ipcContextId, SearchCriteria, dsPV, out ds, out ReturnCode, out MessageText);
					myInventoryObject.Dispose();

//					//HACK: log end of procedure...
//					myEventLog.LogEvent(EventTypeFlag.ApplicationEvent, "End of Invt. Price, Avail. or On Order fetch.");
				}
				catch (Progress.Open4GL.Exceptions.Open4GLException oe)
				{
					myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
					myExceptionManager.Message = oe.Message;
					myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
					errorMessage = oe.Message.ToString();
					AgilityLibraryConnection.Disconnect();
				}

				if(errorMessage.Length != 0)
				{
					myExceptionManager.Message = errorMessage;
				}

				// uncomment next line to dump full DataSet to XML file.
				if(DEBUG)
				{
					AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
					ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsInventoryDetail.xml");
				}
			
				// cache if multiple controls will use it, or if paging is used in a grid that uses it,
				if (CacheDataset)	{ SetCache("dsInventoryDetail", ds); }

				return ds;
			}
		}

		#endregion

		#region QuickList
		
		/// <summary>
		/// Returns a Dmsi.Agility.EntryNet.StrongTypesNS.dsInventoryDataSet of QuickList Items...
		/// </summary>
		/// <param name="SearchCriteria">(string)SearchCriteria built from screen scrape...</param>
		/// <param name="CacheDataset">send true, if you want to cache the dataset...</param>
		/// <returns>Dmsi.Agility.EntryNet.StrongTypesNS.dsInventoryDataSet</returns>
		public dsInventoryDataSet GetQuickList(string SearchCriteria, dsPartnerVuDataSet dsPV, bool CacheDataset, out int ReturnCode, out string MessageText)
		{
            ReturnCode = 0;
            MessageText = "";

			if (SearchCriteria == null)
			{
				throw (new ArgumentNullException());
			}
			else
			{
				Dmsi.Agility.EntryNET.Inventory myInventoryObject;
				Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
				Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
				dsInventoryDataSet ds = new dsInventoryDataSet();

				// get new dataset
				Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

				string errorMessage = "";

				try
				{
                    string ipcContextId = GetCache("opcContextId") as string;

					myInventoryObject = new Inventory(AgilityLibraryConnection.EntryNetProxy);
                    myInventoryObject.siinventoryfetchlist(ipcContextId, SearchCriteria, dsPV, out ds, out ReturnCode, out MessageText);
					myInventoryObject.Dispose();
				}
				catch (Progress.Open4GL.Exceptions.Open4GLException oe)
				{
					myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
					myExceptionManager.Message = oe.Message;
					myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
					errorMessage = oe.Message.ToString();
					AgilityLibraryConnection.Disconnect();
				}

				if(errorMessage.Length != 0)
				{
					myExceptionManager.Message = errorMessage;
				}

				// uncomment next line to dump full DataSet to XML file.
				if(DEBUG)
				{
					AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
					ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsQuickList.xml");
				}
			
				// cache if multiple controls will use it, or if paging is used in a grid that uses it,
				if (CacheDataset)	{ SetCache("dsQuickList", ds); }

				return ds;
			}
		}

		#endregion

		#region ProductGroup List

		/// <summary>
		/// Gets the Product Group List to populate the Drop-Down controls
		/// </summary>
		/// <param name="SearchCriteria">Branch=<selected>Product Group Major=<selected>Product Group Minor=<selected>SearchType=<selected>SearchTran=<entered>Available=<Yes/No></param>
		/// <param name="CacheDataset">When true, resulting DataSet is cached.</param>
		/// <returns></returns>
		public dsProductGroupDataSet GetProductGroupList(string SearchCriteria, bool CacheDataset, out int ReturnCode, out string MessageText)
		{
            ReturnCode = 0;
            MessageText = "";

			Dmsi.Agility.EntryNET.Inventory myProductGroupObject;
			Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
			Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
			dsProductGroupDataSet ds = new dsProductGroupDataSet();
			
			// remove existing cache
			RemoveCache("dsProductGroups");

			// get new dataset
			Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");
			
			try
			{
                string ipcContextId = GetCache("opcContextId") as string;

				myProductGroupObject = new Dmsi.Agility.EntryNET.Inventory(AgilityLibraryConnection.EntryNetProxy);
                myProductGroupObject.siinventoryfetchlistprodgrp(ipcContextId, SearchCriteria, out ds, out ReturnCode, out MessageText);
				myProductGroupObject.Dispose();
			}
			catch (Progress.Open4GL.Exceptions.Open4GLException oe)
			{
				myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
				myExceptionManager.Message = oe.Message;
				myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
				AgilityLibraryConnection.Disconnect();
			}

			// uncomment next line to dump full DataSet to XML file.
			if(DEBUG)
			{
				AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
				ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsProductGroups.xml");
			}
			
			// cache if multiple controls will use it, or if paging is used in a grid that uses it,
			if (CacheDataset)	{ SetCache("dsProductGroups", ds); }

			return ds;
		}

		#endregion

		#region GetCustomerList

		/// <summary>
		/// Get a branch context DataTable of customers
		/// </summary>
		/// <param name="branch"></param>
		/// <param name="CacheDataset"></param>
		/// <returns>System.Data.DataTable</returns>
		public void GetCustomerList(string branch, ref DataTable dt, bool CacheDataset)
		{
			if (branch == null)
			{
				throw (new ArgumentNullException());
			}
			else
			{
				string cust_system_id = "";

				dsCustomerDataSet ds = (dsCustomerDataSet)GetCache("dsCU");

				dt.TableName = "ttCustomer";

				DataColumn col1 = new DataColumn();
				col1.ColumnName = "cust_name";

				DataColumn col2 = new DataColumn();
				col2.ColumnName = "cust_code";

				dt.Columns.Add(col1);
				dt.Columns.Add(col2);

				foreach(DataRow row in ds.Tables["ttbranch_cust"].Rows)
				{
					if(row["branch_id"].ToString() == branch)
					{
						cust_system_id = row["cust_system_id"].ToString();
					}
				}

				foreach(DataRow row in ds.Tables["ttCustomer"].Rows)
				{
					if(row["system_id"].ToString() == cust_system_id)
					{
						dt.Rows.Add(new object[] { row["cust_name"].ToString(), row["cust_code"].ToString() } );
					}
				}

				if (CacheDataset)	{ SetCache("dtCustomers", ds); }
			}
		}

		#endregion

		#region GetShiptToList

		/// <summary>
		/// Gets the Branch-Customer ShipTo List to populate the Drop-Down controls
		/// </summary>
		/// <param name="SearchCriteria">Branch=<selected>Customer=<selected></param>
		/// <param name="CacheDataset">When true, resulting DataSet is cached.</param>
		/// <returns></returns>
		public void GetShipToList(string SearchCriteria, ref dsCustomerDataSet ds, bool CacheDataset, out int ReturnCode, out string MessageText)
		{
            ReturnCode = 0;
            MessageText = "";

			if (SearchCriteria == null)
			{
				throw (new ArgumentNullException());
			}
			else
			{
				Dmsi.Agility.EntryNET.Customer myShipToObject;
				Dmsi.Agility.EntryNET.EventLog myEventLog = new EventLog();
				Dmsi.Agility.EntryNET.ExceptionManager myExceptionManager = new ExceptionManager();
			
				// remove existing cache
				RemoveCache("dsShipToList");

				// get new dataset
				Connection AgilityLibraryConnection = (Connection)GetCache("AgilityLibraryConnection");

				string errorMessage = "";
			
				try
				{
                    string ipcContextId = GetCache("opcContextId") as string;

					myShipToObject = new Dmsi.Agility.EntryNET.Customer(AgilityLibraryConnection.EntryNetProxy);
                    myShipToObject.sicustomerfetchshiptolist(ipcContextId, SearchCriteria, ref ds, out ReturnCode, out MessageText);
					myShipToObject.Dispose();
				}
				catch (Progress.Open4GL.Exceptions.Open4GLException oe)
				{
					myEventLog.LogError(EventTypeFlag.AppServerDBerror, oe);
					myExceptionManager.Message = oe.Message;
					myExceptionManager.EventType = EventTypeFlag.AppServerDBerror;
					errorMessage = oe.Message.ToString();
					AgilityLibraryConnection.Disconnect();
				}

				if(errorMessage.Length != 0)
				{
					myExceptionManager.Message = errorMessage;
				}

				//HACK: turn dsShipToList on and off here...
				// uncomment next line to dump full DataSet to XML file.
				if(DEBUG)
				{
					AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
					ds.WriteXml(DocManager.DocFolder + aUser.GetContextValue("currentUserLogin") + "dsShipToList.xml");
				}
			
				// cache if multiple controls will use it, or if paging is used in a grid that uses it,
				if (CacheDataset)	{ SetCache("dsShipToList", ds); }
			}
		}

		#endregion

		#endregion

		#region Grids and Grid Settings

		#region GetGrid Settings
		/// <summary>
		/// Returns a dataset of the user's or default grid settings
		/// </summary>
		/// <param name="GridID">The string value of Request.QueryString["Display"]</param>
		/// <returns>System.Data.DataSet of grid settings</returns>
		public DataSet GetGridSettings(string GridID)
		{
			if (GridID == null)
			{
				throw (new ArgumentNullException());
			}
			else
			{
				AgilityEntryNetUser aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
				string cacheName = "GridSettings" + GridID.Replace(" ", "") + "Grid";
				string nodeID = new StringBuilder(GridID).Replace(" ", "").ToString();
				DataSet dsSettings = new DataSet();

				if (GetCache(cacheName) == null)
				{
//					XmlNode GridNode;

                    string version = System.Configuration.ConfigurationManager.AppSettings["agilityversion"];
					string userFilePath = DocManager.DocFolder + aUser.GetContextValue("currentUserLogin").ToLower() + "." + version + ".config";
					XmlDocument doc = new XmlDocument();

					FileInfo userFI = new FileInfo(userFilePath);
					if (userFI.Exists)
					{
						// load user's .config file
						doc.Load(userFilePath);
					}
					else
					{
						// get default.config's settings
						doc.Load(DocManager.DocFolder + "default" + "." + version + ".config");

						// save to new .config file
						doc.Save(userFilePath);
					}

					// get/create settings for the present grid
					XmlNode GridNode = doc.GetElementById(nodeID + "Grid");
					if (GridNode == null)	// grid settings don't exist
					{
						// get and copy default settings for present grid
						XmlDocument defaultDoc = new XmlDocument();
						defaultDoc.Load(DocManager.DocFolder + "default" + "." + version + ".config");
						XmlNode defaultGridNode = defaultDoc.GetElementById(nodeID + "Grid");

						// add user's grid settings to rest of user's settings, and save
						XmlNode objectsNode = doc.FirstChild.SelectSingleNode("objects");
						objectsNode.AppendChild(defaultGridNode);

						// save changed xml document, handling file attributes
						FileInfo fi = new FileInfo(userFilePath);
						FileAttributes att = new FileAttributes();
						att = fi.Attributes;
						fi.Attributes = FileAttributes.Normal;
						doc.Save(userFilePath);
						fi.Attributes = att;
					}

					// fill dataset with user's settings for present grid ONLY
					dsSettings.ReadXml(new XmlTextReader(GridNode.InnerXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None)));
					SetCache(cacheName, dsSettings);
				}
				else	// for --> if (GetCache(cacheName) == null)
				{
					dsSettings = (DataSet)GetCache(cacheName);
					// check to be sure user's grid settings are in the dataset
					//				if (!dsSettings.Tables.Contains(nodeID + "Grid"))
					//				{
					//					// if not, clear the cache and rebuild
					//					RemoveCache(cacheName, sessID);
					//					GetGridSettings(GridID, sessID);
					//				}
				}	// end --> if (GetCache(cacheName) == null)

				return dsSettings;		
			}
		}

		#endregion

		#region SetGridSettings
		/// <summary>
		/// Saves new grid settings for the current user.
		/// </summary>
		/// <param name="GridID">The string value of Request.QueryString["Display"]</param>
		/// <param name="dsSettings">System.Data.DataSet of grid settings.</param>
		public void SetGridSettings(string GridID, System.Data.DataSet dsSettings)
		{
			AgilityEntryNetUser aUser = new AgilityEntryNetUser();
			aUser = (AgilityEntryNetUser)GetCache("AgilityEntryNetUser");
            string version = System.Configuration.ConfigurationManager.AppSettings["agilityversion"];
			string userFilePath = DocManager.DocFolder + aUser.GetContextValue("currentUserLogin").ToLower() + "." + version + ".config";
			string cacheName = "GridSettings" + GridID.Replace(" ", "") + "Grid";

			// get user's general grid seettings
			XmlDocument doc = new XmlDocument();
			doc.Load(userFilePath);

			// get settings for the present grid
			string nodeID = new StringBuilder(GridID).Replace(" ", "").ToString();
			XmlNode GridNode = doc.GetElementById(nodeID + "Grid");

			// replace with new grid settings
			GridNode.InnerXml = dsSettings.GetXml().ToString();

			// save changed xml document, handling file attributes
//			FileInfo fi = new FileInfo(userFilePath);
//			FileAttributes att = new FileAttributes();
//			att = fi.Attributes;
//			fi.Attributes = FileAttributes.Normal;
			doc.Save(userFilePath);
//			fi.Attributes = att;

			RemoveCache(cacheName);
		}

		#endregion

		#endregion

		#region Build DataTable from Verbose DataSet

		public DataTable BuildDropDownListDataTable(ref dsCustomerDataSet ds, string tableName, string[] s_columns)
		{
			int i = s_columns.GetLength(0);

			DataColumn[] columnArray = new DataColumn[i];

			for(int j=0; j<i; j++)
			{
				DataColumn col = new DataColumn();
				columnArray[j] = col;
				columnArray[j].ColumnName = s_columns[j];
			}

			DataTable dt = new DataTable();

			foreach(DataColumn col in columnArray)
			{
				dt.Columns.Add(col);
			}

			//			bool dontDelete = false;
			//			for(int j=dt.Columns.Count - 1; j>=1; j--)
			//			{
			//				for(int k=0; k<i; k++)
			//				{
			//					if(dt.Columns[j].ColumnName == s_columns[k])
			//					{
			//						dontDelete = true;
			//					}
			//				}
			//				if(dontDelete == false)
			//				{
			//					dt.Columns.RemoveAt(j);
			//				}
			//				dontDelete = false;
			//			}
			
			string[,] stringArray = new string[ds.Tables[tableName].Rows.Count, i];
			for(int j=0; j<ds.Tables[tableName].Rows.Count; j++)
			{
				for(int k=0; k<i; k++)
				{
					stringArray[j, k] = ds.Tables[tableName].Rows[j][s_columns[k]].ToString(); 
				}
			}

			dt.Rows.Add(new Object[] { " All" } );

			for(int j=0; j<ds.Tables[tableName].Rows.Count; j++)
			{
				Object[] myobj = new Object[] { stringArray[j,0], stringArray[j,1] } ;
				dt.Rows.Add(myobj);
			}

			return dt;
		}

		#endregion

		#region User Config Update

		public void UserConfigUpdate(string userName, string sessID)
		{
            string version = System.Configuration.ConfigurationManager.AppSettings["agilityversion"];
			string defaultFilePath = DocManager.DocFolder + "default" + "." + version + ".config";
			string userFileCurrentVersionPath = DocManager.DocFolder + userName + "." + version + ".config";

			XmlDocument xmlDefaultConfig = new XmlDocument();
			xmlDefaultConfig.Load(defaultFilePath);

			XmlDocument xmlUserConfig = new XmlDocument();

			FileInfo userFI = new FileInfo(userFileCurrentVersionPath);
			if (!userFI.Exists)
			{
				bool userOldFI = false;
				DirectoryInfo directory = new DirectoryInfo(DocManager.DocFolder);
				FileInfo[] filelist = directory.GetFiles();
				foreach(FileInfo fi in filelist)
				{
					if(fi.Name.StartsWith(userName) && fi.Name.EndsWith(".config"))
					{
						userOldFI = true;
					}
				}

				if(userOldFI)
				{
					//If we're here it means we have found an older user.version.config file
					//for this user. It doesn't match the current version, so it's time to...
					//Perform config file upgrade
					UpgradeUserConfigFile(userName);
				}
				else
				{
					//copy default "current version" .config to user "current version" .config
					xmlDefaultConfig.Save(userFileCurrentVersionPath);
				}
			}

			//cache all grid settings from new, current or updated user.version.config file
			xmlUserConfig.Load(userFileCurrentVersionPath);
			XmlNodeList FormsNodeList = xmlUserConfig.SelectNodes("/forms/form/objects/grid");

			foreach(XmlNode node in FormsNodeList)
			{
				DataSet dsSettings = new DataSet();
				dsSettings.ReadXml(new XmlTextReader(node.InnerXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None)));
				SetCache("GridSettings" + node.Attributes.GetNamedItem("id").Value.ToString(), dsSettings);
			}
		}

		public void UpgradeUserConfigFile(string username)
		{
			//load default.version.config into new XmlDocument
            string version = System.Configuration.ConfigurationManager.AppSettings["agilityversion"];
			string defaultFilePath = DocManager.DocFolder + "default" + "." + version + ".config";
			
			//1. XmlDocument, new version
			XmlDocument defaultConfig = new XmlDocument();
			defaultConfig.Load(defaultFilePath);

			//load old user.previousversion.config into new XmlDocument
			string oldUserFileName = "";
			DirectoryInfo directory = new DirectoryInfo(DocManager.DocFolder);
			FileInfo[] filelist = directory.GetFiles();
			foreach(FileInfo fi in filelist)
			{
				if(fi.Name.StartsWith(username) && fi.Name.EndsWith(".config"))
				{
					oldUserFileName = fi.Name;
				}
			}
			
			//2. XmlDocument, older version
			XmlDocument oldConfig = new XmlDocument();
			oldConfig.Load(DocManager.DocFolder + oldUserFileName);

			//07-19-05 new method to Update Grids
			UpdateGrids(ref defaultConfig, ref oldConfig);
			
			defaultConfig.Save(DocManager.DocFolder + username + "." + version + ".config");
		}

		public void UpdateGrids(ref XmlDocument defaultConfig, ref XmlDocument oldConfig)
		{
			foreach(XmlNode gridNode in oldConfig.SelectNodes("/forms/form/objects/grid"))
			{
				string gridName = gridNode.Attributes.GetNamedItem("id").Value.ToString();
				int index = 0;

				for(int j=0; j<=1; j++)
				{
					//if there are columns in old.available that are in new.display, move them to new.available
					index = 0;
					foreach(XmlNode counterNode in gridNode.SelectNodes("columns/availablecolumn" + j.ToString().Trim()))
					{
						index++;
					}

					string[] availablecolumnArray = new string[index];
					index = 0;
					foreach(XmlNode availablecolumnNode in gridNode.SelectNodes("columns/availablecolumn" + j.ToString().Trim()))
					{
						availablecolumnArray[index] = availablecolumnNode.SelectSingleNode("dataName").InnerText;
						index++;
					}
					//availablecolumnArray now contains a list of the available dataNames in oldConfig...

					//loop through the Array, looking for matches in new.displayed to move to new.available.
					for(int x=0; x<=availablecolumnArray.Length-1; x++)
					{
						//outer loop for defaultConfig.forms.form.objects.grid nodes
						foreach(XmlNode defaultGridNode in defaultConfig.SelectNodes("/forms/form/objects/grid"))
						{
							if(defaultGridNode.Attributes.GetNamedItem("id").Value.ToString() == gridName)
							{
								foreach(XmlNode displayedcolumnNode in defaultGridNode.SelectNodes("columns/displayedcolumn" + j.ToString().Trim()))
								{
									if(displayedcolumnNode.SelectSingleNode("dataName").InnerText == availablecolumnArray[x])
									{
										XmlNode findNode = defaultGridNode.SelectSingleNode("columns/availablecolumn" + j.ToString().Trim());
										if(findNode != null)
										{
											XmlNode newNode = findNode.CloneNode(true);

											foreach(XmlNode childofnewNode in newNode.ChildNodes)
											{
												childofnewNode.InnerText = displayedcolumnNode.SelectSingleNode(childofnewNode.Name).InnerText;
											}

											XmlNode columnNode = defaultGridNode.SelectSingleNode("columns");
											columnNode.AppendChild(newNode);
											displayedcolumnNode.ParentNode.RemoveChild(displayedcolumnNode);
										}
									}
								}
							}
						}
					}

					//if there are columns in old.display that are in new.available, move them to new.display
					index = 0;
					foreach(XmlNode counterNode in gridNode.SelectNodes("columns/displayedcolumn" + j.ToString().Trim()))
					{
						index++;
					}

					string[] displayedcolumnArray = new string[index];
					index = 0;
					foreach(XmlNode displayedcolumnNode in gridNode.SelectNodes("columns/displayedcolumn" + j.ToString().Trim()))
					{
						displayedcolumnArray[index] = displayedcolumnNode.SelectSingleNode("dataName").InnerText;
						index++;
					}
					//displayedcolumnArray now contains a list of the displayed dataNames in oldConfig...

					//loop through the Array, looking for matches in new.available to move to new.display.
					for(int x=0; x<=displayedcolumnArray.Length-1; x++)
					{
						//outer loop for defaultConfig.forms.form.objects.grid nodes
						foreach(XmlNode defaultGridNode in defaultConfig.SelectNodes("/forms/form/objects/grid"))
						{
							if(defaultGridNode.Attributes.GetNamedItem("id").Value.ToString() == gridName)
							{
								foreach(XmlNode availablecolumnNode in defaultGridNode.SelectNodes("columns/availablecolumn" + j.ToString().Trim()))
								{
									if(availablecolumnNode.SelectSingleNode("dataName").InnerText == displayedcolumnArray[x])
									{
										XmlNode findNode = defaultGridNode.SelectSingleNode("columns/displayedcolumn" + j.ToString().Trim());
										XmlNode newNode = findNode.CloneNode(true);

										foreach(XmlNode childofnewNode in newNode.ChildNodes)
										{
											childofnewNode.InnerText = availablecolumnNode.SelectSingleNode(childofnewNode.Name).InnerText;
										}

										XmlNode columnNode = defaultGridNode.SelectSingleNode("columns");
										columnNode.AppendChild(newNode);
										availablecolumnNode.ParentNode.RemoveChild(availablecolumnNode);
									}
								}
							}
						}
					}

					//order the columns according to oldConfig.displayed
					index = 0;
					foreach(XmlNode counterNode in gridNode.SelectNodes("columns/displayedcolumn" + j.ToString().Trim()))
					{
						index++;
					}

					string[] displayedcolumnArray2 = new string[index];
					index = 0;
					foreach(XmlNode displayedcolumnNode in gridNode.SelectNodes("columns/displayedcolumn" + j.ToString().Trim()))
					{
						displayedcolumnArray2[index] = displayedcolumnNode.SelectSingleNode("dataName").InnerText;
						index++;
					}
					//displayedcolumnArray2 now contains a list of the displayed dataNames in the order they should appear in defaultConfig...

					//loop through the Array bottom to top and put matching defaultConfig.nodes at the top of the list...
					for(int x = displayedcolumnArray2.Length-1; x>=0; x--)
					{
						//outer loop for defaultConfig.forms.form.objects.grid nodes
						foreach(XmlNode defaultGridNode in defaultConfig.SelectNodes("/forms/form/objects/grid"))
						{
							if(defaultGridNode.Attributes.GetNamedItem("id").Value.ToString() == gridName)
							{
								foreach(XmlNode displayedcolumnNode in defaultGridNode.SelectNodes("columns/displayedcolumn" + j.ToString().Trim()))
								{
									if(displayedcolumnNode.SelectSingleNode("dataName").InnerText == displayedcolumnArray2[x])
									{
										displayedcolumnNode.ParentNode.InsertAfter(displayedcolumnNode, defaultGridNode.SelectSingleNode("columns/settings"));
									}
								}
							}
						}
					}
				}
			}
		}

        #endregion

        #region Action Allocations
        /// <summary>
        /// Returns if a specific action allocation is granted for the current user
        /// </summary>
        /// <param name="cActionAllocationToken">The action allocation token code one is interested in</param>
        /// <returns>System.Boolean</returns>
        public bool ActionAllocationExists(string cActionAllocationToken)
        {
            dsPartnerVuDataSet dsPVSettings = (dsPartnerVuDataSet)GetCache("dsPV");

            return dsPVSettings.bPV_Action.Select("token_code='" + cActionAllocationToken + "'").Length > 0;
        }
        #endregion

        #region Partnerview Parameters
        /// <summary>
        /// Returns if a Partnerview parameter is set a specific way
        /// </summary>
        /// <param name="cParamKey">The parameter one is interested in</param>
        /// <param name="cExpectedValue">The expected value of the parameter</param>
        /// <returns>System.Boolean</returns>
        public bool PVParameter(string cParamKey,
                                string cExpectedValue)
        {
            dsPVParamDataSet dsPVParam = (dsPVParamDataSet)GetCache("dsPVParam");

            return dsPVParam.ttparam_pv.Select("property_name='" + cParamKey + "' and property_value='" + cExpectedValue + "'").Length > 0;
        }
        #endregion

        #endregion

        #endregion
    }

}
