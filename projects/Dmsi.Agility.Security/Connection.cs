// This Class handles the connection to the AppServer and Agility
// 
// This should replace the ConnectionManager in Agility.Library in the future.
// For now use this as the main connection initiator for individual business entities.
// For ASP application, check the Session variable AgilityLibraryConnection before falling
// back to this object.

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using Dmsi.Agility.EntryNET;

namespace Dmsi.Agility.Security
{
    public class Connection: IDisposable 
    {

        #region Declaration

        private Progress.Open4GL.Proxy.Connection _ProgressConnection;
        private Dmsi.Agility.EntryNET.EntryNET _ProgressApplicationObject;
        private string _userName;
        private string _password;
        private bool _IsConnected;
        private string _SessionID;
        private System.Uri _Uri;

        private Dmsi.Agility.EntryNET.StrongTypesNS.dsSessionMgrDataSet _SessionManagerDataSet;
        private Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet _CustomerDataSet;
        private Dmsi.Agility.EntryNET.StrongTypesNS.dsPartnerVuDataSet _PVDataSet;
        private Dmsi.Agility.EntryNET.StrongTypesNS.dsPVParamDataSet _PVParamDataSet;
        
        #endregion

        #region Properties

        /// <summary>
        /// ASP application can cache this
        /// </summary>
        public DataSet SessionManagerDataSet
        {
            get { return _SessionManagerDataSet; }
        }

        public DataSet PVDataSet
        {
            get { return _PVDataSet; }
        }

        /// <summary>
        /// ASP application SHOULD cache this
        /// </summary>
        [DefaultValue(null)]
        public Progress.Open4GL.Proxy.Connection ProgressConnection
        {
            get { return _ProgressConnection; }
            set { _ProgressConnection = value; }
        }

        /// <summary>
        /// ASP application SHOULD cache this
        /// </summary>
        [DefaultValue(null)]
        public Dmsi.Agility.EntryNET.EntryNET ProgressApplicationObject
        {
            get { return _ProgressApplicationObject; }
            set { _ProgressApplicationObject = value; }
        }
    
        [DefaultValue("")]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        [DefaultValue("")]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        [DefaultValue(false)]
        public bool IsConnected
        {
            get { return _IsConnected; }
        }

        [DefaultValue("")]
        public string SessionID
        {
            get { return _SessionID; }
            set { _SessionID = value; }
        }

        [DefaultValue("")]
        public System.Uri Uri
        {
            get { return _Uri; }
            set { _Uri = value; }
        }

        #endregion

        #region Methods


        public Connection()
        {
            this._IsConnected = false;
            this.SessionID = "";
        }

        public Connection(string sessionID)
        {
            this._IsConnected = false;
            this.SessionID = sessionID;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                this.Disconnect();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true);
        }


        /// <summary>
        /// AppServer Login Process
        /// </summary>
        /// <param name="url"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool Connect(System.Uri url, string userName, string password, out string result)
        {
            bool loginAccepted = false;

            result = "";

            if (url == null)
                throw (new LogOnException("Data connection URL can not be null"));
            else if (userName == null)
                throw (new LogOnException("User Name can not be null"));
            else if (password == null)
                throw (new LogOnException("Password can not be null"));
            else
            {
                if (userName.Length == 0)
                    result = "User Name is required.";
                else if (password.Length == 0)
                    result = "Password is required.";

                if (result.Length != 0)
                    return loginAccepted;
            }

            // Static assignment
            this.Uri = url;
            this.UserName = userName;
            this.Password = password;
            
            try
            {
                this.ProgressConnection = new Progress.Open4GL.Proxy.Connection(url.ToString(), "Default", "Default", "Agility PartnerView");
            }
            catch (Progress.Open4GL.Exceptions.ConnectException ce)
            {
                result = ce.Message;
                this._IsConnected = false;
            }

            if (this.ProgressConnection == null)
            {
                this._IsConnected = false;
                result = "Unable to connect to the Application Server.";
            }
            else
            {
                try
                {
                    if ((url.ToString().IndexOf("https") != -1) || (url.ToString().IndexOf("AppServerDCS") != -1))
                    {
                        AcceptAllCertificatePolicy policy = new AcceptAllCertificatePolicy();
                        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(policy.CheckValidationResult);
                    }

                    this._IsConnected = true;
                    loginAccepted = true;
                }
                catch (Progress.Open4GL.Exceptions.ConnectException ce)
                {
                    result = ce.Message;
                    this._IsConnected = false;
                }
            }

            return loginAccepted;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool Connect(string url, string userName, string password, out string result)
        {
            System.Uri uri = new Uri(url);
            this.Uri = uri;
            return this.Connect(uri, userName, password, out result);
        }

        /// <summary>
        /// Application level login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public int LogIn(string userName, string password, out string result)
        {
            Dmsi.Agility.EntryNET.Session session = null;
            int loginAccepted = -1;
            string opcContextId = "";

            result = "";

            if (this._IsConnected)
            {
                try
                {
                    Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

                    this.ProgressApplicationObject = new Dmsi.Agility.EntryNET.EntryNET(_ProgressConnection);
                    session = new Dmsi.Agility.EntryNET.Session(_ProgressApplicationObject);
                    session.sipartnervuestablishcontext(userName, password, "Product=PrtnrView", out opcContextId, out this._SessionManagerDataSet, out this._CustomerDataSet, out this._PVDataSet, out _PVParamDataSet, out loginAccepted, out result);

                    if (System.Web.HttpContext.Current != null)
                    {
                        DataManager dm = new DataManager();
                        dm.SetCache("opcContextId", opcContextId);
                    }

                    session.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.Open4GLException oe)
                {
                    result = oe.Message;
                    this._IsConnected = false;
                    this.Disconnect();
                }
            }
            else
            {
                result = "No AppServer connection found.";
            }

            return loginAccepted;
        }

        /// <summary>
        /// Disconnect from the AppServer and Dispose all related objects
        /// </summary>
        public void Disconnect()
        {
            Dmsi.Agility.EntryNET.EventLog el = new EventLog();
            Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();

            if (_IsConnected)
            {
                try
                {
                    if (this.ProgressApplicationObject != null)
                        this.ProgressApplicationObject.Dispose();
                    
                    if (this.ProgressConnection != null)
                        this.ProgressConnection.Dispose();
                }
                catch (Progress.Open4GL.Exceptions.ConnectException ce)
                {
                    el.LogError(EventTypeFlag.ApplicationEvent, ce);
                    em.Message = ce.Message;
                    em.EventType = EventTypeFlag.ApplicationEvent; 
                }
            }

            this._IsConnected = false;
        }

        /// <summary>
        /// Establishes a connection to the AppServer using the application configuration file
        /// Must be followed by LogIn Call.
        /// </summary>
        public bool SessionStart(string userid, string password)
        {
            Dmsi.Agility.EntryNET.EventLog el = new EventLog();
            Dmsi.Agility.EntryNET.ExceptionManager em = new ExceptionManager();
            
            string errorMessage;
            string connection;
            bool connected = false;
            
            // get the AppServer connection string, ASP- WebConfig, WinForm - Configuration
            connection = System.Configuration.ConfigurationManager.AppSettings["AgilityLibraryConnection"].ToString();

            // connect to the AppServer...
            if (this.Connect(connection, userid, password, out errorMessage))
            {
                if (errorMessage.Length != 0)
                {
                    el.LogError(EventTypeFlag.AgilityLibraryConnection, "Dmsi.Agility.Security.Connection:\r\nConnection to Agility AppServer was made, but AppServer returned an error message\r\n" + errorMessage);
                    em.Message = "Dmsi.Agility.Security.Connection:\r\nConnection to Agility AppServer was made, but AppServer returned an error message\r\n" + errorMessage;
                    em.EventType = EventTypeFlag.AgilityLibraryConnection;
                    connected = false;
                }
                else 
                    connected = true;
            }
            else
            {
                el.LogError(EventTypeFlag.AgilityLibraryConnection, "Dmsi.Agility.Security.Connection:\r\nNo connection can be made to the Agility AppServer.");
                em.Message = "Dmsi.Agility.Security.Connection:\r\nNo connection can be made to the Agility AppServer.";
                em.EventType = EventTypeFlag.AgilityLibraryConnection;
                connected = false;
            }

            return connected;
        }

        #endregion
    }

    #region "helper classes"

    /// <summary>
    /// Certification
    /// </summary>
    internal class AcceptAllCertificatePolicy
    {
        public AcceptAllCertificatePolicy()
        {
        }

        public bool CheckValidationResult(object obj, X509Certificate cert, X509Chain chain, SslPolicyErrors err)
        {
            // Always accept
            return true;
        }
    }

    /// <summary>
    /// LogOn Class Exception
    /// </summary>
    public class LogOnException : System.Exception
    {
        public LogOnException(string message): base(message)
        {
        }
    }

    #endregion
}
