using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;
using System.Reflection;
using System.IO;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class gOrderCartTest : WebFormTestCase
    {
        Progress.Open4GL.Proxy.Connection oConnection;
        EntryNET oEntryNET;
        Session oSession;
        SalesOrders oSalesOrders;

        dsSessionMgrDataSet dsSessionMgr;
        dsCustomerDataSet dsCustomer;
        dsPartnerVuDataSet dsPV;
        dsPVParamDataSet dsPVParam;
        dsSalesOrderDataSet dsSalesOrder;
        dsOrderCartDataSet dsOrderCart;

        int opiLoginAccepted;
        string opcMessages;
        string ipcSelectionCriteria;

        public gOrderCartTest()
        {
        }

        /// <summary>
        /// ATestOrderCartBL.
        /// Login mmcallister/mm
        /// ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Sales Orders" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Sales Order ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=1/11/2007" + "\x0003" + "End Date=2/21/2007" + "\x0003" + "Show Results=Sales Order";
        /// Asserts existence of returned Cart DataSet.
        /// </summary>
        [Test]
        public void ATestOrderCartBL()
        {
            dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomer = new dsCustomerDataSet();
            dsPV = new dsPartnerVuDataSet();
            dsPVParam = new dsPVParamDataSet();
            dsSalesOrder = new dsSalesOrderDataSet();
            dsOrderCart = new dsOrderCartDataSet();

            oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            oEntryNET = new EntryNET(oConnection);
            oSession = new Session(oEntryNET);
            oSalesOrders = new SalesOrders(oEntryNET);

            string ipcContextId = "";

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out ipcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Sales Orders" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Sales Order ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=1/11/2007" + "\x0003" + "End Date=2/21/2007" + "\x0003" + "Show Results=Sales Order";
            dsCustomer = FetchdsCUforSIcallsDataSet(dsCustomer);

            int ReturnCode = 0;
            string MessageText = "";

            oSalesOrders.sisalesorderfetchlist(ipcContextId, ipcSelectionCriteria, dsCustomer, out dsSalesOrder, out ReturnCode, out MessageText);

            Assert.IsNotNull(dsSalesOrder);

            oSalesOrders.Dispose();
            oSession.Dispose();
            oEntryNET.Dispose();
            oConnection.Dispose();
        }

        /// <summary>
        /// BTestDTttorder_cart.
        /// Assert all fields names and order of the fields
        /// in the ttorder_cart table of the OrderCart DataSet.
        /// 14 fields.
        /// </summary>
        [Test]
        public void BTestDTttorder_cart()
        {

            Assert.AreEqual(21, dsOrderCart.Tables["ttorder_cart"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttorder_cart.txt"))
            //{
            //    fileWriter = File.CreateText("ttorder_cart.txt");
            //}
            //for (int x = 0; x < 19; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsOrderCart.Tables["ttorder_cart"].Columns[x].ColumnName + "\", dsOrderCart.Tables[\"ttorder_cart\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("branch_id", dsOrderCart.Tables["ttorder_cart"].Columns[0].ColumnName);
            Assert.AreEqual("user_id", dsOrderCart.Tables["ttorder_cart"].Columns[1].ColumnName);
            Assert.AreEqual("cust_obj", dsOrderCart.Tables["ttorder_cart"].Columns[2].ColumnName);
            Assert.AreEqual("shipto_obj", dsOrderCart.Tables["ttorder_cart"].Columns[3].ColumnName);
            Assert.AreEqual("sequence", dsOrderCart.Tables["ttorder_cart"].Columns[4].ColumnName);
            Assert.AreEqual("ITEM", dsOrderCart.Tables["ttorder_cart"].Columns[5].ColumnName);
            Assert.AreEqual("thickness", dsOrderCart.Tables["ttorder_cart"].Columns[6].ColumnName);
            Assert.AreEqual("WIDTH", dsOrderCart.Tables["ttorder_cart"].Columns[7].ColumnName);
            Assert.AreEqual("LENGTH", dsOrderCart.Tables["ttorder_cart"].Columns[8].ColumnName);
            Assert.AreEqual("SIZE", dsOrderCart.Tables["ttorder_cart"].Columns[9].ColumnName);
            Assert.AreEqual("DESCRIPTION", dsOrderCart.Tables["ttorder_cart"].Columns[10].ColumnName);
            Assert.AreEqual("qty", dsOrderCart.Tables["ttorder_cart"].Columns[11].ColumnName);
            Assert.AreEqual("uom", dsOrderCart.Tables["ttorder_cart"].Columns[12].ColumnName);
            Assert.AreEqual("price_uom", dsOrderCart.Tables["ttorder_cart"].Columns[13].ColumnName);
            Assert.AreEqual("price", dsOrderCart.Tables["ttorder_cart"].Columns[14].ColumnName);
            Assert.AreEqual("line_message", dsOrderCart.Tables["ttorder_cart"].Columns[15].ColumnName);
            Assert.AreEqual("min_pak", dsOrderCart.Tables["ttorder_cart"].Columns[16].ColumnName);
            Assert.AreEqual("extension", dsOrderCart.Tables["ttorder_cart"].Columns[17].ColumnName);
            Assert.AreEqual("configurable", dsOrderCart.Tables["ttorder_cart"].Columns[18].ColumnName);
            Assert.AreEqual("wo_phrase", dsOrderCart.Tables["ttorder_cart"].Columns[19].ColumnName);
            Assert.AreEqual("DescriptionAndWOPhrase", dsOrderCart.Tables["ttorder_cart"].Columns[20].ColumnName);
        }

        [Test]
        public void CTestDTttship_via()
        {
            Assert.AreEqual(2, dsOrderCart.Tables["ttship_via"].Columns.Count);

            Assert.AreEqual("system_id", dsOrderCart.Tables["ttship_via"].Columns[0].ColumnName);
            Assert.AreEqual("ship_via", dsOrderCart.Tables["ttship_via"].Columns[1].ColumnName);
        }

        protected dsCustomerDataSet FetchdsCUforSIcallsDataSet(dsCustomerDataSet dsCustomer)
        {
            dsCustomer.ReadXml("dsCUforSIcalls.xml");
            return dsCustomer;
        }
    }
}
