using System;
using NUnit.Extensions.Asp;
using NUnit.Framework;
using Dmsi.Agility.EntryNET.StrongTypesNS;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class CustomerTest : WebFormTestCase
    {
        private Uri _AppServer;

        public CustomerTest()
        {
            _AppServer = new Uri(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection);
        }

        //private void Login(string testName, string user, string password)
        //{
        //    Console.WriteLine(testName + " - Started; User: " + user + " Password: " + password);

        //    Browser.GetPage(Dmsi.Agility.EntryNET.Properties.Settings.Default.PVurl);

        //    TextBoxTester txtUserName = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtUserName", CurrentWebForm);
        //    TextBoxTester txtPassword = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtPassword", CurrentWebForm);
        //    ButtonTester btnLogin = new ButtonTester("ctl00_cphMaster1_LogOn1_btnLogin", CurrentWebForm);

        //    txtUserName.Text = user;
        //    txtPassword.Text = password;
        //    btnLogin.Click();

        //    LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
        //    AssertVisibility(lblWelcomeHeader, true);
        //    string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
        //    Assert.AreEqual("Welcome ", searchStr, "Login failed, the test user might not be setup to return 0.");
        //    Console.WriteLine("MainTabTest:BBranchChangeUITest - Login successful");
        //    Console.WriteLine("Current URL: " + Browser.CurrentUrl.ToString());
        //}

        #region Customer Class
        /// <summary>
        /// ACustomerClassBLTest.
        /// TestPV005/005PVTest login.
        /// Asserts 6768 customer returned.
        /// </summary>
        [Test]
        public void ACustomerClassBLTest()
        {
            string searchCriteria = "Tables=cust";
            searchCriteria += "\x0003Branch=WESTONBRANCH";
            searchCriteria += "\x0003CustQueryField=cust_obj";
            searchCriteria += "\x0003CustCompareMode=EQ";
            searchCriteria += "\x0003CustCompareValue=115459";

            dsSessionMgrDataSet dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomerDataSet dsCustomer = new dsCustomerDataSet();
            dsPartnerVuDataSet dsPV = new dsPartnerVuDataSet();
            dsPVParamDataSet dsPVParam = new dsPVParamDataSet();

            Progress.Open4GL.Proxy.Connection oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            EntryNET oEntryNET = new EntryNET(oConnection);
            Session oSession = new Session(oEntryNET);

            string ipcContextId = "";
            int opiLoginAccepted;
            string opcMessages;

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out ipcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            int ReturnCode;
            string MessageText;

            // This object creation will also force Proxy related objects
            Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(ipcContextId, _AppServer, "mmcallister", "1234567890", searchCriteria, false, false, "", out ReturnCode, out MessageText);

            Console.WriteLine("CustomerTest:CustomerClassTest - verifying Customer class back-end fetch");
            Console.WriteLine("SearchCriteria: " + searchCriteria);
            Assert.IsNotNull(cust.ReferencedRow, "cust row is null.");
            string custCode = cust.ReferencedRow["cust_code"].ToString();
            cust.Dispose();
            Assert.AreEqual("6768", custCode, "Wrong customer was returned.");
        }

        /// <summary>
        /// BCustomersClassBLTest.
        /// TestPV005/005PVTest login.
        /// Asserts 2 customer rows returned for this login.
        /// </summary>
        [Test]
        public void BCustomersClassBLTest()
        {
            string searchCriteria = "ResultCount=200\x0003CheckUserAllocation=TRUE\x0003Table=Cust";
            searchCriteria += "\x0003Branch=WESTONBRANCH";
            searchCriteria += "\x0003CustQueryField=<All>";

            dsSessionMgrDataSet dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomerDataSet dsCustomer = new dsCustomerDataSet();
            dsPartnerVuDataSet dsPV = new dsPartnerVuDataSet();
            dsPVParamDataSet dsPVParam = new dsPVParamDataSet();

            Progress.Open4GL.Proxy.Connection oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            EntryNET oEntryNET = new EntryNET(oConnection);
            Session oSession = new Session(oEntryNET);

            string ipcContextId = "";
            int opiLoginAccepted;
            string opcMessages;

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out ipcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            // This object creation will also force Proxy related objects
            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Customers custList = new Dmsi.Agility.Data.Customers(ipcContextId, _AppServer, "mmcallister", "1234567890", searchCriteria, false, out ReturnCode, out MessageText);

            Console.WriteLine("CustomerTest:CustomerClassTest - verifying Customer class back-end fetch");
            Console.WriteLine("SearchCriteria: " + searchCriteria + "; User: TestPV005");
            Assert.IsNotNull(custList, "custList is null.");
            int rows = custList.ReferencedDataSet.Tables["ttCustomer"].Rows.Count;
            custList.Dispose();
            Assert.AreEqual(200, rows, "Wrong number of rows returned.");
        }
        #endregion
    }
}
