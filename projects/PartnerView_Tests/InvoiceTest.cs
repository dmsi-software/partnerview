using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class eInvoiceTest : WebFormTestCase
    {
        Progress.Open4GL.Proxy.Connection oConnection;
        EntryNET oEntryNET;
        Session oSession;
        AcctsRcv oAcctsRcv;

        dsSessionMgrDataSet dsSessionMgr;
        dsCustomerDataSet dsCustomer;
        dsPartnerVuDataSet dsPV;
        dsPVParamDataSet dsPVParam;
        dsInvoiceDataSet dsInvoice;
        dsInvoiceDataSet dsInvoiceDetail;

        int opiLoginAccepted;
        string opcMessages;
        string ipcSelectionCriteria;

        public eInvoiceTest()
        {
        }

        /// <summary>
        /// ATestInvoiceBL.
        /// Login as mmcallister/mm.
        /// ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Invoices" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Invoice ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=4/14/2006" + "\x0003" + "End Date=5/14/2007" + "\x0003" + "Show Results=Invoice";
        /// Assert tables returned in Invoice DataSet.
        /// Tests on returned dsInvoice DataSet...
        /// Tests on returned dsInvoiceDetail DataSet...
        /// </summary>
        [Test]
        public void ATestInvoiceBL()
        {
            dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomer = new dsCustomerDataSet();
            dsPV = new dsPartnerVuDataSet();
            dsPVParam = new dsPVParamDataSet();
            dsInvoice = new dsInvoiceDataSet();
            dsInvoiceDetail = new dsInvoiceDataSet();

            oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            oEntryNET = new EntryNET(oConnection);
            oSession = new Session(oEntryNET);
            oAcctsRcv = new AcctsRcv(oEntryNET);

            string ipcContextId = "";

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out ipcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Invoices" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Invoice ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=4/14/2006" + "\x0003" + "End Date=5/14/2007" + "\x0003" + "Show Results=Invoice";
            dsCustomer = FetchdsCUforSIcallsDataSet(dsCustomer);

            int ReturnCode;
            string MessageText;

            oAcctsRcv.siacctsrcvfetchlistinv(ipcContextId, ipcSelectionCriteria, dsCustomer, out dsInvoice, out ReturnCode, out MessageText);

            //Tests on returned dsInvoice DataSet...
            Assert.IsNotNull(dsInvoice);
            Assert.AreEqual(4, dsInvoice.Tables.Count);
            Assert.AreEqual("ttinvoice_header", dsInvoice.Tables[0].TableName);
            Assert.AreEqual("ttinvoice_detail", dsInvoice.Tables[1].TableName);
            Assert.AreEqual("ttinvoice_byitem", dsInvoice.Tables[2].TableName);
            Assert.AreEqual("ttmisc", dsInvoice.Tables[3].TableName);

            //This test needed to be deprecated on 6/22/2010 due to removing the method from the Proxy...
            //ipcSelectionCriteria = "ExtendPrice=YES" + "\x0003" + "OrderTotals=YES" + "\x0003" + "Type=IN" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "SearchType=Invoice ID" + "\x0003" + "SearchTran=00020099-1";
            //oAcctsRcv.siacctsrcvfetchsingleinv(ipcSelectionCriteria, dsCustomer, out dsInvoiceDetail);

            ////Tests on returned dsInvoiceDetail DataSet...
            //Assert.IsNotNull(dsInvoiceDetail);
            //Assert.AreEqual(4, dsInvoiceDetail.Tables.Count);
            //Assert.AreEqual("ttinvoice_header", dsInvoiceDetail.Tables[0].TableName);
            //Assert.AreEqual("ttinvoice_detail", dsInvoiceDetail.Tables[1].TableName);
            //Assert.AreEqual("ttinvoice_byitem", dsInvoiceDetail.Tables[2].TableName);
            //Assert.AreEqual("ttmisc", dsInvoiceDetail.Tables[3].TableName);

            //Assert.AreEqual(0, dsInvoiceDetail.Tables["ttinvoice_header"].Rows.Count);
            //Assert.AreEqual(0, dsInvoiceDetail.Tables["ttinvoice_detail"].Rows.Count);
            //Assert.AreEqual(0, dsInvoiceDetail.Tables["ttinvoice_byitem"].Rows.Count);

            oAcctsRcv.Dispose();
            oSession.Dispose();
            oEntryNET.Dispose();
            oConnection.Dispose();
        }

        /// <summary>
        /// BTestDTttinvoice_header.
        /// Asserts all field names and the order of the fields in the
        /// ttinvoice_header table of the Invoice DataSet.
        /// </summary>
        [Test]
        public void BTestDTttinvoice_header()
        {
            //ttinvoice_header Table
            string[] fields = new string[] 
                            {"branch_id","ref_num","so_id","TYPE","job","reference","cust_po","ordered_by",
                             "order_date","ship_date","invoice_date","due_date","route_id_char","ship_via",
                             "sale_type","sale_type_desc","statusdescr","freight_terms_id","pay_terms_code",
                             "order_subtotal","order_charges","order_taxes","order_total","order_balance",
                             "terms_note","rep_1","rep_2","rep_3","shipToName","shipToAddr1","shipToAddr2",
                             "shipToAddr3","shipToCity","shipToState","shipToZip","prof_name","system_id",
                             "ref_num_sysid","ref_type","ref_num_seq","so_id_sysid","display_type",
                             "shipment_num","status_flag","cust_key","cust_key_sysid","cust_code",
                             "shipto_seq_num","ship_to_seq_num_sysid","sale_type_sysid","override_adf",
                             "adf","taxable","tax_code","taxcode_sysid","discount_amt","discount_date",
                             "cash_discounts","open_flag","payments_tendered"};

            Console.WriteLine("InvoiceTest - BTestDTttinvoice_header: Compare count");
            Assert.AreEqual(fields.Length, dsInvoice.ttinvoice_header.Columns.Count);

            for(int i = 0; i < dsInvoice.ttinvoice_header.Columns.Count; i++)
            {
                Console.WriteLine("InvoiceTest - BTestDTttinvoice_header: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsInvoice.ttinvoice_header.Columns[i].Caption);
            }
        }

        /// <summary>
        /// CTestDTttinvoice_detail.
        /// Asserts all field names and the order of the fields in the
        /// ttinvoice_detail table of the Invoice DataSet.
        /// </summary>
        [Test]
        public void CTestDTttinvoice_detail()
        {
            //ttinvoice_detail Table
            string[] fields = new string[]
                            {"system_id","so_id","so_id_sysid","ref_num","sequence","shipment_num","item_ptr",
                            "item_ptr_sysid","ITEM","DESCRIPTION","so_desc","SIZE","qty_ordered","display_seq",
                            "ord_qty_uom_conv_ptr_sysid","ordqty_uom_conv_ptr","cost","market_cost","cost_desig",
                            "disp_price_conv","disp_qty_conv","bo","qty","uom","shipqty_order_uom","shipqty_price_uom",
                            "price","price_uom_code","price_uom_ptr","price_uom_ptr_sysid","discount_1",
                            "discount_2","discount_3","extra_discount_1","extra_discount_2","extra_discount_3",
                             "extended_price","wo_phrase","shipments_detail_obj","shipments_header_obj",};

            Console.WriteLine("InvoiceTest - CTestDTttinvoice_detail: Compare count");
            Assert.AreEqual(fields.Length, dsInvoice.ttinvoice_detail.Columns.Count);

            for (int i = 0; i < dsInvoice.ttinvoice_detail.Columns.Count; i++)
            {
                Console.WriteLine("InvoiceTest - CTestDTttinvoice_detail: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsInvoice.ttinvoice_detail.Columns[i].Caption);
            }
        }

        /// <summary>
        /// DTestDTttinvoice_byitem.
        /// Asserts all field names and the order of the fields in the
        /// ttinvoice_byitem table of the Invoice DataSet.
        /// </summary>
        [Test]
        public void DTestDTttinvoice_byitem()
        {
            //ttinvoice_detail Table
            string[] fields = new string[]
                            {"branch_id","ref_num","sequence","ITEM","SIZE","DESCRIPTION","so_desc",
                            "qty_ordered","shipqty_order_uom","uom","price","price_uom_code",
                            "extended_price","job","reference","cust_po","order_date","ship_date",
                            "invoice_date","due_date","route_id_char","ship_via","sale_type",
                            "sale_type_desc","statusdescr","rep_1","rep_2","rep_3","shiptoname",
                            "shipToAddr1","shipToAddr2","shipToAddr3","shipToCity","shipToState",
                            "shipToZip","wo_phrase"};

            Console.WriteLine("InvoiceTest - DTestDTttinvoice_byitem: Compare count");
            Assert.AreEqual(fields.Length, dsInvoice.ttinvoice_byitem.Columns.Count);

            for (int i = 0; i < dsInvoice.ttinvoice_byitem.Columns.Count; i++)
            {
                Console.WriteLine("InvoiceTest - DTestDTttinvoice_byitem: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsInvoice.ttinvoice_byitem.Columns[i].Caption);
            }
        }

        /// <summary>
        /// ETestDTttmisc.
        /// Asserts all field names and the order of the fields in the
        /// ttmisc table of the Invoice DataSet.
        /// </summary>
        [Test]
        public void ETestDTttmisc()
        {
            //ttmisc Table
            Assert.AreEqual(1, dsInvoice.Tables["ttmisc"].Columns.Count);

            Assert.AreEqual("doc_storage_avail", dsInvoice.Tables["ttmisc"].Columns[0].Caption);
        }

        protected dsCustomerDataSet FetchdsCUforSIcallsDataSet(dsCustomerDataSet dsCustomer)
        {
            dsCustomer.ReadXml("dsCUforSIcalls.xml");
            return dsCustomer;
        }
    }
}
