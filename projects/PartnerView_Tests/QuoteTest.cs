using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class dQuoteTest : WebFormTestCase
    {
        Progress.Open4GL.Proxy.Connection oConnection;
        EntryNET oEntryNET;
        Session oSession;
        Quote oQuote;

        dsSessionMgrDataSet dsSessionMgr;
        dsCustomerDataSet dsCustomer;
        dsPartnerVuDataSet dsPV;
        dsPVParamDataSet dsPVParam;
        dsQuoteDataSet dsQuote;
        dsQuoteDataSet dsQuoteDetail;
        dsRetailAddressDataSet dsRetailAddress;

        int opiLoginAccepted;
        string opcMessages;
        string ipcSelectionCriteria;

        public dQuoteTest()
        {
        }

        /// <summary>
        /// ATestQuoteBL.
        /// Login mmcallister/mm.
        /// ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Quotes" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Quote ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=4/14/2006" + "\x0003" + "End Date=5/14/2007" + "\x0003" + "Show Results=Quote";
        /// Asserts all tables and table names in the Quote DataSet.
        /// Then fetches a single Quote detail and asserts
        /// the tables in the Quote Detail DataSet.
        /// </summary>
        [Test]
        public void ATestQuoteBL()
        {
            dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomer = new dsCustomerDataSet();
            dsPV = new dsPartnerVuDataSet();
            dsPVParam = new dsPVParamDataSet();
            dsQuote = new dsQuoteDataSet();
            dsRetailAddress = new dsRetailAddressDataSet();

            dsQuoteDetail = new dsQuoteDataSet();

            oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            oEntryNET = new EntryNET(oConnection);
            oSession = new Session(oEntryNET);
            oQuote = new Quote(oEntryNET);

            string opcContextId = "";

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out opcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Quotes" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Quote ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=4/14/2006" + "\x0003" + "End Date=5/14/2007" + "\x0003" + "Show Results=Quote";
            dsCustomer = FetchdsCUforSIcallsDataSet(dsCustomer);

            int ReturnCode;
            string MessageText;

            oQuote.siquotefetchlist(opcContextId, ipcSelectionCriteria, dsCustomer, out dsQuote, out dsRetailAddress, out ReturnCode, out MessageText);

            //Tests on returned dsQuote DataSet...
            Assert.IsNotNull(dsQuote);
            Assert.AreEqual(4, dsQuote.Tables.Count);
            Assert.AreEqual("pvttquote_header", dsQuote.Tables[0].TableName);
            Assert.AreEqual("pvttquote_detail", dsQuote.Tables[1].TableName);
            Assert.AreEqual("pvttquote_byitem", dsQuote.Tables[2].TableName);
            Assert.AreEqual("ttmisc", dsQuote.Tables[3].TableName);

            ipcSelectionCriteria = "OrderTotals=YES" + "\x0003" + "ExtendPrice=YES" + "\x0003" + "Type=QO" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "SearchType=Quote ID" + "\x0003" + "SearchTran=19262";
            oQuote.siquotefetchsingle(opcContextId, ipcSelectionCriteria, out dsQuoteDetail, out ReturnCode, out MessageText);

            //Tests on returned dsQuoteDetail DataSet...
            Assert.IsNotNull(dsQuoteDetail);
            Assert.AreEqual(4, dsQuoteDetail.Tables.Count);
            Assert.AreEqual("pvttquote_header", dsQuoteDetail.Tables[0].TableName);
            Assert.AreEqual("pvttquote_detail", dsQuoteDetail.Tables[1].TableName);
            Assert.AreEqual("pvttquote_byitem", dsQuote.Tables[2].TableName);
            Assert.AreEqual("ttmisc", dsQuoteDetail.Tables[3].TableName);

            Assert.AreEqual(1, dsQuoteDetail.Tables["pvttquote_header"].Rows.Count);
            Assert.AreEqual(1, dsQuoteDetail.Tables["pvttquote_detail"].Rows.Count);
            Assert.AreEqual(0, dsQuoteDetail.Tables["pvttquote_byitem"].Rows.Count);

            oQuote.Dispose();
            oSession.Dispose();
            oEntryNET.Dispose();
            oConnection.Dispose();
        }

        /// <summary>
        /// BTestDTttquote_header.
        /// Asserts all the fields and the order of the fields in the
        /// ttquote_header table of the the Quote DataSet.
        /// </summary>
        [Test]
        public void BTestDTttquote_header()
        {
            //ttquote_header Table
            string[] fields = new string[] 
                            {"branch_id","quote_id","job","reference","cust_po","quote_date",
                            "closed_date","expect_date","expect_date_override","route_id_char","ship_via","sale_type",
                            "sale_type_desc","statusdescr","quoted_for","quoted_by","freight_terms_id",
                            "pay_terms_code","order_subtotal","order_charges","order_taxes",
                            "order_total","rep_1","rep_2","rep_3","shiptoname","shipToAddr1",
                            "shipToAddr2","shipToAddr3","shipToCity","shipToState","shipToZip",
                            "TYPE","prof_name","system_id","sale_type_sysid","cust_key","cust_key_sysid",
                            "cust_code","shipto_seq_num","ship_to_seq_num_sysid","override_adf",
                            "adf","taxable","tax_code","taxcode_sysid","weight","load","orig_source","allow_release","ship_complete","quote_message"};

            Console.WriteLine("QuoteTest - BTestDTttquote_header: Compare count");
            Assert.AreEqual(fields.Length, dsQuote.pvttquote_header.Columns.Count);

            for (int i = 0; i < dsQuote.pvttquote_header.Columns.Count; i++)
            {
                Console.WriteLine("QuoteTest - BTestDTttquote_header: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsQuote.pvttquote_header.Columns[i].Caption);
            }
        }

        /// <summary>
        /// CTestDTttquote_detail.
        /// Asserts all the fields and the order of the fields in the
        /// ttquote_detail table of the the Quote DataSet.
        /// </summary>
        [Test]
        public void CTestDTttquote_detail()
        {
            //ttquote_detail Table
            string[] fields = new string[] 
                            {"display_seq","ITEM","SIZE","quote_desc","qty_ordered","staged_qty",
                            "uom","price","price_uom_code","extended_price","wo_phrase","system_id","quote_id",
                            "quote_id_sysid","sequence","ITEM_ptr_sysid","ITEM_ptr", "thickness", "width", "length" ,"DISP_price_conv",
                            "DISP_qty_conv","discount_1","discount_2","discount_3","extra_discount_1",
                            "extra_discount_2","extra_discount_3", "qty_released", "qty_to_release"};

            Console.WriteLine("QuoteTest - CTestDTttquote_detail: Compare count");
            Assert.AreEqual(fields.Length, dsQuote.pvttquote_detail.Columns.Count);

            for (int i = 0; i < dsQuote.pvttquote_detail.Columns.Count; i++)
            {
                Console.WriteLine("QuoteTest - CTestDTttquote_detail: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsQuote.pvttquote_detail.Columns[i].Caption);
            }
        }

        /// <summary>
        /// DTestDTttquote_byitem.
        /// Asserts all the fields and the order of the fields in the
        /// ttquote_byitem table of the the Quote DataSet.
        /// </summary>
        [Test]
        public void DTestDTttquote_byitem()
        {
            //ttquote_detail Table
            string[] fields = new string[] 
                            {"branch_id","quote_id","quote_id_sysid","job","ITEM_ptr_sysid","ITEM_ptr",
                            "ITEM","SIZE","quote_desc","qty_ordered","uom","staged_qty","price",
                            "price_uom_code","extended_price","reference","cust_po","closed_date",
                            "quote_date","route_id_char","ship_via","sale_type","sale_type_desc",
                            "statusdescr","rep_1","rep_2","rep_3","shiptoname","shipToAddr1",
                            "shipToAddr2","shipToAddr3","shipToCity","shipToState","shipToZip","wo_phrase"};

            Console.WriteLine("QuoteTest - DTestDTttquote_byitem: Compare count");
            Assert.AreEqual(fields.Length, dsQuote.pvttquote_byitem.Columns.Count);

            for (int i = 0; i < dsQuote.pvttquote_byitem.Columns.Count; i++)
            {
                Console.WriteLine("QuoteTest - DTestDTttquote_byitem: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsQuote.pvttquote_byitem.Columns[i].Caption);
            }
        }

        /// <summary>
        /// ETestDTttmisc.
        /// Asserts all the fields and the order of the fields in the
        /// ttmisc table of the the Quote DataSet.
        /// </summary>
        [Test]
        public void ETestDTttmisc()
        {
            //ttmisc Table
            Assert.AreEqual(1, dsQuote.Tables["ttmisc"].Columns.Count);

            Assert.AreEqual("doc_storage_avail", dsQuote.Tables["ttmisc"].Columns[0].Caption);
        }

        protected dsCustomerDataSet FetchdsCUforSIcallsDataSet(dsCustomerDataSet dsCustomer)
        {
            dsCustomer.ReadXml("dsCUforSIcalls.xml");
            return dsCustomer;
        }
    }
}
