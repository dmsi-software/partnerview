using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;
using System.IO;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class cCreditMemoTest : WebFormTestCase
    {
        Progress.Open4GL.Proxy.Connection oConnection;
        EntryNET oEntryNET;
        Session oSession;
        SalesOrders oSalesOrders;

        dsSessionMgrDataSet dsSessionMgr;
        dsCustomerDataSet dsCustomer;
        dsPartnerVuDataSet dsPV;
        dsPVParamDataSet dsPVParam;
        dsSalesOrderDataSet dsSalesOrder;
        dsSalesOrderDataSet dsSalesOrderDetail;

        int opiLoginAccepted;
        string opcMessages;
        string ipcSelectionCriteria;

        public cCreditMemoTest()
        {
        }

        /// <summary>
        /// ATestCreditMemoBL.
        /// Logins in as mmcallister/mm user.
        /// Requests Credit Memo DataSet and asserts all the tables
        /// and that 1 header row and 1 detail row were returned.
        /// ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Credit Memos" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Credit Memo ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=4/11/2006" + "\x0003" + "End Date=5/11/2007" + "\x0003" + "Show Results=Credit Memo";
        /// </summary>
        [Test]
        public void ATestCreditMemoBL()
        {
            dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomer = new dsCustomerDataSet();
            dsPV = new dsPartnerVuDataSet();
            dsPVParam = new dsPVParamDataSet();
            dsSalesOrder = new dsSalesOrderDataSet();

            oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            oEntryNET = new EntryNET(oConnection);
            oSession = new Session(oEntryNET);
            oSalesOrders = new SalesOrders(oEntryNET);

            string ipcContextId = "";

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out ipcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Credit Memos" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Credit Memo ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=4/11/2006" + "\x0003" + "End Date=5/11/2007" + "\x0003" + "Show Results=Credit Memo";
            dsCustomer = FetchdsCUforSIcallsDataSet(dsCustomer);

            int ReturnCode = 0;
            string MessageText = "";
            oSalesOrders.sisalesorderfetchlist(ipcContextId, ipcSelectionCriteria, dsCustomer, out dsSalesOrder, out ReturnCode, out MessageText);

            //Tests on returned dsSalesOrder DataSet...
            Assert.IsNotNull(dsSalesOrder);
            Assert.AreEqual(4, dsSalesOrder.Tables.Count);
            Assert.AreEqual("ttso_header", dsSalesOrder.Tables[0].TableName);
            Assert.AreEqual("ttso_detail", dsSalesOrder.Tables[1].TableName);
            Assert.AreEqual("ttso_byitem", dsSalesOrder.Tables[2].TableName);
            Assert.AreEqual("ttmisc", dsSalesOrder.Tables[3].TableName);

            ipcSelectionCriteria = "Type=Credit Memos" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "SearchType=Credit Memo ID" + "\x0003" + "SearchTran=18729";

            oSalesOrders.sisalesorderfetchsingle(ipcContextId, ipcSelectionCriteria, out dsSalesOrderDetail, out ReturnCode, out MessageText);

            //Tests on returned dsSalesOrderDetail DataSet...
            Assert.IsNotNull(dsSalesOrderDetail);
            Assert.AreEqual(4, dsSalesOrderDetail.Tables.Count);
            Assert.AreEqual("ttso_header", dsSalesOrderDetail.Tables[0].TableName);
            Assert.AreEqual("ttso_detail", dsSalesOrderDetail.Tables[1].TableName);
            Assert.AreEqual("ttso_byitem", dsSalesOrderDetail.Tables[2].TableName);
            Assert.AreEqual("ttmisc", dsSalesOrderDetail.Tables[3].TableName);

            Assert.AreEqual(1, dsSalesOrderDetail.Tables["ttso_header"].Rows.Count);
            Assert.AreEqual(1, dsSalesOrderDetail.Tables["ttso_detail"].Rows.Count);
            Assert.AreEqual(0, dsSalesOrderDetail.Tables["ttso_byitem"].Rows.Count);

            oSalesOrders.Dispose();
            oSession.Dispose();
            oEntryNET.Dispose();
            oConnection.Dispose();
        }

        /// <summary>
        /// BTestDTttso_header.
        /// Asserts all the field names and order of the fields in the
        /// ttso_header table.
        /// </summary>
        [Test]
        public void BTestDTttso_header()
        {
            //ttso_header Table
            string[] fields = new string[] 
                            {"branch_id","so_id","cm_invoice_ref","job","reference","cust_po","ordered_by",
                             "order_date","expect_date","route_id_char","ship_via","sale_type","sale_type_desc",
                             "statusdescr","freight_terms_id","pay_terms_code","order_subtotal",
                             "order_charges","order_taxes","order_total","rep_1","rep_2","rep_3",
                             "shiptoname","shipToAddr1","shipToAddr2","shipToAddr3","shipToCity",
                             "shipToState","shipToZip","prof_name","system_id","TYPE","sale_type_sysid",
                             "so_status","so_status_type","credit_queue_status","price_hold","cust_key",
                             "cust_key_sysid","cust_code","shipto_seq_num","ship_to_seq_num_sysid",
                             "override_adf","adf","taxable","tax_code","taxcode_sysid","weight","load","so_header_obj",
                             "user_email","user_email2","user_fax","order_message","ship_complete"};

            Console.WriteLine("CreditMemoTest - BTestDTttso_header: Compare count");
            Assert.AreEqual(fields.Length, dsSalesOrderDetail.ttso_header.Columns.Count);

            for (int i = 0; i < dsSalesOrderDetail.ttso_header.Columns.Count; i++)
            {
                Console.WriteLine("CreditMemoTest - BTestDTttso_header: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsSalesOrderDetail.ttso_header.Columns[i].Caption);
            }
        }

        /// <summary>
        /// CTestDTttso_detail.
        /// Asserts all the field names and order of the fields in the
        /// ttso_detail table.
        /// </summary>
        [Test]
        public void CTestDTttso_detail()
        {
            Assert.AreEqual(34, dsSalesOrderDetail.Tables["ttso_detail"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttSODetail.txt"))
            //{
            //    fileWriter = File.CreateText("ttSODetail.txt");
            //}
            //for (int x = 0; x < 34; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsSalesOrderDetail.Tables["ttso_detail"].Columns[x].ColumnName + "\", dsSalesOrderDetail.Tables[\"ttso_detail\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("display_seq", dsSalesOrderDetail.Tables["ttso_detail"].Columns[0].ColumnName);
            Assert.AreEqual("ITEM", dsSalesOrderDetail.Tables["ttso_detail"].Columns[1].ColumnName);
            Assert.AreEqual("SIZE", dsSalesOrderDetail.Tables["ttso_detail"].Columns[2].ColumnName);
            Assert.AreEqual("so_desc", dsSalesOrderDetail.Tables["ttso_detail"].Columns[3].ColumnName);
            Assert.AreEqual("qty_ordered", dsSalesOrderDetail.Tables["ttso_detail"].Columns[4].ColumnName);
            Assert.AreEqual("qty_staged", dsSalesOrderDetail.Tables["ttso_detail"].Columns[5].ColumnName);
            Assert.AreEqual("qty_backordered", dsSalesOrderDetail.Tables["ttso_detail"].Columns[6].ColumnName);
            Assert.AreEqual("uom", dsSalesOrderDetail.Tables["ttso_detail"].Columns[7].ColumnName);
            Assert.AreEqual("price", dsSalesOrderDetail.Tables["ttso_detail"].Columns[8].ColumnName);
            Assert.AreEqual("price_uom_code", dsSalesOrderDetail.Tables["ttso_detail"].Columns[9].ColumnName);
            Assert.AreEqual("extended_price", dsSalesOrderDetail.Tables["ttso_detail"].Columns[10].ColumnName);
            Assert.AreEqual("wo_phrase", dsSalesOrderDetail.Tables["ttso_detail"].Columns[11].ColumnName);
            Assert.AreEqual("system_id", dsSalesOrderDetail.Tables["ttso_detail"].Columns[12].ColumnName);
            Assert.AreEqual("so_id", dsSalesOrderDetail.Tables["ttso_detail"].Columns[13].ColumnName);
            Assert.AreEqual("so_id_sysid", dsSalesOrderDetail.Tables["ttso_detail"].Columns[14].ColumnName);
            Assert.AreEqual("sequence", dsSalesOrderDetail.Tables["ttso_detail"].Columns[15].ColumnName);
            Assert.AreEqual("ITEM_ptr_sysid", dsSalesOrderDetail.Tables["ttso_detail"].Columns[16].ColumnName);
            Assert.AreEqual("ITEM_ptr", dsSalesOrderDetail.Tables["ttso_detail"].Columns[17].ColumnName);
            Assert.AreEqual("thickness", dsSalesOrderDetail.Tables["ttso_detail"].Columns[18].ColumnName);
            Assert.AreEqual("WIDTH", dsSalesOrderDetail.Tables["ttso_detail"].Columns[19].ColumnName);
            Assert.AreEqual("LENGTH", dsSalesOrderDetail.Tables["ttso_detail"].Columns[20].ColumnName);
            Assert.AreEqual("DISP_price_conv", dsSalesOrderDetail.Tables["ttso_detail"].Columns[21].ColumnName);
            Assert.AreEqual("DISP_qty_conv", dsSalesOrderDetail.Tables["ttso_detail"].Columns[22].ColumnName);
            Assert.AreEqual("ord_qty_uom_conv_ptr_sysid", dsSalesOrderDetail.Tables["ttso_detail"].Columns[23].ColumnName);
            Assert.AreEqual("ordqty_uom_conv_ptr", dsSalesOrderDetail.Tables["ttso_detail"].Columns[24].ColumnName);
            Assert.AreEqual("discount_1", dsSalesOrderDetail.Tables["ttso_detail"].Columns[25].ColumnName);
            Assert.AreEqual("discount_2", dsSalesOrderDetail.Tables["ttso_detail"].Columns[26].ColumnName);
            Assert.AreEqual("discount_3", dsSalesOrderDetail.Tables["ttso_detail"].Columns[27].ColumnName);
            Assert.AreEqual("extra_discount_1", dsSalesOrderDetail.Tables["ttso_detail"].Columns[28].ColumnName);
            Assert.AreEqual("extra_discount_2", dsSalesOrderDetail.Tables["ttso_detail"].Columns[29].ColumnName);
            Assert.AreEqual("extra_discount_3", dsSalesOrderDetail.Tables["ttso_detail"].Columns[30].ColumnName);
            Assert.AreEqual("line_message", dsSalesOrderDetail.Tables["ttso_detail"].Columns[31].ColumnName);
            Assert.AreEqual("header_charge", dsSalesOrderDetail.Tables["ttso_detail"].Columns[32].ColumnName);
            Assert.AreEqual("override_price", dsSalesOrderDetail.Tables["ttso_detail"].Columns[33].ColumnName);
        }

        /// <summary>
        /// DTestDTttso_byitem.
        /// Asserts all the field names and order of the fields in the
        /// ttso_detail table when using the By Item view.
        /// </summary>
        [Test]
        public void DTestDTttso_byitem()
        {
            //ttso_detail Table
            string[] fields = new string[] 
                            {"branch_id","so_id","sequence","job","ITEM","SIZE","so_desc",
                            "qty_ordered","uom","qty_staged","qty_backordered","price",
                            "price_uom_code","extended_price","reference","cust_po",
                            "order_date","expect_date","route_id_char","ship_via","sale_type",
                            "sale_type_desc","statusdescr","rep_1","rep_2","rep_3","shiptoname",
                            "shipToAddr1","shipToAddr2","shipToAddr3","shipToCity","shipToState","shipToZip","wo_phrase"};

            Console.WriteLine("CreditMemoTest - DTestDTttso_byitem: Compare count");
            Assert.AreEqual(fields.Length, dsSalesOrderDetail.ttso_byitem.Columns.Count);

            for (int i = 0; i < dsSalesOrderDetail.ttso_byitem.Columns.Count; i++)
            {
                Console.WriteLine("CreditMemoTest - DTestDTttso_byitem: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsSalesOrderDetail.ttso_byitem.Columns[i].Caption);
            }
        }

        /// <summary>
        /// ETestDTttmisc.
        /// Asserts all the field names and order of the fields in the
        /// ttmisc table. Looks for the doc_storage_avail field.
        /// </summary>
        [Test]
        public void ETestDTttmisc()
        {
            //ttmisc Table
            Assert.AreEqual(1, dsSalesOrder.Tables["ttmisc"].Columns.Count);

            Assert.AreEqual("doc_storage_avail", dsSalesOrder.Tables["ttmisc"].Columns[0].Caption);
        }

        protected dsCustomerDataSet FetchdsCUforSIcallsDataSet(dsCustomerDataSet dsCustomer)
        {
            dsCustomer.ReadXml("dsCUforSIcalls.xml");
            return dsCustomer;
        }
    }
}
