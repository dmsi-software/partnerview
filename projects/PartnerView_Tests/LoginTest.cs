using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;
using System.Threading;
using System.IO;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class aLoginTest : WebFormTestCase
    {
        Progress.Open4GL.Proxy.Connection _oConnection;
        EntryNET _oEntryNET;
        Session _oSession;

        dsSessionMgrDataSet _dsSessionMgr;
        dsCustomerDataSet _dsCustomer;
        dsPartnerVuDataSet _dsPV;
        dsPVParamDataSet _dsPVParam;
        DataSet _ds;

        int _opiLoginAccepted;
        string _opcMessages;

        private void IsLocatorMode(string title)
        {
            // Dummy test widgets for TabPanel
            LabelTester tpCustomer = new LabelTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel", CurrentWebForm);
            LabelTester tpSalesOrder = new LabelTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_SalesOrderPanel", CurrentWebForm);
            LabelTester tpCreditMemo = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_CreditMemoPanel", CurrentWebForm);
            LabelTester tpQuote = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_QuotePanel", CurrentWebForm);
            LabelTester tpInvoice = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_InvoicePanel", CurrentWebForm);
            LabelTester tpOrderForm = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_OrderFormPanel", CurrentWebForm);

            Console.WriteLine(title + "- Checking tpCustomer's visibility.");
            Assert.IsNotNull(tpCustomer, "tpCustomer is null");
            AssertVisibility(tpCustomer, true);
            Assert.IsTrue(tpCustomer.Enabled, "tpCustomer is disabled.");
            // TODO: Check for active tab when TabPanelTester is finally available

            Console.WriteLine(title + "- Checking if the other tabpanels are hidden.");
            //Assert.That(tpSalesOrder.Enabled == true);
            //Assert.That(tpCreditMemo.Enabled == true);
            //Assert.That(tpQuote.Enabled == true);
            //Assert.That(tpInvoice.Enabled == true);
            //Assert.That(tpOrderForm.Enabled == true);
            AssertVisibility(tpSalesOrder, true);
            AssertVisibility(tpCreditMemo, false);
            AssertVisibility(tpQuote, false);
            AssertVisibility(tpInvoice, false);
            AssertVisibility(tpOrderForm, false);
        }

        private void NoCustomerTabMode(string title)
        {
            // Dummy test widgets for TabPanel
            LabelTester tpCustomer = new LabelTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel", CurrentWebForm);
            LabelTester tpSalesOrder = new LabelTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_SalesOrderPanel", CurrentWebForm);
            LabelTester tpCreditMemo = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_CreditMemoPanel", CurrentWebForm);
            LabelTester tpQuote = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_QuotePanel", CurrentWebForm);
            LabelTester tpInvoice = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_InvoicePanel", CurrentWebForm);
            LabelTester tpOrderForm = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_OrderFormPanel", CurrentWebForm);

            Console.WriteLine(title + "- Checking tpCustomer's invisibility.");
            Assert.IsNotNull(tpCustomer, "tpCustomer is null");
            AssertVisibility(tpCustomer, false);

            Console.WriteLine(title + "- Checking if the other tabpanels are visible.");
            AssertVisibility(tpSalesOrder, true);
            AssertVisibility(tpCreditMemo, true);
            AssertVisibility(tpQuote, true);
            AssertVisibility(tpInvoice, true);
            AssertVisibility(tpOrderForm, true);
            
        }

        public aLoginTest()
        {

        }

        /// <summary>
        /// ATestLoginUIa.
        /// TestPV001/001PVTest login.
        /// Asserts visibility of 13 controls; then clicks the login button.
        /// After login, asserts the visibility of the welcome header.
        /// </summary>
        [Test]
        public void ATestLoginUIa()
        {
            //Browser.GetPage(Dmsi.Agility.EntryNET.Properties.Settings.Default.PVurl);

            //TextBoxTester username = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtUserName", CurrentWebForm);
            //TextBoxTester password = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtPassword", CurrentWebForm);

            //ButtonTester btnLogin = new ButtonTester("ctl00_cphMaster1_LogOn1_btnLogin", CurrentWebForm);
            //ButtonTester btnReset = new ButtonTester("ctl00_cphMaster1_LogOn1_btnReset", CurrentWebForm);
            //ButtonTester btnYes40 = new ButtonTester("ctl00_cphMaster1_LogOn1_btnYes40", CurrentWebForm);
            //ButtonTester btnNo40 = new ButtonTester("ctl00_cphMaster1_LogOn1_btnNo40", CurrentWebForm);
            //ButtonTester btnYes = new ButtonTester("ctl00_cphMaster1_LogOn1_btnYes", CurrentWebForm);
            //ButtonTester btnNo = new ButtonTester("ctl00_cphMaster1_LogOn1_btnNo", CurrentWebForm);
            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);

            ////LabelTester lblLoginAssistance = new LabelTester("ctl00_cphMaster1_LogOn1_lblLoginAssistance", CurrentWebForm);
            //LabelTester lblExtraMessage = new LabelTester("ctl00_cphMaster1_LogOn1_lblExtraMessage", CurrentWebForm);
            //LabelTester lblUserName = new LabelTester("ctl00_cphMaster1_LogOn1_lblUserName", CurrentWebForm);
            //LabelTester lblUser = new LabelTester("ctl00_cphMaster1_LogOn1_lblUser", CurrentWebForm);
            //LabelTester lblPassword = new LabelTester("ctl00_cphMaster1_LogOn1_lblPassword", CurrentWebForm);
            //LabelTester lblPass = new LabelTester("ctl00_cphMaster1_LogOn1_lblPass", CurrentWebForm);

            ////TextBoxes
            //AssertVisibility(username, true);
            //AssertVisibility(password, true);

            ////Buttons
            //AssertVisibility(btnLogin, true);
            //AssertVisibility(btnReset, true);
            //AssertVisibility(btnYes40, false);
            //AssertVisibility(btnNo40, false);
            //AssertVisibility(btnYes, false);
            //AssertVisibility(btnNo, false);

            ////Labels
            ////AssertVisibility(lblLoginAssistance, true);
            //AssertVisibility(lblExtraMessage, true);
            //AssertVisibility(lblUserName, true);
            //AssertVisibility(lblUser, true);
            //AssertVisibility(lblPassword, true);
            //AssertVisibility(lblPass, true);

            //username.Text = "TestPV001";
            //password.Text = "001PVTest";
            //btnLogin.Click();

            ////TODO: Change to divs/labels maybe...
            ////ButtonTester btnLogOut = new ButtonTester("ctl00_btnLogOut", CurrentWebForm);
            ////AssertVisibility(btnLogOut, true);
            ////Assert.AreEqual("Log Out", btnLogOut.Text);

            ////ButtonTester btnChangePassword = new ButtonTester("ctl00_btnChangePassword", CurrentWebForm);
            ////AssertVisibility(btnChangePassword, true);
            ////Assert.AreEqual("Change Password", btnChangePassword.Text);

            ////ButtonTester btnHelp = new ButtonTester("ctl00_btnHelp", CurrentWebForm);
            ////AssertVisibility(btnHelp, true);
            ////Assert.AreEqual("Help", btnHelp.Text);

            //LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            //AssertVisibility(lblWelcomeHeader, true);
            //string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            //Assert.AreEqual("Welcome ", searchStr);

            ////ButtonTester btnFind = new ButtonTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_btnFind", CurrentWebForm);
            ////AssertVisibility(btnFind, true);
            ////Assert.AreEqual("Go", btnFind.Text);
            ////btnFind.Click();

            ////DataGridTester grid = new DataGridTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_gvCustomers", CurrentWebForm);
            ////AssertVisibility(grid, true);

            ////////Header Row is failing...assumed to be a change in implementation by Microsoft that
            ////////makes NUnitAsp 1.5 DataGrid Tester obsolete...
            //////DataGridTester.Row headerRow = grid.GetHeaderRow();
            //////string[] headerCells = headerRow.TrimmedCells;
            //////Assert.AreEqual(11, headerCells.Length);

            ////DataGridTester.Row oRow = grid.GetRow(0);
            ////string[] rowCells = oRow.TrimmedCells;
            ////Assert.AreEqual("Select", rowCells[0]);

            //////Test for lblLoginAssistance Text
            ////bool test = false;
            ////if (lblLoginAssistance.Text.ToString().Contains("Click Here for Login Assistance"))
            ////{
            ////    test = true;
            ////}
            ////Assert.AreEqual(true, test);

            //////Attempt sequential login tests...
            //////*********************************
            ////btnLogOut.Click();

            ////////Inspect TimedOut.aspx page returned.
            //////LinkButtonTester lnkButton = new LinkButtonTester("lbtnGoToLogin", CurrentWebForm);
            //////AssertVisibility(lnkButton, true);
            //////lnkButton.Click();

            //////Thread.Sleep(5000);

            ////TextBoxTester username2 = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtUserName", CurrentWebForm);
            ////TextBoxTester password2 = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtPassword", CurrentWebForm);
            ////ButtonTester btnLogOn = new ButtonTester("ctl00_cphMaster1_LogOn1_btnLogin", CurrentWebForm);

            ////username2.Text = "badLogin";
            ////password2.Text = "badLogin";
            ////btnLogOn.Click();

            ////LabelTester lblMessage = new LabelTester("ctl00_cphMaster1_LogOn1_lblExtraMessage", CurrentWebForm);
            ////AssertVisibility(lblMessage, true);
            //////int foundLocation = lblMessage.Text.IndexOf("Invalid User Name or Password.");
            //////bool found = false;
            //////if (foundLocation != -1)
            //////{
            //////    found = true;
            //////}
            //////Assert.AreEqual(true, found);
            ////Assert.AreEqual(0, lblMessage.Text.IndexOf("Invalid User Name or Password."));

            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUIb.
        /// badLogin/badLogin login.
        /// Asserts return of exact message "Invalid User Name or Password".
        /// </summary>
        [Test]
        public void ATestLoginUIb()
        {
            //GetPvPage("badLogin", "badLogin");

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblExtraMessage = new LabelTester("ctl00_cphMaster1_LogOn1_lblExtraMessage", CurrentWebForm);
            //AssertVisibility(lblExtraMessage, true);
            //Assert.AreEqual(18, lblExtraMessage.Text.IndexOf("Invalid User Name or Password."));
            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI01.
        /// TestPV001/001PVTest login.
        /// Asserts valid login and then asserts visibility of Welcome header
        /// and visibility of proper tabs secured by back-end security logic.
        /// 1 Customer, Default Customer, Multiple Shiptos
        /// </summary>
        [Test]
        public void ATestLoginUI01()
        {
            //GetPvPage("TestPV001", "001PVTest");

            ////TODO: Change to divs/labels maybe...
            ////ButtonTester btnLogOut = new ButtonTester("ctl00_btnLogOut", CurrentWebForm);
            ////AssertVisibility(btnLogOut, true);
            ////Assert.AreEqual("Log Out", btnLogOut.Text);

            ////ButtonTester btnChangePassword = new ButtonTester("ctl00_btnChangePassword", CurrentWebForm);
            ////AssertVisibility(btnChangePassword, true);
            ////Assert.AreEqual("Change Password", btnChangePassword.Text);

            ////ButtonTester btnHelp = new ButtonTester("ctl00_btnHelp", CurrentWebForm);
            ////AssertVisibility(btnHelp, true);
            ////Assert.AreEqual("Help", btnHelp.Text);

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            //AssertVisibility(lblWelcomeHeader, true);
            //string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            //Assert.AreEqual("Welcome ", searchStr);

            //this.IsLocatorMode("LoginTest - ATestLoginUI01");
            //// TestPV001 has only one customer and one shipto
            ////ButtonTester btnFind = new ButtonTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_btnFind", CurrentWebForm);
            ////AssertVisibility(btnFind, true);
            ////Assert.AreEqual("Go", btnFind.Text);
            ////btnFind.Click();

            ////DataGridTester grid = new DataGridTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_gvCustomers", CurrentWebForm);
            ////AssertVisibility(grid, true);

            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI02.
        /// TestPV002/002PVTest login.
        /// Asserts valid login and then asserts visibility of Welcome header
        /// and visibility of proper tabs secured by back-end security logic.
        /// 1 Customer, Multiple Ship-tos
        /// </summary>
        [Test]
        public void ATestLoginUI02()
        {
            //GetPvPage("TestPV002", "002PVTest");

            ////TODO: Change to divs/labels maybe...
            ////ButtonTester btnLogOut = new ButtonTester("ctl00_btnLogOut", CurrentWebForm);
            ////AssertVisibility(btnLogOut, true);
            ////Assert.AreEqual("Log Out", btnLogOut.Text);

            ////ButtonTester btnChangePassword = new ButtonTester("ctl00_btnChangePassword", CurrentWebForm);
            ////AssertVisibility(btnChangePassword, true);
            ////Assert.AreEqual("Change Password", btnChangePassword.Text);

            ////ButtonTester btnHelp = new ButtonTester("ctl00_btnHelp", CurrentWebForm);
            ////AssertVisibility(btnHelp, true);
            ////Assert.AreEqual("Help", btnHelp.Text);

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            //AssertVisibility(lblWelcomeHeader, true);
            //string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            //Assert.AreEqual("Welcome ", searchStr);

            //this.IsLocatorMode("LoginTest - ATestLoginUI02");
            ////ButtonTester btnFind = new ButtonTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_btnFind", CurrentWebForm);
            ////AssertVisibility(btnFind, true);
            ////Assert.AreEqual("Go", btnFind.Text);
            ////btnFind.Click();

            ////DataGridTester grid = new DataGridTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_gvCustomers", CurrentWebForm);
            ////AssertVisibility(grid, true);

            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI03.
        /// TestPV003/003PVTest login.
        /// Asserts login returns "Invalid User Name or Password" message.
        /// </summary>
        [Test]
        public void ATestLoginUI03()
        {
            //GetPvPage("TestPV003", "003PVTest");

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblExtraMessage = new LabelTester("ctl00_cphMaster1_LogOn1_lblExtraMessage", CurrentWebForm);
            //AssertVisibility(lblExtraMessage, true);
            //Assert.AreEqual(18, lblExtraMessage.Text.IndexOf("Invalid User Name or Password."));
            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI04.
        /// TestPV004/004PVTest login.
        /// Asserts login is valid; asserts Welcome header; asserts existence
        /// of 'Go' button; clicks Go button.
        /// Finally asserts existence of customer data grid.
        /// </summary>
        [Test]
        public void ATestLoginUI04()
        {
            //GetPvPage("TestPV004", "004PVTest");

            ////TODO: Change to divs/labels maybe...
            ////ButtonTester btnLogOut = new ButtonTester("ctl00_btnLogOut", CurrentWebForm);
            ////AssertVisibility(btnLogOut, true);
            ////Assert.AreEqual("Log Out", btnLogOut.Text);

            ////ButtonTester btnChangePassword = new ButtonTester("ctl00_btnChangePassword", CurrentWebForm);
            ////AssertVisibility(btnChangePassword, true);
            ////Assert.AreEqual("Change Password", btnChangePassword.Text);

            ////ButtonTester btnHelp = new ButtonTester("ctl00_btnHelp", CurrentWebForm);
            ////AssertVisibility(btnHelp, true);
            ////Assert.AreEqual("Help", btnHelp.Text);

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            //AssertVisibility(lblWelcomeHeader, true);
            //string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            //Assert.AreEqual("Welcome ", searchStr);

            //ButtonTester btnFind = new ButtonTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_btnFind", CurrentWebForm);
            //AssertVisibility(btnFind, true);
            //Assert.AreEqual("Go", btnFind.Text);
            //btnFind.Click();

            //DataGridTester grid = new DataGridTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_gvCustomers", CurrentWebForm);
            //AssertVisibility(grid, true);
            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI05.
        /// TestPV005/005PVTest login.
        /// Asserts login is valid; asserts Welcome header; asserts existence
        /// of 'Go' button; clicks Go button.
        /// Finally asserts existence of customer data grid.
        /// </summary>
        [Test]
        public void ATestLoginUI05()
        {
            //GetPvPage("TestPV005", "005PVTest");

            ////TODO: Change to divs/labels maybe...
            ////ButtonTester btnLogOut = new ButtonTester("ctl00_btnLogOut", CurrentWebForm);
            ////AssertVisibility(btnLogOut, true);
            ////Assert.AreEqual("Log Out", btnLogOut.Text);

            ////ButtonTester btnChangePassword = new ButtonTester("ctl00_btnChangePassword", CurrentWebForm);
            ////AssertVisibility(btnChangePassword, true);
            ////Assert.AreEqual("Change Password", btnChangePassword.Text);

            ////ButtonTester btnHelp = new ButtonTester("ctl00_btnHelp", CurrentWebForm);
            ////AssertVisibility(btnHelp, true);
            ////Assert.AreEqual("Help", btnHelp.Text);
            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            //AssertVisibility(lblWelcomeHeader, true);
            //string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            //Assert.AreEqual("Welcome ", searchStr);

            //ButtonTester btnFind = new ButtonTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_btnFind", CurrentWebForm);
            //AssertVisibility(btnFind, true);
            //Assert.AreEqual("Go", btnFind.Text);
            //btnFind.Click();

            //DataGridTester grid = new DataGridTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_gvCustomers", CurrentWebForm);
            //AssertVisibility(grid, true);
            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI06.
        /// TestPV006/006PVTest login.
        /// Asserts login returns exact message "Invalid user login. No Customer access."
        /// </summary>
        [Test]
        public void ATestLoginUI06()
        {
            //GetPvPage("TestPV006", "006PVTest");

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblExtraMessage = new LabelTester("ctl00_cphMaster1_LogOn1_lblExtraMessage", CurrentWebForm);
            //AssertVisibility(lblExtraMessage, true);
            //Assert.AreEqual(18, lblExtraMessage.Text.IndexOf("Invalid user login. No Customer access."));
            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI07.
        /// TestPV007/007PVTest login.
        /// Asserts login is valid; asserts Welcome header; asserts existence
        /// of 'Go' button; clicks Go button.
        /// Finally asserts existence/visibility of customer data grid.
        /// </summary>
        [Test]
        public void ATestLoginUI07()
        {
            //GetPvPage("TestPV007", "007PVTest");

            ////TODO: Change to divs/labels maybe...
            ////ButtonTester btnLogOut = new ButtonTester("ctl00_btnLogOut", CurrentWebForm);
            ////AssertVisibility(btnLogOut, true);
            ////Assert.AreEqual("Log Out", btnLogOut.Text);

            ////ButtonTester btnChangePassword = new ButtonTester("ctl00_btnChangePassword", CurrentWebForm);
            ////AssertVisibility(btnChangePassword, true);
            ////Assert.AreEqual("Change Password", btnChangePassword.Text);

            ////ButtonTester btnHelp = new ButtonTester("ctl00_btnHelp", CurrentWebForm);
            ////AssertVisibility(btnHelp, true);
            ////Assert.AreEqual("Help", btnHelp.Text);
            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            //AssertVisibility(lblWelcomeHeader, true);
            //string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            //Assert.AreEqual("Welcome ", searchStr);

            //ButtonTester btnFind = new ButtonTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_btnFind", CurrentWebForm);
            //AssertVisibility(btnFind, true);
            //Assert.AreEqual("Go", btnFind.Text);
            //btnFind.Click();

            //DataGridTester grid = new DataGridTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_gvCustomers", CurrentWebForm);
            //AssertVisibility(grid, true);
            //btnLogOut.Click();
        }


        /// <summary>
        /// ATestLoginUI08.
        /// TestPV008/008PVTest login.
        /// Asserts login returns exact message "Login Failed. Your user account has been disabled."
        /// </summary>
        [Test]
        public void ATestLoginUI08()
        {
            //GetPvPage("TestPV008", "008PVTest");

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblExtraMessage = new LabelTester("ctl00_cphMaster1_LogOn1_lblExtraMessage", CurrentWebForm);
            //AssertVisibility(lblExtraMessage, true);
            //Assert.AreEqual(18, lblExtraMessage.Text.IndexOf("Login Failed. Your user account has been disabled."));
            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI09.
        /// TestPV009/009PVTest login.
        /// Asserts login returns the page and controls for changing password.
        /// </summary>
        [Test]
        public void ATestLoginUI09()
        {
            //GetPvPage("TestPV009", "009PVTest");

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);

            //TextBoxTester txtCurrentPassword = new TextBoxTester("ctl00_cphMaster1_PasswordMaint1_txtOldPassword", CurrentWebForm);
            //AssertVisibility(txtCurrentPassword, true);

            //TextBoxTester txtNewPassword = new TextBoxTester("ctl00_cphMaster1_PasswordMaint1_txtNewPassword", CurrentWebForm);
            //AssertVisibility(txtNewPassword, true);

            //TextBoxTester txtConfirmNewPassword = new TextBoxTester("ctl00_cphMaster1_PasswordMaint1_txtConfirmNew", CurrentWebForm);
            //AssertVisibility(txtConfirmNewPassword, true);

            //btnLogOut.Click();
        }

        /// <summary>
        /// ATestLoginUI10.
        /// TestPV010/010PVTest login.
        /// Asserts login is successful and checks for Welcome header.
        /// One customer, default customer, 1 ship-to
        /// </summary>
        [Test]
        public void ATestLoginUI10()
        {
            //GetPvPage("TestPV010", "010PVTest");

            ////TODO: Change to divs/labels maybe...
            ////ButtonTester btnLogOut = new ButtonTester("ctl00_btnLogOut", CurrentWebForm);
            ////AssertVisibility(btnLogOut, true);
            ////Assert.AreEqual("Log Out", btnLogOut.Text);

            ////ButtonTester btnChangePassword = new ButtonTester("ctl00_btnChangePassword", CurrentWebForm);
            ////AssertVisibility(btnChangePassword, true);
            ////Assert.AreEqual("Change Password", btnChangePassword.Text);

            ////ButtonTester btnHelp = new ButtonTester("ctl00_btnHelp", CurrentWebForm);
            ////AssertVisibility(btnHelp, true);
            ////Assert.AreEqual("Help", btnHelp.Text);

            //ButtonTester btnLogOut = new ButtonTester("ctl00_LogOutButton", CurrentWebForm);
            //LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            //AssertVisibility(lblWelcomeHeader, true);
            //string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            //Assert.AreEqual("Welcome ", searchStr);

            //// NOTE: NUnit is returning inconsistent tab visibility values
            //// this.NoCustomerTabMode("LoginTest - ATestLoginUI10");
            
            ////ButtonTester btnFind = new ButtonTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_btnFind", CurrentWebForm);
            ////AssertVisibility(btnFind, true);
            ////Assert.AreEqual("Go", btnFind.Text);
            ////btnFind.Click();

            ////DataGridTester grid = new DataGridTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel_CustomerTabPanel1_gvCustomers", CurrentWebForm);
            ////AssertVisibility(grid, true);
            //btnLogOut.Click();
        }

        public void GetPvPage(string usernameInput, string passwordInput)
        {
            TextBoxTester username = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtUserName", CurrentWebForm);
            TextBoxTester password = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtPassword", CurrentWebForm);
            ButtonTester login = new ButtonTester("ctl00_cphMaster1_LogOn1_btnLogin", CurrentWebForm);

            Browser.GetPage(Dmsi.Agility.EntryNET.Properties.Settings.Default.PVurl);
            
            //TextBoxes
            AssertVisibility(username, true);
            AssertVisibility(password, true);

            //Buttons
            AssertVisibility(login, true);

            username.Text = usernameInput;
            password.Text = passwordInput;
            login.Click();
        }

        /// <summary>
        /// BTestLoginBLa.
        /// TestPV001/001PVTest login.
        /// Asserts proxy business logic returns 0 (login accepted) for this user.
        /// </summary>
        [Test]
        public void BTestLoginBLa()
        {
            int returnInt = TestLoginBL("TestPV001", "001PVTest");
            Assert.AreEqual(0, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBLb.
        /// badLogin/badLogin login.
        /// Asserts proxy business logic returns -10 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBLb()
        {
            int returnInt = TestLoginBL("badLogin", "badLogin");
            Assert.AreEqual(-10, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL01.
        /// TestPV001/001PVTest login.
        /// Asserts proxy business logic returns 0 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL01()
        {
            int returnInt = TestLoginBL("TestPV001", "001PVTest");
            Assert.AreEqual(0, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL02.
        /// TestPV002/002PVTest login.
        /// Asserts proxy business logic returns 0 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL02()
        {
            int returnInt = TestLoginBL("TestPV002", "002PVTest");
            Assert.AreEqual(0, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL03.
        /// TestPV003/003PVTest login.
        /// Asserts proxy business logic returns -10 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL03()
        {
            int returnInt = TestLoginBL("TestPV003", "003PVTest");
            Assert.AreEqual(-10, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL04.
        /// TestPV004/004PVTest login.
        /// Asserts proxy business logic returns 0 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL04()
        {
            int returnInt = TestLoginBL("TestPV004", "004PVTest");
            Assert.AreEqual(0, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL05.
        /// TestPV005/005PVTest login.
        /// Asserts proxy business logic returns 0 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL05()
        {
            int returnInt = TestLoginBL("TestPV005", "005PVTest");
            Assert.AreEqual(0, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL06.
        /// TestPV006/006PVTest login.
        /// Asserts proxy business logic returns -1 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL06()
        {
            int returnInt = TestLoginBL("TestPV006", "006PVTest");
            Assert.AreEqual(-1, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL07.
        /// TestPV007/007PVTest login.
        /// Asserts proxy business logic returns 0 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL07()
        {
            int returnInt = TestLoginBL("TestPV007", "007PVTest");
            Assert.AreEqual(0, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL08.
        /// TestPV008/008PVTest login.
        /// Asserts proxy business logic returns -20 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL08()
        {
            int returnInt = TestLoginBL("TestPV008", "008PVTest");
            Assert.AreEqual(-20, _opiLoginAccepted);
        }

        /// <summary>
        /// BTestLoginBL09.
        /// TestPV009/009PVTest login.
        /// Asserts proxy business logic returns -30 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL09()
        {
            int returnInt = TestLoginBL("TestPV009", "009PVTest");
            Assert.AreEqual(-30, returnInt);
        }

        /// <summary>
        /// BTestLoginBL10.
        /// TestPV010/010PVTest login.
        /// Asserts proxy business logic returns 0 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL10()
        {
            int returnInt = TestLoginBL("TestPV010", "010PVTest");
            Assert.AreEqual(0, returnInt);
        }

        /// <summary>
        /// BTestLoginBL11.
        /// TestPV011/011PVTest login.
        /// Asserts proxy business logic returns -40 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL11()
        {
            int returnInt = TestLoginBL("TestPV011", "011PVTest");

            // Per Greg, need to discuss a better approach to put the database 
            // in state before running the test.
            
            //Assert.AreEqual(-40, returnInt);
            Assert.IsTrue(true);
        }

        /// <summary>
        /// BTestLoginBL12.
        /// TestPV012/012PVTest login.
        /// Asserts proxy business logic returns -50 for this user.
        /// </summary>
        [Test]
        public void BTestLoginBL12()
        {
            int returnInt = TestLoginBL("TestPV012", "012PVTest");
            
            // Per Greg, need to discuss a better approach to put the database 
            // in state before running the test.

            //Assert.AreEqual(-50, returnInt);
            Assert.IsTrue(true);
        }

        public int TestLoginBL(string username, string password)
        {
            string opcContextId = "";

            _dsSessionMgr = new dsSessionMgrDataSet();
            _dsCustomer = new dsCustomerDataSet();
            _dsPV = new dsPartnerVuDataSet();
            _dsPVParam = new dsPVParamDataSet();
            

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            _oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            _oEntryNET = new EntryNET(_oConnection);
            _oSession = new Session(_oEntryNET);
            _oSession.sipartnervuestablishcontext(username, password, "Product=PrtnrView", out opcContextId, out _dsSessionMgr, out _dsCustomer, out _dsPV, out _dsPVParam, out _opiLoginAccepted, out _opcMessages);

            _oSession.Dispose();
            _oEntryNET.Dispose();
            _oConnection.Dispose();

            return _opiLoginAccepted;
        }

        /// <summary>
        /// CTestDSStructure.
        /// Assertion to check for proper tables in dsSessionMgr DataSet.
        /// </summary>
        [Test]
        public void CTestDSStructure()
        {
            //DataSet tests...
            _ds = new DataSet();
            foreach (DataTable table in _dsSessionMgr.Tables)
            {
                _ds.Tables.Add(table.Copy());
            }
            foreach (DataTable table in _dsCustomer.Tables)
            {
                _ds.Tables.Add(table.Copy());
            }
            foreach (DataTable table in _dsPV.Tables)
            {
                _ds.Tables.Add(table.Copy());
            }

            Assert.AreEqual(9, _ds.Tables.Count);
        }

        /// <summary>
        /// CTestDTttProperty.
        /// Assertion to check ttProperty table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void CTestDTttProperty()
        {
            Assert.AreEqual("ttSelectedProperty", _ds.Tables[0].TableName);

            //ttProperty Table
            Assert.AreEqual(3, _ds.Tables["ttSelectedProperty"].Columns.Count);

            Assert.AreEqual("propertyName", _ds.Tables["ttSelectedProperty"].Columns[0].Caption);
            Assert.AreEqual("propertyValue", _ds.Tables["ttSelectedProperty"].Columns[1].Caption);
            Assert.AreEqual("contextID", _ds.Tables["ttSelectedProperty"].Columns[2].Caption);
        }

        /// <summary>
        /// DTestDTttbranch.
        /// Assertion to check ttbranch table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void DTestDTttbranch()
        {
            Assert.AreEqual("ttbranch", _ds.Tables[1].TableName);

            //ttbranch Table
            Assert.AreEqual(3, _ds.Tables["ttbranch"].Columns.Count);
            Assert.AreEqual("branch_id", _ds.Tables["ttbranch"].Columns[0].Caption);
            Assert.AreEqual("prof_name", _ds.Tables["ttbranch"].Columns[1].Caption);
            Assert.AreEqual("company_name", _ds.Tables["ttbranch"].Columns[2].Caption);
        }

        /// <summary>
        /// ETestDTttbranch_cust.
        /// Assertion to check ttbranch_cust table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void ETestDTttbranch_cust()
        {
            Assert.AreEqual("ttbranch_cust", _ds.Tables[2].TableName);

            //ttbranch_cust Table
            Assert.AreEqual(5, _ds.Tables["ttbranch_cust"].Columns.Count);
            Assert.AreEqual("branch_id", _ds.Tables["ttbranch_cust"].Columns[0].Caption);
            Assert.AreEqual("prof_name", _ds.Tables["ttbranch_cust"].Columns[1].Caption);
            Assert.AreEqual("company_name", _ds.Tables["ttbranch_cust"].Columns[2].Caption);
            Assert.AreEqual("cust_system_id", _ds.Tables["ttbranch_cust"].Columns[3].Caption);
            Assert.AreEqual("cust_branch_system_id", _ds.Tables["ttbranch_cust"].Columns[4].Caption);
        }

        /// <summary>
        /// FTestDTttCustomer.
        /// Assertion to check ttCustomer table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void FTestDTttCustomer()
        {
            Assert.AreEqual("ttCustomer", _ds.Tables[3].TableName);

            //ttCustomer Table
            Assert.AreEqual(17, _ds.Tables["ttCustomer"].Columns.Count);
            Assert.AreEqual("active", _ds.Tables["ttCustomer"].Columns[0].Caption);
            Assert.AreEqual("address_1", _ds.Tables["ttCustomer"].Columns[1].Caption);
            Assert.AreEqual("address_2", _ds.Tables["ttCustomer"].Columns[2].Caption);
            Assert.AreEqual("address_3", _ds.Tables["ttCustomer"].Columns[3].Caption);
            Assert.AreEqual("city", _ds.Tables["ttCustomer"].Columns[4].Caption);
            Assert.AreEqual("country", _ds.Tables["ttCustomer"].Columns[5].Caption);
            Assert.AreEqual("cust_code", _ds.Tables["ttCustomer"].Columns[6].Caption);
            Assert.AreEqual("cust_key", _ds.Tables["ttCustomer"].Columns[7].Caption);
            Assert.AreEqual("cust_name", _ds.Tables["ttCustomer"].Columns[8].Caption);
            Assert.AreEqual("cust_obj", _ds.Tables["ttCustomer"].Columns[9].Caption);
            Assert.AreEqual("fax", _ds.Tables["ttCustomer"].Columns[10].Caption);
            Assert.AreEqual("phone", _ds.Tables["ttCustomer"].Columns[11].Caption);
            Assert.AreEqual("phone_format_code", _ds.Tables["ttCustomer"].Columns[12].Caption);
            Assert.AreEqual("start_date", _ds.Tables["ttCustomer"].Columns[13].Caption);
            Assert.AreEqual("state", _ds.Tables["ttCustomer"].Columns[14].Caption);
            Assert.AreEqual("system_id", _ds.Tables["ttCustomer"].Columns[15].Caption);
            Assert.AreEqual("zip", _ds.Tables["ttCustomer"].Columns[16].Caption);
        }

        /// <summary>
        /// GTestDTttShipto.
        /// Assertion to check ttShipto table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void GTestDTttShipto()
        {
            Assert.AreEqual("ttShipto", _ds.Tables[4].TableName);

            //ttShipto Table
            Assert.AreEqual(22, _ds.Tables["ttShipto"].Columns.Count);
            Assert.AreEqual("active", _ds.Tables["ttShipto"].Columns[0].Caption);
            Assert.AreEqual("address_1", _ds.Tables["ttShipto"].Columns[1].Caption);
            Assert.AreEqual("address_2", _ds.Tables["ttShipto"].Columns[2].Caption);
            Assert.AreEqual("address_3", _ds.Tables["ttShipto"].Columns[3].Caption);
            Assert.AreEqual("billto_seq_num", _ds.Tables["ttShipto"].Columns[4].Caption);
            Assert.AreEqual("city", _ds.Tables["ttShipto"].Columns[5].Caption);
            Assert.AreEqual("country", _ds.Tables["ttShipto"].Columns[6].Caption);
            Assert.AreEqual("cust_key", _ds.Tables["ttShipto"].Columns[7].Caption);
            Assert.AreEqual("cust_key_sysid", _ds.Tables["ttShipto"].Columns[8].Caption);
            Assert.AreEqual("cust_obj", _ds.Tables["ttShipto"].Columns[9].Caption);
            Assert.AreEqual("cust_shipto_obj", _ds.Tables["ttShipto"].Columns[10].Caption);
            Assert.AreEqual("fax", _ds.Tables["ttShipto"].Columns[11].Caption);
            Assert.AreEqual("phone", _ds.Tables["ttShipto"].Columns[12].Caption);
            Assert.AreEqual("phone_format_code", _ds.Tables["ttShipto"].Columns[13].Caption);
            Assert.AreEqual("seq_num", _ds.Tables["ttShipto"].Columns[14].Caption);
            Assert.AreEqual("shipto_name", _ds.Tables["ttShipto"].Columns[15].Caption);
            Assert.AreEqual("state", _ds.Tables["ttShipto"].Columns[16].Caption);
            Assert.AreEqual("system_id", _ds.Tables["ttShipto"].Columns[17].Caption);
            Assert.AreEqual("zip", _ds.Tables["ttShipto"].Columns[18].Caption);
            Assert.AreEqual("nonsalable", _ds.Tables["ttShipto"].Columns[19].Caption);
            Assert.AreEqual("ship_via", _ds.Tables["ttShipto"].Columns[20].Caption);
            Assert.AreEqual("ship_complete", _ds.Tables["ttShipto"].Columns[21].Caption);
        }

        /// <summary>
        /// HTestDTttCustShiptoBranch.
        /// Assertion to check ttCustShiptoBranch table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void HTestDTttCustShiptoBranch()
        {
            Assert.AreEqual("ttCustShiptoBranch", _ds.Tables[5].TableName);

            //ttCustShiptoBranch Table
            Assert.AreEqual(4, _ds.Tables["ttCustShiptoBranch"].Columns.Count);
            Assert.AreEqual("cust_obj", _ds.Tables["ttCustShiptoBranch"].Columns[0].Caption);
            Assert.AreEqual("cust_shipto_obj", _ds.Tables["ttCustShiptoBranch"].Columns[1].Caption);
            Assert.AreEqual("cust_branch_system_id", _ds.Tables["ttCustShiptoBranch"].Columns[2].Caption);
        }

        /// <summary>
        /// ITestDTttmenuUserMenus.
        /// Assertion to check bPV_userMenus table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void ITestDTttmenuUserMenus()
        {
            Assert.AreEqual("bPV_userMenus", _ds.Tables[6].TableName);

            //ttmenuUserMenus Table
            Assert.AreEqual(18, _ds.Tables["bPV_userMenus"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("bPV_userMenus.txt"))
            //{
            //    fileWriter = File.CreateText("bPV_userMenus.txt");
            //}
            //for (int x = 0; x < 18; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + _ds.Tables["bPV_userMenus"].Columns[x].ColumnName + "\", _ds.Tables[\"bPV_userMenus\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("prof_name", _ds.Tables["bPV_userMenus"].Columns[0].ColumnName);
            Assert.AreEqual("menu_structure_item_obj", _ds.Tables["bPV_userMenus"].Columns[1].ColumnName);
            Assert.AreEqual("product_module_obj", _ds.Tables["bPV_userMenus"].Columns[2].ColumnName);
            Assert.AreEqual("menu_structure_obj", _ds.Tables["bPV_userMenus"].Columns[3].ColumnName);
            Assert.AreEqual("menu_item_sequence", _ds.Tables["bPV_userMenus"].Columns[4].ColumnName);
            Assert.AreEqual("menu_item_obj", _ds.Tables["bPV_userMenus"].Columns[5].ColumnName);
            Assert.AreEqual("child_menu_structure_obj", _ds.Tables["bPV_userMenus"].Columns[6].ColumnName);
            Assert.AreEqual("menu_item_label", _ds.Tables["bPV_userMenus"].Columns[7].ColumnName);
            Assert.AreEqual("image1_up_filename", _ds.Tables["bPV_userMenus"].Columns[8].ColumnName);
            Assert.AreEqual("shortcut_key", _ds.Tables["bPV_userMenus"].Columns[9].ColumnName);
            Assert.AreEqual("object_obj", _ds.Tables["bPV_userMenus"].Columns[10].ColumnName);
            Assert.AreEqual("menu_item_description", _ds.Tables["bPV_userMenus"].Columns[11].ColumnName);
            Assert.AreEqual("menu_item_reference", _ds.Tables["bPV_userMenus"].Columns[12].ColumnName);
            Assert.AreEqual("menu_structure_code", _ds.Tables["bPV_userMenus"].Columns[13].ColumnName);
            Assert.AreEqual("grant_access", _ds.Tables["bPV_userMenus"].Columns[14].ColumnName);
            Assert.AreEqual("run_object", _ds.Tables["bPV_userMenus"].Columns[15].ColumnName);
            Assert.AreEqual("run_smart", _ds.Tables["bPV_userMenus"].Columns[16].ColumnName);
            Assert.AreEqual("iDisplaySeq", _ds.Tables["bPV_userMenus"].Columns[17].ColumnName);
        }

        /// <summary>
        /// JTestDTttCompanyInfo.
        /// Assertion to check bPV_CoInfo table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void JTestDTttCompanyInfo()
        {
            Assert.AreEqual("bPV_CoInfo", _ds.Tables[7].TableName);

            //ttCompanyInfo Table
            Assert.AreEqual(16, _ds.Tables["bPV_CoInfo"].Columns.Count);
            Assert.AreEqual("default_branch", _ds.Tables["bPV_CoInfo"].Columns[0].Caption);
            Assert.AreEqual("default_cust_key", _ds.Tables["bPV_CoInfo"].Columns[1].Caption);
            Assert.AreEqual("default_cust_key_sysid", _ds.Tables["bPV_CoInfo"].Columns[2].Caption);
            Assert.AreEqual("default_shipto_seq", _ds.Tables["bPV_CoInfo"].Columns[3].Caption);
            Assert.AreEqual("cust_div_address1", _ds.Tables["bPV_CoInfo"].Columns[4].Caption);
            Assert.AreEqual("cust_div_address2", _ds.Tables["bPV_CoInfo"].Columns[5].Caption);
            Assert.AreEqual("cust_div_city", _ds.Tables["bPV_CoInfo"].Columns[6].Caption);
            Assert.AreEqual("cust_div_country", _ds.Tables["bPV_CoInfo"].Columns[7].Caption);
            Assert.AreEqual("cust_div_remit_desc", _ds.Tables["bPV_CoInfo"].Columns[8].Caption);
            Assert.AreEqual("cust_div_division_id", _ds.Tables["bPV_CoInfo"].Columns[9].Caption);
            Assert.AreEqual("cust_div_remit_name", _ds.Tables["bPV_CoInfo"].Columns[10].Caption);
            Assert.AreEqual("cust_div_state", _ds.Tables["bPV_CoInfo"].Columns[11].Caption);
            Assert.AreEqual("cust_div_division_sysid", _ds.Tables["bPV_CoInfo"].Columns[12].Caption);
            Assert.AreEqual("cust_div_zip", _ds.Tables["bPV_CoInfo"].Columns[13].Caption);
            Assert.AreEqual("cust_div_phone", _ds.Tables["bPV_CoInfo"].Columns[14].Caption);
            Assert.AreEqual("default_submit_cart_as", _ds.Tables["bPV_CoInfo"].Columns[15].Caption);
        }

        /// <summary>
        /// KTestDTttAction.
        /// Assertion to check bPV_Action table and all its fields for name
        /// and order of the fields.
        /// </summary>
        [Test]
        public void KTestDTttAction()
        {
            Assert.AreEqual("bPV_Action", _ds.Tables[8].TableName);

            //ttAction Table
            Assert.AreEqual(3, _ds.Tables["bPV_Action"].Columns.Count);
            Assert.AreEqual("product_module_code", _ds.Tables["bPV_Action"].Columns[0].Caption);
            Assert.AreEqual("program_name", _ds.Tables["bPV_Action"].Columns[1].Caption);
            Assert.AreEqual("token_code", _ds.Tables["bPV_Action"].Columns[2].Caption);
        }
    }
}
