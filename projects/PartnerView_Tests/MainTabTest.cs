using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;
using System.Threading;
using Dmsi.Agility.Data;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class MainTabTest : WebFormTestCase
    {
        public MainTabTest()
        {

        }

        private void Login(string testName, string user, string password)
        {
            Console.WriteLine(testName + " - Started; User: " + user + " Password: " + password);

            Browser.GetPage(Dmsi.Agility.EntryNET.Properties.Settings.Default.PVurl);

            TextBoxTester txtUserName = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtUserName", CurrentWebForm);
            TextBoxTester txtPassword = new TextBoxTester("ctl00_cphMaster1_LogOn1_txtPassword", CurrentWebForm);
            ButtonTester btnLogin = new ButtonTester("ctl00_cphMaster1_LogOn1_btnLogin", CurrentWebForm);

            txtUserName.Text = user;
            txtPassword.Text = password;
            btnLogin.Click();
            //System.Threading.Thread.Sleep(10000);
            LabelTester lblWelcomeHeader = new LabelTester("ctl00_WelcomeHeader", CurrentWebForm);
            AssertVisibility(lblWelcomeHeader, true);
            string searchStr = lblWelcomeHeader.Text.Substring(0, 8);
            Assert.AreEqual("Welcome ", searchStr, "Login failed, the test user might not be setup to return 0.");
            Console.WriteLine("MainTabTest:BBranchChangeUITest - Login successful");
            Console.WriteLine("Current URL: " + Browser.CurrentUrl.ToString());
        }

        private void IsLocatorMode(string title)
        {
            // Dummy test widgets for TabPanel
            LabelTester tpCustomer = new LabelTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_CustomerPanel", CurrentWebForm);
            LabelTester tpSalesOrder = new LabelTester("ctl00_cphMaster1_MainTabControl1_TabContainer1_SalesOrderPanel", CurrentWebForm);
            LabelTester tpCreditMemo = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_CreditMemoPanel", CurrentWebForm);
            LabelTester tpQuote = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_QuotePanel", CurrentWebForm);
            LabelTester tpInvoice = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_InvoicePanel", CurrentWebForm);
            LabelTester tpOrderForm = new LabelTester("ct100_cphMaster1_MainTabControl1_TabContainer1_OrderFormPanel", CurrentWebForm);

            Console.WriteLine(title + "- Checking tpCustomer's visibility.");
            Assert.IsNotNull(tpCustomer, "tpCustomer is null");
            AssertVisibility(tpCustomer, true);
            Assert.IsTrue(tpCustomer.Enabled, "tpCustomer is disabled.");
            // TODO: Check for active tab when TabPanelTester is finally available

            Console.WriteLine(title + "- Checking if the other tabpanels are hidden.");
            AssertVisibility(tpSalesOrder, true);
            AssertVisibility(tpCreditMemo, false);
            AssertVisibility(tpQuote, false);
            AssertVisibility(tpInvoice, false);
            AssertVisibility(tpOrderForm, false);
        }

        /// <summary>
        /// ALoginToCustomerLocatorUITest.
        /// TestPV005/005PVTest.
        /// Asserts existence/visibility of proper tabs for this login.
        /// </summary>
        [Test]
        public void ALoginToCustomerLocatorUITest()
        {
            //this.Login("MainTabTest:AInitialLoginUITest", "TestPV005", "005PVTest");
            //this.IsLocatorMode("MainTabTest:AInitialLoginUITest");
        }

        /// <summary>
        /// BBranchChangedUITest.
        /// TestPV004/004PVTest.
        /// Asserts the Branch drop-down is visible and enabled for this login.
        /// </summary>
        [Test]
        public void BBranchChangedUITest()
        {
            //this.Login("MainTabTest:BBranchChangeUITest", "TestPV004", "004PVTest");

            //DropDownListTester ddlBranch = new DropDownListTester("ctl00_cphMaster1_MainTabControl1_ddlBranch", CurrentWebForm);

            //Console.WriteLine("MainTabTest:BBranchChangeUITest - Checking ddlBranch's visibility.");
            //Assert.IsNotNull(ddlBranch, "ddlBranch is null.");
            //AssertVisibility(ddlBranch, true);
            //Assert.IsTrue(ddlBranch.Enabled, "ddlBranch is disabled");

            //// Default branch: WESTONBRANCH
            //// Change branch: MULTI-1
            //string currentBranchID = ddlBranch.SelectedItem.Value;

            //Console.WriteLine("MainTabTest:BBranchChangeUITest - Incomplete test . . .");
            //Console.WriteLine("ISSUE: The emitted HTML \"<select></select>\" after the postback doesn't get updated to the newly selected listitem;" +
            //                  "\r\neventhough, the UI displays the correct listitem. This causes NUnit to report the same value after the postback.");
        }
    }
}
