using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;
using System.Reflection;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class bSalesOrderTest : WebFormTestCase
    {
        Progress.Open4GL.Proxy.Connection oConnection;
        EntryNET oEntryNET;
        Session oSession;
        SalesOrders oSalesOrders;

        dsSessionMgrDataSet dsSessionMgr;
        dsCustomerDataSet dsCustomer;
        dsPartnerVuDataSet dsPV;
        dsPVParamDataSet dsPVParam;
        dsSalesOrderDataSet dsSalesOrder;
        dsSalesOrderDataSet dsSalesOrderDetail;
        dsOrderCartDataSet dsOrderCart;

        int opiLoginAccepted;
        string opcMessages;
        string ipcSelectionCriteria;

        public bSalesOrderTest()
        {
        }

        /// <summary>
        /// ATestSalesOrderBL.
        /// Login mmcallister/mm.
        /// ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Sales Orders" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Sales Order ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=1/11/2007" + "\x0003" + "End Date=2/21/2007" + "\x0003" + "Show Results=Sales Order";
        /// Assert all the tables and table names in the SalesOrder DataSet.
        /// Then fetch a single Sales Order detail and assert the tables returned.
        /// </summary>
        [Test]
        public void ATestSalesOrderBL()
        {
            dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomer = new dsCustomerDataSet();
            dsPV = new dsPartnerVuDataSet();
            dsPVParam = new dsPVParamDataSet();
            dsSalesOrder = new dsSalesOrderDataSet();
            dsOrderCart = new dsOrderCartDataSet();

            oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            oEntryNET = new EntryNET(oConnection);
            oSession = new Session(oEntryNET);
            oSalesOrders = new SalesOrders(oEntryNET);

            string ipcContextId = "";

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out ipcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Type=Sales Orders" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Customer=00001271" + "\x0003" + "Ship To=" + "\x0003" + "Status=" + "\x0003" + "SearchType=Sales Order ID" + "\x0003" + "SearchTran=" + "\x0003" + "Start Date=1/11/2007" + "\x0003" + "End Date=2/21/2007" + "\x0003" + "Show Results=Sales Order";
            dsCustomer = FetchdsCUforSIcallsDataSet(dsCustomer);

            int ReturnCode = 0;
            string MessageText = "";

            oSalesOrders.sisalesorderfetchlist(ipcContextId, ipcSelectionCriteria, dsCustomer, out dsSalesOrder, out ReturnCode, out MessageText);

            //Tests on returned dsSalesOrder DataSet...
            Assert.IsNotNull(dsSalesOrder);
            Assert.AreEqual(4, dsSalesOrder.Tables.Count);
            Assert.AreEqual("ttso_header", dsSalesOrder.Tables[0].TableName);
            Assert.AreEqual("ttso_detail", dsSalesOrder.Tables[1].TableName);
            Assert.AreEqual("ttso_byitem", dsSalesOrder.Tables[2].TableName);
            Assert.AreEqual("ttmisc", dsSalesOrder.Tables[3].TableName);

            ipcSelectionCriteria = "Type=Sales Orders" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "SearchType=Sales Order ID" + "\x0003" + "SearchTran=20947";

            oSalesOrders.sisalesorderfetchsingle(ipcContextId, ipcSelectionCriteria, out dsSalesOrderDetail, out ReturnCode, out MessageText);

            //Tests on returned dsSalesOrder DataSet...
            Assert.IsNotNull(dsSalesOrderDetail);
            Assert.AreEqual(4, dsSalesOrderDetail.Tables.Count);
            Assert.AreEqual("ttso_header", dsSalesOrderDetail.Tables[0].TableName);
            Assert.AreEqual("ttso_detail", dsSalesOrderDetail.Tables[1].TableName);
            Assert.AreEqual("ttso_byitem", dsSalesOrderDetail.Tables[2].TableName);
            Assert.AreEqual("ttmisc", dsSalesOrderDetail.Tables[3].TableName);

            Assert.AreEqual(0, dsSalesOrderDetail.Tables["ttso_header"].Rows.Count);
            Assert.AreEqual(0, dsSalesOrderDetail.Tables["ttso_detail"].Rows.Count);
            Assert.AreEqual(0, dsSalesOrderDetail.Tables["ttso_byitem"].Rows.Count);

            oSalesOrders.Dispose();
            oSession.Dispose();
            oEntryNET.Dispose();
            oConnection.Dispose();
        }

        /// <summary>
        /// BTestDTttso_header.
        /// Assert all the fields and the order of the fields in the
        /// ttso_header table of the SalesOrder DataSet.
        /// </summary>
        [Test]
        public void BTestDTttso_header()
        {
            //ttso_header Table
            string[] fields = new string[] 
                            {"branch_id","so_id","cm_invoice_ref","job","reference","cust_po","ordered_by",
                             "order_date","expect_date","route_id_char","ship_via","sale_type","sale_type_desc",
                             "statusdescr","freight_terms_id","pay_terms_code","order_subtotal",
                             "order_charges","order_taxes","order_total","rep_1","rep_2","rep_3",
                             "shiptoname","shipToAddr1","shipToAddr2","shipToAddr3","shipToCity",
                             "shipToState","shipToZip","prof_name","system_id","TYPE","sale_type_sysid",
                             "so_status","so_status_type","credit_queue_status","price_hold","cust_key",
                             "cust_key_sysid","cust_code","shipto_seq_num","ship_to_seq_num_sysid",
                             "override_adf","adf","taxable","tax_code","taxcode_sysid","weight","load","so_header_obj",
                             "user_email","user_email2","user_fax","order_message","ship_complete"};

            Console.WriteLine("SalesOrderTest - BTestDTttso_header: Compare count");
            Assert.AreEqual(fields.Length, dsSalesOrderDetail.ttso_header.Columns.Count);

            for (int i = 0; i < dsSalesOrderDetail.ttso_header.Columns.Count; i++)
            {
                Console.WriteLine("SalesOrderTest - BTestDTttso_header: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsSalesOrderDetail.ttso_header.Columns[i].Caption);
            }
        }

        /// <summary>
        /// CTestDTttso_detail.
        /// Assert all the fields and the order of the fields in the
        /// ttso_detail table of the SalesOrderDetail DataSet.
        /// </summary>
        [Test]
        public void CTestDTttso_detail()
        {
            //ttso_detail Table
            string[] fields = new string[] 
                            {"display_seq","ITEM","SIZE","so_desc","qty_ordered","qty_staged",
                             "qty_backordered","uom","price","price_uom_code","extended_price","wo_phrase",
                             "system_id","so_id","so_id_sysid","sequence","ITEM_ptr_sysid",
                             "ITEM_ptr","thickness","WIDTH","LENGTH","DISP_price_conv","DISP_qty_conv","ord_qty_uom_conv_ptr_sysid",
                             "ordqty_uom_conv_ptr","discount_1","discount_2","discount_3","extra_discount_1",
                             "extra_discount_2","extra_discount_3","line_message", "header_charge", "override_price"};

            Console.WriteLine("SalesOrderTest - CTestDTttso_detail: Compare count");
            Assert.AreEqual(fields.Length, dsSalesOrderDetail.ttso_detail.Columns.Count);

            for (int i = 0; i < dsSalesOrderDetail.ttso_detail.Columns.Count; i++)
            {
                Console.WriteLine("SalesOrderTest - CTestDTttso_detail: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsSalesOrderDetail.ttso_detail.Columns[i].Caption);
            }
        }

        /// <summary>
        /// DTestDTttso_byitem.
        /// Assert all the fields and the order of the fields in the
        /// ttso_byitem table of the SalesOrderDetail DataSet.
        /// </summary>
        [Test]
        public void DTestDTttso_byitem()
        {
            //ttso_detail Table
            string[] fields = new string[] 
                            {"branch_id","so_id","sequence","job","ITEM","SIZE","so_desc",
                            "qty_ordered","uom","qty_staged","qty_backordered","price",
                            "price_uom_code","extended_price","reference","cust_po",
                            "order_date","expect_date","route_id_char","ship_via","sale_type",
                            "sale_type_desc","statusdescr","rep_1","rep_2","rep_3","shiptoname",
                            "shipToAddr1","shipToAddr2","shipToAddr3","shipToCity","shipToState","shipToZip","wo_phrase"};

            Console.WriteLine("SalesOrderTest - DTestDTttso_byitem: Compare count");
            Assert.AreEqual(fields.Length, dsSalesOrderDetail.ttso_byitem.Columns.Count);

            for (int i = 0; i < dsSalesOrderDetail.ttso_byitem.Columns.Count; i++)
            {
                Console.WriteLine("SalesOrderTest - DTestDTttso_byitem: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsSalesOrderDetail.ttso_byitem.Columns[i].Caption);
            }
        }

        /// <summary>
        /// ETestDTttorder_cart.
        /// Assert all the fields and the order of the fields in the
        /// ttorder_cart table of the OrderCart DataSet.
        /// </summary>
        [Test]
        public void ETestDTttorder_cart()
        {
            //ttso_detail Table
            string[] fields = new string[] 
                            {"branch_id", "user_id", "cust_obj", "shipto_obj","sequence", "ITEM","thickness","WIDTH","LENGTH", "SIZE",
                             "DESCRIPTION", "qty", "uom", "price_uom", "price", "line_message", "min_pak", "extension", "configurable", "wo_phrase", "DescriptionAndWOPhrase"};

            Console.WriteLine("SalesOrderTest - ETestDTttorder_cart: Compare count");
            Assert.AreEqual(fields.Length, dsOrderCart.ttorder_cart.Columns.Count);

            for (int i = 0; i < dsOrderCart.ttorder_cart.Columns.Count; i++)
            {
                Console.WriteLine("SalesOrderTest - ETestDTttorder_cart: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsOrderCart.ttorder_cart.Columns[i].Caption);
            }
        }

        /// <summary>
        /// FTestDTttcust_default.
        /// Assert all the fields and the order of the fields in the
        /// ttcust_default table of the OrderCart DataSet.
        /// </summary>
        [Test]
        public void FTestDTttcust_default()
        {
            //ttcust_default Table
            string[] fields = new string[] 
                            {"cust_key","cust_key_sysid","shipto_seq_num","sale_type"};

            Console.WriteLine("SalesOrderTest - FTestDTttocust_default: Compare count");
            Assert.AreEqual(fields.Length, dsOrderCart.ttcust_default.Columns.Count);

            for (int i = 0; i < dsOrderCart.ttcust_default.Columns.Count; i++)
            {
                Console.WriteLine("SalesOrderTest - FTestDTttcust_default: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsOrderCart.ttcust_default.Columns[i].Caption);
            }
        }

        /// <summary>
        /// GTestDTttsales_type.
        /// Assert all the fields and the order of the fields in the
        /// ttsales_type table of the OrderCart DataSet.
        /// </summary>
        [Test]
        public void GTestDTttsales_type()
        {
            //ttsales_type Table
            string[] fields = new string[] { "system_id", "sale_type", "description" };

            Console.WriteLine("SalesOrderTest - GTestDTttsales_type: Compare count");
            Assert.AreEqual(fields.Length, dsOrderCart.ttsales_type.Columns.Count);

            for (int i = 0; i < dsOrderCart.ttsales_type.Columns.Count; i++)
            {
                Console.WriteLine("SalesOrderTest - GTestDTttsales_type: Compare field name: " + fields[i]);
                Assert.AreEqual(fields[i], dsOrderCart.ttsales_type.Columns[i].Caption);
            }
        }

        /// <summary>
        /// HTestDTttmisc.
        /// Assert all the fields and the order of the fields in the
        /// ttmisc table of the SalesOrder DataSet.
        /// </summary>
        [Test]
        public void HTestDTttmisc()
        {
            //ttmisc Table
            Assert.AreEqual(1, dsSalesOrder.Tables["ttmisc"].Columns.Count);

            Assert.AreEqual("doc_storage_avail", dsSalesOrder.Tables["ttmisc"].Columns[0].Caption);
        }

        protected dsCustomerDataSet FetchdsCUforSIcallsDataSet(dsCustomerDataSet dsCustomer)
        {
            dsCustomer.ReadXml("dsCUforSIcalls.xml");
            return dsCustomer;
        }
    }
}
