using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using NUnit.Extensions.Asp.AspTester;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Progress.Open4GL.Proxy;
using System.Data;
using System.IO;

namespace Dmsi.Agility.EntryNET
{
    [TestFixture]
    public class fInventoryTest : WebFormTestCase
    {
        Progress.Open4GL.Proxy.Connection oConnection;
        EntryNET oEntryNET;
        Session oSession;
        Inventory oInventory;

        dsSessionMgrDataSet dsSessionMgr;
        dsCustomerDataSet dsCustomer;
        dsPartnerVuDataSet dsPV;
        dsPVParamDataSet dsPVParam;
        dsInventoryDataSet dsInventory;
        dsInventoryDataSet dsInventoryDetail;
        dsProductGroupDataSet dsProductGroup;

        int opiLoginAccepted;
        string opcMessages;
        string ipcSelectionCriteria;

        public fInventoryTest()
        {
        }

        /// <summary>
        /// ATestInventoryBL.
        /// Tests inventory business logic for the following search criteria
        /// ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Customer=00001271" + "\x0003" + "Shipto=0" + "\x0003" + "Mode=all" + "\x0003" + "ExtendPrice=NO" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Major=" + "\x0003" + "Minor=" + "\x0003" + "SearchType=Item #" + "\x0003" + "SearchTran=" + "\x0003" + "Available=No"
        /// Following tests validate as DataSet tables.
        /// Tests on returned dsInventory DataSet.
        /// Tests on returned dsInventoryDetail DataSet.
        /// Tests on returned dsProductGroup DataSet.
        /// </summary>
        [Test]
        public void ATestInventoryBL()
        {
            dsSessionMgr = new dsSessionMgrDataSet();
            dsCustomer = new dsCustomerDataSet();
            dsPV = new dsPartnerVuDataSet();
            dsPVParam = new dsPVParamDataSet();
            dsInventory = new dsInventoryDataSet();
            dsInventoryDetail = new dsInventoryDataSet();
            dsProductGroup = new dsProductGroupDataSet();

            oConnection = new Progress.Open4GL.Proxy.Connection(Dmsi.Agility.EntryNET.Properties.Settings.Default.AppServerConnection, "Default", "Default", "PartnerView");
            oEntryNET = new EntryNET(oConnection);
            oSession = new Session(oEntryNET);
            oInventory = new Inventory(oEntryNET);

            string ipcContextId = "";

            Progress.Open4GL.RunTimeProperties.SetIntProperty("PROGRESS.Session.SessionModel", 1);

            //sipartnervuestablishcontext assumed to be working from previous test...
            oSession.sipartnervuestablishcontext("mmcallister", "1234567890", "Product=PrtnrView", out ipcContextId, out dsSessionMgr, out dsCustomer, out dsPV, out dsPVParam, out opiLoginAccepted, out opcMessages);

            ipcSelectionCriteria = "UserID=mmcallister" + "\x0003" + "Customer=00001271" + "\x0003" + "Shipto=0" + "\x0003" + "Mode=<all>" + "\x0003" + "ExtendPrice=NO" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "Major=" + "\x0003" + "Minor=" + "\x0003" + "SearchType=Item #" + "\x0003" + "SearchTran=" + "\x0003" + "Available=No";

            int ReturnCode;
            string MessageText;

            oInventory.siinventoryfetchlist(ipcContextId, ipcSelectionCriteria, dsPV, out dsInventory, out ReturnCode, out MessageText);

            //Tests on returned dsInventory DataSet...
            Assert.IsNotNull(dsInventory);
            Assert.AreEqual(11, dsInventory.Tables.Count);
            Assert.AreEqual("ttinventory", dsInventory.Tables[0].TableName);
            Assert.AreEqual("ttPriceInfo", dsInventory.Tables[1].TableName);
            Assert.AreEqual("ttitem_uomconv", dsInventory.Tables[2].TableName);
            Assert.AreEqual("ttavail_header", dsInventory.Tables[3].TableName);
            Assert.AreEqual("ttavail_detail", dsInventory.Tables[4].TableName);
            Assert.AreEqual("ttavail_branch", dsInventory.Tables[5].TableName);
            Assert.AreEqual("ttavail", dsInventory.Tables[6].TableName);
            Assert.AreEqual("tton_order_header", dsInventory.Tables[7].TableName);
            Assert.AreEqual("tton_order_detail", dsInventory.Tables[8].TableName);
            Assert.AreEqual("tton_order_branch", dsInventory.Tables[9].TableName);
            Assert.AreEqual("tton_order", dsInventory.Tables[10].TableName);

            ipcSelectionCriteria = "SecLevel=view_inv_prices" + "\x0003" + "Customer=00001271" + "\x0003" + "Shipto=0" + "\x0003" + "Mode=Single" + "\x0003" + "Branch=WESTONBRANCH" + "\x0003" + "SearchType=Item #" + "\x0003" + "SearchTran=112-2510";
            oInventory.siinventoryfetchlist(ipcContextId, ipcSelectionCriteria, dsPV, out dsInventoryDetail, out ReturnCode, out MessageText);

            //Tests on returned dsInventoryDetail DataSet...
            Assert.IsNotNull(dsInventoryDetail);
            Assert.AreEqual(11, dsInventoryDetail.Tables.Count);
            Assert.AreEqual("ttinventory", dsInventoryDetail.Tables[0].TableName);
            Assert.AreEqual("ttPriceInfo", dsInventoryDetail.Tables[1].TableName);
            Assert.AreEqual("ttitem_uomconv", dsInventoryDetail.Tables[2].TableName);
            Assert.AreEqual("ttavail_header", dsInventoryDetail.Tables[3].TableName);
            Assert.AreEqual("ttavail_detail", dsInventoryDetail.Tables[4].TableName);
            Assert.AreEqual("ttavail_branch", dsInventoryDetail.Tables[5].TableName);
            Assert.AreEqual("ttavail", dsInventoryDetail.Tables[6].TableName);
            Assert.AreEqual("tton_order_header", dsInventoryDetail.Tables[7].TableName);
            Assert.AreEqual("tton_order_detail", dsInventoryDetail.Tables[8].TableName);
            Assert.AreEqual("tton_order_branch", dsInventoryDetail.Tables[9].TableName);
            Assert.AreEqual("tton_order", dsInventoryDetail.Tables[10].TableName);

            ipcSelectionCriteria = "Branch=WESTONBRANCH";
            oInventory.siinventoryfetchlistprodgrp(ipcContextId, ipcSelectionCriteria, out dsProductGroup, out ReturnCode, out MessageText);

            //Tests on returned dsProductGroup DataSet...
            Assert.IsNotNull(dsProductGroup);
            Assert.AreEqual(2, dsProductGroup.Tables.Count);
            Assert.AreEqual("ttProductMajor", dsProductGroup.Tables[0].TableName);
            Assert.AreEqual("ttProductMinor", dsProductGroup.Tables[1].TableName);

            oInventory.Dispose();
            oSession.Dispose();
            oEntryNET.Dispose();
            oConnection.Dispose();
        }

        /// <summary>
        /// BTestDTttinventory.
        /// Asserts order of fields and all field names
        /// in the ttinventory table of the DataSet.
        /// </summary>
        [Test]
        public void BTestDTttinventory()
        {
            //ttinventory Table
            Assert.AreEqual(52, dsInventory.Tables["ttinventory"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttinventory.txt"))
            //{
            //    fileWriter = File.CreateText("ttinventory.txt");
            //}
            //for (int x = 0; x < 52; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["ttinventory"].Columns[x].ColumnName + "\", dsInventory.Tables[\"ttinventory\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("itemRecid", dsInventory.Tables["ttinventory"].Columns[0].ColumnName);
            Assert.AreEqual("prof_name", dsInventory.Tables["ttinventory"].Columns[1].ColumnName);
            Assert.AreEqual("branch_id", dsInventory.Tables["ttinventory"].Columns[2].ColumnName);
            Assert.AreEqual("system_id", dsInventory.Tables["ttinventory"].Columns[3].ColumnName);
            Assert.AreEqual("item_ptr", dsInventory.Tables["ttinventory"].Columns[4].ColumnName);
            Assert.AreEqual("ITEM", dsInventory.Tables["ttinventory"].Columns[5].ColumnName);
            Assert.AreEqual("TYPE", dsInventory.Tables["ttinventory"].Columns[6].ColumnName);
            Assert.AreEqual("SIZE", dsInventory.Tables["ttinventory"].Columns[7].ColumnName);
            Assert.AreEqual("short_des", dsInventory.Tables["ttinventory"].Columns[8].ColumnName);
            Assert.AreEqual("DESCRIPTION", dsInventory.Tables["ttinventory"].Columns[9].ColumnName);
            Assert.AreEqual("display_des", dsInventory.Tables["ttinventory"].Columns[10].ColumnName);
            Assert.AreEqual("long_short", dsInventory.Tables["ttinventory"].Columns[11].ColumnName);
            Assert.AreEqual("image_id", dsInventory.Tables["ttinventory"].Columns[12].ColumnName);
            Assert.AreEqual("image_id_sysid", dsInventory.Tables["ttinventory"].Columns[13].ColumnName);
            Assert.AreEqual("image_file", dsInventory.Tables["ttinventory"].Columns[14].ColumnName);
            Assert.AreEqual("price_code_major", dsInventory.Tables["ttinventory"].Columns[15].ColumnName);
            Assert.AreEqual("price_code_minor", dsInventory.Tables["ttinventory"].Columns[16].ColumnName);
            Assert.AreEqual("price_code_sysid", dsInventory.Tables["ttinventory"].Columns[17].ColumnName);
            Assert.AreEqual("stocking_uom", dsInventory.Tables["ttinventory"].Columns[18].ColumnName);
            Assert.AreEqual("stock_to_pc_calc", dsInventory.Tables["ttinventory"].Columns[19].ColumnName);
            Assert.AreEqual("piece_ref", dsInventory.Tables["ttinventory"].Columns[20].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["ttinventory"].Columns[21].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["ttinventory"].Columns[22].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["ttinventory"].Columns[23].ColumnName);
            Assert.AreEqual("thickness_uom", dsInventory.Tables["ttinventory"].Columns[24].ColumnName);
            Assert.AreEqual("width_uom", dsInventory.Tables["ttinventory"].Columns[25].ColumnName);
            Assert.AreEqual("length_uom", dsInventory.Tables["ttinventory"].Columns[26].ColumnName);
            Assert.AreEqual("price_uom", dsInventory.Tables["ttinventory"].Columns[27].ColumnName);
            Assert.AreEqual("price", dsInventory.Tables["ttinventory"].Columns[28].ColumnName);
            Assert.AreEqual("weight", dsInventory.Tables["ttinventory"].Columns[29].ColumnName);
            Assert.AreEqual("load_factor", dsInventory.Tables["ttinventory"].Columns[30].ColumnName);
            Assert.AreEqual("min_pak", dsInventory.Tables["ttinventory"].Columns[31].ColumnName);
            Assert.AreEqual("active_flag", dsInventory.Tables["ttinventory"].Columns[32].ColumnName);
            Assert.AreEqual("qty_text", dsInventory.Tables["ttinventory"].Columns[33].ColumnName);
            Assert.AreEqual("qty_available", dsInventory.Tables["ttinventory"].Columns[34].ColumnName);
            Assert.AreEqual("qty_onhand", dsInventory.Tables["ttinventory"].Columns[35].ColumnName);
            Assert.AreEqual("qty_onorder", dsInventory.Tables["ttinventory"].Columns[36].ColumnName);
            Assert.AreEqual("qty_onorder_direct", dsInventory.Tables["ttinventory"].Columns[37].ColumnName);
            Assert.AreEqual("qty_committed", dsInventory.Tables["ttinventory"].Columns[38].ColumnName);
            Assert.AreEqual("qty_bo", dsInventory.Tables["ttinventory"].Columns[39].ColumnName);
            Assert.AreEqual("qty_bo_direct", dsInventory.Tables["ttinventory"].Columns[40].ColumnName);
            Assert.AreEqual("qty_net_available", dsInventory.Tables["ttinventory"].Columns[41].ColumnName);
            Assert.AreEqual("major", dsInventory.Tables["ttinventory"].Columns[42].ColumnName);
            Assert.AreEqual("major_description", dsInventory.Tables["ttinventory"].Columns[43].ColumnName);
            Assert.AreEqual("minor", dsInventory.Tables["ttinventory"].Columns[44].ColumnName);
            Assert.AreEqual("minor_description", dsInventory.Tables["ttinventory"].Columns[45].ColumnName);
            Assert.AreEqual("majorminor", dsInventory.Tables["ttinventory"].Columns[46].ColumnName);
            Assert.AreEqual("pg_ptr", dsInventory.Tables["ttinventory"].Columns[47].ColumnName);
            Assert.AreEqual("pg_ptr_sysid", dsInventory.Tables["ttinventory"].Columns[48].ColumnName);
            Assert.AreEqual("quick_list", dsInventory.Tables["ttinventory"].Columns[49].ColumnName);
            Assert.AreEqual("configurable", dsInventory.Tables["ttinventory"].Columns[50].ColumnName);
            Assert.AreEqual("inventory_obj", dsInventory.Tables["ttinventory"].Columns[51].ColumnName);
        }

        /// <summary>
        /// CTestDTttPriceInfo.
        /// Asserts all field names and the order of the fields
        /// in the ttPriceInfo table of the Inventory Detail DataSet.
        /// 82 fields.
        /// </summary>
        [Test]
        public void CTestDTttPriceInfo()
        {
            //ttinventory Table
            Assert.AreEqual(129, dsInventoryDetail.Tables["ttPriceInfo"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttPriceInfo.txt"))
            //{
            //    fileWriter = File.CreateText("ttPriceInfo.txt");
            //}
            //for (int x = 0; x < 129; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventoryDetail.Tables["ttPriceInfo"].Columns[x].ColumnName + "\", dsInventoryDetail.Tables[\"ttPriceInfo\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("qtyLevelString", dsInventoryDetail.Tables["ttPriceInfo"].Columns[0].ColumnName);
            Assert.AreEqual("cPricingType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[1].ColumnName);
            Assert.AreEqual("cMargin", dsInventoryDetail.Tables["ttPriceInfo"].Columns[2].ColumnName);
            Assert.AreEqual("dateString", dsInventoryDetail.Tables["ttPriceInfo"].Columns[3].ColumnName);
            Assert.AreEqual("custOrGroup", dsInventoryDetail.Tables["ttPriceInfo"].Columns[4].ColumnName);
            Assert.AreEqual("startCustOrGroup", dsInventoryDetail.Tables["ttPriceInfo"].Columns[5].ColumnName);
            Assert.AreEqual("itemOrPriceCode", dsInventoryDetail.Tables["ttPriceInfo"].Columns[6].ColumnName);
            Assert.AreEqual("endDate", dsInventoryDetail.Tables["ttPriceInfo"].Columns[7].ColumnName);
            Assert.AreEqual("orderHierarchy", dsInventoryDetail.Tables["ttPriceInfo"].Columns[8].ColumnName);
            Assert.AreEqual("orderSalesType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[9].ColumnName);
            Assert.AreEqual("orderRecordType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[10].ColumnName);
            Assert.AreEqual("RecordType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[11].ColumnName);
            Assert.AreEqual("ITEM", dsInventoryDetail.Tables["ttPriceInfo"].Columns[12].ColumnName);
            Assert.AreEqual("SIZE", dsInventoryDetail.Tables["ttPriceInfo"].Columns[13].ColumnName);
            Assert.AreEqual("DESCRIPTION", dsInventoryDetail.Tables["ttPriceInfo"].Columns[14].ColumnName);
            Assert.AreEqual("ext_description", dsInventoryDetail.Tables["ttPriceInfo"].Columns[15].ColumnName);
            Assert.AreEqual("stockingUOM", dsInventoryDetail.Tables["ttPriceInfo"].Columns[16].ColumnName);
            Assert.AreEqual("costingUOM", dsInventoryDetail.Tables["ttPriceInfo"].Columns[17].ColumnName);
            Assert.AreEqual("cSOQuoteUOM", dsInventoryDetail.Tables["ttPriceInfo"].Columns[18].ColumnName);
            Assert.AreEqual("itemPtrSysID", dsInventoryDetail.Tables["ttPriceInfo"].Columns[19].ColumnName);
            Assert.AreEqual("itemPtr", dsInventoryDetail.Tables["ttPriceInfo"].Columns[20].ColumnName);
            Assert.AreEqual("ItemType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[21].ColumnName);
            Assert.AreEqual("piece_ref", dsInventoryDetail.Tables["ttPriceInfo"].Columns[22].ColumnName);
            Assert.AreEqual("stock_to_pc_calc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[23].ColumnName);
            Assert.AreEqual("thickness_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[24].ColumnName);
            Assert.AreEqual("width_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[25].ColumnName);
            Assert.AreEqual("length_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[26].ColumnName);
            Assert.AreEqual("startDate", dsInventoryDetail.Tables["ttPriceInfo"].Columns[27].ColumnName);
            Assert.AreEqual("date_range", dsInventoryDetail.Tables["ttPriceInfo"].Columns[28].ColumnName);
            Assert.AreEqual("custShipToNum", dsInventoryDetail.Tables["ttPriceInfo"].Columns[29].ColumnName);
            Assert.AreEqual("cShiptoString", dsInventoryDetail.Tables["ttPriceInfo"].Columns[30].ColumnName);
            Assert.AreEqual("iShiptoSort", dsInventoryDetail.Tables["ttPriceInfo"].Columns[31].ColumnName);
            Assert.AreEqual("custCode", dsInventoryDetail.Tables["ttPriceInfo"].Columns[32].ColumnName);
            Assert.AreEqual("saleType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[33].ColumnName);
            Assert.AreEqual("PriceDiscType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[34].ColumnName);
            Assert.AreEqual("TYPE", dsInventoryDetail.Tables["ttPriceInfo"].Columns[35].ColumnName);
            Assert.AreEqual("level", dsInventoryDetail.Tables["ttPriceInfo"].Columns[36].ColumnName);
            Assert.AreEqual("defaultLevel", dsInventoryDetail.Tables["ttPriceInfo"].Columns[37].ColumnName);
            Assert.AreEqual("qtyBreak", dsInventoryDetail.Tables["ttPriceInfo"].Columns[38].ColumnName);
            Assert.AreEqual("qty_break_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[39].ColumnName);
            Assert.AreEqual("uom_for_qty_break", dsInventoryDetail.Tables["ttPriceInfo"].Columns[40].ColumnName);
            Assert.AreEqual("defQtyBreak", dsInventoryDetail.Tables["ttPriceInfo"].Columns[41].ColumnName);
            Assert.AreEqual("uomCode", dsInventoryDetail.Tables["ttPriceInfo"].Columns[42].ColumnName);
            Assert.AreEqual("uomPtr", dsInventoryDetail.Tables["ttPriceInfo"].Columns[43].ColumnName);
            Assert.AreEqual("uomPtrSysid", dsInventoryDetail.Tables["ttPriceInfo"].Columns[44].ColumnName);
            Assert.AreEqual("price", dsInventoryDetail.Tables["ttPriceInfo"].Columns[45].ColumnName);
            Assert.AreEqual("dUnRoundedPrice", dsInventoryDetail.Tables["ttPriceInfo"].Columns[46].ColumnName);
            Assert.AreEqual("Rebate", dsInventoryDetail.Tables["ttPriceInfo"].Columns[47].ColumnName);
            Assert.AreEqual("rebate_flag", dsInventoryDetail.Tables["ttPriceInfo"].Columns[48].ColumnName);
            Assert.AreEqual("PriceNoAddOn", dsInventoryDetail.Tables["ttPriceInfo"].Columns[49].ColumnName);
            Assert.AreEqual("multiplier", dsInventoryDetail.Tables["ttPriceInfo"].Columns[50].ColumnName);
            Assert.AreEqual("costFlag", dsInventoryDetail.Tables["ttPriceInfo"].Columns[51].ColumnName);
            Assert.AreEqual("markupType", dsInventoryDetail.Tables["ttPriceInfo"].Columns[52].ColumnName);
            Assert.AreEqual("markup_amt", dsInventoryDetail.Tables["ttPriceInfo"].Columns[53].ColumnName);
            Assert.AreEqual("discountChain", dsInventoryDetail.Tables["ttPriceInfo"].Columns[54].ColumnName);
            Assert.AreEqual("PromoFlag", dsInventoryDetail.Tables["ttPriceInfo"].Columns[55].ColumnName);
            Assert.AreEqual("BOMPriceFlag", dsInventoryDetail.Tables["ttPriceInfo"].Columns[56].ColumnName);
            Assert.AreEqual("cXrefNum", dsInventoryDetail.Tables["ttPriceInfo"].Columns[57].ColumnName);
            Assert.AreEqual("PromoHierarchy", dsInventoryDetail.Tables["ttPriceInfo"].Columns[58].ColumnName);
            Assert.AreEqual("Comment", dsInventoryDetail.Tables["ttPriceInfo"].Columns[59].ColumnName);
            Assert.AreEqual("stockingQty", dsInventoryDetail.Tables["ttPriceInfo"].Columns[60].ColumnName);
            Assert.AreEqual("conversion", dsInventoryDetail.Tables["ttPriceInfo"].Columns[61].ColumnName);
            Assert.AreEqual("includeAddOn", dsInventoryDetail.Tables["ttPriceInfo"].Columns[62].ColumnName);
            Assert.AreEqual("roundingMethod", dsInventoryDetail.Tables["ttPriceInfo"].Columns[63].ColumnName);
            Assert.AreEqual("applyRounding", dsInventoryDetail.Tables["ttPriceInfo"].Columns[64].ColumnName);
            Assert.AreEqual("prodGroupMajor", dsInventoryDetail.Tables["ttPriceInfo"].Columns[65].ColumnName);
            Assert.AreEqual("prodGroupMinor", dsInventoryDetail.Tables["ttPriceInfo"].Columns[66].ColumnName);
            Assert.AreEqual("priceCodeMajor", dsInventoryDetail.Tables["ttPriceInfo"].Columns[67].ColumnName);
            Assert.AreEqual("priceCodeMajorDesc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[68].ColumnName);
            Assert.AreEqual("priceCodeMinor", dsInventoryDetail.Tables["ttPriceInfo"].Columns[69].ColumnName);
            Assert.AreEqual("priceCodeMinorDesc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[70].ColumnName);
            Assert.AreEqual("usesDimension", dsInventoryDetail.Tables["ttPriceInfo"].Columns[71].ColumnName);
            Assert.AreEqual("CustStandardCode", dsInventoryDetail.Tables["ttPriceInfo"].Columns[72].ColumnName);
            Assert.AreEqual("RecordRowID", dsInventoryDetail.Tables["ttPriceInfo"].Columns[73].ColumnName);
            Assert.AreEqual("dimensions", dsInventoryDetail.Tables["ttPriceInfo"].Columns[74].ColumnName);
            Assert.AreEqual("thickness", dsInventoryDetail.Tables["ttPriceInfo"].Columns[75].ColumnName);
            Assert.AreEqual("WIDTH", dsInventoryDetail.Tables["ttPriceInfo"].Columns[76].ColumnName);
            Assert.AreEqual("LENGTH", dsInventoryDetail.Tables["ttPriceInfo"].Columns[77].ColumnName);
            Assert.AreEqual("IsFirstLayer", dsInventoryDetail.Tables["ttPriceInfo"].Columns[78].ColumnName);
            Assert.AreEqual("piece_price_flag", dsInventoryDetail.Tables["ttPriceInfo"].Columns[79].ColumnName);
            Assert.AreEqual("price_type", dsInventoryDetail.Tables["ttPriceInfo"].Columns[80].ColumnName);
            Assert.AreEqual("disp_price", dsInventoryDetail.Tables["ttPriceInfo"].Columns[81].ColumnName);
            Assert.AreEqual("disp_price_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[82].ColumnName);
            Assert.AreEqual("gmpercent", dsInventoryDetail.Tables["ttPriceInfo"].Columns[83].ColumnName);
            Assert.AreEqual("discount", dsInventoryDetail.Tables["ttPriceInfo"].Columns[84].ColumnName);
            Assert.AreEqual("disc_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[85].ColumnName);
            Assert.AreEqual("disp_rebate", dsInventoryDetail.Tables["ttPriceInfo"].Columns[86].ColumnName);
            Assert.AreEqual("discQty", dsInventoryDetail.Tables["ttPriceInfo"].Columns[87].ColumnName);
            Assert.AreEqual("specific", dsInventoryDetail.Tables["ttPriceInfo"].Columns[88].ColumnName);
            Assert.AreEqual("uom_for_price", dsInventoryDetail.Tables["ttPriceInfo"].Columns[89].ColumnName);
            Assert.AreEqual("net_price", dsInventoryDetail.Tables["ttPriceInfo"].Columns[90].ColumnName);
            Assert.AreEqual("discountable", dsInventoryDetail.Tables["ttPriceInfo"].Columns[91].ColumnName);
            Assert.AreEqual("qty_break_rule", dsInventoryDetail.Tables["ttPriceInfo"].Columns[92].ColumnName);
            Assert.AreEqual("price_uom_by_pc_ref", dsInventoryDetail.Tables["ttPriceInfo"].Columns[93].ColumnName);
            Assert.AreEqual("apply_only_at_invoicing", dsInventoryDetail.Tables["ttPriceInfo"].Columns[94].ColumnName);
            Assert.AreEqual("weight", dsInventoryDetail.Tables["ttPriceInfo"].Columns[95].ColumnName);
            Assert.AreEqual("weight_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[96].ColumnName);
            Assert.AreEqual("load", dsInventoryDetail.Tables["ttPriceInfo"].Columns[97].ColumnName);
            Assert.AreEqual("load_uom", dsInventoryDetail.Tables["ttPriceInfo"].Columns[98].ColumnName);
            Assert.AreEqual("piece_price", dsInventoryDetail.Tables["ttPriceInfo"].Columns[99].ColumnName);
            Assert.AreEqual("cust_name", dsInventoryDetail.Tables["ttPriceInfo"].Columns[100].ColumnName);
            Assert.AreEqual("shipto_name", dsInventoryDetail.Tables["ttPriceInfo"].Columns[101].ColumnName);
            Assert.AreEqual("item_or_pricecode_flag", dsInventoryDetail.Tables["ttPriceInfo"].Columns[102].ColumnName);
            Assert.AreEqual("supp_name", dsInventoryDetail.Tables["ttPriceInfo"].Columns[103].ColumnName);
            Assert.AreEqual("ave_cost", dsInventoryDetail.Tables["ttPriceInfo"].Columns[104].ColumnName);
            Assert.AreEqual("gm_percent", dsInventoryDetail.Tables["ttPriceInfo"].Columns[105].ColumnName);
            Assert.AreEqual("gm_dollars", dsInventoryDetail.Tables["ttPriceInfo"].Columns[106].ColumnName);
            Assert.AreEqual("last_po_cost", dsInventoryDetail.Tables["ttPriceInfo"].Columns[107].ColumnName);
            Assert.AreEqual("last_recv_landed_cost", dsInventoryDetail.Tables["ttPriceInfo"].Columns[108].ColumnName);
            Assert.AreEqual("last_recv_unit_cost", dsInventoryDetail.Tables["ttPriceInfo"].Columns[109].ColumnName);
            Assert.AreEqual("market_cost", dsInventoryDetail.Tables["ttPriceInfo"].Columns[110].ColumnName);
            Assert.AreEqual("misc_cost_price_1", dsInventoryDetail.Tables["ttPriceInfo"].Columns[111].ColumnName);
            Assert.AreEqual("misc_cost_price_2", dsInventoryDetail.Tables["ttPriceInfo"].Columns[112].ColumnName);
            Assert.AreEqual("misc_cost_price_3", dsInventoryDetail.Tables["ttPriceInfo"].Columns[113].ColumnName);
            Assert.AreEqual("misc_cost_price_4", dsInventoryDetail.Tables["ttPriceInfo"].Columns[114].ColumnName);
            Assert.AreEqual("misc_cost_price_5", dsInventoryDetail.Tables["ttPriceInfo"].Columns[115].ColumnName);
            Assert.AreEqual("misc_cost_price_1_desc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[116].ColumnName);
            Assert.AreEqual("misc_cost_price_2_desc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[117].ColumnName);
            Assert.AreEqual("misc_cost_price_3_desc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[118].ColumnName);
            Assert.AreEqual("misc_cost_price_4_desc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[119].ColumnName);
            Assert.AreEqual("misc_cost_price_5_desc", dsInventoryDetail.Tables["ttPriceInfo"].Columns[120].ColumnName);
            Assert.AreEqual("price_rec_major", dsInventoryDetail.Tables["ttPriceInfo"].Columns[121].ColumnName);
            Assert.AreEqual("price_rec_minor", dsInventoryDetail.Tables["ttPriceInfo"].Columns[122].ColumnName);
            Assert.AreEqual("discount1", dsInventoryDetail.Tables["ttPriceInfo"].Columns[123].ColumnName);
            Assert.AreEqual("discount2", dsInventoryDetail.Tables["ttPriceInfo"].Columns[124].ColumnName);
            Assert.AreEqual("discount3", dsInventoryDetail.Tables["ttPriceInfo"].Columns[125].ColumnName);
            Assert.AreEqual("extraDisc1", dsInventoryDetail.Tables["ttPriceInfo"].Columns[126].ColumnName);
            Assert.AreEqual("extraDisc2", dsInventoryDetail.Tables["ttPriceInfo"].Columns[127].ColumnName);
            Assert.AreEqual("extraDisc3", dsInventoryDetail.Tables["ttPriceInfo"].Columns[128].ColumnName);
        }

        /// <summary>
        /// DTestDTttitem_uomconv.
        /// Assets all fields and the order of the fields
        /// in the ttitem_uomconv table of the Inventory DataSet.
        /// 7 fields.
        /// </summary>
        [Test]
        public void DTestDTttitem_uomconv()
        {
            //ttitem_uomconv Table
            Assert.AreEqual(7, dsInventory.Tables["ttitem_uomconv"].Columns.Count);

            Assert.AreEqual("item_ptr_sysid", dsInventory.Tables["ttitem_uomconv"].Columns[0].Caption);
            Assert.AreEqual("item_ptr", dsInventory.Tables["ttitem_uomconv"].Columns[1].Caption);
            Assert.AreEqual("uom_ptr", dsInventory.Tables["ttitem_uomconv"].Columns[2].Caption);
            Assert.AreEqual("uom_ptr_sysid", dsInventory.Tables["ttitem_uomconv"].Columns[3].Caption);
            Assert.AreEqual("convert_from", dsInventory.Tables["ttitem_uomconv"].Columns[4].Caption);
            Assert.AreEqual("convert_to", dsInventory.Tables["ttitem_uomconv"].Columns[5].Caption);
            Assert.AreEqual("conv_factor", dsInventory.Tables["ttitem_uomconv"].Columns[6].Caption);
        }

        /// <summary>
        /// ETestDTttavail_header.
        /// Assets all fields and the order of the fields
        /// in the ttavail_header table of the Inventory DataSet.
        /// 13 fields.
        /// </summary>
        [Test]
        public void ETestDTttavail_header()
        {
            //ttavail_header Table
            Assert.AreEqual(17, dsInventory.Tables["ttavail_header"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttavail_header.txt"))
            //{
            //    fileWriter = File.CreateText("ttavail_header.txt");
            //}
            //for (int x = 0; x < 17; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["ttavail_header"].Columns[x].ColumnName + "\", dsInventory.Tables[\"ttavail_header\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("PARENT_key", dsInventory.Tables["ttavail_header"].Columns[0].ColumnName);
            Assert.AreEqual("branch_id", dsInventory.Tables["ttavail_header"].Columns[1].ColumnName);
            Assert.AreEqual("system_id", dsInventory.Tables["ttavail_header"].Columns[2].ColumnName);
            Assert.AreEqual("item_ptr", dsInventory.Tables["ttavail_header"].Columns[3].ColumnName);
            Assert.AreEqual("item_ptr_sysid", dsInventory.Tables["ttavail_header"].Columns[4].ColumnName);
            Assert.AreEqual("tag", dsInventory.Tables["ttavail_header"].Columns[5].ColumnName);
            Assert.AreEqual("char-pc-cnt", dsInventory.Tables["ttavail_header"].Columns[6].ColumnName);
            Assert.AreEqual("qty_text", dsInventory.Tables["ttavail_header"].Columns[7].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["ttavail_header"].Columns[8].ColumnName);
            Assert.AreEqual("stocking_uom", dsInventory.Tables["ttavail_header"].Columns[9].ColumnName);
            Assert.AreEqual("loc_ptr", dsInventory.Tables["ttavail_header"].Columns[10].ColumnName);
            Assert.AreEqual("loc_ptr_sysid", dsInventory.Tables["ttavail_header"].Columns[11].ColumnName);
            Assert.AreEqual("location", dsInventory.Tables["ttavail_header"].Columns[12].ColumnName);
            Assert.AreEqual("lot", dsInventory.Tables["ttavail_header"].Columns[13].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["ttavail_header"].Columns[14].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["ttavail_header"].Columns[15].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["ttavail_header"].Columns[16].ColumnName);
        }

        /// <summary>
        /// FTestDTttavail_detail.
        /// Assets all fields and the order of the fields
        /// in the ttavail_detail table of the Inventory DataSet.
        /// 12 fields.
        /// </summary>
        [Test]
        public void FTestDTttavail_detail()
        {
            //ttavail_detail Table
            Assert.AreEqual(13, dsInventory.Tables["ttavail_detail"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttavail_detail.txt"))
            //{
            //    fileWriter = File.CreateText("ttavail_detail.txt");
            //}
            //for (int x = 0; x < 13; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["ttavail_detail"].Columns[x].ColumnName + "\", dsInventory.Tables[\"ttavail_detail\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("PARENT_key", dsInventory.Tables["ttavail_detail"].Columns[0].ColumnName);
            Assert.AreEqual("system_id", dsInventory.Tables["ttavail_detail"].Columns[1].ColumnName);
            Assert.AreEqual("item_ptr", dsInventory.Tables["ttavail_detail"].Columns[2].ColumnName);
            Assert.AreEqual("loc_ptr", dsInventory.Tables["ttavail_detail"].Columns[3].ColumnName);
            Assert.AreEqual("lot", dsInventory.Tables["ttavail_detail"].Columns[4].ColumnName);
            Assert.AreEqual("tag", dsInventory.Tables["ttavail_detail"].Columns[5].ColumnName);
            Assert.AreEqual("char-pc-cnt", dsInventory.Tables["ttavail_detail"].Columns[6].ColumnName);
            Assert.AreEqual("qty_text", dsInventory.Tables["ttavail_detail"].Columns[7].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["ttavail_detail"].Columns[8].ColumnName);
            Assert.AreEqual("stocking_uom", dsInventory.Tables["ttavail_detail"].Columns[9].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["ttavail_detail"].Columns[10].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["ttavail_detail"].Columns[11].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["ttavail_detail"].Columns[12].ColumnName);
        }

        /// <summary>
        /// GTestDTttavail_branch.
        /// Assets all fields and the order of the fields
        /// in the ttavail_branch table of the Inventory DataSet.
        /// 5 fields.
        /// </summary>
        [Test]
        public void GTestDTttavail_branch()
        {
            //ttavail_branch Table
            Assert.AreEqual(9, dsInventory.Tables["ttavail_branch"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttavail_branch.txt"))
            //{
            //    fileWriter = File.CreateText("ttavail_branch.txt");
            //}
            //for (int x = 0; x < 9; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["ttavail_branch"].Columns[x].ColumnName + "\", dsInventory.Tables[\"ttavail_branch\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("branch_id", dsInventory.Tables["ttavail_branch"].Columns[0].ColumnName);
            Assert.AreEqual("item_ptr", dsInventory.Tables["ttavail_branch"].Columns[1].ColumnName);
            Assert.AreEqual("item_ptr_sysid", dsInventory.Tables["ttavail_branch"].Columns[2].ColumnName);
            Assert.AreEqual("qty_text", dsInventory.Tables["ttavail_branch"].Columns[3].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["ttavail_branch"].Columns[4].ColumnName);
            Assert.AreEqual("uom", dsInventory.Tables["ttavail_branch"].Columns[5].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["ttavail_branch"].Columns[6].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["ttavail_branch"].Columns[7].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["ttavail_branch"].Columns[8].ColumnName);
        }

        /// <summary>
        /// HTestDTttavail.
        /// Assets all fields and the order of the fields
        /// in the ttavail table of the Inventory DataSet.
        /// 4 fields.
        /// </summary>
        [Test]
        public void HTestDTttavail()
        {
            //ttavail Table
            Assert.AreEqual(8, dsInventory.Tables["ttavail"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("ttavail.txt"))
            //{
            //    fileWriter = File.CreateText("ttavail.txt");
            //}
            //for (int x = 0; x < 8; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["ttavail"].Columns[x].ColumnName + "\", dsInventory.Tables[\"ttavail\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("item_ptr", dsInventory.Tables["ttavail"].Columns[0].ColumnName);
            Assert.AreEqual("item_ptr_sysid", dsInventory.Tables["ttavail"].Columns[1].ColumnName);
            Assert.AreEqual("qty_text", dsInventory.Tables["ttavail"].Columns[2].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["ttavail"].Columns[3].ColumnName);
            Assert.AreEqual("uom", dsInventory.Tables["ttavail"].Columns[4].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["ttavail"].Columns[5].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["ttavail"].Columns[6].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["ttavail"].Columns[7].ColumnName);
        }

        /// <summary>
        /// ITestDTtton_order_header.
        /// Assets all fields and the order of the fields
        /// in the tton_order_header table of the Inventory DataSet.
        /// 12 fields.
        /// </summary>
        [Test]
        public void ITestDTtton_order_header()
        {
            //tton_order_header Table
            Assert.AreEqual(16, dsInventory.Tables["tton_order_header"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("tton_order_header.txt"))
            //{
            //    fileWriter = File.CreateText("tton_order_header.txt");
            //}
            //for (int x = 0; x < 16; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["tton_order_header"].Columns[x].ColumnName + "\", dsInventory.Tables[\"tton_order_header\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("PARENT_key", dsInventory.Tables["tton_order_header"].Columns[0].ColumnName);
            Assert.AreEqual("branch_id", dsInventory.Tables["tton_order_header"].Columns[1].ColumnName);
            Assert.AreEqual("system_id", dsInventory.Tables["tton_order_header"].Columns[2].ColumnName);
            Assert.AreEqual("item_ptr", dsInventory.Tables["tton_order_header"].Columns[3].ColumnName);
            Assert.AreEqual("item_ptr_sysid", dsInventory.Tables["tton_order_header"].Columns[4].ColumnName);
            Assert.AreEqual("tran_id", dsInventory.Tables["tton_order_header"].Columns[5].ColumnName);
            Assert.AreEqual("tran_sysid", dsInventory.Tables["tton_order_header"].Columns[6].ColumnName);
            Assert.AreEqual("tran_type", dsInventory.Tables["tton_order_header"].Columns[7].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["tton_order_header"].Columns[8].ColumnName);
            Assert.AreEqual("stocking_uom", dsInventory.Tables["tton_order_header"].Columns[9].ColumnName);
            Assert.AreEqual("expect_date", dsInventory.Tables["tton_order_header"].Columns[10].ColumnName);
            Assert.AreEqual("due_date", dsInventory.Tables["tton_order_header"].Columns[11].ColumnName);
            Assert.AreEqual("po_status", dsInventory.Tables["tton_order_header"].Columns[12].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["tton_order_header"].Columns[13].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["tton_order_header"].Columns[14].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["tton_order_header"].Columns[15].ColumnName);
        }

        /// <summary>
        /// JTestDTtton_order_detail.
        /// Assets all fields and the order of the fields
        /// in the tton_order_detail table of the Inventory DataSet.
        /// 10 fields.
        /// </summary>
        [Test]
        public void JTestDTtton_order_detail()
        {
            //tton_order_detail Table
            Assert.AreEqual(11, dsInventory.Tables["tton_order_detail"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("tton_order_detail.txt"))
            //{
            //    fileWriter = File.CreateText("tton_order_detail.txt");
            //}
            //for (int x = 0; x < 11; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["tton_order_detail"].Columns[x].ColumnName + "\", dsInventory.Tables[\"tton_order_detail\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("PARENT_key", dsInventory.Tables["tton_order_detail"].Columns[0].ColumnName);
            Assert.AreEqual("system_id", dsInventory.Tables["tton_order_detail"].Columns[1].ColumnName);
            Assert.AreEqual("item_ptr", dsInventory.Tables["tton_order_detail"].Columns[2].ColumnName);
            Assert.AreEqual("tran_id", dsInventory.Tables["tton_order_detail"].Columns[3].ColumnName);
            Assert.AreEqual("tran_sysid", dsInventory.Tables["tton_order_detail"].Columns[4].ColumnName);
            Assert.AreEqual("tran_type", dsInventory.Tables["tton_order_detail"].Columns[5].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["tton_order_detail"].Columns[6].ColumnName);
            Assert.AreEqual("stocking_uom", dsInventory.Tables["tton_order_detail"].Columns[7].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["tton_order_detail"].Columns[8].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["tton_order_detail"].Columns[9].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["tton_order_detail"].Columns[10].ColumnName);
        }

        /// <summary>
        /// KTestDTtton_order_branch.
        /// Assets all fields and the order of the fields
        /// in the tton_order_branch table of the Inventory DataSet.
        /// 5 fields.
        /// </summary>
        [Test]
        public void KTestDTtton_order_branch()
        {
            //tton_order_branch Table
            Assert.AreEqual(8, dsInventory.Tables["tton_order_branch"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("tton_order_branch.txt"))
            //{
            //    fileWriter = File.CreateText("tton_order_branch.txt");
            //}
            //for (int x = 0; x < 8; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["tton_order_branch"].Columns[x].ColumnName + "\", dsInventory.Tables[\"tton_order_branch\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("branch_id", dsInventory.Tables["tton_order_branch"].Columns[0].ColumnName);
            Assert.AreEqual("item_ptr", dsInventory.Tables["tton_order_branch"].Columns[1].ColumnName);
            Assert.AreEqual("item_ptr_sysid", dsInventory.Tables["tton_order_branch"].Columns[2].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["tton_order_branch"].Columns[3].ColumnName);
            Assert.AreEqual("uom", dsInventory.Tables["tton_order_branch"].Columns[4].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["tton_order_branch"].Columns[5].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["tton_order_branch"].Columns[6].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["tton_order_branch"].Columns[7].ColumnName);
        }

        /// <summary>
        /// LTestDTtton_order.
        /// Assets all fields and the order of the fields
        /// in the tton_order table of the Inventory DataSet.
        /// 4 fields.
        /// </summary>
        [Test]
        public void LTestDTtton_order()
        {
            //tton_order Table
            Assert.AreEqual(7, dsInventory.Tables["tton_order"].Columns.Count);

            ////To build fields test...
            //StreamWriter fileWriter = null;
            //if (!File.Exists("tton_order.txt"))
            //{
            //    fileWriter = File.CreateText("tton_order.txt");
            //}
            //for (int x = 0; x < 7; x++)
            //{
            //    fileWriter.Write("Assert.AreEqual(\"" + dsInventory.Tables["tton_order"].Columns[x].ColumnName + "\", dsInventory.Tables[\"tton_order\"].Columns[" + x.ToString().Trim() + "].ColumnName);" + "\x000D\x000A");
            //}
            //fileWriter.Close();

            Assert.AreEqual("item_ptr", dsInventory.Tables["tton_order"].Columns[0].ColumnName);
            Assert.AreEqual("item_ptr_sysid", dsInventory.Tables["tton_order"].Columns[1].ColumnName);
            Assert.AreEqual("qty", dsInventory.Tables["tton_order"].Columns[2].ColumnName);
            Assert.AreEqual("uom", dsInventory.Tables["tton_order"].Columns[3].ColumnName);
            Assert.AreEqual("thickness", dsInventory.Tables["tton_order"].Columns[4].ColumnName);
            Assert.AreEqual("WIDTH", dsInventory.Tables["tton_order"].Columns[5].ColumnName);
            Assert.AreEqual("LENGTH", dsInventory.Tables["tton_order"].Columns[6].ColumnName);
        }

        /// <summary>
        /// MTestDTttProductMajor.
        /// Assets all fields and the order of the fields
        /// in the ttProductMajor table of the Product Group DataSet.
        /// 3 fields.
        /// </summary>
        [Test]
        public void MTestDTttProductMajor()
        {
            //ttProductMajor Table
            Assert.AreEqual(3, dsProductGroup.Tables["ttProductMajor"].Columns.Count);

            Assert.AreEqual("system_id", dsProductGroup.Tables["ttProductMajor"].Columns[0].Caption);
            Assert.AreEqual("major", dsProductGroup.Tables["ttProductMajor"].Columns[1].Caption);
            Assert.AreEqual("major_description", dsProductGroup.Tables["ttProductMajor"].Columns[2].Caption);
        }

        /// <summary>
        /// NTestDTttProductMinor.
        /// Assets all fields and the order of the fields
        /// in the ttProductMinor table of the Product Group DataSet.
        /// 5 fields.
        /// </summary>
        [Test]
        public void NTestDTttProductMinor()
        {
            //ttProductMinor Table
            Assert.AreEqual(5, dsProductGroup.Tables["ttProductMinor"].Columns.Count);

            Assert.AreEqual("system_id", dsProductGroup.Tables["ttProductMinor"].Columns[0].Caption);
            Assert.AreEqual("major", dsProductGroup.Tables["ttProductMinor"].Columns[1].Caption);
            Assert.AreEqual("minor", dsProductGroup.Tables["ttProductMinor"].Columns[2].Caption);
            Assert.AreEqual("majorminor", dsProductGroup.Tables["ttProductMinor"].Columns[3].Caption);
            Assert.AreEqual("DESCRIPTION", dsProductGroup.Tables["ttProductMinor"].Columns[4].Caption);
        }
    }
}
