<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserMonitor.aspx.cs" Inherits="UserMonitor"
    Title="User Login Count" %>

<%--Once we are ready for automating the retrieval of user login, comment out the content of the this page.--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>DMSi Software</title>
</head>
<form id="form1" runat="server">
<div style="padding: 5px">
    <p>
        <asp:label runat="server" text="PartnerView and PartnerView Mobile Usage" font-names="Helvatica,Verdana,Tahoma,Arial;"
            font-size="Medium"></asp:label>
    </p>
    <table style="width: 100%;">
        <tr>
            <td nowrap="nowrap">
                <p>
                    <asp:label id="lblCurrent" runat="server" text="Current" style="font-weight: bold"></asp:label>
                </p>
                <asp:gridview id="gvLogins1" runat="server" autogeneratecolumns="False" onrowdatabound="gvLogins_RowDataBound"
                    pagesize="32000" width="100%">
                <Columns>
                    <asp:BoundField DataField="product" HeaderText="Product" 
                        SortExpression="product" ReadOnly="True">
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="category" HeaderText="Category" ReadOnly="True" 
                        SortExpression="category">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="login_count" HeaderText="Login Count" 
                        ReadOnly="True" SortExpression="login_count">
                        <HeaderStyle Wrap="False" />
                    </asp:BoundField>
                </Columns>
            </asp:gridview>
            </td>
            <td nowrap="nowrap">
                <p>
                    <asp:label id="lblLast" runat="server" text="Last Month" style="font-weight: bold"></asp:label>
                </p>
                <asp:gridview id="gvLogins2" runat="server" autogeneratecolumns="False" onrowdatabound="gvLogins_RowDataBound"
                    pagesize="32000" width="100%">
                    <Columns>
                        <asp:BoundField DataField="product" HeaderText="Product" 
                            SortExpression="product" ReadOnly="True">
                            <HeaderStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="category" HeaderText="Category" ReadOnly="True" 
                            SortExpression="category">
                            <HeaderStyle Wrap="False" />
                            <ItemStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="login_count" HeaderText="Login Count" 
                            ReadOnly="True" SortExpression="login_count">
                            <HeaderStyle Wrap="False" />
                        </asp:BoundField>
                    </Columns>
                </asp:gridview>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                <p>
                    <asp:label id="lbl2Months" runat="server" text="2 Months Prior" style="font-weight: bold"></asp:label>
                </p>
                <asp:gridview id="gvLogins3" runat="server" autogeneratecolumns="False" onrowdatabound="gvLogins_RowDataBound"
                    pagesize="32000" width="100%">
                    <Columns>
                        <asp:BoundField DataField="product" HeaderText="Product" 
                            SortExpression="product" ReadOnly="True">
                            <HeaderStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="category" HeaderText="Category" ReadOnly="True" 
                            SortExpression="category">
                            <HeaderStyle Wrap="False" />
                            <ItemStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="login_count" HeaderText="Login Count" 
                            ReadOnly="True" SortExpression="login_count">
                            <HeaderStyle Wrap="False" />
                        </asp:BoundField>
                    </Columns>
                </asp:gridview>
            </td>
            <td nowrap="nowrap">
                <p>
                    <asp:label id="lbl3Months" runat="server" text="3 Months Prior" style="font-weight: bold"></asp:label>
                </p>
                <asp:gridview id="gvLogins4" runat="server" autogeneratecolumns="False" onrowdatabound="gvLogins_RowDataBound"
                    pagesize="32000" width="100%">
                    <Columns>
                        <asp:BoundField DataField="product" HeaderText="Product" 
                            SortExpression="product" ReadOnly="True">
                            <HeaderStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="category" HeaderText="Category" ReadOnly="True" 
                            SortExpression="category">
                            <HeaderStyle Wrap="False" />
                            <ItemStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="login_count" HeaderText="Login Count" 
                            ReadOnly="True" SortExpression="login_count">
                            <HeaderStyle Wrap="False" />
                        </asp:BoundField>
                    </Columns>
                </asp:gridview>
            </td>
        </tr>
    </table>
</div>
</form>
</html>
