﻿using System;
using System.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Infragistics.Web.UI.NavigationControls;

public partial class Settings : BasePage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        WebDataMenu1.StyleSetName = System.Configuration.ConfigurationManager.AppSettings["Theme"];

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        if (user != null && user.GetContextValue("currentUseDebug") != null && user.GetContextValue("currentUseDebug").ToLower().Equals("yes"))
        {
            NavItem nitem = WebDataMenu1.Items.FindNavItemByText("Log Viewer");
            if (nitem != null)
                nitem.Visible = true;
        }
        else
        {
            NavItem nitem = WebDataMenu1.Items.FindNavItemByText("Log Viewer");
            if (nitem != null)
                nitem.Visible = false;

        }

        if (dsPV != null &&
           dsPV.bPV_Action != null &&
           dsPV.bPV_Action.Rows.Count > 0 &&
           user.GetContextValue("UserType").ToLower() == "internal")
        {
            bool readOnly = false;
            if (string.IsNullOrEmpty(lblSttingName.Text))
            {
            lblSttingName.Text = "Settings";
            }


            foreach (DataRow row in dsPV.bPV_Action.Rows)
            {
                if (row["token_code"].ToString() == "read_only_content_msgs")
                {
                    readOnly = true;
                    break;
                }
            }

            if (!readOnly)
            {
                NavItem nitem = WebDataMenu1.Items.FindNavItemByText("Messages");
                if (nitem != null)
                    nitem.Visible = true;
            }
        }

        if ((WebDataMenu1.Items.FindNavItemByText("Log Viewer")).Visible == false && (WebDataMenu1.Items.FindNavItemByText("Messages")).Visible == false)
        {
            (WebDataMenu1.Items.FindNavItemByText("Change Password")).Visible = false;
            PasswordMaintSettings.Visible = true;
        }
    }

    protected void lnkPasswordMaint_Click(object sender, EventArgs e)
    {
        PasswordMaintSettings.Visible = true;
        LogView1.Visible = false;
        MessagesMaint1.Visible = false;
    }

    protected void lnkLogViewer_Click(object sender, EventArgs e)
    {
        PasswordMaintSettings.Visible = false;
        LogView1.Visible = true;
        hdnlogview.Value = "1";
        MessagesMaint1.Visible = false;
    }

    protected void lnkMessages_Click(object sender, EventArgs e)
    {
        PasswordMaintSettings.Visible = false;
        LogView1.Visible = false;
        MessagesMaint1.Visible = true;
    }

    protected void lnkHome_Click(object sender, EventArgs e)
    {
        Redirect();
    }

    private void Redirect()
    {
        int PasswordReturnCode = Convert.ToInt32(Session["passRC"]);
        string urlName = Convert.ToString(Session["Pagefrom"]);

        switch (PasswordReturnCode)
        {
            case -30:
            case -40:
                Response.Redirect("./TimedOut.aspx");
                break;

            case -50:
                Session["passRC"] = 0;
                Session["LoggedIn"] = true;
                Response.Redirect("default.aspx?Display=Customers");
                break;

            default:
                Session["IsHome"] = true;
                Response.Redirect("default.aspx?Display=Home");
                break;

        }
    }

    protected void WebDataMenu1_ItemClick(object sender, Infragistics.Web.UI.NavigationControls.DataMenuItemEventArgs e)
    {
        AnalyticsInput aInput = new AnalyticsInput();
        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);

        switch (e.Item.Text.ToLower())
        {
            case "home":
                
                Redirect();
                break;

            case "change password":
                lblSttingName.Text = "Change Password";
                PasswordMaintSettings.Visible = true;
                LogView1.Visible = false;
                MessagesMaint1.Visible = false;
                ChangeTheme1.Visible = false;
                
                break;

            case "log viewer":
                lblSttingName.Text = "Log Viewer";
                PasswordMaintSettings.Visible = false;
                LogView1.Visible = true;
                MessagesMaint1.Visible = false;
                hdnlogview.Value = "1";
                ChangeTheme1.Visible = false;

                break;
           
            case "messages":
                lblSttingName.Text = "Messages";
                PasswordMaintSettings.Visible = false;
                LogView1.Visible = false;
                MessagesMaint1.Visible = true;
                ChangeTheme1.Visible = false;

                break;

            case "change theme":
                lblSttingName.Text = "Select Theme";
                 PasswordMaintSettings.Visible = false;
                LogView1.Visible = false;
                MessagesMaint1.Visible = false;
                ChangeTheme1.Visible = true;

                break;
        }
    }
}