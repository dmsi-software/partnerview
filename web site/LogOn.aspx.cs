using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Web.UI;

public partial class LogOn : BasePage
{
    private Progress.Open4GL.Proxy.Connection _ProgressConnection;
    private Dmsi.Agility.EntryNET.EntryNET _ProgressApplicationObject;
    protected DataManager _dataManager;
    protected ExceptionManager _exceptionManager;
    protected EventLog _eventLog;
    protected SecurityManager _securityManager;

    protected string _errorMessageParam;
    protected string _errorMessageCatch;
    protected int _passwordRC;
    [DefaultValue(null)]
    public Progress.Open4GL.Proxy.Connection ProgressConnection
    {
        get { return _ProgressConnection; }
        set { _ProgressConnection = value; }
    }

    /// <summary>
    /// ASP application SHOULD cache this
    /// </summary>
    [DefaultValue(null)]
    public Dmsi.Agility.EntryNET.EntryNET ProgressApplicationObject
    {
        get { return _ProgressApplicationObject; }
        set { _ProgressApplicationObject = value; }
    }
    protected void Page_Init(object sender, EventArgs e)
    {

        Page.ClientTarget = "uplevel";
        if (Session["GenericLogin"] != null)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                if ((string)Session["IsCustomTheme"] == "True")
                {

                    Session["disable_theme_authentication"] = "true";
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        DataManager dm = new DataManager();

        if (Session["LoggedIn"] == null && Session["UserName"] != null || (Session["UserName"] != null && (string)Session["UserName"] == System.Configuration.ConfigurationManager.AppSettings["ProductSearchUserName"].ToString()))
        {
            AnalyticsInput aInput = new AnalyticsInput();
            aInput.Type = "event";
            aInput.Action = "Login link on Public Catalog";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

        if (Session["LoggedIn"] != null && Session["UserName"] != null && (string)Session["UserName"] != System.Configuration.ConfigurationManager.AppSettings["ProductSearchUserName"].ToString())
        {
            AnalyticsInput aInput = new AnalyticsInput();
            aInput.Type = "event";
            aInput.Action = "Logout Click";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

        string isCustomtheme = null;
        if (Session["LoggedIn"] == null)
        {
            Session["LoggedIn"] = false;
        }
        if (Request.QueryString["Logout"] != null)
        {
            Session["UserName"] = null;


        }
        if (!string.IsNullOrEmpty((string)Session["UserName"]) && ((bool)Session["LoggedIn"] == true))
        {

            Response.Redirect("~/Default.aspx");


        }

        else
        {

            if (Session["SavedProductGroupMajorGUID"] != null)
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

                if (fi.Exists)
                {
                    fi.Delete();
                }
            }

            Session["SavedProductGroupMajorGUID"] = null; //Reset, force Product Group Major control to reload.


            if ((string)Session["GenericLogin"] != "True")
            {
                if (Session["IsCustomTheme"] != null)
                {
                    isCustomtheme = (string)Session["IsCustomTheme"];
                }

                if ((string)dm.GetCache("opcContextId") != null)
                {
                    try
                    {
                        int? ReturnCode = null;
                        string MessageText = null;

                        com.dmsi.agility.SessionService session = new com.dmsi.agility.SessionService();

                        session.Logout((string)dm.GetCache("opcContextId"), out ReturnCode, out MessageText);

                    }
                    catch { }
                }

                // If coming from Password maintenance and the user did change the password,
                // don't reset the settings.
                if (Session["ChangePasswordBeforeLoginSuccessful"] == null)
                {
                    Dmsi.Agility.EntryNET.Connection connection = (Dmsi.Agility.EntryNET.Connection)dm.GetCache("AgilityLibraryConnection");

                    if (connection != null)
                        connection.Disconnect();
                    connection = null;

                    if (Session["IsCustomTheme"] == null)
                    {

                        Session["IsCustomTheme"] = isCustomtheme;
                    }
                }
            }
            else
            {

                if (Session["ChangeTheme"] != null)
                {


                }
            }
        }
    }
}


