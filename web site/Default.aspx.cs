using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Configuration;
using System.Web.Profile;
using System.Threading;

public partial class _Default : BasePage
{
    protected DataManager _dataManager;
    protected ExceptionManager _exceptionManager;
    protected EventLog _eventLog;
    protected SecurityManager _securityManager;

    protected string _errorMessageParam;
    protected string _errorMessageCatch;
    protected int _passwordRC;
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";

        if ((string)Session["IsThemeChanged"] == "yes")
        {
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Cache-Control", "no-cache");

            Response.CacheControl = "no-cache";
            Response.Expires = -1;

            Response.ExpiresAbsolute = new DateTime(1900, 1, 1);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (hdnHelpClick.Value != "")
        {
            hdnHelpClick.Value = "";

            AnalyticsInput aInput = new AnalyticsInput();
            aInput.Type = "event";
            aInput.Action = "User Menu - Help Link Click";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

        if (Page.IsPostBack)
        {
            this.MaintainScrollPositionOnPostBack = true;
        }

        if (!IsPostBack)
        {
            if (Request.Url.ToString().ToLower().Contains("http:") && System.Configuration.ConfigurationManager.AppSettings["ForceHTTPS"].ToLower() == "true")
            {
                if ((Session["LoggedIn"] == null) || ((bool)Session["LoggedIn"] != true))
                {
                    Response.Redirect(Request.Url.ToString().ToLower().Replace("http:", "https:"));
                }
            }
            else
            {
                if (System.Configuration.ConfigurationManager.AppSettings["StartProductSearchFirst"].ToLower() == "true")
                {
                    if ((Session["LoggedIn"] == null) || ((bool)Session["LoggedIn"] != true))
                    {
                        Response.Redirect("~/StartPage.aspx");
                    }
                }

                if (Session["QuoteEditVisible"] != null && (bool)Session["QuoteEditVisible"] == true)
                {
                    Session["QuoteEditVisible"] = null;
                    MainTabControl1.AllTabsOff();
                }

                if (Session["QuoteReleaseVisible"] != null && (bool)Session["QuoteReleaseVisible"] == true)
                {
                    Session["QuoteReleaseVisible"] = null;
                    MainTabControl1.AllTabsOff();
                }

                if (Session["RetainpriorLogin_SelectedDetails"] != null)
                {
                    if ((int)Session["RetainpriorLogin_SelectedDetails"] == 2)
                    {
                        return;
                    }
                }
            }
            if (LoggedIn())
            {
                GenerateSessionProductXML();
            }
        }

        this.MaintainScrollPositionOnPostBack = true;
    }

    /// <summary>
    /// Finds a Control recursively. Note finds the first match that exists...
    /// </summary>
    /// <param name="Root"></param>
    /// <param name="Id"></param>
    /// <returns></returns>
    private Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

        return null;
    }

    protected void cmdPrice_Command(object sender, CommandEventArgs e)
    {
        if ((bool)Session["PriceExpanded"])
        {
            Session["PriceExpanded"] = false;
            //hide price section
        }
        else
        {
            Session["PriceExpanded"] = true;
            //show price section
        }
    }

    [System.Web.Services.WebMethod]
    public static string GetControlid(string sender)
    {
        string[] SplitstringDetails;
        SplitstringDetails = sender.Split('_');
        int iLength = SplitstringDetails.Length;
        if (iLength > 0)
        {
            sender = SplitstringDetails[iLength - 1].ToString();
        }

        if (sender == "btnAddAll2")
        {
            HttpContext.Current.Session["InventorybtnReturnToItem_Click"] = 2;
            HttpContext.Current.Session["QuoteEditAddItemsbtnReturnToItem_Click"] = 2;
        }
        else if (sender == "btnViewQuickList")
        {
            HttpContext.Current.Session["InventorybtnReturnToItem_Click"] = 3;
            HttpContext.Current.Session["QuoteEditAddItemsbtnReturnToItem_Click"] = 3;
        }
        else if (sender.StartsWith("btnCancel"))
        {
            if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
                HttpContext.Current.Session["UserPref_DisplaySetting"] != null && (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
            {
                System.Web.Profile.ProfileBase.Properties["price_Markup_Factor_List"].DefaultValue = HttpContext.Current.Session["MarkupFactor"];
            }
            else
                System.Web.Profile.ProfileBase.Properties["price_Markup_Factor"].DefaultValue = HttpContext.Current.Session["MarkupFactor"];

            System.Web.Profile.ProfileBase.Properties["display_Setting"].DefaultValue = HttpContext.Current.Session["UserPref_DisplaySetting"];
            System.Web.Profile.ProfileBase.Properties["retain_Display"].DefaultValue = HttpContext.Current.Session["UserPref_Retain"];
        }
        //if price has been changed from price to retail or vice versa
        else if (sender.StartsWith("RetainPrevious"))
        {
            bool retainDisplay1 = true;
            bool dataHasChanged = true;

            string displaySetting = string.Empty;
            decimal markupFactor = 0;
            string PriceType = string.Empty;
            string[] SplitPriceDetails;
            SplitPriceDetails = sender.Split('+');

            if (SplitPriceDetails != null)
            {
                if (SplitPriceDetails.Length > 0)
                {
                    retainDisplay1 = Convert.ToBoolean(SplitPriceDetails[2].ToString());
                    markupFactor = Convert.ToDecimal(SplitPriceDetails[3].ToString());
                    displaySetting = SplitPriceDetails[1].ToString();

                    if (dataHasChanged == true)
                    {
                        HttpContext.Current.Session["InventorybtnReturnToItem_Click"] = 9;
                        HttpContext.Current.Session["QuoteEditAddItemsbtnReturnToItem_Click"] = 9;
                    }

                    HttpContext.Current.Session["UserPref_CheckboxRetain"] = SplitPriceDetails[2].ToString();
                    HttpContext.Current.Session["UserPref_NumericMarkup"] = SplitPriceDetails[3].ToString();

                    if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
                        HttpContext.Current.Session["UserPref_DisplaySetting"] != null && (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                    {
                        System.Web.Profile.ProfileBase.Properties["price_Markup_Factor_List"].DefaultValue = markupFactor;
                    }
                    else
                        System.Web.Profile.ProfileBase.Properties["price_Markup_Factor"].DefaultValue = markupFactor;

                    if (SplitPriceDetails[1].ToString() == "Retail price" || SplitPriceDetails[1].ToString() == "Retail price - markup on cost" || SplitPriceDetails[1].ToString() == "Retail price - discount from list")
                    {
                        HttpContext.Current.Session["UserPref_PiceDisplaySetting"] = SplitPriceDetails[1].ToString();
                        PriceType = "Retail price";
                    }
                    else if (SplitPriceDetails[1].ToString() == "Net price")
                    {
                        HttpContext.Current.Session["UserPref_PiceDisplaySetting"] = "Net price";

                        PriceType = "Price";
                    }
                }
            }
        }
        else if (sender.StartsWith("btnOK"))
        {
            bool retainDisplay1 = true;
            bool dataHasChanged = true;

            string displaySetting = string.Empty;
            decimal markupFactor = 0;
            string PriceType = string.Empty;

            string[] SplitPriceDetails;
            SplitPriceDetails = sender.Split('+');

            if (SplitPriceDetails != null)
            {
                if (SplitPriceDetails.Length > 0)
                {
                    retainDisplay1 = Convert.ToBoolean(SplitPriceDetails[2].ToString());
                    markupFactor = Convert.ToDecimal(SplitPriceDetails[3].ToString());
                    displaySetting = SplitPriceDetails[1].ToString();

                    if (dataHasChanged == true)
                    {
                        HttpContext.Current.Session["InventorybtnReturnToItem_Click"] = 7;
                        HttpContext.Current.Session["QuoteEditAddItemsbtnReturnToItem_Click"] = 7;
                    }

                    HttpContext.Current.Session["UserPref_CheckboxRetain"] = SplitPriceDetails[2].ToString();
                    HttpContext.Current.Session["UserPref_NumericMarkup"] = SplitPriceDetails[3].ToString();

                    if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
                        HttpContext.Current.Session["UserPref_DisplaySetting"] != null && (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                    {
                        System.Web.Profile.ProfileBase.Properties["price_Markup_Factor_List"].DefaultValue = markupFactor;
                    }
                    else
                        System.Web.Profile.ProfileBase.Properties["price_Markup_Factor"].DefaultValue = markupFactor;

                    if (SplitPriceDetails[1].ToString() == "Retail price" ||
                        SplitPriceDetails[1].ToString() == "Retail price - markup on cost" ||
                        SplitPriceDetails[1].ToString() == "Retail price - discount from list")
                    {
                        HttpContext.Current.Session["UserPref_PiceDisplaySetting"] = SplitPriceDetails[1].ToString();
                        PriceType = "Retail price";
                    }
                    else if (SplitPriceDetails[1].ToString() == "Net price")
                    {
                        HttpContext.Current.Session["UserPref_PiceDisplaySetting"] = "Net price";

                        PriceType = "Price";
                    }
                }
            }
        }

        return sender;
    }

    protected void btnFetchTicket_Click(object sender, EventArgs e)
    {
        Session["TicketUsed"] = true;
    }

    private void GenerateSessionProductXML()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.Data.ProductGroups productGroups = null;
        ProductMajorClassGenerator xmlGenerator = new ProductMajorClassGenerator(dm, productGroups);

        if (!xmlGenerator.GetProductGroups())
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        xmlGenerator.GenerateXMLFileForSession();
        xmlGenerator.SetSessionVariable();

        if (Session["SavedProductGroupMajorGUID"] != null)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

            if (fi.Exists)
            {
                fi.Delete();
            }
        }

        xmlGenerator.dsPG.WriteXml(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");
    }

    private bool LoggedIn()
    {
        return ((Session["LoggedIn"] != null) && ((bool)Session["LoggedIn"] == true));
    }
}
