﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class PreConfigurator : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)dm.GetCache("dsSessionManager");
        string currentBranch = (string)dm.GetCache("currentBranchID");
        string SessionContextId = (string)dm.GetCache("opcContextId");
        string Url = System.Configuration.ConfigurationManager.AppSettings["com.dmsi.agility.Session"].ToString();

        string pageURL = System.Configuration.ConfigurationManager.AppSettings["VC_URL"].ToString() + "?parent=preconfigurator&url=" + Url +
                "&context=" + SessionContextId +
                "&currentbranch=" + HttpUtility.UrlEncode(currentBranch) +
                "&item=" + HttpUtility.UrlEncode(Request.QueryString["item"]) +
                "&size=" + HttpUtility.UrlEncode(Request.QueryString["size"]) +
                "&description=" + HttpUtility.UrlEncode(Request.QueryString["description"]) +
                "&uom=" + HttpUtility.UrlEncode(Request.QueryString["uom"]) +
                "&universalOptions=" + HttpUtility.UrlEncode(Request.QueryString["universalOptions"]);

        string custcode = "";
        string shipto = "";
        string shiptoname = "";
        string saleType = "";
        string locationReference = "";

        decimal onTheFlyMarkup = 1;

        dsQuoteDataSet dsQuote;

        if (Request.QueryString["source"] != null)
        {
            pageURL += "&source=" + Request.QueryString["source"];

            if (Request.QueryString["source"].ToLower() == "pvquote")
            {
                if (Session["QuoteEdit_dsQuote"] != null)
                {
                    dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];

                    pageURL += "&quote_id=" + dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();

                    custcode = dsQuote.pvttquote_header.Rows[0]["cust_code"].ToString();
                    shipto = dsQuote.pvttquote_header.Rows[0]["shipto_seq_num"].ToString();
                    saleType = dsQuote.pvttquote_header.Rows[0]["sale_type"].ToString();
                    shiptoname = dsQuote.pvttquote_header.Rows[0]["shiptoname"].ToString();

                    if (Request.QueryString["mode"].ToLower() == "update")
                    {
                        DataRow[] rows = dsQuote.pvttquote_detail.Select("sequence =" + Request.QueryString["sequence"].ToString());

                        locationReference = rows[0]["location_reference"] as string;

                        if (Session["UserPref_DisplaySetting"] != null &&
                           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost"))
                        {
                            if (rows.Length > 0 && Convert.ToDecimal(rows[0]["net_price"]) > 0)
                                onTheFlyMarkup = Convert.ToDecimal(rows[0]["retail_price"]) / Convert.ToDecimal(rows[0]["net_price"]);
                        }
                        //else if(Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Net price")
                        //{
                        //    if (rows.Length > 0 && Convert.ToDecimal(rows[0]["net_price"]) > 0)
                        //        onTheFlyMarkup = Convert.ToDecimal(rows[0]["retail_price"]) / Convert.ToDecimal(rows[0]["net_price"]);
                        //}
                        else
                        {
                            if (rows.Length > 0 && Convert.ToDecimal(rows[0]["retail_listbased_price"]) > 0)
                                onTheFlyMarkup = Convert.ToDecimal(rows[0]["retail_price"]) / Convert.ToDecimal(rows[0]["retail_listbased_price"]);
                        }
                    }

                    if (Request.QueryString["mode"].ToLower() == "create")
                    {
                        onTheFlyMarkup = (decimal)Session["MarkupFactor"];
                    }
                }
            }
        }
        else
        {
            locationReference = Request.QueryString["locref"];

            DataRow rowCust = (DataRow)dm.GetCache("CustomerRowForInformationPanel");
            custcode = rowCust["cust_code"].ToString();

            DataRow rowShip = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
            shipto = rowShip["seq_num"].ToString();
            shiptoname = rowShip["shipto_name"].ToString();

            if (dm.GetCache("SaleType") == null)
                saleType = "default"; // the back-end will try to use the default sale type to fetch prices else <All>
            else
                saleType = (string)dm.GetCache("SaleType");
        }

        pageURL += "&custcode=" + HttpUtility.UrlEncode(custcode) + "&custshipto=" + shipto + "&salestype=" + HttpUtility.UrlEncode(saleType) + "&shiptoname=" + HttpUtility.UrlEncode(shiptoname);

        if (Request.QueryString["mode"] != null)
        {
            pageURL += "&mode=" + HttpUtility.UrlEncode(Request.QueryString["mode"]);
        }

        if (Request.QueryString["sequence"] != null)
        {
            pageURL += "&sequence=" + Request.QueryString["sequence"];
        }

        if (Request.QueryString["orderqty"] != null)
        {
            pageURL += "&orderqty=" + Request.QueryString["orderqty"];
        }

        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Net price")
        {
            pageURL += "&applymarkupfactor=false";

            if (Request.QueryString["source"] != null && Request.QueryString["source"].ToLower() == "pvquote")
            {
                pageURL += "&markupfactor=" + onTheFlyMarkup.ToString();
            }
            else if (Session["MarkupFactor"] != null)
            {
                pageURL += "&markupfactor=" + ((decimal)Session["MarkupFactor"]).ToString();
            }
        }
        else if (Session["UserPref_DisplaySetting"] != null &&
                ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                 (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                 (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            pageURL += "&applymarkupfactor=true";

            if ((string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost")
                pageURL += "&retailmode=Net";

            if ((string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                pageURL += "&retailmode=List";

            if (Request.QueryString["source"] != null && Request.QueryString["source"].ToLower() == "pvquote")
            {
                 pageURL += "&markupfactor=" + onTheFlyMarkup.ToString();
            }
            else if (Session["MarkupFactor"] != null)
            {
                pageURL += "&markupfactor=" + ((decimal)Session["MarkupFactor"]).ToString();
            }
        }

        if (Session["VC_view_component_prices"] != null && (bool)Session["VC_view_component_prices"] == true)
            pageURL += "&vc_vcp=true";

        if (Session["VC_view_component_prices"] != null && (bool)Session["VC_view_component_prices"] == false)
            pageURL += "&vc_vcp=false";

        if (Session["VC_view_incomplete_parent_price"] != null && (bool)Session["VC_view_incomplete_parent_price"] == true)
            pageURL += "&vc_vipp=true";

        if (Session["VC_view_incomplete_parent_price"] != null && (bool)Session["VC_view_incomplete_parent_price"] == false)
            pageURL += "&vc_vipp=false";

        if (Session["VC_print_extended_price_on_vc_form"] != null && (bool)Session["VC_print_extended_price_on_vc_form"] == true)
            pageURL += "&printextprice=true";

        if (Session["VC_print_extended_price_on_vc_form"] != null && (bool)Session["VC_print_extended_price_on_vc_form"] == false)
            pageURL += "&printextprice=false";

        pageURL += "&locref=" + HttpUtility.UrlEncode(locationReference);

        DataRow[] row = dsSessionManager.ttbranch.Select("branch_id = '" + currentBranch + "'");

        bool vc_suppress_price = false;

        if (row.Length == 1)
            vc_suppress_price = (bool)row[0]["supp_wo_price"];

        pageURL += "&vc_suppress_price=" + vc_suppress_price.ToString().ToLower();
       
        if (Session["GenericLogin"] != null && (string)Session["GenericLogin"] == "True")
            pageURL += "&genericlogin=" + "true";
        else if (Session["GenericLogin"] != null && (string)Session["GenericLogin"] == "False")
           pageURL += "&genericlogin=" + "false";  

        Response.Redirect(pageURL);
    }
}