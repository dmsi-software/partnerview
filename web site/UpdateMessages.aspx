<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PlainMaster.master" CodeFile="UpdateMessages.aspx.cs" Inherits="UpdateMessages" Title="Update Messages" ValidateRequest="false" %>

<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.NavigationControls" TagPrefix="ig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster1" runat="Server">
    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <%--<script type="text/javascript" src="Scripts/jquery.cleditor.min.js"></script>--%>
    <script type="text/javascript" src="Scripts/summernote/summernote-lite.js?v1.0.0.3"></script>
    <script type="text/javascript" src="Scripts/summernote/plugin/imageattributes/summernote-image-attributes.js?v1.0.0.3"></script>
    <script type="text/javascript" src="Scripts/UpdateMessages.js?v1.0.0.3"> </script>
    <div id="body-container">
        <div class="mid-container">
            <div class="right-section">
                <div class="container-sec">
                    <div id="MessageViewDiv" runat="server">
                        <table width="742px">
                            <tr>
                                <td class="SimplePanelHeader2" align="left" colspan="2">
                                    <asp:Label ID="lblMessageViewHeader" runat="server" Text="Update Message" Width="100%"></asp:Label>
                                </td>
                            </tr>

                            <tr align="left">
                                <td colspan="2" align="left">
                                    <div id="RichTextEditorDiv" class="resizable" runat="server" style="width: 742px;">
                                        <p style="padding-bottom: 10px;" />
                                        <textarea name="rte" class="rte" id="rte" rows="10" cols="80" runat="server"></textarea>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Button ID="btnSaveEdit" Text="Save" runat="server" CssClass="btn btn-primary" OnClick="btnSaveEdit_Click" />
                                    <asp:Button ID="btnCancelEdit" Text="Cancel" runat="server" CssClass="btn btn-default" OnClick="btnCancelEdit_Click" />
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                        SelectCommand="SELECT Category_Name, Sequence_Number, Message_Text, Active FROM Message WHERE (Category_Name = @Category_Name) ORDER BY Category_Name, Sequence_Number"
                                        UpdateCommand="UPDATE Message SET Message_Text = @Message_Text, Active = @Active WHERE (Category_Name = @Category_Name) AND (Sequence_Number = @Sequence_Number)"
                                        DeleteCommand="DELETE FROM Message WHERE (Category_Name = @Category_Name) AND (Sequence_Number = @Sequence_Number)"
                                        InsertCommand="INSERT INTO Message(Category_Name, Sequence_Number, Message_Text) VALUES (@Category_Name, @Sequence_Number, @Message_Text)">
                                        <SelectParameters>
                                            <asp:Parameter Name="Category_Name" Type="String" />
                                        </SelectParameters>
                                        <DeleteParameters>
                                            <asp:Parameter Name="Category_Name" />
                                            <asp:Parameter Name="Sequence_Number" />
                                        </DeleteParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="Message_Text" />
                                            <asp:Parameter Name="Category_Name" />
                                            <asp:Parameter Name="Sequence_Number" />
                                            <asp:Parameter DefaultValue="False" Name="Active" />
                                        </UpdateParameters>
                                        <InsertParameters>
                                            <asp:Parameter DefaultValue="" Name="Category_Name" />
                                            <asp:Parameter Name="Sequence_Number" />
                                            <asp:Parameter Name="Message_Text" />
                                        </InsertParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
