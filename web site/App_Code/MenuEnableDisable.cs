﻿using Infragistics.Web.UI.NavigationControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public interface IMenuEnableDisable
{
    void EnableMenuItems();
    void DisableMenuItems();
}

/// <summary>
/// Summary description for MenuEnableDisable
/// </summary>
public class MenuEnableDisable
{
    public MenuEnableDisable()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void EnableMenuItems(Page page, Control control)
    {
        SetControlState(page, control, true);
    }
    public void DisableMenuItems(Page page, Control control)
    {
        SetControlState(page, control, false);
    }
    private void SetControlState(Page page, Control control, bool lEnable)
    {
        WebDataMenu menu = control.Parent.FindControl("WebDataMenu1") as WebDataMenu;
        if (menu != null)
        {
            menu.Enabled = lEnable;
            SetCSSClass(menu, lEnable);
        }

        Panel p = control.Parent.FindControl("pnlTextImage") as Panel;
        if (p != null)
        {
            p.Enabled = lEnable;
            SetCSSClass(p, lEnable);
        }

        DropDownList ddl = page.Master.FindControl("ddlBranch") as DropDownList;
        if (ddl != null)
        {
            ddl.Enabled = lEnable;
            SetCSSClass(ddl, lEnable);
        }

        HtmlGenericControl c = page.Master.FindControl("dvlabelbox") as HtmlGenericControl;
        if (c != null)
        {
            c.Disabled = !lEnable;
            SetCSSClass(c, lEnable);
        }

        HtmlImage img = page.Master.FindControl("imgCustAddress") as HtmlImage;
        if (img != null)
        {
            if (lEnable)
            {
                img.Attributes.Add("onmouseover", "ShowPopup(this)");
            }
            else
            {
                img.Attributes.Remove("onmouseover");
            }
            SetCSSClass(img, lEnable);
        }

        ImageButton button = page.Master.FindControl("imgbtnCart") as ImageButton;
        if (button != null)
        {
            button.Enabled = lEnable;
            SetCSSClass(button, lEnable);
        }

        LinkButton lnk = page.Master.FindControl("lblCart") as LinkButton;
        if (lnk != null)
        {
            lnk.Enabled = lEnable;
            SetCSSClass(lnk, lEnable);
        }

        ImageButton imgButton = control.Parent.FindControl("imgLogo") as ImageButton;
        if (imgButton != null)
        {
            imgButton.Enabled = lEnable;
            SetCSSClass(imgButton, lEnable);
        }

        HtmlAnchor acustname = page.Master.FindControl("acustname") as HtmlAnchor;
        if (acustname != null)
        {
            acustname.Disabled = !lEnable;
            SetCSSClass(acustname, lEnable);
        }
        HtmlAnchor awelcomeheader = page.Master.FindControl("awelcomeheader") as HtmlAnchor;
        if (awelcomeheader != null)
        {
            awelcomeheader.Disabled = !lEnable;
            SetCSSClass(awelcomeheader, lEnable);
        }

    }
    private void SetCSSClass(WebControl control, bool lEnable)
    {
        if (lEnable)
        {
            control.Attributes.CssStyle.Remove("notActive");
            control.Attributes.Add("class", "activatePointerEvents");
        }
        else
        {
            control.Attributes.Add("class", "notActive");
            control.Attributes.CssStyle.Remove("activatePointerEvents");
        }
    }
    private void SetCSSClass(HtmlControl control, bool lEnable)
    {
        if (lEnable)
        {
            control.Attributes.CssStyle.Remove("notActive");
            control.Attributes.Add("class", "activatePointerEvents");

        }
        else
        {
            control.Attributes.Add("class", "notActive");
            control.Attributes.CssStyle.Remove("activatePointerEvents");
        }
    }
}