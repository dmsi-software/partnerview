﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

/// <summary>
/// Summary description for WorkOrderService
/// </summary>
[WebService(Namespace = "http://dmsi.agility.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WorkOrderService : System.Web.Services.WebService
{
    public WorkOrderService() // Default ctor...
    {
    }

    [WebMethod]
    public void BOMNextStep(string ContextID, string BranchID, string ParentItem, string StepNumber, string SelectedItem, out string StepDescription, out string StepOptions)
    {
        DataSet ds = new DataSet();
        string path = Server.MapPath("~/App_Data");

        StepDescription = "";
        StepOptions = "";

        try
        {
            ds.ReadXml(path + "\\" + ParentItem + ".xml");
        }
        catch
        {
            StepDescription = "Item not found.";
            return;
        }

        DataRelation rel = new DataRelation("BomID", ds.Tables["ttOptParents"].Columns["BOM_ID"], ds.Tables["ttOptLists"].Columns["bom_id"]);
        ds.Relations.Add(rel);
        
        ds.Tables["ttOptParents"].DefaultView.Sort = "BOM_ID ASC";

        int indexNumber = Convert.ToInt32(StepNumber);

        if (indexNumber < ds.Tables["ttOptParents"].DefaultView.Count)
        {
            DataRow row = ds.Tables["ttOptParents"].DefaultView[indexNumber].Row;
            if (row["description"].ToString().Length > 0)
                StepDescription = row["description"].ToString();

            DataSet newDs = new DataSet();
            DataRow[] rows = row.GetChildRows("BomID");
            if (rows != null)
            {
                newDs.Merge(rows);
                StepOptions = newDs.GetXml();
            }
        }
    }
}
