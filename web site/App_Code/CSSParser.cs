﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using System.Web;


/// <summary>
/// Summary description for CSSParser
/// </summary>
///  [Serializable]
///  
[Serializable]
public class CSSParser : List<KeyValuePair<String, List<KeyValuePair<String, String>>>>, ICSSParser
{



    private const String _SelectorKey = "selector";
    private const String _NameKey = "name";
    private const String _ValueKey = "value";
    /// <summary>
    /// Regular expression to parse the Stylesheet
    /// </summary>
    [NonSerialized]
    private Regex _rStyles = new Regex(RegularExpressionLibrary.CSSGroups, RegexOptions.IgnoreCase | RegexOptions.Compiled);

    private string _stylesheet = String.Empty;
    private Dictionary<String, Dictionary<String, String>> _classes;
    private Dictionary<String, Dictionary<String, String>> _elements;



    /// <summary>
    /// Original Style Sheet loaded
    /// </summary>
    public String StyleSheet
    {
        get
        {
            return this._stylesheet;
        }
        set
        {
            //If the style sheet changes we will clean out any dependant data
            this._stylesheet = value;
            this.Clear();
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="CascadingStyleSheet"/> class.
    /// </summary>
    public CSSParser()
    {
        this.StyleSheet = String.Empty;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="CascadingStyleSheet"/> class.
    /// </summary>
    /// <param name="CascadingStyleSheet">The cascading style sheet.</param>
    public CSSParser(String CascadingStyleSheet)
    {
        this.Read(CascadingStyleSheet);
    }

    /// <summary>
    /// Reads the CSS file.
    /// </summary>
    /// <param name="Path">The path.</param>
    public void ReadCSSFile(String Path, DataTable ds)
    {
        this.StyleSheet = File.ReadAllText(Path);

        this.ReadcssFile(StyleSheet, ds);
    }
    /// <summary>
    /// Reads the specified cascading style sheet.
    /// </summary>
    /// <param name="CascadingStyleSheet">The cascading style sheet.</param>
    public void Read(String CascadingStyleSheet)
    {
        this.StyleSheet = CascadingStyleSheet;
        DataSet ds = new DataSet();

        if (!String.IsNullOrEmpty(CascadingStyleSheet))
        {
            //Remove comments before parsing the CSS. Don't want any comments in the collection. Don't know how iTextSharp would react to CSS Comments
            MatchCollection MatchList = _rStyles.Matches(Regex.Replace(CascadingStyleSheet, RegularExpressionLibrary.CSSComments, String.Empty));
            foreach (Match item in MatchList)
            {

                //Check for nulls
                if (item != null && item.Groups != null && item.Groups[_SelectorKey] != null && item.Groups[_SelectorKey].Captures != null && item.Groups[_SelectorKey].Captures[0] != null && !String.IsNullOrEmpty(item.Groups[_SelectorKey].Value))
                {
                    String strSelector = item.Groups[_SelectorKey].Captures[0].Value.Trim();
                    var style = new List<KeyValuePair<String, String>>();

                    for (int i = 0; i < item.Groups[_NameKey].Captures.Count; i++)
                    {
                        String className = item.Groups[_NameKey].Captures[i].Value;
                        String value = item.Groups[_ValueKey].Captures[i].Value;
                        //Check for null values in the properies
                        if (!String.IsNullOrEmpty(className) && !String.IsNullOrEmpty(value))
                        {
                            className = className.TrimWhiteSpace();
                            value = value.TrimWhiteSpace();
                            //One more check to be sure we are only pulling valid css values
                            if (!String.IsNullOrEmpty(className) && !String.IsNullOrEmpty(value))
                            {
                                style.Add(new KeyValuePair<String, String>(className, value));
                            }
                        }
                    }
                    this.Add(new KeyValuePair<String, List<KeyValuePair<String, String>>>(strSelector, style));
                }
            }
        }
    }


    public void ReadcssFile(String CascadingStyleSheet, DataTable ds1)
    {
        this.StyleSheet = CascadingStyleSheet;
        DataSet ds = new DataSet();

        if (!String.IsNullOrEmpty(CascadingStyleSheet))
        {
            //Remove comments before parsing the CSS. Don't want any comments in the collection. Don't know how iTextSharp would react to CSS Comments
            MatchCollection MatchList = _rStyles.Matches(Regex.Replace(CascadingStyleSheet, RegularExpressionLibrary.CSSComments, String.Empty));
            foreach (Match item in MatchList)
            {

                //Check for nulls
                if (item != null && item.Groups != null && item.Groups[_SelectorKey] != null && item.Groups[_SelectorKey].Captures != null && item.Groups[_SelectorKey].Captures[0] != null && !String.IsNullOrEmpty(item.Groups[_SelectorKey].Value))
                {
                    String strSelector = item.Groups[_SelectorKey].Captures[0].Value.Trim();
                    var style = new List<KeyValuePair<String, String>>();

                    for (int i = 0; i < item.Groups[_NameKey].Captures.Count; i++)
                    {
                        String className = item.Groups[_NameKey].Captures[i].Value;
                        String value = item.Groups[_ValueKey].Captures[i].Value;
                        //Check for null values in the properies
                        if (!String.IsNullOrEmpty(className) && !String.IsNullOrEmpty(value))
                        {
                            className = className.TrimWhiteSpace();
                            if (strSelector == "#header-section .top-div-nav" && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeHeaderBarBackGroundColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";


                            }
                            if (strSelector == ".SimplePanelHeader2" && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeHeaderBarBackGroundColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";


                            }
                            else if (strSelector == ".btn-default" && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }
                            else if (strSelector == ".inactivesbutton" && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }
                            else if (strSelector == ".btn-primary" && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }
                            else if ((strSelector == "ul.list li a.active") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".activesettings") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".activesettings:hover") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if (strSelector == ".btn-primary:hover" && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }
                            else if (strSelector == ".loginsbuttonprimary:hover" && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }
                            else if (strSelector == ".loginsbuttonprimary:focus" && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }
                            else if ((strSelector == ".igdt_NodeHolder a:hover") && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if (strSelector == ".loginsbuttonprimary" && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }


                            else if ((strSelector == ".custom-grid table.grid-tabler th") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".carttable th") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".custom-grid table th") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".igg_Office2007SilverHeader") && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".optional a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".brd-crumbactive") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".SettingsLinkMarginTop") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".CollapsibleAdvSearchPanel") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".pd-quick-btn") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".pd-Title a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == "#body-container .mid-container .right-section div.container-sec .brd-crumb ul li a:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".varigdt_NodeHolder a:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }


                            else if ((strSelector == ".pd-quick-btn") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }

                            else if ((strSelector == ".prodaddqlist") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".SettingsLinkMarginTop") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".login a:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".login a:focus") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".login a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".logout a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            if (strSelector == ".SimplePanelHeader2" && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";


                            }
                            else if ((strSelector == ".Cartdetails") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".dropdown-menu li .MenuLink") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".dropdown-menu > li > a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace();
                            }

                            else if ((strSelector == "#header-section .top li span") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace();
                            }

                            else if ((strSelector == "#header-section .top li span a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace();
                            }
                            else if ((strSelector == ".logout a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".caret") && (className == "color"))
                            {

                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace();
                            }
                            else if ((strSelector == ".changeAcctLink") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".igdm_DMSiBlueMenuItemHorizontalRootSelected > a") && (className == "color"))
                            {

                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".igdm_DMSiBlueMenuItemHorizontalRootActive > a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".SettingsLink") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }

                            else if ((strSelector == ".SettingsLink:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }

                            else if ((strSelector == ".igdt_NodeGroup li ul li a:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".productsconfigure") && (className == "border-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".igdm_DMSiBlueMenuItemHorizontalRoot a:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == "ul.list li a:hover") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace();
                            }
                            else if ((strSelector == ".igdm_DMSiBlueMenuItemHorizontalRootHover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".igdm_DMSiBlueMenuItemHorizontalRootHover > a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".pd-Title a:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveLinkTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".ajax__tab_default .ajax__tab_header") && (className == "border-bottom"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveTabColorandRelatedUnderlineBar" + "'");
                                value = "solid 3px " + "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".custom-grid table th a") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".PagerGridView td span") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".custom-grid table.grid-tabler th") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".igg_Office2007SilverHeader") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".igg_Office2007SilverHeaderCaption") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".carttable th") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".custom-grid table th") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".btn-default") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".inactivesbutton") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".inactiveli") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".btn-primary") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".loginsbuttonprimary") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".igdt_NodeHolder a:hover") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }

                            else if ((strSelector == ".cartbuttondefault") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !important";
                            }
                            else if ((strSelector == "ul.list li a.active") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".activesettings") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }

                            else if ((strSelector == ".custom-tabler th") && (className == "background-color"))
                            {

                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".listPaging") && (className == "background"))
                            {

                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".PagerGridView td span") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".custom-tabler th") && (className == "color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".listPaging") && (className == "color"))
                            {

                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeGridHeaderTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".Custom .ajax__tab_active .ajax__tab_outer" && (className == "background")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveTabColorandRelatedUnderlineBar" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }

                            else if ((strSelector == ".Custom .ajax__tab_active .ajax__tab_outer a" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActiveTabTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }


                            else if ((strSelector == ".lemongreen a" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == "#header-section .top-div-nav" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeCustomerUserInformationTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".btnpopup-default" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".loginsbuttonsecondary" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".btnpopup-primary" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".btnpopup-default" && (className == "background-color")))
                            {



                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }

                            else if ((strSelector == ".btn btn-default:hover") && (className == "background-color"))
                            {



                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }

                            else if ((strSelector == ".inactivesbutton:hover") && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";
                            }
                            else if ((strSelector == ".btn-default:hover") && (className == "background-color"))
                            {



                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".btn-default:focus") && (className == "background-color"))
                            {



                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".loginsbuttonsecondary:hover") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".loginsbuttonsecondary:focus") && (className == "background"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".loginsbuttonsecondary") && (className == "background-color"))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".inactiveli" && (className == "background")))
                            {

                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".inactiveli:hover" && (className == "background")))
                            {



                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".btnpopup-primary" && (className == "background-color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".activesbutton" && (className == "background")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".activesbutton:hover" && (className == "background-color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".activesbutton:focus" && (className == "background-color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".logviewadvbutton" && (className == "background")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".logviewadvbutton" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".activesbutton" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeActionButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".cartbuttondefault" && (className == "background-color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }
                            else if ((strSelector == ".cartbuttondefault" && (className == "color")))
                            {
                                DataRow[] drs;
                                drs = ds1.Select("propertyName='" + "pvthemeInactiveButtonTextColor" + "'");
                                value = "#" + drs[0][1].ToString().TrimWhiteSpace() + " !Important";

                            }

                            else
                            {
                                value = value.TrimWhiteSpace();
                            }
                            //One more check to be sure we are only pulling valid css values
                            if (!String.IsNullOrEmpty(className) && !String.IsNullOrEmpty(value))
                            {
                                style.Add(new KeyValuePair<String, String>(className, value));
                            }
                        }
                    }
                    this.Add(new KeyValuePair<String, List<KeyValuePair<String, String>>>(strSelector, style));
                }
            }
        }
    }

    /// <summary>
    /// Gets the CSS classes.
    /// </summary>
    public Dictionary<String, Dictionary<String, String>> Classes
    {
        get
        {
            if (_classes == null || _classes.Count == 0)
            {
                this._classes = this.Where(cl => cl.Key.StartsWith(".")).ToDictionary(cl => cl.Key.Trim(new Char[] { '.' }), cl => cl.Value.ToDictionary(p => p.Key, p => p.Value));
            }

            return _classes;
        }
    }

    /// <summary>
    /// Gets the elements.
    /// </summary>
    public Dictionary<String, Dictionary<String, String>> Elements
    {
        get
        {
            if (_elements == null || _elements.Count == 0)
            {
                _elements = this.Where(el => !el.Key.StartsWith(".")).ToDictionary(el => el.Key, el => el.Value.ToDictionary(p => p.Key, p => p.Value));
            }
            return _elements;
        }
    }

    /// <summary>
    /// Gets all styles in an Immutable collection
    /// </summary>
    public IEnumerable<KeyValuePair<String, List<KeyValuePair<String, String>>>> Styles
    {
        get
        {
            return this.ToArray();
        }
    }

    /// <summary>
    /// Removes all elements from the <see cref="CSSParser"></see>.
    /// </summary>
    new public void Clear()
    {
        // base.Clear();
        this._classes = null;
        this._elements = null;
    }

    /// <summary>
    /// Returns a <see cref="System.String"/> the CSS that was entered as it is stored internally.
    /// </summary>
    /// <returns>
    /// A <see cref="System.String"/> that represents this instance.
    /// </returns>
    public override string ToString()
    {
        StringBuilder strb = new StringBuilder(this.StyleSheet.Length);
        foreach (var item in this)
        {
            strb.AppendLine(item.Key)
                .AppendLine("{");
            foreach (var property in item.Value)
            {
                strb.Append("    ")
                    .Append(property.Key)
                    .Append(":")
                    .Append(property.Value)
                    .AppendLine(";");
            }
            strb.AppendLine("}")
                .AppendLine();
        }

        return strb.ToString();
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <returns>
    /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
    /// </returns>
    public override int GetHashCode()
    {
        return StyleSheet == null ? 0 : StyleSheet.GetHashCode();
    }

    /// <summary>
    /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
    /// </summary>
    /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
    /// <returns>
    ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
    /// </returns>
    /// <exception cref="T:System.NullReferenceException">
    /// The <paramref name="obj"/> parameter is null.
    ///   </exception>
    public override bool Equals(object obj)
    {
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        if (obj == null)
        {
            return false;
        }

        CSSParser o = obj as CSSParser;
        return this.StyleSheet.Equals(o.StyleSheet);
    }
}
