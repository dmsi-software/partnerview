// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Permissive License.
// See http://www.microsoft.com/resources/sharedsource/licensingbasics/sharedsourcelicenses.mspx.
// All other rights reserved.


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Data;
using AjaxControlToolkit;

/// <summary>
/// Helper web service for CascadingDropDown sample
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ProductGroupService : WebService
{
    // Member variables
    private static DataSet _referencedDataSet;
    private static object _lock = new object();
    protected string _contextKey;

    // we make these public statics just so we can call them from externally for the
    // page method call
    public DataSet ReferencedDataSet
    {
        get
        {
            lock (_lock)
            {
                //MDM Removed singleton pattern so DataSet is repopulated from same-named .xml that has new contents.
                _referencedDataSet = new DataSet();
                // Read DataSet from Session-named file...
                _referencedDataSet.ReadXml(HttpContext.Current.Server.MapPath("~/App_Data/" + this._contextKey + ".xml"));
            }
            return _referencedDataSet;
        }
    }

    /// <summary>
    /// Constructor to initialize members
    /// </summary>
    public ProductGroupService()
    {
    }

    [WebMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetMajors(string knownCategoryValues, string category, string contextKey)
    {
        _contextKey = contextKey;

        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
        foreach (DataRow row in ReferencedDataSet.Tables["ttProductMajor"].Rows)
        {
            string major_description = (string)row["major_description"];
            string major_code = (string)row["major"];
            values.Add(new CascadingDropDownNameValue(major_description, major_code));
        }
        return values.ToArray();
    }

    [WebMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetMinorsForMajor(string knownCategoryValues, string category, string contextKey)
    {
        _contextKey = contextKey;

        // Get a dictionary of known category/value pairs
        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
        string major_code;
        if (kv.ContainsKey("Major"))
        {
            major_code = kv["Major"].ToString();
        }
        else
        {
            return null;
        }

        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        foreach (DataRow row in ReferencedDataSet.Tables["ttProductMinor"].Rows)
        {
            if ((string)row["major"] == major_code)
            {
                values.Add(new CascadingDropDownNameValue((string)row["DESCRIPTION"], row["minor"].ToString()));
            }
        }

        return values.ToArray();
    }
}