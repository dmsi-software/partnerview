﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for DateTimeValidator
/// </summary>
public class DateTimeValidator
{
    #region "Properties"
    private string _inputString;
    private string _validationMessage = "";
    private DateTime parsedDateTime;
    #endregion

    #region "Constructors"
    public DateTimeValidator() { }
    public DateTimeValidator(TextBox txt)
    {
        _inputString = txt.Text.TrimWhiteSpace();
    }
    public DateTimeValidator(string inputString)
    {
        _inputString = inputString.TrimWhiteSpace();
    }
    #endregion

    #region "Methods"
    #region "Public"
    public bool InfoEntered()
    {
        return _inputString.Length > 0;
    }

    public bool ValidDate()
    {
        if (!DateTime.TryParse(_inputString, out parsedDateTime))
        {
            _validationMessage = "Invalid input date.";
            return false;
        }

        return true;
    }

    public DateTime ParsedDateTime()
    {
        return parsedDateTime;
    }

    public string FormattedDateTime()
    {
        return parsedDateTime.ToDMSiDateFormat();
    }

    public string ValidationMessage()
    {
        return _validationMessage;
    }
    #endregion
    #region "Private"
    #endregion
    #endregion
}