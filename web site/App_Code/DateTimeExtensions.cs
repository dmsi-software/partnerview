﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Extension methods for DateTimes
/// </summary>
public static partial class DateTimeExtensions
{
    /// <summary>
    /// Formats dates in DMSi format of MM/dd/yyyy
    /// </summary>
    /// <param name="dt">The DateTime to format</param>
    /// <returns>Formatted string</returns>
    public static String ToDMSiDateFormat(this DateTime dt)
    {
        if (dt == null)
        {
            return null;
        }
        return dt.ToString("MM/dd/yyyy");
    }
}