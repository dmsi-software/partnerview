﻿using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System;
using System.Data;
using System.Web;

/// <summary>
/// Summary description for ProductMajorClassGenerator
/// </summary>
public class ProductMajorClassGenerator
{
    private Dmsi.Agility.EntryNET.DataManager dm = null;
    private Dmsi.Agility.Data.ProductGroups productGroups = null;
    private int ReturnCode;
    private string MessageText;
    public dsProductGroupDataSet dsPG = null;

    public ProductMajorClassGenerator(Dmsi.Agility.EntryNET.DataManager _dm, Dmsi.Agility.Data.ProductGroups _productGroups)
    {
        dm = _dm;
        productGroups = _productGroups;
    }

    public bool GetProductGroups()
    {
        string searchCriteria = "Branch=" + (string)dm.GetCache("currentBranchID");

        productGroups = new ProductGroups(searchCriteria, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            return false;
        }
        return true;
    }


    public void GenerateXMLFileForSession()
    {
        dsPG = (dsProductGroupDataSet)productGroups.ReferencedDataSet;

        foreach (System.Data.DataRow row in dsPG.ttProductMajor.Rows)
        {
            if (!DBNull.Value.Equals(row["major_description"]) && (string)ConvertFromDBVal<string>(row["major_description"]) != "")
            {
                row["major_description"] = (string)ConvertFromDBVal<string>(row["major"]) + ", " + (string)ConvertFromDBVal<string>(row["major_description"]);
            }
            else
            {
                row["major_description"] = (string)ConvertFromDBVal<string>(row["major"]);
            }
        }

        foreach (DataRow row in dsPG.ttProductMinor.Rows)
        {
            if (!DBNull.Value.Equals(row["DESCRIPTION"]) && (string)ConvertFromDBVal<string>(row["DESCRIPTION"]) != "")
            {
                row["DESCRIPTION"] = (string)ConvertFromDBVal<string>(row["minor"]) + ", " + (string)ConvertFromDBVal<string>(row["DESCRIPTION"]);
            }
            else
            {
                row["DESCRIPTION"] = (string)ConvertFromDBVal<string>(row["minor"]);
            }
        }
    }
    public void SetSessionVariable() {
        Guid newGuid = Guid.NewGuid();
        HttpContext.Current.Session["SavedProductGroupMajorGUID"] = newGuid.ToString();
    }
    public static T ConvertFromDBVal<T>(object obj)
    {
        if (obj == null || obj == DBNull.Value)
        {
            return default(T); // returns the default value for the type
        }
        else
        {
            return (T)obj;
        }
    }
}