﻿using Dmsi.Agility.EntryNET.StrongTypesNS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TaxRateHelper
/// </summary>
public class TaxRateHelper
{
    private int _quoteID;
    private ProfileCommon _profile;
    public TaxRateHelper()
    {

    }

    public TaxRateHelper(int quoteID,
                         ProfileCommon profile)
    {
        _quoteID = quoteID;
        _profile = profile;
    }

    public void SaveDefaultTaxRateIfEmpty()
    {
        //if the two datasets come back null then this function will fail so keep it from doing so
        //if the default tax rate is zero, don't even bother performing this work because it won't change how the retail quote prints
        if (HttpContext.Current.Session["dsQuoteDataSet"] == null ||
            HttpContext.Current.Session["dsRetailAddressForQuotes"] == null ||
            Convert.ToDecimal(_profile.DefaultTaxRate) == 0)
        {
            return;
        }

        Dmsi.Agility.Data.Quotes quote = new Dmsi.Agility.Data.Quotes();
        dsRetailAddressDataSet dsRetailAddress = new dsRetailAddressDataSet();
        dsRetailAddressDataSet dsRetailAddressSet = (dsRetailAddressDataSet)HttpContext.Current.Session["dsRetailAddressForQuotes"];
        dsRetailAddressDataSet.tt_retail_addressRow[] rows = (dsRetailAddressDataSet.tt_retail_addressRow[])dsRetailAddressSet.tt_retail_address.Select("tran_id=" + _quoteID);

        if (rows.Length != 0 && rows[0]["misc_5"].ToString() != "") { return; }

        dsQuoteDataSet ds = quote.GetQuoteDataset(_quoteID);

        //if no retail information has been saved yet, then we need to store the default tax rate in order for it to show on retail quote
        if (rows.Length == 0)
        {
            dsRetailAddressDataSet.tt_retail_addressRow row = dsRetailAddress.tt_retail_address.Newtt_retail_addressRow();
            row["misc_5"] = _profile.DefaultTaxRate;
            row["dispatch_shipment_num"] = 0;
            row["dispatch_tran_id"] = 0;
            row["dispatch_tran_sysid"] = "";
            row["dispatch_tran_type"] = "";
            row["shipment_num"] = 0;
            row["tran_id"] = _quoteID;
            row["tran_sysid"] = "";
            row["type"] = "";

            dsRetailAddress.tt_retail_address.Rows.Add(row);
        }
        //if there is a quote that has retail customer info but no tax rate saved yet (IE quote saved prior to tax rate mod),
        //merge the default tax rate into the existing info
        else if (rows[0]["misc_5"].ToString() == "")
        {
            foreach (dsRetailAddressDataSet.tt_retail_addressRow row in rows)
            {
                row["misc_5"] = _profile.DefaultTaxRate;
                dsRetailAddress.tt_retail_address.Rows.Add(row.ItemArray);
            }
        }
        dsRetailAddress.AcceptChanges();

        quote.UpdateQuote(ds, dsRetailAddress);
    }
}