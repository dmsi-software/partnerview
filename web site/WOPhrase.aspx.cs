﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WOPhrase : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        tbxWOPhrase.Text = Request.QueryString["wophrase"] as string;
    }
}