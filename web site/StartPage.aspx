﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StartPage.aspx.cs" Inherits="StartPage"
    MasterPageFile="~/MasterPages/SecureMaster.master" %>
<%@ Register Assembly="Infragistics.WebUI.WebResizingExtender" Namespace="Infragistics.WebUI" TagPrefix="igui" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster1" runat="Server" >
    <style type="text/css">
        .txtMessages
        {
            margin: 15px;
        }
    </style>
    <asp:TextBox ID="txtMessages" runat="server" Height="310px" Width="558px" TextMode="MultiLine"
        CssClass="txtMessages"></asp:TextBox>
    <igui:WebResizingExtender ID="TextBox1_WebResizingExtender" runat="server" TargetControlID="txtMessages">
    </igui:WebResizingExtender>
    <asp:SqlDataSource ID="SqlDataSourcePVDatabase" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        SelectCommand="SELECT username FROM tbl_user WHERE (username = @username)">
        <SelectParameters>
            <asp:Parameter Name="userName" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
