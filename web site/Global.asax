<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        Application["Version"] = System.Configuration.ConfigurationManager.AppSettings["version"].ToString();
        Application["InternalVersion"] = System.Configuration.ConfigurationManager.AppSettings["internalversion"].ToString();
        Application["AgilityVersion"] = System.Configuration.ConfigurationManager.AppSettings["agilityversion"].ToString();
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Server.MapPath("~/App_Data/"));
        System.IO.FileInfo[] files = di.GetFiles();
        foreach (System.IO.FileInfo fi in files)
        {
            if (fi.Name.EndsWith(".xml") && (fi.Name.Length == 36 || fi.Name.Length == 40))
            {
                fi.Delete();
            }
        }
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown
        if (System.Web.HttpContext.Current != null)
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Server.MapPath("~/App_Data/"));
            System.IO.FileInfo[] files = di.GetFiles();
            foreach (System.IO.FileInfo fi in files)
            {
                if (fi.Name.EndsWith(".xml") && (fi.Name.Length == 36 || fi.Name.Length == 40))
                {
                    fi.Delete();
                }
            }
        }
    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        
        // FormsAuthentication ???; // set FormsAuthentication timeout parameter
    }

    void Session_End(object sender, EventArgs e)
    {
        //Dmsi.Agility.EntryNET.DataManager myDataManager = new Dmsi.Agility.EntryNET.DataManager();
        //if (myDataManager.GetCache("AgilityLibraryConnection") != null)
        //{
        //    Dmsi.Agility.EntryNET.Connection myconnection = (Dmsi.Agility.EntryNET.Connection)myDataManager.GetCache("AgilityLibraryConnection");
        //    if ((myconnection != null) && (myconnection.IsConnected))
        //    {
        //        myconnection.Disconnect();
        //    }
        //}
    }
       
</script>
