﻿$(document).ready(function () {
    $.fn.extend({
        placeCursorAtEnd: function () {
            // Places the cursor at the end of a contenteditable container (should also work for textarea / input)
            if (this.length === 0) {
                throw new Error("Cannot manipulate an element if there is no element!");
            }

            var el = this[0];
            var range = document.createRange();
            var sel = window.getSelection();
            var childLength = el.childNodes.length;

            if (childLength > 0) {
                var lastNode = el.childNodes[childLength - 1];
                var lastNodeChildren = lastNode.childNodes.length;

                range.setStart(lastNode, lastNodeChildren);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }

            return this;
        }
    });

    $('.rte').summernote({
        height: 300,
        width: 743,
        disableDragAndDrop: true,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontfamily', ['fontname', 'fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['picture', 'link', 'video']],
            ['misc', ['undo', 'redo']],
            ['codeview', ['codeview']]
        ],
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                //['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        lang: 'en-US', // Change to your chosen language
        imageAttributes: {
            icon: '<i class="note-icon-pencil"/>',
            removeEmpty: true, // true = remove attributes | false = leave empty if present
            disableNavigation:true, // true = don't display Navigation links | Display Navigation Links
            hideImageTab: false,// true = don't display Image Options | Display Image Options
            hideAttributesTab: true,// true = don't display Attribute Options | Display Attribute Options
            hideLinkTab: false,// true = don't display Link Options | Display Link Options
            hideUploadTab: true,// true = don't display Upload Options | Display Upload Options
            hideImageSource: true,
            hideDimensions: true,
            hideAltText: true,
            hideLinkClass: true,
            hideLinkStyle: true,
            hideLinkRel: true,
            hideLinkRole: true
        }
    });

    $('.note-codable').on('blur', function () {
        var codeviewHtml = $(this).val();
        var $summernoteTextarea = $(this).closest('.note-editor').siblings('textarea');

        $summernoteTextarea.val(codeviewHtml);
    });

    //Set the cursor at the end of the text area to auto-select the proper font family and text size
    $('.note-editable').placeCursorAtEnd();
    $('.rte').summernote("code", $('.rte').summernote('code'));
    $('.note-editable').placeCursorAtEnd();
});