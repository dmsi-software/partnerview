<%@ Page Language="C#" MasterPageFile="~/MasterPages/OpenMaster35SP1.master" AutoEventWireup="true"
    CodeFile="TimedOut.aspx.cs" Inherits="TimedOut" Title="Session Expired" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster1" runat="Server">
    <center>
        <br />
        <p>
        <b>Your session has expired.</b>
        </p>
        <asp:LinkButton ID="lbtnGoToLogin" runat="server" 
            onclick="lbtnGoToLogin_Click" >Click here to log on again.</asp:LinkButton>
    </center>
</asp:Content>
