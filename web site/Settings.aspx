﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/PlainMaster.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="Settings.aspx.cs" Inherits="Settings" Title="Settings" %>

<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.NavigationControls" TagPrefix="ig" %>
<%@ Register Src="UserControls/LogView.ascx" TagName="LogView" TagPrefix="uc1" %>
<%@ Register Src="UserControls/PasswordMaintSettings.ascx" TagName="PasswordMaintSettings"
    TagPrefix="uc2" %>
<%@ Register Src="UserControls/MessagesMaint.ascx" TagName="MessagesMaint" TagPrefix="uc3" %>
<%@ Register Src="UserControls/ChangeTheme.ascx" TagName="ChangeTheme" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster1" runat="Server">
    <style type="text/css">
        .menucell {
            margin-left: 20px;
            margin-right: 20px;
        }

        .menuleftmargin {
            margin-left: 6px;
        }

        .pnlPopUp {
            right: 152px !important;
            margin-right: 0px !important;
        }
    </style>
    <div id="body-container">
        <div class="mid-container">
            <div class="left-section">
                <div class="lft-cont-sec">
                    <div class="search-opt settingsblock">
                        <ig:WebDataMenu ID="WebDataMenu1" runat="server" OnItemClick="WebDataMenu1_ItemClick"
                            CssClass="list">
                            <GroupSettings Orientation="Vertical" />
                            <Items>
                                <ig:DataMenuItem Text="Change Password">
                                </ig:DataMenuItem>
                                <ig:DataMenuItem Text="Log Viewer" Visible="False">
                                </ig:DataMenuItem>
                                <ig:DataMenuItem Text="Messages" Visible="False">
                                </ig:DataMenuItem>
                                <ig:DataMenuItem Text="Change Theme">
                                </ig:DataMenuItem>
                            </Items>
                        </ig:WebDataMenu>
                    </div>
                </div>
            </div>
            <div class="right-section">
                <div class="container-sec">
                    <h2>
                        <asp:Label ID="lblSttingName" runat="server" /></h2>
                    <uc2:PasswordMaintSettings ID="PasswordMaintSettings" runat="server" Visible="False" />
                    <uc1:LogView ID="LogView1" runat="server" Visible="False" />
                    <uc3:MessagesMaint ID="MessagesMaint1" runat="server" Visible="False" />
                    <uc4:ChangeTheme ID="ChangeTheme1" runat="server" Visible="false" />
                    <asp:HiddenField ID="hdnlogview" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
