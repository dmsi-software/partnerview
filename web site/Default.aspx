<%@ Page Language="C#" Async="True" AsyncTimeout="2" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/MasterPages/OpenMaster35SP1.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" Title="Partner View" %>

<%@ Register Src="UserControls/MainTabControl.ascx" TagName="MainTabControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Inventory.ascx" TagName="Inventory1" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster1" runat="Server">
    <link id="jquiCSS" rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/ui-lightness/jquery-ui.css" type="text/css" media="all" />
    <uc1:MainTabControl ID="MainTabControl1" runat="server" />
    <asp:LinkButton ID="cmdPrice" runat="server" CausesValidation="False" EnableViewState="False"
        OnCommand="cmdPrice_Command"></asp:LinkButton>
    <asp:HiddenField runat="server" ID="hdnHelpClick" Visible="true" />
    <asp:HiddenField runat="server" ID="hdnExternalLinkClick" Visible="true" />
    <asp:Button runat="server" ID="btnFetchTicket" OnClick="btnFetchTicket_Click" Visible="false" />
    <script language="javascript" type="text/javascript">
        var _settop = 0;

        function ddlClick() {
            _settop = $(window).scrollTop();
        }

        function hypHelp_click(obj) {
            $get('ctl00_cphMaster1_hdnHelpClick').value = 'hypHelpClicked';
        }

        function hypNavigateExternalSite_click(obj) {
            $get('ctl00_cphMaster1_hdnExternalLinkClick').value = 'hypNavigateExternalClicked';
            __doPostBack('<%= btnFetchTicket.UniqueID %>', '');

            var url = "";

            if ($get('ctl00_cphMaster1_MainTabControl1_Inventory1_hypNavigateExternalSite') != null)
                url = $get('ctl00_cphMaster1_MainTabControl1_Inventory1_hypNavigateExternalSite').href;

            if ($get('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_hypNavigateExternalSite') != null)
                url = $get('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_hypNavigateExternalSite').href;

            window.open(url, '_blank'); return false;
        }

        $(window).load(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(UpdatePanelLoaded)
        });

        function wneFixedAmount_TextChanged(sender, eventArgs) {
            Validate_wneFixedAmount();
        }

        function wneFixedAmount_Blur(sender, eventArgs) {
            Validate_wneFixedAmount();
        }

        function Validate_wneFixedAmount() {
            if (Number($get("ctl00_cphMaster1_MainTabControl1_Payments1_wneFixedAmount").value) >= 0.01) {
                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCC") && $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCC").style.display == '') {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCC").disabled = false;
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayACH") && $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayACH").style.display == '') {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayACH").disabled = false;
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenFieldSurchargeUsage") && $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenFieldSurchargeUsage").value == 'True' && $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblTotalPayment")) {
                    var surchargetype = $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenFieldSurchargeType").value;
                    var surchargeamt = $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenFieldSurchargeValue").value;
                    var surchargeTotal;

                    var currentAmount = $get("ctl00_cphMaster1_MainTabControl1_Payments1_wneFixedAmount").value;

                    if (surchargetype == 'Fixed') {
                        surchargeTotal = surchargeamt;
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeAmt").innerHTML = '$' + Number(parseFloat(surchargeTotal)).formatMoney(2);
                    }

                    if (surchargetype == 'Percent') {
                        surchargeTotal = currentAmount * (surchargeamt / 100);
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeAmt").innerHTML = '$' + Number(parseFloat(surchargeTotal)).formatMoney(2);
                    }

                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblTotalPayment").innerHTML = '$' + Number((parseFloat(currentAmount) + parseFloat(surchargeTotal))).formatMoney(2);

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal") != null)
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenFieldCCSurchargeTotal").value = surchargeTotal;
                }
            }
            else {
                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCC")) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCC").disabled = true;
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayACH")) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayACH").disabled = true;
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeAmt"))
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeAmt").innerHTML = '$0.00';

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblTotalPayment"))
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblTotalPayment").innerHTML = '$0.00';
            }
        }

        function UpdatePanelLoaded(sender, args) {
            setTimeout("scrollWindowToPosition()", 500);
        }

        function scrollWindowToPosition() {
            if ($("input[id$='hdnScrollToTop']").val() == "true") {
                $("input[id$='hdnScrollToTop']").val("");
                _settop = 0;
                $(window).scrollTop(_settop);
            }

            if (_settop != 0) {
                $(window).scrollTop(_settop);
                _settop = 0;
            }
        }

        var mydragg = function () {
            return {
                move: function (divid, xpos, ypos) {
                    divid.style.left = xpos + 'px';
                    divid.style.top = ypos + 'px';
                },
                startMoving: function (divid, container, evt) {
                    evt = evt || window.event;
                    var posX = evt.clientX,
                        posY = evt.clientY,
                    divTop = divid.style.top,
                    divLeft = divid.style.left,
                    eWi = parseInt(divid.style.width),
                    eHe = parseInt(divid.style.height),
                    cWi = parseInt(document.getElementById(container).style.width),
                    cHe = parseInt(document.getElementById(container).style.height);
                    document.getElementById(container).style.cursor = 'move';
                    divTop = divTop.replace('px', '');
                    divLeft = divLeft.replace('px', '');
                    var diffX = posX - divLeft,
                        diffY = posY - divTop;
                    document.onmousemove = function (evt) {
                        evt = evt || window.event;
                        var posX = evt.clientX,
                            posY = evt.clientY,
                            aX = posX - diffX,
                            aY = posY - diffY;
                        if (aX < 0) aX = 0;
                        if (aY < 0) aY = 0;
                        if (aX + eWi > cWi) aX = cWi - eWi;
                        if (aY + eHe > cHe) aY = cHe - eHe;
                        mydragg.move(divid, aX, aY);
                    }



                },
                stopMoving: function (container) {
                    var a = document.createElement('script');
                    document.getElementById(container).style.cursor = 'default';
                    document.onmousemove = function () { }
                },
            }
        }();

        function touchHandler(evt) {

            var touch = evt.changedTouches[0];

            var simulatedEvent = document.createEvent("MouseEvent");
            simulatedEvent.initMouseEvent({
                touchstart: "mousedown",
                touchmove: "mousemove",
                touchend: "mouseup"
            }[evt.type], true, true, window, 1,
            touch.screenX, touch.screenY,
            touch.clientX, touch.clientY, false,
            false, false, false, 0, null);

            touch.target.dispatchEvent(simulatedEvent);
            evt.preventDefault();
        }

        function init() {
            document.addEventListener("touchstart", touchHandler, true);
            document.addEventListener("touchmove", touchHandler, true);
            document.addEventListener("touchend", touchHandler, true);
            document.addEventListener("touchcancel", touchHandler, true);
        }
    </script>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequest);
        function beginRequest()
        {
            //prm._scrollPosition = null;
            ddlClick();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function QuoteReleaseHideCancelAndSubmitButtons() {
            document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteRelease_btnSubmitOrder2').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteRelease_btnSubmitOrder').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteRelease_btnEditCart2').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteRelease_btnEditCart').style.display = 'none';
        }

        function CartSubmiteHideCancelAndSubmitButtons() {
            document.getElementById('ctl00_cphMaster1_MainTabControl1_OrderTab1_btnSubmitOrder2').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_OrderTab1_btnSubmitOrder').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_OrderTab1_btnEditCart2').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_OrderTab1_btnEditCart').style.display = 'none';
        }

        function ElementHideCloseButton() {
            document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnExitElement').style.display = 'none';
        }

        function HideCopyQuoteYesButtons() {
            document.getElementById('ctl00_cphMaster1_MainTabControl1_Quotes1_btnCopyQuote').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_Quotes1_btnNo').style.display = 'none';

            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Quotes1_btnCopyWithRetail') != null) {
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Quotes1_btnCopyWithRetail').style.display = 'none';
            }
        }

        function ACHHideCloseButtons() {
            document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnACHPay').style.display = 'none';
            document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnACHCancel').style.display = 'none';
        }

        function iOS() {
            var iDevices = [
              'iPad Simulator',
              'iPhone Simulator',
              'iPod Simulator',
              'iPad',
              'iPhone',
              'iPod'
            ];

            if (!!navigator.platform) {
                while (iDevices.length) {
                    if (navigator.platform === iDevices.pop()) { return true; }
                }
            }

            return false;
        }

        function Android() {
            if (navigator.userAgent.indexOf('Android') != -1)
                return true;
            else
                return false;
        }

        function Firefox() {
            if (navigator.userAgent.toLowerCase().indexOf('firefox') != -1)
                return true;
            else
                return false;
        }

        function fibo(max) {
            var x = 0;
            for (var i = 0, j = 1, k = 0; k < max; i = j, j = x, k++) {
                x = i + j;
            }
        }

        function FixedPayHideCloseButtons() {
            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCC') != null)
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCC').style.display = 'none';

            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayACH') != null)
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayACH').style.display = 'none';

            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCancel') != null)
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnFixedPayCancel').style.display = 'none';
        }

        function TotalAndDisplayValues() {
            //total values and display
            var grandTotal = 0.00;
            if (checkedValues.length > 0) {
                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment") != null) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment").disabled = true;
                }

                grandTotal = checkedValues.reduce(function (a, b) { return Number(a) + Number(b); });
                grandTotal = round(grandTotal, 2);

                if (($get("ctl00_cphMaster1_MainTabControl1_Payments1_rdoCC") != null && $get("ctl00_cphMaster1_MainTabControl1_Payments1_rdoCC").checked == true) || $get("ctl00_cphMaster1_MainTabControl1_Payments1_rdoCC") == null) {
                    if (apply_surchargeValue == true && grandTotal > 0) {
                        var surcharge = 0.00;

                        if (surcharge_basis_typeValue == 'Fixed') {
                            surcharge = surcharge_basis_amtValue;
                        }

                        if (surcharge_basis_typeValue == 'Percent') {
                            surcharge = grandTotal * (surcharge_basis_amtValue / 100);
                        }

                        grandTotal = grandTotal + surcharge;

                        if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal") != null)
                            $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal").innerHTML = '$' + Number(surcharge).formatMoney(2);
                    }
                }

                if (apply_surchargeValue == true && grandTotal <= 0) {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal") != null)
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal").innerHTML = '$0.00';
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblGrandTotal") != null) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblGrandTotal").innerHTML = '$' + Number(grandTotal).formatMoney(2);
                }

                if (grandTotal <= 0) {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = true;
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = true;
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                    }
                }
                else {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = false;
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").addEventListener("click", HidePayButtons);
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = false;
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").addEventListener("click", HidePayButtons);
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                    }
                }

                if (grandTotal == 0) {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = false;
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").addEventListener("click", HidePayButtons);
                    }
                }
            }
            else {
                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblGrandTotal") != null)
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblGrandTotal").innerHTML = '$0.00';

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal") != null)
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal").innerHTML = '$0.00';

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = true;
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = true;
                }

                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment") != null) {
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment").disabled = false;
                }
            }
        }

        function UpdateHiddenFields() {
            //update the value of the hidden field
            $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField").value = checkedIndeces.toString();
            $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField2").value = checkedValues.toString();

            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal") != null)
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenFieldCCSurchargeTotal").value = $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal").innerHTML;
        }

        function HidePayButtons() {
            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnPay') != null)
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnPay').style.display = 'none';

            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank') != null)
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank').style.display = 'none';

            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits') != null)
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits').style.display = 'none';

            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment') != null)
                document.getElementById('ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment').style.display = 'none';
        }

        function ddlBankAccount_SelectionChanged(ddl) {
            if (ddl.selectedIndex != 0) {
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnACHPay").disabled = false;
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnACHPay").addEventListener("click", ACHHideCloseButtons);
            }
            else {
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnACHPay").disabled = true;
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        function Expander_Click(type) {
            if (type == 'price') {
                __doPostBack('<%= cmdPrice.ClientID %>', '');
            }
        }

        function SetBannerId(obj) {
            if ($get("ctl00_cphMaster1_MainTabControl1_Inventory1_hdnBannerId") != null)
                $get("ctl00_cphMaster1_MainTabControl1_Inventory1_hdnBannerId").value = obj;

            if ($get("ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_hdnBannerId") != null)
                $get("ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_hdnBannerId").value = obj;
        }

        function GetControlIds(obj) {
            PageMethods.GetControlid(obj);
        };

        function ImageButtonClick() {
        };

        function OpenNewURL(obj) {
            var url = obj;
            window.open(url, "_blank");
            return false;
        }

        function fnPagingNotification() {
            var gvDrv1 = document.getElementById("ctl00_cphMaster1_MainTabControl1_Inventory1_TabContainer1_tbAdvSearch_hdnAddCart");
            if (gvDrv1 != null && gvDrv1.value == 1) {

                var bConfirm = confirm('Quantities entered have not been added to the cart.  Do you want to continue?');
                if (bConfirm == true) {
                    gvDrv1.value = 0;
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                gvDrv1 = document.getElementById("ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_TabContainer1_tbAdvSearch_hdnAddCart_QEAI");
                if (gvDrv1 != null && gvDrv1.value == 1) {

                    var bConfirm = confirm('Quantities entered have not been added to the quote.  Do you want to continue?');
                    if (bConfirm == true) {
                        gvDrv1.value = 0;
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
            }

            return true;
        };

        function fnAddTocartTextchanged(obj) {
            var txtval = document.getElementById(obj);
            var gvDrv = document.getElementById("ctl00_cphMaster1_MainTabControl1_Inventory1_TabContainer1_tbAdvSearch_hdnAddCart");
            if (gvDrv != null) {
                var ddlval = document.getElementById("ctl00_cphMaster1_MainTabControl1_Inventory1_ddlHeader").value;
                if (txtval.value.length > 0) {

                    gvDrv.value = 1;
                }
                else {

                    var b = true;
                    for (var i = 0; i < ddlval; i++) {
                        var txtvalmepty = document.getElementById("ctl00_cphMaster1_MainTabControl1_Inventory1_lvProducts_ctrl" + i + "_txtOrder");
                        if (txtvalmepty != null) {
                            if (txtvalmepty.value.length > 0) {
                                b = false;
                                break;
                            }
                        }
                    }

                    if (b == true) {
                        gvDrv.value = 0;

                    }
                    else {
                        gvDrv.value = 1;
                    }
                }
            }

            txtval = document.getElementById(obj);
            gvDrv = document.getElementById("ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_TabContainer1_tbAdvSearch_hdnAddCart_QEAI");
            if (gvDrv != null) {
                ddlval = document.getElementById("ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_ddlHeader").value;
                if (txtval.value.length > 0) {

                    gvDrv.value = 1;
                }
                else {

                    var b = true;
                    for (var i = 0; i < ddlval; i++) {
                        var txtvalmepty = document.getElementById("ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_lvProducts_ctrl" + i + "_txtOrder");
                        if (txtvalmepty != null) {
                            if (txtvalmepty.value.length > 0) {
                                b = false;
                                break;
                            }
                        }
                    }

                    if (b == true) {
                        gvDrv.value = 0;

                    }
                    else {
                        gvDrv.value = 1;
                    }
                }
            }

            return true;
        };

        function GetPriceChangeControlIds(obj) {
            var ControlName = document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle');

            if (ControlName != null && ControlName.value == 'Margin' && ControlName.style.display == '') {
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value = (1 / (1 - (document.getElementById('ctl00_OptionsSelector_numericMarginFactor').value / 100))).toFixed(4);

                if (document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value > 99) {
                    document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value = 99;
                }
            }

            var sDropdownVal = document.getElementById('ctl00_OptionsSelector_cbxDisplay');
            var sDropdownVal_List = document.getElementById('ctl00_OptionsSelector_cbxDisplay_List');

            var scbxRetain = document.getElementById('ctl00_OptionsSelector_cbxRetain').checked;

            if (document.getElementById('ctl00_OptionsSelector_numericMarkupFactor') != null)
                var snumericMarkupFactor = document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value;

            if (document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List') != null)
                var snumericMarkupFactor_List = document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').value;

            if (sDropdownVal != null && sDropdownVal.style.width != '1px' && scbxRetain != null && snumericMarkupFactor != null) {
                var dropdownselval = sDropdownVal.options[sDropdownVal.selectedIndex].value;

                var tableProductsExists = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_lvProducts_ctrl0_Tr1');
                if (tableProductsExists == null) {
                    tableProductsExists = document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_lvProducts_ctrl0_Tr1');
                }

                var tablelvNoImagesProductsList = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_lvNoImagesProductsList_ctrl0_Tr1');
                if (tablelvNoImagesProductsList == null) {
                    tablelvNoImagesProductsList = document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_lvNoImagesProductsList_ctrl0_Tr1');
                }

                var MethodVal = obj.toString() + '+' + dropdownselval.toString() + '+' + scbxRetain.toString() + '+' + snumericMarkupFactor.toString();
                var sSplit = obj.toString().split("_");

                if (tableProductsExists != null || tablelvNoImagesProductsList != null) {
                    PageMethods.GetControlid(MethodVal);
                }
                else if (obj == "ctl00_OptionsSelector_btnCancel") {
                    var MethodVal = 'btnCancel' + '+' + dropdownselval.toString() + '+' + scbxRetain.toString() + '+' + snumericMarkupFactor.toString();
                    PageMethods.GetControlid(MethodVal);
                }
                else {
                    var MethodVal = 'RetainPrevious' + '+' + dropdownselval.toString() + '+' + scbxRetain.toString() + '+' + snumericMarkupFactor.toString();
                    PageMethods.GetControlid(MethodVal);
                }
            }

            if (sDropdownVal_List != null && sDropdownVal_List.style.width != '1px' && scbxRetain != null && snumericMarkupFactor_List != null) {
                var dropdownselval = sDropdownVal_List.options[sDropdownVal_List.selectedIndex].value;

                var actualMarkup;
                if (dropdownselval == 'Retail price - discount from list')
                    actualMarkup = snumericMarkupFactor_List.toString();

                if (dropdownselval == 'Retail price - markup on cost' || dropdownselval == 'Net price')
                    actualMarkup = snumericMarkupFactor.toString();

                var tableProductsExists = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_lvProducts_ctrl0_Tr1');
                if (tableProductsExists == null) {
                    tableProductsExists = document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_lvProducts_ctrl0_Tr1');
                }

                var tablelvNoImagesProductsList = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_lvNoImagesProductsList_ctrl0_Tr1');
                if (tablelvNoImagesProductsList == null) {
                    tablelvNoImagesProductsList = document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_lvNoImagesProductsList_ctrl0_Tr1');
                }

                var MethodVal = obj.toString() + '+' + dropdownselval.toString() + '+' + scbxRetain.toString() + '+' + actualMarkup.toString();
                var sSplit = obj.toString().split("_");

                if (tableProductsExists != null || tablelvNoImagesProductsList != null) {
                    PageMethods.GetControlid(MethodVal);
                }
                else if (obj == "ctl00_OptionsSelector_btnCancel") {
                    var MethodVal = 'btnCancel' + '+' + dropdownselval.toString() + '+' + scbxRetain.toString() + '+' + actualMarkup.toString();
                    PageMethods.GetControlid(MethodVal);
                }
                else {
                    var MethodVal = 'RetainPrevious' + '+' + dropdownselval.toString() + '+' + scbxRetain.toString() + '+' + actualMarkup.toString();
                    PageMethods.GetControlid(MethodVal);
                }
            }
        };

        function GetPriceChangeControlCancel(obj) {
            document.getElementById('ctl00_OptionsModalDiv').style.display = "none";

            return false;
        }


        //MDM - global javascript vars to hold the row indexes and money; notice hard-coded .get_cell(19) row index
        var checkedIndeces = new Array();
        var checkedValues = new Array();
        var apply_surchargeValue;
        var surcharge_basis_typeValue;
        var surcharge_basis_amtValue;

        Number.prototype.formatMoney = function (c, d, t) {
            var n = this,
                c = isNaN(c = Math.abs(c)) ? 2 : c,
                d = d == undefined ? "." : d,
                t = t == undefined ? "," : t,
                s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };

        //MDM - global for saving expanded/contracted state of Billing Search
        var varAdjustGrid = true;

        //function ctl00_WebHierarchicalDataGridDetail_ContainerGrid_DataBound(sender)
        //{
        //    setTimeout(function () { adjustGrid(varAdjustGrid)}, 2000);
        //}

        //function ctl00_WebHierarchicalDataGridDetail_ContainerGrid_MouseOver(sender, eventArgs) {
        //    adjustGrid(varAdjustGrid);
        //}

        function ctl00_WebDataGridAging_Grid_Click(sender, eventArgs) {
            checkedIndeces = new Array();
            checkedValues = new Array();
        }

        function enteringEditMode(grid, eventArgs) {
            var cell = eventArgs.getCell();
            var checkboxCell = cell.get_row().get_cellByColumnKey("PayThis");

            if (cell.get_column().get_key() == "payment_amt") {

                if (checkboxCell.get_value() == false) {
                    eventArgs.set_cancel(true);
                    return (false);
                }

                reftypeCell = cell.get_row().get_cellByColumnKey("ref_type");

                if (reftypeCell.get_value() == 'CM' || reftypeCell.get_value() == 'CA' || reftypeCell.get_value() == 'CI') {
                    eventArgs.set_cancel(true);
                    return (false);
                }

                if (reftypeCell.get_value() == 'IN' && parseFloat(cell.get_text()) <= 0) {
                    eventArgs.set_cancel(true);
                    return (false);
                }

                if (iOS() || Android() || Firefox()) {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = true;
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = true;
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                    }
                }
            }
        }

        function WebHierarchicalDataGridDetail_MouseUp(sender, eventArgs) {
            if (Firefox()) {
                fibo(1000);
            }
        }

        function WebHierarchicalDataGridDetail_MouseDown(sender, eventArgs) {
            if (Firefox()) {
                fibo(1000);
            }
        }

        function exitingEditMode(sender, eventArgs) {
            var cell = eventArgs.getCell();
            var isCancelled = false;

            if (cell.get_column().get_key() == "payment_amt") {
                var balDueValue = cell.get_row().get_cellByColumnKey("balance_due").get_value();

                var newPayAmt = parseFloat(eventArgs._text);

                if (isNaN(newPayAmt)) {
                    eventArgs.set_cancel(true);
                    isCancelled = true;

                    if (!Firefox())
                        alert("'Amount to Pay' must contain only numbers and optional period for cents.");

                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").childNodes[0].value = balDueValue;
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").childNodes[0].select();

                    if (iOS() || Android() || Firefox()) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").focus();
                    }
                }
                else if (eventArgs._text.indexOf('.') >= 0 && eventArgs._text.substr(eventArgs._text.indexOf('.') + 1).length >= 3) {
                    eventArgs.set_cancel(true);
                    isCancelled = true;

                    if (!Firefox())
                        alert("'Amount to Pay' can only include 2 decimals.");

                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").childNodes[0].value = balDueValue;
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").childNodes[0].select();

                    if (iOS() || Android() || Firefox()) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").focus();
                    }
                }
                else if (eventArgs._text == null || newPayAmt <= 0 || newPayAmt > balDueValue) {
                    eventArgs.set_cancel(true);
                    isCancelled = true;

                    if (!Firefox())
                        alert("'Amount to Pay' must be greater than zero and less than or equal to 'Balance Due'.");

                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").childNodes[0].value = balDueValue;
                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").childNodes[0].select();

                    if (iOS() || Android() || Firefox()) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_WebHierarchicalDataGridDetail_ctl00_ed0").focus();
                    }
                }
            }

            if (Firefox()) {
                fibo(1000);
            }

            if ((iOS() || Android() || Firefox()) && isCancelled) {
                eventArgs.set_cancel(false);
            }

            try {
                if (eventArgs._props[0] != null && eventArgs._props[0].keyCode != null && eventArgs._props[0].keyCode == 9) {
                    eventArgs._props[0].rawEvent.preventDefault();
                    eventArgs._props[0].rawEvent.stopImmediatePropagation();
                }
            } catch (err) { }
        }

        function exitedEditMode(sender, eventArgs) {
            var newValue = parseFloat(eventArgs._cell.get_value());

            for (i = 0; i < checkedIndeces.length; i++) {
                if (checkedIndeces[i] == eventArgs._cell._row.get_dataKey()[0].toString() + '|' + eventArgs._cell._row.get_dataKey()[1].toString() + '|' + eventArgs._cell._row.get_dataKey()[2].toString()) {
                    checkedIndeces.splice(i, 1);
                    checkedValues.splice(i, 1);
                }
            }

            checkedIndeces.push(eventArgs._cell._row.get_dataKey()[0].toString() + '|' + eventArgs._cell._row.get_dataKey()[1].toString() + '|' + eventArgs._cell._row.get_dataKey()[2].toString());
            checkedValues.push(Number(newValue));

            TotalAndDisplayValues();

            UpdateHiddenFields();

            if (iOS() || Android() || Firefox()) {
                var grandTotal = 0.00;
                grandTotal = checkedValues.reduce(function (a, b) { return Number(a) + Number(b); });
                grandTotal = round(grandTotal, 2);

                if (grandTotal <= 0) {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = true;
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = true;
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                    }
                }
                else {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = false;
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").addEventListener("click", HidePayButtons);
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = false;
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").addEventListener("click", HidePayButtons);
                    }

                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                    }
                }

                if (grandTotal == 0) {
                    if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = false;
                        $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").addEventListener("click", HidePayButtons);
                    }
                }
            }
        }

        function WebHierarchicalDataGridDetail_Grid_Initialize(sender, eventArgs) {
            //need to reset on every grid initialize, cause server code may have emptied the Hidden fields.
            checkedIndeces = new Array();
            checkedValues = new Array();

            //upon callback, reinitialize the array with the value of the hidden field
            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField").value != "") {
                checkedIndeces = $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField").value.split(",");
            }

            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField2").value != "") {
                checkedValues = $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField2").value.split(",");
            }

            //adjustGrid(varAdjustGrid);
        }

        function WebHierarchicalDataGridDetail_Grid_Click(sender, eventArgs) {
            try {
                if (eventArgs.get_item().get_column().get_key() == "PayThis" && eventArgs.get_browserEvent().target.nodeName == 'IMG') {
                    // MDM - The following three variables are globals. They are used in calculations outside this scope.
                    apply_surchargeValue = eventArgs.get_item().get_row().get_cellByColumnKey("apply_surcharge").get_value();
                    surcharge_basis_typeValue = eventArgs.get_item().get_row().get_cellByColumnKey("surcharge_basis_type").get_value();
                    surcharge_basis_amtValue = eventArgs.get_item().get_row().get_cellByColumnKey("surcharge_basis_amt").get_value();

                    // if an item has been checked, push the row values into the global arrays
                    if (eventArgs.get_item().get_value() == true) {
                        if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment") != null) {
                            $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnLumpSumPayment").disabled = true;
                        }

                        reftypeCell = eventArgs.get_item().get_row().get_cellByColumnKey("ref_type");

                        if (!(reftypeCell.get_value() == 'CM' || reftypeCell.get_value() == 'CA' || reftypeCell.get_value() == 'CI')) {
                            balancedueCell = eventArgs.get_item().get_row().get_cellByColumnKey("balance_due");

                            paymentamtCell = eventArgs.get_item().get_row().get_cellByColumnKey("payment_amt");
                            paymentamtCell.get_element().className = paymentamtCell.get_element().className + " yellowbackground";
                            paymentamtCell.set_value(Number(balancedueCell.get_text().replace('$', "").replace(/,/g, '').replace('(', '-').replace(')', '')));

                            if (parseFloat(paymentamtCell.get_text()) <= 0) {
                                paymentamtCell.get_element().className = paymentamtCell.get_element().className.replace(" yellowbackground", "");
                                paymentamtCell.set_value("0.00");
                            }
                        }

                        if (reftypeCell.get_value() == 'CM' || reftypeCell.get_value() == 'CA' || reftypeCell.get_value() == 'CI') {
                            balancedueCell = eventArgs.get_item().get_row().get_cellByColumnKey("balance_due");

                            paymentamtCell = eventArgs.get_item().get_row().get_cellByColumnKey("payment_amt");
                            paymentamtCell.set_value(Number(balancedueCell.get_text().replace('$', "").replace(/,/g, '').replace('(', '-').replace(')', '')));
                        }

                        checkedIndeces.push(eventArgs.get_item().get_row().get_dataKey()[0].toString() + '|' + eventArgs.get_item().get_row().get_dataKey()[1].toString() + '|' + eventArgs.get_item().get_row().get_dataKey()[2].toString());

                        if (eventArgs.get_item().get_row().get_cell(19).get_text().indexOf('(') != -1)
                            checkedValues.push(Number(0.00 - eventArgs.get_item().get_row().get_cell(19).get_text().replace('$', "").replace(/,/g, '').replace('(', '-').replace(')', '')));
                        else
                            checkedValues.push(Number(eventArgs.get_item().get_row().get_cell(19).get_text().replace('$', "").replace(/,/g, '').replace('(', '-').replace(')', '')));

                        //total values and display
                        var grandTotal = 0.00;
                        grandTotal = checkedValues.reduce(function (a, b) { return Number(a) + Number(b); });
                        grandTotal = round(grandTotal, 2);

                        if (($get("ctl00_cphMaster1_MainTabControl1_Payments1_rdoCC") != null && $get("ctl00_cphMaster1_MainTabControl1_Payments1_rdoCC").checked == true) || $get("ctl00_cphMaster1_MainTabControl1_Payments1_rdoCC") == null) {
                            if (apply_surchargeValue == true && grandTotal > 0) {
                                var surcharge = 0.00;

                                if (surcharge_basis_typeValue == 'Fixed') {
                                    surcharge = surcharge_basis_amtValue;
                                }

                                if (surcharge_basis_typeValue == 'Percent') {
                                    surcharge = grandTotal * (surcharge_basis_amtValue / 100);
                                }

                                grandTotal = grandTotal + surcharge;

                                if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal") != null) {
                                    $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblSurchargeTotal").innerHTML = '$' + Number(surcharge).formatMoney(2);
                                }
                            }
                        }

                        if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_lblGrandTotal") != null) {
                            $get("ctl00_cphMaster1_MainTabControl1_Payments1_lblGrandTotal").innerHTML = '$' + Number(grandTotal).formatMoney(2);
                        }

                        if (grandTotal <= 0) {
                            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = true;
                            }

                            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = true;
                            }

                            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                            }
                        }
                        else {
                            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay") != null) {
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").disabled = false;
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPay").addEventListener("click", HidePayButtons);
                            }

                            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank") != null) {
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").disabled = false;
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnPayBank").addEventListener("click", HidePayButtons);
                            }

                            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = true;
                            }
                        }

                        if (grandTotal == 0) {
                            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits") != null) {
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").disabled = false;
                                $get("ctl00_cphMaster1_MainTabControl1_Payments1_btnApplyCredits").addEventListener("click", HidePayButtons);
                            }
                        }
                    } else {
                        reftypeCell = eventArgs.get_item().get_row().get_cellByColumnKey("ref_type");

                        if (!(reftypeCell.get_value() == 'CM' || reftypeCell.get_value() == 'CA' || reftypeCell.get_value() == 'CI')) {

                            balancedueCell = eventArgs.get_item().get_row().get_cellByColumnKey("balance_due");

                            paymentamtCell = eventArgs.get_item().get_row().get_cellByColumnKey("payment_amt");
                            paymentamtCell.get_element().className = paymentamtCell.get_element().className.replace(" yellowbackground", "").replace("yellowbackground ", "");
                            paymentamtCell.set_value("$0.00");
                        }

                        if (reftypeCell.get_value() == 'CM' || reftypeCell.get_value() == 'CA' || reftypeCell.get_value() == 'CI') {

                            balancedueCell = eventArgs.get_item().get_row().get_cellByColumnKey("balance_due");

                            paymentamtCell = eventArgs.get_item().get_row().get_cellByColumnKey("payment_amt");
                            paymentamtCell.set_value("$0.00");
                        }

                        //should a checked item be unchecked, remove it from the arrays
                        for (i = 0; i < checkedIndeces.length; i++) {
                            if (checkedIndeces[i] == eventArgs.get_item().get_row().get_dataKey()[0].toString() + '|' + eventArgs.get_item().get_row().get_dataKey()[1].toString() + '|' + eventArgs.get_item().get_row().get_dataKey()[2].toString()) {
                                checkedIndeces.splice(i, 1);
                                checkedValues.splice(i, 1);
                            }
                        }

                        TotalAndDisplayValues();
                    }
                }
            }
            catch (ex) { }

            UpdateHiddenFields();
        }

        function round(value, exp) {
            if (typeof exp === 'undefined' || +exp === 0)
                return Math.round(value);

            value = +value;
            exp = +exp;

            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
                return NaN;

            // Shift
            value = value.toString().split('e');
            value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

            // Shift back
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
        }

        function WebHierarchicalDataGridDetail_ClientColumnSorting(sender, eventArgs) {
            $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField3").value = 'Sorting';
        }

        function BillingFlyout_Click() {
            if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_BillingFlyoutImage").src.indexOf("forward.png") != -1) {
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_BillingLeftSection").className = 'left-section';
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_BillingFlyoutImage").src = 'Images/back.png';
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField4").value = 'back';
                //adjustGrid(true);
                varAdjust = true;
            }
            else if ($get("ctl00_cphMaster1_MainTabControl1_Payments1_BillingFlyoutImage").src.indexOf("back.png") != -1) {
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_BillingLeftSection").className = 'left-section left-section-hidden';
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_BillingFlyoutImage").src = 'Images/forward.png';
                $get("ctl00_cphMaster1_MainTabControl1_Payments1_HiddenField4").value = 'forward';
                //adjustGrid(false);
                varAdjustGrid = false;
            }
        }

        function adjustGrid(leftSideExpanded) {

            var offset1;
            var offset2 = 20;
            var offset3 = 21;
            var offset4 = 100;
            var offset5 = 390;
            var offset6 = 350;

            if (leftSideExpanded) {
                offset1 = 250;
            } else {
                offset1 = 40;
            }

            var t = $(window).width()
            var e = t - 74;

            if (t > 1750) {
                e = t - offset5;
            } else if ((t > 820) && (t < 1750)) {
                e = t - offset6;
            }

            $(".right-section").css("width", t - offset1);
            $(".container-sec .custom-grid").css("width", e - offset2); var i = t - offset1; p = i - offset3; $(".right-section .container-sec .custom-grid").css("width", i); $(".right-section .container-sec .childgrid").css("width", p); var o = t - offset5; $(".pr-right-section .container-sec .custom-grid").css("width", o);
            $("right-section .container-sec .prodcut-row").css("width", o - offset4);
            $("right-section .container-sec .sorting-section").css("width", o - offset4);

        }

    </script>
    <script type="text/javascript" id="igClientScript">

        function ctl00_ImgLarge_ImageClick() {

            //Add code to handle your event here.

            var imageControl1 = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_imgLarge');

            var currentImageUrl = imageControl1.src;

            if (currentImageUrl.indexOf('http://images.dmsi.com') == 0 || currentImageUrl.indexOf('https://images.dmsi.com') == 0) {
                currentImageUrl = currentImageUrl.replace('__large', '');
                currentImageUrl = currentImageUrl.replace('__small', '');
                currentImageUrl = currentImageUrl.replace('.jpg', '__large.jpg');
                currentImageUrl = currentImageUrl.replace('.JPG', '__large.jpg');
            }

            imageControl1.src = currentImageUrl;
        }

        function ctl00_WebImageViewer2_ImageClick(sender, eventArgs) {

            //alert('I made it into the ctl00_WebImageViewer1_ImageClick(sender, eventArgs) handler.');

            ///<summary>
            ///
            ///</summary>
            ///<param name="sender" type="Infragistics.Web.UI.WebImageViewer"></param>
            ///<param name="eventArgs" type="Infragistics.Web.UI.ImageViewerEventArgs"></param>

            //Add code to handle your event here.
            var imageItem = eventArgs.getImageItem();
            var imageControl;
            if (sender._id == "ctl00_cphMaster1_MainTabControl1_Inventory1_WebImageViewer2") {
                imageControl = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_imgLarge');
            }
            else if (sender._id == "ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_WebImageViewer2") {
                imageControl = document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_imgLarge');
            }
            var currentImageUrl = imageItem._element.href;

            if (currentImageUrl == null) {
                currentImageUrl = imageItem._element.src;
            }

            //alert(currentImageUrl);

            if (currentImageUrl.toLowerCase().indexOf('http://images.dmsi.com') == 0 || currentImageUrl.toLowerCase().indexOf('https://images.dmsi.com') == 0) {
                currentImageUrl = currentImageUrl.replace('__large', '');
                currentImageUrl = currentImageUrl.replace('__small', '');
                currentImageUrl = currentImageUrl.replace('.jpg', '__large.jpg');
                currentImageUrl = currentImageUrl.replace('.JPG', '__large.jpg');
            }

            imageControl.src = currentImageUrl;
        }

        function ctl00_WebImageViewer1_ImageClick(sender, eventArgs) {


            //Add code to handle your event here.
            var imageItem = eventArgs.getImageItem();
            var imageControl;
            var imageControl1;
            if (sender._id == 'ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_WebImageViewer1') {
                imageControl = document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_imageItemDetail');
                imageControl1 = document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteEditAddItems_QuoteEditInventory_imgLarge');
            }
            else {
                imageControl = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_imageItemDetail');
                imageControl1 = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_imgLarge');
            }
            var currentImageUrl = imageItem._element.href;

            if (currentImageUrl == null) {
                currentImageUrl = imageItem._element.src;
            }

            //alert(currentImageUrl);

            if (currentImageUrl.indexOf('http://images.dmsi.com') == 0 || currentImageUrl.indexOf('https://images.dmsi.com') == 0) {
                currentImageUrl = currentImageUrl.replace('__large', '');
                currentImageUrl = currentImageUrl.replace('__small', '');
                currentImageUrl = currentImageUrl.replace('.jpg', '__large.jpg');
                currentImageUrl = currentImageUrl.replace('.JPG', '__large.jpg');
            }

            imageControl.src = currentImageUrl;
            imageControl1.src = currentImageUrl;
        }
    </script>
    <script type="text/javascript">



        function ShowPopup(id, popupID) {

            var winheight = $(window).height();
            winheight = winheight - 210;

            var Div = document.getElementById(id);
            var popUpDiv = document.getElementById(popupID);
            var iwindowheight = screen.height;
            if (iwindowheight > 540) {
                iwindowheight = iwindowheight - 0;
            }
            else {
                iwindowheight = iwindowheight - 180;
            }
            var divW = popUpDiv.offsetWidth;
            var divH = popUpDiv.offsetHeight;
            var left = getLeft(divW);
            var top = getTop(divH);
            popUpDiv.style.left = left;

            popUpDiv.style.top = top;
            if (id == 'ctl00_cphMaster1_MainTabControl1_QuoteRelease_OrderConfirmation' || 'ctl00_cphMaster1_MainTabControl1_OrderTab1_pnlOrderConfirmation') {
                //document.getElementById('ctl00_cphMaster1_MainTabControl1_QuoteRelease_dvquoterelease').style.height=iwindowheight+'px';
                document.getElementById('body-container').style.height = winheight + 'px';

                document.getElementById('body-container').style.background = 'gray';
            }
            Div.style.visibility = 'visible';



            return false
        };

        function HidePopup(id) {
            if (id != null) {
                document.getElementById(id).style.visibility = 'hidden'
            }
            return false
        };
        function getTop(divHeight) {
            xTop = null;
            var xHeight = getHeight();
            xTop = (xHeight - divHeight) / 2;
            return xTop;
        }
        function getWidth() {
            xWidth = null;
            if (window.screen != null)
                xWidth = window.screen.availWidth;

            if (window.innerWidth != null)
                xWidth = window.innerWidth;



            return xWidth;
        }


        function getLeft(divWidth) {
            xLeft = null;
            var xWidth = getWidth();
            xLeft = (xWidth - divWidth) / 2;
            return xLeft;
        }
        // Get the Height of the Screen

        function getHeight() {
            xHeight = null;
            if (window.screen != null)
                xHeight = window.screen.availHeight;

            if (window.innerHeight != null)
                xHeight = window.innerHeight;


            xHeight = Math.max(document.body.clientHeight, document.documentElement.clientHeight);

            return xHeight;
        }

        function numericMarkupFactor_ValueChanged(oEdit, fields, event) {
            if (oEdit._last != oEdit.value) {
                if (oEdit._last < oEdit._min || oEdit._last > oEdit._max) {
                    if (oEdit._min == 1 && oEdit._max == 999) {
                        alert(oEdit._last + " is an invalid value.\nYou are marking up from Cost so the multiplier needs to be greater than or equal to " + oEdit._min + ".");
                    }
                    else {
                        alert(oEdit._last + " is an invalid value.\nValue must be greater than or equal to " + oEdit._min + "\nand less than or equal to " + oEdit._max + ".");
                    }

                    var prefMarkup = document.getElementById("ctl00_cphMaster1_MainTabControl1_QuoteEdit_MultiplierSelector_preferredMarkupValue");

                    if (prefMarkup != null)
                        oEdit.set_value(prefMarkup.value);
                   
                    var prefMarkup2 = document.getElementById("ctl00_cphMaster1_MainTabControl1_QuoteEditRetail_Info_MultiplierSelector_preferredMarkupValue");

                    if (prefMarkup2 != null)
                        oEdit.set_value(prefMarkup2.value);
                }
            }
        }

    </script>


</asp:Content>
