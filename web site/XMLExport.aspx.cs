﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using Dmsi.Agility.EntryNET;

public partial class XMLExport : System.Web.UI.Page
{
    string _currentBranchID;
    int _tranID;

    protected void Page_Load(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new Dmsi.Agility.EntryNET.DataManager();
        _currentBranchID = (string)dm.GetCache("currentBranchID");

        _tranID = Convert.ToInt32(Request.QueryString["TranID"].ToString());

        TaxRateHelper trh = new TaxRateHelper(_tranID, Profile);
        trh.SaveDefaultTaxRateIfEmpty();

        DataSet ds = new DataSet();
        Dmsi.Agility.Data.Documents docs = new Dmsi.Agility.Data.Documents(ds);

        string xmlString = "";

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Value = "0";
        aInput.Label = "";

        if (Request.QueryString["Type"].ToString() == "Order")
        {
            xmlString = docs.GetSalesOrderXML(_tranID,"SO");

            aInput.Action = "Orders - XML Click";
           
        }

        if (Request.QueryString["Type"].ToString() == "Quote")
        {
            xmlString = docs.GetSalesOrderXML(_tranID, "Quot");
            aInput.Action = "Quotes - XML Click";
        }

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

        Response.Clear();
        XmlDocument xDoc = new XmlDocument();
        xDoc.LoadXml(xmlString);
        Response.ContentType = "text/plain";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + _currentBranchID + "_" + _tranID.ToString() + ".xml");
        xDoc.Save(Response.OutputStream);
        Response.End();
    }
}