using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Pages_loginassistance : BasePage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblPhone.Text = System.Configuration.ConfigurationManager.AppSettings["SupportPhone"].ToString();
        hplSupport.Text = System.Configuration.ConfigurationManager.AppSettings["SupportURL"].ToString();
        hplSupport.NavigateUrl = "http://" + System.Configuration.ConfigurationManager.AppSettings["SupportURL"].ToString();
        hplSupport.Target = "_blank";
    }
}
