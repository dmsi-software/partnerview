using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dmsi.Agility.EntryNET;

public partial class UpdateMessages : BasePage
{
    #region "Events"
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";

    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!this.IsPostBack)
        {

            if (Session["messageupdate"] != null)
            {
                Session["Ismessagechanged"] = "Yes";
                if ((string)Session["messageupdate"] == "true")
                {
                    DataRow row = (DataRow)Session["CurrentMessage"];

                    if (Session["CurrentMessage"] != null)
                    {
                        lblMessageViewHeader.Text = "Update message " + row["Sequence_Number"].ToString() + " for category " + row["Category_Name"].ToString();
                        rte.Value = row["Message_Text"].ToString();

                        Session["messageupdate"] = null;
                    }
                }
            }
            else
            {
                Session["Ismessagechanged"] = "Yes";
                DataRow row = (DataRow)Session["CurrentMessage"];
                if (row != null)
                {
                    lblMessageViewHeader.Text = "Add new message for category " + row["Category_Name"].ToString();
                }
                else
                {
                    DataRow rowcat = (DataRow)Session["CurrentCategory"];
                    Session["MessageAddMode"] = true;
                    if (rowcat != null)
                    {
                        lblMessageViewHeader.Text = "Add new message for category " + rowcat["Category_Name"].ToString();
                    }
                }
            }

            AssignMessagesDataSource();
        }
        else
        {
            // RichTextEditor uses submit to send data.
            // all other event should have NULL value for this control
            // except for btnSaveEdit (<input type="submit">)

            if (Request.Form["ctl00$cphMaster1$editor"] != null &&
                Request.Form["ctl00$cphMaster1$btnCancelEdit"] == null)
            {
                rte.Value = Request.Form["ctl00$cphMaster1$editor"];

                UpdateOrAddMessage();
            }
        }
    }
    #region "Buttons"
    protected void btnSaveEdit_Click(object sender, EventArgs e)
    {
        UpdateOrAddMessage();
    }
    protected void btnCancelEdit_Click(object sender, EventArgs e)
    {
        Session["Ismessagechanged"] = "Yes";
        Response.Redirect("~/default.aspx");
    }
    #endregion
    #endregion

    #region "Message Saving"
    private void UpdateOrAddMessage()
    {
        if (Session["MessageAddMode"] != null)
        {
            AddMessage();
            Session.Remove("MessageAddMode");

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Messages - Messages for Categories - Save Button";
            aInput.Label = "Message - Add Mode";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }
        else
        {
            SaveMessage();
        }
        Session["Ismessagechanged"] = "Yes";
        Response.Redirect("~/default.aspx");
    }
    protected void AddMessage()
    {
        if (rte.Value != "")
        {
            int seq = 1;

            if (Session["Messages"] != null)
                seq = ((DataTable)Session["Messages"]).Rows.Count + 1;

            try
            {
                DataRow currCategory = (DataRow)Session["CurrentCategory"];

                SqlDataSource2.InsertParameters["Sequence_Number"].DefaultValue = seq.ToString();
                SqlDataSource2.InsertParameters["Category_Name"].DefaultValue = currCategory["Category_Name"].ToString();
                SqlDataSource2.InsertParameters["Message_Text"].DefaultValue = rte.Value;
                SqlDataSource2.Insert();

                AssignMessagesDataSource();

                DataTable dt = (DataTable)Session["Messages"];
                Session["CurrentMessage"] = dt.Rows[dt.Rows.Count - 1];

                rte.Value = "";
            }
            catch
            {

            }
        }
        rte.Value = "";
    }
    protected void SaveMessage()
    {
        if (Session["CurrentMessage"] != null)
        {
            DataRow row = (DataRow)Session["CurrentMessage"];
            row["Message_Text"] = rte.Value;
            SqlDataSource2.UpdateParameters["Category_Name"].DefaultValue = row["Category_Name"].ToString();
            SqlDataSource2.UpdateParameters["Sequence_Number"].DefaultValue = row["Sequence_Number"].ToString();
            SqlDataSource2.UpdateParameters["Active"].DefaultValue = row["Active"].ToString();
            SqlDataSource2.UpdateParameters["Message_Text"].DefaultValue = row["Message_Text"].ToString();
            SqlDataSource2.Update();

            rte.Value = "";
            AssignMessagesDataSource();
        }
    }
    private void AssignMessagesDataSource()
    {
        if (Session["CurrentCategory"] != null)
        {
            DataRow categoryRow = (DataRow)Session["CurrentCategory"];
            SqlDataSource2.SelectParameters["Category_Name"].DefaultValue = categoryRow["Category_Name"].ToString();
        }
    }
    #endregion
}