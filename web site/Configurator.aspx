﻿<%@ Page ValidateRequest="false" Language="C#" AutoEventWireup="true" CodeFile="Configurator.aspx.cs" Inherits="Configurator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>Configurator</title>

    <script type="text/javascript" src="Silverlight.js"></script>

    <style type="text/css">
    html, body {
        height: 100%;
        overflow: auto;
    }
    body {
        padding: 0;
        margin: 0;
    }
    #silverlightControlHost {
        height: 100%;
        text-align:center;
    }
    </style>

    <script type="text/javascript">
        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }

        function CallingExit(obj) {
            var oExternal = window.external;
            if (oExternal != null) {
                //alert('window.external handle is not null...');
                //alert(obj);

                if (obj == "Finished") {
                    //oExternal.CommunicationFromBrowser("Finished");
                    oExternal.ShowMessageBox(obj, "Message from Silverlight and JavaScript...");
                    //oExternal.Close();
                }

                if (obj == "Canceled") {
                    //oExternal.CommunicationFromBrowser("Canceled");
                    //oExternal.ShowMessageBox("Message from Silverlight and JavaScript...");
                    oExternal.Close();
                }
            }
        }

        function page_onbeforeunload() {
            var SLPlugin = document.getElementById("SLObject");
            try {
                SLPlugin.Content.SL_ContentObj.PageUnloading();
            }
            catch (e) {
            }
        }

        function page_onunload() {
            if (window.opener != null && window.opener.demomask != null)
                window.opener.hidemaskpanel();
        }

        function ReturnToPV(obj) {
            if (obj == "Canceled") {
                if (window.opener.demomask)
                    window.opener.hidemaskpanel();

                window.opener = self;
                window.open('', '_self', '');
                this.close();
            }

            if (obj == "Save") {
                if (window.opener.demomask) {
                    window.opener.hidemaskpanel();

                    if (window.opener.document.all("ctl00_SaveConfigurationButton"))
                        window.opener.document.all("ctl00_SaveConfigurationButton").click();
                }

                window.opener = self;
                window.open('', '_self', '');
                this.close();
            }

            if (obj == "SaveQuote") {
                if (window.opener.demomask) {
                    window.opener.hidemaskpanel();

                    if (window.opener.document.all("ctl00_SaveQuoteConfigurationButton"))
                        window.opener.document.all("ctl00_SaveQuoteConfigurationButton").click();
                }

                window.opener = self;
                window.open('', '_self', '');
                this.close();
            }
        }
    </script>

    <script type="text/javascript"> 
    <!--
        window.onbeforeunload = page_onbeforeunload;
        window.onunload = page_onunload;
    //-->
    </script>

</head>
<body>
    <form id="formConfigurator" runat="server" style="height:100%">
    <div id="silverlightControlHost">
        <object id="SLObject" data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
            <param name="source" value="ClientBin/Configurator.xap" />
            <param name="onError" value="onSilverlightError" />
            <param name="background" value="white" />
            <param name="minRuntimeVersion" value="5.0.61118.0" />
            <param name="autoUpgrade" value="true" />
            <param name="enableHtmlAccess" value="true"/>
            <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=5.0.61118.0" style="text-decoration:none">
 			  <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
		    </a>
        </object>
        <iframe id="_sl_historyFrame" style="visibility: hidden; height: 0px; width: 0px; border: 0px"></iframe>
    </div>
    </form>
</body>
</html>
