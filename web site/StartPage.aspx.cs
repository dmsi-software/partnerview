﻿using System;
using System.Configuration;
using System.Data;
using System.Threading;
using System.Web.Security;
using System.Web.UI;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.Profile;

public partial class StartPage : BasePage
{
    protected DataManager _dataManager;
    protected ExceptionManager _exceptionManager;
    protected EventLog _eventLog;
    protected SecurityManager _securityManager;

    string _sUsername, _sPassword;

    protected string _errorMessageParam;
    protected string _errorMessageCatch;
    protected int _passwordRC;

    protected void Page_Load(object sender, EventArgs e)
    {
        DocManager.DocFolder = Server.MapPath(@"~/Documents/").ToString();

        //reset globals
        Session["UserPref_DisplaySetting"] = null;
        Session["UserPref_Retain"] = null;
        Session["MarkupFactor"] = null;
        Session["MarginFactor"] = null;
        Session["MarkupSetting"] = null;

        if (ConfigurationManager.AppSettings["StartProductSearchFirst"].ToString() == "false")
        {
            Session["GenericLogin"] = "False";
            Response.Redirect("~\\Logon.aspx");
        }
        else
        {
            Session["GenericLogin"] = "True";

            _dataManager = new DataManager();
            _exceptionManager = new ExceptionManager();
            _eventLog = new EventLog();
            _securityManager = new SecurityManager();
            _sUsername = ConfigurationManager.AppSettings["ProductSearchUserName"].ToString();
            _sPassword = ConfigurationManager.AppSettings["ProductSearchPassword"].ToString();

            Session["FromPage"] = "./Default.aspx";

            try
            {
                _dataManager.SetCache("userNameAtLogin", _sUsername.ToLower());
                //Login passes back an integer...
                _passwordRC = _securityManager.UserLogin("Product=PrtnrView", _sUsername.ToLower(), _sPassword, ref _errorMessageParam, ref _errorMessageCatch);

                _dataManager.SetCache("haveCalledLogin", true);

                //pass = -40;
                Session["UserName"] = _sUsername;
                Session["LoginMessage"] = _errorMessageParam.ToString().Replace("\n", "<br />");
                Session["passRC"] = _passwordRC;
                string errorMessage;
                errorMessage = _errorMessageParam;
                errorMessage += _errorMessageCatch;
                _exceptionManager.Message = errorMessage;

                switch (_passwordRC)
                {
                    case 0: // Success
                        LoginAccepted_SetupUser();

                        break;
                    case -1:  // System-type error or "No Customers"
                    case -10: // Invalid User Name or Password
                    case -20: // Login Failed. Your user account has been disabled.
                        if (_exceptionManager.MessageExists)
                        {
                            Dmsi.Agility.EntryNET.Connection connection = (Dmsi.Agility.EntryNET.Connection)Session["AgilityLibraryConnection"];
                            //   MainTabControl1.Controls[1].Visible = true;
                            if (connection != null)
                                connection.Disconnect();

                            throw new Exception(_exceptionManager.Message);
                        }
                        break;
                    case -30: // Your are required to change your password at login.
                    case -40: // Your password has expired and must be changed.
                        Session["UserName"] = _sUsername;
                        Response.Redirect("./ChangePassword.aspx", false);
                        break;
                    case -50: //Your password will expire in n days. Do you want to change your password?
                        LoginAccepted_SetupUser();
                        break;
                    default:
                        _exceptionManager.Message = "Fatal Error, no details available...please contact Support.";

                        throw new Exception(_exceptionManager.Message);
                }
            }
            catch (Exception exc)
            {
                txtMessages.Text = "Exception Message:\r\n" + exc.Message + "\r\n\r\n" +
                                   "Exception StackTrace:\r\n" + exc.StackTrace + "\r\n\r\n";

                if (exc.InnerException != null)
                {
                    txtMessages.Text = txtMessages.Text + "Exception Inner Exception StackTrace:\r\n" + exc.InnerException.StackTrace;
                }


            }
        }
    }

    private void LoginAccepted_SetupUser()
    {
        _sUsername = ConfigurationManager.AppSettings["ProductSearchUserName"].ToString();
        _sPassword = ConfigurationManager.AppSettings["ProductSearchPassword"].ToString();

        if ((_dataManager.GetCache("haveCalledLogin") != null) || (_dataManager.GetCache("ChangePasswordBeforeLoginSuccessful") != null))
        {
            bool hasDefaultShipto = false;
            PriceModeHelper pmh = new PriceModeHelper(_dataManager);

            _exceptionManager.Message = _errorMessageParam;
            _exceptionManager.Message += _errorMessageCatch;

            AgilityEntryNetUser user = new AgilityEntryNetUser();
            dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");
            dsCustomerDataSet dsCU = (dsCustomerDataSet)_dataManager.GetCache("dsCU");
            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dataManager.GetCache("dsPV");
            dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dataManager.GetCache("dsPVParam");
            _dataManager.SetCache("dsCustomerDataSet", dsCU);
            DataTable dtProperties = new DataTable();
            dtProperties.Columns.Add("propertyname");
            dtProperties.Columns.Add("Color");

            DataRow[] drDisplayExtDesc = dsPVParam.ttparam_pv.Select("property_name='" + "display_ext_desc" + "'");

            if (drDisplayExtDesc.Length > 0)
            {
                Session["display_ext_desc"] = drDisplayExtDesc[0]["property_value"].ToString();
            }

            DataRow[] drProductSearchQty = dsPVParam.ttparam_pv.Select("property_name='" + "product_search_qty" + "'");

            if (drProductSearchQty.Length > 0)
            {
                Session["product_search_qty"] = drProductSearchQty[0]["property_value"].ToString();
            }

            DataRow[] drlink_external_website = dsPVParam.ttparam_pv.Select("property_name='" + "link_external_website" + "'");

            if (drlink_external_website.Length > 0)
            {
                Session["link_external_website"] = true;

                DataRow[] drexternal_link = dsPVParam.ttparam_pv.Select("property_name='" + "external_link" + "'");
                Session["external_link"] = ((string)drexternal_link[0]["property_value"]).ToString();

                DataRow[] drexternal_url = dsPVParam.ttparam_pv.Select("property_name='" + "external_url" + "'");
                Session["external_url"] = ((string)drexternal_url[0]["property_value"]).ToString();
            }

            DataRow[] drCustomtheme;
            drCustomtheme = dsPVParam.ttparam_pv.Select("property_name ='" + "use_custom_theme" + "'");

            DataRow[] drThemeauthentication;
            drThemeauthentication = dsPV.bPV_Action.Select("token_code ='" + "disable_theme_administration" + "'");
            if (drCustomtheme.Length > 0)
            {
                Session["IsCustomTheme"] = drCustomtheme[0]["property_value"].ToString();
            }
            if (drThemeauthentication.Length > 0)
            {
                Session["disable_theme_authentication"] = "true";
            }
            else
            {
                Session["disable_theme_authentication"] = "false";
            }

            if (Session["IsCustomTheme"] != null)
            {
                if (((string)Session["IsCustomTheme"]).ToLower() == "yes")
                {
                    if (!System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                    {

                        DataTable dtDynamictheming = new DataTable();
                        dtDynamictheming.Columns.Add("propertyname");
                        dtDynamictheming.Columns.Add("Color");

                        DataRow[] rows = dsSessionManager.ttSelectedProperty.Select("propertyName='pvthemeActiveTabTextColor' OR " +
                                                                                    "propertyName='pvthemeInactiveButtonTextColor' OR " +
                                                                                    "propertyName='pvthemeGridHeaderTextColor' OR " +
                                                                                    "propertyName='pvthemeActionButtonTextColor' OR " +
                                                                                    "propertyName='pvthemeActionButtonColor' OR " +
                                                                                    "propertyName='pvthemeActiveLinkTextColor' OR " +
                                                                                    "propertyName='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                                    "propertyName='pvthemeGridHeaderColor' OR " +
                                                                                    "propertyName='pvthemeHeaderBarBackGroundColor' OR " +
                                                                                    "propertyName='pvthemeInactiveButtonColor' ");

                        if (rows.Length == 0)
                        {
                            rows = dsPVParam.ttparam_pv.Select("property_name='pvthemeActiveTabTextColor' OR  " +
                                                               "property_name='pvthemeInactiveButtonTextColor' OR " +
                                                               "property_name='pvthemeGridHeaderTextColor' OR " +
                                                               "property_name='pvthemeActionButtonTextColor' OR " +
                                                               "property_name='pvthemeActionButtonColor' OR " +
                                                               "property_name='pvthemeActiveLinkTextColor' OR " +
                                                               "property_name='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                               "property_name='pvthemeCustomerUserInformationTextColor' OR " +
                                                               "property_name='pvthemeCustomerUserInformationTextColor' OR " +
                                                               "property_name='pvthemeGridHeaderColor' OR " +
                                                               "property_name='pvthemeHeaderBarBackGroundColor' OR " +
                                                               "property_name='pvthemeInactiveButtonColor' ");
                        }

                        if (rows.Length > 0)
                        {
                            for (int i = 0; i < rows.Length; i++)
                            {
                                DataRow drnew;
                                drnew = dtDynamictheming.NewRow();
                                drnew["propertyname"] = rows[i]["propertyName"].ToString();
                                drnew["Color"] = rows[i]["propertyvalue"].ToString();
                                dtDynamictheming.Rows.Add(drnew);
                                dtDynamictheming.AcceptChanges();
                            }
                        }
                        if (dtProperties.Rows.Count > 0)
                        {
                            Session["ChangeTheme"] = dtDynamictheming;

                        }
                        CSSParser css = new CSSParser();

                        css.ReadCSSFile(Server.MapPath(@"~/Documents/DynamicThemes/DefaultTheme.css"), dtDynamictheming);

                        string sparsed;
                        sparsed = css.ToString();

                        if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                        {
                            System.IO.File.Delete(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css");
                        }
                        System.IO.File.WriteAllText(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css", sparsed.ToString());
                    }
                }
                else
                {


                }
            }
            StoreCustBranchTable(dsCU);

            switch (_passwordRC)
            {
                case 0:
                case -30:
                case -40:
                case -50:

                    ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
                    Profile.Initialize((string)Session["UserName"], false);

                    Profile.display_Setting = pc.display_Setting;

                    try
                    {
                        Profile.price_Markup_Factor = Convert.ToDecimal(pc.price_Markup_Factor.ToString());
                    }
                    catch
                    {
                        Profile.price_Markup_Factor = 1;
                    }

                    try
                    {
                        Profile.price_Markup_Factor_List = Convert.ToDecimal(pc.price_Markup_Factor_List.ToString());
                    }
                    catch
                    {
                        Profile.price_Markup_Factor_List = 1;
                    }

                    Profile.retain_Display = pc.retain_Display;

                    Profile.Save();

                    if (Profile.retain_Display == false)
                    {
                        Session["UserPref_DisplaySetting"] = pmh.NETPRICE;
                    }

                    if (!pmh.HaveNetModeEnabled() && !pmh.HaveRetailPricingEnabled())
                    {
                        throw new Exception(pmh.ERROR_ACTIONALLOCATIONMISMATCH);
                    }

                    if (pmh.InAModeUserDoesntHaveAccessTo())
                    {
                        Session["UserPref_DisplaySetting"] = pmh.DefaultUserPreferenceDisplaySetting();
                    }

                    foreach (dsPVParamDataSet.ttparam_pvRow paramrow in dsPVParam.ttparam_pv.Rows)
                    {
                        user.AddContext(paramrow.property_name, paramrow.property_value);
                    }

                    foreach (dsSessionMgrDataSet.ttSelectedPropertyRow row in dsSessionManager.ttSelectedProperty.Rows)
                    {
                        // MDM user.AddContext(row.propertyName, row.propertyValue, row.contextID);
                        user.AddContext(row.propertyName, row.propertyValue);
                    }

                    user.AddContext("currentUserPassword", _sPassword);

                    // cache the user object
                    _dataManager.SetCache("AgilityEntryNetUser", user);

                    // store user specific defaults
                    _dataManager.SetCache("AllowRetailPricing", user.GetContextValue("allow_retail_pricing"));

                    _dataManager.SetCache("AllowListBasedRetailPrice", user.GetContextValue("allow_list_based_retail_price"));

                    _dataManager.SetCache("currentBranchID", user.GetContextValue("currentBranchID"));
                    DataRow[] rows = dsCU.ttbranch_cust.Select("branch_id='" + user.GetContextValue("currentBranchID") + "'");

                    if (rows.Length > 0)
                    {
                        _dataManager.SetCache("CurrentBranchRow", rows[0]);
                    }

                    if (Profile.loggedInto453Already == false)
                    {
                        Profile.loggedInto453Already = true;
                        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvInventoryPricing_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvInventoryPricing_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderHeaders_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvSalesOrderHeaders_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderDetail_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvSalesOrderDetail_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderItems_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvSalesOrderItems_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteHeaders_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvQuoteHeaders_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteDetail_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvQuoteDetail_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteItems_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvQuoteItems_Columns_Visible = defaultValue;

                        Profile.Save();
                    }

                    break;
                default:
                    throw new Exception(_exceptionManager.Message);
            }

            _eventLog.LogEvent(EventTypeFlag.ApplicationEvent, "Successful login, username '" + (string)_dataManager.GetCache("userNameAtLogin").ToString().ToLower() + "'.");

            //TODO: create DataManager.UserConfigUpdate method...
            //Read default.config, [user name].config ...cross-check for new schema, update and write new [user name].config
            //...and cache all user's grid setting for all grids...


            // The following code is being commented out per Mike M.  
            // the interface to Datamanager.UserConfigUpdate does not play well with others 
            //myDataManager.UserConfigUpdate((string)myDataManager.GetCache("userNameAtLogin").ToString().ToLower(), Session.SessionID);

            string default_cust_key = dsPV.bPV_CoInfo.Rows[0]["default_cust_key"].ToString();
            int default_shipto_seq = (int)dsPV.bPV_CoInfo.Rows[0]["default_shipto_seq"];
            Session["LoggedIn"] = true;

            //MDM - Added cache values for PV/Visual Cafe; view_component_prices and view_incomplete_parent_price

            //Initialize to false on each login; even if in same session...
            Session["VC_view_component_prices"] = false;
            Session["VC_view_incomplete_parent_price"] = false;
            Session["VC_print_extended_price_on_vc_form"] = false;
            Session["UserAction_AllowRetailPricing"] = false;

            if (dsPV != null &&
                dsPV.bPV_Action != null &&
                dsPV.bPV_Action.Rows.Count > 0)
            {
                foreach (DataRow row in dsPV.bPV_Action.Rows)
                {
                    if (row["token_code"].ToString() == "view_component_prices")
                    {
                        Session["VC_view_component_prices"] = true;
                    }

                    if (row["token_code"].ToString() == "view_incomplete_parent_price")
                    {
                        Session["VC_view_incomplete_parent_price"] = true;
                    }

                    if (row["token_code"].ToString() == "print_extended_price_on_vc_form")
                    {
                        Session["VC_print_extended_price_on_vc_form"] = true;
                    }

                    if (row["token_code"].ToString() == "enable_retail_price_display")
                    {
                        Session["UserAction_AllowRetailPricing"] = true;

                        if (pmh.OnlyNetModeEnabled()) { Session["UserAction_AllowRetailPricing"] = false; }
                    }

                    if (row["token_code"].ToString() == "disable_on_screen_user_pref_control")
                    {
                        Session["UserAction_DisableOnScreenUserPreferences"] = true;
                    }
                }
            }

            if (user.GetContextValue("allow_retail_pricing").Trim().ToLower() == "no" || (bool)Session["UserAction_AllowRetailPricing"] == false)
            {
                if (Profile.UserName == (string)ConfigurationManager.AppSettings["ProductSearchUserName"])
                {
                    _dataManager.SetCache("AllowRetailPricing", "no");
                    Profile.display_Setting = "Net price";
                    Profile.Save();
                }
            }

            //TODO:Forms authentication...
            FormsAuthentication.SetAuthCookie(_sUsername, true);

            int counter = 0;

            retry: try
            {
                SqlDataSourcePVDatabase.SelectParameters["UserName"].DefaultValue = _sUsername.ToLower();
                DataView view = (DataView)SqlDataSourcePVDatabase.Select(DataSourceSelectArguments.Empty);

                if (view == null && (view != null && view.Count == 0))
                {
                    SqlDataSourcePVDatabase.InsertCommand = "INSERT INTO tbl_user (username) VALUES ('" + _sUsername.ToLower() + "')";
                    SqlDataSourcePVDatabase.Insert();
                }
            }
            catch
            {
                counter++;
                if (counter < 4)
                {
                    Thread.Sleep(2000);
                    goto retry;
                }
                else
                    throw;
            }

            string fromPage = Page.Request.Url.ToString().ToLower();

            if (fromPage.ToLower().IndexOf("logon.aspx?source=stocknet", 0) != -1)
            {
                // when coming from LogOn change the URL to point to default
                fromPage = fromPage.Replace("logon", "default");
                Session["FromStockNET"] = fromPage;
                Session["FromPage"] = fromPage;
            }

            if (default_cust_key.Length == 0)
            {
                SendControlTo("./default.aspx?Display=Customer Locator");
                //Response.Redirect(Session["FromPage"].ToString(), false);
            }
            else
            {
                if (dsCU.ttCustomer.Rows.Count > 0)
                {
                    // TODO: Put this in the back-end, clear the default customer when not found
                    //       in the current current customer list per current branch
                    //       When there is only one customer for the current branch use that as the default 
                    //       customer.
                    foreach (DataRow row in dsCU.ttCustomer.Rows)
                    {
                        if (row["cust_key"].ToString() == default_cust_key)
                        {
                            _dataManager.SetCache("CustomerRowForInformationPanel", row); // this is the current display

                            int ReturnCode;
                            string MessageText;

                            Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                            {
                                Session["LoggedIn"] = null;
                                Response.Redirect("~/Logon.aspx");
                            }

                            bool isBranchShared = cust.IsBranchSharedForCustomer(_dataManager.GetCache("currentBranchID").ToString(), out ReturnCode, out MessageText);

                            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                            {
                                Session["LoggedIn"] = null;
                                Response.Redirect("~/Logon.aspx");
                            }

                            if (!isBranchShared)
                                _dataManager.RemoveCache("CustomerRowForInformationPanel");
                            else // get the ship-to of cached customer
                            {
                                _dataManager.SetCache("BranchShipTos", cust.BranchShipTos);

                                // get the default shipto, this will drive the tab
                                if (default_shipto_seq > 0)
                                {
                                    // Verify if the currently selected ship-to is valid for the current customer
                                    DataRow shiptoRow = cust.GetShiptoRow(default_shipto_seq);
                                    if (shiptoRow != null)
                                    {
                                        hasDefaultShipto = true;
                                        _dataManager.SetCache("ShiptoRowForInformationPanel", shiptoRow);
                                        shiptoRow = null;
                                    }
                                }
                            }

                            cust.Dispose();
                            break;
                        }
                    }

                    // Check if this is a customer's customer log on, in some cases have only one customer and ship-to,
                    // Customer tab has to be hidden. Save a session variable for MainTabControl usage
                    if (dsCU.ttCustomer.Rows.Count == 1)
                    {
                        // Verify if the customer is valid for the current branch, "CurrentBranchRow" must be set first
                        _dataManager.SetCache("CustomerRowForInformationPanel", dsCU.ttCustomer.Rows[0]);

                        if (user.GetContextValue("HasMoreCustomers") != "TRUE") // customer from other branches
                            _dataManager.SetCache("SingleCust", dsCU.ttCustomer.Rows[0]);

                        int ReturnCode;
                        string MessageText;

                        Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                        {
                            Session["LoggedIn"] = null;
                            Response.Redirect("~/Logon.aspx");
                        }

                        bool isBranchShared = cust.IsBranchSharedForCustomer(_dataManager.GetCache("currentBranchID").ToString(), out ReturnCode, out MessageText);

                        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                        {
                            Session["LoggedIn"] = null;
                            Response.Redirect("~/Logon.aspx");
                        }

                        if (!isBranchShared)
                            _dataManager.RemoveCache("CustomerRowForInformationPanel");
                        else if (dsCU.Tables["ttShipto"].Rows.Count == 1)
                        {
                            // Check if the shipto is valid for the customer in the current branch
                            DataRow shiptoRow = cust.GetShiptoRow(((Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet.ttShiptoRow)dsCU.Tables["ttShipto"].Rows[0]).seq_num);

                            if (shiptoRow != null)
                            {
                                hasDefaultShipto = true;
                            }

                            // This will allow the program to be reset to the default values
                            // once the use switch to a branch where the values are valid again
                            if (user.GetContextValue("HasMoreCustomers") != "TRUE") // customer from other branches
                                _dataManager.SetCache("SingleShipto", dsCU.ttShipto.Rows[0]);  // this is needed during branch change         
                        }

                        cust.Dispose();
                    }
                }

                // if single shipto is present that means this user can only access one customer, so the locator is not applicable
                if (!hasDefaultShipto && _dataManager.GetCache("SingleShipto") == null)
                    SendControlTo("./default.aspx?Display=Customer Locator");
                else
                    SendControlTo(Session["FromPage"].ToString());
            }
        }
    }

    protected void SendControlTo(string destinationURL)
    {
        string[] parts = destinationURL.Split(new char[] { '?' });

        Session["IsHome"] = parts.Length == 1;

        switch (_passwordRC)
        {
            case 0:
                Response.Redirect(destinationURL, false);
                break;

            case -30:
            case -40:
            case -50:
                Session["FromPage"] = destinationURL;
                Response.Redirect("./ChangePassword.aspx", false);
                break;
        }

    }

    private void StoreCustBranchTable(dsCustomerDataSet dsCU)
    {
        dsCustomerDataSet.ttbranch_custDataTable table = new dsCustomerDataSet.ttbranch_custDataTable();

        foreach (dsCustomerDataSet.ttbranch_custRow row in dsCU.ttbranch_cust.Rows)
        {
            table.LoadDataRow(row.ItemArray, true);
        }

        Session["ttbranch_cust"] = table;
    }
}