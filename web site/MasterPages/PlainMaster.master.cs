using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;

public partial class MasterPages_PlainMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //MDM - PureChat code
        DataManager dm = new DataManager();
        if (dm.GetCache("ChatScript") != null && !Page.ClientScript.IsStartupScriptRegistered("ChatScript"))
        {
            string myScript = (string)dm.GetCache("ChatScript");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ChatScript", myScript, false);
        }

        // Initialize the logging file location
        if (!this.IsPostBack)
        {
            DocManager.DocFolder = Server.MapPath(@"~/Documents/").ToString();
        }

        //if (!Page.Request.Url.ToString().Contains("LogOn") && !Page.Request.Url.ToString().Contains("Logon"))
        //{
        //    int TimeOutInSeconds = (Session.Timeout - 1) * 60;
        //    HtmlMeta T_O_Meta = new HtmlMeta();

        //    T_O_Meta.HttpEquiv = "Refresh";
        //    T_O_Meta.Content = TimeOutInSeconds.ToString() + "; URL=./TimedOut.aspx";

        //    Page.Header.Controls.Add(T_O_Meta);
        //}
        
        this.Version.ToolTip = "Version " + Application["AgilityVersion"].ToString().Replace("_",".");

        //AddressValidator();
    }

    protected void Page_Init(object Sender, EventArgs e)
    {

        if (Session["IsCustomTheme"] != null)
        {
            if (((string)Session["IsCustomTheme"]).ToLower() == "yes")
            {
                if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                {
                    NewDemo.Href = "../Documents/DynamicThemes/" + "Custom" + ".css?v1.5";
                }
            }
            else
            {
                NewDemo.Href = "../Documents/DynamicThemes/" + "DefaultTheme" + ".css?v1.5";

            }
        }
        else
        {
            NewDemo.Href = "../Documents/DynamicThemes/" + "DefaultTheme" + ".css?v1.5";
        }
       

    }
    //protected void AddressValidator()
    //{
    //    bool useSsl = System.Configuration.ConfigurationManager.AppSettings["SSL"].ToString().ToLower() == "true";
    //    string MyUrl;
    //    MyUrl = Page.Request.Url.ToString();

    //    if (MyUrl.Contains("localhost") || (MyUrl.Contains("https:")) || !useSsl)
    //    {
    //        //You are in the right place
    //    }
    //    else
    //    {
    //        //You need to address this page as a ssl page,
    //        //you are now being redirected to a valid address.
    //        Response.Redirect(NewUrl(MyUrl.ToString()));
    //    }

    //}

    //protected string NewUrl(string OldUrl)
    //{
    //    string convertedURL;
    //    int stringLength;
    //    stringLength = OldUrl.Length - 4;
    //    convertedURL = "https" + OldUrl.Substring(4, stringLength);
    //    return convertedURL;

    //}

    protected void LogOutButton_Click(object sender, EventArgs e)
    {
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Logout Click"; 
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
               
        Response.Redirect("./LogOn.aspx");
    }
}
