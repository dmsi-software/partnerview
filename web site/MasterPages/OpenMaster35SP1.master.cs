﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

using Infragistics.Web.UI.NavigationControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Web.ClientServices;

public partial class MasterPages_OpenMaster35SP1 : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClearCacheItems();

        UserControls_OrderTab OrderTab1 = (UserControls_OrderTab)FindControlRecursive(this.cphMaster1, "OrderTab1");

        if (OrderTab1 != null)
        {
            OrderTab1.OnPublishCart += new UserControls_OrderTab.PublishCartDelegate(OrderTab1_OnPublishCart);
            OrderTab1.OnCartRowEditModeChanged += new UserControls_OrderTab.CartRowEditModeDelegate(OrderTab1_OnCartRowEditModeChanged);
        }

        UserControls_Inventory defaultInventory = (UserControls_Inventory)FindControlRecursive(this.cphMaster1, "Inventory1");
        if (defaultInventory != null)
        {
            defaultInventory.OnUserPrefModeChanged += new UserControls_Inventory.UserPrefModeDelegate(Inventory1_OnUserPrefModeChanged);
        }

        if (!IsPostBack)
        {
            hypHelp.NavigateUrl = ConfigurationManager.AppSettings["HelpFile"];
            hypHelp.Attributes.Add("onclick", "javascript:return hypHelp_click(this.id);");

            // Initialize the logging file location
            DocManager.DocFolder = Server.MapPath(@"~/Documents/").ToString();

            try
            {
                if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
                    HttpContext.Current.Session["UserPref_DisplaySetting"] != null && (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                {
                    this.OptionsSelector.MarkupFactor = Profile.price_Markup_Factor_List;
                    Session["MarkupFactor"] = Profile.price_Markup_Factor_List;
                }

                if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
                    HttpContext.Current.Session["UserPref_DisplaySetting"] != null && ((string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Net price"))
                {
                    this.OptionsSelector.MarkupFactor = Profile.price_Markup_Factor;
                    Session["MarkupFactor"] = Profile.price_Markup_Factor;
                }
            }
            catch
            {
                if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
                    HttpContext.Current.Session["UserPref_DisplaySetting"] != null && (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                {
                    decimal markupFactor = Convert.ToDecimal(System.Web.Profile.ProfileBase.Properties["price_Markup_Factor_List"].DefaultValue.ToString());
                    Profile.price_Markup_Factor_List = markupFactor;
                    this.OptionsSelector.MarkupFactor = markupFactor;
                    Session["MarkupFactor"] = markupFactor;
                }
                else
                {
                    decimal markupFactor = Convert.ToDecimal(System.Web.Profile.ProfileBase.Properties["price_Markup_Factor"].DefaultValue.ToString());
                    Profile.price_Markup_Factor = markupFactor;
                    this.OptionsSelector.MarkupFactor = markupFactor;
                    Session["MarkupFactor"] = markupFactor;
                }
            }

            this.OptionsSelector.MarkupSetting = Profile.ddlMarkup_Setting;
            Session["MarkupSetting"] = Profile.ddlMarkup_Setting;

            this.OptionsSelector.Retain = Profile.retain_Display;
            Session["UserPref_Retain"] = Profile.retain_Display;

            this.OptionsSelector.MarginFactor = Profile.price_Margin_Factor;
            Session["MarginFactor"] = Profile.price_Margin_Factor;

            if (this.OptionsSelector.Retain)
            {
                this.OptionsSelector.DisplaySetting = Profile.display_Setting;
                Session["UserPref_DisplaySetting"] = Profile.display_Setting;
            }
            else
            {
                if (Session["SaveDisplayHasBeenSet"] == null || (bool)Session["SaveDisplayHasBeenSet"] == false)
                {
                    string displaySetting = "";

                    try
                    {
                        displaySetting = System.Web.Profile.ProfileBase.Properties["display_Setting"].DefaultValue.ToString();
                    }
                    catch
                    {
                        displaySetting = "Net price";
                    }

                    Profile.display_Setting = displaySetting;
                    this.OptionsSelector.DisplaySetting = displaySetting;
                    Session["UserPref_DisplaySetting"] = displaySetting;
                    Session["SaveDisplayHasBeenSet"] = true;
                }
            }
        }
        else
        {
            if (Session["InventorybtnReturnToItem_Click"] != null)
            {
                if ((int)Session["InventorybtnReturnToItem_Click"] == 7 || (int)Session["InventorybtnReturnToItem_Click"] == 9)
                {
                    string sDisplay;
                    bool sCheckboxretain;
                    decimal dMarkupValue;
                    string sMarkupval = (string)Session["UserPref_NumericMarkup"];
                    sDisplay = (string)Session["UserPref_PiceDisplaySetting"];

                    if ((string)Session["UserPref_CheckboxRetain"] == "false")
                    {
                        sCheckboxretain = false;
                    }
                    else
                    {
                        sCheckboxretain = true;
                    }

                    if (!string.IsNullOrEmpty(sMarkupval))
                    {
                        dMarkupValue = Convert.ToDecimal(sMarkupval);
                    }
                    else
                    {
                        dMarkupValue = 1;
                    }

                    RefreshUIObjectsWhenUserPrefsChange(sDisplay, sCheckboxretain, dMarkupValue);
                }
            }
        }

        //Reinit the timer on every call...
        Timer1.Interval = (Session.Timeout - 1) * 60000;
        Timer1.Enabled = true;

        this.Version.ToolTip = "Version " + Application["AgilityVersion"].ToString().Replace("_", ".");

        string fromPage = Page.Request.Url.ToString().ToLower();

        if ((Session["LoggedIn"] == null) || ((bool)Session["LoggedIn"] != true))
        {
            //redirect to login page
            if (fromPage.ToLower().Contains("timedout"))
            {
                WelcomeHeader.Visible = false;
                WelcomeHeaderDisabled.Visible = false;

                if (ConfigurationManager.AppSettings["StartProductSearchFirst"].ToString() == "false")
                {
                    Response.Redirect("./LogOn.aspx");
                }
                else
                {
                    Response.Redirect("./StartPage.aspx");
                }
            }
            else
            {
                string[] pageParts = fromPage.Split(new char[] { '?' });

                if (pageParts.Length == 2)
                    Response.Redirect("./LogOn.aspx?" + pageParts[1]); // carry over querystring to next page
                else
                    Response.Redirect("./LogOn.aspx");
            }
        }
        else
        {
            // When the user login first before accessing something from StockNet the
            // LogOn.aspx won't be called again
            if (fromPage.ToLower().IndexOf("default.aspx?source=stocknet", 0) != -1)
                Session["FromStockNET"] = fromPage;

            Session["fromPage"] = fromPage;

            AddressValidator();

            WelcomeHeader.Text = WhoIsLoggedOn();
            WelcomeHeaderDisabled.Text = WelcomeHeader.Text;

            if ((string)Session["GenericLogin"] == "False")
            {
                WelcomeHeader.Visible = false;
                WelcomeHeaderDisabled.Visible = true;
            }
            else
            {
                WelcomeHeader.Text = "";
                WelcomeHeader.Visible = false;
                WelcomeHeaderDisabled.Text = "";
                WelcomeHeaderDisabled.Visible = false;
            }
        }
    }
    public void ClearCacheItems()
    {
        var enumerator = HttpContext.Current.Cache.GetEnumerator();

        while (enumerator.MoveNext())
        {
            HttpContext.Current.Cache.Remove(enumerator.Key.ToString());
        }
    }
    void Inventory1_OnUserPrefModeChanged(bool userPrefsOff)
    {
        if (userPrefsOff)
        {
            Session["DisableUserPrefs"] = userPrefsOff;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    this.WelcomeHeader.Visible = false;
                    this.OptionsModalDiv.Visible = false;
                    this.WelcomeHeaderDisabled.Visible = true;
                    this.OptionsModalDiv.FindControl("hypUserPreferences").Visible = false;
                    HyperLink hypUserPreferences = (HyperLink)this.FindControl("hypUserPreferences");
                    hypUserPreferences.Visible = false;

                    this.hypNetRetail.Enabled = false;
                }
            }
        }
        else
        {
            Session["DisableUserPrefs"] = userPrefsOff;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    this.WelcomeHeader.Visible = true;
                    this.OptionsModalDiv.Visible = true;
                    this.WelcomeHeaderDisabled.Visible = false;
                    this.OptionsModalDiv.FindControl("hypUserPreferences").Visible = true;
                    this.hypUserPreferences.Visible = true;
                    HyperLink hypUserPreferences = (HyperLink)this.FindControl("hypUserPreferences");
                    hypUserPreferences.Visible = true;

                    if (Session["UserAction_DisableOnScreenUserPreferences"] != null)
                    {
                        if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                        {
                            this.hypNetRetail.Enabled = true;

                            if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || 
                                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || 
                                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                            {
                                this.hypNetRetail.Text = "Retail";
                            }
                            else
                            {
                                this.hypNetRetail.Text = "Net";
                            }
                        }
                    }
                }
            }
        }
    }
    protected void Page_Init(object Sender, EventArgs e)
    {
        if ((string)Session["IsThemeChanged"] == "yes")
        {
            ClearCacheItems();
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Cache-Control", "no-cache");

            Response.CacheControl = "no-cache";
            Response.Expires = -1;

            Response.ExpiresAbsolute = new DateTime(1900, 1, 1);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }

         Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
         if (Session["IsCustomTheme"] != null)
         {
             if (((string)Session["IsCustomTheme"]).ToLower() == "yes")
             {
                 if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                 {
                     NewDemo.Href = "../Documents/DynamicThemes/" + "Custom" + ".css?v1.5";
                 }
             }
             else
             {
                 NewDemo.Href = "../Documents/DynamicThemes/" + "DefaultTheme" + ".css?v1.5";

             }
         }
         else
         {
             NewDemo.Href = "../Documents/DynamicThemes/" + "DefaultTheme" + ".css?v1.5";
         }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["IsThemeChanged"] != null)
        {
            if ((string)Session["IsThemeChanged"] == "yes")
            {
                ClearCacheItems();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun", "Themepicker();", true);
                Session["IsThemeChanged"] = null;
            }
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Shipto ship = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        lblCustName.Text = ship.ShiptoName;
        lblCustomerName.Text = ship.ShiptoName;

        if (ship.ShiptoSequence == 0)
        {
            lblCustAcctAndShipToSequence.Text = "Acct: " + cust.CustomerCode + "-None";
            lblAddress1.Text = "";
            lblAddress2.Text = "";
            lblCSZ.Text = "";
            uldropdown.Style.Add("Left", "-65px");
            return;
        }
        else
        {
            lblCustAcctAndShipToSequence.Text = "Acct: " + cust.CustomerCode + "-" + ship.ShiptoSequence;
            uldropdown.Style.Add("Left", "0px");
        }

        if (ship.Address1 != null)
            lblAddress1.Text = ship.Address1;
        else
            lblAddress1.Text = "";

        if (ship.Address2 != null && ship.Address2.Length == 0)
        {
            lblAddress2.Text = CreateCityStateZip(ship);
            lblCSZ.Text = "";
        }

        if (ship.Address2 != null && ship.Address2.Length > 0)
        {
            lblAddress2.Text = ship.Address2;
            lblCSZ.Text = CreateCityStateZip(ship);
        }

        if (Session["UserPref_DisplaySetting"] == null)
        {
            this.OptionsSelector.DisplaySetting = Profile.display_Setting;
            Session["UserPref_DisplaySetting"] = Profile.display_Setting;
        }

        this.OptionsSelector.Retain = Profile.retain_Display;
        Session["UserPref_Retain"] = Profile.retain_Display;

        if (Session["DisableUserPrefs"] != null)
        {
            if ((bool)Session["DisableUserPrefs"])
            {
                if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
                {
                    if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                    {
                        this.WelcomeHeader.Visible = false;
                        this.WelcomeHeaderDisabled.Visible = true;
                    }
                }
            }
            else
            {
                if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
                {
                    if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                    {
                        this.WelcomeHeader.Visible = true;
                        this.WelcomeHeaderDisabled.Visible = false;
                    }
                }
            }
        }

        if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
            HttpContext.Current.Session["UserPref_DisplaySetting"] != null && (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
        {
            try
            {
                this.OptionsSelector.MarkupFactor = Profile.price_Markup_Factor_List;
                Session["MarkupFactor"] = Profile.price_Markup_Factor_List;
            }
            catch
            {
                Profile.price_Markup_Factor_List = 1;
                this.OptionsSelector.MarkupFactor = Profile.price_Markup_Factor_List;
                Session["MarkupFactor"] = Profile.price_Markup_Factor_List;
            }
        }
        else
        {
            try
            {
                this.OptionsSelector.MarkupFactor = Profile.price_Markup_Factor;
                Session["MarkupFactor"] = Profile.price_Markup_Factor;
            }
            catch
            {
                Profile.price_Markup_Factor = 1;
                this.OptionsSelector.MarkupFactor = Profile.price_Markup_Factor;
                Session["MarkupFactor"] = Profile.price_Markup_Factor;
            }
        }

        //MDM - PureChat code
        if (dm.GetCache("ChatScript") != null && !Page.ClientScript.IsStartupScriptRegistered("ChatScript"))
        {
            string myScript = (string)dm.GetCache("ChatScript");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ChatScript", myScript, false);
        }
    }

    private string CreateCityStateZip(Shipto ship)
    {
        string citystatezip = "";

        if (ship.City != null && ship.City.Length > 0)
            citystatezip += ship.City;

        if (ship.City != null && ship.City.Length > 0 && (ship.State.Length > 0 || ship.Zip.Length > 0))
            citystatezip += ", ";

        if (ship.State != null && ship.State.Length > 0)
            citystatezip += ship.State + " ";

        if (ship.Zip != null && ship.Zip.Length > 0)
            citystatezip += ship.Zip;

        return citystatezip;
    }

    void OrderTab1_OnPublishCart(int itemCount, string cartTotal)
    {
        lblCart.Text = "Cart (" + itemCount.ToString() + ")";
        lblCartTotal.Text = cartTotal;
    }

    void OrderTab1_OnCartRowEditModeChanged(bool isInEditMode)
    {
        if (isInEditMode)
        {
            Session["DisableUserPrefs"] = isInEditMode;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    this.WelcomeHeader.Visible = false;
                     this.OptionsModalDiv.Visible = true;
                    this.WelcomeHeaderDisabled.Visible = true;
                    HyperLink hpl = (HyperLink)this.FindControl("hypUserPreferences");
                    hpl.Visible = true;

                    if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                    {
                        this.liNetRetail.Visible = true;
                        this.hypNetRetail.Enabled = true;

                        if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || 
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || 
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                        {
                            this.hypNetRetail.Text = "Retail";
                        }
                        else
                        {
                            this.hypNetRetail.Text = "Net";
                        }
                    }
                }
            }
        }
        else if ((bool)Session["UIisOnCheckout"] == false)
        {
            Session["DisableUserPrefs"] = isInEditMode;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    this.WelcomeHeader.Visible = true;
                    this.OptionsModalDiv.Visible = true;
                    this.WelcomeHeaderDisabled.Visible = false;
                    HyperLink hpl = (HyperLink)this.FindControl("hypUserPreferences");
                    hpl.Visible = true;

                    if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                    {
                        this.liNetRetail.Visible = true;
                        this.hypNetRetail.Enabled = true;


                        if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || 
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || 
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                        {
                            this.hypNetRetail.Text = "Retail";
                        }
                        else
                        {
                            this.hypNetRetail.Text = "Net";
                        }
                    }
                }
            }
            else
            {
                this.WelcomeHeader.Visible = true;
                this.OptionsModalDiv.Visible = false;
                this.WelcomeHeaderDisabled.Visible = false;
                HyperLink hpl = (HyperLink)this.FindControl("hypUserPreferences");
                hpl.Visible = false;

                this.liNetRetail.Visible = false;
            }
        }

        if (Session["UIisOnInventoryDetail"] != null && (bool)Session["UIisOnInventoryDetail"] == true)
        {
            Session["DisableUserPrefs"] = true;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    this.WelcomeHeader.Visible = false;
                    this.OptionsModalDiv.Visible = false;
                    this.WelcomeHeaderDisabled.Visible = true;
                    HyperLink hpl = (HyperLink)this.FindControl("hypUserPreferences");
                    hpl.Visible = false;

                    hypNetRetail.Enabled = false;
                }
            }
            else
            {
                this.WelcomeHeader.Visible = true;
                this.OptionsModalDiv.Visible = false;
                this.WelcomeHeaderDisabled.Visible = false;
                HyperLink hpl = (HyperLink)this.FindControl("hypUserPreferences");
                hpl.Visible = false;

                hypNetRetail.Enabled = false;
            }
        }
        else if (Session["UIisOnInventoryDetail"] != null && (bool)Session["UIisOnInventoryDetail"] == true)
        { }
    }

    protected void AddressValidator()
    {
        string destination;
        destination = Page.Request.Url.ToString();
        if (destination.Contains("localhost") || (destination.Contains("http:")))
        {
            //we are in the right place
        }
        else
        {
            if (ConfigurationManager.AppSettings["SSL"].ToLower() == "true")
                Response.Redirect(ConvertHttpsToHttp(destination.ToString()));
        }
    }

    protected string ConvertHttpsToHttp(string OldUrl)
    {
        string convertedURL;
        int stringLength;
        stringLength = OldUrl.Length - 5;
        convertedURL = "http" + OldUrl.Substring(5, stringLength);
        return convertedURL;
    }

    protected string WhoIsLoggedOn()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string userName = "";

        // Display the user name
        if (dm.GetCache("AgilityEntryNetUser") != null)
        {
            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
            userName = user.GetContextValue("currentUserName").ToString();
        }
        else
            userName = "".ToUpper();

        return userName;
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        if (Session["SavedProductGroupMajorGUID"] != null)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

            if (fi.Exists)
            {
                fi.Delete();
            }
        }

        Session["SavedProductGroupMajorGUID"] = null; //Reset, so new Product Group Majors will be refreshed upon new login in same IIS session.
                
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Logout Click";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
        
        Response.Redirect("./LogOn.aspx");
    }

    protected void btnHelp_Click(object sender, EventArgs e)
    {
        string HelpPage;
        HelpPage = ConfigurationManager.AppSettings["HelpFile"];
        Response.Redirect(HelpPage);
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string userLogin = "";

        if (dm.GetCache("AgilityEntryNetUser") != null)
        {
            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
            userLogin = user.GetContextValue("currentUserLogin").ToString();
        }

        if (userLogin.ToLower() == System.Configuration.ConfigurationManager.AppSettings["ProductSearchUserName"].ToString().ToLower())
            Response.Redirect("./Default.aspx");
        else
            Response.Redirect("./LogOn.aspx");
    }

    protected void LogOutButton_Click(object sender, EventArgs e)
    {
        if (Session["SavedProductGroupMajorGUID"] != null)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

            if (fi.Exists)
            {
                fi.Delete();
            }
        }

        Session["SavedProductGroupMajorGUID"] = null; //Reset, so new Product Group Majors will be refreshed upon new login in same IIS session.

        Response.Redirect("./LogOn.aspx");
    }

    protected void SaveConfigurationButton_Click(object sender, EventArgs e)
    {
        UserControls_OrderTab orderTab = FindControlRecursive(this, "OrderTab1") as UserControls_OrderTab;
        orderTab.btnClose_Click(sender, null);
        orderTab.AssignDataSource(true);
    }

    protected void SaveQuoteConfigurationButton_Click(object sender, EventArgs e)
    {
        UserControls_QuoteEdit quoteEditControl = FindControlRecursive(this, "QuoteEdit") as UserControls_QuoteEdit;
        quoteEditControl.UpdateQuoteAfterVisualCafeSave();
    }

    /// <summary>
    /// Finds a Control recursively. Note finds the first match that exists...
    /// </summary>
    /// <param name="Root"></param>
    /// <param name="Id"></param>
    /// <returns></returns>
    private Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

        return null;
    }

    protected void hypHome_Click(object sender, EventArgs e)
    {
        ChooseVisibleContent("Home1");
    }

    protected void lnkChangeCustomer_Click(object sender, EventArgs e)
    {
      
        ChooseVisibleContent("CustomerTabPanel1");

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Account Change Link";
        aInput.Label  = "Change link under account in upper menu";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
    }

    protected void ChooseVisibleContent(string contentID)
    {
       
        MainTabControl tabs = (MainTabControl)FindControlRecursive(this.cphMaster1, "MainTabControl1");
        if (tabs != null)
        {
            tabs.AllTabsOff();
            WebDataMenu menu = (WebDataMenu)tabs.FindControl("WebDataMenu1");
            if (menu != null)
                menu.SelectedItem = null;
        }

        Panel p = (Panel)FindControlRecursive(this.cphMaster1, "pnlContentContainer");
        if (p != null)
        {
            p.Visible = true;
            HideAllContent(p);
            MakeContentVisible(p, contentID);
        }
    }

   
    protected void HideAllContent(Panel p)
    {
        foreach (Control cntrls in p.Controls)
        {
            if (cntrls as UserControl != null)
                cntrls.Visible = false;
        }
    }

    protected void MakeContentVisible(Panel p, string contentID)
    {
        UserControl control = (UserControl)p.FindControl(contentID);
        if (control != null)
            control.Visible = true;
    }
    protected void imgbtnCart_Click(object sender, ImageClickEventArgs e)
    {
        ChooseVisibleContent("OrderTab1");

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Cart Icon Click";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
   }

    protected void lblCart_Click(object sender, EventArgs e)
    {
        ChooseVisibleContent("OrderTab1");

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Cart Label Click";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
    }
    protected void ChangeSettings_Click(object sender, EventArgs e)
    {
        ChooseVisibleContent("PasswordChanges1");
        Panel p = (Panel)FindControlRecursive(this.cphMaster1, "pnlContentContainer");
        UserControl control1 = (UserControl)p.FindControl("PasswordChanges1");
        UserControl control = (UserControl)p.FindControl("PasswordChanges1").FindControl("PasswordMaintSettings");
        UserControl msgmaint = (UserControl)p.FindControl("PasswordChanges1").FindControl("MessagesMaint1");
        UserControl ChangeTheme=(UserControl)p.FindControl("PasswordChanges1").FindControl("ChangeTheme1");
        HtmlControl dvlogview=(HtmlControl)p.FindControl("PasswordChanges1").FindControl("LogView1");
        HtmlControl dvlogpanel = (HtmlControl)p.FindControl("PasswordChanges1").FindControl("adsearchdiv");

        dvlogpanel.Visible = false;
        dvlogview.Visible = false;
        ChangeTheme.Visible = false;
        msgmaint.Visible = false;
        LinkButton lnks = (LinkButton)control1.FindControl("lnkchangepass");
        LinkButton lnkChange = (LinkButton)control1.FindControl("lnkChange");
        lnkChange.CssClass = "inactiveli";
        LinkButton lnkLogview = (LinkButton)control1.FindControl("lnkLogview");
        lnkLogview.CssClass = "inactiveli";
        LinkButton lnkmessages = (LinkButton)control1.FindControl("lnkmessages");
        lnkmessages.CssClass = "inactiveli";
        System.Web.UI.HtmlControls.HtmlGenericControl lilogview = (System.Web.UI.HtmlControls.HtmlGenericControl)control1.FindControl("lilogview");
        System.Web.UI.HtmlControls.HtmlGenericControl limessage = (System.Web.UI.HtmlControls.HtmlGenericControl)control1.FindControl("limessage");

        Label lbllblSttingName = (Label)control1.FindControl("lblSttingName");
       
        if (lnks != null)
        {
            lnks.CssClass = "inactiveli";
        }

        if (lbllblSttingName != null)
        {
            lbllblSttingName.Text = "Settings";
        }

        if (control1 != null)
        {
            control1.Visible = true;
        }

        if(control!=null)
        {
            control.Visible = false;
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");
        int logmessagedisable = 0;
        int logviewrdisable = 0;

        if (user != null && user.GetContextValue("currentUseDebug") != null && user.GetContextValue("currentUseDebug").ToLower().Equals("yes"))
        {
            lnkLogview.Visible = true;
            lilogview.Visible = true;
            logviewrdisable = 0;
        }
        else
        {
            lnkLogview.Visible = false;
            lilogview.Visible = false;
            logviewrdisable = 1;
        }

        if (Session["disable_theme_authentication"] != null)
        {
            if ((string)Session["disable_theme_authentication"] == "true")
            {
                lnkChange.Visible = false;
            }
            else
            {
                lnkChange.Visible = true;
            }
        }
        else
        {
            lnkChange.Visible = true;
        }

        if (dsPV != null &&
           dsPV.bPV_Action != null &&
           dsPV.bPV_Action.Rows.Count > 0 &&
           user.GetContextValue("UserType").ToLower() == "internal")
        {
            bool readOnly = false;

            if (string.IsNullOrEmpty(lbllblSttingName.Text))
            {
                lbllblSttingName.Text = "Settings";
            }

            foreach (DataRow row in dsPV.bPV_Action.Rows)
            {
                if (row["token_code"].ToString() == "read_only_content_msgs")
                {
                    readOnly = true;
                    break;
                }

            }

            if (!readOnly)
            {
                lnkmessages.Visible = true;
                limessage.Visible = true;
                logmessagedisable = 0;
            }
            else
            {
                lnkmessages.Visible = false;
                limessage.Visible = false;

                if ((string)Session["disable_theme_authentication"] == "true")
                {
                    logmessagedisable = 1;
                }
                else
                {
                    logmessagedisable = 0;
                }
            }
        }
        else
        {
            lnkmessages.Visible = false;
            limessage.Visible = false;
            control.Visible = true;
            lnks.CssClass = "activesettings";
            logmessagedisable = 1;
            logmessagedisable = 1;
        
            if (Session["disable_theme_authentication"] != null)
            {
                if ((string)Session["disable_theme_authentication"] == "true")
                {
                    logmessagedisable = 1;
                    logviewrdisable = 1;
                }
                else
                {
                    logmessagedisable = 0;

                }
            }
            else
            {
                logmessagedisable = 1;
                logviewrdisable = 1;
            }
        }
        if ((logmessagedisable == 1) &&  (logviewrdisable==1))
        {
            control.Visible = true;
            lnks.CssClass = "activesettings";
            lbllblSttingName.Text = "Change Password";

            if ((string)Session["disable_theme_authentication"] == "true")
            {
                lnks.CssClass = "activesettings activechangepassword";
            }
        }
        else
        {
            lnks.CssClass = "inactiveli";
        }

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   =  "event";
        aInput.Action =  "User Menu - Settings Link Click";
        aInput.Label  =  "";
        aInput.Value  =  "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
               
    }
    protected void ChangePassword_Click(object sender, EventArgs e)
    {
        ChooseVisibleContent("PasswordChanges1");

        Panel p = (Panel)FindControlRecursive(this.cphMaster1, "pnlContentContainer");
        UserControl control1 = (UserControl)p.FindControl("PasswordChanges1");
        UserControl control = (UserControl)p.FindControl("PasswordChanges1").FindControl("PasswordMaintSettings");
        LinkButton lnks = (LinkButton)control1.FindControl("lnkchangepass");
        Label lbllblSttingName = (Label)control1.FindControl("lblSttingName");
       
        LinkButton lnkChange = (LinkButton)control1.FindControl("lnkChange");
        lnkChange.CssClass = "inactiveli";
        LinkButton lnkLogview = (LinkButton)control1.FindControl("lnkLogview");
        lnkLogview.CssClass = "inactiveli";
        LinkButton lnkmessages = (LinkButton)control1.FindControl("lnkmessages");
        lnkmessages.CssClass = "inactiveli";
        UserControl msgmaint = (UserControl)p.FindControl("PasswordChanges1").FindControl("MessagesMaint1");
        UserControl ChangeTheme = (UserControl)p.FindControl("PasswordChanges1").FindControl("ChangeTheme1");
        HtmlControl dvlogview = (HtmlControl)p.FindControl("PasswordChanges1").FindControl("LogView1");
        HtmlControl dvlogpanel = (HtmlControl)p.FindControl("PasswordChanges1").FindControl("adsearchdiv");
        msgmaint.Visible = false;
        ChangeTheme.Visible = false;
        dvlogview.Visible = false;
        ChangeTheme.Visible = false;
        dvlogpanel.Visible = false;

        if (lnks != null)
        {
            lnks.CssClass = "activesettings";
        }

        if (lbllblSttingName != null)
        {
            lbllblSttingName.Text = "Change Password";
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");
        int logmessagedisable = 0;
        int logviewrdisable = 0;

        if (user != null && user.GetContextValue("currentUseDebug") != null && user.GetContextValue("currentUseDebug").ToLower().Equals("yes"))
        {
            logviewrdisable = 0;
        }
        else
        {
            logviewrdisable = 1;
        }

        if (dsPV != null &&
           dsPV.bPV_Action != null &&
           dsPV.bPV_Action.Rows.Count > 0 &&
           user.GetContextValue("UserType").ToLower() == "internal")
        {
            bool readOnly = false;

            foreach (DataRow row in dsPV.bPV_Action.Rows)
            {
                if (row["token_code"].ToString() == "read_only_content_msgs")
                {
                    readOnly = true;
                    break;
                }

            }

            if (!readOnly)
            {
                logmessagedisable = 0;
            }
            else
            {
                if ((string)Session["disable_theme_authentication"] == "true")
                {
                    logmessagedisable = 1;
                }
                else
                {
                    logmessagedisable = 0;
                }
            }
        }
        else
        {
            logmessagedisable = 1;
            logmessagedisable = 1;

            if (Session["disable_theme_authentication"] != null)
            {
                if ((string)Session["disable_theme_authentication"] == "true")
                {
                    logmessagedisable = 1;
                    logviewrdisable = 1;
                }
                else
                {
                    logmessagedisable = 0;
                }
            }
            else
            {
                logmessagedisable = 1;
                logviewrdisable = 1;
            }
        }

        if (control != null)
            control.Visible = true;

        if ((logmessagedisable == 1) && (logviewrdisable == 1))
        {
           
            lnkmessages.Visible = false;
            lnkChange.Visible = false;
            lnkLogview.Visible = false;
            if ((string)Session["disable_theme_authentication"] == "true")
            {
                lnks.CssClass = "activesettings activechangepassword";
            }
        }

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "User Menu - Change Password Link Click (Screen Request)";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
    }
    private void RefreshUIObjectsWhenUserPrefsChange(string displaySetting, bool retainDisplay, decimal markupFactor)
    {
        bool dataHasChanged = true;
        bool displaySettingHasChanged = true;

        if (Session["UserPref_DisplaySetting"] != null && displaySetting == (string)Session["UserPref_DisplaySetting"])
            displaySettingHasChanged = false;

        if (Session["UserPref_DisplaySetting"] != null && displaySetting == (string)Session["UserPref_DisplaySetting"] && retainDisplay == (bool)Session["UserPref_Retain"] && markupFactor == Convert.ToDecimal(Session["MarkupFactor"]))
            dataHasChanged = false;

        if (retainDisplay)
            Profile.display_Setting = displaySetting;

        Session["UserPref_DisplaySetting"] = displaySetting;

        Profile.retain_Display = retainDisplay;
        Session["UserPref_Retain"] = retainDisplay;

        if (HttpContext.Current.Session["AllowListBasedRetailPrice"] != null && ((string)HttpContext.Current.Session["AllowListBasedRetailPrice"]).ToLower() == "yes" &&
            HttpContext.Current.Session["UserPref_DisplaySetting"] != null && (string)HttpContext.Current.Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
        {
            Profile.price_Markup_Factor_List = markupFactor;
            Session["MarkupFactor"] = markupFactor;
        }
        else
        {
            Profile.price_Markup_Factor = markupFactor;
            Session["MarkupFactor"] = markupFactor;
        }

        if (Profile.IsDirty)
            Profile.Save();
        
        if (retainDisplay)
            Session["SaveDisplayHasBeenSet"] = false;
        else
            Session["SaveDisplayHasBeenSet"] = true;

        //TODO: look at values and update UI when necessary...
        if (dataHasChanged)
        {
            DataManager dm = new DataManager();
            if (dm.GetCache("currentBranchID") != null)
            {
                UserControls_OrderTab control = FindControlRecursive(this, "OrderTab1") as UserControls_OrderTab;
                if (control != null)
                {
                    control.SetPricingView();
                    control.ApplyUserPrefPriceColumnSelection();
                    control.Reset((string)dm.GetCache("currentBranchID"));
                }
            }

            if (Session["CachedInventoryDataSet_QEAI"] != null)
            {
                bool needsReload = false;

                Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet dsCached = (Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet)Session["CachedInventoryDataSet_QEAI"];
                if (dsCached.ttinventory.Rows.Count > 0)
                    needsReload = true;

                if (needsReload)
                {
                    UserControls_Inventory control = FindControlRecursive(this, "QuoteEditInventory") as UserControls_Inventory;
                    if (control != null)
                    {
                        if (Session["btnViewQuickList_Click_QEAI"].ToString() == "1")
                            Session["NeedToClickQuoteEditAddItemsQuicklist"] = true;
                        else
                            Session["NeedToClickQuoteEditAddItemsSearch"] = true;
                    }
                }
            }

            UserControls_SalesOrders control3 = FindControlRecursive(this, "SalesOrders1") as UserControls_SalesOrders;
            if (control3 != null)
            {
                if (displaySettingHasChanged)
                {
                    control3.InitializeColumnVisibility(control3.GvSalesOrderHeaders, "gvSalesOrderHeaders_Columns_Visible", true);
                    control3.InitializeColumnVisibility(control3.GvSalesOrderByItem, "gvSalesOrderItems_Columns_Visible", true);

                    if (dm.GetCache("dsSalesOrderDataSet") != null && ((dsSalesOrderDataSet)dm.GetCache("dsSalesOrderDataSet")).ttso_header.Rows.Count > 0)
                        control3.btnFind_Click(null, null);
                    else if (dm.GetCache("dsSalesOrderDataSet") != null && ((dsSalesOrderDataSet)dm.GetCache("dsSalesOrderDataSet")).ttso_byitem.Rows.Count > 0)
                        control3.btnFind_Click(null, null);
                }
            }

            UserControls_Quotes control4 = FindControlRecursive(this, "Quotes1") as UserControls_Quotes;
            if (control4 != null)
            {
                control4.InitializeColumnVisibility(control4.GvQuoteHeaders, "gvQuoteHeaders_Columns_Visible", 3, true);
                control4.InitializeColumnVisibility(control4.GvQuoteByItem, "gvQuoteItems_Columns_Visible", 0, true);

                if (dm.GetCache("dsQuoteDataSet") != null && ((dsQuoteDataSet)dm.GetCache("dsQuoteDataSet")).pvttquote_header.Rows.Count > 0)
                    control4.btnFind_Click(null, null);
                else if (dm.GetCache("dsQuoteDataSet") != null && ((dsQuoteDataSet)dm.GetCache("dsQuoteDataSet")).pvttquote_byitem.Rows.Count > 0)
                    control4.btnFind_Click(null, null);
            }

            //TODO: handle being in the middle of a QuoteEdit...
            if (Session["IsQuoteInUpdateMode"] != null && (bool)Session["IsQuoteInUpdateMode"] == true)
                Session["UserPrefChangedInMiddleOfQuoteEdit"] = true;

            if (Session["IsQuoteInUpdateModeRetail_Info"] != null && (bool)Session["IsQuoteInUpdateModeRetail_Info"] == true)
                Session["UserPrefChangedInMiddleOfQuoteEditRetail_Info"] = true;

            Profile.Save();
        }
    }

    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        MainTabControl tabs = (MainTabControl)FindControlRecursive(this.cphMaster1, "MainTabControl1");

        tabs.ddlBranch_SelectedIndexChanged(sender, e);

        DropDownList ddl = (DropDownList)tabs.FindControl("ddlBranches");
        Session["RecallFetchAttributes"] = 1;
        ddl.SelectedIndexChanged += new EventHandler(tabs.ddlBranch_SelectedIndexChanged);

        Session["BranchHasChanged"] = true;

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Change Branch Menu Selection";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
    }

    protected void hypNetRetail_Click(object sender, EventArgs e)
    {
        this.ModalPopupExtenderOptions.Show();

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "On Screen Net Retail click";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
    }
}


