using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;

using System.Web.UI.WebControls.WebParts;

using System.ComponentModel;

using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Data;
public partial class MasterPages_SecureMaster : System.Web.UI.MasterPage
{
    protected DataManager _dataManager;
    protected ExceptionManager _exceptionManager;
    protected EventLog _eventLog;
    protected SecurityManager _securityManager;

    string _sUsername, _sPassword;
    protected string _errorMessageParam;
    protected string _errorMessageCatch;
    protected int _passwordRC;
    protected void Page_Load(object sender, EventArgs e)
    {
        //MDM - PureChat code
        DataManager dm = new DataManager();
        if (dm.GetCache("ChatScript") != null && !Page.ClientScript.IsStartupScriptRegistered("ChatScript"))
        {
            string myScript = (string)dm.GetCache("ChatScript");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ChatScript", myScript, false);
        }

        // Initialize the logging file location
        if (!this.IsPostBack)
        {
            DocManager.DocFolder = Server.MapPath(@"~/Documents/").ToString();
            
        }

        if (!Page.Request.Url.ToString().Contains("LogOn") && !Page.Request.Url.ToString().Contains("Logon"))
        {
            int TimeOutInSeconds = (Session.Timeout - 1) * 60;
            HtmlMeta T_O_Meta = new HtmlMeta();

            T_O_Meta.HttpEquiv = "Refresh";
            T_O_Meta.Content = TimeOutInSeconds.ToString() + "; URL=./TimedOut.aspx";

            Page.Header.Controls.Add(T_O_Meta);
        }
        
        this.Version.ToolTip = "Version " + Application["AgilityVersion"].ToString().Replace("_",".");

        AddressValidator();
    }
    protected void Page_Init(object Sender, EventArgs e)
    {
        if (System.Configuration.ConfigurationManager.AppSettings["StartProductSearchFirst"].ToLower() != "true")
        {
            if (Session["IsCustomTheme"] == null)
            {
                LoginAccepted_SetupUser();
            }
        }
        if (Session["IsCustomTheme"] != null)
        {
            if (((string)Session["IsCustomTheme"]).ToLower() == "yes")
            {
                if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                {
                    securedemo.Href = "../Documents/DynamicThemes/" + "Custom" + ".css?v1.5";
                }
            }
            else
            {
                securedemo.Href = "../Documents/DynamicThemes/" + "DefaultTheme" + ".css?v1.5";

            }
        }
        else
        {
            securedemo.Href = "../Documents/DynamicThemes/" + "DefaultTheme" + ".css?v1.5";
        }
        //NewDemo.Href

    }
    protected void AddressValidator()
    {
        bool useSsl = System.Configuration.ConfigurationManager.AppSettings["SSL"].ToString().ToLower() == "true";
        string MyUrl;
        MyUrl = Page.Request.Url.ToString();

        if (MyUrl.Contains("localhost") || (MyUrl.Contains("https:")) || !useSsl)
        {
            //You are in the right place
        }
        else
        {
            //You need to address this page as a ssl page,
            //you are now being redirected to a valid address.
            Response.Redirect(NewUrl(MyUrl.ToString()));
        }

    }

    protected string NewUrl(string OldUrl)
    {
        string convertedURL;
        int stringLength;
        stringLength = OldUrl.Length - 4;
        convertedURL = "https" + OldUrl.Substring(4, stringLength);
        return convertedURL;

    }

    protected void LogOutButton_Click(object sender, EventArgs e)
    {
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Logout Click";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
                
        Response.Redirect("./LogOn.aspx");
    }
    private void LoginAccepted_SetupUser()
    {
        _sUsername = ConfigurationManager.AppSettings["ProductSearchUserName"].ToString();
        _sPassword = ConfigurationManager.AppSettings["ProductSearchPassword"].ToString();
        

        _dataManager = new DataManager();
        _exceptionManager = new ExceptionManager();
        _eventLog = new EventLog();
        _securityManager = new SecurityManager();
        _passwordRC = _securityManager.UserLogin("Product=PrtnrView", _sUsername.ToLower(), _sPassword, ref _errorMessageParam, ref _errorMessageCatch);

        AgilityEntryNetUser user = new AgilityEntryNetUser();
        dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");
        dsCustomerDataSet dsCU = (dsCustomerDataSet)_dataManager.GetCache("dsCU");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dataManager.GetCache("dsPV");
        dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dataManager.GetCache("dsPVParam");

        _dataManager.SetCache("dsCustomerDataSet", dsCU);
        DataTable dtProperties = new DataTable();
        dtProperties.Columns.Add("propertyName");
        dtProperties.Columns.Add("Color");
        DataRow[] drCustomtheme;
        drCustomtheme = dsPVParam.ttparam_pv.Select("property_name='" + "use_custom_theme" + "'");

        DataRow[] drThemeauthentication;
        drThemeauthentication = dsPV.bPV_Action.Select("token_code ='" + "disable_theme_administration" + "'");
        if (drCustomtheme.Length > 0)
        {
            Session["IsCustomTheme"] = drCustomtheme[0]["property_value"].ToString();
            Session["IsCustomThemes"] = drCustomtheme[0]["property_value"].ToString();
        }
        if (drThemeauthentication.Length > 0)
        {
            Session["disable_theme_authentication"] = "true";
        }
        else
        {
            Session["disable_theme_authentication"] = "false";
        }

        if (Session["IsCustomTheme"] != null)
        {
            if (((string)Session["IsCustomTheme"]).ToLower() == "yes")
            {
                if (!System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                {

                    DataTable dtDynamictheming = new DataTable();
                    dtDynamictheming.Columns.Add("propertyName");
                    dtDynamictheming.Columns.Add("Color");

                    DataRow[] rows = dsSessionManager.ttSelectedProperty.Select("propertyName='pvthemeActiveTabTextColor' OR " +
                                                                                "propertyName='pvthemeInactiveButtonTextColor' OR " +
                                                                                "propertyName='pvthemeGridHeaderTextColor' OR " +
                                                                                "propertyName='pvthemeActionButtonTextColor' OR " +
                                                                                "propertyName='pvthemeActionButtonColor' OR " +
                                                                                "propertyName='pvthemeActiveLinkTextColor' OR " +
                                                                                "propertyName='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                                                "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                                "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                                "propertyName='pvthemeGridHeaderColor' OR " +
                                                                                "propertyName='pvthemeHeaderBarBackGroundColor' OR " +
                                                                                "propertyName='pvthemeInactiveButtonColor' ");

                    if (rows.Length == 0)
                    {
                        rows = dsPVParam.ttparam_pv.Select("property_name='pvthemeActiveTabTextColor' OR  " +
                                                           "property_name='pvthemeInactiveButtonTextColor' OR " +
                                                           "property_name='pvthemeGridHeaderTextColor' OR " +
                                                           "property_name='pvthemeActionButtonTextColor' OR " +
                                                           "property_name='pvthemeActionButtonColor' OR " +
                                                           "property_name='pvthemeActiveLinkTextColor' OR " +
                                                           "property_name='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                           "property_name='pvthemeCustomerUserInformationTextColor' OR " +
                                                           "property_name='pvthemeCustomerUserInformationTextColor' OR " +
                                                           "property_name='pvthemeGridHeaderColor' OR " +
                                                           "property_name='pvthemeHeaderBarBackGroundColor' OR " +
                                                           "property_name='pvthemeInactiveButtonColor' ");
                    }

                    if (rows.Length > 0)
                    {
                        for (int i = 0; i < rows.Length; i++)
                        {
                            DataRow drnew;
                            drnew = dtDynamictheming.NewRow();
                            drnew["propertyName"] = rows[i]["propertyName"].ToString();
                            drnew["Color"] = rows[i]["propertyvalue"].ToString();
                            dtDynamictheming.Rows.Add(drnew);
                            dtDynamictheming.AcceptChanges();
                        }
                    }
                    if (dtProperties.Rows.Count > 0)
                    {
                        Session["ChangeTheme"] = dtDynamictheming;

                    }
                    CSSParser css = new CSSParser();

                    css.ReadCSSFile(Server.MapPath(@"~/Documents/DynamicThemes/DefaultTheme.css"), dtDynamictheming);

                    string sparsed;
                    sparsed = css.ToString();

                    if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                    {
                        System.IO.File.Delete(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css");
                    }
                    System.IO.File.WriteAllText(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css", sparsed.ToString());
                }
               
            }
            else
            {


            }
        }
       

    }
}
