using System;
using System.Configuration;
using System.Web.UI;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;

public partial class MasterPages_SecureMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //MDM - PureChat code
        DataManager dm = new DataManager();
        if (dm.GetCache("ChatScript") != null && !Page.ClientScript.IsStartupScriptRegistered("ChatScript"))
        {
            string myScript = (string)dm.GetCache("ChatScript");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ChatScript", myScript, false);
        }

        // Initialize the logging file location
        if (!this.IsPostBack)
        {
            DocManager.DocFolder = Server.MapPath(@"~/Documents/").ToString();
        }

        WelcomeHeader.Text = WhoIsLoggedOn();

        this.hypHelp.NavigateUrl = ConfigurationManager.AppSettings["HelpFile"];
    }

    protected void LogOutButton_Click(object sender, EventArgs e)
    {
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type   = "event";
        aInput.Action = "Logout Click";
        aInput.Label  = "";
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
        
        Response.Redirect("./LogOn.aspx");
    }

    protected void hypHome_Click(object sender, EventArgs e)
    {
        Session["IsHome"] = true;
        Response.Redirect("default.aspx?Display=Home");
    }

    protected void imgLogo_Click(object sender, ImageClickEventArgs e)
    {
        hypHome_Click(null, null);
    }

    protected string WhoIsLoggedOn()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        } 

        string userName = "";

        // Display the user name
        if (dm.GetCache("AgilityEntryNetUser") != null)
        {
            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
            userName = user.GetContextValue("currentUserName").ToString();
        }
        else
            userName = "".ToUpper();

        return userName;
    }
}
