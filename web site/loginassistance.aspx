<%@ Page Language="C#" AutoEventWireup="true" CodeFile="loginassistance.aspx.cs" Inherits="Pages_loginassistance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>EntryNET Login Assistance</title>
</head>
<body>
		<TABLE id="Table1" cellSpacing="2" cellPadding="2" width="100%" align="center" bgColor="white" border="0">
			<TR>
				<TD class="FieldLabel" align="center">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="FieldLabel" align="center">Login Assistance</TD>
			</TR>
			<TR>
				<TD class="Field">
					<UL>
						<LI>
						Is your CAPS LOCK on?
						<LI>
							Check the spelling of your username and password</LI>
						<LI>
							Did you type your password in the correct case? The password field is 
							case-sensitive.</LI></UL>
				</TD>
			</TR>
			<TR>
				<TD class="FieldLabel" align="center">If you still need assistance, contact 
					Customer Support:</TD>
			</TR>
			<TR>
				<TD class="Field">
					<UL>
						<LI>
							<asp:Label id="lblPhone" runat="server"></asp:Label>
						<LI>
							<asp:HyperLink id="hplSupport" runat="server"></asp:HyperLink></LI></UL>
				</TD>
			</TR>
		</TABLE>
</body>
</html>
