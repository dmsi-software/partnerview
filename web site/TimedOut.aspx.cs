using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class TimedOut : BasePage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new Dmsi.Agility.EntryNET.DataManager();
        Dmsi.Agility.EntryNET.Connection connection = (Dmsi.Agility.EntryNET.Connection)dm.GetCache("AgilityLibraryConnection");

        if (connection != null)
            connection.Disconnect();
        connection = null;
        Session.RemoveAll();

        //// FormsAuthentication.SignOut();
        int TimeOutInSeconds = 60;
        HtmlMeta T_O_Meta = new HtmlMeta();
        T_O_Meta.HttpEquiv = "Refresh";
        if (System.Configuration.ConfigurationManager.AppSettings["StartProductSearchFirst"].ToLower() == "true")
        {
            if ((Session["LoggedIn"] == null) || ((bool)Session["LoggedIn"] != true))
            {
                 T_O_Meta.Content = TimeOutInSeconds.ToString() + "; URL=./StartPage.aspx";

            }
        }
        else
        {

            T_O_Meta.Content = TimeOutInSeconds.ToString() + "; URL=./Logon.aspx";
        }

        Page.Header.Controls.Add(T_O_Meta);
    }
    protected void lbtnGoToLogin_Click(object sender, EventArgs e)
    {
        if (System.Configuration.ConfigurationManager.AppSettings["StartProductSearchFirst"].ToLower() == "true")
        {
            if ((Session["LoggedIn"] == null) || ((bool)Session["LoggedIn"] != true))
            {
               Response.Redirect( "~/StartPage.aspx");

            }
        }
        else
        {

            Response.Redirect( "~/Logon.aspx");
        }

    }
}

