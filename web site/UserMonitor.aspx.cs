using System;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using Dmsi.Agility.EntryNET;

public partial class UserMonitor : BasePage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Session sessionSubAppObject;
        EventManager em = new EventManager();

        // Initiate a connection to the AppServer
        string userName = "Default";
        string password = "Default";
        Connection connection = em.SessionStart(ref userName, ref password);
        sessionSubAppObject = new Dmsi.Agility.EntryNET.Session(connection.EntryNetProxy);

        // We may include an application level login here
        string str;
        sessionSubAppObject.sifetchpvlogin(out str);

        //Dispose immediately before page has a chance to unload.
        sessionSubAppObject.Dispose();
        connection.EntryNetProxy.Dispose();
        connection.AppServerConnection.Dispose();
        
        // When this page is ready for automation switch Basepage back to System.Web.UI.Page 
        // Remove or comment out anything after Respose.End()
        if (Request.QueryString["output"] != null && Request.QueryString["output"] == "xml")
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(str);
            Response.End();
        }
        else
        {
            //XmlDataDocument xmlDoc = new XmlDataDocument();
            string[] parts = str.Split(new char[] { '\n' });
            //xmlDoc.LoadXml(parts[1].Trim());

            DataSet ds = new DataSet("logins");
            StringReader reader = new StringReader(parts[1]);
            ds.ReadXml(reader);
            AssignDataSource(ds, false, gvLogins1, "block=1");
            AssignDataSource(ds, false, gvLogins2, "block=2");
            AssignDataSource(ds, false, gvLogins3, "block=3");
            AssignDataSource(ds, false, gvLogins4, "block=4");
        }
    }

    private void AssignDataSource(DataSet ds, bool empty, GridView gv, string filter)
    {
        //DataTable dt = ds.Tables[0];
        DataView dv = new DataView(ds.Tables[0], filter, "block", DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("TableCopy"); // this will give us the correct column arrangement

        if (dt.Rows.Count > 0 && !empty)
        {
            gv.DataSource = dt;
            gv.DataBind();
        }
        else
        {
            // this is a work around so the grid will show when the datasource is empty
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);
        }

        switch (gv.ID)
        {
            case "gvLogins1":
                lblCurrent.Text = "Current (" + dt.Rows[0]["startdate"] + "-" + dt.Rows[0]["enddate"] + ")";
                break;
            case "gvLogins2":
                lblLast.Text = "Last (" + dt.Rows[0]["startdate"] + "-" + dt.Rows[0]["enddate"] + ")";
                break;
            case "gvLogins3":
                lbl2Months.Text = "2 Months Prior (" + dt.Rows[0]["startdate"] + "-" + dt.Rows[0]["enddate"] + ")";
                break;
            case "gvLogins4":
                lbl3Months.Text = "3 Months Prior (" + dt.Rows[0]["startdate"] + "-" + dt.Rows[0]["enddate"] + ")";
                break;
        }
    }
    
    private static void RowDataBound(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }
            }

            e.Row.Attributes.Add("onmouseover", "this.className='GridViewRowMouseHover'");
            e.Row.Attributes.Add("onmouseout", "this.className='GridViewRowMouseOut'");
        }
    }
    protected void gvLogins_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        RowDataBound(e);
    }
}
