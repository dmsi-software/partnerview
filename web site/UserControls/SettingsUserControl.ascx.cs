﻿using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class UserControls_SettingsUserControl : System.Web.UI.UserControl
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);

        csCustomLogView.GridView = this.gvLogView;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        if (user != null && user.GetContextValue("currentUseDebug") != null && user.GetContextValue("currentUseDebug").ToLower().Equals("yes"))
        {
            lnkLogview.Visible = true;
        }
        else
        {
            lnkLogview.Visible = false;
        }

        if ((string)Session["disable_theme_authentication"] == "true")
        {
            lnkChange.Visible = false;
        }
        else
        {
            lnkChange.Visible = true;
        }

        if (dsPV != null &&
            dsPV.bPV_Action != null &&
            dsPV.bPV_Action.Rows.Count > 0 )
        {
            bool readOnly = false;

            foreach (DataRow row in dsPV.bPV_Action.Rows)
            {
                if (row["token_code"].ToString() == "read_only_content_msgs")
                {
                    readOnly = true;
                    break;
                }
            }

            if (!readOnly)
            {
                lnkmessages.Visible = true; 
            }
            else
            {
                lnkmessages.Visible = false;
            }
        }
    }   
             

    protected void Page_Load(object sender, EventArgs e)
    {
        this.csCustomLogView.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomLogView_OnOKClicked);
        this.csCustomLogView.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomLogView_OnResetClicked);



        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");



        if (!IsPostBack)
        {

            dm.SetCache("gvLogViewSortColumn", "time");
            dm.SetCache("gvLogViewSortDirection", " DESC");


            this.gvLogView.PageSize = Convert.ToInt32(this.ddlLogViewRows.SelectedValue);
            dm.SetCache("GridShow", Request.QueryString["Display"]);



            bool clearSort = false;
            dm.SetCache("clearSort", clearSort);

        }
    }

    protected void lnkPasswordMaint_Click(object sender, EventArgs e)
    {
        PasswordMaintSettings.Visible = true;
        LogView1.Visible = false;
        MessagesMaint1.Visible = false;
    }

    protected void lnkLogViewer_Click(object sender, EventArgs e)
    {
        PasswordMaintSettings.Visible = false;
        LogView1.Visible = true;
        hdnlogview.Value = "1";
        MessagesMaint1.Visible = false;
    }

    protected void lnkMessages_Click(object sender, EventArgs e)
    {
        PasswordMaintSettings.Visible = false;
        LogView1.Visible = false;
        MessagesMaint1.Visible = true;
    }

    protected void lnkHome_Click(object sender, EventArgs e)
    {
        Redirect();
    }

    private void Redirect()
    {
        int PasswordReturnCode = Convert.ToInt32(Session["passRC"]);
        string urlName = Convert.ToString(Session["Pagefrom"]);

        switch (PasswordReturnCode)
        {
            case -30:
            case -40:
                Response.Redirect("./TimedOut.aspx");
                break;

            case -50:
                Session["passRC"] = 0;
                Session["LoggedIn"] = true;
                Response.Redirect("default.aspx?Display=Customers");
                break;

            default:
                Session["IsHome"] = true;
                Response.Redirect("default.aspx?Display=Home");
                break;

        }
    }

    protected void WebDataMenu1_ItemClick(object sender, Infragistics.Web.UI.NavigationControls.DataMenuItemEventArgs e)
    {

    }

    protected void lnk_Click(object sender, EventArgs e)
    {
        lnkChange.CssClass = "inactiveli";
        lnkchangepass.CssClass = "inactiveli";
        lnkLogview.CssClass = "inactiveli";
        lnkmessages.CssClass = "inactiveli";
        LinkButton lnk = (LinkButton)sender;
        lnk.CssClass = "activesettings";

        AnalyticsInput aInput = new AnalyticsInput();
        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);

        switch (lnk.Text.ToLower())
        {
            case "home":

                Redirect();
                adsearchdiv.Visible = false;
                break;

            case "change password":
                lblSttingName.Text = "Change Password";
                PasswordMaintSettings.Visible = true;
                LogView1.Visible = false;
                MessagesMaint1.Visible = false;
                ChangeTheme1.Visible = false;
                adsearchdiv.Visible = false;
                
                /* Analytics Tracking */
                aInput.Type = "event";
                aInput.Action = "Settings - Change Password Button (Screen Request)";
                aInput.Label = "";
                aInput.Value = "0";

                am.BeginTrackEvent();
                
                break;

            case "log viewer":
                XML_Reader();
                InitializeColumnVisibility(gvLogView, "gvLogViewer_Columns_Visible");
                AssignDataSource(false, gvLogView);
                adsearchdiv.Visible = true;
                lblSttingName.Text = "Log Viewer";
                PasswordMaintSettings.Visible = false;
                LogView1.Visible = true;
                MessagesMaint1.Visible = false;
                hdnlogview.Value = "1";
                ChangeTheme1.Visible = false;

                /* Analytics Tracking */
                aInput.Type = "event";
                aInput.Action = "Settings - Log Viewer Button";
                aInput.Label = "";
                aInput.Value = "0";

                am.BeginTrackEvent();

                break;

            case "messages":
                adsearchdiv.Visible = false;
                lblSttingName.Text = "Messages";
                PasswordMaintSettings.Visible = false;
                LogView1.Visible = false;
                MessagesMaint1.Visible = true;
                ChangeTheme1.Visible = false;

                /* Analytics Tracking */
                aInput.Type = "event";
                aInput.Action = "Settings - Messages Button";
                aInput.Label = "";
                aInput.Value = "0";

                am.BeginTrackEvent();
                
                break;

            case "change theme":
                adsearchdiv.Visible = false;
                lblSttingName.Text = "Change Theme";
                PasswordMaintSettings.Visible = false;
                LogView1.Visible = false;
                MessagesMaint1.Visible = false;
                ChangeTheme1.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ColorThemesInAndroid();", true);

                /* Analytics Tracking */
                aInput.Type = "event";
                aInput.Action = "Settings - Change Theme Button";
                aInput.Label = "";
                aInput.Value = "0";

                am.BeginTrackEvent();
                
                break;
        }
    }
    #region
    protected void gvLogView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small");
    }
    protected void gvLogView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvLogView.PageIndex = e.NewPageIndex;
        this.AssignDataSource(false, gvLogView);
    }
    protected void gvLogView_Sorting(object sender, GridViewSortEventArgs e)
    {
        Grid_Sorting((GridView)sender, e, "gvLogViewSortColumn", "gvLogViewSortDirection");
        this.AssignDataSource(false, gvLogView);
    }
    protected void gvLogView_RowCreated(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvLogView, e.Row, "gvLogViewSortColumn", "gvLogViewSortDirection", 0);
    }

    protected void csCustomLogView_OnOKClicked(CheckBoxList checkBoxList)
    {
        Profile.gvLogViewer_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvLogView);
        this.AssignDataSource(false, gvLogView);
    }

    private static void ManipulateRow(GridViewRow row, string largeorsmall)
    {
        for (int i = 0; i < row.Cells.Count; i++)
        {
            if (row.Cells[i] != null && row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
            {
                row.Cells[i].Text = row.Cells[i].Text.Replace(" ", "&nbsp;");
            }
        }

        if (largeorsmall == "large")
        {
            row.Attributes.Add("onmouseover", "this.className='GridViewRowMouseHover'");
        }
        else
        {
            row.Attributes.Add("onmouseover", "this.className='GridViewRowMouseHover'");
        }

        row.Attributes.Add("onmouseout", "this.className='GridViewRowMouseOut'");
    }



    private void AssignDataSource(bool empty, GridView gv)
    {
        // fetch all
        string gvName = gv.ID.ToString();
        string filter = "";
        string SortColumnName = gvName + "SortColumn";
        string SortDirection = gvName + "SortDirection";
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string sort = (string)dm.GetCache(SortColumnName);
        DataTable dt;

        if (sort == null || sort.Length == 0)
        {
            sort = "time DESC";
            dm.SetCache(SortColumnName, "time ");
            dm.SetCache(SortDirection, "DESC");

        }
        else
        {
            sort += dm.GetCache(SortDirection);
        }

        // First column is the Select link, do this to force new arrangement of fields
        string searchCriteria = (string)dm.GetCache("LogViewSearchCriteria");
        DataTable dtLogView = (DataTable)dm.GetCache("dtLogView");

        double timeValue = Convert.ToDouble(ddlTimeSpan.SelectedValue.ToString());
        DateTime myTime = System.DateTime.Now.AddDays(timeValue * -1);
        int myDateStamp = (myTime.Year * 10000) + (myTime.Month * 100) + (myTime.Day);
        string myTimeStamp = " ";
        if (myTime.Hour < 10)
        {
            myTimeStamp += "0";
        }
        myTimeStamp += (myTime.TimeOfDay.ToString());
        filter = "time > '" + myDateStamp.ToString() + myTimeStamp.ToString() + "'";
        DataView dv = new DataView();

        dv = new DataView(dtLogView, filter, sort, DataViewRowState.CurrentRows);

        filter = this.BuildFilter(gvName);
        if (filter.Length < 1)
            dt = dv.ToTable("dtLogView_Copy");
        else
        {
            DataTable dtTemp = dv.ToTable("dtLogView_TempCopy");
            dv = new DataView(dtTemp, filter, sort, DataViewRowState.CurrentRows);
            dt = dv.ToTable("dtLogView_Copy");
        }

        if (dt.Rows.Count < 200)
            lblLogViewError.Visible = false;
        else
            this.lblLogViewError.Visible = true;

        if (dt.Rows.Count > 0)
        {
            dm.RemoveCache("NoLogEventsAvailable"); // this way the postback knows the grid is not empty
            gv.DataSource = dt;
            gv.DataBind();
            if (gv.ID == "gvLogView")
            {

                int ipageindex = gv.PageIndex;
                int igridcount = gv.Rows.Count;
                int itotalrecords = dt.Rows.Count;

                int iNoofrows = 0;
                if (ddlLogViewRows.SelectedValue == "All")
                {
                    iNoofrows = 3200;
                }
                else
                {
                    iNoofrows = Convert.ToInt32(ddlLogViewRows.SelectedValue);
                }
                int iFirstrecordno = ipageindex * (iNoofrows);
                iFirstrecordno = iFirstrecordno + 1;
                int iLastRecord = (iFirstrecordno - 1) + igridcount;
                if (itotalrecords > 0)
                {
                    lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
                }
            }
        }
        else
        {
            lblpageno.Text = "";
            // this is a work around so the grid will show when the datasource is empty
            dm.SetCache("NoLogEventsAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);
        }
    }

    private string BuildFilter(string qvName)
    {
        string filter = "";
        if (txtTranID.Text.Length > 0)
            filter = ddlTranID.SelectedValue.ToString() + "='" + txtTranID.Text.ToString() + "'";
        return filter;
    }
    protected void XML_Reader()
    {
        string myDocFolder;

        myDocFolder = Server.MapPath(@"~/Documents/").ToString();
        Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();

        DataSet ds = new DataSet();
        System.Xml.XmlReaderSettings myXmlSettings = new XmlReaderSettings();
        myXmlSettings.DtdProcessing = DtdProcessing.Parse;
        myXmlSettings.ValidationType = ValidationType.DTD;
        System.IO.FileStream myFS = null;
        myFS = new System.IO.FileStream(myDocFolder + "AppLog.xml", System.IO.FileMode.Open, System.IO.FileAccess.Read);

        System.Xml.XmlReader myXmlReader = System.Xml.XmlReader.Create(myFS, myXmlSettings);


        ds.ReadXml(myXmlReader);
        DataTable dtKeys = ds.Tables["field"];
        DataTable dtLogs = ds.Tables["event"];
        myDataManager.RemoveCache("dtLogView");
        myDataManager.SetCache("dtLogView", dtLogs);

        //
        myFS.Dispose();

        myFS.Close();
    }

    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey, int columnOffset)
    {
        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey, columnOffset);

        if (sortColumnIndex != -1)
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }
    }
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {
        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        Image sortImage = new Image();
        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
        ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

        // Add the image to the appropriate header cell.
        imageLink.CommandName = "Sort";
        imageLink.CommandArgument = (string)Session[sessionSortColKey];
        imageLink.Controls.Add(sortImage);
        headerRow.Cells[columnIndex].Controls.Add(imageLink);

        imageLink.Dispose();
        sortImage.Dispose();
    }
    private int GetSortColumnIndex(GridView gv, string sessionSortColKey, int columnOffset)
    {

        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + columnOffset;
            }
        }

        return -1;
    }
    private void Grid_Sorting(GridView gv, GridViewSortEventArgs e, string sortColumn, string sortDirection)
    {
        if ((string)Session[sortColumn] != e.SortExpression)
        {
            Session[sortDirection] = " ASC";
        }
        else
        {
            if ((string)Session[sortDirection] == " ASC")
            {
                Session[sortDirection] = " DESC";
            }
            else
            {
                Session[sortDirection] = " ASC";
            }
        }

        gv.PageIndex = 0;
        Session[sortColumn] = e.SortExpression;
    }
    /// <summary>
    /// Routine to build the string corresponding to the visible columns of the grid
    /// </summary>
    /// <param name="checkBoxList"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    private string BuildColumnVisibilityProperty(CheckBoxList checkBoxList, GridView gridView)
    {
        string outputString = "";
        char c;

        for (int x = 0; x < checkBoxList.Items.Count; x++)
        {
            if (checkBoxList.Items[x].Selected)
            {
                c = '1';
                gridView.Columns[x].Visible = true;
            }
            else
            {
                c = '0';
                gridView.Columns[x].Visible = false;
            }
            outputString += c;
        }

        return outputString;
    }

    /// <summary>
    /// Assign the column visibility based on profile settings
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="profileName"></param>
    private void InitializeColumnVisibility(GridView gridView, string profileName)
    {
        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            try
            {
                bool isVisible = false;

                if ((Profile.PropertyValues[profileName] == null &&
                    System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString().Substring(x, 1) == "1")
                || (Profile.PropertyValues[profileName] != null &&
                    Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(x, 1) == "1"))
                {
                    isVisible = true;
                }
                gridView.Columns[x].Visible = isVisible;
            }
            catch (System.ArgumentOutOfRangeException aoor)
            {
                string message = aoor.Message;

            }
        }

    }


    void csCustomLogView_OnResetClicked()
    {
        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvLogViewer_Columns_Visible"].DefaultValue.ToString();
        Profile.gvLogViewer_Columns_Visible = defaultValue;
        InitializeColumnVisibility(gvLogView, "gvLogViewer_Columns_Visible");

        AssignDataSource(false, gvLogView);
    }
    protected void btnFind_Click(object sender, EventArgs e)
    {
        XML_Reader();
        AssignDataSource(false, gvLogView);
    }

    protected void ddlLogViewRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        int pageSize;
        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        }

        this.gvLogView.PageSize = pageSize;
        this.AssignDataSource(false, gvLogView);
    }
    #endregion

}