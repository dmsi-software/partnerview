<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiplierSelector.ascx.cs"
    Inherits="UserControls_MultiplierSelector" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<style type="text/css">
    .margintop {
        margin-top: 6px;
        padding: 3px;
        padding-right: 0px;
    }

    .center {
        margin-left: auto;
        margin-right: auto;
    }

    .linspace13em {
        line-height: 1.3em !important;
    }

    .lineheight13em {
        line-height: 1.3em !important;
    }
</style>
<table cellpadding="0" cellspacing="2" width="243px">
    <tr>
        <td colspan="2" valign="top" style="width: 100%; text-align: left">
            <asp:Panel ID="pnlScroller" runat="server" ScrollBars="None">
                <ul class="retail">
                    <li>
                        <asp:Label ID="Label2" runat="server"
                            Text="&lt;b&gt;Apply new retail prices to quote&lt;/b&gt;"></asp:Label><br />
                        <asp:Label CssClass="lineheight13em"  ID="Label5" runat="server" Text="Calculate new retail prices for all items&lt;br/&gt;on this quote only."></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="lblMultiplier" runat="server" Text="Multiplier" CssClass="lblpopupright"></asp:Label>
                        <ig:WebNumericEditor ID="numericMarkupFactor" runat="server" DataMode="Decimal" MaxDecimalPlaces="4"
                            MinValue="1.01" CssClass="margintop" Height="30px" Nullable="False" HideEnterKey="True" MaxLength="9" TabIndex="1" NullText="1.01" NullValue="1.01" ClientEvents-ValueChanged="numericMarkupFactor_ValueChanged">
                        </ig:WebNumericEditor>
                    </li>
                    <li>
                        <asp:Label runat="server" ID="invalidmultiplier" ForeColor="Red" Text="" />
                    </li>
                    <li style="line-height: 1.3em !important">
                        <asp:Label CssClass="lineheight13em" runat="server" ID="lblExample" Text="<i>Example: To give a 25% discount,<br/>enter .75 as a multiplier.<br>$100 * (.75) = $75</i>" />
                    </li>
                </ul>
                <asp:HiddenField runat="server" ID="preferredMarkupValue" />
            </asp:Panel>
        </td>
    </tr>
</table>
<div class="modal-footer" style="width: 100%; padding-right: 0px !important;">
    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="btn btnpopup-primary btnpopupleft" OnClick="btnOK_Click" TabIndex="3" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btnpopup-default  btnpopupleft" OnClick="btnCancel_Click" TabIndex="2" />
</div>
<div style="clear: both;"></div>
