using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_Home : System.Web.UI.UserControl
{
   public delegate void DelMethodWithoutParam();

  
       
    
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsPartnerVuDataSet ds = (dsPartnerVuDataSet)dm.GetCache("dsPV");
        string cityStateZip = ds.bPV_CoInfo.Rows[0]["cust_div_city"].ToString() + ", "
                           + ds.bPV_CoInfo.Rows[0]["cust_div_state"].ToString() + " "
                           + ds.bPV_CoInfo.Rows[0]["cust_div_zip"].ToString();

        if (cityStateZip.Trim().Equals(","))
           cityStateZip = "";

        this.CompanyDescLabel.Text = ds.bPV_CoInfo.Rows[0]["cust_div_remit_name"].ToString();
        this.CompanyDescLabel.Text += "<br>" + ds.bPV_CoInfo.Rows[0]["cust_div_remit_desc"].ToString();
        this.CompanyAddressLabel.Text = ds.bPV_CoInfo.Rows[0]["cust_div_address1"].ToString();
        this.CompanyAddressLabel.Text += "<br>" + ds.bPV_CoInfo.Rows[0]["cust_div_address2"].ToString();
        this.CompanyAddressLabel.Text += "<br>" + cityStateZip;
        this.CompanyAddressLabel.Text += "<br>" + ds.bPV_CoInfo.Rows[0]["cust_div_country"].ToString();
        this.CompanyAddressLabel.Text += "<br>" + ds.bPV_CoInfo.Rows[0]["cust_div_phone"].ToString();


        DataView dv = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);

        foreach (DataRowView row in dv)
        {
            int rows = DisplayDynamicContents((string)row["Category_Name"]);

            if (rows > 0)
            {
                Label newLine = new Label();
                newLine.Text = "<br />";
                this.PlaceHolder1.Controls.Add(newLine);
            }
        }
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);

    }
    
    private int DisplayDynamicContents(string headerText)
    {
        int rows = 0;
        bool first = true;
        SqlDataSource2.SelectParameters["Category_Name"].DefaultValue = headerText;
        DataView dv = (DataView)SqlDataSource2.Select(DataSourceSelectArguments.Empty);
        rows = dv.Count;

        foreach (DataRowView row in dv)
        {
            UserControls_SimplePanel messagePanel = new UserControls_SimplePanel();

            messagePanel.HeaderText = headerText;
            messagePanel.BodyText = (string)row["Message_Text"];

            if (first)
                first = false;
            else
                messagePanel.HideHeader = true;

            this.PlaceHolder1.Controls.Add(messagePanel);
        }

        return rows;
    }
    protected void btnreload_Click(object sender, EventArgs e)
    {

    }
}
