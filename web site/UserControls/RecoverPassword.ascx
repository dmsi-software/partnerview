<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecoverPassword.ascx.cs" Inherits="UserControls_RecoverPassword" %>
<asp:Label ID="lblForgotPassword" runat="server" Text="Forgot your password?" Width="100%" CssClass="CaptionText"></asp:Label>
<br />
<asp:Label ID="lblError" runat="server" CssClass="ErrorTextRed"></asp:Label>
<table width = "100%">
<tr >
<td width="50%" align="center">
    <asp:Label ID="Label1" runat="server" Text="To reset your password, enter your user name and submit to receive email confirmation." CssClass="CaptionText"></asp:Label>
</td>
</tr>
    <tr>
        <td align="center" colspan="1" valign="middle">
            <asp:Label ID="Label2" runat="server" CssClass="FieldLabel" Text="User Name:"></asp:Label>
    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></td>
    </tr>
<tr >
<td colspan ="1" align="center">
    <asp:Button ID="btnResetPassword" runat="server" Text="Submit Request" OnClick="btnResetPassword_Click" />
    <asp:Button ID="btnCancelRequest" runat="server" Text="Cancel Request" 
        onclick="btnCancelRequest_Click"/>
</td>
</tr>
</table>
