﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageView.ascx.cs" Inherits="UserControls_ImageViewl" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.ListControls" TagPrefix="ig" %>
<style type="text/css">
    *:focus
    {
        outline: none;
    }
</style>

<table border="0" cellpadding="0" cellspacing="0" style="margin-left: 0px; margin-right: 0px;
    margin-top: 0px; margin-bottom: 0px; outline: none" width="100%">
    <tr>
        <td align="center" style="padding-top: 12px; padding-bottom: 12px">
            <asp:Image ID="imgLarge" runat="server" Height="512px" />
        </td>
    </tr>
    <tr runat="server" id="tablerowImageViewer" style="outline: none">
        <td align="center" style="padding-top: 6px; padding-bottom: 30px; outline-style: none;
            outline: transparent, none, 0">
            <ig:WebImageViewer ID="WebImageViewer1" runat="server" Width="540px" Height="128px"
                EnableInitialFadeAnimation="False" EnableDragScrolling="False" WrapAround="False"
                OnItemBound="WebImageViewer1_ItemBound">
                <ScrollAnimations>
                    <Continuous MaxSpeed="15" />
                </ScrollAnimations>
               
            </ig:WebImageViewer>
        </td>
    </tr>
</table>
