﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SettingsModal.ascx.cs"
    Inherits="UserControls_SettingsModal" %>
<%@ Register Src="PasswordMaintSettings.ascx" TagName="PasswordMaintSettings" TagPrefix="uc1" %>
<table cellpadding="0" cellspacing="2" width="700px" style="height: 464px">
    <tr>
        <td colspan="2" valign="top" style="width: 700px">
            <asp:Panel ID="pnlScroller" runat="server" CssClass="BorderedContainer" Height="441px"
                ScrollBars="Auto" Width="100%">
                <uc1:PasswordMaintSettings runat="server" ID="PasswordMaintenanceSettings" />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td align="right" valign="top">
            <asp:Button ID="btnClose" runat="server" Text="Close" Width="75px" 
                Height="23px" onclick="btnClose_Click"/>
        </td>
    </tr>
</table>
