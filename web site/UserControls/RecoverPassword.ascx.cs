using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_RecoverPassword : System.Web.UI.UserControl
{
    protected DataManager myDataManager;
    protected ExceptionManager myExceptionManager;
    protected EventLog myEventLog;
    protected SecurityManager mySecurityManager;
    protected Boolean errorMessage_Boolean;
    protected string errorMessage_Text;
    protected string myUserName;
    protected string validationErrorText;
    protected void Page_Load(object sender, EventArgs e)
    {
        myDataManager = new DataManager();
        myExceptionManager = new ExceptionManager();
        myEventLog = new EventLog();
        mySecurityManager = new SecurityManager();
        string PasswordRequirements = ConfigurationManager.AppSettings["PW_RequirementsText"].ToString();
        myDataManager = new DataManager();
        myExceptionManager = new ExceptionManager();
        myEventLog = new EventLog();
        mySecurityManager = new SecurityManager();
        if (!IsPostBack)
        {
            lblError.Text = "";
        }
    }
    protected void btnResetPassword_Click(object sender, EventArgs e)
    {
        string UserName = txtUserName.Text.ToString();
        Session["UserName"] = UserName;

        int ReturnCode;
        string MessageText;

        mySecurityManager.ResetPassword(UserName, ref errorMessage_Boolean, ref errorMessage_Text, out ReturnCode, out MessageText);

        switch (errorMessage_Boolean)
        {
            case (true):
                Response.Redirect("./LogOn.aspx");
                break;
            case (false):
                this.lblError.Text = errorMessage_Text == null ? "Unknown error has occured." : errorMessage_Text.ToString();
                break;
        }
    }
    protected void btnCancelRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("./LogOn.aspx");
    }
}
