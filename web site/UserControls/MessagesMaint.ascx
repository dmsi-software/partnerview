﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessagesMaint.ascx.cs"
    Inherits="UserControls_MessagesMaint" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.NavigationControls" TagPrefix="ig" %>

<div class="col-lg-12 messages" id="MessageListDivheaders" runat="server" style="padding-top: 20px; min-width: 800px;">
    <div class="col-sm-6 messages_block" id="Div2" runat="server">
        <h4>
            <asp:Label ID="lblCategories" runat="server" Text="Categories"></asp:Label>

        </h4>
    </div>
    <div class="col-sm-6 messages_block" style="padding-right: 0px" id="Div1" runat="server">
        <h4>
            <asp:Label ID="lblMessages" runat="server" Text="Special-Messages"></asp:Label></h4>
    </div>
</div>
<div class="col-lg-12 messages" id="MessageListDiv" runat="server" style="padding-top: 19px; min-width: 800px;">
    <div class="col-sm-6 messages_block" id="CategoryDiv" runat="server">

        <hr>
        <div>
            <asp:TextBox ID="txtCategory" runat="server" CssClass="messagetextbox"></asp:TextBox>
            <asp:Button ID="btnAddCategory" runat="server" Text="Add" OnClick="btnAddCategory_Click"
                CssClass="btn btn-primary msg-add-category" /><br />
            <asp:Label ID="lblCategoryError" runat="server" CssClass="ErrorTextRed"></asp:Label>
        </div>
        <div style="padding-right: 0px; overflow-x: visible; padding-bottom: 10px; margin-top: 20px;">

            <asp:GridView ID="gvCategories" runat="server" AutoGenerateColumns="False" DataKeyNames="Category_Name"
                OnSelectedIndexChanged="gvCategories_SelectedIndexChanged"
                OnRowDataBound="gvCategories_RowDataBound" PageSize="32000" OnRowCancelingEdit="gvCategories_RowCancelingEdit"
                OnRowDeleting="gvCategories_RowDeleting" OnRowEditing="gvCategories_RowEditing"
                OnRowUpdating="gvCategories_RowUpdating" Width="100%" CssClass="grid-tabler">
                <Columns>

                    <asp:CommandField ShowEditButton="true" ShowDeleteButton="true" HeaderText="Actions" DeleteImageUrl="~/Images/delete-icon.png" EditImageUrl="~/Images/edit-icon.png" ControlStyle-Width="16px" ButtonType="Image" CancelText="Cancel" UpdateText="Update" CancelImageUrl="~/Images/Grid_Cancel.png" UpdateImageUrl="~/Images/Grid_UpdateSave.png" />

                    <asp:CommandField ShowSelectButton="True" ButtonType="Link" ItemStyle-CssClass="selectlinkUnderline" />

                    <asp:TemplateField HeaderText="Active">
                        <ItemTemplate>
                            <asp:CheckBox ID="ActiveCategory" runat="server" AutoPostBack="True" OnCheckedChanged="ActiveCategory_CheckedChanged" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>

                    <asp:BoundField DataField="Category_Name" HeaderText="Category Name" SortExpression="Category_Name">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle Wrap="False" CssClass="wrapstylecatogeries" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
        <asp:Button ID="btnUpCategory" runat="server" Enabled="False" OnClick="btnUpCategory_Click"
            Text="Up" CssClass="btn btn-msgdefault" />
        <asp:Button ID="btnDownCategory" runat="server" Enabled="False" OnClick="btnDownCategory_Click"
            Text="Down" CssClass="btn btn-msgdefault" />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT Sequence_Number, Category_Name, Active FROM Category ORDER BY Sequence_Number"
            InsertCommand="INSERT INTO Category(Sequence_Number, Category_Name) VALUES (@Sequence_Number, @Category_Name)"
            UpdateCommand="UPDATE Category SET Sequence_Number = @Sequence_Number, Category_Name = @New_Category_Name, Active = @Active WHERE (Category_Name = @Old_Category_Name)"
            DeleteCommand="DELETE FROM Category WHERE (Category_Name = @Category_Name)">
            <DeleteParameters>
                <asp:Parameter Name="Category_Name" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter DefaultValue="1" Name="Sequence_Number" />
                <asp:Parameter DefaultValue="Home Messages" Name="New_Category_Name" />
                <asp:Parameter DefaultValue="" Name="Old_Category_Name" />
                <asp:Parameter DefaultValue="False" Name="Active" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter DefaultValue="1" Name="Sequence_Number" />
                <asp:Parameter DefaultValue="Home Messages" Name="Category_Name" />
            </InsertParameters>
        </asp:SqlDataSource>
    </div>
    <div class="col-sm-6 messages_block" style="padding-right: 0px" id="MessageDiv" runat="server">

        <hr>
        <div>
            <asp:Label ID="lblMessageError" CssClass="ErrorTextRed" runat="server" Text=""></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" CssClass="messagetextbox hidden_box"></asp:TextBox>
            <asp:Button ID="btnAddMessage" OnClick="btnAddMessage_Click" CssClass="btn btn-primary messageaddbutton" runat="server" Text="Add" />


        </div>


        <div style="padding-right: 0px; overflow-x: visible; padding-bottom: 10px; margin-top: 19px;">

            <asp:GridView ID="gvMessages" runat="server" AutoGenerateColumns="False" DataKeyNames="Category_Name,Sequence_Number"
                Width="100%" OnRowDataBound="gvMessages_RowDataBound"
                PageSize="32000" OnSelectedIndexChanged="gvMessages_SelectedIndexChanged" OnRowEditing="gvMessages_RowEditing"
                OnRowDeleting="gvMessages_RowDeleting" CssClass="grid-tabler messagegrid">
                <Columns>
                    <asp:CommandField HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px" ShowEditButton="True" HeaderText="Actions" ControlStyle-Width="16px" ControlStyle-Height="16px" ShowDeleteButton="true" DeleteImageUrl="~/Images/delete-icon.png" ButtonType="Image" EditImageUrl="~/Images/edit-icon.png" CancelImageUrl="~/Images/delete-icon.png" UpdateImageUrl="~/Images/update-icon.png" />

                    <asp:CommandField HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px" ShowSelectButton="true" ButtonType="Link" ItemStyle-CssClass="selectlinkUnderline" />
                    <asp:TemplateField HeaderText="Active" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                        <ItemTemplate>
                            <asp:CheckBox ID="ActiveMessage" runat="server" AutoPostBack="True" OnCheckedChanged="ActiveMessage_CheckedChanged" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />

                    </asp:TemplateField>
                    <asp:BoundField HeaderStyle-Width="150px" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px" DataField="Message_Text" HeaderText="Message" SortExpression="Message_Text">
                        <HeaderStyle Wrap="False" Width="150px" />
                        <ItemStyle Wrap="False" Width="150px" />
                    </asp:BoundField>

                    <asp:TemplateField>
                        <ItemTemplate></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <asp:Button ID="btnUpMessage" runat="server" Enabled="False" OnClick="btnUpMessage_Click"
            Text="Up" CssClass="btn btn-msgdefault" />
        <asp:Button ID="btnDownMessage" runat="server" Enabled="False" OnClick="btnDownMessage_Click"
            Text="Down" CssClass="btn btn-msgdefault" />
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT Category_Name, Sequence_Number, Message_Text, Active FROM Message WHERE (Category_Name = @Category_Name) ORDER BY Category_Name, Sequence_Number"
            UpdateCommand="UPDATE Message SET Message_Text = @Message_Text, Active = @Active WHERE (Category_Name = @Category_Name) AND (Sequence_Number = @Sequence_Number)"
            DeleteCommand="DELETE FROM Message WHERE (Category_Name = @Category_Name) AND (Sequence_Number = @Sequence_Number)"
            InsertCommand="INSERT INTO Message(Category_Name, Sequence_Number, Message_Text) VALUES (@Category_Name, @Sequence_Number, @Message_Text)">
            <SelectParameters>
                <asp:Parameter Name="Category_Name" Type="String" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="Category_Name" />
                <asp:Parameter Name="Sequence_Number" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Message_Text" />
                <asp:Parameter Name="Category_Name" />
                <asp:Parameter Name="Sequence_Number" />
                <asp:Parameter DefaultValue="False" Name="Active" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter DefaultValue="" Name="Category_Name" />
                <asp:Parameter Name="Sequence_Number" />
                <asp:Parameter Name="Message_Text" />
            </InsertParameters>
        </asp:SqlDataSource>

    </div>
</div>

<div id="HtmlDiv" class="SimplePanelDIV" align="left" visible="false" runat="server"
    style="width: 750px; float: left">
    <asp:Label ID="lblHtmlViewOfMessage" Text="" runat="server"></asp:Label>
</div>



