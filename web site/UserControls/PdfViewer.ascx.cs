﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_PdfViewer : System.Web.UI.UserControl
{

    private string _docStorageType = "";
    private string _currentGuid = "";
    private string _branch = "";
    private string _tranID = "";
    private string _shipmentNum = "";
    private string _tranType = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            _branch = Request.QueryString["ProfName"];
            _tranID = Request.QueryString["TranID"];
            _shipmentNum = Request.QueryString["ShipmentNum"];
            _tranType = Request.QueryString["TranType"];
            _docStorageType = Request.QueryString["DocStorageReportName"];
            _currentGuid = Guid.NewGuid().ToString();

            // Store the unique indentifier for this page
            lblContext.Text = _docStorageType + "," + _currentGuid;

            // Clear previously fetched document
            Session.Remove(_docStorageType + _currentGuid + "Url");
            Session.Remove(_docStorageType + _currentGuid);
            Session.Remove("NO" + _docStorageType + _currentGuid);
            Session.Remove("ERROR" + _docStorageType + _currentGuid);
            Session.Remove(_docStorageType + _currentGuid + "Port");

            Session[_docStorageType + _currentGuid + "StartDate"] = DateTime.Now;
            Session[_docStorageType + _currentGuid + "CurrentGuid"] = _currentGuid;
            Session[_docStorageType + _currentGuid + "DocStorageReportName"] = _docStorageType;

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Value = "0";

            switch(_tranType)
            {
                case "OrderAck":

                    aInput.Action = "Orders - PDF Click";
                    aInput.Label  = "Order Acknowledgement";
                    break;

                case "QUote":

                    aInput.Action = "Quotes - PDF Click";
                    aInput.Label  = "Quotation";
                    break;

                case "Invoice":

                    aInput.Action = "Invoices - PDF Click";
                    aInput.Label  = "Invoice";
                                        
                    break;                   

                case "CreditMemo":
                                        
                    aInput.Action = "CreditMemos - PDF Click";
                    aInput.Label  = "Credit Memo";

                    if (_docStorageType == "Credit Memo Invoice")
                    {
                        aInput.Action = "Invoices - PDF Click";
                        aInput.Label = "Credit Memo Invoice";
                    }


                    break;

                case "Billing":

                    aInput.Action = "Billing - PDF Click";
                    aInput.Label  = "Invoice";
                    break;

            }
           
            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

            /*
             * Document generation and client proces
             */
            // Get a free port
            int min = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TcpMinPort"].ToString());
            int max = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TcpMaxPort"].ToString());
            int port = 0;

            for (int i = min; i <= max; i++)
            {
                if (Dmsi.Agility.Communications.Port.PortMonitor.IsServerSocketAvailable(System.Configuration.ConfigurationManager.AppSettings["TcpIp"].ToString(), i))
                {
                    port = i;
                    break;
                }
            }

            if (port != 0)
            {
                // Start listener
                Thread waitThread = new Thread(new ParameterizedThreadStart(WaitForDocument));
                waitThread.Priority = ThreadPriority.Lowest;
                waitThread.Start(port);

                string searchCriteria = BuildDocumentSearchCriteria(port);
              
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.Document doc = new Dmsi.Agility.Data.Document(searchCriteria, out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    lblMessage.Text = MessageText;
                    imgProgress.Visible = false;
                    Timer1.Enabled = false;
                    return;
                }

                if (doc.DocumentUrl.Length > 0 && doc.ReferencedRow["docSource"].ToString() != "")
                    Session[_docStorageType + _currentGuid + "Url"] = doc.DocumentUrl;
                else if (doc.ReferencedRow != null && doc.ReferencedRow["docSource"].ToString() == "ERROR")
                    Session["ERROR" + _docStorageType + _currentGuid] = "Unable to retrieve the form.<BR />" + doc.ReferencedRow["docFilename"].ToString().Replace("type .", "type.") + "<br />&nbsp;<br />If the problem persists, please contact the administrator.</P>";

                doc.DisposeAppServerObjects();
                             

                Timer1.Enabled = true;
            }
            else
                Session["NO" + _docStorageType + _currentGuid] = true;
        }
        else
        {
            // Retrieve some pertinent information about this page
            string[] parts = lblContext.Text.Split(new char[] { ',' });
            _docStorageType = parts[0];
            _currentGuid = parts[1];
        }
    }

    private void WaitForDocument(object port)
    {
        /*
          * Socket Server Process 
          */
        byte[] data = null;
        string ip = System.Configuration.ConfigurationManager.AppSettings["TcpIp"].ToString();
        DateTime start = (DateTime)Session[_docStorageType + _currentGuid + "StartDate"];

        // blocks and waits for response
        data = Dmsi.Agility.Communications.Port.PortMonitor.Listen(ip, (int)port, "");

        if (data != null)
            Session[_docStorageType + _currentGuid] = data;
        else
            Session["NO" + _docStorageType + _currentGuid] = true;

        //if (Session["SO_223494"] != null)
        //{
        //    Session[_docStorageType + _currentGuid] = (byte[])Session["SO_223494"];
        //    Session["SO_223494"] = null;
        //}
    }

    private string BuildDocumentSearchCriteria(int portNumber)
    {
        string branch = "Branch=" + _branch;

        //following hack absolutely has to occur to handle difference between Billing tab and all other PDF methods.
        string referenceNumber;
        string intermediateTranType = _tranType;

        if (_tranType == "Billing")
        {
            referenceNumber = "TranID=" + _tranID;
            intermediateTranType = "Invoice";
        }
        else
            referenceNumber = "TranID=" + _tranID + "-" + _shipmentNum;

        string guid = "GUID=" + _currentGuid;

        string searchCriteria = "TranType=" + intermediateTranType; // SalesOrder, Invoice, CreditMemo - from which tab
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + referenceNumber;
        searchCriteria += "\x0003" + guid;
        searchCriteria += "\x0003DocStorageType=" + _docStorageType;
        searchCriteria += "\x0003TcpIP=" + System.Configuration.ConfigurationManager.AppSettings["TcpIp"].ToString();
        searchCriteria += "\x0003TcpPort=" + portNumber.ToString();
        searchCriteria += "\x0003Monitor=Port";
        searchCriteria += "\x0003OutputType=PDF";

        Console.WriteLine(searchCriteria);
        return searchCriteria;
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        DateTime start = (DateTime)Session[_docStorageType + _currentGuid + "StartDate"];

        if ((DateTime.Now - start).Minutes >= 2) // wait for 2 minutes
            Session["NO" + _docStorageType + _currentGuid] = true;

        if (Session[_docStorageType + _currentGuid + "Url"] != null)
        {
            Timer1.Enabled = false;
            Server.Transfer((string)Session[_docStorageType + _currentGuid + "Url"]);
        }
        else if (Session[_docStorageType + _currentGuid] != null)
        {
            Timer1.Enabled = false;
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "filename=" + _currentGuid + ".pdf");
            byte[] data = (byte[])Session[_docStorageType + _currentGuid];
            Session.Remove(_docStorageType + _currentGuid);
            Response.OutputStream.Write(data, 0, data.Length);
            Response.End();
        }
        else if (Session["NO" + _docStorageType + _currentGuid] != null)
        {
            Timer1.Enabled = false;
            lblMessage.Text = "Unable to retrieve the form.<BR />Please try again.<br />&nbsp;<br />If the problem persists, please contact the administrator.</P>";
            imgProgress.Visible = false;
        }
        else if (Session["ERROR" + _docStorageType + _currentGuid] != null)
        {
            Timer1.Enabled = false;
            lblMessage.Text = (string)Session["ERROR" + _docStorageType + _currentGuid];
            imgProgress.Visible = false;
        }
    }
}
