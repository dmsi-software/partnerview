using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Data;

public partial class UserControls_SalesOrders : System.Web.UI.UserControl
{
    public GridView GvSalesOrderHeaders
    {
        get { return this.gvSalesOrderHeaders; }
    }

    public GridView GvSalesOrderByItem
    {
        get { return this.gvByItem; }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);
        if ((string)Session["ddlSalesOrderShowResultsBy"] == "Item")
            csCustomHeader.GridView = this.gvByItem;
        else
            csCustomHeader.GridView = this.gvSalesOrderHeaders;

        if (((string)Session["DisplayCanceledSO"]).ToLower().Trim() != "yes")
            ddlStatus.Items.Remove(ddlStatus.Items.FindByText("Canceled"));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.csCustomHeader.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomHeader_OnOKClicked);
        this.csCustomHeader.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomHeader_OnResetClicked);
        this.csCustomDetail.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomDetail_OnOKClicked);
        this.csCustomDetail.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomDetail_OnResetClicked);

        if (!this.IsPostBack)
        {
            ddlSearchStyle.SelectedIndex = 2;

            //load data fields with today and 30 days prior...
            this.txtToDate.Text = DateTime.Now.ToDMSiDateFormat();
            this.txtFromDate.Text = (DateTime.Today.AddMonths(-1)).ToDMSiDateFormat();

            this.ddlSalesOrderHeaderRows.SelectedIndex = Profile.ddlSalesOrderHeaderRows_SelectedIndex;

            int pageSize = Convert.ToInt32(ddlSalesOrderHeaderRows.SelectedValue);

            this.gvSalesOrderHeaders.PageSize = pageSize;
            this.gvByItem.PageSize = pageSize;

            InitializeColumnVisibility(gvSalesOrderHeaders, "gvSalesOrderHeaders_Columns_Visible", false);
            InitializeColumnVisibility(gvByItem, "gvSalesOrderItems_Columns_Visible", false);

            ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
            if (pc.SOddlTranID_SelectedIndex > ddlTranID.Items.Count)
                ddlTranID.SelectedIndex = 0;
            else
                ddlTranID.SelectedIndex = pc.SOddlTranID_SelectedIndex;

            SetDdlSearchTypeValues(ddlTranID.Text);

            if (pc.SOddlSearchStyle_SelectedIndex > ddlSearchStyle.Items.Count)
                ddlSearchStyle.SelectedIndex = 0;
            else
                ddlSearchStyle.SelectedIndex = pc.SOddlSearchStyle_SelectedIndex;
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // this is needed for the columns to show correctly
        if (!this.IsPostBack || dm.GetCache("NoSalesOrdersAvailable") != null)
        {
            if (!this.IsPostBack)
            {
                dm.SetCache("gvSalesOrderHeadersSortColumn", "so_id");
                dm.SetCache("gvSalesOrderHeadersSortDirection", " DESC");
                dm.SetCache("gvSalesOrderDetailSortColumn", "ITEM");
                dm.SetCache("gvSalesOrderDetailSortDirection", " ASC");
                dm.SetCache("gvSalesOrderByItemSortColumn", "ITEM");
                dm.SetCache("gvSalesOrderByItemSortDirection", " ASC");
            }

            if ((string)dm.GetCache("ddlSalesOrderShowResultsBy") == "Item")
                this.AssignHeaderDataSource(true, "ttso_byitem");
            else
                this.AssignHeaderDataSource(true, "ttso_header");
        }
        else if (dm.GetCache("DocViewerSOID") != null)
        {
            if (Session["Salesorder_SelectedRow"] != null)
            {
                int Rowid = 0;

                if (!(gvSalesOrderHeaders.Rows.Count - 1 < Convert.ToInt32(Session["Salesorder_SelectedRow"])))
                    Rowid = Convert.ToInt32(Session["Salesorder_SelectedRow"]);

                RebindDocviewer(Rowid);
            }
        }

        dm = null;
    }

    #region Private Methods

    /// <summary>
    /// Routine to build the string corresponding to the visible columns of the grid
    /// </summary>
    /// <param name="checkBoxList"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    private string BuildColumnVisibilityProperty(CheckBoxList checkBoxList, GridView gridView)
    {
        string outputString = "";
        char c;

        for (int x = 0; x < checkBoxList.Items.Count; x++)
        {
            if (checkBoxList.Items[x].Selected)
            {
                c = '1';
                gridView.Columns[x].Visible = true;
            }
            else
            {
                c = '0';
                gridView.Columns[x].Visible = false;
            }

            outputString += c;
        }

        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Net price")
        {
            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_SalesOrders1_gvSalesOrderHeaders")
            {
                string oneString = outputString.Substring(0, 19);
                string twoString = outputString.Substring(20);

                outputString = oneString + "0" + twoString;

                gridView.Columns[19].Visible = false;
            }

            if (gridView.ID == "gvSalesOrderDetailInner")
            {
                string oneString = outputString.Substring(0, 12);

                outputString = oneString + "000";

                gridView.Columns[12].Visible = false;
                gridView.Columns[13].Visible = false;
                gridView.Columns[14].Visible = false;
            }

            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_SalesOrders1_gvByItem")
            {
                string oneString = outputString.Substring(0, 12);
                string twoString = outputString.Substring(15);

                outputString = oneString + "000" + twoString;

                gridView.Columns[12].Visible = false;
                gridView.Columns[13].Visible = false;
                gridView.Columns[14].Visible = false;
            }
        }
        else
        {
            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_SalesOrders1_gvSalesOrderHeaders")
            {
                string oneString = outputString.Substring(0, 15);
                string twoString = outputString.Substring(19);

                outputString = oneString + "0000" + twoString;

                gridView.Columns[16].Visible = false;
                gridView.Columns[17].Visible = false;
                gridView.Columns[18].Visible = false;
                gridView.Columns[19].Visible = false;
            }

            if (gridView.ID == "gvSalesOrderDetailInner")
            {
                string oneString = outputString.Substring(0, 9);
                string twoString = outputString.Substring(12);

                outputString = oneString + "000" + twoString;

                gridView.Columns[9].Visible = false;
                gridView.Columns[10].Visible = false;
                gridView.Columns[11].Visible = false;
            }

            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_SalesOrders1_gvByItem")
            {
                string oneString = outputString.Substring(0, 9);
                string twoString = outputString.Substring(12);

                outputString = oneString + "000" + twoString;

                gridView.Columns[9].Visible = false;
                gridView.Columns[10].Visible = false;
                gridView.Columns[11].Visible = false;
            }
        }

        return outputString;
    }

    /// <summary>
    /// Assign the column visibility based on profile settings
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="profileName"></param>
    public void InitializeColumnVisibility(GridView gridView, string profileName, bool calledFromUserPrefChange)
    {
        string saledetail = Profile.gvSalesOrderDetail_Columns_Visible;
        string saledetailsubstring = saledetail.Substring(0, 9);

        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            Type t = gridView.Columns[x].GetType();
            if ((t.Name != "ButtonField") && (t.Name != "TemplateField"))
            {
                try
                {
                    bool isVisible = false;

                    if ((Profile.PropertyValues[profileName] == null && System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString().Substring(x, 1) == "1") || (Profile.PropertyValues[profileName] != null &&
                        Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(x, 1) == "1"))
                    {
                        isVisible = true;
                    }

                    gridView.Columns[x].Visible = isVisible;
                }
                catch (System.ArgumentOutOfRangeException aoor)
                {
                    string message = aoor.Message;
                    Profile.PropertyValues[profileName].PropertyValue = System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString();
                }
            }

            if ((gridView.ID == "gvSalesOrderHeaders"))
            {
                if (x == 0)
                    gridView.Columns[x].Visible = true;
            }
        }

        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Net price")
        {
            if (profileName == "gvSalesOrderHeaders_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Retail Total")
                        col.Visible = false;

                    if ((col.HeaderText == "Sub Total" || col.HeaderText == "Total Taxes" || col.HeaderText == "Total Charges" || col.HeaderText == "Total") && calledFromUserPrefChange)
                    {
                        col.Visible = true;
                    }
                }
            }

            if (profileName == "gvSalesOrderDetail_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                        col.Visible = false;

                    if ((col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount") && calledFromUserPrefChange)
                    {
                        col.Visible = true;
                        Profile.gvSalesOrderDetail_Columns_Visible = saledetailsubstring + "111000";
                    }
                }
            }

            if (profileName == "gvSalesOrderItems_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                        col.Visible = false;

                    if ((col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount"))
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;
                        }
                        else
                        {
                            if (col.HeaderText == "Price")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(6, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Price UOM")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(7, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Extended Amount")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(8, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (profileName == "gvSalesOrderHeaders_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Sub Total" || col.HeaderText == "Total Taxes" || col.HeaderText == "Total Charges" || col.HeaderText == "Total")
                        col.Visible = false;

                    if (col.HeaderText == "Retail Total")
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;
                        }
                        else
                        {
                            if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(20, 1) == "0")
                                col.Visible = false;
                            else
                                col.Visible = true;
                        }
                    }
                }
            }

            if (profileName == "gvSalesOrderDetail_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount")
                        col.Visible = false;

                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;
                            Profile.gvSalesOrderDetail_Columns_Visible = saledetailsubstring + "000111";
                        }
                        else
                        {
                            if (col.HeaderText == "Retail Price")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(12, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Price UOM")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(13, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Extended Amount")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(14, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }
                        }
                    }
                }
            }

            if (profileName == "gvSalesOrderItems_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount")
                        col.Visible = false;

                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;
                        }
                        else
                        {
                            if (col.HeaderText == "Retail Price")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(12, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Price UOM")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(13, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Extended Amount")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(14, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }
                        }
                    }
                }
            }
        }
    }

    // This is a helper method used to determine the index of the
    // column being sorted. If no column is being sorted, -1 is returned.
    private int GetSortColumnIndex(GridView gv, string sessionSortColKey, int offSet)
    {
        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + offSet;
            }
        }

        return -1;
    }

    // This is a helper method used to add a sort direction
    // image to the header of the column being sorted.
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {
        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        Image sortImage = new Image();

        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
        ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

        // Add the image to the appropriate header cell.
        imageLink.CommandName = "Sort";
        imageLink.CommandArgument = (string)Session[sessionSortColKey];
        imageLink.Controls.Add(sortImage);
        headerRow.Cells[columnIndex].Controls.Add(imageLink);

        imageLink.Dispose();
        sortImage.Dispose();
    }

    /// <summary>
    /// Insert the sort image to the column header
    /// </summary>
    /// <param name="gv"></param>
    /// <param name="row"></param>
    /// <param name="sessionSortColKey"></param>
    /// <param name="sessionDirectionKey"></param>
    /// <param name="offSet">Refers to the number non-boundable columns before boundable columns</param>
    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey, int offSet)
    {
        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey, offSet);

        if (sortColumnIndex != -1)
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }
    }

    private void AssignHeaderDataSource(bool empty, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string sort = "";

        if (tableName == "ttso_byitem")
        {
            sort = (string)dm.GetCache("gvSalesOrderByItemSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "ITEM";
            else
                sort += dm.GetCache("gvSalesOrderByItemSortDirection");

            this.gvByItem.Visible = true;
            this.gvSalesOrderHeaders.Visible = false;

            FetchHeaderDataSource(empty, "ITEM LIKE '%'", sort, this.gvByItem, tableName);
        }
        else
        {
            sort = (string)dm.GetCache("gvSalesOrderHeadersSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "so_id";
            else
                sort += dm.GetCache("gvSalesOrderHeadersSortDirection");

            this.gvSalesOrderHeaders.Visible = true;
            this.gvByItem.Visible = false;

            FetchHeaderDataSource(empty, "so_id>0", sort, this.gvSalesOrderHeaders, tableName);
        }
    }

    private void FetchHeaderDataSource(bool empty, string filter, string sort, GridView gv, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet salesOrderList;

        // First column is the Select link, do this to force new arrangement of fields
        string searchCriteria = (string)dm.GetCache("SalesOrderSearchCriteria");
        dm.RemoveCache("ttso_header");

        if (!this.IsPostBack || searchCriteria == null || empty)
            salesOrderList = new dsSalesOrderDataSet();
        else
        {
            if (dm.GetCache("dsSalesOrderDataSet") != null)
                salesOrderList = (dsSalesOrderDataSet)dm.GetCache("dsSalesOrderDataSet");
            else
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

                siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

                foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
                {
                    siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
                }

                currentCust.Dispose();

                Dmsi.Agility.Data.SalesOrders SalesOrders = new Dmsi.Agility.Data.SalesOrders(searchCriteria, siCallDS, "dsSalesOrderDataSet", out ReturnCode, out MessageText);

                siCallDS.Dispose();

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                salesOrderList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet)SalesOrders.ReferencedDataSet;
                dm.SetCache("dsSalesOrderDataSet", salesOrderList);
            }
        }

        // there is no dataset available, just initialize its schema
        if (salesOrderList == null)
            salesOrderList = new dsSalesOrderDataSet();

        DataView dv = null;
        if (tableName == "ttso_byitem")
            dv = new DataView(salesOrderList.ttso_byitem, filter, sort, DataViewRowState.CurrentRows);
        else
            dv = new DataView(salesOrderList.ttso_header, filter, sort, DataViewRowState.CurrentRows);

        DataTable dt = dv.ToTable(tableName + "Copy"); // this will give us the correct column arrangement

        if (dt.Rows.Count < 100)
            lblSalesOrderHeaderError.Visible = false;
        else
        {
            lblSalesOrderHeaderError.Visible = true;
            lblSalesOrderHeaderError.Text = "First 100 orders returned. Add criteria to refine search.";
        }

        if (dt.Rows.Count > 0)
        {
            dm.RemoveCache("NoSalesOrdersAvailable"); // this way the postback knows the grid is not empty
            gv.DataSource = dt;
            gv.DataBind();
            int ipageindex = gv.PageIndex;
            int igridcount = gv.Rows.Count;
            int itotalrecords = dt.Rows.Count;
            int iNoofrows = Convert.ToInt32(ddlSalesOrderHeaderRows.SelectedValue);
            int iFirstrecordno = ipageindex * (iNoofrows);
            iFirstrecordno = iFirstrecordno + 1;
            int iLastRecord = (iFirstrecordno - 1) + igridcount;

            if (itotalrecords > 0)
            {
                lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
            }
            else
            {
                lblpageno.Text = "";
            }

            if (dt.Rows.Count == 1 && tableName == "ttso_header")
            {
                dm.SetCache("ttso_header", dt.Rows[0]);
                this.gvSalesOrderHeaders.SelectedIndex = 0;

                GridViewRow row = gvSalesOrderHeaders.Rows[0];

                row.FindControl("innergrids").Visible = true;

                DropDownList ddlSalesOrderDetailRows = (DropDownList)row.FindControl("ddlSalesOrderDetailRows");
                string myDataItem = "";
                myDataItem = gvSalesOrderHeaders.DataKeys[row.RowIndex].Value.ToString();
                Label lblSalesOrderDetail = (Label)row.FindControl("lblSalesOrderDetail");
                lblSalesOrderDetail.Text = "Order Details for ID " + " " + myDataItem;
                ImageButton imgShowHide = (ImageButton)row.FindControl("imgbtnplusminus");
                imgShowHide.CommandName = "Hide";
                imgShowHide.ImageUrl = "~/images/collapse-icon.png";

                GridViewRow grinner = gvSalesOrderHeaders.Rows[row.RowIndex];
                ddlSalesOrderDetailRows.SelectedIndex = Profile.ddlSalesOrderDetailRows_SelectedIndex;
                GridView gvSalesOrderDetailInner = (GridView)grinner.FindControl("gvSalesOrderDetailInner");
                UserControls_DocViewer docViewer = (UserControls_DocViewer)grinner.FindControl("docViewer");
                DataRowView rowView = (DataRowView)grinner.DataItem;

                if (gvSalesOrderDetailInner != null)
                {
                    InitializeColumnVisibility(gvSalesOrderDetailInner, "gvSalesOrderDetail_Columns_Visible", false);
                    AssignDetailDataSource(false, gvSalesOrderDetailInner, docViewer, filter, myDataItem);
                }

                row.CssClass = "active-row-tr";

                SizeHiddenRow(row, ddlSalesOrderDetailRows, gvSalesOrderDetailInner);
            }
        }
        else
        {
            // this is a work around so the grid will show when the datasource is empty
            dm.SetCache("NoSalesOrdersAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);
            lblpageno.Text = "";
        }
    }

    private string DetailBuildFilter()
    {
        string filter = "";

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ttso_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ttso_header");
            filter = "so_id=" + row["so_id"].ToString();
        }
        else
            filter = "so_id=1234567890";

        return filter;
    }

    private void UpdateDetailInformation()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        UserControls_DocViewer docViewer = (UserControls_DocViewer)this.FindControl("gvQuoteHeaders").FindControl("docViewer");

        if (dm.GetCache("ttso_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ttso_header");

            docViewer.DocumentTitle = "Documents for ID " + row["so_id"].ToString();
            docViewer.Type = "SalesOrder";
            docViewer.OrderStatus = row["statusdescr"].ToString();
            docViewer.PriceHold = (bool)row["price_hold"];
            docViewer.AllowOrderAck = (bool)row["allow_order_ack"];
            docViewer.TransactionID = (int)row["so_id"];
            docViewer.BranchID = (string)row["branch_id"];

            dm.SetCache("DocViewerSOID", (int)row["so_id"]);
        }
        else
        {
            docViewer.DocumentTitle = "Documents";
            docViewer.Type = "";
            docViewer.TransactionID = 0;
            dm.RemoveCache("DocViewerSOID");
        }

        docViewer.Refresh();
    }

    private string BuildDocumentSearchCriteria(string salesOrderID)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string branch = "";
        string orderID = "";
        string sessionID = "";

        branch = "Branch=" + (string)dm.GetCache("currentBranchID");
        orderID = "TranID=" + salesOrderID;
        sessionID = "SessionID=" + Session.SessionID.ToString().Trim();

        searchCriteria = "DocType=OrderAck";
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + orderID;
        searchCriteria += "\x0003" + sessionID;

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        searchCriteria += "\x0003UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003Type=Order Acknowledgement";

        return searchCriteria;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Call this to clear bound data
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    /// <param name="customer">Current Customer Object</param>
    public void Reset()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("dsSalesOrderDataSet");
        dm.RemoveCache("ttso_header");
        dm.RemoveCache("SalesOrderSearchCriteria");
        this.AssignHeaderDataSource(true, "ttso_header");

        dm = null;
    }

    /// <summary>
    /// Call this to clear bound data when the branch or customer has changed
    /// </summary>
    /// <param name="branchID">Current Branch ID</param>
    /// <param name="shiptoObj">Current Shipto Object</param>
    public void Reset(string branchID, string shiptoObj)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if ((dm.GetCache("currentBranchID") == null ||
            (dm.GetCache("currentBranchID") != null && (string)dm.GetCache("currentBranchID") != branchID)) ||
            (dm.GetCache("ShiptoRowForInformationPanel") == null ||
            (dm.GetCache("ShiptoRowForInformationPanel") != null && ((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["cust_shipto_obj"].ToString() != shiptoObj)))
        {
            this.Reset();
        }

        dm = null;
    }

    #endregion

    public void btnFind_Click(object sender, EventArgs e)
    {
        ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
        pc.SOddlTranID_SelectedIndex = ddlTranID.SelectedIndex;
        pc.SOddlSearchStyle_SelectedIndex = ddlSearchStyle.SelectedIndex;
        pc.Save();

        ExecuteSearch();

        if (sender != null && e != null)
        {
            string analyticsLabel =
                                 "Search by = " + ddlTranID.SelectedItem + ", " +
                                 "Search Type = " + ddlSearchStyle.Text + ", " +
                                 "Status = " + ddlStatus.SelectedItem + ", " +
                                 "Show Results = " + this.ddlSalesOrderShowResultsBy.SelectedItem.Text + ", " +
                                 "Include all Ship-tos = " + this.cbShipTo.Checked.ToString() + ", " +
                                 "My orders only = " + this.cbMyOrdersOnly.Checked.ToString();

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Orders - Search Button";
            aInput.Label = analyticsLabel;
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }
    }

    protected void gvSalesOrderHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }
                e.Row.Cells[0].ToolTip = "View";
            }
        }
    }

    protected void gvSalesOrderHeader_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvSalesOrderHeaders.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "ttso_header");
        this.gvSalesOrderHeaders.SelectedIndex = -1;

        Session["Salesorder_SelectedRow"] = null;
    }

    protected void gvByItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvByItem.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "ttso_byitem");
    }

    protected void gvSalesOrderHeader_SelectedIndexChanged(object sender, EventArgs e)
    {
        string dataItem = "";
        GridView gv = (GridView)sender;

        dataItem = gv.SelectedDataKey.Value.ToString();

        int ReturnCode = 0;
        string MessageText = "";
        Dmsi.Agility.Data.SalesOrders salesOrderList = new Dmsi.Agility.Data.SalesOrders("dsSalesOrderDataSet", out ReturnCode, out MessageText);

        Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
        DataRow[] salesOrderRows = salesOrderList.ReferenceHeaderTable.Select("so_id='" + dataItem + "'");
        myDataManager.SetCache("ttso_header", salesOrderRows[0]);

        salesOrderList.Dispose();
    }

    protected void ddlSalesOrderHeaderRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlSalesOrderHeaderRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);

        this.gvSalesOrderHeaders.PageSize = pageSize;
        this.gvByItem.PageSize = pageSize;

        if ((string)Session["ddlSalesOrderShowResultsBy"] == "Item")
        {
            this.AssignHeaderDataSource(false, "ttso_byitem");
        }
        else
        {
            this.AssignHeaderDataSource(false, "ttso_header");
        }

        Session["Salesorder_SelectedRow"] = null;
    }

    protected void ddlSalesOrderDetailRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlSalesOrderDetailRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize;

        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        }

        DropDownList ddlSalesOrderDetailRows = (sender as DropDownList);
        LinkButton lnkSalesOrderInnerCustomView = (LinkButton)ddlSalesOrderDetailRows.Parent.FindControl("lnkSalesOrderInnerCustomView");
        int rowid = Convert.ToInt32(lnkSalesOrderInnerCustomView.CommandArgument);
        GridViewRow gr = (lnkSalesOrderInnerCustomView.NamingContainer as GridViewRow);

        string myDataItem = "";

        myDataItem = gvSalesOrderHeaders.DataKeys[gr.RowIndex].Value.ToString();

        GridView gvSalesOrderDetailInner = (GridView)gr.FindControl("gvSalesOrderDetailInner");
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("docViewer");

        DataRowView rowView = (DataRowView)gr.DataItem;

        if (gvSalesOrderDetailInner != null)
        {
            string filter = "so_id=" + myDataItem.ToString();

            gvSalesOrderDetailInner.PageSize = pageSize;

            AssignDetailDataSource(false, gvSalesOrderDetailInner, docViewer, filter, myDataItem);
        }

        SizeHiddenRow(gr, ddlSalesOrderDetailRows, gvSalesOrderDetailInner);
    }

    private void csCustomHeader_OnOKClicked(CheckBoxList checkBoxList)
    {
        string resultsView = (string)Session["ddlSalesOrderShowResultsBy"] == null ? "Sales Order" : (string)Session["ddlSalesOrderShowResultsBy"];

        if (resultsView == "Sales Order")
        {
            string sbuildcolumns = BuildColumnVisibilityProperty(checkBoxList, gvSalesOrderHeaders);
            Profile.gvSalesOrderHeaders_Columns_Visible = "1" + sbuildcolumns;
            InitializeColumnVisibility(gvSalesOrderHeaders, "gvSalesOrderHeaders_Columns_Visible", false);

            this.AssignHeaderDataSource(false, "ttso_header");
        }
        else
        {
            Profile.gvSalesOrderItems_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvByItem);
            this.AssignHeaderDataSource(false, "ttso_byitem");
        }
    }

    private void csCustomDetail_OnOKClicked(CheckBoxList checkBoxList)
    {
        if (Session["Salesorder_SelectedRow"] != null)
        {
            int irow = Convert.ToInt32(Session["Salesorder_SelectedRow"]);
            GridViewRow gvr = GvSalesOrderHeaders.Rows[irow];

            string myDataItem = "";

            myDataItem = gvSalesOrderHeaders.DataKeys[gvr.RowIndex].Value.ToString();

            GridView gvSalesOrderDetailInner = (GridView)gvr.FindControl("gvSalesOrderDetailInner");
            UserControls_DocViewer docViewer = (UserControls_DocViewer)gvr.FindControl("docViewer");
            Profile.gvSalesOrderDetail_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvSalesOrderDetailInner);

            DataRowView rowView = (DataRowView)gvr.DataItem;

            if (gvSalesOrderDetailInner != null)
            {

                string filter = "so_id=" + myDataItem.ToString();

                AssignDetailDataSource(false, gvSalesOrderDetailInner, docViewer, filter, myDataItem);
            }
        }
    }

    private void csCustomDetail_OnResetClicked()
    {
        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderDetail_Columns_Visible"].DefaultValue.ToString();
        Profile.gvSalesOrderDetail_Columns_Visible = defaultValue;

        if (Session["Salesorder_SelectedRow"] != null)
        {
            int irow = Convert.ToInt32(Session["Salesorder_SelectedRow"]);
            GridViewRow gvr = GvSalesOrderHeaders.Rows[irow];

            string myDataItem = "";

            myDataItem = gvSalesOrderHeaders.DataKeys[gvr.RowIndex].Value.ToString();

            GridView gvSalesOrderDetailInner = (GridView)gvr.FindControl("gvSalesOrderDetailInner");
            UserControls_DocViewer docViewer = (UserControls_DocViewer)gvr.FindControl("docViewer");

            DataRowView rowView = (DataRowView)gvr.DataItem;

            if (gvSalesOrderDetailInner != null)
            {
                string filter = "so_id=" + myDataItem.ToString();

                InitializeColumnVisibility(gvSalesOrderDetailInner, "gvSalesOrderDetail_Columns_Visible", false);

                AssignDetailDataSource(false, gvSalesOrderDetailInner, docViewer, filter, myDataItem);
            }
        }
    }

    private void csCustomHeader_OnResetClicked()
    {
        string defaultValue;
        string resultsView = (string)Session["ddlSalesOrderShowResultsBy"] == null ? "Sales Order" : (string)Session["ddlSalesOrderShowResultsBy"];

        switch (resultsView)
        {
            case "Item":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderItems_Columns_Visible"].DefaultValue.ToString();
                    Profile.gvSalesOrderItems_Columns_Visible = defaultValue;
                    InitializeColumnVisibility(gvByItem, "gvSalesOrderItems_Columns_Visible", false);

                    break;
                }
            case "Sales Order":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderHeaders_Columns_Visible"].DefaultValue.ToString();
                    Profile.gvSalesOrderHeaders_Columns_Visible = defaultValue;
                    InitializeColumnVisibility(gvSalesOrderHeaders, "gvSalesOrderHeaders_Columns_Visible", false);

                    break;
                }
        }

        btnFind_Click(null, null);
    }

    protected void ExecuteSearch()
    {
        // save the current search criteria until a new find is initiated
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // This is needed when building the header caption inside HeaderBuildSearchCriteria
        if (this.ddlSalesOrderShowResultsBy.SelectedValue == "Item")
            dm.SetCache("ddlSalesOrderShowResultsBy", "Item");
        else
            dm.SetCache("ddlSalesOrderShowResultsBy", "Sales Order");

        string searchCriteria = this.HeaderBuildSearchCriteria();

        if (searchCriteria == "Invalid input date.")
        {
            lblSalesOrderHeaderError.Visible = true;
            lblSalesOrderHeaderError.Text = searchCriteria;
            lblSalesOrderHeader.Text = "Orders";
            return;
        }

        if (searchCriteria == "Invalid date range.")
        {
            lblSalesOrderHeaderError.Visible = true;
            lblSalesOrderHeaderError.Text = searchCriteria;
            lblSalesOrderHeader.Text = "Orders";
            return;
        }

        if (searchCriteria == "")
        {
            lblSalesOrderHeaderError.Visible = true;
            lblSalesOrderHeaderError.Text = "Limit date range to 2 years or less.";
            lblSalesOrderHeader.Text = "Orders";
            return;
        }

        dm.SetCache("SalesOrderSearchCriteria", searchCriteria);
        dm.RemoveCache("dsSalesOrderDataSet");
        dm.RemoveCache("DocViewerSOID");

        this.gvSalesOrderHeaders.SelectedIndex = -1;

        if (this.ddlSalesOrderShowResultsBy.SelectedValue == "Item")
            this.AssignHeaderDataSource(false, "ttso_byitem");
        else
            this.AssignHeaderDataSource(false, "ttso_header");
    }

    private string HeaderBuildSearchCriteria()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string shipto = "";
        string shipToSequence = "";
        string searchType = "";
        string searchStyle = "";
        string searchTran = "";
        string customer = "";
        string branch = "";
        string status = "";
        string showResults = "";
        string startDateStr = "";
        string endDateStr = "";
        string gridTitle = "";
        string myorders = "";

        DataRow shipToRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
        shipToSequence = shipToRow["seq_num"].ToString() + "";
        branch = "Branch=" + (string)dm.GetCache("currentBranchID");

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer currentCustomer = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        customer = "Customer=" + currentCustomer.CustomerKey;
        currentCustomer.Dispose();

        if (ddlTranID.SelectedValue == "Order ID")
            searchType = "SearchType=Sales Order ID";
        else
            searchType = "SearchType=" + ddlTranID.SelectedValue;

        searchStyle = "SearchStyle=" + ddlSearchStyle.Text;

        if (searchType.Contains("PO ID"))
            searchType = "SearchType=Customer PO #";

        if (txtTranID.Text.Trim().Length == 0)
            searchTran = "SearchTran=<ALL>";
        else
            searchTran = "SearchTran=" + txtTranID.Text.Trim();

        if (ddlStatus.SelectedValue == "All")
            status = "Status=<ALL>";
        else
        {
            status = "Status=" + ddlStatus.SelectedValue;
            gridTitle += status + " ";
        }

        if (this.cbShipTo.Checked)
            shipto = "Ship To=<ALL>";
        else
        {
            shipto = "Ship To=" + shipToSequence;
            gridTitle += shipto + " ";
        }

        if (this.cbMyOrdersOnly.Checked)
            myorders = "MyOrdersOnly=YES";
        else
        {
            myorders = "MyOrdersOnly=NO";
            gridTitle += myorders + " ";
        }

        if (this.ddlSalesOrderShowResultsBy.SelectedValue == "Order")
            showResults = "ShowResults=Sales Order";
        else
            showResults = "Show Results=" + this.ddlSalesOrderShowResultsBy.SelectedValue;

        bool hasStartDate = false, hasEndDate = false;
        DateTime startDate = DateTime.Today.AddDays(-30), endDate = DateTime.Today;
        DateTimeValidator startDateValidator = new DateTimeValidator(this.txtFromDate.Text);
        DateTimeValidator endDateValidator = new DateTimeValidator(this.txtToDate.Text);

        hasStartDate = startDateValidator.InfoEntered();
        hasEndDate = endDateValidator.InfoEntered();

        if (hasStartDate && startDateValidator.ValidDate())
        {
            startDate = startDateValidator.ParsedDateTime();

            if (this.txtFromDate.Text != startDateValidator.FormattedDateTime())
            {
                this.txtFromDate.Text = startDateValidator.FormattedDateTime();
            }
        }
        else if (hasStartDate)
        {
            return startDateValidator.ValidationMessage();
        }

        if (hasEndDate && endDateValidator.ValidDate())
        {
            endDate = endDateValidator.ParsedDateTime();

            if (this.txtToDate.Text != endDateValidator.FormattedDateTime())
            {
                this.txtToDate.Text = endDateValidator.FormattedDateTime();
            }
        }
        else if (hasEndDate)
        {
            return endDateValidator.ValidationMessage();
        }

        startDateStr = "Start Date=" + (hasStartDate ? startDate.ToShortDateString() : "<ALL>");
        endDateStr = "End Date=" + (hasEndDate ? endDate.ToShortDateString() : "<ALL>");

        if (hasStartDate && hasEndDate)
        {
            if (startDate.AddYears(2) < endDate)
            {
                return "";
            }
            else if (startDate > endDate)
            {
                return "Invalid date range.";
            }
            else
                gridTitle = "Orders from " + startDate.ToDMSiDateFormat() + " to " + endDate.ToDMSiDateFormat();
        }
        else if (hasStartDate)
        {
            if (startDate.AddYears(2) < DateTime.Now)
            {
                return "";
            }
            else
                gridTitle = "Orders from " + startDate.ToDMSiDateFormat();
        }
        else if (hasEndDate)
        {
            gridTitle = "Orders up to " + endDate.ToDMSiDateFormat();
            return "";
        }
        else
        {
            gridTitle = "Orders";
            return "";
        }

        lblSalesOrderHeader.Text = gridTitle;
        lblSalesOrderHeader.Text.Trim();

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        searchCriteria = "UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + customer;

        searchCriteria += "\x0003" + shipto;
        searchCriteria += "\x0003" + myorders;
        searchCriteria += "\x0003" + status;
        searchCriteria += "\x0003" + searchType;
        searchCriteria += "\x0003" + searchStyle;
        searchCriteria += "\x0003" + searchTran;
        searchCriteria += "\x0003" + startDateStr;
        searchCriteria += "\x0003" + endDateStr;
        searchCriteria += "\x0003" + showResults;
        searchCriteria += "\x0003Type=Sales Orders";
        searchCriteria += "\x0003ExtendPrice=YES";

        if ((string)Session["ddlSalesOrderShowResultsBy"] == "Sales Order")
            searchCriteria += "\x0003OrderTotals=YES";

        return searchCriteria;
    }

    protected void gvSalesOrderHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvSalesOrderHeaders, e.Row, "gvSalesOrderHeadersSortColumn", "gvSalesOrderHeadersSortDirection", 0);
    }

    protected void gvSalesOrderDetailInner_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(sender as GridView, e.Row, "gvSalesOrderDetailSortColumn", "gvSalesOrderDetailSortDirection", 0);
    }

    protected void gvSalesOrderHeader_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvSalesOrderHeadersSortColumn"] != e.SortExpression)
            Session["gvSalesOrderHeadersSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvSalesOrderHeadersSortDirection"] == " ASC")
                Session["gvSalesOrderHeadersSortDirection"] = " DESC";
            else
                Session["gvSalesOrderHeadersSortDirection"] = " ASC";
        }

        gvSalesOrderHeaders.PageIndex = 0;
        Session["gvSalesOrderHeadersSortColumn"] = e.SortExpression;

        if ((string)Session["ddlSalesOrderShowResultsBy"] == "Item")
            this.AssignHeaderDataSource(false, "ttso_byitem");
        else
            this.AssignHeaderDataSource(false, "ttso_header");
    }

    protected void gvSalesOrderDetailInner_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvSalesOrderDetailSortColumn"] != e.SortExpression)
            Session["gvSalesOrderDetailSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvSalesOrderDetailSortDirection"] == " ASC")
                Session["gvSalesOrderDetailSortDirection"] = " DESC";
            else
                Session["gvSalesOrderDetailSortDirection"] = " ASC";
        }

        Session["gvSalesOrderDetailSortColumn"] = e.SortExpression;
        GridView gvSalesOrderDetailInner = (sender as GridView);
        gvSalesOrderDetailInner.PageIndex = 0;
        LinkButton lnkSalesOrderInnerCustomView = (LinkButton)gvSalesOrderDetailInner.Parent.FindControl("lnkSalesOrderInnerCustomView");

        GridViewRow gr = (lnkSalesOrderInnerCustomView.NamingContainer as GridViewRow);
        int rowid = gr.RowIndex;
        string myDataItem = "";

        myDataItem = gvSalesOrderHeaders.DataKeys[gr.RowIndex].Value.ToString();

        gvSalesOrderDetailInner = (GridView)gr.FindControl("gvSalesOrderDetailInner");
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("docViewer");

        DataRowView rowView = (DataRowView)gr.DataItem;

        if (gvSalesOrderDetailInner != null)
        {
            string filter = "so_id=" + myDataItem.ToString();
            AssignDetailDataSource(false, gvSalesOrderDetailInner, docViewer, filter, myDataItem);
        }
    }

    protected void gvByItem_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvSalesOrderByItemSortColumn"] != e.SortExpression)
            Session["gvSalesOrderByItemSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvSalesOrderByItemSortDirection"] == " ASC")
                Session["gvSalesOrderByItemSortDirection"] = " DESC";
            else
                Session["gvSalesOrderByItemSortDirection"] = " ASC";
        }

        this.gvByItem.PageIndex = 0;
        Session["gvSalesOrderByItemSortColumn"] = e.SortExpression;
        this.AssignHeaderDataSource(false, "ttso_byitem");
    }

    protected void gvByItem_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvByItem, e.Row, "gvSalesOrderByItemSortColumn", "gvSalesOrderByItemSortDirection", 0);
    }

    protected void gvSalesOrderDetailInner_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvSalesOrderDetailInner = (sender as GridView);
        LinkButton lnkSalesOrderInnerCustomView = (LinkButton)gvSalesOrderDetailInner.Parent.FindControl("lnkSalesOrderInnerCustomView");

        GridViewRow gr = (lnkSalesOrderInnerCustomView.NamingContainer as GridViewRow);
        int rowid = gr.RowIndex;
        string myDataItem = "";

        myDataItem = gvSalesOrderHeaders.DataKeys[gr.RowIndex].Value.ToString();

        gvSalesOrderDetailInner = (GridView)gr.FindControl("gvSalesOrderDetailInner");
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("docViewer");

        DataRowView rowView = (DataRowView)gr.DataItem;
        if (gvSalesOrderDetailInner != null)
        {
            string filter = "so_id=" + myDataItem.ToString();
            gvSalesOrderDetailInner.PageIndex = e.NewPageIndex;
            AssignDetailDataSource(false, gvSalesOrderDetailInner, docViewer, filter, myDataItem);
        }

        Session["Salesorder_SelectedRow"] = null;

        DropDownList ddlSalesOrderDetailRows = (DropDownList)gr.FindControl("ddlSalesOrderDetailRows");

        SizeHiddenRow(gr, ddlSalesOrderDetailRows, gvSalesOrderDetailInner);
    }

    protected void Show_Hide_ChildGrid(object sender, EventArgs e)
    {
        this.gvSalesOrderHeaders.SelectedIndex = -1;

        ImageButton imgShowHide = (sender as ImageButton);
        imgShowHide.ToolTip = "View";
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        Session["Salesorder_SelectedRow"] = row.RowIndex;
        int irowids = Convert.ToInt32(imgShowHide.CommandArgument);
        for (int i = 0; i <= gvSalesOrderHeaders.Rows.Count - 1; i++)
        {
            GridViewRow gvr = (GridViewRow)gvSalesOrderHeaders.Rows[i];

            ImageButton imgbutton = (ImageButton)gvr.FindControl("imgbtnplusminus");

            if (i != row.RowIndex)
            {
                int inextrow = row.RowIndex;
                inextrow = inextrow + 1;

                if (i == inextrow)
                {
                    gvr.CssClass = row.CssClass = "activealt-row-tr";
                    this.gvSalesOrderHeaders.SelectedIndex = -1;
                }
                else
                {
                    gvr.CssClass = "";
                }

                gvr.FindControl("innergrids").Visible = false;
                gvr.FindControl("innerplaceholder").Visible = false;

                imgbutton.CommandName = "Details";
                imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                imgbutton.ToolTip = "View";
            }
            else
            {
                if (imgShowHide.ImageUrl == "~/images/collapse-icon.png")
                {
                    gvr.FindControl("innergrids").Visible = false;
                    gvr.FindControl("innerplaceholder").Visible = false;

                    imgbutton.CommandName = "Hide";
                    imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                    imgbutton.ToolTip = "View";
                }
            }
        }

        if (imgShowHide.CommandName == "Details")
        {
            string myDataItem = gvSalesOrderHeaders.DataKeys[row.RowIndex].Value.ToString();

            DataManager dm = new DataManager();

            DropDownList ddlSalesOrderDetailRows = (DropDownList)row.FindControl("ddlSalesOrderDetailRows");
            ddlSalesOrderDetailRows.SelectedIndex = Profile.ddlSalesOrderDetailRows_SelectedIndex;

            Label lblSalesOrderDetail = (Label)row.FindControl("lblSalesOrderDetail");
            lblSalesOrderDetail.Text = "Order Details for ID " + " " + myDataItem;
            imgShowHide.CommandName = "Hide";
            imgShowHide.ImageUrl = "~/images/collapse-icon.png";

            GridViewRow grinner = gvSalesOrderHeaders.Rows[row.RowIndex];
            GridView gvSalesOrderDetailInner = (GridView)grinner.FindControl("gvSalesOrderDetailInner");
            UserControls_DocViewer docViewer = (UserControls_DocViewer)grinner.FindControl("docViewer");

            if (gvSalesOrderDetailInner != null)
            {
                string filter = "so_id=" + myDataItem.ToString();

                int ReturnCode = 0;
                string MessageText = "";
                Dmsi.Agility.Data.SalesOrders salesOrderList = new Dmsi.Agility.Data.SalesOrders("dsSalesOrderDataSet", out ReturnCode, out MessageText);

                if (salesOrderList != null)
                {
                    DataRow[] CreditMemoRows = salesOrderList.ReferenceHeaderTable.Select("so_id='" + myDataItem + "'");
                    dm.SetCache("ttso_header", CreditMemoRows[0]);
                    InitializeColumnVisibility(gvSalesOrderDetailInner, "gvSalesOrderDetail_Columns_Visible", false);
                    AssignDetailDataSource(false, gvSalesOrderDetailInner, docViewer, filter, myDataItem);
                }
            }

            row.CssClass = "active-row-tr";
            this.gvSalesOrderHeaders.SelectedIndex = irowids % this.gvSalesOrderHeaders.PageSize;

            SizeHiddenRow(row, ddlSalesOrderDetailRows, gvSalesOrderDetailInner);
        }
        else
        {
            row.FindControl("innergrids").Visible = false;
            row.FindControl("innerplaceholder").Visible = false;

            imgShowHide.CommandName = "Details";
            imgShowHide.ImageUrl = "~/Images/Grid_Plus.gif";
            imgShowHide.ToolTip = "View";
        }
    }

    public void AssignDetailDataSource(bool empty, GridView gvSalesOrderDetailInner, UserControls_DocViewer docViewer, string filter, string soid)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet ds;

        if (docViewer != null)
        {
            if (dm.GetCache("ttso_header") != null)
            {
                DataRow row = (DataRow)dm.GetCache("ttso_header");
                docViewer.DocumentTitle = "Documents for ID " + row["so_id"].ToString();
                docViewer.Type = "SalesOrder";
                docViewer.OrderStatus = row["statusdescr"].ToString();
                docViewer.PriceHold = (bool)row["price_hold"];
                docViewer.AllowOrderAck = (bool)row["allow_order_ack"];
                docViewer.TransactionID = (int)row["so_id"];
                docViewer.BranchID = (string)row["branch_id"];

                dm.SetCache("DocViewerSOID", (int)row["so_id"]);
            }
            else
            {
                docViewer.DocumentTitle = "Document";
                docViewer.Type = "";
                docViewer.TransactionID = 0;
                dm.RemoveCache("DocViewerCMID");
            }

            docViewer.Refresh();
            GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

            if (gvdoc.Rows.Count <= 0)
            {
                docViewer.Visible = false;
            }
        }

        string sort = (string)dm.GetCache("gvSalesOrderDetailSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "ITEM";
        else
            sort += dm.GetCache("gvSalesOrderDetailSortDirection");

        if (!this.IsPostBack || empty)
            ds = new dsSalesOrderDataSet();
        else
            ds = (dsSalesOrderDataSet)dm.GetCache("dsSalesOrderDataSet");

        // no fetch yet
        if (ds == null)
            ds = new dsSalesOrderDataSet();

        DataView dv = new DataView(ds.ttso_detail, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttso_detailCopy");
        DropDownList ddlSalesOrderDetailRows = (DropDownList)gvSalesOrderDetailInner.Parent.FindControl("ddlSalesOrderDetailRows");

        if (dt.Rows.Count > 0)
        {
            int pageSize;
            if (ddlSalesOrderDetailRows.SelectedValue == "All")
            {
                pageSize = 32000;
            }
            else
            {
                pageSize = Convert.ToInt32(ddlSalesOrderDetailRows.SelectedValue);
            }

            csCustomDetail.GridView = gvSalesOrderDetailInner;
            gvSalesOrderDetailInner.PageSize = pageSize;
            gvSalesOrderDetailInner.DataSource = dt;
            gvSalesOrderDetailInner.DataBind();
        }
        else
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gvSalesOrderDetailInner);
    }

    protected void lnkSalesorder_Click(object sender, EventArgs e)
    {
        LinkButton lnkSalesorder = (sender as LinkButton);
        GridViewRow gvrsalesdetails = (lnkSalesorder.NamingContainer as GridViewRow);
        Session["Salesorder_SelectedRow"] = gvrsalesdetails.RowIndex;
        ModalPopupExtender2.Show();
    }

    protected void RebindDocviewer(int Rowid)
    {
        GridViewRow gr = gvSalesOrderHeaders.Rows[Rowid];

        gr.CssClass = "active-row-tr";
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("docViewer");

        string myDataItem = "";
        myDataItem = gvSalesOrderHeaders.DataKeys[Rowid].Value.ToString();
        string filter = "so_id=" + myDataItem.ToString();

        int ReturnCode = 0;
        string MessageText = "";
        Dmsi.Agility.Data.SalesOrders salesOrderList = new Dmsi.Agility.Data.SalesOrders("dsSalesOrderDataSet", out ReturnCode, out MessageText);
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (salesOrderList != null)
        {
            DataRow[] CreditMemoRows = salesOrderList.ReferenceHeaderTable.Select("so_id='" + myDataItem + "'");
            dm.SetCache("ttso_header", CreditMemoRows[0]);

            if (docViewer != null)
            {
                if (dm.GetCache("ttso_header") != null)
                {
                    DataRow row = (DataRow)dm.GetCache("ttso_header");

                    docViewer.DocumentTitle = "Documents for ID " + row["so_id"].ToString();
                    docViewer.Type = "SalesOrder";
                    docViewer.OrderStatus = row["statusdescr"].ToString();
                    docViewer.PriceHold = (bool)row["price_hold"];
                    docViewer.AllowOrderAck = (bool)row["allow_order_ack"];
                    docViewer.TransactionID = (int)row["so_id"];
                    docViewer.BranchID = (string)row["branch_id"];

                    dm.SetCache("DocViewerSOID", (int)row["so_id"]);
                }
                else
                {
                    docViewer.DocumentTitle = "Documents";
                    docViewer.Type = "";
                    docViewer.TransactionID = 0;
                    dm.RemoveCache("DocViewerCMID");
                }

                docViewer.Refresh();
                GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

                if (gvdoc.Rows.Count <= 0)
                {
                    docViewer.Visible = false;
                }
            }
        }
    }

    protected void ddlTranID_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDdlSearchTypeValues(ddlTranID.Text);
    }

    private void SetDdlSearchTypeValues(string ddlTranIDText)
    {
        switch (ddlTranIDText)
        {
            case "Order ID":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.Items.Add(new ListItem("Starting at"));
                break;

            case "Reference #":
            case "Item #":
            case "PO ID":
            case "Job #":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Beginning with"));
                ddlSearchStyle.Items.Add(new ListItem("Containing"));
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.SelectedIndex = 2;
                break;
        }
    }

    private static void SizeHiddenRow(GridViewRow row, DropDownList ddlSalesOrderDetailRows, GridView gvSalesOrderDetailInner)
    {
        UserControls_DocViewer docViewer = (UserControls_DocViewer)row.FindControl("docViewer");

        int gvdocHeight = 0;

        GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

        if (gvdoc != null && gvdoc.Rows.Count > 0)
        {
            gvdocHeight = 42 + (gvdoc.Rows.Count * 42);
        }
        else
            gvdocHeight = -42;

        int detailrowcount = gvSalesOrderDetailInner.Rows.Count;

        if (ddlSalesOrderDetailRows.SelectedValue != "All")
        {
            if (detailrowcount > Convert.ToInt32(ddlSalesOrderDetailRows.SelectedValue))
            {
                detailrowcount = Convert.ToInt32(ddlSalesOrderDetailRows.SelectedValue);
            }
        }

        int pagerAddHeight = 0;

        if (gvSalesOrderDetailInner.PageCount > 1)
            pagerAddHeight = 56;

        int panelheight = 232 + gvdocHeight + (detailrowcount * 42) + pagerAddHeight;

        row.FindControl("innergrids").Visible = true;

        Panel p = row.FindControl("innerplaceholder") as Panel;
        p.Visible = true;
        p.Height = new Unit(panelheight, UnitType.Pixel);
    }
}
