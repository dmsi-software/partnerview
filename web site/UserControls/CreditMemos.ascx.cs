using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Data;

public partial class UserControls_CreditMemos : System.Web.UI.UserControl
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);
        if ((string)Session["ddlCreditMemoShowResultsBy"] == "Item")
            csCustomHeader.GridView = this.gvByItem;
        else
            csCustomHeader.GridView = this.gvCreditMemoHeaders;

        if (Session["currentUserType"] != null && ((string)Session["currentUserType"]).ToLower() == "guest")
        {
            this.cbMyCreditsOnly.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.csCustomHeader.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomHeader_OnOKClicked);
        this.csCustomHeader.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomHeader_OnResetClicked);
        this.csCustomDetail.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomDetail_OnOKClicked);
        this.csCustomDetail.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomDetail_OnResetClicked);

        if (!this.IsPostBack)
        {
            ddlSearchStyle.SelectedIndex = 2;

            //load data fields with today and 30 days prior...
            this.txtToDate.Text = DateTime.Now.ToDMSiDateFormat();
            this.txtFromDate.Text = (DateTime.Today.AddMonths(-1)).ToDMSiDateFormat();

            this.ddlCreditMemoHeaderRows.SelectedIndex = Profile.ddlCreditMemoHeaderRows_SelectedIndex;

            int pageSize = Convert.ToInt32(ddlCreditMemoHeaderRows.SelectedValue);

            this.gvCreditMemoHeaders.PageSize = pageSize;
            this.gvByItem.PageSize = pageSize;

            InitializeColumnVisibility(gvCreditMemoHeaders, "gvCreditMemoHeaders_Columns_Visible");
            InitializeColumnVisibility(gvByItem, "gvCreditMemoItems_Columns_Visible");

            ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
            if (pc.CMddlTranID_SelectedIndex > ddlTranID.Items.Count)
                ddlTranID.SelectedIndex = 0;
            else
                ddlTranID.SelectedIndex = pc.CMddlTranID_SelectedIndex;

            SetDdlSearchTypeValues(ddlTranID.Text);

            if (pc.CMddlSearchStyle_SelectedIndex > ddlSearchStyle.Items.Count)
                ddlSearchStyle.SelectedIndex = 0;
            else
                ddlSearchStyle.SelectedIndex = pc.CMddlSearchStyle_SelectedIndex;
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // this is needed for the columns to show correctly
        if (!this.IsPostBack || dm.GetCache("NoCreditMemosAvailable") != null)
        {
            if (!this.IsPostBack)
            {
                dm.SetCache("gvCreditMemoHeadersSortColumn", "so_id");
                dm.SetCache("gvCreditMemoHeadersSortDirection", " DESC");
                dm.SetCache("gvCreditMemoDetailSortColumn", "ITEM");
                dm.SetCache("gvCreditMemoDetailSortDirection", " ASC");
                dm.SetCache("gvCreditMemoByItemSortColumn", "ITEM");
                dm.SetCache("gvCreditMemoByItemSortDirection", " ASC");
                dm.SetCache("gvCreditMemoDetailCustomeViewSortColumn", "ITEM");
                dm.SetCache("gvCreditMemoDetailCustomViewSortDirection", " ASC");
            }

            if ((string)dm.GetCache("ddlCreditMemoShowResultsBy") == "Item")
                this.AssignHeaderDataSource(true, "ttcm_byitem");
            else
                this.AssignHeaderDataSource(true, "ttcm_header");
        }
        else if (dm.GetCache("DocViewerCMID") != null)
        {
            if (Session["creditmemos_RowId"] != null)
            {
                int rowid = 0;

                if (!(gvCreditMemoHeaders.Rows.Count - 1 < Convert.ToInt32(Session["creditmemos_RowId"])))
                    rowid = Convert.ToInt32(Session["creditmemos_RowId"]);

                GridViewRow gvcreditdetails = gvCreditMemoHeaders.Rows[rowid];
                if (gvcreditdetails != null)
                {
                    BindDocumentviewr(rowid);
                }
            }
        }

        dm = null;
    }

    #region Private Methods

    /// <summary>
    /// Routine to build the string corresponding to the visible columns of the grid
    /// </summary>
    /// <param name="checkBoxList"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    private string BuildColumnVisibilityProperty(CheckBoxList checkBoxList, GridView gridView)
    {
        string outputString = "";
        char c;

        for (int x = 0; x < checkBoxList.Items.Count; x++)
        {
            if (checkBoxList.Items[x].Selected)
            {
                c = '1';
                gridView.Columns[x].Visible = true;
            }
            else
            {
                c = '0';
                gridView.Columns[x].Visible = false;
            }
            outputString += c;
        }

        return outputString;
    }

    /// <summary>
    /// Assign the column visibility based on profile settings
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="profileName"></param>
    private void InitializeColumnVisibility(GridView gridView, string profileName)
    {
        string a = Profile.PropertyValues[profileName].PropertyValue.ToString();
        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            Type t = gridView.Columns[x].GetType();
            if ((t.Name != "ButtonField") && (t.Name != "TemplateField"))
            {
                try
                {
                    bool isVisible = false;

                    if ((Profile.PropertyValues[profileName] == null && System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString().Substring(x, 1) == "1") || (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(x, 1) == "1"))
                    {
                        isVisible = true;
                    }

                    gridView.Columns[x].Visible = isVisible;
                }
                catch (System.ArgumentOutOfRangeException aoor)
                {
                    string message = aoor.Message;
                    Profile.PropertyValues[profileName].PropertyValue = System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString();
                }
            }

            if ((gridView.ID == "gvCreditMemoHeaders"))
            {
                if (x == 0)
                    gridView.Columns[x].Visible = true;
            }
        }
    }

    // This is a helper method used to determine the index of the
    // column being sorted. If no column is being sorted, -1 is returned.
    private int GetSortColumnIndex(GridView gv, string sessionSortColKey, int offSet)
    {
        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + offSet;
            }
        }

        return -1;
    }

    // This is a helper method used to add a sort direction
    // image to the header of the column being sorted.
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {
        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        Image sortImage = new Image();
        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
        ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

        // Add the image to the appropriate header cell.
        imageLink.CommandName = "Sort";
        imageLink.CommandArgument = (string)Session[sessionSortColKey];
        imageLink.Controls.Add(sortImage);
        headerRow.Cells[columnIndex].Controls.Add(imageLink);

        imageLink.Dispose();
        sortImage.Dispose();
    }

    /// <summary>
    /// Insert the sort image to the column header
    /// </summary>
    /// <param name="gv"></param>
    /// <param name="row"></param>
    /// <param name="sessionSortColKey"></param>
    /// <param name="sessionDirectionKey"></param>
    /// <param name="offSet">Refers to the number non-boundable columns before boundable columns</param>
    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey, int offSet)
    {
        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey, offSet);

        if (sortColumnIndex != -1)
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }
    }

    private void AssignHeaderDataSource(bool empty, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string sort = "";

        if (tableName == "ttcm_byitem")
        {
            sort = (string)dm.GetCache("gvCreditMemoByItemSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "ITEM";
            else
                sort += dm.GetCache("gvCreditMemoByItemSortDirection");

            this.gvByItem.Visible = true;
            this.gvCreditMemoHeaders.Visible = false;
            this.pnlSaleOrderDetail.Visible = false; // Detail GridView container

            this.lnkCreditMemoDetailCustomView.Visible = false;
            this.pnlCreditMemoDetailCustomView.Visible = false;

            FetchHeaderDataSource(empty, "ITEM LIKE '%'", sort, this.gvByItem, tableName);
        }
        else
        {
            sort = (string)dm.GetCache("gvCreditMemoHeadersSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "so_id";
            else
                sort += dm.GetCache("gvCreditMemoHeadersSortDirection");

            this.gvCreditMemoHeaders.Visible = true;
            this.gvByItem.Visible = false;
            this.pnlSaleOrderDetail.Visible = true; // Detail GridView container

            this.lnkCreditMemoDetailCustomView.Visible = true;
            this.pnlCreditMemoDetailCustomView.Visible = true;

            FetchHeaderDataSource(empty, "so_id>0", sort, this.gvCreditMemoHeaders, tableName);
        }
    }

    private void FetchHeaderDataSource(bool empty, string filter, string sort, GridView gv, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet creditMemoList;

        // First column is the Select link, do this to force new arrangement of fields
        string searchCriteria = (string)dm.GetCache("CreditMemoSearchCriteria");
        dm.RemoveCache("ttcm_header");

        if (!this.IsPostBack || searchCriteria == null || empty)
            creditMemoList = new dsSalesOrderDataSet();
        else
        {
            if (dm.GetCache("dsCreditMemoDataSet") != null)
                creditMemoList = (dsSalesOrderDataSet)dm.GetCache("dsCreditMemoDataSet");
            else
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

                siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

                foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
                {
                    siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
                }

                currentCust.Dispose();

                Dmsi.Agility.Data.SalesOrders CreditMemos = new Dmsi.Agility.Data.SalesOrders(searchCriteria, siCallDS, "dsCreditMemoDataSet", out ReturnCode, out MessageText);
                siCallDS.Dispose();

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                creditMemoList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet)CreditMemos.ReferencedDataSet;
                dm.SetCache("dsCreditMemoDataSet", creditMemoList);
            }
        }

        // there is no dataset available, just initialize its schema
        if (creditMemoList == null)
            creditMemoList = new dsSalesOrderDataSet();

        DataView dv = null;
        if (tableName == "ttcm_byitem")
            dv = new DataView(creditMemoList.ttso_byitem, filter, sort, DataViewRowState.CurrentRows);
        else
            dv = new DataView(creditMemoList.ttso_header, filter, sort, DataViewRowState.CurrentRows);

        DataTable dt = dv.ToTable(tableName + "Copy"); // this will give us the correct column arrangement

        if (dt.Rows.Count < 100)
            lblCreditMemoHeaderError.Visible = false;
        else
        {
            lblCreditMemoHeaderError.Visible = true;
            lblCreditMemoHeaderError.Text = "First 100 credit memos returned. Add criteria to refine search.";
        }

        if (dt.Rows.Count > 0)
        {
            dm.RemoveCache("NoCreditMemosAvailable"); // this way the postback knows the grid is not empty
            gv.DataSource = dt;
            gv.DataBind();
            int ipageindex = gv.PageIndex;
            int igridcount = gv.Rows.Count;
            int itotalrecords = dt.Rows.Count;
            int iNoofrows = Convert.ToInt32(ddlCreditMemoHeaderRows.SelectedValue);
            int iFirstrecordno = ipageindex * (iNoofrows);
            iFirstrecordno = iFirstrecordno + 1;
            int iLastRecord = (iFirstrecordno - 1) + igridcount;

            if (itotalrecords > 0)
            {
                lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
            }

            if (dt.Rows.Count == 1 && tableName == "ttcm_header")
            {
                dm.SetCache("ttcm_header", dt.Rows[0]);
                gvCreditMemoHeaders.SelectedIndex = 0;
                GridViewRow grheader = gvCreditMemoHeaders.Rows[0];
                BindselectedInnerGrid(grheader);
                grheader.CssClass = "active-row-tr";
            }
        }
        else
        {
            lblpageno.Text = "";
            // this is a work around so the grid will show when the datasource is empty
            dm.SetCache("NoCreditMemosAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);
        }
    }

    private string DetailBuildFilter()
    {
        string filter = "";
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ttcm_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ttcm_header");
            filter = "so_id=" + row["so_id"].ToString();
        }
        else
        {
            filter = "so_id=1234567890";
        }

        return filter;
    }

    private void UpdateDetailInformation()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ttcm_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ttcm_header");
            dm.SetCache("DocViewerCMID", (int)row["so_id"]);
        }
        else
        {
            dm.RemoveCache("DocViewerCMID");
        }
    }

    private string BuildDocumentSearchCriteria(string CreditMemoID)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string branch = "";
        string orderID = "";
        string sessionID = "";

        branch = "Branch=" + (string)dm.GetCache("currentBranchID");
        orderID = "TranID=" + CreditMemoID;
        sessionID = "SessionID=" + Session.SessionID.ToString().Trim();

        searchCriteria = "DocType=CreditMemo";
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + orderID;
        searchCriteria += "\x0003" + sessionID;

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        searchCriteria += "\x0003UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003Type=Credit Memo";

        return searchCriteria;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Call this to clear bound data
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    /// <param name="customer">Current Customer Object</param>
    public void Reset()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("dsCreditMemoDataSet");
        dm.RemoveCache("ttcm_header");
        dm.RemoveCache("CreditMemoSearchCriteria");
        this.AssignHeaderDataSource(true, "ttcm_header");

        dm = null;
    }

    /// <summary>
    /// Call this to clear bound data when the branch or customer has changed
    /// </summary>
    /// <param name="branchID">Current Branch ID</param>
    /// <param name="shiptoObj">Current Shipto Object</param>
    public void Reset(string branchID, string shiptoObj)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if ((dm.GetCache("currentBranchID") == null ||
            (dm.GetCache("currentBranchID") != null && (string)dm.GetCache("currentBranchID") != branchID)) ||
            (dm.GetCache("ShiptoRowForInformationPanel") == null ||
            (dm.GetCache("ShiptoRowForInformationPanel") != null && ((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["cust_shipto_obj"].ToString() != shiptoObj)))
        {
            this.Reset();
        }

        dm = null;
    }

    #endregion

    protected void btnFind_Click(object sender, EventArgs e)
    {
        ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
        pc.CMddlTranID_SelectedIndex = ddlTranID.SelectedIndex;
        pc.CMddlSearchStyle_SelectedIndex = ddlSearchStyle.SelectedIndex;
        pc.Save();

        ExecuteSearch();

        if (sender != null && e != null)
        {

            string analyticsLabel =
                                    "Search by = " + ddlTranID.SelectedItem + ", " +
                                    "Search Type = " + ddlSearchStyle.Text + ", " +
                                    "Status = " + ddlStatus.SelectedItem + ", " +
                                    "Show Results = " + this.ddlCreditMemoShowResultsBy.SelectedItem.Text + ", " +
                                    "Include all Ship-tos = " + this.cbShipTo.Checked.ToString() + ", " +
                                    "My credit memos only = " + this.cbMyCreditsOnly.Checked.ToString();

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Credit Memos - Search Button";
            aInput.Label = analyticsLabel;
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }
    }

    protected void gvCreditMemoHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }
                e.Row.Cells[0].ToolTip = "View";
            }
        }
    }

    public void AssignDetailDataSource(bool empty, GridView gvCreditMemoDetail, UserControls_DocViewer docViewer, string filter, string Soid)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsSalesOrderDataSet ds;

        if (docViewer != null)
        {
            if (dm.GetCache("ttcm_header") != null)
            {
                DataRow row = (DataRow)dm.GetCache("ttcm_header");

                docViewer.DocumentTitle = "Documents for ID " + row["so_id"].ToString();
                docViewer.Type = "CreditMemo";
                docViewer.TransactionID = (int)row["so_id"];
                docViewer.BranchID = (string)row["branch_id"];

                dm.SetCache("DocViewerCMID", (int)row["so_id"]);
            }
            else
            {
                docViewer.DocumentTitle = "Documents";
                docViewer.Type = "";
                docViewer.TransactionID = 0;
                dm.RemoveCache("DocViewerCMID");
            }

            docViewer.Refresh();
        }

        string sort = (string)dm.GetCache("gvCreditMemoDetailCustomeViewSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "ITEM";
        else
            sort += dm.GetCache("gvCreditMemoDetailCustomViewSortDirection");

        if (!this.IsPostBack || empty)
            ds = new dsSalesOrderDataSet();
        else
            ds = (dsSalesOrderDataSet)dm.GetCache("dsCreditMemoDataSet");

        // no fetch yet
        if (ds == null)
            ds = new dsSalesOrderDataSet();

        DataView dv = new DataView(ds.ttso_detail, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttcm_detailCopy"); // this will give us the correct column arrangement

        csCustomDetail.GridView = gvCreditMemoDetail;

        if (dt.Rows.Count > 0)
        {
            gvCreditMemoDetail.DataSource = dt;
            gvCreditMemoDetail.DataBind();
        }
        else
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gvCreditMemoDetail);
    }

    protected void gvCreditMemoHeader_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvCreditMemoHeaders.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "ttcm_header");
        this.gvCreditMemoHeaders.SelectedIndex = -1;

        //Empty detail data...
        Session["creditmemos_RowId"] = null;
    }

    protected void gvByItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvByItem.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "ttcm_byitem");
    }

    protected void gvCreditMemoHeader_SelectedIndexChanged(object sender, EventArgs e)
    {
        string myDataItem = "";
        GridView gv = (GridView)sender;

        myDataItem = gv.SelectedDataKey.Value.ToString();

        int ReturnCode = 0;
        string MessageText = "";
        Dmsi.Agility.Data.SalesOrders CreditMemoList = new Dmsi.Agility.Data.SalesOrders("dsCreditMemoDataSet", out ReturnCode, out MessageText);
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        DataRow[] CreditMemoRows = CreditMemoList.ReferenceHeaderTable.Select("so_id='" + myDataItem + "'");
        dm.SetCache("ttcm_header", CreditMemoRows[0]);

        CreditMemoList.Dispose();
        Session["creditmemos_RowId"] = null;
    }

    protected void ddlCreditMemoHeaderRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlCreditMemoHeaderRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        this.gvCreditMemoHeaders.PageSize = pageSize;
        this.gvByItem.PageSize = pageSize;

        if ((string)Session["ddlCreditMemoShowResultsBy"] == "Item")
        {
            this.AssignHeaderDataSource(false, "ttcm_byitem");
        }
        else
        {
            this.AssignHeaderDataSource(false, "ttcm_header");
        }

        Session["creditmemos_RowId"] = null;
    }

    protected void ddlCreditMemoDetailRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlCreditMemoDetailRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize;
        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        }

        DropDownList ddlcreditmemodetail = (sender as DropDownList);
        LinkButton lnkCrMemoDetailCustomView = (LinkButton)ddlcreditmemodetail.Parent.FindControl("lnkCrMemoDetailCustomView");
        int creditmemoid = Convert.ToInt32(lnkCrMemoDetailCustomView.CommandArgument);
        GridViewRow gr = (GridViewRow)gvCreditMemoHeaders.Rows[creditmemoid];
        GridView Gvcreditdetail = (GridView)gr.FindControl("gvCreditMemoDetailCustomView");
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("gvdetailsdocViewer");
        string myDataItem = "";
        myDataItem = gvCreditMemoHeaders.DataKeys[gr.RowIndex].Value.ToString();

        string filter = "so_id=" + myDataItem.ToString();

        Gvcreditdetail.PageSize = pageSize;
        AssignDetailDataSource(false, Gvcreditdetail, docViewer, filter, myDataItem);

        DropDownList ddlCreditMemoDetailRows = (DropDownList)gr.FindControl("ddlCreditMemoDetailRows");

        SizeHiddenRow(gr, ddlCreditMemoDetailRows, Gvcreditdetail);
    }

    private void csCustomHeader_OnOKClicked(CheckBoxList checkBoxList)
    {
        string resultsView = (string)Session["ddlCreditMemoShowResultsBy"] == null ? "Credit Memo" : (string)Session["ddlCreditMemoShowResultsBy"];

        if (resultsView == "Credit Memo")
        {
            string sbuildcolumns = BuildColumnVisibilityProperty(checkBoxList, gvCreditMemoHeaders);
            Profile.gvCreditMemoHeaders_Columns_Visible = "1" + sbuildcolumns;
            InitializeColumnVisibility(gvCreditMemoHeaders, "gvCreditMemoHeaders_Columns_Visible");
            this.AssignHeaderDataSource(false, "ttcm_header");
        }
        else
        {
            Profile.gvCreditMemoItems_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvByItem);
            this.AssignHeaderDataSource(false, "ttcm_byitem");
        }
    }

    private void csCustomDetail_OnOKClicked(CheckBoxList checkBoxList)
    {
        if (Session["creditmemos_RowId"] != null)
        {
            int irowid = Convert.ToInt32(Session["creditmemos_RowId"]);
            GridViewRow gvr = gvCreditMemoHeaders.Rows[irowid];
            GridView Gvcreditdetail = (GridView)gvr.FindControl("gvCreditMemoDetailCustomView");
            UserControls_DocViewer docViewer = (UserControls_DocViewer)gvr.FindControl("gvdetailsdocViewer");
            string myDataItem = gvCreditMemoHeaders.DataKeys[irowid].Value.ToString();
            string filter = "so_id=" + myDataItem.ToString();
            Profile.gvCreditMemoDetail_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, Gvcreditdetail);
            AssignDetailDataSource(false, Gvcreditdetail, docViewer, filter, myDataItem);
        }
    }

    private void csCustomDetail_OnResetClicked()
    {
        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvCreditMemoDetail_Columns_Visible"].DefaultValue.ToString();
        Profile.gvCreditMemoDetail_Columns_Visible = defaultValue;

        if (Session["creditmemos_RowId"] != null)
        {
            int irowid = Convert.ToInt32(Session["creditmemos_RowId"]);
            GridViewRow gvr = gvCreditMemoHeaders.Rows[irowid];
            GridView Gvcreditdetail = (GridView)gvr.FindControl("gvCreditMemoDetailCustomView");
            UserControls_DocViewer docViewer = (UserControls_DocViewer)gvr.FindControl("gvdetailsdocViewer");
            string myDataItem = gvCreditMemoHeaders.DataKeys[irowid].Value.ToString();
            string filter = "so_id=" + myDataItem.ToString();
            InitializeColumnVisibility(Gvcreditdetail, "gvCreditMemoDetail_Columns_Visible");
            AssignDetailDataSource(false, Gvcreditdetail, docViewer, filter, myDataItem);
        }
    }

    private void csCustomHeader_OnResetClicked()
    {
        string defaultValue;
        string resultsView = (string)Session["ddlCreditMemoShowResultsBy"] == null ? "Credit Memo" : (string)Session["ddlCreditMemoShowResultsBy"];

        switch (resultsView)
        {
            case "Item":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvCreditMemoItems_Columns_Visible"].DefaultValue.ToString();
                    Profile.gvCreditMemoItems_Columns_Visible = defaultValue;
                    InitializeColumnVisibility(gvByItem, "gvCreditMemoItems_Columns_Visible");

                    break;
                }

            case "Credit Memo":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvCreditMemoHeaders_Columns_Visible"].DefaultValue.ToString();
                    //need to add 1 because need to consider plus image button as one column
                    Profile.gvCreditMemoHeaders_Columns_Visible = defaultValue;
                    InitializeColumnVisibility(gvCreditMemoHeaders, "gvCreditMemoHeaders_Columns_Visible");

                    break;
                }
        }

        btnFind_Click(null, null);
    }

    protected void ExecuteSearch()
    {
        // save the current search criteria until a new find is initiated
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // This is needed when building the header caption inside HeaderBuildSearchCriteria
        if (this.ddlCreditMemoShowResultsBy.SelectedValue == "Item")
            dm.SetCache("ddlCreditMemoShowResultsBy", "Item");
        else
            dm.SetCache("ddlCreditMemoShowResultsBy", "Credit Memo");

        string searchCriteria = this.HeaderBuildSearchCriteria();

        if (searchCriteria == "Invalid input date.")
        {
            lblCreditMemoHeaderError.Visible = true;
            lblCreditMemoHeaderError.Text = searchCriteria;
            lblCreditMemoHeader.Text = "Credit Memos";
            return;
        }

        if (searchCriteria == "Invalid date range.")
        {
            lblCreditMemoHeaderError.Visible = true;
            lblCreditMemoHeaderError.Text = searchCriteria;
            lblCreditMemoHeader.Text = "Credit Memos";
            return;
        }

        if (searchCriteria == "")
        {
            lblCreditMemoHeaderError.Visible = true;
            lblCreditMemoHeaderError.Text = "Limit date range to 2 years or less.";
            lblCreditMemoHeader.Text = "Credit Memos";
            return;
        }

        dm.SetCache("CreditMemoSearchCriteria", searchCriteria);
        dm.RemoveCache("dsCreditMemoDataSet");
        dm.RemoveCache("DocViewerCMID");

        this.gvCreditMemoHeaders.SelectedIndex = -1;

        if (this.ddlCreditMemoShowResultsBy.SelectedValue == "Item")
            this.AssignHeaderDataSource(false, "ttcm_byitem");
        else
            this.AssignHeaderDataSource(false, "ttcm_header");
    }

    private string HeaderBuildSearchCriteria()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string shipto = "";
        string myorders = "";
        string shipToSequence = "";
        string searchType = "";
        string searchStyle = "";
        string searchTran = "";
        string customer = "";
        string branch = "";
        string status = "";
        string showResults = "";
        string startDateStr = "";
        string endDateStr = "";
        string gridTitle = "";
        DataRow shipToRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
        shipToSequence = shipToRow["seq_num"].ToString() + "";
        branch = "Branch=" + (string)dm.GetCache("currentBranchID");

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer currentCustomer = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        customer = "Customer=" + currentCustomer.CustomerKey;
        currentCustomer.Dispose();

        searchType = "SearchType=" + ddlTranID.SelectedValue;

        if (searchType.Contains("PO ID"))
            searchType = "SearchType=Customer PO #";

        searchStyle = "SearchStyle=" + ddlSearchStyle.Text;

        if (txtTranID.Text.Trim().Length == 0)
            searchTran = "SearchTran=<ALL>";
        else
            searchTran = "SearchTran=" + txtTranID.Text.Trim();

        if (ddlStatus.SelectedValue == "All")
            status = "Status=<ALL>";
        else
        {
            status = "Status=" + ddlStatus.SelectedValue;
            gridTitle += status + " ";
        }

        if (this.cbShipTo.Checked)
            shipto = "Ship To=<ALL>";
        else
        {
            shipto = "Ship To=" + shipToSequence;
            gridTitle += shipto + " ";
        }

        if (this.cbMyCreditsOnly.Checked)
            myorders = "MyOrdersOnly=YES";
        else
        {
            myorders = "MyOrdersOnly=NO";
            gridTitle += myorders + " ";
        }

        showResults = "Show Results=" + ddlCreditMemoShowResultsBy.SelectedValue;

        bool hasStartDate = false, hasEndDate = false;
        DateTime startDate = DateTime.Today.AddDays(-30), endDate = DateTime.Today;
        DateTimeValidator startDateValidator = new DateTimeValidator(this.txtFromDate.Text);
        DateTimeValidator endDateValidator = new DateTimeValidator(this.txtToDate.Text);

        hasStartDate = startDateValidator.InfoEntered();
        hasEndDate = endDateValidator.InfoEntered();

        if (hasStartDate && startDateValidator.ValidDate())
        {
            startDate = startDateValidator.ParsedDateTime();

            if (this.txtFromDate.Text != startDateValidator.FormattedDateTime())
            {
                this.txtFromDate.Text = startDateValidator.FormattedDateTime();
            }
        }
        else if (hasStartDate)
        {
            return startDateValidator.ValidationMessage();
        }

        if (hasEndDate && endDateValidator.ValidDate())
        {
            endDate = endDateValidator.ParsedDateTime();

            if (this.txtToDate.Text != endDateValidator.FormattedDateTime())
            {
                this.txtToDate.Text = endDateValidator.FormattedDateTime();
            }
        }
        else if (hasEndDate)
        {
            return endDateValidator.ValidationMessage();
        }

        startDateStr = "Start Date=" + (hasStartDate ? startDate.ToShortDateString() : "<ALL>");
        endDateStr = "End Date=" + (hasEndDate ? endDate.ToShortDateString() : "<ALL>");

        if (hasStartDate && hasEndDate)
        {
            if (startDate.AddYears(2) < endDate)
            {
                return "";
            }
            else if (startDate > endDate)
            {
                return "Invalid date range.";
            }
            else
                gridTitle = "Credit Memos from " + startDate.ToDMSiDateFormat() + " to " + endDate.ToDMSiDateFormat();
        }
        else if (hasStartDate)
        {
            if (startDate.AddYears(2) < endDate)
            {
                return "";
            }
            else
                gridTitle = "Credit Memos from " + startDate.ToDMSiDateFormat();
        }
        else if (hasEndDate)
        {
            gridTitle = "Credit Memos up to " + endDate.ToDMSiDateFormat();
            return "";
        }
        else
        {
            gridTitle = "Credit Memos";
            return "";
        }


        lblCreditMemoHeader.Text = gridTitle;
        lblCreditMemoHeader.Text.Trim();

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        searchCriteria = "UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + customer;

        searchCriteria += "\x0003" + shipto;
        searchCriteria += "\x0003" + myorders;
        searchCriteria += "\x0003" + status;
        searchCriteria += "\x0003" + searchType;
        searchCriteria += "\x0003" + searchStyle;
        searchCriteria += "\x0003" + searchTran;
        searchCriteria += "\x0003" + startDateStr;
        searchCriteria += "\x0003" + endDateStr;
        searchCriteria += "\x0003" + showResults;
        searchCriteria += "\x0003Type=Credit Memos";
        searchCriteria += "\x0003ExtendPrice=YES";

        if ((string)Session["ddlCreditMemoShowResultsBy"] == "Credit Memo")
            searchCriteria += "\x0003OrderTotals=YES";

        return searchCriteria;
    }

    protected void gvCreditMemoHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvCreditMemoHeaders, e.Row, "gvCreditMemoHeadersSortColumn", "gvCreditMemoHeadersSortDirection", 0);
    }

    protected void gvCreditMemoHeader_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvCreditMemoHeadersSortColumn"] != e.SortExpression)
            Session["gvCreditMemoHeadersSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvCreditMemoHeadersSortDirection"] == " ASC")
                Session["gvCreditMemoHeadersSortDirection"] = " DESC";
            else
                Session["gvCreditMemoHeadersSortDirection"] = " ASC";
        }

        gvCreditMemoHeaders.PageIndex = 0;
        Session["gvCreditMemoHeadersSortColumn"] = e.SortExpression;

        if ((string)Session["ddlCreditMemoShowResultsBy"] == "Item")
            this.AssignHeaderDataSource(false, "ttcm_byitem");
        else
            this.AssignHeaderDataSource(false, "ttcm_header");
    }

    protected void gvByItem_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvCreditMemoByItemSortColumn"] != e.SortExpression)
            Session["gvCreditMemoByItemSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvCreditMemoByItemSortDirection"] == " ASC")
                Session["gvCreditMemoByItemSortDirection"] = " DESC";
            else
                Session["gvCreditMemoByItemSortDirection"] = " ASC";
        }

        this.gvByItem.PageIndex = 0;
        Session["gvCreditMemoByItemSortColumn"] = e.SortExpression;
        this.AssignHeaderDataSource(false, "ttcm_byitem");
    }

    protected void gvByItem_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvByItem, e.Row, "gvCreditMemoByItemSortColumn", "gvCreditMemoByItemSortDirection", 0);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.SuppressContent = true;
    }

    protected void gvCreditMemoDetailCustomView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(sender as GridView, e.Row, "gvCreditMemoDetailCustomeViewSortColumn", "gvCreditMemoDetailCustomViewSortDirection", 0);
    }

    protected void gvCreditMemoDetailCustomView_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvCreditMemoDetailCustomeViewSortColumn"] != e.SortExpression)
            Session["gvCreditMemoDetailCustomViewSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvCreditMemoDetailCustomViewSortDirection"] == " ASC")
                Session["gvCreditMemoDetailCustomViewSortDirection"] = " DESC";
            else
                Session["gvCreditMemoDetailCustomViewSortDirection"] = " ASC";
        }

        Session["gvCreditMemoDetailCustomeViewSortColumn"] = e.SortExpression;

        GridView gv = (sender as GridView);
        LinkButton lnkCrMemoDetailCustomView = (LinkButton)gv.Parent.FindControl("lnkCrMemoDetailCustomView");

        GridViewRow gr = (lnkCrMemoDetailCustomView.NamingContainer as GridViewRow);
        int creditmemoid = Convert.ToInt32(gr.RowIndex);

        GridView Gvcreditdetail = (GridView)gr.FindControl("gvCreditMemoDetailCustomView");
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("gvdetailsdocViewer");
        string myDataItem = "";
        myDataItem = gvCreditMemoHeaders.DataKeys[gr.RowIndex].Value.ToString();

        string filter = "so_id=" + myDataItem.ToString();
        Gvcreditdetail.PageIndex = 0;
        AssignDetailDataSource(false, Gvcreditdetail, docViewer, filter, myDataItem);
    }

    protected void gvCreditMemoDetailCustomView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gv = (sender as GridView);
        LinkButton lnkCrMemoDetailCustomView = (LinkButton)gv.Parent.FindControl("lnkCrMemoDetailCustomView");
        GridViewRow gr = (lnkCrMemoDetailCustomView.NamingContainer as GridViewRow);
        int creditmemoid = Convert.ToInt32(gr.RowIndex);
        GridView Gvcreditdetail = (GridView)gr.FindControl("gvCreditMemoDetailCustomView");
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("gvdetailsdocViewer");
        string myDataItem = "";
        myDataItem = gvCreditMemoHeaders.DataKeys[gr.RowIndex].Value.ToString();

        string filter = "so_id=" + myDataItem.ToString();
        Gvcreditdetail.PageIndex = e.NewPageIndex;
        AssignDetailDataSource(false, Gvcreditdetail, docViewer, filter, myDataItem);

        DropDownList ddlCreditMemoDetailRows = (DropDownList)gr.FindControl("ddlCreditMemoDetailRows");

        SizeHiddenRow(gr, ddlCreditMemoDetailRows, Gvcreditdetail);
    }

    protected void lnkCrMemoDetailCustomView_Click(object sender, EventArgs e)
    {
        LinkButton lnkCrMemoDetailCustomView = (sender as LinkButton);
        GridViewRow grcreditmemo = (lnkCrMemoDetailCustomView.NamingContainer as GridViewRow);
        Session["creditmemos_RowId"] = grcreditmemo.RowIndex;

        ModalPopupExtender2.Show();
    }

    protected void Show_Hide_ChildGrid(object sender, EventArgs e)
    {
        this.gvCreditMemoHeaders.SelectedIndex = -1;

        ImageButton imgShowHide = (sender as ImageButton);
        imgShowHide.ToolTip = "View";
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        string commandname = imgShowHide.CommandName;
        int irowid = Convert.ToInt32(imgShowHide.CommandArgument);
        Session["creditmemos_RowId"] = row.RowIndex;

        for (int i = 0; i <= gvCreditMemoHeaders.Rows.Count - 1; i++)
        {
            GridViewRow gvr = (GridViewRow)gvCreditMemoHeaders.Rows[i];
            ImageButton imgbutton = (ImageButton)gvr.FindControl("imgbtnplusminus");

            if (i != row.RowIndex)
            {
                int inextrow = row.RowIndex;
                inextrow = inextrow + 1;

                if (i == inextrow)
                {
                    gvr.CssClass = row.CssClass = "activealt-row-tr";
                    this.gvCreditMemoHeaders.SelectedIndex = -1;
                }
                else
                {
                    gvr.CssClass = "";
                }

                gvr.FindControl("innergrids").Visible = false;
                gvr.FindControl("innerplaceholder").Visible = false;

                imgbutton.CommandName = "Details";
                imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                imgbutton.ToolTip = "View";
            }
            else
            {
                if (imgbutton.ImageUrl == "~/images/collapse-icon.png")
                {
                    gvr.FindControl("innergrids").Visible = false;
                    gvr.FindControl("innerplaceholder").Visible = false;

                    imgbutton.CommandName = "Hide";
                    imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                    imgbutton.ToolTip = "View";
                }
            }
        }

        if (commandname == "Details")
        {
            BindselectedInnerGrid(row);
            this.gvCreditMemoHeaders.SelectedIndex = irowid % this.gvCreditMemoHeaders.PageSize;
        }
        else
        {
            row.FindControl("innergrids").Visible = false;
            row.FindControl("innerplaceholder").Visible = false;

            imgShowHide.CommandName = "Details";
            imgShowHide.ImageUrl = "~/images/Grid_Plus.gif";
            imgShowHide.ToolTip = "View";
        }
    }

    protected void BindDocumentviewr(int irowid)
    {
        GridViewRow gr = gvCreditMemoHeaders.Rows[irowid];
        int creditmemoid = Convert.ToInt32(gr.RowIndex);

        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("gvdetailsdocViewer");

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (docViewer != null)
        {
            if (dm.GetCache("ttcm_header") != null)
            {
                DataRow row = (DataRow)dm.GetCache("ttcm_header");

                docViewer.DocumentTitle = "Documents for ID " + row["so_id"].ToString();
                docViewer.Type = "CreditMemo";
                docViewer.TransactionID = (int)row["so_id"];
                docViewer.BranchID = (string)row["branch_id"];

                dm.SetCache("DocViewerCMID", (int)row["so_id"]);
            }

            docViewer.Refresh();
        }
    }

    protected void BindselectedInnerGrid(GridViewRow row)
    {
        row.FindControl("innergrids").Visible = true;
        row.CssClass = "active-row-tr";
        DropDownList ddlCreditMemoDetailRows = (DropDownList)row.FindControl("ddlCreditMemoDetailRows");
        string myDataItem = "";
        myDataItem = gvCreditMemoHeaders.DataKeys[row.RowIndex].Value.ToString();
        Label lblQuoteInnerId = (Label)row.FindControl("lblCreditMemoDetail");
        lblQuoteInnerId.Text = "Credit Memo Details for ID   " + myDataItem;
        ImageButton imgShowHide = (ImageButton)row.FindControl("imgbtnplusminus");
        imgShowHide.CommandName = "Hide";
        imgShowHide.ImageUrl = "~/images/collapse-icon.png";

        GridViewRow gr = gvCreditMemoHeaders.Rows[row.RowIndex];
        gr.CssClass = "active-row-tr";

        ddlCreditMemoDetailRows.SelectedIndex = Profile.ddlCreditMemoDetailRows_SelectedIndex;

        GridView Gvcreditdetail = (GridView)gr.FindControl("gvCreditMemoDetailCustomView");

        int pageSize = 0;

        if (ddlCreditMemoDetailRows.SelectedValue == "All")
            pageSize = 32000;
        else
            pageSize = Convert.ToInt32(ddlCreditMemoDetailRows.SelectedValue);

        Gvcreditdetail.PageSize = pageSize;

        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("gvdetailsdocViewer");

        DataRowView rowView = (DataRowView)gr.DataItem;

        if (Gvcreditdetail != null)
        {
            string filter = "so_id=" + myDataItem.ToString();

            int ReturnCode = 0;
            string MessageText = "";
            Dmsi.Agility.Data.SalesOrders CreditMemoList = new Dmsi.Agility.Data.SalesOrders("dsCreditMemoDataSet", out ReturnCode, out MessageText);
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            DataRow[] CreditMemoRows = CreditMemoList.ReferenceHeaderTable.Select("so_id='" + myDataItem + "'");
            dm.SetCache("ttcm_header", CreditMemoRows[0]);

            InitializeColumnVisibility(Gvcreditdetail, "gvCreditMemoDetail_Columns_Visible");
            AssignDetailDataSource(false, Gvcreditdetail, docViewer, filter, myDataItem);

            SizeHiddenRow(row, ddlCreditMemoDetailRows, Gvcreditdetail);
        }
    }

    private static void SizeHiddenRow(GridViewRow row, DropDownList ddlCreditMemoDetailRows, GridView Gvcreditdetail)
    {
        UserControls_DocViewer docViewer = (UserControls_DocViewer)row.FindControl("gvdetailsdocViewer");

        int gvdocHeight = 0;

        GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

        if (gvdoc != null && gvdoc.Rows.Count > 0)
        {
            gvdocHeight = 42 + (gvdoc.Rows.Count * 42);
        }
        else
            gvdocHeight = -42;

        int detailrowcount = Gvcreditdetail.Rows.Count;

        if (ddlCreditMemoDetailRows.SelectedValue != "All")
        {
            if (detailrowcount > Convert.ToInt32(ddlCreditMemoDetailRows.SelectedValue))
            {
                detailrowcount = Convert.ToInt32(ddlCreditMemoDetailRows.SelectedValue);
            }
        }

        int pagerAddHeight = 0;

        if (Gvcreditdetail.PageCount > 1)
            pagerAddHeight = 56;

        int panelheight = 232 + gvdocHeight + (detailrowcount * 42) + pagerAddHeight;

        row.FindControl("innergrids").Visible = true;
        Panel p = row.FindControl("innerplaceholder") as Panel;
        p.Visible = true;
        p.Height = new Unit(panelheight, UnitType.Pixel);
    }

    protected void ddlTranID_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDdlSearchTypeValues(ddlTranID.Text);
    }

    private void SetDdlSearchTypeValues(string ddlTranIDText)
    {
        switch (ddlTranIDText)
        {
            case "Credit Memo ID":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.Items.Add(new ListItem("Starting at"));
                ddlSearchStyle.Enabled = true;
                break;

            case "Reference #":
            case "Item #":
            case "Job #":
            case "PO ID":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Beginning with"));
                ddlSearchStyle.Items.Add(new ListItem("Containing"));
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.Enabled = true;
                ddlSearchStyle.SelectedIndex = 2;
                break;

            case "Original Invoice #":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.Enabled = false;
                break;
        }
    }
}
