<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LogOn.ascx.cs" Inherits="UserControls_Login" %>
<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>




<div class="login">
    <div class="mainlogo">
        <img src="Images/DmsiBlue/topgen.png" />
        <div class="downarrow"></div>
    </div>

    <div class="login-form">

        <table>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblExtraMessage"
                        CssClass="FieldLabel"
                        runat="server"
                        ForeColor="Red" EnableViewState="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblUser" CssClass="loginslabel" runat="server">User name</asp:Label>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="textbox">
                        <span class="user-icon">&nbsp;</span>
                        <asp:TextBox ID="txtUserName" runat="server" EnableViewState="False" CssClass="text" TabIndex="1" ></asp:TextBox>



                    </div>
                    <asp:Label ID="lblUserName"
                        CssClass="Validator" runat="server" EnableViewState="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblPass" CssClass="loginslabel" runat="server">Password</asp:Label>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="textbox">
                        <span class="password-icon">&nbsp;</span>
                        <asp:TextBox ID="txtPassword" runat="server" EnableViewState="False" CssClass="text"
                            TextMode="Password" TabIndex="2" ></asp:TextBox>

                    </div>
                    <asp:Label ID="lblPassword" CssClass="Validator" runat="server"
                        EnableViewState="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>

                    <asp:CheckBox ID="cbxRememberUser" runat="server" CssClass="Field" Text="Remember user name" TabIndex="3" />


                </td>
                <td>
                    <asp:LinkButton ID="linkForgotPassword" TabIndex="4"
                        runat="server" EnableViewState="False" href="#" OnClientClick="onpopupfocus();"  data-toggle="modal">Forgot your password?</asp:LinkButton>


                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;">

                    <asp:Button ID="btnLogin" CssClass="loginsbuttonprimary" runat="server" EnableViewState="False"
                        Text="Login" OnClick="btnLogin_Click" TabIndex="6"></asp:Button>
                    <asp:Button ID="btnReset" Text="Reset" CssClass="loginsbuttonsecondary" runat="server"
                        EnableViewState="False" OnClick="btnReset_Click" TabIndex="5"></asp:Button>

                </td>


            </tr>
            <tr>
                <td>
                    <asp:SqlDataSource ID="SqlDataSourcePVDatabase" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT username FROM tbl_user WHERE (username = @username)">
                        <SelectParameters>
                            <asp:Parameter Name="userName" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>

            </tr>
        </table>


        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
    DropShadow="true" PopupControlID="pnlForgotPassword" 
    TargetControlID="linkForgotPassword">
</cc1:ModalPopupExtender>

<asp:Panel  ID="pnlForgotPassword" runat="server" style="display:none"  >
   

   
    <div class="modal-dialog" id="dvForgotPasswordHeader" runat="server" >
        <div class="modal-content" >
            <div class="modal-header">
               
                <h4 class="modal-title" id="myModalLabel">
                    <asp:Label ID="lblForgotPassword" runat="server" Text="Forgot your password?" Width="95%"></asp:Label></h4>
            </div>
            <div class="modal-body" >

                <table class="frgtchangepopup">
                    <tr>
                        <td>

                            <asp:Label ID="lblError" runat="server" CssClass="ErrorTextRed"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="forgotpasswordlabel">User name</label></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="textbox" style="width:569px;">
                                <span class="user-icon">&nbsp</span>
                                
                                <asp:TextBox ID="txtForgotUserName" focus-me="focusInput"  runat="server"   CssClass="forgotpasswordtext"  BorderStyle="Solid" ></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                </table>
                <p style=" line-height: 25px;">To reset your password, enter your user name and submit to receive email confirmation. </p>
            </div>
            <div class="modal-footer" style="width:100%;">
                <asp:Button ID="btnResetPassword" runat="server" TabIndex="9" Text="Submit Request" OnClick="btnResetPassword_Click" class="btn btn-primary canreq" />

                <asp:Button ID="btnCancelRequest" runat="server" Text="Cancel Request"  TabIndex="8"
                    class="btn btn-default" data-dismiss="modal" OnClick="btnCancelRequest_Click1" />


               
            </div>
             <div style="clear:both;"></div>
        </div>
    </div>
         
</asp:Panel>
    </div>
</div>



