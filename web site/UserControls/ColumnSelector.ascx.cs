using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Linq;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_ColumnSelector : System.Web.UI.UserControl
{
    Dmsi.Agility.EntryNET.DataManager _dm;

    public delegate void OKClickedDelegate(CheckBoxList checkBoxList);
    public event OKClickedDelegate OnOKClicked;
    
    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    private GridView _GridView;

    public GridView GridView
    {
        get
        {
            return _GridView;
        }
        set
        {
            _GridView = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _dm = new DataManager();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Type columnType;

        if (_GridView != null)
        {
            _GridView.AllowSorting = true;
            //Use this when Joe gets qtyAvailableDisplay fixed...
            dsSessionMgrDataSet dsSession = (dsSessionMgrDataSet)_dm.GetCache("dsSessionManager");
            DataRow[] rows = dsSession.ttSelectedProperty.Select("propertyName='qtyAvailableDisplay'");
            string availableTextDisplay = "";
            if (rows.Length == 1)
                availableTextDisplay = rows[0]["propertyValue"].ToString();

            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dm.GetCache("dsPV");
            if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
            {
                DataRow[] rows2 = dsPV.bPV_Action.Select("token_code='view_avail_inv'");
                if (rows2.Length == 1)
                    Session["Show_Available"] = true;
            }

            cblColumns.Items.Clear();
            foreach (DataControlField col in _GridView.Columns)
            {
                columnType = col.GetType();

                if (columnType.Name == "BoundField" || 
                    columnType.Name == "CheckBoxField")
                {
                    if (_GridView.ID == "gvInventory")
                    {
                        if (col.HeaderText != "" && 
                            col.HeaderText != "Item" && 
                            col.HeaderText != "Size" && 
                            col.HeaderText != "Description" && 
                            col.HeaderText != "Min Pack" &&
                            col.HeaderText != "Price" &&
                            col.HeaderText != "Retail Price" &&
                            col.HeaderText != "Price UOM")
                        {
                            if ((col.HeaderText == "Available" || col.HeaderText == "Qty Available" || col.HeaderText == "UOM"))
                            {
                                if (Session["Show_Available"] != null && (bool)Session["Show_Available"])
                                {
                                    //Use this when Joe gets qtyAvailableDisplay fixed...
                                    if (availableTextDisplay == "Text" && col.HeaderText == "Available")
                                        AddCheckBox(col);
                                    if (availableTextDisplay == "Quantity" && col.HeaderText == "Qty Available")
                                        AddCheckBox(col);
                                    if (col.HeaderText == "UOM")
                                    {
                                        if (availableTextDisplay == "Text")
                                            col.Visible = false;
                                        if (availableTextDisplay == "Quantity")
                                            col.Visible = true;
                                    }
                                }
                            }
                            else
                            {
                                AddCheckBox(col);
                            }
                        }
                    }
                    else if (_GridView.ID == "gvInventoryPricing")
                    {
                        if (col.HeaderText != "Price" &&
                            col.HeaderText != "Retail Price")
                        {
                            AddCheckBox(col);
                        }
                    }
                    else
                    {
                        AddCheckBox(col);
                    }
                }
            }
        }
    }

    private void AddCheckBox(DataControlField col)
    {
        ListItem li = new ListItem();
        li.Text = col.HeaderText;
        li.Selected = col.Visible;
        
        cblColumns.Items.Add(li);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (OnOKClicked != null)
            OnOKClicked(cblColumns); // will be caught by the parent
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }
}
