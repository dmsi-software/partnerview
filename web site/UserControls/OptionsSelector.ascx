<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OptionsSelector.ascx.cs"
    Inherits="UserControls_OptionsSelector" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<style type="text/css">
    .margintop {
        margin-top: 6px;
    }

    .center {
        margin-left: 97px !important;
        margin-right: 0px !important;
    }

    .centerless {
        margin-left: 48px !important;
        margin-right: auto;
    }

    .lineheight14 {
        line-height: 14px;
        padding: 4px;
    }

    .displayLabel {
        width: 84px;
        margin-right:6px;
    }

    .marginleft4 {
        margin-left: 4px !important;
    }

    .lineheight13em {
        line-height: 1.3em;
        font-size: 11px !important;
        white-space: nowrap !important;
    }

    .standardLabelSize {
        width: 90px;
        height: 30px;
    }

    .standardInputSize {
        width: 196px;
        height: 30px;
        margin: 2px 0px 2px 4px;
        padding: 5px 0px;
    }

    .warning {
        font-weight: bold;
        color: red;
    }

    .warningLabel {
        line-height: normal;
        margin-top: 12px;
    }

    .hidden {
        display: none;
    }
</style>
<script language="javascript" type="text/javascript">
    function GetControlIds(obj) {
        var sDropdownVal = document.getElementById('ctl00_OptionsSelector_cbxDisplay');
        var sDropdownVal_List = document.getElementById('ctl00_OptionsSelector_cbxDisplay_List');
        var scbxRetain = document.getElementById('ctl00_OptionsSelector_cbxRetain').checked;

        if (document.getElementById('ctl00_OptionsSelector_numericMarkupFactor') != null)
            var snumericMarkupFactor = document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value;

        if (document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List') != null)
            var snumericMarkupFactor = document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').value;

        if (sDropdownVal != null)
            var sv = sDropdownVal.options[sDropdownVal.selectedIndex].value;

        if (sDropdownVal_List != null)
            var sv = sDropdownVal_List.options[sDropdownVal_List.selectedIndex].value;

        var tableExists = document.getElementById('ctl00_cphMaster1_MainTabControl1_Inventory1_lvProducts_ctrl0');
        var sVal = obj.toString() + '+' + sv.toString() + '+' + scbxRetain.toString() + '+' + snumericMarkupFactor.toString();

        PageMethods.GetControlid(sVal);
    };

    function CheckIfSellingAtCost() {
        var PriceMode = document.getElementById('hdfMode').value;

        //reset warning indications
        $('div[id$="MarkupWarning"]').addClass('hidden');
        $('div[id$="MarginWarning"]').addClass('hidden');
        $('span[id$="SellAtCostIndicator"]').addClass('hidden');

        //only show indicators if in any of the three retail modes
        if (PriceMode != 'list' && PriceMode != 'net') {
            var lblMult = $('label[name$="lblMultiplier"]:not([style*="display: none"])');
            var cbxMult = $('select[name$="cbxMarkupToggle"]:not([style*="display: none"])');

            if (cbxMult.length && cbxMult[0].value == 'Multiplier') {
                if ($('input[name$="numericMarkupFactor"]')[0].value <= 1) {
                    $('div[id$="MarkupWarning"]').removeClass('hidden');
                    $('span[id$="SellAtCostIndicator"]').removeClass('hidden');
                }
            }
            else if (cbxMult.length && cbxMult[0].value == 'Margin')
            {
                if ($('input[name$="numericMarginFactor"]')[0].value <= 0) {
                    $('div[id$="MarginWarning"]').removeClass('hidden');
                    $('span[id$="SellAtCostIndicator"]').removeClass('hidden');
                }
            }
        }
    }
</script>

<script language="javascript" type="text/javascript">
    function CbxDisplayToggle(ddlId) {
        var ControlName = document.getElementById(ddlId.id);
        var PriceMode = document.getElementById('hdfMode');

        if (ControlName.value == 'Net price') {
            document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblMultiplier').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = 'none';

            PriceMode.value = "net";
        }
        else {
            document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = '';
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblMultiplier').style.display = 'none';

            PriceMode.value = "cost";

            var oMarkupToggle = document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle');
            if (oMarkupToggle.value == 'Multiplier') {
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = '';
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = 'none';

                document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = '';
                document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = 'none';
            }
            else {
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = '';

                document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = '';
            }
        }

        CheckIfSellingAtCost();
    }
</script>

<script language="javascript" type="text/javascript">
    function HideWidgets(ddlId) {
        var ControlName = document.getElementById(ddlId.id);
        var PriceMode = document.getElementById('hdfMode');

        PriceMode.value = "cost";

        if (ControlName.value == 'Net price') {
            document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblMultiplier').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = 'none';
            PriceMode.value = "net";
        }

        if (ControlName.value == 'Retail price - markup on cost') {
            document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = '';
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblMultiplier').style.display = 'none';

            PriceMode.value = "cost";

            var oMarkupToggle = document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle');
            if (oMarkupToggle.value == 'Multiplier') {
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = '';
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = 'none';

                document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = '';
                document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = 'none';
            }
            else {
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = '';

                document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
                document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = '';
            }
        }

        if (ControlName.value == 'Retail price - discount from list') {
            document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = '';

            document.getElementById('ctl00_OptionsSelector_cbxMarkupToggle').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblMultiplier').style.display = '';

            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = 'none';

            PriceMode.value = "list";
        }

        CheckIfSellingAtCost();
    }
</script>

<script language="javascript" type="text/javascript">
    function ToggleMarkupSetting(ddlId) {
        var ControlName = document.getElementById(ddlId.id);

        if (ControlName.value == 'Multiplier') {
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = '';
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = '';
            document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = 'none';

            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value = (1 / (1 - (document.getElementById('ctl00_OptionsSelector_numericMarginFactor').value / 100))).toFixed(4);

            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').focus();
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').blur();
        }
        else {
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_numericMarkupFactor_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').style.display = '';

            document.getElementById('ctl00_OptionsSelector_lblRetail').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblRetail_List').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample1').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample2').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblExample3').style.display = 'none';
            document.getElementById('ctl00_OptionsSelector_lblMargin').style.display = '';

            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').value = (((document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value - 1) / (1 + (document.getElementById('ctl00_OptionsSelector_numericMarkupFactor').value - 1))) * 100).toFixed(0);

            if (document.getElementById('ctl00_OptionsSelector_numericMarginFactor').value > 99) {
                document.getElementById('ctl00_OptionsSelector_numericMarginFactor').value = 99;
            }

            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').focus();
            document.getElementById('ctl00_OptionsSelector_numericMarginFactor').blur();
        }
        CheckIfSellingAtCost();
    }
</script>

<div class="modal-content" style="display: block; width: 350px">
    <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">User Preferences</h4>
    </div>
    <div class="modal-body">
        <asp:HiddenField ID="hdfMode" runat="server" ClientIDMode="static" />

        <table cellpadding="0" cellspacing="2">
            <tr>
                <td colspan="2" valign="top" style="width: 100%; text-align: left">
                    <asp:Panel ID="pnlScroller" runat="server" CssClass="BorderedContainer "
                        ScrollBars="None" DefaultButton="btnOK">
                        <h4 class="modal-title" style="padding-bottom: 20px;">Pricing Display Settings</h4>
                        <table border="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblPreferenceLabel" runat="server" Text="Display" CssClass="preferencelabel displayLabel"></asp:Label>
                                </td>
                                <td>
                                    <span id="spanNonListModeOptions" runat="server" style="height: 1px; width: 1px; padding-left:4px;">
                                        <asp:DropDownList ID="cbxDisplay" runat="server" Width="210px" CssClass="input igte_Edit" Height="30px" onchange="CbxDisplayToggle(this);">
                                            <asp:ListItem>Net price</asp:ListItem>
                                            <asp:ListItem>Retail price</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <span id="spanListModeOptions" runat="server" style="height: 1px; width: 1px; padding-left:4px;">
                                        <asp:DropDownList ID="cbxDisplay_List" runat="server" Width="210px" CssClass="input igte_Edit" Height="30px" onchange="HideWidgets(this);">
                                            <asp:ListItem>Net price</asp:ListItem>
                                            <asp:ListItem>Retail price - markup on cost</asp:ListItem>
                                            <asp:ListItem>Retail price - discount from list</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:CheckBox ID="cbxRetain" runat="server" Text="Retain display setting" CssClass="editcheckbox userprefrence" TabIndex="2" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 90px !important;">
                                    <span style="height: 1px; width: 1px">
                                        <asp:Label ID="lblMultiplier" runat="server" Width="74px" CssClass="preferencelabel" Text="Multiplier"></asp:Label>
                                    </span>
                                    <span style="height: 1px; width: 1px">
                                        <asp:DropDownList ID="cbxMarkupToggle" runat="server" Width="84px" CssClass="input igte_Edit" Height="30px" TabIndex="3" onchange="ToggleMarkupSetting(this)">
                                            <asp:ListItem>Multiplier</asp:ListItem>
                                            <asp:ListItem>Margin</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                </td>
                                <td>
                                    <ig:WebNumericEditor ID="numericMarkupFactor" runat="server" DataMode="Decimal" MaxDecimalPlaces="4"
                                        MinValue="1" Nullable="False" CssClass="textarea editQuotemultiplier marginleft4" Width="210px"
                                        Height="30px" TabIndex="3" NullText="1" NullValue="1" HideEnterKey="True" MaxLength="9"
                                        ClientEvents-ValueChanged="CheckIfSellingAtCost" ClientEvents-Initialize="CheckIfSellingAtCost">
                                    </ig:WebNumericEditor>
                                    <ig:WebNumericEditor ID="numericMarkupFactor_List" runat="server" DataMode="Decimal" MaxDecimalPlaces="4"
                                        MinValue="0.0001" Nullable="False" CssClass="textarea editQuotemultiplier marginleft4" Width="210px" Height="30px"
                                        ValueText="1" TabIndex="3" HideEnterKey="True" NullValue="1" NullText="1" MaxLength="9" MaxValue="1">
                                    </ig:WebNumericEditor>
                                    <ig:WebNumericEditor ID="numericMarginFactor" runat="server" DataMode="Int" MaxDecimalPlaces="0"
                                        MinValue="0" MaxValue="99" Nullable="False" CssClass="textarea editQuotemultiplier marginleft4" 
                                        Width="210px" Height="30px" TabIndex="3" HideEnterKey="True" NullValue="0" NullText="0" MaxLength="2"
                                        ClientEvents-ValueChanged="CheckIfSellingAtCost" >
                                    </ig:WebNumericEditor>
                                    <span id="SellAtCostIndicator" runat="server" class="hidden warning">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblRetail" runat="server" Text="Retail price = Multiplier * Net price"
                                        CssClass="center"></asp:Label>
                                    <asp:Label ID="lblRetail_List" runat="server" Text="Retail price = Multiplier * List price"
                                        CssClass="center"></asp:Label>
                                    <asp:Label ID="lblExample1" runat="server" Text="<i>Example:&nbsp;To&nbsp;give&nbsp;a&nbsp;25%&nbsp;discount,</i><br/>"
                                        CssClass="center lineheight13em"></asp:Label>
                                    <asp:Label ID="lblExample2" runat="server" Text="<i>enter&nbsp;.75&nbsp;as&nbsp;a&nbsp;multiplier.</i>"
                                        CssClass="center lineheight13em"></asp:Label>
                                    <asp:Label ID="lblExample3" runat="server" Text="<i>$100&nbsp;*&nbsp;(.75)&nbsp;=&nbsp;$75</i>"
                                        CssClass="center lineheight13em"></asp:Label>
                                    <asp:Label ID="lblMargin" runat="server" Text="Retail price = Net price / (1 - (Margin% / 100))" CssClass="centerless">
                                    </asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <div id="PriceModeDescriptions">
            <asp:Label ID="lblDescForNetPrice" runat="server" Text="Net price � dealer cost from the distributor<br />"></asp:Label>
            <asp:Label ID="lblDescForRetailPrice" runat="server" Text="Retail price � price the dealer offers their customer<br />"></asp:Label>
            <asp:Label ID="lblDescForCostMarkup" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;Markup on cost � markup from the net price<br />"></asp:Label>
            <asp:Label ID="lblDescForListDiscount" runat="server" Text="&nbsp;&nbsp;&nbsp;&nbsp;Discount from list � discount off of the item�s list price<br />"></asp:Label>
        </div>
        <div id="TaxSection">
            <asp:Label ID="lblDefaultTaxRate" runat="server" Text="Default tax rate %" CssClass="standardLabelSize"></asp:Label>
            <ig:WebNumericEditor ID="numericDefaultTaxRate" runat="server" DataMode="Decimal" MaxDecimalPlaces="4" MinValue="0.0000" MaxValue="100.0000"
                Nullable="True" CssClass="textarea standardInputSize" HideEnterKey="True" NullText="A value between 0.0000 and 100.0000" MaxLength="8" TabIndex="4">
            </ig:WebNumericEditor>
        </div>
        <h4 class="modal-title" style="padding-bottom: 10px;">Retail Quote Form Logo URL</h4>
        <ig:WebTextEditor ID="txtURL" runat="server" NullText="Logo image size: 256 x 80 pixels                                    Image output format: .jpg                                                 Image URL on a server accessible from the Internet.       See Help for additional information."
            Width="318px" Height="70px" TabIndex="5" BorderWidth="1px" EnableTheming="False" MaxLength="1000" TextMode="MultiLine" MultiLine-Rows="1" CssClass="lineheight14">
        </ig:WebTextEditor>
        <div id="MarkupWarning" runat="server" class="hidden warning warningLabel">Please verify the markup multiplier.  The current retail price is set to your cost.</div>
        <div id="MarginWarning" runat="server" class="hidden warning warningLabel">Please verify the margin value.  The current retail price is set to your cost.</div>
    </div>
    <div class="modal-footer" style="width: 100%">
        <div style="float: right">
            <asp:Button ID="btnOK" runat="server" Text="OK" class="btn btn-primary" OnClick="btnOK_Click" OnClientClick="javascript:return GetPriceChangeControlIds(this.id)" TabIndex="7" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-default" OnClick="btnCancel_Click" OnClientClick="javascript:return GetPriceChangeControlIds(this.id)" TabIndex="6" />
        </div>
    </div>
    <div style="clear: both"></div>
</div>






