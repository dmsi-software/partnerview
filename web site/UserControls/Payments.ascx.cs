﻿using com.elementexpress.certreporting;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Infragistics.Web.UI.GridControls;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

public partial class UserControls_Payments : System.Web.UI.UserControl
{
    private dspvarDataSet _ds;
    private dsCustBankDataSet _dsCustBank;
    private dspvarDataSet _newDs;
    private dspvarDataSet _boundDs;

    private string _filterString;
    private string _checkedIDs;
    private string[] _checkedValues;

    decimal _fixedAmountToPay = 0;
    decimal _CCSurcharge = 0;

    com.elementexpress.certtransaction.Express _expressService;
    com.elementexpress.certtransaction.Application _application;
    com.elementexpress.certtransaction.Credentials _credentials;
    com.elementexpress.certtransaction.Response _response;
    com.elementexpress.certtransaction.Terminal _terminal;
    com.elementexpress.certtransaction.Transaction _transaction;
    com.elementexpress.certtransaction.TransactionSetup _transactionSetup;
    com.elementexpress.certtransaction.Address _address;

    com.elementexpress.certreporting.Response _reportingResponse;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["_filterString"] != null)
            _filterString = (string)Session["_filterString"];

        if (!IsPostBack)
        {
            btnLumpSumPayment.Visible = false;
            Session["FirstLoad"] = true;

            btnApplyCredits.Enabled = false;
        }

        txtTranID.ForeColor = Color.Black;
        txtFromDate.ForeColor = Color.Black;
        txtToDate.ForeColor = Color.Black;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("dspvarDataSet") != null)
        {
            _ds = (dspvarDataSet)dm.GetCache("dspvarDataSet");
        }

        if (Session["WebHierarchicalDataGridDetail_DataSource"] != null)
        {
            _boundDs = (dspvarDataSet)Session["WebHierarchicalDataGridDetail_DataSource"];
            Session["WebHierarchicalDataGridDetail_DataSource"] = null;
            SetGridBindingProperties(_boundDs);
        }

        if (HiddenFieldCCSurchargeTotal.Value != "")
        {
            _CCSurcharge = Convert.ToDecimal(HiddenFieldCCSurchargeTotal.Value.Replace("$", "").Replace(",", ""));
        }

        if (HiddenField.Value != "")
        {
            _checkedIDs = HiddenField.Value;

            _checkedValues = HiddenField2.Value.Split(new string[1] { "," }, StringSplitOptions.None);

            decimal grandTotal = 0;

            for (int x = 0; x < _checkedValues.Length; x++)
                grandTotal += Convert.ToDecimal(_checkedValues[x]);

            grandTotal = grandTotal + _CCSurcharge;

            lblGrandTotal.Text = "$" + grandTotal.ToString("N2");
            lblSurchargeTotal.Text = "$" + _CCSurcharge.ToString("N2");
            btnPay.Enabled = true;
        }
        else
        {
            lblGrandTotal.Text = "$0.00";
            lblSurchargeTotal.Text = "$0.00";
        }
            

        if (IsPostBack)
        {
            WebHierarchicalDataGridDetail.EnsureTemplates();
            WebHierarchicalDataGridDetail.GridView.EnsureTemplates();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["ElementHealthCheck"] == null && Session["apply_payments"] != null && (bool)Session["apply_payments"] == true)
        {
            DataManager tempdm = new DataManager();
            AgilityEntryNetUser eUser = tempdm.GetCache("AgilityEntryNetUser") as AgilityEntryNetUser;

            string serviceUrl = "";

            try
            {
                serviceUrl = eUser.GetContextValue("TransactionURL");
            }
            catch (System.Exception ex)
            {
                if (ex.Message.ToLower().Contains("not found"))
                {
                    Session["ElementHealthCheck"] = true;
                    Session["apply_payments"] = null;
                    return;
                }
            }

            if (serviceUrl == "")
            {
                Session["ElementHealthCheck"] = true;
                Session["apply_payments"] = null;
                return;
            }

            if (serviceUrl.ToLower().StartsWith("http://"))
                serviceUrl = serviceUrl.Substring(7);

            if (serviceUrl.ToLower().StartsWith("https://"))
                serviceUrl = serviceUrl.Substring(8);

            serviceUrl = "https://" + serviceUrl;

            if (!serviceUrl.ToLower().EndsWith("/express.asmx"))
                serviceUrl += "/express.asmx";

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _expressService = new com.elementexpress.certtransaction.Express();
            _expressService.Url = serviceUrl;

            _application = new com.elementexpress.certtransaction.Application();
            _credentials = new com.elementexpress.certtransaction.Credentials();

            _application.ApplicationID = "5332";
            _application.ApplicationName = "PartnerView";
            _application.ApplicationVersion = Application["AgilityVersion"].ToString().Replace("_", ".");

            _credentials.AccountID = eUser.GetContextValue("AccountID");
            _credentials.AccountToken = eUser.GetContextValue("AccountToken");
            _credentials.AcceptorID = eUser.GetContextValue("AcceptorID");

            try
            {
                _response = _expressService.HealthCheck(_credentials, _application);

                if (_response.ExpressResponseCode != "0" || _response.ExpressResponseMessage != "ONLINE")
                {
                    Session["apply_payments"] = null;
                    Dmsi.Agility.EntryNET.EventLog log = new EventLog();
                    log.LogError(EventTypeFlag.ApplicationEvent, new Exception(DateTime.Now.ToString() + ": " + "A failure occurred while calling Element's HealthCheck SOAP Service; ExpressResponseCode is " + _response.ExpressResponseCode + "; ExpressResponseMessage is " + _response.ExpressResponseMessage + "."));
                }
                else
                    Session["ElementHealthCheck"] = true;

                //throw new ArgumentException("Fake Element License Failure. --MDM");
            }
            catch (SystemException ex)
            {
                Session["apply_payments"] = null;
                Dmsi.Agility.EntryNET.EventLog log = new EventLog();
                log.LogError(EventTypeFlag.UnhandledException, ex);
            }
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (this.Visible && dm.GetCache("dspvarDataSet") == null)
        {
            DataFetch();
        }

        if (HiddenField3.Value != "Sorting")
        {
            if (Session["DetailGridHasPaged"] != null && (bool)Session["DetailGridHasPaged"] == true)
            {
                Session["DetailGridHasPaged"] = null;
            }
            else
            {
                if (_filterString != null && _filterString != "")
                {
                    _newDs = new dspvarDataSet();

                    if ((bool)Session["ShowACHForBilling"] == true && rdoACH.Checked)
                        _ds = (dspvarDataSet)((dspvarDataSet)Session["dspvarBackupDataSet"]).Copy();

                    if (_filterString.ToLower() != "all")
                    {
                        DataRow[] rows = _ds.Tables["ttarbalance"].Select("aging_label='" + _filterString + "'");

                        foreach (DataRow row in rows)
                            _newDs.Tables["ttarbalance"].LoadDataRow(row.ItemArray, false);
                    }
                    else
                    {
                        foreach (DataRow row in _ds.ttarbalance.Rows)
                            _newDs.Tables["ttarbalance"].LoadDataRow(row.ItemArray, false);
                    }

                    foreach (DataRow agingrow in _ds.ttaginginfo.Rows)
                        _newDs.Tables["ttaginginfo"].LoadDataRow(agingrow.ItemArray, false);

                    BindAllDataButDetailGrid(_newDs);
                    SetGridBindingProperties(_newDs);
                }
                else
                {
                    BindAllDataButDetailGrid(_ds);
                    SetGridBindingProperties(_ds);
                }
            }
        }
        else
        {
            BindAllDataButDetailGrid(_ds);
        }

        if (Session["DisplayAvailDiscounts"] != null && ((string)Session["DisplayAvailDiscounts"]).Trim().ToLower() == "no")
        {
            WebHierarchicalDataGridDetail.GridView.Columns["open_amt"].Hidden = true;
            WebHierarchicalDataGridDetail.GridView.Columns["discount_amt"].Hidden = true;

            WebHierarchicalDataGridDetail.Columns["open_amt"].Hidden = true;
            WebHierarchicalDataGridDetail.Columns["discount_amt"].Hidden = true;
        }

        WebHierarchicalDataGridDetail.GridView.Columns["show_pdf"].Hidden = true;
        WebHierarchicalDataGridDetail.GridView.Columns["branch"].Hidden = true;
        WebHierarchicalDataGridDetail.GridView.Columns["ship_num"].Hidden = true;
        WebHierarchicalDataGridDetail.GridView.Columns["tran_id"].Hidden = true;

        WebHierarchicalDataGridDetail.Columns["show_pdf"].Hidden = true;
        WebHierarchicalDataGridDetail.Columns["branch"].Hidden = true;
        WebHierarchicalDataGridDetail.Columns["ship_num"].Hidden = true;
        WebHierarchicalDataGridDetail.Columns["tran_id"].Hidden = true;

        int count_of_rows = ((dspvarDataSet)WebHierarchicalDataGridDetail.DataSource).Tables["ttarbalance"].Rows.Count;

        //MDM - Dec. 2015 Leave this here for now; likely will implement paging at a later date.
        //      The code below hides the pager if all records fit on one page; else show the pager; via CSS.
        //if (count_of_rows <= 1000)
        //{
        //    WebHierarchicalDataGridDetail.Behaviors.Paging.PagerCssClass = "hideexpansioncolumn";
        //    WebHierarchicalDataGridDetail.GridView.Behaviors.Paging.PagerCssClass = "hideexpansioncolumn";
        //}
        //else
        //{
        //    WebHierarchicalDataGridDetail.Behaviors.Paging.PagerCssClass = "";
        //    WebHierarchicalDataGridDetail.GridView.Behaviors.Paging.PagerCssClass = "";
        //}

        if (HiddenField4.Value == "forward")
        {
            HtmlImage im = this.BillingFlyoutImage as HtmlImage;
            im.Src = "~/Images/forward.png";

            BillingLeftSection.Attributes["class"] = "left-section left-section-hidden";
        }

        if (HiddenField4.Value == "back")
        {
            HtmlImage im = this.BillingFlyoutImage as HtmlImage;
            im.Src = "~/Images/back.png";

            BillingLeftSection.Attributes["class"] = "left-section";
        }

        //handle security
        if (HiddenField3.Value != "Sorting")
        {
            if (Session["apply_payments"] != null && (bool)Session["apply_payments"] == true)
            {
                //top level security is good; now check for create_cash_on_acct
                btnLumpSumPayment.Visible = LumpSumPaymentButtonVisible();
            }
            else
            {
                divFloatingPay.Visible = false;
                btnLumpSumPayment.Visible = false;
                WebHierarchicalDataGridDetail.Columns["PayThis"].Hidden = true;
                WebHierarchicalDataGridDetail.GridView.Columns["PayThis"].Hidden = true;
                WebHierarchicalDataGridDetail.Columns["payment_amt"].Hidden = true;
                WebHierarchicalDataGridDetail.GridView.Columns["payment_amt"].Hidden = true;
            }
        }

        if (Session["ShowPayForBilling"] != null && (bool)Session["ShowPayForBilling"] == false && Session["allow_ach_payments"] != null && ((string)Session["allow_ach_payments"]).ToLower() == "no")
        {
            divFloatingPay.Visible = false;
            btnLumpSumPayment.Visible = false;
            WebHierarchicalDataGridDetail.Columns["PayThis"].Hidden = true;
            WebHierarchicalDataGridDetail.GridView.Columns["PayThis"].Hidden = true;
            WebHierarchicalDataGridDetail.Columns["payment_amt"].Hidden = true;
            WebHierarchicalDataGridDetail.GridView.Columns["payment_amt"].Hidden = true;
        }

        if (Session["ShowPayForBilling"] != null && (bool)Session["ShowPayForBilling"] == true)
        {
            btnPay.Visible = true;
            lblDisclaimer.Visible = true;
            btnFixedPayCC.Visible = true;
        }
        else
        {
            btnPay.Visible = false;
            lblDisclaimer.Visible = false;
            btnFixedPayCC.Visible = false;
        }

        if (Session["allow_ach_payments"] != null && ((string)Session["allow_ach_payments"]).ToLower() == "yes" && Session["ShowACHForBilling"] != null && (bool)Session["ShowACHForBilling"] == true)
        {
            btnPayBank.Visible = true;
            Session["btnPayBank.Visible"] = true;

            btnFixedPayACH.Visible = true;
        }
        else
        {
            btnPayBank.Visible = false;
            Session["btnPayBank.Visible"] = false;

            btnFixedPayACH.Visible = false;
        }

        if (btnPay.Visible == false && btnPayBank.Visible == false && (bool)Session["btnPayBank.Visible"] == false)
        {
            divFloatingPay.Visible = false;
            btnLumpSumPayment.Visible = false;
            WebHierarchicalDataGridDetail.Columns["PayThis"].Hidden = true;
            WebHierarchicalDataGridDetail.GridView.Columns["PayThis"].Hidden = true;
            WebHierarchicalDataGridDetail.Columns["payment_amt"].Hidden = true;
            WebHierarchicalDataGridDetail.GridView.Columns["payment_amt"].Hidden = true;
        }

        if (btnPay.Visible == true || btnPayBank.Visible == true || SafeSessionGet("btnPayBank.Visible",true))
        {
            divFloatingPay.Visible = true;
            btnLumpSumPayment.Visible = LumpSumPaymentButtonVisible();
            WebHierarchicalDataGridDetail.Columns["PayThis"].Hidden = false;
            WebHierarchicalDataGridDetail.GridView.Columns["PayThis"].Hidden = false;
            WebHierarchicalDataGridDetail.Columns["payment_amt"].Hidden = false;
            WebHierarchicalDataGridDetail.GridView.Columns["payment_amt"].Hidden = false;
        }

        if (Session["view_realtime_invoice"] == null || !((bool)Session["view_realtime_invoice"] == true))
        {
            WebHierarchicalDataGridDetail.Columns["document"].Hidden = true;
            WebHierarchicalDataGridDetail.GridView.Columns["document"].Hidden = true;
        }

        foreach (GridRecord record in WebHierarchicalDataGridDetail.Rows)
        {
            record.Items.FindItemByKey("payment_amt").CssClass = "";
        }

        if (HiddenField.Value.Length > 0)
        {
            string[] invoiceNums = HiddenField.Value.Split(',');
            for (int x = 0; x < invoiceNums.Length; x++)
            {
                foreach (GridRecord record in WebHierarchicalDataGridDetail.Rows)
                {
                    string invoice_num = (string)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["invoice_num"]);
                    string[] splitOnPipe = invoiceNums[x].Split('|');
                    string refType = (string)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["ref_type"]);

                    if (invoice_num == splitOnPipe[0])
                    {
                        GridRecordItem it = record.Items.FindItemByKey("PayThis");
                        if (it != null)
                        {
                            it.Value = true;

                            if (!(refType == "CA" || refType == "CM" || refType == "CI"))
                                (record.Items.FindItemByKey("payment_amt")).CssClass = "yellowbackground";
                        }
                    }
                }
            }
        }

        decimal grTotal = 0;

        if (lblGrandTotal.Text.Contains("("))
        {
            grTotal = 0 - Convert.ToDecimal(lblGrandTotal.Text.Replace("$", "").Replace(",", "").Replace("(", "").Replace(")", ""));
        }
        else
            grTotal = Convert.ToDecimal(lblGrandTotal.Text.Replace("$", "").Replace(",", ""));

        if (HiddenField3.Value == "Sorting" && HiddenField.Value.Length > 0 && grTotal <= 0)
        {
            btnLumpSumPayment.Enabled = false;
            btnPayBank.Enabled = false;
            btnPay.Enabled = false;
            btnApplyCredits.Enabled = false;
        }

        if (HiddenField3.Value == "Sorting" && HiddenField.Value.Length > 0 && grTotal > 0)
        {
            btnLumpSumPayment.Enabled = false;
            btnPayBank.Enabled = true;
            btnPay.Enabled = true;
            btnApplyCredits.Enabled = false;
        }

        if (HiddenField3.Value == "Sorting" && HiddenField.Value.Length == 0 && grTotal == 0)
        {
            btnLumpSumPayment.Enabled = true;
            btnPayBank.Enabled = false;
            btnPay.Enabled = false;
            btnApplyCredits.Enabled = false;
        }

        if (HiddenField3.Value == "Sorting" && HiddenField.Value.Length > 0 && grTotal == 0)
        {
            btnApplyCredits.Enabled = true;
            btnPayBank.Enabled = false;
            btnPay.Enabled = false;
            btnLumpSumPayment.Enabled = false;
        }

        if (HiddenField3.Value == "Sorting")
            HiddenField3.Value = "";

        if (Session["apply_open_acct_credits_to_invoices"] == null)
            btnApplyCredits.Visible = false;

        //MDM - new rules for "105716 - PV - Remove Discount for CC pmts - Divide UI into 2 separate payment flows"
        if (Session["allow_ach_payments"] != null && ((string)Session["allow_ach_payments"]).ToLower().Trim() == "no")
            divFetchType.Visible = false;
        else if ((bool)Session["ShowACHForBilling"] == false || (bool)Session["ShowPayForBilling"] == false)
            divFetchType.Visible = false;
        else
            divFetchType.Visible = true;

        if (divFetchType.Visible)
        {
            if (rdoCC.Checked)
            {
                btnPayBank.Visible = false;
                btnFixedPayACH.Visible = false;

                btnPay.Visible = true;
                lblDisclaimer.Visible = true;
                btnFixedPayCC.Visible = true;

                if (Session["UsingCCSurcharge"] != null && (bool)Session["UsingCCSurcharge"] == true)
                {
                    lblSurchargeTotalLabel.Visible = true;
                    lblSurchargeTotal.Visible = true;
                    tblSurcharge.Visible = true;
                }
            }

            if (rdoACH.Checked)
            {
                btnPayBank.Visible = true;
                btnFixedPayACH.Visible = true;

                btnPay.Visible = false;
                lblDisclaimer.Visible = false;
                btnFixedPayCC.Visible = false;

                lblSurchargeTotalLabel.Visible = false;
                lblSurchargeTotal.Visible = false;
                tblSurcharge.Visible = false;
            }
        }
        //MDM - End of new rules for 105716...

        //MDM - PureChat code
        if (dm.GetCache("ChatScript") != null)
        {
            if (divFloatingPay.Attributes.CssStyle.Keys.Count == 0)
                divFloatingPay.Attributes.Add("style", "height: 112px");
        }
    }

    #region Events

    protected void btnFind_Click(object sender, EventArgs e)
    {
        if (ValidSearchFields())
        {
            string idSearch = "";

            if (txtTranID.Text != "")
            {
                if (ddlTranID.SelectedIndex == 0)
                    idSearch += "invoice_num Like '*";
                else if (ddlTranID.SelectedIndex == 1)
                    idSearch += "cust_po Like '*";
                else if (ddlTranID.SelectedIndex == 2)
                    idSearch += "job Like '*";

                idSearch += txtTranID.Text + "*'";
            }

            string dateSearch = "";

            if (txtFromDate.Text != "" || txtToDate.Text != "")
            {
                if (this.ddlDateChoice.SelectedIndex == 0)
                    dateSearch += "due_date";
                else
                    dateSearch += "invoice_date";
            }

            if (txtFromDate.Text != "" && txtToDate.Text == "")
            {
                dateSearch += ">=#" + txtFromDate.Text + "# AND " + dateSearch + "<=#" + (DateTime.Now).AddYears(84).ToShortDateString() + "#";
            }
            else if (txtFromDate.Text == "" && txtToDate.Text != "")
            {
                dateSearch += ">=#1/1/1900# AND " + dateSearch + "<=#" + txtToDate.Text + "#";
            }
            else if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                dateSearch += ">=#" + txtFromDate.Text + "# AND " + dateSearch + "<=#" + txtToDate.Text + "#";
            }

            string finalSearch = "";

            if (idSearch == "" && dateSearch != "")
            {
                finalSearch = dateSearch;
            }
            else if (idSearch != "" && dateSearch == "")
            {
                finalSearch = idSearch;
            }
            else if (idSearch != "" && dateSearch != "")
            {
                finalSearch = idSearch + " AND (" + dateSearch + ")";
            }

            _newDs = new dspvarDataSet();
            _newDs = _ds.Copy() as dspvarDataSet;
            _newDs.Tables["ttarbalance"].Rows.Clear();
            _newDs.AcceptChanges();

            dspvarDataSet cachedDS = (dspvarDataSet)Session["WebHierarchicalDataGridDetail_DataSource"];
            DataRow[] rows = _ds.Tables["ttarbalance"].Select(finalSearch);
            if (rows.Length > 0)
            {
                foreach (DataRow row in rows)
                {
                    _newDs.Tables["ttarbalance"].LoadDataRow(row.ItemArray, true);
                }
            }

            _newDs.AcceptChanges();

            _filterString = "";
            Session["_filterString"] = null;

            WebDataGridAging.Behaviors.Selection.SelectedRows.Clear();
            _ds = _newDs.Copy() as dspvarDataSet;

            ResetFields();

            lblAccountDetail.Text = "Account Detail";
            this.WebDataGridAging.Behaviors.Selection.SelectedRows.Clear();

            if (sender != null && e != null)
            {

                string analyticsLabel = "Search by = " + ddlTranID.SelectedItem + ", " +
                                        "Date Option = " + this.ddlDateChoice.SelectedItem.ToString();

                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type   = "event";
                aInput.Action = "Billing - Search Button";
                aInput.Label  = analyticsLabel;
                aInput.Value  = "0";

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

            }

        }
    }

    protected void btnStartOver_Click(object sender, EventArgs e)
    {
        rdoCC.Checked = true;
        rdoACH.Checked = false;

        _filterString = "";
        Session["_filterString"] = null;

        ResetSearchFields();

        WebDataGridAging.Behaviors.Selection.SelectedRows.Clear();

        DataFetch();

        lblAccountDetail.Text = "Account Detail: All";

        Session["FirstLoad"] = true; //MDM - to enable All row selction in aging grid.

        if (sender != null && e != null)
        {
            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Start Over Button";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }
            
    }

    protected void btnPay_Click(object sender, EventArgs e)
    {
        if (sender != null && e != null)
        {
            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Pay Via Credit Card Button";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

        ElementTransActionSetup(false);
    }

    protected void btnPayBank_Click(object sender, EventArgs e)
    {
        Session["ACHFixedPayment"] = null;
        ACHPaymentSetup(false);
    }

    protected void btnFixedPayACH_Click(object sender, EventArgs e)
    {
        Session["ACHFixedPayment"] = true;
        ACHPaymentSetup(true);

        if (sender != null && e != null)
        {

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Payment on Account - Pay Via Bank Account Button";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

    }
    
    protected void btnExitElement_Click(object sender, EventArgs e)
    {
         if (sender != null && e != null)
         {

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Credit Card Payment Screen - Close Button";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }
        
        bool IsFixedAmount = false;

        if (Session["IsFixedAmount"] != null && (bool)Session["IsFixedAmount"] == true)
            IsFixedAmount = true;

        Session["IsFixedAmount"] = null;

        bool success = ElementReportingServiceCall(IsFixedAmount);
    }

    protected void btnACHPay_Click(object sender, EventArgs e)
    {
        if (Session["ACHFixedPayment"] == null)
            ACHPay(false);
        else
        {
            Session["ACHFixedPayment"] = null;
            ACHPay(true);
        }

        if (sender != null && e != null)
        {

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Billing - Bank Account Screen - Pay Button";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

    }

    protected void btnACHCancel_Click(object sender, EventArgs e)
    {
        ResetFields();
        DataFetch();

        if (sender != null && e != null)
        {
            Button b = sender as Button;
              
            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Label  = "";
            aInput.Value  = "0";

            if (b.ID == "btnACHCancel")
            {
                aInput.Action = "Billing - Bank on Account - Cancel Button";
            }
            else
            {
                aInput.Action = "Billing - Payment on Account - Cancel Button";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }
    }

    protected void btnLumpSumPayment_Click(object sender, EventArgs e)
    {
        if (sender != null && e != null)
        {
            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Payment On Account Button Click";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

        mpeCC.Enabled = false;
        mpeACH.Enabled = false;

        wneFixedAmount.Text = "";

        mpeFixedPayment.Enabled = true;
        mpeFixedPayment.Show();
    }

    protected void btnFixedPayCC_Click(object sender, EventArgs e)
    {
        _fixedAmountToPay = Convert.ToDecimal(wneFixedAmount.Text) + _CCSurcharge;

        ElementTransActionSetup(true);
        Session["IsFixedAmount"] = true;

        if (sender != null && e != null)
        {

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Payment on Account - Pay Via Credit Card Button";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }
    }

    protected void btnApplyCredits_Click(object sender, EventArgs e)
    {
        bool success = this.StartPayment("", false, 0, "Net Zero");
        btnApplyCredits.Enabled = false;
        ResetFields();

        DataFetch();

        if (sender != null && e != null)
        {
            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Apply Credits Button Click";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        } 
    
}

    protected void rdo_CheckedChanged(object sender, EventArgs e)
    {
        if (rdoCC.Checked)
        {
            if ((bool)Session["allow_disc_when_cc"] == false && (bool)Session["ShowPayForBilling"] == true)
            {
                dspvarDataSet localDs = (dspvarDataSet)((dspvarDataSet)Session["dspvarBackupDataSet"]).Copy();

                foreach (DataRow row in localDs.ttarbalance.Rows)
                {
                    if ((decimal)row["discount_amt"] > 0)
                    {
                        row["discount_amt"] = 0;
                        row["balance_due"] = row["open_amt"];
                    }
                }

                localDs.AcceptChanges();

                _ds = localDs;
            }

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Pay Toggle - Credit Card";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        if (rdoACH.Checked)
        {
            _ds = (dspvarDataSet)((dspvarDataSet)Session["dspvarBackupDataSet"]).Copy();

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Billing - Pay Toggle - Bank Account";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        //if (sender == null) //aging grid row click event calls this way
        //    ResetFields(); 
        //else
        //    ResetSearchFields(); //'real' rdo Check change

        ResetFields();

        if (txtTranID.Text.Length > 0 || txtFromDate.Text.Length > 0 || txtToDate.Text.Length > 0)
            btnFind_Click(null, null); //'real' rdo Check change
    }

    #endregion

    #region Data Methods

    private void DataFetch()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dspvarDataSet ARDataSet;
        Dmsi.Agility.EntryNET.StrongTypesNS.dsCustBankDataSet CustBankDataSet;

        DataRow custRow;
        if (dm.GetCache("CustomerRowForInformationPanel") != null)
            custRow = (DataRow)dm.GetCache("CustomerRowForInformationPanel");
        else
            return;

        DataRow shiptoRow;
        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
            shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
        else
            return;

        string custCode = (string)custRow["cust_code"];
        int shipto = (int)shiptoRow["Seq_num"];

        string SearchCriteria = "invoice_num=\x0003cust_po=\x0003invoice_date=\x0003due_date=\x0003";
        SearchCriteria += "cust_id=" + custCode + "\x0003shipto_seq=" + shipto.ToString();
        SearchCriteria += "\x0003cc_surcharge_branch=" + (string)Session["currentBranchID"];

        int ReturnCode;
        string MessageText;

        AccountsReceivable oAccountsReceivable = new AccountsReceivable(SearchCriteria, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        } 

        Session["ShowPayForBilling"] = oAccountsReceivable.ShowPay;
        Session["ShowACHForBilling"] = oAccountsReceivable.ShowACH;

        ARDataSet = (Dmsi.Agility.EntryNET.StrongTypesNS.dspvarDataSet)oAccountsReceivable.ReferencedDataSet;

        //for (int x = 0; x < ARDataSet.ttarbalance.Rows.Count; x++)
        //{
        //    string invoicenum = ARDataSet.ttarbalance.Rows[x]["invoice_num"] as string;
        //    int refnumseq = Convert.ToInt32(ARDataSet.ttarbalance.Rows[x]["ref_num_seq"]);

        //    for (int y = 0; y < ARDataSet.ttarbalance.Rows.Count; y++)
        //    {
        //        if (ARDataSet.ttarbalance.Rows[y]["invoice_num"] as string == invoicenum &&
        //            x != y && y > x &&
        //            Convert.ToInt32(ARDataSet.ttarbalance.Rows[y]["ref_num_seq"]) == refnumseq
        //           )
        //        {
        //            string body = Request.UserHostName + "\r\n";
        //            body += "PartnerView current user = " + ((AgilityEntryNetUser)Session["AgilityEntryNetUser"]).GetContextValue("currentUserLogin") + "\r\n";
        //            body += "\r\n";

        //            body += "First record:" + "\r\n";
        //            body += "invoice_num              = " + (string)ARDataSet.ttarbalance.Rows[x]["invoice_num"] + "\r\n";
        //            body += "ref_num_seq              = " + Convert.ToInt32(ARDataSet.ttarbalance.Rows[x]["ref_num_seq"]) + "\r\n";
        //            body += "ref_num_sysid            = " + (string)ARDataSet.ttarbalance.Rows[x]["ref_num_sysid"] + "\r\n";
        //            body += "prof_name                = " + (string)ARDataSet.ttarbalance.Rows[x]["prof_name"] + "\r\n";
        //            body += "cust_key_sysid           = " + (string)ARDataSet.ttarbalance.Rows[x]["cust_key_sysid"] + "\r\n";
        //            body += "cust_key                 = " + Convert.ToInt32(ARDataSet.ttarbalance.Rows[x]["cust_key"]) + "\r\n";
        //            body += "ref_type                 = " + (string)ARDataSet.ttarbalance.Rows[x]["ref_type"] + "\r\n";
        //            body += "\r\n";
        //            body += "Second record:" + "\r\n";
        //            body += "invoice_num              = " + (string)ARDataSet.ttarbalance.Rows[y]["invoice_num"] + "\r\n";
        //            body += "ref_num_seq              = " + Convert.ToInt32(ARDataSet.ttarbalance.Rows[y]["ref_num_seq"]) + "\r\n";
        //            body += "ref_num_sysid            = " + (string)ARDataSet.ttarbalance.Rows[y]["ref_num_sysid"] + "\r\n";
        //            body += "prof_name                = " + (string)ARDataSet.ttarbalance.Rows[y]["prof_name"] + "\r\n";
        //            body += "cust_key_sysid           = " + (string)ARDataSet.ttarbalance.Rows[y]["cust_key_sysid"] + "\r\n";
        //            body += "cust_key                 = " + Convert.ToInt32(ARDataSet.ttarbalance.Rows[y]["cust_key"]) + "\r\n";
        //            body += "ref_type                 = " + (string)ARDataSet.ttarbalance.Rows[y]["ref_type"] + "\r\n";

        //            try
        //            {
        //                //MikeMcAllister.Mail oMail = new MikeMcAllister.Mail();
        //                //oMail.MailMike("partnerview@dmsi.com", "ehaifley@dmsi.com", "PartnerView code detected duplicate keys in ttarbalance table; invoice_num and ref_num_seq are not unique.", body, new System.Exception("PartnerView ttarbalance duplicate keys of invoice_num and ref_num_seq"));
        //            }
        //            catch { }
        //        }
        //    }
        //}

        DataColumn[] key = new DataColumn[3];
        key[0] = ARDataSet.Tables["ttarbalance"].Columns["invoice_num"];
        key[1] = ARDataSet.Tables["ttarbalance"].Columns["ref_num_seq"];
        key[2] = ARDataSet.Tables["ttarbalance"].Columns["ref_num_sysid"];

        ARDataSet.Tables["ttarbalance"].PrimaryKey = key;
        ARDataSet.AcceptChanges();

        dm.SetCache("dspvarBackupDataSet", ARDataSet.Copy());

        if (Session["allow_disc_when_cc"] != null && (bool)Session["allow_disc_when_cc"] == false && (bool)Session["ShowPayForBilling"] == true)
        {
            foreach (DataRow row in ARDataSet.ttarbalance.Rows)
            {
                if ((decimal)row["discount_amt"] > 0)
                {
                    row["discount_amt"] = 0;
                    row["balance_due"] = row["open_amt"];
                }
            }

            ARDataSet.AcceptChanges();
        }

        dm.SetCache("dspvarDataSet", ARDataSet);
        _ds = (dspvarDataSet)dm.GetCache("dspvarDataSet");

        CustBankDataSet = (Dmsi.Agility.EntryNET.StrongTypesNS.dsCustBankDataSet)oAccountsReceivable.ReferencedCustBankDataSet;
        dm.SetCache("dsCustBankDataSet", CustBankDataSet);
        _dsCustBank = (dsCustBankDataSet)dm.GetCache("dsCustBankDataSet");

        ResetSearchFields();

        if (ARDataSet.Tables["ttarbalance"].Rows.Count > 0)
        {
            if ((bool)ARDataSet.Tables["ttarbalance"].Rows[0]["apply_surcharge"] == true)
            {
                Session["UsingCCSurcharge"] = true;
                lblSurchargeTotal.Visible = true;
                lblSurchargeTotalLabel.Visible = true;
                tblSurcharge.Visible = true;

                HiddenFieldSurchargeUsage.Value = Convert.ToString(ARDataSet.Tables["ttarbalance"].Rows[0]["apply_surcharge"]);
                HiddenFieldSurchargeType.Value  = Convert.ToString(ARDataSet.Tables["ttarbalance"].Rows[0]["surcharge_basis_type"]);
                HiddenFieldSurchargeValue.Value = Convert.ToString(ARDataSet.Tables["ttarbalance"].Rows[0]["surcharge_basis_amt"]);
            }
            else
            {
                Session["UsingCCSurcharge"] = false;
                lblSurchargeTotal.Visible = false;
                lblSurchargeTotalLabel.Visible = false;
                tblSurcharge.Visible = false;

                HiddenFieldSurchargeUsage.Value = "";
                HiddenFieldSurchargeType.Value  = "";
                HiddenFieldSurchargeValue.Value = "";
            }
        }
        else
        {
            Session["UsingCCSurcharge"] = false;
            lblSurchargeTotal.Visible = false;
            lblSurchargeTotalLabel.Visible = false;
            tblSurcharge.Visible = false;

            HiddenFieldSurchargeUsage.Value = "";
            HiddenFieldSurchargeType.Value  = "";
            HiddenFieldSurchargeValue.Value = "";
        }

        WebHierarchicalDataGridDetail.GridView.Behaviors.Sorting.SortedColumns.Clear();
        WebHierarchicalDataGridDetail.GridView.Behaviors.Sorting.SortedColumns.Add("invoice_num", Infragistics.Web.UI.SortDirection.Ascending);
    }

    private void SetGridBindingProperties(dspvarDataSet ds)
    {
        DataColumn[] key = new DataColumn[3];
        key[0] = ds.Tables["ttarbalance"].Columns["invoice_num"];
        key[1] = ds.Tables["ttarbalance"].Columns["ref_num_seq"];
        key[2] = ds.Tables["ttarbalance"].Columns["ref_num_sysid"];

        ds.Tables["ttarbalance"].PrimaryKey = key;

        if (Session["ResetFields"] != null)
        {
            Session["ResetFields"] = null;
            foreach (DataRow row in ds.ttarbalance.Rows)
            {
                row["payment_amt"] = 0.00;
            }
        }

        ds.AcceptChanges();

        WebHierarchicalDataGridDetail.DataSource = null;
        WebHierarchicalDataGridDetail.DataSourceID = "";
        WebHierarchicalDataGridDetail.GridView.ClearDataSource();
        WebHierarchicalDataGridDetail.DataKeyFields = "invoice_num,ref_num_seq";
        WebHierarchicalDataGridDetail.DataMember = "ttarbalance";
        Session["WebHierarchicalDataGridDetail_DataSource"] = ds;
        WebHierarchicalDataGridDetail.DataSource = ds;
    }

    private void BindAllDataButDetailGrid(dspvarDataSet localds)
    {
        if (Session["FirstLoad"] != null && (bool)Session["FirstLoad"] == true)
        {
            WebDataGridAging.Behaviors.Selection.SelectedRows.Clear();
        }

        //small aging grid
        WebDataGridAging.DataSource = localds;
        WebDataGridAging.DataMember = "ttaginginfo";
        WebDataGridAging.DataBind();
    }

    #endregion

    #region WebDataGridAging Events

    protected void WebDataGridAging_InitializeRow(object sender, RowEventArgs e)
    {
        if (e.Row.Items[0].Text.ToLower() == "all")
        {
            SelectedRowCollection selectedRows = this.WebDataGridAging.Behaviors.Selection.SelectedRows;

            if (selectedRows.Count == 0)
            {
                if (Session["FirstLoad"] != null)
                {
                    Session["FirstLoad"] = null;
                    selectedRows.Add(e.Row);
                }
            }
            else
            {
                _filterString = selectedRows[0].Items[0].Text;
                Session["_filterString"] = _filterString;
                lblAccountDetail.Text = "Account Detail: " + _filterString;
            }
        }
    }

    protected void WebDataGridAging_RowSelectionChanged(object sender, SelectedRowEventArgs e)
    {
        IDPair p = e.CurrentSelectedRows.GetIDPair(0);

        object o;
        p.TryGetValue("index", out o);
        Int32 index = Convert.ToInt32(o);
        _filterString =_filterString = (string)_ds.ttaginginfo.Rows[index]["aging_label"];
        Session["_filterString"] = _filterString;

        txtTranID.Text = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";

        ResetFields();

        rdo_CheckedChanged(null, null);
    }

    #endregion

    #region WebHierarchicalDataGridDetail Events

    protected void WebHierarchicalDataGridDetail_DataBound(object sender, EventArgs e)
    {
        foreach (GridRecord record in WebHierarchicalDataGridDetail.Rows)
        {
            string refType = "";

            refType = (string)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["ref_type"]);

            bool show_pdf = (bool)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["show_pdf"]);
            string branch = (string)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["branch"]);
            Int32 ship_num = (Int32)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["ship_num"]);
            string invoice_num = (string)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["invoice_num"]);
            decimal balanceDue = (decimal)(((DataRowView)((Infragistics.Web.UI.Framework.Data.DataSetNode)(record.DataItem)).Item)["balance_due"]);

            GridRecordItem it = record.Items.FindItemByKey("PayThis");
            if (it != null)
            {
                if (Session["apply_open_acct_credits_to_invoices"] != null && (bool)Session["apply_open_acct_credits_to_invoices"] == true && (refType == "CA" || refType == "CM" || refType == "CI"))
                    it.CssClass = "";
                else if (balanceDue > 0 && (refType == "IN" || refType == "FC" || refType == "DM"))
                    it.CssClass = "";
                else
                    it.CssClass = "notvisible";

                if (balanceDue == 0)
                    it.CssClass = "notvisible";
            }

            GridRecordItem gri = record.Items.FindItemByKey("document");
            if (gri != null)
            {
                Label lblPDF = gri.FindControl("lblPDF") as Label;
                if (show_pdf)
                {
                    lblPDF.Visible = true;
                    string pdfURL = "PdfViewer.aspx?ProfName=" + branch + "&TranID=" + invoice_num.ToString() + "&ShipmentNum=" + ship_num.ToString() + "&TranType=Billing&DocStorageReportName=Invoice";
                    lblPDF.Text = "<a href=\"" + pdfURL + "\" target=\"_blank\"><img border=0 src=\"Images/pdf_icon.gif\"></a>";
                    lblPDF.ToolTip = "View PDF";
                }
                else
                    lblPDF.Visible = false;

                if (Session["view_realtime_invoice"] == null || (bool)Session["view_realtime_invoice"] == false)
                    lblPDF.Visible = false;
            }
        }
    }

    #endregion

    #region private/utility methods

    private void ResetSearchFields()
    {
        ddlDateChoice.SelectedIndex = 0;
        ddlTranID.SelectedIndex = 0;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtTranID.Text = "";

        ResetFields();
    }

    private void ResetFields()
    {
        HiddenField.Value = "";
        HiddenField2.Value = "";
        lblGrandTotal.Text = "$0.00";
        lblSurchargeTotal.Text = "$0.00";
        btnPay.Enabled = false;
        btnPayBank.Enabled = false;
        ddlBankAccount.SelectedIndex = 0;
        wneFixedAmount.Text = "";
        btnLumpSumPayment.Enabled = true;

        Session["ResetFields"] = true;
    }

    private bool ValidSearchFields()
    {
        bool valid = true;

        if (txtFromDate.Text.Trim() == "" && txtToDate.Text.Trim() == "" && txtTranID.Text.Trim() == "")
            valid = false;

        if (txtTranID.Text.Length > 0 && txtTranID.Text.Trim() == "")
        {
            txtTranID.Text = "Invalid Input";
            txtTranID.ForeColor = Color.Red;
            valid = false;
        }

        if (txtTranID.Text.Contains("*"))
        {
            txtTranID.Text = "* not valid character";
            txtTranID.ForeColor = Color.Red;
            valid = false;
        }

        if (txtTranID.Text.Contains("%"))
        {
            txtTranID.Text = "% not valid character";
            txtTranID.ForeColor = Color.Red;
            valid = false;
        }

        if (txtFromDate.Text.Length > 0 && txtFromDate.Text.Trim() == "")
        {
            txtFromDate.Text = "Invalid Date";
            txtFromDate.ForeColor = Color.Red;
            valid = false;
        }

        if (txtToDate.Text.Length > 0 && txtToDate.Text.Trim() == "")
        {
            txtToDate.Text = "Invalid Date";
            txtToDate.ForeColor = Color.Red;
            valid = false;
        }

        if (txtFromDate.Text.Trim() != "")
        {
            try
            {
                Convert.ToDateTime(txtFromDate.Text);
            }
            catch
            {
                valid = false;
                txtFromDate.Text = "Invalid Date";
                txtFromDate.ForeColor = Color.Red;
            }
        }

        if (txtToDate.Text.Trim() != "")
        {
            try
            {
                Convert.ToDateTime(txtToDate.Text);
            }
            catch
            {
                valid = false;
                txtToDate.Text = "Invalid Date";
                txtToDate.ForeColor = Color.Red;
            }
        }

        return valid;
    }

    private void ACHPaymentSetup(bool IsFixedAmount)
    {
        string totalToPay = "Amount: ";

        if (IsFixedAmount)
            totalToPay += "$" + wneFixedAmount.Text.Trim();
        else
            totalToPay += lblGrandTotal.Text;

        this.lblBankTotal.Text = totalToPay;

        //TODO: parse checked grid rows and build list of available bank accounts.
        dspvarDataSet localDs = (dspvarDataSet)WebHierarchicalDataGridDetail.DataSource;
        dspvarDataSet paymentDs = new dspvarDataSet();

        dsCustBankDataSet dsCustBank = (dsCustBankDataSet)Session["dsCustBankDataSet"];

        for (int x = ddlBankAccount.Items.Count - 1; x > 0; x--)
            ddlBankAccount.Items.RemoveAt(x);

        if (!IsFixedAmount)
        {
            string[] transactions = HiddenField.Value.Split(',');

            for (int x = 0; x < transactions.Length; x++)
            {
                string[] transplit = transactions[x].Split('|');
                DataRow[] rows = localDs.Tables["ttarbalance"].Select("invoice_num='" + transplit[0] + "' AND ref_num_seq = " + transplit[1] + " AND ref_num_sysid = '" + transplit[2] + "'");

                if (rows.Length == 1)
                {
                    paymentDs.Tables["ttarbalance"].LoadDataRow(rows[0].ItemArray, true);
                }
            }
        }

        for (int i = 0; i < dsCustBank.ttcust_bank.Rows.Count; i++)
        {
            bool foundBank = false;
            foreach (ListItem li in ddlBankAccount.Items)
            {
                if (li.Value == (string)dsCustBank.ttcust_bank.Rows[i]["cust_bank_guid"])
                {
                    foundBank = true;
                }
            }

            if (!foundBank)
            {
                ListItem newLi = new ListItem(Server.HtmlDecode(((string)dsCustBank.ttcust_bank.Rows[i]["acct_string"]).Replace("     ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")), (string)dsCustBank.ttcust_bank.Rows[i]["cust_bank_guid"]);
                ddlBankAccount.Items.Add(newLi);
            }
        }

        paymentDs.AcceptChanges();

        Session["paymentDataSet"] = paymentDs;

        if (ddlBankAccount.Items.Count == 1)
        {
            lblNoBanks.Visible = true;
            ddlBankAccount.Enabled = false;
        }
        else
        {
            lblNoBanks.Visible = false;
            ddlBankAccount.Enabled = true;
        }

        this.mpeCC.Enabled = false;
        this.mpeFixedPayment.Enabled = false;
        this.mpeACH.Enabled = true; 
        this.mpeACH.Show();
    }

    private void ElementTransActionSetup(bool IsFixedAmount)
    {
        DataManager tempdm = new DataManager();
        AgilityEntryNetUser eUser = tempdm.GetCache("AgilityEntryNetUser") as AgilityEntryNetUser;

        string serviceUrl = eUser.GetContextValue("TransactionURL");

        if (serviceUrl.ToLower().StartsWith("http://"))
            serviceUrl = serviceUrl.Substring(7);

        if (serviceUrl.ToLower().StartsWith("https://"))
            serviceUrl = serviceUrl.Substring(8);

        serviceUrl = "https://" + serviceUrl;

        if (!serviceUrl.ToLower().EndsWith("/express.asmx"))
            serviceUrl += "/express.asmx";

        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        _expressService = new com.elementexpress.certtransaction.Express();
        _expressService.Url = serviceUrl;
        _application = new com.elementexpress.certtransaction.Application();
        _credentials = new com.elementexpress.certtransaction.Credentials();

        _application.ApplicationID = "5332";
        _application.ApplicationName = eUser.GetContextValue("ApplicationName");
        _application.ApplicationVersion = Application["AgilityVersion"].ToString().Replace("_", ".");

        _credentials.AccountID = eUser.GetContextValue("AccountID");
        _credentials.AccountToken = eUser.GetContextValue("AccountToken");
        _credentials.AcceptorID = eUser.GetContextValue("AcceptorID");

        _terminal = new com.elementexpress.certtransaction.Terminal();
        _terminal.TerminalID = eUser.GetContextValue("TerminalID");
        _terminal.CardPresentCode = com.elementexpress.certtransaction.CardPresentCode.UseDefault;
        _terminal.CardholderPresentCode = com.elementexpress.certtransaction.CardholderPresentCode.ECommerce;
        _terminal.CardInputCode = com.elementexpress.certtransaction.CardInputCode.ManualKeyed;
        _terminal.CVVPresenceCode = com.elementexpress.certtransaction.CVVPresenceCode.UseDefault;
        _terminal.TerminalType = com.elementexpress.certtransaction.TerminalType.ECommerce;
        _terminal.TerminalCapabilityCode = com.elementexpress.certtransaction.TerminalCapabilityCode.KeyEntered;
        _terminal.TerminalEnvironmentCode = com.elementexpress.certtransaction.TerminalEnvironmentCode.ECommerce;
        _terminal.MotoECICode = com.elementexpress.certtransaction.MotoECICode.NonAuthenticatedSecureECommerceTransaction;

        _transaction = new com.elementexpress.certtransaction.Transaction();
        _transaction.MarketCode = com.elementexpress.certtransaction.MarketCode.ECommerce;
        _transaction.TicketNumber = "0";
        _transaction.CommercialCardCustomerCode = "1";

        DataManager dm = new DataManager();

        if (!IsFixedAmount)
        {
            _transaction.TransactionAmount = this.lblGrandTotal.Text.Replace("$", "").Replace(",", "").Replace("(", "").Replace(")", "");

            if (HiddenField.Value.Length > 50)
                _transaction.ReferenceNumber = HiddenField.Value.Substring(0, 50);
            else
                _transaction.ReferenceNumber = HiddenField.Value;

            dspvarDataSet cachedDs = (dspvarDataSet)dm.GetCache("dspvarDataSet");

            decimal totalSalesTax = 0;

            string[] trans1 = HiddenField.Value.Split(new Char[] { ',' });
            string[] trans2;

            for (int x = 0; x < trans1.Length; x++)
            {
                trans2 = trans1[x].Split(new Char[] { '|' });

                DataRow[] rows = cachedDs.ttarbalance.Select("invoice_num = '" + trans2[0] + "' AND ref_num_seq = '" + trans2[1] + "' AND ref_num_sysid = '" + trans2[2] + "'");

                if (rows.Length == 1)
                    totalSalesTax = totalSalesTax + Convert.ToDecimal(rows[0]["sales_tax_open_amt"]);
            }

            _transaction.SalesTaxAmount = (String.Format("{0:C}", totalSalesTax)).Replace("$", "").Replace(",", "");
        }
        else
        {
            _transaction.TransactionAmount = _fixedAmountToPay.ToString("N2").Trim();
            _transaction.ReferenceNumber = "0";
            _transaction.SalesTaxAmount = "0.00";
        }

        _transactionSetup = new com.elementexpress.certtransaction.TransactionSetup();
        _transactionSetup.Embedded = com.elementexpress.certtransaction.BooleanType.True;
        _transactionSetup.TransactionSetupMethod = com.elementexpress.certtransaction.TransactionSetupMethod.CreditCardSale;
        _transactionSetup.CompanyName = eUser.GetContextValue("ApplicationName");

        DataRow custRow = null;
        DataRow currBranchRow = null;
        DataRow shiptoRow = null;

        if (dm.GetCache("CurrentBranchRow") != null)
            currBranchRow = (DataRow)dm.GetCache("CurrentBranchRow");

        if (dm.GetCache("CustomerRowForInformationPanel") != null)
            custRow = (DataRow)dm.GetCache("CustomerRowForInformationPanel");

        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
            shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

        _address = new com.elementexpress.certtransaction.Address();
        _address.BillingName = (string)shiptoRow["shipto_name"];
        _address.ShippingZipcode = (string)currBranchRow["shipping_zipcode"];

        try
        {
            _response = _expressService.TransactionSetup(_credentials, _application, _terminal, _transaction, _transactionSetup, _address, null, null);

            if (_response.ExpressResponseCode == "0" && _response.ExpressResponseMessage == "Success")
            {
                Session["TransactionSetupID"] = _response.Transaction.TransactionSetupID;

                bool success = this.StartPayment(_response.Transaction.TransactionSetupID, IsFixedAmount, Convert.ToDecimal(_transaction.TransactionAmount), "");

                if (success)
                {
                    string setupUrl = eUser.GetContextValue("HostedPaymentURL");

                    if (setupUrl.ToLower().StartsWith("http://"))
                        setupUrl = setupUrl.Substring(7);

                    if (setupUrl.ToLower().StartsWith("https://"))
                        setupUrl = setupUrl.Substring(8);

                    setupUrl = "https://" + setupUrl + "/?TransactionSetupID=" + _response.Transaction.TransactionSetupID;

                    iframeElement.Attributes["src"] = setupUrl;

                    //TODO: Inject JavaScript setInterval function to inspect the iframes's src=URL for changes to close the modal popup...
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "IntervalTimer", "var tickInterval = setInterval(function(){ var iframeSource = $get('ctl00_cphMaster1_MainTabControl1_Payments1_iframeElement').src; debugger; if(iframeSource.indexOf('Approved') != -1){ clearInterval(tickInterval); $get('ctl00_cphMaster1_MainTabControl1_Payments1_btnExitElement').click();}}, 30000);", true);

                    //MDM - OK; show modal and iframe NOW...
                    this.mpeACH.Enabled = false;
                    this.mpeFixedPayment.Enabled = false;
                    this.mpeCC.Enabled = true;
                    this.mpeCC.Show();
                }
                else
                {
                    this.lblGrandTotal.Text = "Credit card payment configuration failed. See Log Viewer for details.";
                    btnPay.Enabled = false;
                    HiddenField.Value = "";
                    HiddenField2.Value = "";
                }
            }
            else
            {
                Dmsi.Agility.EntryNET.EventLog log = new EventLog();
                log.LogError(EventTypeFlag.ApplicationEvent, new Exception(DateTime.Now.ToString() + ": " + "A failure occurred while calling Element's TransactionSetup SOAP Service; ExpressResponseCode is " + _response.ExpressResponseCode + "; ExpressResponseMessage is " + _response.ExpressResponseMessage + "."));

                btnPay.Enabled = false;
                btnPayBank.Enabled = false;
                HiddenField.Value = "";
                HiddenField2.Value = "";
            }
        }
        catch (SystemException ex)
        {
            Dmsi.Agility.EntryNET.EventLog log = new EventLog();
            log.LogError(EventTypeFlag.UnhandledException, ex);

            btnPay.Enabled = false;
            btnPayBank.Enabled = false;
            HiddenField.Value = "";
            HiddenField2.Value = "";
        }
    }

    private bool ElementReportingServiceCall(bool IsFixedAmount)
    {
        bool success = false;

        DataManager tempdm = new DataManager();
        AgilityEntryNetUser eUser = tempdm.GetCache("AgilityEntryNetUser") as AgilityEntryNetUser;

        string serviceUrl = eUser.GetContextValue("ReportingURL");

        if (serviceUrl.ToLower().StartsWith("http://"))
            serviceUrl = serviceUrl.Substring(7);

        if (serviceUrl.ToLower().StartsWith("https://"))
            serviceUrl = serviceUrl.Substring(8);

        serviceUrl = "https://" + serviceUrl;

        if (!serviceUrl.ToLower().EndsWith("/express.asmx"))
            serviceUrl += "/express.asmx";

        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        com.elementexpress.certreporting.Application reportingApplication = new com.elementexpress.certreporting.Application();
        reportingApplication.ApplicationID = "5332";
        reportingApplication.ApplicationName = eUser.GetContextValue("ApplicationName");
        reportingApplication.ApplicationVersion = Application["AgilityVersion"].ToString().Replace("_", ".");
        
        com.elementexpress.certreporting.Credentials reportingCredentials = new com.elementexpress.certreporting.Credentials();
        reportingCredentials.AccountID = eUser.GetContextValue("AccountID");
        reportingCredentials.AccountToken = eUser.GetContextValue("AccountToken");
        reportingCredentials.AcceptorID = eUser.GetContextValue("AcceptorID");

        com.elementexpress.certreporting.Parameters reportParams = new com.elementexpress.certreporting.Parameters();
        reportParams.TransactionSetupID = Session["TransactionSetupID"].ToString();

        com.elementexpress.certreporting.Express reportingService = new com.elementexpress.certreporting.Express();
        reportingService.Url = serviceUrl;

        string capturedValues = "";

        StringBuilder sb = new StringBuilder();
        sb.Append("reportingService.Url = " + reportingService.Url + "\r\n");
        sb.Append("reportingApplication.ApplicationID = " + reportingApplication.ApplicationID + "\r\n");
        sb.Append("reportingApplication.ApplicationName = " + reportingApplication.ApplicationName + "\r\n");
        sb.Append("reportingApplication.ApplicationVersion = " + reportingApplication.ApplicationVersion + "\r\n");
        sb.Append("reportingCredentials.AccountID = " + reportingCredentials.AccountID + "\r\n");
        sb.Append("reportingCredentials.AccountToken = " + reportingCredentials.AccountToken + "\r\n");
        sb.Append("reportingCredentials.AcceptorID = " + reportingCredentials.AcceptorID + "\r\n");
        sb.Append("reportParams.TransactionSetupID = " + reportParams.TransactionSetupID + "\r\n");

        capturedValues = sb.ToString();

        int retryCounter = 0;

loop:   try
        {
            _reportingResponse = reportingService.TransactionQuery(reportingCredentials, reportingApplication, reportParams, null);

            if (_reportingResponse.ExpressResponseCode == "90" && retryCounter <= 2)
            {
                Thread.Sleep(3000); //MDM -- 3 second delay, then try again
                retryCounter++;
                goto loop;
            }

            //// MDM - Testing an ExpressResponseCode of "90".
            //_reportingResponse.ExpressResponseCode = "90";
            //_reportingResponse.ExpressResponseMessage = "No record(s)";

            if ((_reportingResponse.ExpressResponseCode == "0" && _reportingResponse.ExpressResponseMessage.ToUpper() == "SUCCESS") || _reportingResponse.ExpressResponseCode == "5")
            {
                DataSet dsReportingData = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(_reportingResponse.ReportingData);
                dsReportingData.ReadXml(xmlSR, XmlReadMode.Auto);

                bool hasBeenApproved = false;

                foreach(DataRow row in dsReportingData.Tables["Item"].Rows)
                {
                    if (row["TransactionStatus"].ToString().ToUpper() == "APPROVED" || row["TransactionStatus"].ToString().ToUpper() == "PARTIAL APPROVED")
                    {
                        bool paymentSuccess = ApplyPayment(reportParams.TransactionSetupID, _reportingResponse.ReportingData, _reportingResponse, IsFixedAmount);

                        hasBeenApproved = true;
                        success = true;
                    }
                }

                if (!hasBeenApproved)
                {
                    success = false;
                    bool canceled = CancelPayment(reportParams.TransactionSetupID, IsFixedAmount);
                }
            }
            else
            {
                if (_reportingResponse.ExpressResponseCode == "90" && _reportingResponse.ExpressResponseMessage.ToLower().Contains("no record"))
                {
                    Dmsi.Agility.EntryNET.EventLog log = new EventLog();
                    log.LogError(EventTypeFlag.ApplicationEvent, new Exception(DateTime.Now.ToString() + ": " + "Transaction Setup ID " + reportParams.TransactionSetupID + ". A non-success was returned from calling WorldPay's Transaction Reporting SOAP Service; ExpressResponseCode is " + _reportingResponse.ExpressResponseCode + "; ExpressResponseMessage is " + _reportingResponse.ExpressResponseMessage + "."));
                }
                else
                {
                    bool canceled = CancelPayment(reportParams.TransactionSetupID, IsFixedAmount);

                    Dmsi.Agility.EntryNET.EventLog log = new EventLog();
                    log.LogError(EventTypeFlag.ApplicationEvent, new Exception(DateTime.Now.ToString() + ": " + "Payment was cancelled in Agility for Transaction Setup ID " + reportParams.TransactionSetupID + ". A non-success was returned from calling WorldPay's Transaction Reporting SOAP Service; ExpressResponseCode is " + _reportingResponse.ExpressResponseCode + "; ExpressResponseMessage is " + _reportingResponse.ExpressResponseMessage + "."));
                }

                success = false;
            }

            DataFetch();

            btnPay.Enabled = false;
            btnPayBank.Enabled = false;
            HiddenField.Value = "";
            HiddenField2.Value = "";
            lblGrandTotal.Text = "$0.00";
            lblSurchargeTotal.Text = "$0.00";
        }
        catch (System.Exception ex)
        {
            Dmsi.Agility.EntryNET.EventLog log = new EventLog();
            log.LogError(EventTypeFlag.UnhandledException, ex);
            success = false;

            btnPay.Enabled = false;
            btnPayBank.Enabled = false;
            HiddenField.Value = "";
            HiddenField2.Value = "";
            lblGrandTotal.Text = "$0.00";
            lblSurchargeTotal.Text = "$0.00";
        }

        return success;
    }

    private bool StartPayment(string guid, bool IsFixedAmount, decimal transactionAmount, string mode)
    {
        bool success = false;
        decimal ccSurchargeAmount = 0;
        string ccSurchargeBasisType = "";
        decimal ccSurchargeBasisAmount = 0;

        dspvarDataSet localDs = (dspvarDataSet)WebHierarchicalDataGridDetail.DataSource;
        dspvarDataSet paymentDs = new dspvarDataSet();

        //if using credit card surchage
        if (Convert.ToBoolean(HiddenFieldSurchargeUsage.Value))
        {
            ccSurchargeAmount = Convert.ToDecimal(HiddenFieldCCSurchargeTotal.Value.Replace("$","")) ;
            ccSurchargeBasisType = HiddenFieldSurchargeType.Value;
            ccSurchargeBasisAmount = Convert.ToDecimal(HiddenFieldSurchargeValue.Value);
        }

        if (!IsFixedAmount)
        {
            foreach (GridRecord record in WebHierarchicalDataGridDetail.Rows)
            {
                if (record.Items[5].Value.ToString().ToLower() == "true")
                {
                    (((Infragistics.Web.UI.GridControls.UnboundCheckBoxGridRecordItem)(record.Items[5]))).Value = false;

                    decimal paymentAmt = Convert.ToDecimal(record.Items[20].Value);
                    Int32 ship_num = Convert.ToInt32(record.Items[2].Value);
                    string invoice_num = (string)(record.Items[6].Value);
                    string ref_num_sysid = (string)(record.Items[21].Value);

                    DataRow[] rows = localDs.Tables["ttarbalance"].Select("invoice_num = '" + invoice_num + "' AND ship_num = '" + ship_num + "' AND ref_num_sysid = '" + ref_num_sysid + "'");

                    if (rows.Length == 1)
                    {
                        rows[0]["payment_amt"] = paymentAmt.ToString();
                    }
                }
            }

            localDs.AcceptChanges();

            string[] transactions = HiddenField.Value.Split(',');

            for (int x = 0; x < transactions.Length; x++)
            {
                string[] transplit = transactions[x].Split('|');

                DataRow[] rows = localDs.Tables["ttarbalance"].Select("invoice_num='" + transplit[0] + "' AND ref_num_seq = " + transplit[1] + " AND ref_num_sysid = '" + transplit[2] + "'");
                if (rows.Length == 1)
                {
                    paymentDs.Tables["ttarbalance"].LoadDataRow(rows[0].ItemArray, true);
                }
            }

            paymentDs.AcceptChanges();
            Session["CachedPaymentDataSet"] = paymentDs;
        }

        string branch = "";
        if (Session["currentBranchID"] != null)
            branch = (string)Session["currentBranchID"];

        DataManager dm = new DataManager();

        DataRow custRow = (DataRow)dm.GetCache("CustomerRowForInformationPanel");
        DataRow shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

        string custCode = (string)custRow["cust_code"];
        int shipto = (int)shiptoRow["Seq_num"];

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AccountsReceivable ar = new AccountsReceivable(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string pvURL = ""; //MDM - send in blank if CC; if mode is not blank, then set pvURL and pass it in.

        if (mode == "")
        {
            if (IsFixedAmount)
                mode = "CA-Payment Pending";
            else
                mode = "Payment Pending";
        }
        else
        {
            pvURL = Request.Url.ToString();
            pvURL = pvURL.Substring(0, pvURL.LastIndexOf("/"));
        }

        success = ar.ProcessPayment(custCode, 
                                    shipto, 
                                    guid, 
                                    "", 
                                    "", 
                                    "", 
                                    "", 
                                    transactionAmount, 
                                    mode, 
                                    branch, 
                                    "", 
                                    pvURL, 
                                    paymentDs, 
                                    new dsCustBankDataSet(), 
                                    ccSurchargeAmount, 
                                    ccSurchargeBasisType, 
                                    ccSurchargeBasisAmount, 
                                    out ReturnCode, 
                                    out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        return success;
    }

    private bool CancelPayment(string guid, bool IsFixedAmount)
    {
        bool success = false;

        dspvarDataSet paymentDs = (dspvarDataSet)Session["CachedPaymentDataSet"];

        string branch = "";
        if (Session["currentBranchID"] != null)
            branch = (string)Session["currentBranchID"];

        DataManager dm = new DataManager();

        DataRow custRow = (DataRow)dm.GetCache("CustomerRowForInformationPanel");
        DataRow shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

        string custCode = (string)custRow["cust_code"];
        int shipto = (int)shiptoRow["Seq_num"];

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AccountsReceivable ar = new AccountsReceivable(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string mode = "";

        if (IsFixedAmount)
            mode = "CA-Cancel Payment";
        else
            mode = "Cancel Payment";

        success = ar.ProcessPayment(custCode, 
                                    shipto, 
                                    guid, 
                                    "", 
                                    "", 
                                    "", 
                                    "", 
                                    0, 
                                    mode, 
                                    branch, 
                                    "", 
                                    "", 
                                    paymentDs, 
                                    new dsCustBankDataSet(), 
                                    0, 
                                    "", 
                                    0, 
                                    out ReturnCode, 
                                    out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        return success;
    }

    private bool ApplyPayment(string guid, string reportingData, Response entireResponse, bool IsFixedAmount)
    {
        bool success = false;

        string transactionID = "";
        decimal totalAmount = 0;
        string CCNumber = "";
        string approvalNumber = "";
        string transactionType = "";
        decimal ccSurchargeAmount = 0;
        string ccSurchargeBasisType = "";
        decimal ccSurchargeBasisAmount = 0;

        if (Convert.ToBoolean(HiddenFieldSurchargeUsage.Value))
        {
            ccSurchargeAmount = Convert.ToDecimal(HiddenFieldCCSurchargeTotal.Value.Replace("$", ""));
            ccSurchargeBasisType = HiddenFieldSurchargeType.Value;
            ccSurchargeBasisAmount = Convert.ToDecimal(HiddenFieldSurchargeValue.Value);
        }

        DataSet dsReportingData = new DataSet();
        System.IO.StringReader xmlSR = new System.IO.StringReader(reportingData);
        dsReportingData.ReadXml(xmlSR, XmlReadMode.Auto);

        foreach (DataRow row in dsReportingData.Tables["Item"].Rows)
        {
            if (row["TransactionStatus"].ToString().ToUpper() == "APPROVED" || row["TransactionStatus"].ToString().ToUpper() == "PARTIAL APPROVED")
            {
                transactionID = row["TransactionID"].ToString();
                totalAmount = Convert.ToDecimal(row["TransactionAmount"].ToString());
                CCNumber = row["CardNumberMasked"].ToString();
                approvalNumber = row["ApprovalNumber"].ToString();
                transactionType = row["TransactionType"].ToString();

                StringBuilder sb = new StringBuilder();
                sb.Append("<?xml version=\"1.0\" encoding=\"ISO8859-1\"?>\r\n");
                sb.Append("<response xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns=\"https://reporting.elementexpress.com\">\r\n");
                sb.Append("<ExpressResponseCode>" + entireResponse.ExpressResponseCode + "</ExpressResponseCode>\r\n");
                sb.Append("<ExpressResponseMessage>" + entireResponse.ExpressResponseMessage + "</ExpressResponseMessage>\r\n");
                sb.Append("<ExpressTransactionDate>" + entireResponse.ExpressTransactionDate + "</ExpressTransactionDate>\r\n");
                sb.Append("<ExpressTransactionTime>" + entireResponse.ExpressTransactionTime + "</ExpressTransactionTime>\r\n");
                sb.Append("<ExpressTransactionTimezone>" + entireResponse.ExpressTransactionTimezone + "</ExpressTransactionTimezone>\r\n");
                sb.Append("<ReportingData>\r\n");
                sb.Append(reportingData + "\r\n");
                sb.Append("</ReportingData>\r\n");
                sb.Append("<ReportingID>" + entireResponse.ReportingID + "</ReportingID>\r\n");
                sb.Append("</response>");

                DataManager dm = new DataManager();

                DataRow custRow = (DataRow)dm.GetCache("CustomerRowForInformationPanel");
                DataRow shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

                string custCode = (string)custRow["cust_code"];
                int shipto = (int)shiptoRow["Seq_num"];

                dspvarDataSet paymentDs = (dspvarDataSet)Session["CachedPaymentDataSet"];

                string branch = "";
                if (Session["currentBranchID"] != null)
                    branch = (string)Session["currentBranchID"];

                string pvURL = Request.Url.ToString();
                pvURL = pvURL.Substring(0, pvURL.LastIndexOf("/"));

                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.AccountsReceivable ar = new AccountsReceivable(out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                string mode = "";

                if (IsFixedAmount)
                    mode = "CA-Apply Payment";
                else
                    mode = "Apply Payment";

                success = ar.ProcessPayment(custCode, 
                                            shipto, 
                                            guid, 
                                            CCNumber, 
                                            approvalNumber, 
                                            transactionType, 
                                            transactionID, 
                                            totalAmount, 
                                            mode, 
                                            branch, 
                                            sb.ToString(), 
                                            pvURL, 
                                            paymentDs, 
                                            new dsCustBankDataSet(),
                                            ccSurchargeAmount,
                                            ccSurchargeBasisType,
                                            ccSurchargeBasisAmount,
                                            out ReturnCode, 
                                            out MessageText);

                string filename = MapPath("~/Documents") + "/ProcessPayment.txt";
                File.AppendAllText(filename, "custCode               = " + custCode + "\r\n");
                File.AppendAllText(filename, "shipto                 = " + shipto.ToString() + "\r\n");
                File.AppendAllText(filename, "guid                   = " + guid + "\r\n");
                File.AppendAllText(filename, "CCNumber               = " + CCNumber + "\r\n");
                File.AppendAllText(filename, "approvalNumber         = " + approvalNumber + "\r\n");
                File.AppendAllText(filename, "transactionType        = " + transactionType + "\r\n");
                File.AppendAllText(filename, "transactionID          = " + transactionID + "\r\n");
                File.AppendAllText(filename, "totalAmount            = " + totalAmount.ToString() + "\r\n");
                File.AppendAllText(filename, "mode                   = " + mode + "\r\n");
                File.AppendAllText(filename, "branch                 = " + branch + "\r\n");
                File.AppendAllText(filename, "sb.ToString()          = " + sb.ToString() + "\r\n");
                File.AppendAllText(filename, "pvURL                  = " + pvURL + "\r\n");
                File.AppendAllText(filename, "ccSurchargeAmount      = " + ccSurchargeAmount.ToString() + "\r\n");
                File.AppendAllText(filename, "ccSurchargeBasisType   = " + ccSurchargeBasisType.ToString() + "\r\n");
                File.AppendAllText(filename, "ccSurchargeBasisAmount = " + pvURL + "\r\n");

                paymentDs.WriteXml(MapPath("~/Documents") + "/paymentDs.xml");

                if (ReturnCode == 2 && MessageText.Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }
            }
        }

        return success;
    }

    private bool ACHPay(bool IsFixedAmount)
    {
        bool success = false;

        decimal totalAmount = 0;

        dsCustBankDataSet dsPaymentBank = new dsCustBankDataSet();
        dspvarDataSet dsPaymentAR = new dspvarDataSet();

        dsCustBankDataSet dsCustBank = (dsCustBankDataSet)Session["dsCustBankDataSet"];

        if (Session["paymentDataSet"] != null)
            dsPaymentAR = (dspvarDataSet)Session["paymentDataSet"];

        if (!IsFixedAmount)
        {
            if (lblGrandTotal.Text.Contains("("))
                totalAmount = 0 - Convert.ToDecimal(this.lblGrandTotal.Text.Replace("$", "").Replace("(", "").Replace(")", ""));
            else
                totalAmount = Convert.ToDecimal(this.lblGrandTotal.Text.Replace("$", ""));
        }
        else
            totalAmount = Convert.ToDecimal(wneFixedAmount.Text.Trim());

        int selected = ddlBankAccount.SelectedIndex;
        ListItem li = ddlBankAccount.SelectedItem;

        DataRow[] rows = dsCustBank.ttcust_bank.Select("cust_bank_guid = '" + li.Value + "'");

        if (rows.Length > 0)
        {
            dsPaymentBank.ttcust_bank.LoadDataRow(rows[0].ItemArray, true);
        }

        dsPaymentBank.AcceptChanges();

        DataManager dm = new DataManager();

        DataRow custRow = (DataRow)dm.GetCache("CustomerRowForInformationPanel");
        DataRow shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

        string custCode = (string)custRow["cust_code"];
        int shipto = (int)shiptoRow["Seq_num"];

        string branch = "";
        if (Session["currentBranchID"] != null)
            branch = (string)Session["currentBranchID"];

        string pvURL = Request.Url.ToString();
        pvURL = pvURL.Substring(0, pvURL.LastIndexOf("/"));

        int ReturnCode;
        string MessageText;

        AccountsReceivable ar = new AccountsReceivable(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string mode = "";

        if (IsFixedAmount)
            mode = "CA-ACH";
        else
            mode = "ACH";

        success = ar.ProcessPayment(custCode, 
                                    shipto, 
                                    "", 
                                    "", 
                                    "", 
                                    "", 
                                    "", 
                                    totalAmount, 
                                    mode, 
                                    branch, 
                                    "", 
                                    pvURL, 
                                    dsPaymentAR, 
                                    dsPaymentBank, 
                                    0, 
                                    "", 
                                    0, 
                                    out ReturnCode, 
                                    out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        DataFetch();

        ResetFields();

        return success;
    }

    private bool LumpSumPaymentButtonVisible()
    {
        return SafeSessionGet("apply_payments", true) && SafeSessionGet("create_cash_on_acct", true);
    }

    private bool SafeSessionGet(string pcActionName, bool plExpectedValue)
    {
        if ((Session[pcActionName] != null && (bool)Session[pcActionName] == plExpectedValue)) { return true; }

        return false;
    }

    private bool SafeSessionGet(string pcActionName, string plExpectedValue)
    {
        if ((Session[pcActionName] != null && (string)Session[pcActionName] == plExpectedValue)) { return true; }

        return false;
    }

    #endregion

    #region public Methods

    public void Reset(string branchID, string shiptoObjString)
    {
        //TODO: What do we need to reset Payments?
        //Until further clarification; just call the Start Over button's click method.
        btnStartOver_Click(null, null);
    }

    #endregion

}