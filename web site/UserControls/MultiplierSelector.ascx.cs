using System;
using System.Data;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_MultiplierSelector : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate(decimal markupFactor);
    public event OKClickedDelegate OnOKClicked;

    private decimal _MarkupFactor;
    public decimal MarkupFactor
    {
        get { return _MarkupFactor; }
        set
        {
            _MarkupFactor = value;
            numericMarkupFactor.ValueDecimal = value;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Change multiplier min value, based on mode; Project: 94713 - Pricing in PartnerView - Quotes
        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price")
        {
            lblExample.Visible = false;

            this.numericMarkupFactor.MinValue = 1.0000;

            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                this.numericMarkupFactor.MinValue = 1;
                this.numericMarkupFactor.MaxValue = 99;
                this.numericMarkupFactor.MaxLength = 2;
                this.numericMarkupFactor.MaxDecimalPlaces = 0;
                this.numericMarkupFactor.NullValue = 1;

                if (Session["CachedMargin"] == null)
                    this.numericMarkupFactor.ValueInt = 1;

                lblMultiplier.Text = "Margin";
            }
            else
            {
                this.numericMarkupFactor.MinValue = 1;
                this.numericMarkupFactor.MaxValue = 999;
                this.numericMarkupFactor.MaxLength = 7;
                this.numericMarkupFactor.MaxDecimalPlaces = 4;
                this.numericMarkupFactor.NullValue = 1;

                if (Session["CachedMultiplier"] == null)
                    this.numericMarkupFactor.ValueDecimal = 1.00M;

                lblMultiplier.Text = "Multiplier";
            }
        }
        else if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost")
        {
            lblExample.Visible = false;

            this.numericMarkupFactor.MinValue = 1;

            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                this.numericMarkupFactor.MinValue = 1;
                this.numericMarkupFactor.MaxValue = 99;
                this.numericMarkupFactor.MaxLength = 2;
                this.numericMarkupFactor.MaxDecimalPlaces = 0;
                this.numericMarkupFactor.NullValue = 1;

                if (Session["CachedMargin"] == null)
                    this.numericMarkupFactor.ValueInt = 1;

                lblMultiplier.Text = "Margin";
            }
            else
            {
                this.numericMarkupFactor.MinValue = 1;
                this.numericMarkupFactor.MaxValue = 999;
                this.numericMarkupFactor.MaxLength = 7;
                this.numericMarkupFactor.MaxDecimalPlaces = 4;
                this.numericMarkupFactor.NullValue = 1;

                if (Session["CachedMultiplier"] == null)
                    this.numericMarkupFactor.ValueDecimal = 1.00M;

                lblMultiplier.Text = "Multiplier";
            }
        }
        else if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
        {
            lblExample.Visible = true;

            this.numericMarkupFactor.MinValue = 0.0001;
            this.numericMarkupFactor.MaxValue = 1;
            this.numericMarkupFactor.MaxLength = 9;
            this.numericMarkupFactor.MaxDecimalPlaces = 4;
            this.numericMarkupFactor.NullValue = 0.0001;

            lblMultiplier.Text = "Multiplier";

            if (Session["CachedMultiplier"] == null)
                this.numericMarkupFactor.ValueDecimal = 0.0001M;
        }

        if (Session["CachedMultiplier"] != null)
        {
            numericMarkupFactor.Value = (decimal)Session["CachedMultiplier"];
        }
        else if (Session["CachedMargin"] != null)
        {
            numericMarkupFactor.Value = (decimal)Session["CachedMargin"];
        }
        else
        {
            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Multiplier")
            {
                numericMarkupFactor.Value = (decimal)Session["MarkupFactor"];
            }

            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                numericMarkupFactor.Value = (int)Session["MarginFactor"];
            }
        }

        if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Multiplier")
        {
            preferredMarkupValue.Value = ((decimal)Session["MarkupFactor"]).ToString();
        }

        if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
        {
            preferredMarkupValue.Value = ((int)Session["MarginFactor"]).ToString();
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
        {
            Session["MarkupSetting"] = "Multiplier";
        }

        if (numericMarkupFactor.Value != null)
        {
            decimal finalValue = 1;

            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                finalValue = Math.Round(Convert.ToDecimal(1 / (1 - (numericMarkupFactor.ValueDecimal / 100))), 4);

                Session["CachedMargin"] = numericMarkupFactor.ValueDecimal;
                Session["CachedMultiplier"] = null;
            }
            else
            {
                finalValue = numericMarkupFactor.ValueDecimal;

                Session["CachedMargin"] = null;
                Session["CachedMultiplier"] = numericMarkupFactor.ValueDecimal;
            }

            if (OnOKClicked != null)
                OnOKClicked(Convert.ToDecimal(finalValue)); // will be caught by the parent
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (Session["CachedMultiplier"] != null)
            numericMarkupFactor.Value = (decimal)Session["CachedMultiplier"];
    }
}
