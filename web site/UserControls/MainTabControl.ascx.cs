using AjaxControlToolkit;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Infragistics.Web.UI.EditorControls;
using Infragistics.Web.UI.NavigationControls;
using System;
using System.Data;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MainTabControl : System.Web.UI.UserControl
{
    bool _displayCustomerSelection = false;

    protected void QuoteRelease_OnQuoteReleased()
    {
        this.Quotes1.btnFind_Click(null, null);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       WebDataMenu1.StyleSetName = System.Configuration.ConfigurationManager.AppSettings["Theme"];

        this.CustomerTabPanel1.OnShiptoSelectionChanged += new UserControls_Customers.ShiptoSelectionChangedDelegate(CustomerTabPanel1_OnShiptoSelectionChanged);
        this.CustomerTabPanel1.OnCustomerSelectionChanged += new UserControls_Customers.CustomerSelectionChangedDelegate(CustomerTabPanel1_OnCustomerSelectionChanged);
        
        this.Inventory1.OnAddToCart += new UserControls_Inventory.AddToCartDelegate(Default1_OnAddToCart);
        this.ComplementaryItems.OnAddToCart += new UserControls_ComplementaryItems.AddToCartDelegate(Default1_OnAddToCart);
        this.OrderTab1.OnSaleTypeChanged += new UserControls_OrderTab.SaleTypeChangedDelegate(OrderTab1_OnSaleTypeChanged);
        this.QuoteRelease.OnQuoteReleased += QuoteRelease_OnQuoteReleased;

        if (!this.IsPostBack)
        {
            
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            } 

            // Get the name of the currently selected customer from dsCU
            Dmsi.Agility.Data.Customers custDS = new Customers(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            } 

            DropDownList ddlBranch = (DropDownList)this.Page.Master.FindControl("ddlBranch");
            if (custDS.ReferencedDataSet != null)
            {
                ddlBranch.DataTextField = "company_name";
                ddlBranch.DataValueField = "branch_id";
                ddlBranch.DataSource = custDS.ReferencedDataSet.Tables["ttbranch_cust"];
                ddlBranch.DataBind();
            }
           
            Dmsi.Agility.Data.Branch branch = new Branch();
            ddlBranch.Text = branch.BranchId;

            // Show tab based on user security
          
                this.UpdateMainTabDisplay();
            
            // Route URL to the correct tab
                
               
            if ((string)Session["GenericLogin"] == "False")
            {
                if (Session["RetainpriorLogin_SelectedDetails"] != null)
                {
                    UserControls_Inventory control = FindControlRecursive(this, "Inventory1") as UserControls_Inventory;
                    control.Visible = true;
                    
                    ActivateControl("Inventory1");
                    Session["ActiveProductsPage"] = 1;
                }
                else
                {
                  
                        this.SelectActiveTab();
                       
                }
            }
            else
            {
                ActivateControl("Inventory1");
                WebDataMenu1.Visible = false;
                SalesOrders1.Visible = false;
                CreditMemos1.Visible = false;
                Quotes1.Visible = false;
                QuoteRelease.Visible = false;
                QuoteEdit.Visible = false;
                Invoices1.Visible = false;
                Inventory1.Visible = true;
                OrderTab1.Visible = false;
                Payments1.Visible = false;
                string sFileName = Path.GetFileName(this.Page.MasterPageFile);

                if (sFileName == "OpenMaster35SP1.master")
                {
                    this.Page.Master.FindControl("CartPanel").Visible = false;
                    HyperLink hypLogOut = (HyperLink)this.Page.Master.FindControl("hypLogOut");
                    hypLogOut.Text = "Login";
                    this.Page.Master.FindControl("UpdatePanel2").FindControl("lblbranch").Visible = false;
                    this.Page.Master.FindControl("UpdatePanel2").FindControl("awelcomeheader").Visible = false;
                    this.Page.Master.FindControl("UpdatePanel2").FindControl("licartdetails").Visible = false;
                    this.Page.Master.FindControl("UpdatePanel2").FindControl("liCustdetails").Visible = false;
                    Label1.Visible = false;
                    imgLogo.Enabled = false;
                }
            }
            if (Session["IsThemeChanged"] != null)
            {
                if ((string)Session["IsThemeChanged"] == "yes")
                {
                    
                    UserControls_SettingsUserControl settingcontrol = FindControlRecursive(this, "PasswordChanges1") as UserControls_SettingsUserControl;
                    settingcontrol.Visible = true;
                    

                    ChooseVisibleContent("PasswordChanges1");
                   
                    UserControls_SettingsUserControl control1 = FindControlRecursive(this, "PasswordChanges1") as UserControls_SettingsUserControl;


                    
                   control1.FindControl("ChangeTheme1").Visible=true;
                   ScriptManager.RegisterClientScriptBlock(control1.Page, this.GetType(), "MyFun", "Themepicker();", true);
                   
                    HtmlControl dvlogview = (HtmlControl)control1.FindControl("LogView1");
                    HtmlControl dvlogpanel = (HtmlControl)control1.FindControl("adsearchdiv");



                    dvlogpanel.Visible = false;
                    dvlogview.Visible = false;
                    
                    LinkButton lnks = (LinkButton)control1.FindControl("lnkchangepass");
                    LinkButton lnkChange = (LinkButton)control1.FindControl("lnkChange");
                    lnkChange.CssClass = "inactiveli";
                    LinkButton lnkLogview = (LinkButton)control1.FindControl("lnkLogview");
                    lnkLogview.CssClass = "inactiveli";
                    LinkButton lnkmessages = (LinkButton)control1.FindControl("lnkmessages");
                    lnkmessages.CssClass = "inactiveli";
                    lnks.CssClass = "inactiveli";
                    System.Web.UI.HtmlControls.HtmlGenericControl lilogview = (System.Web.UI.HtmlControls.HtmlGenericControl)control1.FindControl("lilogview");
                    System.Web.UI.HtmlControls.HtmlGenericControl limessage = (System.Web.UI.HtmlControls.HtmlGenericControl)control1.FindControl("limessage");
                    Label lbllblSttingName = (Label)control1.FindControl("lblSttingName");
                    lbllblSttingName.Text = "Change Theme";
                    lbllblSttingName.Visible = true;
                    lnkChange.Visible = true;
                   
                    lnkChange.CssClass = "activesettings";
                 
                   
                    control1.LoadControl("~/UserControls/ChangeTheme.ascx");
                    ScriptManager.RegisterClientScriptBlock(control1.Page, this.GetType(), "MyFun", "Themepicker();", true);
                   
                    
                   
                }
            }
            if(Session["Ismessagechanged"]!=null)
            {

                if((string)Session["Ismessagechanged"]=="Yes")
                {
                    UserControls_SettingsUserControl settingcontrol = FindControlRecursive(this, "PasswordChanges1") as UserControls_SettingsUserControl;
                    settingcontrol.Visible = true;
                    Session["Ismessagechanged"] = null;
                    Session["IsmessageCatchanged"] = "yes";
                    ChooseVisibleContent("PasswordChanges1");

                    UserControls_SettingsUserControl control1 = FindControlRecursive(this, "PasswordChanges1") as UserControls_SettingsUserControl;
                   


                    control1.FindControl("MessagesMaint1").Visible = true;
                    HtmlControl dvlogview = (HtmlControl)control1.FindControl("LogView1");
                    HtmlControl dvlogpanel = (HtmlControl)control1.FindControl("adsearchdiv");



                    dvlogpanel.Visible = false;
                    dvlogview.Visible = false;

                    LinkButton lnks = (LinkButton)control1.FindControl("lnkchangepass");
                    LinkButton lnkChange = (LinkButton)control1.FindControl("lnkChange");
                    lnkChange.CssClass = "inactiveli";
                    LinkButton lnkLogview = (LinkButton)control1.FindControl("lnkLogview");
                    lnkLogview.CssClass = "inactiveli";
                    LinkButton lnkmessages = (LinkButton)control1.FindControl("lnkmessages");
                    lnkmessages.CssClass = "inactiveli";
                    lnks.CssClass = "inactiveli";
                    System.Web.UI.HtmlControls.HtmlGenericControl lilogview = (System.Web.UI.HtmlControls.HtmlGenericControl)control1.FindControl("lilogview");
                    System.Web.UI.HtmlControls.HtmlGenericControl limessage = (System.Web.UI.HtmlControls.HtmlGenericControl)control1.FindControl("limessage");
                    Label lbllblSttingName = (Label)control1.FindControl("lblSttingName");
                    lbllblSttingName.Text = "Message";
                    lbllblSttingName.Visible = true;
                    lnkmessages.Visible = true;
                    lnkmessages.CssClass = "activesettings";
                }
            }
            if ((string)Session["GenericLogin"] == "False")
            {

                imgLogo.Enabled = true;
            }
            else
            {
                imgLogo.Enabled = false;
            }
        }
        else
        {
            
            string ctrlname = Request.Params.Get("__EVENTTARGET");

            if ((string)Session["GenericLogin"] == "True")
            {
                string sFileName = Path.GetFileName(this.Page.MasterPageFile);

                if (sFileName == "OpenMaster35SP1.master")
                {


                    this.Page.Master.FindControl("UpdatePanel2").FindControl("lblbranch").Visible = false;
                    this.Page.Master.FindControl("UpdatePanel2").FindControl("awelcomeheader").Visible = false;
                    this.Page.Master.FindControl("UpdatePanel2").FindControl("licartdetails").Visible = false;
                    this.Page.Master.FindControl("UpdatePanel2").FindControl("liCustdetails").Visible = false;
                   
                }
            }
        }
       
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
       

        if (Session["IsThemeChanged"] != null)
        {
            if ((string)Session["IsThemeChanged"] == "yes")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun", "Themepicker();", true);
                Session["IsThemeChanged"] = null;
            }
        }
        //TODO: StockNET interface
        //figure out if we're coming from StockNET...
        if (Session["FromStockNET"] != null)
            FetchStockNetItem();


        if (Session["AllowRetailPricing"] != null && ((string)Session["AllowRetailPricing"]).ToLower() == "yes")
        {
            if (Inventory1.Visible && Inventory1.FindControl("trDetails").Visible && Session["ActiveProductsPage"] != null && (int)Session["ActiveProductsPage"] == 1)
            {
                this.Page.Master.FindControl("hypUserPreferences").Visible = false;
                this.Page.Master.FindControl("OptionsModalDiv").Visible = false;

                if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                {
                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                }
            }

            if (Inventory1.Visible && Inventory1.FindControl("dvProdListView").Visible && Session["ActiveProductsPage"] != null && (int)Session["ActiveProductsPage"] == 1)
            {
                this.Page.Master.FindControl("hypUserPreferences").Visible = true;
                this.Page.Master.FindControl("OptionsModalDiv").Visible = true;

                if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                {
                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

                    if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || 
                        (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || 
                        (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
                    }
                    else
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
                    }
                }
            }

            if (Session["UserAction_DisableOnScreenUserPreferences"] == null || (bool)Session["UserAction_DisableOnScreenUserPreferences"] == true)
            {
                this.Page.Master.FindControl("liNetRetail").Visible = false;
            }
        }

        int visiblecounter = 0;
        for (int x = 0; x < WebDataMenu1.Items.Count; x++)
        {
            if (WebDataMenu1.Items[x].Visible)
                visiblecounter++;
        }

        if (visiblecounter == 6)
            this.WebDataMenu1.Width = new Unit(530, UnitType.Pixel);

        if (_displayCustomerSelection)
        {
            _displayCustomerSelection = false;
            Home1.Visible = false;
            CustomerTabPanel1.Visible = true;
        }
    }

    #region Public Methods

    public void UpdateMainTabDisplay()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("Shopping Cart");

        // Check if this is a true 1 customer and one 1 shipto case.
        // This is a special case usually for customer's customer log on.
        if (dm.GetCache("SingleShipto") != null)
        {
            SetVisibilityOfAccountChangeLink(false);
        }


        // Hide non-default content controls
        SalesOrders1.Visible = false;
        CreditMemos1.Visible = false;
        Quotes1.Visible = false;
        QuoteRelease.Visible = false;
        QuoteEdit.Visible = false;
        Invoices1.Visible = false;
        OrderTab1.Visible = false;
        Payments1.Visible = false;

        // Hide non-default menu items
        DataMenuItem productsItem = WebDataMenu1.Items.FindDataMenuItemByText("Products");
        productsItem.Visible = false;

        DataMenuItem quotesItem = WebDataMenu1.Items.FindDataMenuItemByText("Quotes");
        quotesItem.Visible = false;

        DataMenuItem salesordersItem = WebDataMenu1.Items.FindDataMenuItemByText("Orders");
        salesordersItem.Visible = false;

        DataMenuItem invoicesItem = WebDataMenu1.Items.FindDataMenuItemByText("Invoices");
        invoicesItem.Visible = false;

        DataMenuItem creditmemosItem = WebDataMenu1.Items.FindDataMenuItemByText("Credit Memos");
        creditmemosItem.Visible = false;

        DataMenuItem paymentsItem = WebDataMenu1.Items.FindDataMenuItemByText("Billing");
        paymentsItem.Visible = false;

        Session["InventoryIsAllowed"] = null;

        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            string menu_structure_obj_main = "";
            Dmsi.Agility.EntryNET.StrongTypesNS.dsPartnerVuDataSet ds = (dsPartnerVuDataSet)dm.GetCache("dsPV");
            DataRow[] rows = ds.bPV_userMenus.Select("menu_structure_obj='0'");
            int iMenuTabRowCount;
            iMenuTabRowCount = ds.Tables[0].Rows.Count;

            if (rows.Length > 0)
            {
                menu_structure_obj_main = rows[0]["child_menu_structure_obj"].ToString();
                DataRow[] menuRows = ds.bPV_userMenus.Select("menu_structure_obj='" + menu_structure_obj_main + "' AND menu_item_label='Main'");

                if (menuRows.Length > 0)
                {
                    menu_structure_obj_main = menuRows[0]["child_menu_structure_obj"].ToString();
                    DataRow[] childRows = ds.bPV_userMenus.Select("menu_structure_obj='" + menu_structure_obj_main + "'");

                    if (childRows.Length > 0)
                    {
                        for (int i = 0; i < childRows.Length; i++)
                        {
                            switch (childRows[i]["menu_item_label"].ToString())
                            {
                                case "Sales Orders":
                                    {
                                        WebDataMenu1.Visible = true;
                                        DataMenuItem ordersItem = WebDataMenu1.Items.FindDataMenuItemByText("Orders");
                                        ordersItem.Visible = true;
                                        break;
                                    }
                                case "Shopping Cart":
                                    {
                                        dm.SetCache("Shopping Cart", true);
                                        break;
                                    }
                                case "Quotes":
                                    {
                                        WebDataMenu1.Enabled = true;
                                        DataMenuItem quotesItem2 = WebDataMenu1.Items.FindDataMenuItemByText("Quotes");
                                        quotesItem2.Visible = true;
                                        break;
                                    }
                                case "Credit Memos":
                                    {
                                        WebDataMenu1.Enabled = true;
                                        DataMenuItem creditmemosItem2 = WebDataMenu1.Items.FindDataMenuItemByText("Credit Memos");
                                        creditmemosItem2.Visible = true;
                                        break;
                                    }
                                case "Invoices":
                                    {
                                        WebDataMenu1.Enabled = true;
                                        DataMenuItem invoicesItem2 = WebDataMenu1.Items.FindDataMenuItemByText("Invoices");
                                        invoicesItem2.Visible = true;
                                        break;
                                    }
                                case "Inventory Items":
                                    {
                                        WebDataMenu1.Enabled = true;

                                        //needed for FetchStockNetItem method.
                                        Session["InventoryIsAllowed"] = true;

                                        DataMenuItem productItem2 = WebDataMenu1.Items.FindDataMenuItemByText("Products");
                                        productItem2.Visible = true;
                                        break;
                                    }
                                case "Billing":
                                    {
                                        WebDataMenu1.Enabled = true;
                                        DataMenuItem paymentsItem2 = WebDataMenu1.Items.FindDataMenuItemByText("Billing");
                                        paymentsItem2.Visible = true;
                                        break;
                                    }
                            }
                        }
                    }

                    DataRow[] drFilterInventory = ds.bPV_userMenus.Select("menu_item_label='" + "Inventory Items" + "'");

                    if (drFilterInventory.Length > 0)
                    {
                        dvSearchTextBox.Visible = true;
                    }
                    else
                    {
                        dvSearchTextBox.Visible = false;
                    }
                }

                int rightmostmenu = 0;
                for (int x = 0; x < WebDataMenu1.Items.Count; x++)
                {
                    if (WebDataMenu1.Items[x].Visible)
                        rightmostmenu = x;
                }
                WebDataMenu1.Items[rightmostmenu].CssClass = "menulastlist";
            }

            this.ApplyShiptoRelatedSettings();

            if ((string)Session["GenericLogin"] == "False")
            {
                Session["iMenuTabRowCount"] = iMenuTabRowCount;
                
                Session["IsHome"] = null;
                ActivateControl("None");
            }
        }
        else
        {
            SetVisibilityOfCart(false);
        }
    }

    public void AllTabsOff()
    {
        this.Home1.Visible = false;
        this.CustomerTabPanel1.Visible = false;
        this.SalesOrders1.Visible = false;
        this.CreditMemos1.Visible = false;
        this.Quotes1.Visible = false;
        this.QuoteEdit.Visible = false;
        this.QuoteRelease.Visible = false;
        this.Invoices1.Visible = false;
        this.OrderTab1.Visible = false;
        this.Inventory1.Visible = false;
        this.Payments1.Visible = false;
    }

    #endregion

    void OrderTab1_OnSaleTypeChanged(string newSaleType, string newSaleTypeDescription)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();        
    }

    void Inventory1_OnAddToCart(dsOrderCartDataSet.ttorder_cartRow row)
    {
        OrderTab1.SwitchCartToEditMode(row);
    }

    void Default1_OnAddToCart(dsOrderCartDataSet.ttorder_cartRow row)
    {
        OrderTab1.SwitchCartToEditMode(row);
    }

    public void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["CachedMultiplier"] = null;

        if (Session["SavedProductGroupMajorGUID"] != null)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

            if (fi.Exists)
            {
                fi.Delete();
            }
        }

        Session["SavedProductGroupMajorGUID"] = null; //Change branch should reset, so FetchProductGroups will run again.
        UserControls_Inventory ucInventory = this.FindControl("Inventory1") as UserControls_Inventory;
        ucInventory.clearProductDetails();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        string branchID = ((DropDownList)sender).SelectedValue;

        dm.SetCache("newBranchIDforShiptoFetch", branchID);

        string shiptoObjString = "";

        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            dm.SetCache("cachednonsalable", (DataRow)dm.GetCache("ShiptoRowForInformationPanel"));
        }

        // TODO: dsCU contains the branch listing, create a Branches class later on
        //       with a constructor that will accept a branchid
        DataTable branchCust = (DataTable)Session["ttbranch_cust"];

        if (branchCust != null)
        {
            DataRow[] rows = branchCust.Select("branch_id='" + branchID + "'");
            if (rows.Length > 0)
                dm.SetCache("CurrentBranchRow", rows[0]);
        }

        // Re-assign the orignal customer and shipto. So it can be verified for this branch.
        // This will put back the original cust and shipto once the values are valid
        // for the current branch. The verification process will remove them if they are not
        // valid.
        if (dm.GetCache("SingleShipto") != null)
        {
            dm.SetCache("CustomerRowForInformationPanel", dm.GetCache("SingleCust"));
            dm.SetCache("ShiptoRowForInformationPanel", dm.GetCache("SingleShipto"));
        }

        // Verify that the currently selected customer is valid for the newly selected branch
        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer custRow = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        bool isBranchShared = custRow.IsBranchSharedForCustomer(branchID, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        if (custRow.ReferencedRow != null && !isBranchShared)
        {
            dm.RemoveCache("CustomerRowForInformationPanel");
            dm.RemoveCache("ShiptoRowForInformationPanel");
        }
        else if (custRow.ReferencedRow != null)
        {
            // Verify that the currently selected shipto is valid for the newly selected branch
            Dmsi.Agility.Data.Shipto shipTo = new Shipto(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            isBranchShared = shipTo.IsBranchSharedForShipto(branchID, out ReturnCode, out MessageText);

            if (shipTo.ReferencedRow != null && !isBranchShared)
            {
                dm.RemoveCache("CustomerRowForInformationPanel");
                dm.RemoveCache("ShiptoRowForInformationPanel");
            }
            else if (shipTo.ReferencedRow != null)
            {
                // Verify if the currently selected ship-to is valid for the current customer
                Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                DataRow row = cust.GetShiptoRow(shipTo.ShiptoSequence);

                if (row == null)
                {
                    dm.RemoveCache("ShiptoRowForInformationPanel");

                    _displayCustomerSelection = true;
                }

                shiptoObjString = shipTo.ShiptoObj.ToString();
            }
        }

        // re-apply menu security if the current customer is no longer valid for the branch
        if (dm.GetCache("ShiptoRowForInformationPanel") == null)
        {
            // Hide the tabs until another valid shipto is selected
            this.UpdateMainTabDisplay();
            dm.RemoveCache("SecurityApplied");
            dm.RemoveCache("dspvarDataSet");
            Session["WebHierarchicalDataGridDetail_DataSource"] = null;

            if (dm.GetCache("SingleShipto") == null)
            {
                AllTabsOff();
                pnlContentContainer.Visible = true;
                HideAllUserControls(pnlContentContainer);
                MakeContentVisible(pnlContentContainer, "CustomerTabPanel1");
                dvSearchTextBox.Visible = false;
            }
            else
            {
                AllTabsOff();
                pnlContentContainer.Visible = true;
                HideAllUserControls(pnlContentContainer);
                MakeContentVisible(pnlContentContainer, "Home1");
                //while changing branch global text box should be hided
                dvSearchTextBox.Visible = false;
            }
        }
        else if (dm.GetCache("SingleShipto") != null && dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            dm.RemoveCache("SecurityApplied");
            dm.RemoveCache("dspvarDataSet");
            Session["WebHierarchicalDataGridDetail_DataSource"] = null;
            this.CustomerTabPanel1_OnShiptoSelectionChanged();
        }

        // Cache the newly selected branch
        // We do this last because we use branchID for comparison in child controls.
        dm.SetCache("currentBranchID", branchID);

        // Ripple the effect on each of the tabs
        ResetTabContents(branchID, shiptoObjString);

        dm.RemoveCache("newBranchIDforShiptoFetch"); //MDM - This was temporary variable for changing branch in AgilityCustomer object sooner than it normally changes through cart fetch.

        this.ApplyShiptoRelatedSettings();

        if (Session["QuoteEditVisible"] != null && (bool)Session["QuoteEditVisible"] == true)
        {
            Session["QuoteEditVisible"] = false;
            QuoteEdit.Visible = false;

            if (dm.GetCache("SecurityApplied") != null &&
                (bool)dm.GetCache("SecurityApplied") == true &&
                OrderTab1.Visible == false &&
                CreditMemos1.Visible == false &&
                SalesOrders1.Visible == false &&
                Invoices1.Visible == false &&
                Home1.Visible == false &&
                CustomerTabPanel1.Visible == false &&
                Payments1.Visible == false)
            {
                Quotes1.Visible = true;
            }
        }

        if (Session["QuoteEditRetail_InfoVisible"] != null && (bool)Session["QuoteEditRetail_InfoVisible"] == true)
        {
            Session["QuoteEditRetail_InfoVisible"] = false;
            QuoteEditRetail_Info.Visible = false;

            if (dm.GetCache("SecurityApplied") != null &&
                (bool)dm.GetCache("SecurityApplied") == true &&
                OrderTab1.Visible == false &&
                CreditMemos1.Visible == false &&
                SalesOrders1.Visible == false &&
                Invoices1.Visible == false &&
                Home1.Visible == false &&
                CustomerTabPanel1.Visible == false &&
                Payments1.Visible == false)
            {
                Quotes1.Visible = true;
            }
        }

        if (Session["QuoteReleaseVisible"] != null && (bool)Session["QuoteReleaseVisible"] == true)
        {
            Session["QuoteReleaseVisible"] = false;
            QuoteRelease.Visible = false;

            if (dm.GetCache("SecurityApplied") != null &&
                (bool)dm.GetCache("SecurityApplied") == true &&
                OrderTab1.Visible == false &&
                CreditMemos1.Visible == false &&
                SalesOrders1.Visible == false &&
                Invoices1.Visible == false &&
                Home1.Visible == false &&
                CustomerTabPanel1.Visible == false &&
                Payments1.Visible == false)
            {
                Quotes1.Visible = true;
            }
        }

        if (QuoteRelease.Visible)
        {
            QuoteRelease.Visible = false;
        }

        if (QuoteEdit.Visible)
        {
            QuoteEdit.Visible = false;
        }

        Session["AddInfo_txtCustomerId"] = null;
        Session["AddInfo_txtShipTo"] = null;
        Session["AddInfo_txtCustomerName"] = null;
        Session["AddInfo_txtAddress1"] = null;
        Session["AddInfo_txtAddress2"] = null;
        Session["AddInfo_txtAddress3"] = null;
        Session["AddInfo_txtCity"] = null;
        Session["AddInfo_txtState"] = null;
        Session["AddInfo_txtZip"] = null;
        Session["AddInfo_txtCountry"] = null;
        Session["AddInfo_txtPhone"] = null;
        Session["AddInfo_txtContactName"] = null;
        Session["AddInfo_txtEmail"] = null;
        Session["AddInfo_txtReference"] = null;
        Session["AddInfo_taxRate"] = null;
        Session["AddInfo_taxableDescription"] = null;
        Session["AddInfo_taxableAmount"] = null;
        Session["AddInfo_nonTaxableDescription"] = null;
        Session["AddInfo_nonTaxableAmount"] = null;

        Session["Shipto_txtCustomerName"] = null;
        Session["Shipto_txtAddress1"] = null;
        Session["Shipto_txtAddress2"] = null;
        Session["Shipto_txtAddress3"] = null;
        Session["Shipto_txtCity"] = null;
        Session["Shipto_txtState"] = null;
        Session["Shipto_txtZip"] = null;
        Session["Shipto_txtCountry"] = null;
        Session["Shipto_txtPhone"] = null;

        Session["ShiptoQuote_txtCustomerName"] = null;
        Session["ShiptoQuote_txtAddress1"] = null;
        Session["ShiptoQuote_txtAddress2"] = null;
        Session["ShiptoQuote_txtAddress3"] = null;
        Session["ShiptoQuote_txtCity"] = null;
        Session["ShiptoQuote_txtState"] = null;
        Session["ShiptoQuote_txtZip"] = null;
        Session["ShiptoQuote_txtCountry"] = null;
        Session["ShiptoQuote_txtPhone"] = null;

        Session["ShiptoQuoteEdit_txtCustomerName"] = null;
        Session["ShiptoQuoteEdit_txtAddress1"] = null;
        Session["ShiptoQuoteEdit_txtAddress2"] = null;
        Session["ShiptoQuoteEdit_txtAddress3"] = null;
        Session["ShiptoQuoteEdit_txtCity"] = null;
        Session["ShiptoQuoteEdit_txtState"] = null;
        Session["ShiptoQuoteEdit_txtZip"] = null;
        Session["ShiptoQuoteEdit_txtCountry"] = null;
        Session["ShiptoQuoteEdit_txtPhone"] = null;

        dm = null;

        if (Inventory1.Visible)
            Inventory1.lnkHome_Click(null, null);
    }

    public void BranchDropdownChange(object sender)
    {
 
        Session["CachedMultiplier"] = null;

        if (Session["SavedProductGroupMajorGUID"] != null)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

            if (fi.Exists)
            {
                fi.Delete();
            }
        }
        
        Session["SavedProductGroupMajorGUID"] = null; //Change branch should reset, so FetchProductGroups will run again.

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        string branchID = ((DropDownList)sender).SelectedValue;
        string shiptoObjString = "";

        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            dm.SetCache("cachednonsalable", (DataRow)dm.GetCache("ShiptoRowForInformationPanel"));
        }

        // TODO: dsCU contains the branch listing, create a Branches class later on
        //       with a constructor that will accept a branchid
        DataTable branchCust = (DataTable)Session["ttbranch_cust"];

        if (branchCust != null)
        {
            DataRow[] rows = branchCust.Select("branch_id='" + branchID + "'");
            if (rows.Length > 0)
                dm.SetCache("CurrentBranchRow", rows[0]);
        }

        // Re-assign the orignal customer and shipto. So it can be verified for this branch.
        // This will put back the original cust and shipto once the values are valid
        // for the current branch. The verification process will remove them if they are not
        // valid.
        if (dm.GetCache("SingleShipto") != null)
        {
            dm.SetCache("CustomerRowForInformationPanel", dm.GetCache("SingleCust"));
            dm.SetCache("ShiptoRowForInformationPanel", dm.GetCache("SingleShipto"));
        }

        // Verify that the currently selected customer is valid for the newly selected branch
        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer custRow = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        bool isBranchShared = custRow.IsBranchSharedForCustomer(branchID, out ReturnCode, out MessageText);

        if (custRow.ReferencedRow != null && !isBranchShared)
        {
            dm.RemoveCache("CustomerRowForInformationPanel");
            dm.RemoveCache("ShiptoRowForInformationPanel");
        }
        else if (custRow.ReferencedRow != null)
        {
            // Verify that the currently selected shipto is valid for the newly selected branch

            Dmsi.Agility.Data.Shipto shipTo = new Shipto(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            isBranchShared = shipTo.IsBranchSharedForShipto(branchID, out ReturnCode, out MessageText);

            if (shipTo.ReferencedRow != null && !isBranchShared)
            {
                dm.RemoveCache("CustomerRowForInformationPanel");
                dm.RemoveCache("ShiptoRowForInformationPanel");
            }
            else if (shipTo.ReferencedRow != null)
            {
                // Verify if the currently selected ship-to is valid for the current customer

                Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                DataRow row = cust.GetShiptoRow(shipTo.ShiptoSequence);

                if (row == null)
                {
                    dm.RemoveCache("ShiptoRowForInformationPanel");
                }

                shiptoObjString = shipTo.ShiptoObj.ToString();
            }
        }

        // re-apply menu security if the current customer is no longer valid for the branch
        if (dm.GetCache("ShiptoRowForInformationPanel") == null)
        {
            // Hide the tabs until another valid shipto is selected
            this.UpdateMainTabDisplay();
            dm.RemoveCache("SecurityApplied");

            if (dm.GetCache("SingleShipto") == null)
            {
                AllTabsOff();
                pnlContentContainer.Visible = true;
                HideAllUserControls(pnlContentContainer);
                MakeContentVisible(pnlContentContainer, "CustomerTabPanel1");
                dvSearchTextBox.Visible = false;
            }
            else
            {
                AllTabsOff();
                pnlContentContainer.Visible = true;
                HideAllUserControls(pnlContentContainer);
                MakeContentVisible(pnlContentContainer, "Home1");
                //while changing branch global text box should be hided
                dvSearchTextBox.Visible = false;
            }
        }
        else if (dm.GetCache("SingleShipto") != null && dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            dm.RemoveCache("SecurityApplied");
            this.CustomerTabPanel1_OnShiptoSelectionChanged();
        }

        // Ripple the effect on each of the tabs
        ResetTabContents(branchID, shiptoObjString);

        // Cache the newly selected branch
        // We do this last because we use branchID for comparison in child controls.
        dm.SetCache("currentBranchID", branchID);

        this.ApplyShiptoRelatedSettings();

        if (Session["QuoteEditVisible"] != null && (bool)Session["QuoteEditVisible"] == true)
        {
            Session["QuoteEditVisible"] = false;
            QuoteEdit.Visible = false;

            if (dm.GetCache("SecurityApplied") != null &&
                (bool)dm.GetCache("SecurityApplied") == true &&
                OrderTab1.Visible == false &&
                CreditMemos1.Visible == false &&
                SalesOrders1.Visible == false &&
                Invoices1.Visible == false &&
                Home1.Visible == false &&
                CustomerTabPanel1.Visible == false &&
                Payments1.Visible == false)
            {
                Quotes1.Visible = true;
            }
        }

        if (Session["QuoteReleaseVisible"] != null && (bool)Session["QuoteReleaseVisible"] == true)
        {
            Session["QuoteReleaseVisible"] = false;
            QuoteRelease.Visible = false;

            if (dm.GetCache("SecurityApplied") != null &&
                (bool)dm.GetCache("SecurityApplied") == true &&
                OrderTab1.Visible == false &&
                CreditMemos1.Visible == false &&
                SalesOrders1.Visible == false &&
                Invoices1.Visible == false &&
                Home1.Visible == false &&
                CustomerTabPanel1.Visible == false &&
                Payments1.Visible == false)
            {
                Quotes1.Visible = true;
            }
        }

        if (QuoteRelease.Visible)
        {
            QuoteRelease.Visible = false;
        }

        if (QuoteEdit.Visible)
        {
            QuoteEdit.Visible = false;
        }

        Session["AddInfo_txtCustomerId"] = null;
        Session["AddInfo_txtShipTo"] = null;
        Session["AddInfo_txtCustomerName"] = null;
        Session["AddInfo_txtAddress1"] = null;
        Session["AddInfo_txtAddress2"] = null;
        Session["AddInfo_txtAddress3"] = null;
        Session["AddInfo_txtCity"] = null;
        Session["AddInfo_txtState"] = null;
        Session["AddInfo_txtZip"] = null;
        Session["AddInfo_txtCountry"] = null;
        Session["AddInfo_txtPhone"] = null;
        Session["AddInfo_txtContactName"] = null;
        Session["AddInfo_txtEmail"] = null;
        Session["AddInfo_txtReference"] = null;
        Session["AddInfo_taxRate"] = null;
        Session["AddInfo_taxableDescription"] = null;
        Session["AddInfo_taxableAmount"] = null;
        Session["AddInfo_nonTaxableDescription"] = null;
        Session["AddInfo_nonTaxableAmount"] = null;

        Session["Shipto_txtCustomerName"] = null;
        Session["Shipto_txtAddress1"] = null;
        Session["Shipto_txtAddress2"] = null;
        Session["Shipto_txtAddress3"] = null;
        Session["Shipto_txtCity"] = null;
        Session["Shipto_txtState"] = null;
        Session["Shipto_txtZip"] = null;
        Session["Shipto_txtCountry"] = null;
        Session["Shipto_txtPhone"] = null;

        Session["ShiptoQuote_txtCustomerName"] = null;
        Session["ShiptoQuote_txtAddress1"] = null;
        Session["ShiptoQuote_txtAddress2"] = null;
        Session["ShiptoQuote_txtAddress3"] = null;
        Session["ShiptoQuote_txtCity"] = null;
        Session["ShiptoQuote_txtState"] = null;
        Session["ShiptoQuote_txtZip"] = null;
        Session["ShiptoQuote_txtCountry"] = null;
        Session["ShiptoQuote_txtPhone"] = null;

        Session["ShiptoQuoteEdit_txtCustomerName"] = null;
        Session["ShiptoQuoteEdit_txtAddress1"] = null;
        Session["ShiptoQuoteEdit_txtAddress2"] = null;
        Session["ShiptoQuoteEdit_txtAddress3"] = null;
        Session["ShiptoQuoteEdit_txtCity"] = null;
        Session["ShiptoQuoteEdit_txtState"] = null;
        Session["ShiptoQuoteEdit_txtZip"] = null;
        Session["ShiptoQuoteEdit_txtCountry"] = null;
        Session["ShiptoQuoteEdit_txtPhone"] = null;

        dm = null;
    
    }

    private void CustomerTabPanel1_OnCustomerSelectionChanged()
    {
        Session["CachedMultiplier"] = null;

        WebDataMenu1.SelectedItem = null;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("dspvarDataSet");
        this.UpdateMainTabDisplay();
        dm.RemoveCache("SecurityApplied");

        // Customer Tab Panel has already changed the cached
        // CurrentCustomerRow and/or CurrentShiptoRow
        // Ripple the effect on each of the tabs

        DropDownList ddlmasterBranch =(DropDownList) this.Page.Master.FindControl("ddlBranch");
        string branchID = ddlmasterBranch.SelectedValue;
        ResetTabContents(branchID, "");

        if (Session["QuoteEditVisible"] != null && (bool)Session["QuoteEditVisible"] == true)
        {
            Session["QuoteEditVisible"] = false;
            QuoteEdit.Visible = false;

            if (dm.GetCache("ShiptoRowForInformationPanel") != null)
                Quotes1.Visible = true;
        }

        if (Session["QuoteReleaseVisible"] != null && (bool)Session["QuoteReleaseVisible"] == true)
        {
            Session["QuoteReleaseVisible"] = false;
            QuoteRelease.Visible = false;

            if (dm.GetCache("ShiptoRowForInformationPanel") != null)
                Quotes1.Visible = true;
        }

        if (QuoteRelease.Visible)
        {
            QuoteRelease.Visible = false;
        }

        if (QuoteEdit.Visible)
        {
            QuoteEdit.Visible = false;
        }

        Session["AddInfo_txtCustomerId"] = null;
        Session["AddInfo_txtShipTo"] = null;
        Session["AddInfo_txtCustomerName"] = null;
        Session["AddInfo_txtAddress1"] = null;
        Session["AddInfo_txtAddress2"] = null;
        Session["AddInfo_txtAddress3"] = null;
        Session["AddInfo_txtCity"] = null;
        Session["AddInfo_txtState"] = null;
        Session["AddInfo_txtZip"] = null;
        Session["AddInfo_txtCountry"] = null;
        Session["AddInfo_txtPhone"] = null;
        Session["AddInfo_txtContactName"] = null;
        Session["AddInfo_txtEmail"] = null;
        Session["AddInfo_txtReference"] = null;
        Session["AddInfo_taxRate"] = null;
        Session["AddInfo_taxableDescription"] = null;
        Session["AddInfo_taxableAmount"] = null;
        Session["AddInfo_nonTaxableDescription"] = null;
        Session["AddInfo_nonTaxableAmount"] = null;

        Session["Shipto_txtCustomerName"] = null;
        Session["Shipto_txtAddress1"] = null;
        Session["Shipto_txtAddress2"] = null;
        Session["Shipto_txtAddress3"] = null;
        Session["Shipto_txtCity"] = null;
        Session["Shipto_txtState"] = null;
        Session["Shipto_txtZip"] = null;
        Session["Shipto_txtCountry"] = null;
        Session["Shipto_txtPhone"] = null;

        Session["ShiptoQuote_txtCustomerName"] = null;
        Session["ShiptoQuote_txtAddress1"] = null;
        Session["ShiptoQuote_txtAddress2"] = null;
        Session["ShiptoQuote_txtAddress3"] = null;
        Session["ShiptoQuote_txtCity"] = null;
        Session["ShiptoQuote_txtState"] = null;
        Session["ShiptoQuote_txtZip"] = null;
        Session["ShiptoQuote_txtCountry"] = null;
        Session["ShiptoQuote_txtPhone"] = null;

        Session["ShiptoQuoteEdit_txtCustomerName"] = null;
        Session["ShiptoQuoteEdit_txtAddress1"] = null;
        Session["ShiptoQuoteEdit_txtAddress2"] = null;
        Session["ShiptoQuoteEdit_txtAddress3"] = null;
        Session["ShiptoQuoteEdit_txtCity"] = null;
        Session["ShiptoQuoteEdit_txtState"] = null;
        Session["ShiptoQuoteEdit_txtZip"] = null;
        Session["ShiptoQuoteEdit_txtCountry"] = null;
        Session["ShiptoQuoteEdit_txtPhone"] = null;

        dm = null;
    }

    private void CustomerTabPanel1_OnShiptoSelectionChanged()
    {
        Session["CachedMultiplier"] = null;

        WebDataMenu1.SelectedItem = null;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("dspvarDataSet"); //Billing needs to be refreshed.
        Session["WebHierarchicalDataGridDetail_DataSource"] = null;

        if (dm.GetCache("SecurityApplied") == null)
        {
            this.UpdateMainTabDisplay();
            dm.SetCache("SecurityApplied", true);
        }
        else // override security, if the selected shipto is nonsalable
        {
            this.ApplyShiptoRelatedSettings();
        }

        // Customer Tab Panel has already changed the cached
        // CurrentCustomerRow and/or CurrentShiptoRow
        // Ripple the effect on each of the tabs
        DropDownList ddlmasterbranch = (DropDownList)this.Page.Master.FindControl("ddlBranch");

        string branchID = ddlmasterbranch.SelectedValue;
        ResetTabContents(branchID, "");

        if (Session["QuoteEditVisible"] != null && (bool)Session["QuoteEditVisible"] == true)
        {
            Session["QuoteEditVisible"] = false;
            QuoteEdit.Visible = false;

            if (dm.GetCache("SecurityApplied") != null && (bool)dm.GetCache("SecurityApplied") == false)
                Quotes1.Visible = true;
        }

        if (Session["QuoteReleaseVisible"] != null && (bool)Session["QuoteReleaseVisible"] == true)
        {
            Session["QuoteReleaseVisible"] = false;
            QuoteRelease.Visible = false;

            if (dm.GetCache("SecurityApplied") != null && (bool)dm.GetCache("SecurityApplied") == false)
                Quotes1.Visible = true;
        }

        if (QuoteRelease.Visible)
        {
            QuoteRelease.Visible = false;
        }

        if (QuoteEdit.Visible)
        {
            QuoteEdit.Visible = false;
        }

        Session["AddInfo_txtCustomerId"] = null;
        Session["AddInfo_txtShipTo"] = null;
        Session["AddInfo_txtCustomerName"] = null;
        Session["AddInfo_txtAddress1"] = null;
        Session["AddInfo_txtAddress2"] = null;
        Session["AddInfo_txtAddress3"] = null;
        Session["AddInfo_txtCity"] = null;
        Session["AddInfo_txtState"] = null;
        Session["AddInfo_txtZip"] = null;
        Session["AddInfo_txtCountry"] = null;
        Session["AddInfo_txtPhone"] = null;
        Session["AddInfo_txtContactName"] = null;
        Session["AddInfo_txtEmail"] = null;
        Session["AddInfo_txtReference"] = null;
        Session["AddInfo_taxRate"] = null;
        Session["AddInfo_taxableDescription"] = null;
        Session["AddInfo_taxableAmount"] = null;
        Session["AddInfo_nonTaxableDescription"] = null;
        Session["AddInfo_nonTaxableAmount"] = null;

        Session["Shipto_txtCustomerName"] = null;
        Session["Shipto_txtAddress1"] = null;
        Session["Shipto_txtAddress2"] = null;
        Session["Shipto_txtAddress3"] = null;
        Session["Shipto_txtCity"] = null;
        Session["Shipto_txtState"] = null;
        Session["Shipto_txtZip"] = null;
        Session["Shipto_txtCountry"] = null;
        Session["Shipto_txtPhone"] = null;

        Session["ShiptoQuote_txtCustomerName"] = null;
        Session["ShiptoQuote_txtAddress1"] = null;
        Session["ShiptoQuote_txtAddress2"] = null;
        Session["ShiptoQuote_txtAddress3"] = null;
        Session["ShiptoQuote_txtCity"] = null;
        Session["ShiptoQuote_txtState"] = null;
        Session["ShiptoQuote_txtZip"] = null;
        Session["ShiptoQuote_txtCountry"] = null;
        Session["ShiptoQuote_txtPhone"] = null;

        Session["ShiptoQuoteEdit_txtCustomerName"] = null;
        Session["ShiptoQuoteEdit_txtAddress1"] = null;
        Session["ShiptoQuoteEdit_txtAddress2"] = null;
        Session["ShiptoQuoteEdit_txtAddress3"] = null;
        Session["ShiptoQuoteEdit_txtCity"] = null;
        Session["ShiptoQuoteEdit_txtState"] = null;
        Session["ShiptoQuoteEdit_txtZip"] = null;
        Session["ShiptoQuoteEdit_txtCountry"] = null;
        Session["ShiptoQuoteEdit_txtPhone"] = null;

        dm = null;
    }

    private void ApplyShiptoRelatedSettings()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ShiptoRowForInformationPanel") != null &&
            !(bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"] &&
            dm.GetCache("Shopping Cart") != null)
        {
            if (dm.GetCache("cachednonsalable") != null)
            {
                DataRow cachedRow = (DataRow)dm.GetCache("cachednonsalable");
                DataRow newRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

                if ((bool)cachedRow["nonsalable"] == true &&
                    (int)cachedRow["seq_num"] == (int)newRow["seq_num"] &&
                    (decimal)cachedRow["cust_shipto_obj"] == (decimal)newRow["cust_shipto_obj"])
                {
                    newRow["nonsalable"] = true;
                    dm.SetCache("ShiptoRowForInformationPanel", newRow);
                }
                else
                {
                    SetVisibilityOfCart(true);
                    this.Inventory1.SetDefaultColumnVisibility(false);                  
                }
            }
            else
            {
                SetVisibilityOfCart(true);
                this.Inventory1.SetDefaultColumnVisibility(false);
            }
        }
        else
        {
            SetVisibilityOfCart(false);
            this.Inventory1.SetDefaultColumnVisibility(true);
        }

        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            if (dm.GetCache("AllCurrentCustomerShiptos") != null)
            {
                DataTable table = (DataTable)dm.GetCache("AllCurrentCustomerShiptos");
                int sequence = (int)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["seq_num"];
                DataRow[] rows = table.Select("seq_num = " + sequence.ToString());

                if (rows.Length > 0)
                {
                    if ((bool)rows[0]["nonsalable"] == true)
                    {
                        SetVisibilityOfCart(false);
                        this.Inventory1.SetDefaultColumnVisibility(true);
                    }
                    else
                    {
                        SetVisibilityOfCart(true);
                        this.Inventory1.SetDefaultColumnVisibility(false);
                    }
                }
            }
            else
            {
                if ((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"] == true)
                {
                    SetVisibilityOfCart(false);
                    this.Inventory1.SetDefaultColumnVisibility(true);
                }
                else
                {
                    SetVisibilityOfCart(true);
                    this.Inventory1.SetDefaultColumnVisibility(false);
                }
            }
        }
    }

    private void SelectActiveTab()
    {
        string tabName = Request.QueryString["Display"];

        if (Session["SingleShipto"] != null)
        {
            tabName = ""; // Customer Tab is hidden, so don't give focus to it
        }
       
        if ((string)Session["GenericLogin"] == "False")
        {
            switch (tabName)
            {
                case "Customer Locator": // Login case only
                case "Accounts": // Postback case
                    {
                        if (tabName == "Customer Locator")
                        {
                            dvSearchTextBox.Visible = false;
                        }
                        ActivateControl("CustomerTabPanel1");
                        break;
                    }
                case "Sales Orders":
                    {
                        ActivateControl("SalesOrders1");
                        break;
                    }
                case "Credit Memos":
                    {
                        ActivateControl("CreditMemos1");
                        break;
                    }
                case "Quotes":
                    {
                        ActivateControl("QuoteDecisionTree");
                        break;
                    }
                case "Invoices":
                    {
                        ActivateControl("Invoices1");
                        break;
                    }
                case "Inventory":
                    {
                        ActivateControl("Inventory1");
                        break;
                    }
                case "Billing":
                    {
                        ActivateControl("Payments1");
                        break;
                    }
                default:
                    {
                        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

                        dm.SetCache("SecurityApplied", true);

                        ActivateControl("Home1");
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);

                        //need to manually set this for SetFromPage method...
                        tabName = "Home";
                        break;
                    }
            }
        }

        this.SetFromPage(tabName);
    }

    private void ResetTabContents(string branchID, string shiptoObjString)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        this.CustomerTabPanel1.Reset(branchID);
        this.Quotes1.Reset(branchID, shiptoObjString);
        this.QuoteRelease.Reset("MainTabCalling");
        this.QuoteEdit.Reset("MainTabCalling");
        this.QuoteEditRetail_Info.Reset("MainTabCalling");
        this.SalesOrders1.Reset(branchID, shiptoObjString);
        this.CreditMemos1.Reset(branchID, shiptoObjString);
        this.Invoices1.Reset(branchID, shiptoObjString);

        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
            this.Inventory1.Reset(branchID, shiptoObjString, (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]);
        else
            this.Inventory1.Reset(branchID, shiptoObjString, false);

        this.OrderTab1.Reset(branchID, shiptoObjString);
        this.Payments1.Reset(branchID, shiptoObjString);
    }

    private void SetFromPage(string tabHeaderText)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dm.SetCache("Pagefrom", "./Default.aspx?Display=" + tabHeaderText);
        dm = null;
    }

    protected void WebDataMenu1_ItemClick(object sender, Infragistics.Web.UI.NavigationControls.DataMenuItemEventArgs e)
    {

        /*aInput.Category = this.Page.Request.Url.ToString().Replace(":", "").Replace("/", "-").Replace("--", "-"); */

        AnalyticsInput aInput = new AnalyticsInput();
       
        aInput.Type      = "event";
        aInput.Action    = "Menu Click";
        aInput.Label     = e.Item.Text;
        aInput.Value     = "0";
        
        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

        Session["FromStockNET"] = null;
        Session["IsHome"] = null;

        switch (e.Item.Text.ToLower())
        {
            case "orders":
                if (Session["ActiveProductsPage"] != null)
                {
                    Session["ActiveProductsPage"] = null;
                }
                ActivateControl("SalesOrders1");
                Getsession("SalesOrders1");
                break;

            case "credit memos":
                if (Session["ActiveProductsPage"] != null)
                {
                    Session["ActiveProductsPage"] = null;
                }
                ActivateControl("CreditMemos1");
                Getsession("CreditMemos1");
                break;

            case "quotes":
                if (Session["ActiveProductsPage"] != null)
                {
                    Session["ActiveProductsPage"] = null;
                }
                ActivateControl("QuoteDecisionTree");
                Getsession("QuoteDecisionTree");
                break;

            case "invoices":
                if (Session["ActiveProductsPage"] != null)
                {
                    Session["ActiveProductsPage"] = null;
                }
                ActivateControl("Invoices1");
                Getsession("Invoices1");
                break;

            case "products":
                UserControls_Inventory ucInventory = this.FindControl("Inventory1") as UserControls_Inventory;
                ucInventory.RetainSelectedDetails();
                ActivateControl("Inventory1");
                Session["ActiveProductsPage"] = 1;
                Getsession("Inventory1");
                break;

            case "billing":
                this.Payments1.Reset("", ""); //MDM - to reset to CC mode if using.

                Session["WebHierarchicalDataGridDetail_DataSource"] = null;
                Session["dspvarDataSet"] = null;

                if (Session["ActiveProductsPage"] != null)
                {
                    Session["ActiveProductsPage"] = null;
                }
                ActivateControl("Payments1");
                Getsession("Payments1");
                break;
        }
    }

    public void ActivateControl(string controlId)
    {
       
        AllTabsOff();
        pnlContentContainer.Visible = true;
        HideAllUserControls(pnlContentContainer);

        if (controlId == "QuoteDecisionTree")
        {
            if (Session["QuoteReleaseVisible"] != null && (bool)Session["QuoteReleaseVisible"])
                MakeContentVisible(pnlContentContainer, "QuoteRelease");
            else if (Session["QuoteEditVisible"] != null && (bool)Session["QuoteEditVisible"])
                MakeContentVisible(pnlContentContainer, "QuoteEdit");
            else if (Session["QuoteEditAddItemsVisible"] != null && (bool)Session["QuoteEditAddItemsVisible"])
                MakeContentVisible(pnlContentContainer, "QuoteEditAddItems");
            else
                MakeContentVisible(pnlContentContainer, "Quotes1");
        }
        else
            MakeContentVisible(pnlContentContainer, controlId);
    }

    public void Getsession(string contentID)
    {

        DataTable dtBackDataset = new DataTable();

        if (Session["BackDataset"] == null)
        {


            dtBackDataset.Columns.Add("Order");
            dtBackDataset.Columns.Add("TabName");
            DataRow dr;
            dr = dtBackDataset.NewRow();
            dr["Order"] = 1;
            dr["TabName"] = contentID;
            dtBackDataset.Rows.Add(dr);
            dtBackDataset.AcceptChanges();
            dtBackDataset.GetChanges();
            Session["BackDataset"] = dtBackDataset;

        }
        else
        {
            dtBackDataset = (DataTable)Session["BackDataset"];
            if (dtBackDataset.Rows.Count > 0)
            {
                DataRow dr;
                dr = dtBackDataset.NewRow();
                dr["Order"] = dtBackDataset.Rows.Count + 1;
                dr["TabName"] = contentID;
                dtBackDataset.Rows.Add(dr);
                dtBackDataset.AcceptChanges();
                dtBackDataset.GetChanges();
                Session["BackDataset"] = dtBackDataset;
            }
            else
            {
                DataRow dr;
                dr = dtBackDataset.NewRow();
                dr["Order"] = 1;
                dr["TabName"] = contentID;
                dtBackDataset.Rows.Add(dr);
                dtBackDataset.AcceptChanges();
                dtBackDataset.GetChanges();
                Session["BackDataset"] = dtBackDataset;
            }
        }


        Session["BackDataset"] = dtBackDataset;
    }

    protected void HideAllUserControls(Panel p)
    {
        if (p.HasControls())
        {
            foreach (Control c in p.Controls)
            {
                if (c as UserControl != null)
                    c.Visible = false;
            }
        }
    }

    protected void MakeContentVisible(Panel p, string contentID)
    {
        UserControl control = (UserControl)p.FindControl(contentID);
        if (control != null)
            control.Visible = true;
    }

    protected void imgCustAddress_OnClick(object sender, ImageClickEventArgs e)
    {
        
        AnalyticsInput aInput = new AnalyticsInput();
        aInput.Type = "event";
        aInput.Action = "Products - Upper Keyword Search";
        aInput.Label =  "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

        Session["UIisOnCheckout"] = false;

        if (Inventory1.FindControl("tblInventory") != null)
        {
            if (Inventory1.FindControl("tblInventory").FindControl("trSearch").FindControl("Div1").FindControl("TabContainer1").FindControl("tbAdvSearch").FindControl("InventorySearchFieldPanel").FindControl("btnFind") != null)
            {
                Button btny = (Button)Inventory1.FindControl("tblInventory").FindControl("trSearch").FindControl("Div1").FindControl("TabContainer1").FindControl("tbAdvSearch").FindControl("InventorySearchFieldPanel").FindControl("btnFind");
                btny.Click += new EventHandler(Inventory1.btnFind_Click);
                ActivateControl("Inventory1");

                Inventory1.FinDetails(txtProductSearchKeyword.Text);
                txtProductSearchKeyword.Text = "";
                WebDataMenu1.Items[0].Selected = true;
            }
        }
    }

    private Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

        return null;
    }

    protected void btnCustAddress_OnClick(object sender, EventArgs e)
    {
        Session["btnCustAddress_OnClick"] = 1;
        Session["UIisOnCheckout"] = false;

        UserControls_Inventory control = FindControlRecursive(this, "Inventory1") as UserControls_Inventory;

        control.FinDetails(txtProductSearchKeyword.Text);
        txtProductSearchKeyword.Text = "";

        Session["InventorybtnReturnToItem_Click"] = 1;
        ActivateControl("Inventory1");

        WebDataMenu1.Items[0].Selected = true;
       
    }

    private void FetchStockNetItem()
    {
        string savedURL = ((string)Session["FromStockNET"]).ToLower();

        if (savedURL.ToLower().Contains("source=stocknet"))
        {
            string[] nameValuePairs = savedURL.Split('&');
            foreach (string str in nameValuePairs)
            {
                if (str.ToLower().StartsWith("item=") && Session["InventoryIsAllowed"] != null && (bool)Session["InventoryIsAllowed"] == true && str.Substring(5) != (string)Session["StockNETPreviousItem"])
                {
                    ActivateControl("Inventory1");

                    TabContainer tabContainer = this.Inventory1.FindControl("TabContainer1") as TabContainer;
                    tabContainer.ActiveTab = tabContainer.Tabs[1];

                    WebTextEditor tbx = tabContainer.Tabs[1].FindControl("txtInventorySearchValue") as WebTextEditor;
                    tbx.Text = str.Substring(5);
                    this.Inventory1.btnFind_Click(tbx, null);
                    Session["StockNETPreviousItem"] = tbx.Text;
                    Session["FromStockNET"] = null;
                }
            }
        }
    }

    private void SetVisibilityOfAccountChangeLink(bool visible)
    {
        Control parentTemplate = this.Parent.TemplateControl;
        if (parentTemplate != null)
        {
            LinkButton l = (LinkButton)parentTemplate.FindControl("lnkChangeCustomer");
            if (l != null)
            {
                l.Visible = visible;
            }
        }
    }

    private void SetVisibilityOfCart(bool visible)
    {
        Control parentTemplate = this.Parent.TemplateControl;
        if (parentTemplate != null)
        {
            Panel p = (Panel)parentTemplate.FindControl("CartPanel");
            if (p != null)
                p.Visible = visible;
        }
    }

    protected void txtProductSearchKeyword_EnterKeyPress1(object sender, EventArgs e)
    {
        Session["UIisOnCheckout"] = false;
        ActivateControl("Inventory1");

        if (Inventory1.FindControl("tblInventory") != null)
        {
            if (Inventory1.FindControl("tblInventory").FindControl("trSearch").FindControl("Div1").FindControl("TabContainer1").FindControl("tbAdvSearch").FindControl("InventorySearchFieldPanel").FindControl("btnFind") != null)
            {
                Button btny = (Button)Inventory1.FindControl("tblInventory").FindControl("trSearch").FindControl("Div1").FindControl("TabContainer1").FindControl("tbAdvSearch").FindControl("InventorySearchFieldPanel").FindControl("btnFind");
                btny.Click += new EventHandler(Inventory1.btnFind_Click);
                btny.Click += new EventHandler(Inventory1.btnFind_Click);
                Inventory1.FinDetails(txtProductSearchKeyword.Text);
                txtProductSearchKeyword.Text = "";
            }
        }
    }

    protected void imgLogo_Click(object sender, ImageClickEventArgs e)
    {
        if ((Session["btnCustAddress_OnClick"]) != null)
        {
            if ((int)Session["btnCustAddress_OnClick"] != 1)
            {
                hypHome_Click(null, null);
            }
            else if ((int)Session["btnCustAddress_OnClick"] == 1)
            {
                ChooseVisibleContent("Inventory1");
                Session["btnCustAddress_OnClick"] = 0;

                //UserControls_Inventory
                string sText = string.Empty;
                Infragistics.Web.UI.EditorControls.WebTextEditor TextBox1 = (Infragistics.Web.UI.EditorControls.WebTextEditor)this.Page.Master.FindControl("cphMaster1").Controls[0].FindControl("txtProductSearchKeyword");

                if (TextBox1 != null)
                {
                    sText = TextBox1.Text;
                }
                UserControls_Inventory defaultControl = (UserControls_Inventory)this.Page.Master.FindControl("cphMaster1").Controls[0].Controls[6].FindControl("Inventory1");

                if (defaultControl != null)
                {
                    if (defaultControl.FindControl("tblInventory") != null)
                    {
                        if (defaultControl.FindControl("tblInventory").FindControl("trSearch").FindControl("Div1").FindControl("TabContainer1").FindControl("tbAdvSearch").FindControl("InventorySearchFieldPanel").FindControl("btnFind") != null)
                        {
                            Button btny = (Button)defaultControl.FindControl("tblInventory").FindControl("trSearch").FindControl("Div1").FindControl("TabContainer1").FindControl("tbAdvSearch").FindControl("InventorySearchFieldPanel").FindControl("btnFind");
                            btny.Click += new EventHandler(defaultControl.btnFind_Click);

                            defaultControl.FinDetails(sText);
                            TextBox1.Text = "";
                        }
                    }
                }
            }
        }
        else
        {  
            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Company Logo Click";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

            hypHome_Click(null, null);
        }
    }

    protected void hypHome_Click(object sender, EventArgs e)
    {
        ChooseVisibleContent("Home1");
    }

    protected void ChooseVisibleContent(string contentID)
    {
        MainTabControl tabs = (MainTabControl)FindControlRecursive(this.Page.Master.FindControl("cphMaster1"), "MainTabControl1");
        if (tabs != null)
        {
            tabs.AllTabsOff();
            WebDataMenu menu = (WebDataMenu)tabs.FindControl("WebDataMenu1");
            if (menu != null)
                menu.SelectedItem = null;
        }
        
        Panel p = (Panel)FindControlRecursive(this.Page.Master.FindControl("cphMaster1"), "pnlContentContainer");
        if (p != null)
        {
            p.Visible = true;
            HideAllContent(p);
            MakeContentVisible2(p, contentID);
        }
    }

    protected void HideAllContent(Panel p)
    {
        foreach (Control cntrls in p.Controls)
        {
            if (cntrls as UserControl != null)
                cntrls.Visible = false;
        }
    }

    protected void MakeContentVisible2(Panel p, string contentID)
    {
        UserControl control = (UserControl)p.FindControl(contentID);
        if (control != null)
            control.Visible = true;
    }
}
