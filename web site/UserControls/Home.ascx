<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Home.ascx.cs" Inherits="UserControls_Home" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc2" %>
<%@ Register Src="SimplePanel.ascx" TagName="SimplePanel" TagPrefix="uc1" %>



    <div class="left-section">
    <div class="lft-cont-sec">
        <h2>
             Company Information
        </h2>

             <asp:Panel ID="CompanyFieldPanel" runat="server" CssClass="SimplePanelBody" Width="100%">
                            <asp:Label ID="CompanyDescLabel" runat="server" Text="Description" CssClass="HomeCompanyDescLabel"></asp:Label><br />
                            <asp:Label ID="CompanyAddressLabel" runat="server" Text="Address" CssClass="HomeCompanyAddressLabel"></asp:Label>
                        </asp:Panel>
            </div>
    </div>

    <div class="right-section">
        <div class="container-sec">
     <div class="custom-grid abc" >
         <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
         <asp:Button ID="btnreload" runat="server" Visible="false" OnClick="btnreload_Click" />
         </div>
            </div>
    </div>

<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
    SelectCommand="SELECT Category_Name FROM Category WHERE (Active = 1) ORDER BY Sequence_Number">
</asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
    SelectCommand="SELECT Message_Text FROM Message WHERE (Category_Name = @Category_Name) AND (Active = 1) ORDER BY Category_Name, Sequence_Number">
    <SelectParameters>
        <asp:Parameter Name="Category_Name" Type="String" />
    </SelectParameters>
</asp:SqlDataSource>
