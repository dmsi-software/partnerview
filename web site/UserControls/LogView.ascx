<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LogView.ascx.cs" Inherits="UserControls_LogViewer" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxcontrol" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="left-section">
            <div class="lft-cont-sec">
                <div id="my-tab-content">
                    <div class="search-opt">
                        <div class="advsearch">
                            <div class="SimplePanelFixedWidthDIV">
                                <div>
                                    <asp:Panel ID="LogViewerPanel" runat="server" CssClass="CollapsibleHeaderPanel">
                                        <div class="NonCollapsibleHeaderDIV" style="vertical-align: middle; text-align: left">
                                            Search Criteria<br />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="LogViewerFieldPanel" runat="server" CssClass="SearchPanelBody" DefaultButton="btnFind">
                                        <asp:Label ID="Label3" runat="server" CssClass="FieldLabel" Text="Search by"/>

                                        <br />
                                        <asp:DropDownList ID="ddlTranID" runat="server" Width="197px">
                                            <asp:ListItem Value="remoteuser">Remote User ID</asp:ListItem>
                                            <asp:ListItem Value="authuser">Authenticated User ID</asp:ListItem>
                                            <asp:ListItem Value="remoteip">Remote IP Address</asp:ListItem>
                                            <asp:ListItem Value="type">Event Type</asp:ListItem>
                                            <asp:ListItem Value="baseurl">Base URL</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <asp:TextBox ID="txtTranID" runat="server" Width="190px"/>
                                       
                                        <asp:Label ID="FieldLabel" runat="server" Text="Limit search to" CssClass="FieldLabel"></asp:Label><br />
                                        <asp:DropDownList ID="ddlTimeSpan" runat="server" Width="197px">
                                            <asp:ListItem Selected="True" Value=".02">Last 30 Minutes</asp:ListItem>
                                            <asp:ListItem Value=".0416">Last Hour</asp:ListItem>
                                            <asp:ListItem Value=".5">Last 12 Hours</asp:ListItem>
                                            <asp:ListItem Value="1">Last 24 Hours</asp:ListItem>
                                            <asp:ListItem Value="7">Last 7 days</asp:ListItem>
                                            <asp:ListItem Value="9999">Select All</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Button ID="btnFind" runat="server" CssClass="Button" Height="24px" OnClick="btnFind_Click"
                                            Text="Go" Width="35px" />
                                    </asp:Panel>
                                </div>
                                <ajaxcontrol:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" AutoCollapse="False"
                                    AutoExpand="false" CollapseControlID="PreferencePanel" Collapsed="False" CollapsedImage="~/images/collapse_blue.jpg"
                                    CollapsedSize="0" CollapsedText="Preferences" ExpandControlID="PreferencePanel"
                                    ExpandDirection="Vertical" ExpandedImage="~/images/expand_blue.jpg" ExpandedText="Preferences"
                                    ImageControlID="PreferenceImage" ScrollContents="False" SuppressPostBack="true"
                                    TargetControlID="PreferenceFieldPanel">
                                </ajaxcontrol:CollapsiblePanelExtender>
                                <div>
                                    <asp:Panel ID="PreferencePanel" runat="server" CssClass="CollapsibleHeaderPanel"
                                        onmouseover="this.className='CollapsibleHeaderPanelHover'" onmouseout="this.className='CollapsibleHeaderPanel'">
                                        <div class="CollapsibleHeaderDIV">
                                            <asp:Image ID="PreferenceImage" runat="server" ImageAlign="Right" ImageUrl="~/images/collapse_blue.jpg" />
                                            Preferences
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="PreferenceFieldPanel" runat="server" CssClass="SimplePanelBody">
                                        <asp:Label ID="lblLogViewPreference" runat="server" Text="Log Entries:" CssClass="FieldLabel"
                                            Width="171px" Visible="false"></asp:Label><br />
                                        <asp:Label ID="Label11" runat="server" Text="Show" Font-Bold="true"></asp:Label>
                                        <asp:DropDownList ID="ddlLogViewRows" runat="server" AutoPostBack="true" Width="50px"
                                            OnSelectedIndexChanged="ddlLogViewRows_SelectedIndexChanged">
                                            <asp:ListItem Selected="True">10</asp:ListItem>
                                            <asp:ListItem>15</asp:ListItem>
                                            <asp:ListItem>20</asp:ListItem>
                                            <asp:ListItem>25</asp:ListItem>
                                            <asp:ListItem>30</asp:ListItem>
                                            <asp:ListItem>40</asp:ListItem>
                                            <asp:ListItem>50</asp:ListItem>
                                            <asp:ListItem>75</asp:ListItem>
                                            <asp:ListItem>100</asp:ListItem>
                                            <asp:ListItem>250</asp:ListItem>
                                            <asp:ListItem>500</asp:ListItem>
                                            <asp:ListItem>All</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblrows" runat="server" Text="Results" Font-Bold="true"></asp:Label>
                                        &nbsp;&nbsp;
                                <ajaxcontrol:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
                                    DropShadow="true" PopupControlID="pnlCustomLogView" PopupDragHandleControlID="pnlDragLogView"
                                    TargetControlID="lnkLogViewCustomView">
                                </ajaxcontrol:ModalPopupExtender>
                                        <asp:LinkButton ID="lnkLogViewCustomView" runat="server"> Select Columns</asp:LinkButton>
                                    </asp:Panel>
                                </div>
                                <div id="pnlCustomLogView">
                                    <div id="pnlDragLogView" runat="server" class="modal-dialog smlwidth">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <asp:Button ID="btnclose" CssClass="close" runat="server" Text="X" />
                                                <h4 class="modal-title">Columns to View</h4>
                                            </div>
                                            <div class="modal-body">
                                                <uc3:ColumnSelector ID="csCustomLogView" runat="server" />
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="right-section">
            <div class="container-sec">

                <div class="custom-grid abc" id="accordion">
                    <h2>
                        <asp:Label ID="lblLogEntries" runat="server" Text="Log Entries"></asp:Label>
                    </h2>
                    <div>


                        <asp:Label ID="lblLogViewError" runat="server" CssClass="ErrorTextRed" Text="Record limit has been exceeded. Add criteria to refine search."
                            Visible="False"></asp:Label>

                        <asp:GridView ID="gvLogView" runat="server" AllowPaging="True" AllowSorting="True"
                            OnPageIndexChanging="gvLogView_PageIndexChanging" OnRowCreated="gvLogView_RowCreated"
                            OnRowDataBound="gvLogView_RowDataBound" OnSorting="gvLogView_Sorting" Width="100%"
                            AutoGenerateColumns="False" HorizontalAlign="Left" CssClass="grid-tabler">
                            <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                            <Columns>
                                <asp:BoundField DataField="eventid" HeaderText="Event ID" ReadOnly="True" SortExpression="eventid">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="type" HeaderText="Event Type" ReadOnly="True" SortExpression="type">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="time" HeaderText="Event Timestamp" ReadOnly="True" SortExpression="time">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="remoteip" HeaderText="Remote IP" ReadOnly="True" SortExpression="remoteip">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="remotehost" HeaderText="Remote Host IP" ReadOnly="True"
                                    SortExpression="remotehost">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="remoteuser" HeaderText="Remote User ID" ReadOnly="True"
                                    SortExpression="remoteuser">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="unmappedremoteuser" HeaderText="Unmapped Remote User"
                                    ReadOnly="True" SortExpression="unmappedremoteuser" Visible="False">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="authuser" HeaderText="Authenticated User ID" ReadOnly="True"
                                    SortExpression="authuser">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="baseurl" HeaderText="Base URL" ReadOnly="True" SortExpression="baseurl">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="querystring" HeaderText="Query String " ReadOnly="True"
                                    SortExpression="querystring">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="http_method" HeaderText="HTTP Method" ReadOnly="True"
                                    SortExpression="http_method">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                    SortExpression="description">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="innerexception" HeaderText="Inner Exception" ReadOnly="True"
                                    SortExpression="innerexception" Visible="False">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="source" HeaderText="Source" ReadOnly="True" SortExpression="source"
                                    Visible="False">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="method" HeaderText="Method" ReadOnly="True" SortExpression="method"
                                    Visible="False">
                                    <ItemStyle Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="trace" HeaderText="Trace" ReadOnly="True" SortExpression="trace"
                                    Visible="False">
                                    <ItemStyle Width="220px" />
                                    <HeaderStyle Width="220px" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>

                    </div>
                    <div class="pagination-sec">
                        <div class="info">
                            <asp:Label ID="lblpageno" runat="server" Text="" />
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </ContentTemplate>
</asp:UpdatePanel>
