﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemListView.ascx.cs"
    Inherits="UserControls_ItemListView" %>
<style type="text/css">
    
</style>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="LeftPane">
            <div style="width:700px; text-align: right; padding-right: 60px;">
                <asp:Button ID="btnViewQuicklist" runat="server" Text="View Quicklist" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnAddtoCart" runat="server" Text="Add to Cart" />
            </div>
            <div>
                &nbsp;</div>
            <div style="width:700px;">
                <asp:UpdatePanel ID="upnl1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <asp:ListView ID="lvProducts" runat="server" ItemPlaceholderID="itemPlaceHolder"
                            DataKeyNames="ProductID" OnItemDataBound="lvProducts_ItemDataBound" EnableViewState="False">
                            <LayoutTemplate>
                                <table id="tblProducts" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr style="visibility: collapse; display: none;">
                                            <th style="width: 120px;">
                                                <asp:Label ID="lblProductImage" runat="server" Text="Product Image" Font-Bold="true"
                                                    ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <th style="width: 25px;">
                                                &nbsp;
                                            </th>
                                            <th style="width: 450px;">
                                                <asp:Label ID="lblProductDetails" runat="server" Text="Product Details" Font-Bold="true"
                                                    ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <th style="width: 25px;">
                                                &nbsp;
                                            </th>
                                            <th style="width: 250px;">
                                                <asp:Label ID="lblProductPriceDetails" runat="server" Text="Product Price Details"
                                                    Font-Bold="true" ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <div id="itemPlaceHolder" runat="server">
                                            </div>
                                        </tr>
                                    </thead>
                                    <tbody id="groupplaceholder" runat="server">
                                    </tbody>
                                </table>
                                <%--                        <asp:DataPager ID="ItemDataPager" runat="server" PageSize="5">
                            <Fields>
                                <asp:NumericPagerField ButtonCount="3" />
                            </Fields>
                        </asp:DataPager>--%>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr style="height: 100px;">
                                    <td align="center" style="width: 120px; height: 100px; padding-left: 10px;">
                                        <asp:ImageButton ID="imgProduct" runat="server" ImageAlign="Middle" ImageUrl="~/Images/logo.png"
                                            Width="100px" Height="100px" BorderWidth="1px" BorderColor="Black" BorderStyle="Solid" />
                                        <asp:HiddenField ID="hfID" runat="server" />
                                    </td>
                                    <td style="width: 25px;">
                                        &nbsp;
                                    </td>
                                    <td style="width: 450px;">
                                        <asp:LinkButton ID="lbtProdDesc" runat="server" Text="Product Description"></asp:LinkButton>
                                        <br />
                                        <asp:Label ID="lblAvailable" runat="server" Text="Available: " Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblAvilableCount" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblAvilableCountText" runat="server" Text="/ EACH" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblPartNumber" runat="server" Text="Part No: "></asp:Label>
                                        <asp:Label ID="lblPartNo" runat="server" Text=""></asp:Label>
                                        <br />
                                        <asp:Label ID="lblProductGroup" runat="server" Text="Product Group: "></asp:Label>
                                        <asp:Label ID="lblMajorMinor" runat="server" Text="Major / Minor "></asp:Label>
                                    </td>
                                    <td style="width: 25px;">
                                        &nbsp;
                                    </td>
                                    <td style="width: 250px;">
                                        <asp:Label ID="lblPrice" runat="server" Text=" $ " Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblPriceText" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblPriceText1" runat="server" Text="/ EACH" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblOrder" runat="server" Text="Qty: " Font-Bold="true"></asp:Label>
                                        <asp:TextBox ID="txtOrder" runat="server" Width="50px" Height="12px"></asp:TextBox>
                                        <asp:Label ID="lblOrderText" runat="server" Text=" EACH" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblMinPack" runat="server" Text="Must be ordered in multiple of 10"></asp:Label>
                                        <br />
                                        <asp:LinkButton ID="lbtnQuicklist" runat="server" Text="Add to my quicklist"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 15px;">
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td>
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td>
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td>
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td style="padding-right: 20px;">
                                        <hr style="border-color: Black;" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <%--                    <ItemSeparatorTemplate>
                        <div class="productItem">
                        </div>
                    </ItemSeparatorTemplate>  --%>
                            <AlternatingItemTemplate>
                                <tr style="height: 100px;">
                                    <td align="center" style="width: 120px; height: 100px; padding-left: 10px;">
                                        <asp:ImageButton ID="imgProduct" runat="server" ImageAlign="Middle" ImageUrl="~/Images/Vision6.jpg"
                                            Width="100px" Height="100px" BorderWidth="1px" BorderColor="Black" BorderStyle="Solid" />
                                        <asp:HiddenField ID="hfID" runat="server" />
                                    </td>
                                    <td style="width: 25px;">
                                        &nbsp;
                                    </td>
                                    <td style="width: 450px;">
                                        <asp:LinkButton ID="lbtProdDesc" runat="server" Text="Product Description"></asp:LinkButton>
                                        <br />
                                        <asp:Label ID="lblAvailable" runat="server" Text="Available: " Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblAvilableCount" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblAvilableCountText" runat="server" Text="/ EACH" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblPartNumber" runat="server" Text="Part No: "></asp:Label>
                                        <asp:Label ID="lblPartNo" runat="server" Text=""></asp:Label>
                                        <br />
                                        <asp:Label ID="lblProductGroup" runat="server" Text="Product Group: "></asp:Label>
                                        <asp:Label ID="lblMajorMinor" runat="server" Text="Major / Minor "></asp:Label>
                                    </td>
                                    <td style="width: 25px;">
                                        &nbsp;
                                    </td>
                                    <td style="width: 250px;">
                                        <asp:Label ID="lblPrice" runat="server" Text=" $ " Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblPriceText" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblPriceText1" runat="server" Text="/ EACH" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblOrder" runat="server" Text="Qty: " Font-Bold="true"></asp:Label>
                                        <asp:TextBox ID="txtOrder" runat="server" Width="50px" Height="12px"></asp:TextBox>
                                        <asp:Label ID="lblOrderText" runat="server" Text=" EACH" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblMinPack" runat="server" Text="Must be ordered in multiple of 10"></asp:Label>
                                        <br />
                                        <asp:LinkButton ID="lbtnQuicklist" runat="server" Text="Add to my quicklist"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 15px;">
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td>
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td>
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td>
                                        <hr style="border-color: Black;" />
                                    </td>
                                    <td style="padding-right: 20px;">
                                        <hr style="border-color: Black;" />
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:ListView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </td>
    </tr>
</table>
