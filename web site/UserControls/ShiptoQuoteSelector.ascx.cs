using System;
using System.Data;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI;

public partial class UserControls_ShiptoQuoteSelector : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate();
    public event OKClickedDelegate OnOKClicked;
    
    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    public delegate void CancelClickedDelegate();
    public event CancelClickedDelegate OnCancelClicked;

    private dsQuoteDataSet _quoteDataSet;
    public dsQuoteDataSet QuoteDataSet
    {
        get { return _quoteDataSet;  }
        set { _quoteDataSet = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (_quoteDataSet != null)
        {
            if (Session["ShiptoQuote_txtCustomerName"] == null)
            {
                if (_quoteDataSet.pvttquote_header.Rows.Count > 0)
                {
                    DataRow row = _quoteDataSet.pvttquote_header.Rows[0];

                    Session["ShiptoQuote_txtCustomerName"] = row["shiptoname"];
                    Session["ShiptoQuote_txtAddress1"] = row["shipToAddr1"];
                    Session["ShiptoQuote_txtAddress2"] = row["shipToAddr2"];
                    Session["ShiptoQuote_txtAddress3"] = row["shipToAddr3"];
                    Session["ShiptoQuote_txtCity"] = row["shipToCity"];
                    Session["ShiptoQuote_txtState"] = row["shipToState"];
                    Session["ShiptoQuote_txtZip"] = row["shipToZip"];
                    Session["ShiptoQuote_txtCountry"] = row["shipToCountry"];
                    Session["ShiptoQuote_txtPhone"] = row["shipToPhone"];
                }
            }
        }

        if (Session["ShiptoQuote_txtCustomerName"] != null)
        {
            txtCustomerName.Text = Convert.ToString(Session["ShiptoQuote_txtCustomerName"]).Trim();
            txtAddress1.Text = Convert.ToString(Session["ShiptoQuote_txtAddress1"]).Trim();
            txtAddress2.Text = Convert.ToString(Session["ShiptoQuote_txtAddress2"]).Trim();
            txtAddress3.Text = Convert.ToString(Session["ShiptoQuote_txtAddress3"]).Trim();
            txtCity.Text = Convert.ToString(Session["ShiptoQuote_txtCity"]).Trim();
            txtState.Text = Convert.ToString(Session["ShiptoQuote_txtState"]).Trim();
            txtZip.Text = Convert.ToString(Session["ShiptoQuote_txtZip"]).Trim();
            txtCountry.Text = Convert.ToString(Session["ShiptoQuote_txtCountry"]).Trim();
            txtPhone.Text = Convert.ToString(Session["ShiptoQuote_txtPhone"]).Trim();
        }
        else
        {
            txtCustomerName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
        }

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        Session["ShiptoQuote_txtCustomerName"] = txtCustomerName.Text.Trim();
        Session["ShiptoQuote_txtAddress1"] = txtAddress1.Text.Trim();
        Session["ShiptoQuote_txtAddress2"] = txtAddress2.Text.Trim();
        Session["ShiptoQuote_txtAddress3"] = txtAddress3.Text.Trim();
        Session["ShiptoQuote_txtCity"] = txtCity.Text.Trim();
        Session["ShiptoQuote_txtState"] = txtState.Text.Trim();
        Session["ShiptoQuote_txtZip"] = txtZip.Text.Trim();
        Session["ShiptoQuote_txtCountry"] = txtCountry.Text.Trim();
        Session["ShiptoQuote_txtPhone"] = txtPhone.Text.Trim();

        if (sender != null && e != null)
        {
            /* Analytics Tracking */

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Quote Release - Shipping Address Update - OK Button";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        if (OnOKClicked != null)
            OnOKClicked(); // will be caught by the parent
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (OnCancelClicked != null)
            OnCancelClicked(); // will be caught by the parent
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtCustomerName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtZip.Text = "";
        txtCountry.Text = "";
        txtPhone.Text = "";

        Session["ShiptoQuote_txtCustomerName"] = null;
        Session["ShiptoQuote_txtAddress1"] = null;
        Session["ShiptoQuote_txtAddress2"] = null;
        Session["ShiptoQuote_txtAddress3"] = null;
        Session["ShiptoQuote_txtCity"] = null;
        Session["ShiptoQuote_txtState"] = null;
        Session["ShiptoQuote_txtZip"] = null;
        Session["ShiptoQuote_txtCountry"] = null;
        Session["ShiptoQuote_txtPhone"] = null;

        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }
}
