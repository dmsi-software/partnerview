﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Threading;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_CQPdfViewer : System.Web.UI.UserControl
{
    private string _currentGuid = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TaxRateHelper trh = new TaxRateHelper(Convert.ToInt32(HttpContext.Current.Request.QueryString["tranid"]), Profile);
            trh.SaveDefaultTaxRateIfEmpty();

            //_tranID = 
            _currentGuid = Guid.NewGuid().ToString();

            // Store the unique indentifier for this page
            lblContext.Text = _currentGuid;

            // Clear previously fetched document
            Session.Remove(_currentGuid + "Url");
            Session.Remove(_currentGuid);
            Session.Remove("NO" + _currentGuid);
            Session.Remove("ERROR" + _currentGuid);
            Session.Remove(_currentGuid + "Port");

            Session[_currentGuid + "StartDate"] = DateTime.Now;
            Session[_currentGuid + "CurrentGuid"] = _currentGuid;

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Quotes - Retail Quote Icon Click";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Document doc = new Dmsi.Agility.Data.Document("", out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            doc.TranID = Request.QueryString["TranID"];
            doc.Type = Request.QueryString["Type"];

            // Start listener
            Thread waitThread = new Thread(new ParameterizedThreadStart(WaitForDocument));
            waitThread.Priority = ThreadPriority.Lowest;
            waitThread.Start(doc);

            Timer1.Enabled = true;
        }
        else
        {
            // Retrieve some pertinent information about this page
            string[] parts = lblContext.Text.Split(new char[] { ',' });
            _currentGuid = parts[0];
        }
    }

    private void WaitForDocument(object oObject)
    {
        byte[] data = null;
        DateTime start = (DateTime)Session[_currentGuid + "StartDate"];

        string parsedURL = Profile.RetailQuoteLogoURL;
        if (parsedURL.Trim().Length > 0)
        {
            if (!(parsedURL.ToLower().StartsWith("http://") || parsedURL.ToLower().StartsWith("https://")))
                parsedURL = "http://" + parsedURL;
        }

        // blocks and waits for response
        Dmsi.Agility.Data.Document doc = (Dmsi.Agility.Data.Document)oObject;
        data = doc.GetCQFileBytes(doc.ContextID, parsedURL);

        if (data != null)
            Session[_currentGuid] = data;
        else
            Session["NO" + _currentGuid] = true;
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        DateTime start = (DateTime)Session[_currentGuid + "StartDate"];

        if ((DateTime.Now - start).Minutes >= 2) // wait for 2 minutes
            Session["NO" + _currentGuid] = true;

        if (Session[_currentGuid + "Url"] != null)
        {
            Timer1.Enabled = false;
            Server.Transfer((string)Session[_currentGuid + "Url"]);
        }
        else if (Session[_currentGuid] != null)
        {
            Timer1.Enabled = false;
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "filename=" + _currentGuid + ".pdf");
            byte[] data = (byte[])Session[_currentGuid];
            Session.Remove(_currentGuid);
            Response.OutputStream.Write(data, 0, data.Length);
            Response.End();
        }
        else if (Session["NO" + _currentGuid] != null)
        {
            Timer1.Enabled = false;
            lblMessage.Text = "Unable to retrieve the form.<BR />Please try again.<br />&nbsp;<br />If the problem persists, please contact the administrator.</P>";
            imgProgress.Visible = false;
        }
        else if (Session["ERROR" + _currentGuid] != null)
        {
            Timer1.Enabled = false;
            lblMessage.Text = (string)Session["ERROR" + _currentGuid];
            imgProgress.Visible = false;
        }
    }
}
