﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PasswordMaintSettings.ascx.cs"
    Inherits="UserControls_PasswordMaintSettings" %>
<style type="text/css">
    .style7 {
        height: 23px;
    }

    .style11 {
        height: 23px;
        vertical-align: bottom;
        margin-left: 12px;
    }
</style>

<div class=" col-lg-12">
    
    <div class="theme-row form">

         <div class="msg-error" id="dverr" runat="server" visible="false">
            <asp:Label ID="lblValidationError" runat="server" CssClass="ErrorTextRed"></asp:Label>
        </div>
        <ul class="default-list passwordfocus">
            <li>
                <label>
                    Current password</label>

                <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password"></asp:TextBox>&nbsp;
                            <asp:Label ID="lblOldPass" runat="server" CssClass="ErrorTextRed"></asp:Label>
            </li>
            <li>
                <label>
                    New password</label>
                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" ></asp:TextBox>&nbsp;
                            <asp:Label ID="lblNewPass" runat="server" CssClass="ErrorTextRed"></asp:Label>

            </li>
            <li>
                <label>
                    Confirm new password</label>
                <asp:TextBox ID="txtConfirmNew" runat="server" TextMode="Password" ></asp:TextBox>
                <asp:Label ID="lblNewConfirm" runat="server" CssClass="ErrorTextRed"></asp:Label>

            </li>
        </ul>



        <div class="note">
            <strong>
                <asp:Label ID="ChangePasswordTitle" runat="server" Text="Change your password"></asp:Label>

            </strong>
        </div>
        <div class="update-row">
            <asp:Button ID="btnChange" runat="server" Text="Change Password" class="btn btn-primary changebutton" OnClick="btnChange_Click" />

        </div>
       
        <div class="msg-success" id="dvsuccess" runat="server" visible="false">
            <asp:Label ID="lblSuccess" runat="server" Font-Bold="False" Text="lblSuccess" Width="100%"></asp:Label>
        </div>
    </div>
</div>




