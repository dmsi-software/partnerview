using System;
using System.Data;
using System.Web.UI.WebControls;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
public partial class UserControls_Customers : System.Web.UI.UserControl
{
    public delegate void CustomerSelectionChangedDelegate();
    public event CustomerSelectionChangedDelegate OnCustomerSelectionChanged;

    public delegate void ShiptoSelectionChangedDelegate();
    public event ShiptoSelectionChangedDelegate OnShiptoSelectionChanged;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.ColumnSelector1.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(ColumnSelector1_OnOKClicked);
        this.ColumnSelector1.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(ColumnSelector1_OnResetClicked);
        this.ColumnSelector2.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(ColumnSelector2_OnOKClicked);
        this.ColumnSelector2.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(ColumnSelector2_OnResetClicked);
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (!this.IsPostBack)
        {
            // Add any initial setting that applies to the gridview before binding it.
            this.ddlCustomersRows.SelectedIndex = Profile.ddlCustomersRows_SelectedIndex;
            int pageSize = Convert.ToInt32(ddlCustomersRows.SelectedValue);
            this.gvCustomers.PageSize = pageSize;
            this.gvShiptos.PageSize = pageSize;
            string buildcolumn = Profile.gvCustomers_Columns_Visible;

            //when user first time logins he will be getting appropriate default columns

            if ((buildcolumn.Length) == (gvCustomers.Columns.Count - 1))
            { }
            else
            {
                Profile.gvCustomers_Columns_Visible = "1" + buildcolumn;
            }

            InitializeColumnVisibility(gvCustomers, "gvCustomers_Columns_Visible");
            Profile.gvCustomers_Columns_Visible = buildcolumn;

            // Create search field dropdown
            if (gvCustomers.Columns.Count > 0)
            {
                for (int i = 0; i < gvCustomers.Columns.Count; i++)
                {
                    if ((i > 0) && (i <= 11))
                    {
                        ListItem item = new ListItem(gvCustomers.Columns[i].HeaderText, ((BoundField)gvCustomers.Columns[i]).DataField);
                        this.ddlSearchField.Items.Add(item);
                        item = null;
                    }
                }

                ListItem shiptoItem = new ListItem("Ship-to Name", "shipto_name");
                ddlSearchField.Items.Insert(2, shiptoItem);
            }
            else
                this.ddlSearchField.Items.Add("All"); // dummy entry
        }

        // this is needed for the columns to show correctly
        if (dm.GetCache("NoCustomersAvailable") != null)
            this.AssignCustomersDataSource(true);

        if (dm.GetCache("NoShiptosAvailable") != null)
            this.AssignShiptosDataSource(true);

        if (!this.IsPostBack)
        {
            // this will allow the sort image to be rendered initially
            this.ddlSearchField.SelectedIndex = 0;
            RefreshCustomerGrid(false);

            // pre-fetch ship-tos of default customer
            if (dm.GetCache("CustomerRowForInformationPanel") != null)
            {
                dm.SetCache("CurrentCustomerRow", dm.GetCache("CustomerRowForInformationPanel"));
                if (Session["GenericLogin"] != null)
                {
                    if ((string)Session["GenericLogin"] != "True")
                    {
                        this.AssignShiptosDataSingleCust(false);
                    }
                }
            }
        }

        dm = null;
    }

    #region Public Methods

    /// <summary>
    /// Call this to clear bound data
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    /// <param name="customer">Current Customer Object</param>
    public void Reset()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        Session["orderDateStrings"] = null;
        Session["quoteReleaseDateStrings"] = null;
        Session["LastChosenRequestedDeliveryDate"] = null;

        gvCustomers.SelectedIndex = -1;
        gvShiptos.SelectedIndex = -1;

        this.AssignCustomersDataSource(true);
        this.AssignShiptosDataSource(true);
    }

    /// <summary>
    /// Call this to clear bound data when the branch or customer has changed
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    /// <param name="customer">Current Customer Object</param>
    public void Reset(string branchId)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("currentBranchID") == null ||
            (dm.GetCache("currentBranchID") != null && !dm.GetCache("currentBranchID").ToString().Equals(branchId)))
        {
            this.Reset();
        }

        dm = null;
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Routine to build the string corresponding to the visible columns of the grid
    /// </summary>
    /// <param name="checkBoxList"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    private string BuildColumnVisibilityProperty(CheckBoxList checkBoxList, GridView gridView)
    {
        string outputString = "";
        char c;

        for (int x = 0; x < checkBoxList.Items.Count; x++)
        {
            if (checkBoxList.Items[x].Selected)
            {
                c = '1';
                gridView.Columns[x].Visible = true;
            }
            else
            {
                c = '0';
                gridView.Columns[x].Visible = false;
            }

            outputString += c;
        }

        return outputString;
    }

    /// <summary>
    /// Assign the column visibility based on profile settings
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="profileName"></param>
    private void InitializeColumnVisibility(GridView gridView, string profileName)
    {
        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            Type t = gridView.Columns[x].GetType();
            if ((t.Name != "ButtonField") && (t.Name != "TemplateField"))
            {
                try
                {
                    bool isVisible = false;

                    if ((Profile.PropertyValues[profileName] == null && System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString().Substring(x, 1) == "1") || (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(x, 1) == "1"))
                    {
                        isVisible = true;
                    }

                    gridView.Columns[x].Visible = isVisible;
                }
                catch (System.ArgumentOutOfRangeException aoor)
                {
                    string message = aoor.Message;
                    Profile.PropertyValues[profileName].PropertyValue = System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString();
                }
            }
            if ((gridView.ID == "gvCustomers") || (gridView.ID == "gvShiptos_Columns_Visible"))
            {
                if (x == 0)
                    gridView.Columns[x].Visible = true;
            }
        }
    }

    /// <summary>
    /// Retrieve customer DataSet and setup GridView. This depends on several Session variables that 
    /// dictate the filter and sort of the Datasource. Reset those variables to re-evaluate the content of
    /// the GridView
    /// </summary>
    /// <param name="empty">Create an empty grid</param>
    private void AssignCustomersDataSource(bool empty)
    {
        string searchCriteria = this.CustomersBuildFilter();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds;

        string sort = (string)dm.GetCache("gvCustomersSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "cust_name";
        else
            sort += dm.GetCache("gvCustomersSortDirection");

        if (empty)
            ds = new dsCustomerDataSet();
        else
        {
            if (dm.GetCache("dsCustomerDataSet") != null)
                searchCriteria = null;

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Customers custList = new Dmsi.Agility.Data.Customers(searchCriteria, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            ds = (dsCustomerDataSet)custList.ReferencedDataSet;

            // Copy the branch list to the worker dataset
            dsCustomerDataSet dsCU = (dsCustomerDataSet)dm.GetCache("dsCU");

            if (dsCU != null && !dsCU.Equals(ds)) // During login ds and dsCU will be the same
            {
                ds.ttbranch_cust.Clear();

                foreach (dsCustomerDataSet.ttbranch_custRow branch in dsCU.ttbranch_cust.Rows)
                {
                    ds.ttbranch_cust.ImportRow(branch);
                }
            }
        }

        DataView dv;

        if ((string)Session["ddlSearchField"] == "shipto_name")
        {
            dv = new DataView(ds.ttCustomer, null, "cust_name ASC", DataViewRowState.CurrentRows);
        }
        else
        {
            // Get the list using aascending sort
            dv = new DataView(ds.ttCustomer, "cust_obj>0", sort, DataViewRowState.CurrentRows);
        }

        DataTable dt = dv.ToTable("ttCustomerCopy");

        if (dt.Rows.Count < 200)
            lblCustomerError.Visible = false;
        else
            lblCustomerError.Visible = true;

        // "NoCustomersAvailable" is used during post back to make sure that the grid header is displayed
        if (dv.Count > 0)
        {
            dm.SetCache("dsCustomerDataSet", ds);
            dm.RemoveCache("NoCustomersAvailable");
            this.gvCustomers.DataSource = dt;
            this.gvCustomers.DataBind();

            int ipageindex = gvCustomers.PageIndex;
            int igridcount = gvCustomers.Rows.Count;
            int itotalrecords = dt.Rows.Count;
            int iNoofrows = Convert.ToInt32(ddlCustomersRows.SelectedValue);
            int iFirstrecordno = ipageindex * (iNoofrows);
            iFirstrecordno = iFirstrecordno + 1;
            int iLastRecord = (iFirstrecordno - 1) + igridcount;

            if (itotalrecords > 0)
            {
                lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
            }

            // Check if there is only one row, so the shiptos can be populated too
            if (dt.Rows.Count == 1)
            {
                dm.SetCache("CurrentCustomerRow", dt.Rows[0]);
                dm.SetCache("CustomerRowForInformationPanel", dt.Rows[0]);
                gvCustomers.SelectedIndex = 0;

                GridViewRow gvrow = gvCustomers.Rows[0];

                ImageButton imgShowHide = (ImageButton)gvrow.FindControl("imgbtnplusminus");
                gvrow.FindControl("innergrids").Visible = true;
                DropDownList ddlShiptosRows = (DropDownList)gvrow.FindControl("ddlShiptosRows");

                //previously from the gridselect index change event we used to get the data source of customers but now we are calling to get customer details based on order and filter
                DataTable dtGridcustdetails = new DataTable();
                dtGridcustdetails = GetCustomerDatasource(false);
                imgShowHide.CommandName = "Hide";
                imgShowHide.ImageUrl = "~/images/collapse-icon.png";
                gvrow.FindControl("innergrids").Visible = true;
                GridView gvshiptoinner = (GridView)gvCustomers.Rows[0].FindControl("gvShiptosinner");
                int pageSize = 0;
                ddlShiptosRows.SelectedIndex = Profile.ddlShiptosRows_SelectedIndex;

                if (ddlShiptosRows.SelectedValue == "All")
                    pageSize = 32000;
                else
                    pageSize = Convert.ToInt32(ddlShiptosRows.SelectedValue);

                gvshiptoinner.PageSize = pageSize;
                InitializeColumnVisibility(gvshiptoinner, "gvShiptos_Columns_Visible");

                ColumnSelector2.GridView = gvshiptoinner;

                AssignShiptosDataSource(false, gvshiptoinner, 0, 0);
                Session["shiptocolcount"] = 1;

                if (dtGridcustdetails != null)
                {
                    if (dtGridcustdetails.Rows.Count > 0)
                    {
                        dm.SetCache("CurrentCustomerRow", (dtGridcustdetails).Rows[0]);
                    }
                }

                gvrow.CssClass = "active-row-tr";

                SizeHiddenRow(gvrow, gvshiptoinner, ddlShiptosRows);
            }
        }
        else
        {
            lblpageno.Text = "";

            dm.SetCache("NoCustomersAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, this.gvCustomers);
        }

        dt.Dispose();
        dv.Dispose();
        ds.Dispose();
        dm = null;
    }

    private void AssignShiptosDataSource(bool empty)
    {
        //Necessary to set a flag so Inventory tab can turn off leftover messages...
        Session["Ship-toChanged"] = true;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer(true, "CurrentCustomerRow", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        DataTable table;

        string sort = (string)dm.GetCache("gvShiptosSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "seq_num";
        else
            sort += dm.GetCache("gvShiptosSortDirection");

        // no fetch yet
        if (empty || cust == null)
            table = new dsCustomerDataSet.ttShiptoDataTable();
        else
        {

            table = cust.BranchShipTos;
        }

        DataView dv;

        if (table != null)
        {
            table.CaseSensitive = false;

            if ((string)Session["ddlSearchField"] == "shipto_name" && (string)Session["txtSearchValue"] != "")
            {
                string filter = "shipto_name LIKE '" + (string)Session["txtSearchValue"] + "*' OR shipto_name LIKE '" + ((string)Session["txtSearchValue"]).ToUpper() + "*'";
                dv = new DataView(table, filter, sort, DataViewRowState.CurrentRows);
            }
            else
            {
                dv = new DataView(table, "seq_num>0", sort, DataViewRowState.CurrentRows);
            }

            DataTable dt = dv.ToTable("ttShiptoCopy"); // this will give us the correct column arrangement

            if (dt.Rows.Count > 0)
            {
                dm.RemoveCache("NoShiptosAvailable");
                this.gvShiptos.DataSource = dt;
                this.gvShiptos.DataBind();
            }
            else
            {
                dm.SetCache("NoShiptosAvailable", true);
                Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, this.gvShiptos);
            }
        }

        dm = null;
    }

    private void AssignShiptosDataSingleCust(bool empty)
    {
        //Necessary to set a flag so Inventory tab can turn off leftover messages...
        Session["Ship-toChanged"] = true;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer(true, "CurrentCustomerRow", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        DataTable table;

        string sort = (string)dm.GetCache("gvShiptosSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "seq_num";
        else
            sort += dm.GetCache("gvShiptosSortDirection");

        // no fetch yet
        if (empty || cust == null)
            table = new dsCustomerDataSet.ttShiptoDataTable();
        else
        {
            table = cust.BranchShipTos;
        }

        DataView dv;

        if (table != null)
        {
            table.CaseSensitive = false;

            if ((string)Session["ddlSearchField"] == "shipto_name" && (string)Session["txtSearchValue"] != "")
            {
                string filter = "shipto_name LIKE '" + (string)Session["txtSearchValue"] + "*' OR shipto_name LIKE '" + ((string)Session["txtSearchValue"]).ToUpper() + "*'";
                dv = new DataView(table, filter, sort, DataViewRowState.CurrentRows);
            }
            else
            {
                dv = new DataView(table, "seq_num>0", sort, DataViewRowState.CurrentRows);
            }

            DataTable dt = dv.ToTable("ttShiptoCopy"); // this will give us the correct column arrangement
            dm.RemoveCache("NoShiptosAvailable");

            if (dt.Rows.Count > 0)
            {
                dm.RemoveCache("NoShiptosAvailable");
                this.gvShiptos.DataSource = dt;
                this.gvShiptos.DataBind();

                if (dt.Rows.Count == 1)
                {
                    dm.SetCache("ShiptoRowForInformationPanel", ((DataTable)gvShiptos.DataSource).Rows[0]);
                }

                if ((dt.Rows.Count == 1) && (dm.GetCache("NoShiptosAvailable") == null))
                {

                }
            }
            else
            {
                dm.SetCache("NoShiptosAvailable", true);
                Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, this.gvShiptos);
            }
        }

        dm = null;
    }
    private void AssignShiptosDataSource(bool empty, GridView gvShiptoInner, int rowid, int rowchange)
    {
        Label lblShiptoGridView = (Label)gvCustomers.Rows[rowid].FindControl("lblShiptoGridView");
        //Necessary to set a flag so Inventory tab can turn off leftover messages...
        Session["Ship-toChanged"] = true;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer(true, "CurrentCustomerRow", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        DataTable table;

        string sort = (string)dm.GetCache("gvShiptosSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "seq_num";
        else
            sort += dm.GetCache("gvShiptosSortDirection");

        // no fetch yet
        if (empty || cust == null)
            table = new dsCustomerDataSet.ttShiptoDataTable();
        else
        {
            lblShiptoGridView.Text = "Ship-tos for " + cust.CustomerCode;
            table = cust.BranchShipTos;
        }

        DataView dv;

        if (table != null)
        {
            table.CaseSensitive = false;

            if ((string)Session["ddlSearchField"] == "shipto_name" && (string)Session["txtSearchValue"] != "")
            {
                string filter = "shipto_name LIKE '" + (string)Session["txtSearchValue"] + "*' OR shipto_name LIKE '" + ((string)Session["txtSearchValue"]).ToUpper() + "*'";
                dv = new DataView(table, filter, sort, DataViewRowState.CurrentRows);
            }
            else
            {
                dv = new DataView(table, "seq_num>0", sort, DataViewRowState.CurrentRows);
            }

            DataTable dt = dv.ToTable("ttShiptoCopy"); // this will give us the correct column arrangement

            if (dt.Rows.Count > 0)
            {
                dm.RemoveCache("NoShiptosAvailable");

                gvShiptoInner.DataSource = dt;
                gvShiptoInner.DataBind();
                Session["dt_ShiptoCopy"] = dt;

                if (dt.Rows.Count == 1)
                {
                    if (dm.GetCache("NoShiptosAvailable") == null && ((DataTable)gvShiptoInner.DataSource).Rows.Count == 1)
                    {
                        dm.SetCache("ShiptoRowForInformationPanel", ((DataTable)gvShiptoInner.DataSource).Rows[0]);

                        if (OnShiptoSelectionChanged != null)
                            OnShiptoSelectionChanged(); // will be caught by the parent
                    }
                }
                else
                {

                    //only when plus image is selected
                    if (rowchange == 1)
                    {
                        dm.RemoveCache("ShiptoRowForInformationPanel");

                        if (OnCustomerSelectionChanged != null)
                            OnCustomerSelectionChanged();
                    }
                }
            }
            else
            {
                dm.SetCache("NoShiptosAvailable", true);
                dm.RemoveCache("ShiptoRowForInformationPanel");

                if (OnCustomerSelectionChanged != null)
                    OnCustomerSelectionChanged(); // will be caught by the parent
                Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, this.gvShiptos);
            }
        }

        dm = null;
    }
    private string CustomersBuildFilter()
    {
        string filter = "";
        string searchField, searchValue;

        // Get the values save during the Find button click event
        searchField = (string)Session["ddlSearchField"];
        searchValue = (string)Session["txtSearchValue"];

        filter = "ResultCount=200\x0003CheckUserAllocation=TRUE\x0003Table=Cust";
        filter += "\x0003" + "Branch=" + (string)Session["currentBranchID"];
        filter += "\x0003" + "ActiveOnly=YES";

        // If searching for all, CustQueryField must be set to <ALL>
        if (searchValue == null ||
            searchValue.Trim() == "" ||
            searchValue.Trim().ToUpper() == "<ALL>" ||
            searchValue.Trim().ToUpper() == "ALL")
            filter += "\x0003" + "CustQueryField=<ALL>";
        else
        {
            filter += "\x0003" + "CustQueryField=" + searchField;
            filter += "\x0003" + "CustCompareMode=BEGINS";
            filter += "\x0003" + "CustCompareValue=" + searchValue;
        }

        return filter;
    }

    // This is a helper method used to determine the index of the
    // column being sorted. If no column is being sorted, -1 is returned.
    private int GetSortColumnIndex(GridView gv, string sessionSortColKey)
    {
        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + 1;
            }
        }

        return -1;
    }

    // This is a helper method used to add a sort direction
    // image to the header of the column being sorted.
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {
        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        Image sortImage = new Image();

        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        if (columnIndex != 0)
        {
            string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
            ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

            // Add the image to the appropriate header cell.
            imageLink.CommandName = "Sort";
            imageLink.CommandArgument = (string)Session[sessionSortColKey];
            imageLink.Controls.Add(sortImage);
            headerRow.Cells[columnIndex].Controls.Add(imageLink);
        }

        imageLink.Dispose();
        sortImage.Dispose();
    }

    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey)
    {
        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey);

        if (gv.ID != "gvShiptos")
        {
            sortColumnIndex = sortColumnIndex - 1;
        }

        if ((sortColumnIndex != -1) && (sortColumnIndex != -2))
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }
    }

    private static void AddHtmlAttributesToRow(GridViewRow row)
    {
        for (int i = 0; i < row.Cells.Count; i++)
        {
            if (row.Cells[i] != null && row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
            {
                row.Cells[i].Text = row.Cells[i].Text.Replace(" ", "&nbsp;");
            }

            row.Cells[0].Width = new Unit("30px");
        }
    }

    #endregion

    #region Events

    protected void gvCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        GridView gv = (GridView)sender;
        int row = gv.SelectedIndex + (gv.PageIndex * gv.PageSize);

        this.AssignCustomersDataSource(false);

        // this is used for the left pane display
        dm.SetCache("CustomerRowForInformationPanel", ((DataTable)gv.DataSource).Rows[row]);

        // this is used internally by this class for keeping track of what shiptos has to be displayed
        // just in case the search return only one row
        dm.SetCache("CurrentCustomerRow", ((DataTable)gv.DataSource).Rows[row]);

        Session["orderDateStrings"] = null;
        Session["quoteReleaseDateStrings"] = null;
        Session["LastChosenRequestedDeliveryDate"] = null;

        this.AssignShiptosDataSource(false); // display ship-tos based on currently selected cust row 

        // Default to the ship-to if the customer has only one ship-to
        if (dm.GetCache("NoShiptosAvailable") == null && ((DataTable)this.gvShiptos.DataSource).Rows.Count == 1)
        {
            dm.SetCache("ShiptoRowForInformationPanel", ((DataTable)this.gvShiptos.DataSource).Rows[0]);

            if (OnShiptoSelectionChanged != null)
                OnShiptoSelectionChanged(); // will be caught by the parent
        }
        else
        {
            dm.RemoveCache("ShiptoRowForInformationPanel");

            if (OnCustomerSelectionChanged != null)
                OnCustomerSelectionChanged(); // will be caught by the parent
        }
    }

    protected void gvCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvCustomers.PageIndex = e.NewPageIndex;
        AssignCustomersDataSource(false);
    }

    protected void btnFind_Click(object sender, EventArgs e)
    {
        Session.Remove("CurrentCustomerRow");
        gvCustomers.PageIndex = 0;

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Accounts - Search Button";
        aInput.Label = "Search by = " + this.ddlSearchField.SelectedItem;
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

        RefreshCustomerGrid(true);
    }

    private void RefreshCustomerGrid(bool clearCustSessionVariable)
    {
        // save the current search criteria until a new find is initiated
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.SetCache("ddlSearchField", this.ddlSearchField.SelectedValue);
        dm.SetCache("txtSearchValue", this.txtSearchValue.Text);
        dm.SetCache("CustomerSearchCriteria", true); // dummy entry

        // default sort to the dropdown list value - actual field name
        dm.SetCache("gvCustomersSortColumn", this.ddlSearchField.SelectedValue);
        dm.SetCache("gvCustomersSortDirection", " ASC");
        dm.SetCache("gvShiptosSortColumn", "seq_num");
        dm.SetCache("gvShiptosSortDirection", " ASC");

        // Login can also set this variable, and we don't want to clear it during the initial load
        if (clearCustSessionVariable)
            dm.RemoveCache("dsCustomerDataSet");

        this.gvCustomers.SelectedIndex = -1;

        this.AssignCustomersDataSource(false);

        if (dm.GetCache("CurrentCustomerRow") != null)
            this.AssignShiptosDataSource(false);
        else
        {
            this.AssignShiptosDataSource(true); // reset ship-to list
        }

        dm = null;
    }

    protected void ddlCustomerRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlCustomersRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        this.gvCustomers.PageSize = pageSize;
        this.AssignCustomersDataSource(false);
    }

    protected void gvCustomer_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvCustomersSortColumn"] != e.SortExpression)
        {
            Session["gvCustomersSortDirection"] = " ASC";
        }
        else
        {
            if ((string)Session["gvCustomersSortDirection"] == " ASC")
            {
                Session["gvCustomersSortDirection"] = " DESC";
            }
            else
            {
                Session["gvCustomersSortDirection"] = " ASC";
            }
        }

        gvCustomers.PageIndex = 0;
        Session["gvCustomersSortColumn"] = e.SortExpression;
        this.AssignCustomersDataSource(false);
    }

    protected void gvShiptos_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvShiptosSortColumn"] != e.SortExpression)
        {
            Session["gvShiptosSortDirection"] = " ASC";
        }
        else
        {
            if ((string)Session["gvShiptosSortDirection"] == " ASC")
            {
                Session["gvShiptosSortDirection"] = " DESC";
            }
            else
            {
                Session["gvShiptosSortDirection"] = " ASC";
            }
        }

        gvCustomers.PageIndex = 0;
        Session["gvShiptosSortColumn"] = e.SortExpression;

        GridView gvshiptoinner = (sender as GridView);
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        ImageButton imgShowHide = (ImageButton)gvshiptoinner.Parent.FindControl("imgbtnplusminus");

        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);

        DataTable dtGridcustdetails = new DataTable();
        dtGridcustdetails = GetCustomerDatasource(false);

        int irowid = Convert.ToInt32(imgShowHide.CommandArgument);

        if (dtGridcustdetails != null)
        {
            if (dtGridcustdetails.Rows.Count > 0)
            {
                dm.SetCache("CurrentCustomerRow", (dtGridcustdetails).Rows[irowid]);
            }
        }

        AssignShiptosDataSource(false, gvshiptoinner, row.RowIndex, 0);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);
        ColumnSelector1.GridView = gvCustomers;
    }

    protected void gvCustomer_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].ToolTip = "Select";
            AddHtmlAttributesToRow(e.Row);
        }
    }

    protected void gvCustomer_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvCustomers, e.Row, "gvCustomersSortColumn", "gvCustomersSortDirection");
    }

    protected void gvShiptos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
            AddHtmlAttributesToRow(e.Row);
    }

    protected void gvShiptos_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvShiptos, e.Row, "gvShiptosSortColumn", "gvShiptosSortDirection");
    }

    protected void gvShiptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            dm.SetCache("cachednonsalable", (DataRow)dm.GetCache("ShiptoRowForInformationPanel"));
        }

        GridView gv = (GridView)sender;
        int row = gv.SelectedIndex + (gv.PageIndex * gv.PageSize);

        this.AssignShiptosDataSource(false); // display ship-tos based on currently selected cust row  
        GridView gvShiptosinner = (GridView)gv.Parent.FindControl("innergrids").FindControl("gvShiptosinner");

        if (Session["dt_ShiptoCopy"] != null)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dt_ShiptoCopy"];

            if (dt.Rows.Count > 0)
            {
                dm.SetCache("ShiptoRowForInformationPanel", (dt.Rows[row]));

                // Since we are auto displaying the shiptos of single customer result. The user can now select 
                // the shipto without manually selecting a customer.
                dsCustomerDataSet ds = (dsCustomerDataSet)dm.GetCache("dsCustomerDataSet");
                DataRow[] rows = ds.ttCustomer.Select("cust_obj=" + (dt.Rows[row]["cust_obj"].ToString()));

                // The currently selected account has to be updated based on the shipto that was selected.
                if (rows.Length > 0)
                {
                    dm.SetCache("CustomerRowForInformationPanel", rows[0]);
                    dm.SetCache("dspvarDataSet", null); //changing customer requires re-fething Billing tab data
                    Session["WebHierarchicalDataGridDetail_DataSource"] = null;
                }
            }
        }

        Session["orderDateStrings"] = null;
        Session["quoteReleaseDateStrings"] = null;
        Session["LastChosenRequestedDeliveryDate"] = null;

        if (OnShiptoSelectionChanged != null)
            OnShiptoSelectionChanged(); // will be caught by the parent
    }

    protected void gvShiptos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvshiptoinner = (sender as GridView);
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        ImageButton imgShowHide = (ImageButton)gvshiptoinner.Parent.FindControl("imgbtnplusminus");

        int rowid = Convert.ToInt32(imgShowHide.CommandArgument);

        GridViewRow grinner = (imgShowHide.NamingContainer as GridViewRow);
        DataTable dtGridcustdetails = new DataTable();
        dtGridcustdetails = GetCustomerDatasource(false);

        if (dtGridcustdetails != null)
        {
            if (dtGridcustdetails.Rows.Count > 0)
            {
                dm.SetCache("CurrentCustomerRow", (dtGridcustdetails).Rows[rowid]);
            }
        }

        gvshiptoinner.PageIndex = e.NewPageIndex;
        AssignShiptosDataSource(false, gvshiptoinner, grinner.RowIndex, 0);

        DropDownList ddlShiptosRows = (DropDownList)grinner.FindControl("ddlShiptosRows");

        SizeHiddenRow(grinner, gvshiptoinner, ddlShiptosRows);
    }

    protected void ddlShiptosRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlShiptosRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize;

        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        }

        DropDownList ddlShiptosRows = (sender as DropDownList);

        LinkButton lnkCrMemoDetailCustomView = (LinkButton)ddlShiptosRows.Parent.FindControl("lnkShiptosCustomViewinner");
        int rowid = Convert.ToInt32(lnkCrMemoDetailCustomView.CommandArgument);
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        DataTable dtGridcustdetails = new DataTable();
        dtGridcustdetails = GetCustomerDatasource(false);

        GridViewRow grinner = (lnkCrMemoDetailCustomView.NamingContainer as GridViewRow);
        GridView gvshiptoinner = (GridView)grinner.FindControl("gvShiptosinner");

        if (dtGridcustdetails != null)
        {
            if (dtGridcustdetails.Rows.Count > 0)
            {
                dm.SetCache("CurrentCustomerRow", (dtGridcustdetails).Rows[rowid]);
            }
        }

        gvshiptoinner.PageSize = pageSize;
        AssignShiptosDataSource(false, gvshiptoinner, grinner.RowIndex, 0);

        SizeHiddenRow(grinner, gvshiptoinner, ddlShiptosRows);
    }

    private void ColumnSelector1_OnOKClicked(CheckBoxList checkBoxList)
    {
        string buildcolumnvisible = BuildColumnVisibilityProperty(checkBoxList, gvCustomers);
        Profile.gvCustomers_Columns_Visible = "1" + buildcolumnvisible;
        InitializeColumnVisibility(gvCustomers, "gvCustomers_Columns_Visible");
        this.AssignCustomersDataSource(false);
    }

    private void ColumnSelector2_OnOKClicked(CheckBoxList checkBoxList)
    {
        if (Session["Changecust_SelectedRow"] != null)
        {
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            int irow = Convert.ToInt32(Session["Changecust_SelectedRow"]);
            int irowindex = Convert.ToInt32(Session["Changecust_SelectedRowIndex"]);

            GridView gvshiptoinner = (GridView)gvCustomers.Rows[irowindex].FindControl("gvShiptosinner");
            string buildcolumns = BuildColumnVisibilityProperty(checkBoxList, gvshiptoinner);
            Profile.gvShiptos_Columns_Visible = "1" + buildcolumns;
            InitializeColumnVisibility(gvshiptoinner, "gvShiptos_Columns_Visible");
            DataTable dtGridcustdetails = new DataTable();
            dtGridcustdetails = GetCustomerDatasource(false);

            if (dtGridcustdetails != null)
            {
                if (dtGridcustdetails.Rows.Count > 0)
                {
                    dm.SetCache("CurrentCustomerRow", (dtGridcustdetails).Rows[irow]);
                }

            }

            AssignShiptosDataSource(false, gvshiptoinner, irowindex, 0);
        }
    }

    private void ColumnSelector2_OnResetClicked()
    {
        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvShiptos_Columns_Visible"].DefaultValue.ToString();
        Profile.gvShiptos_Columns_Visible = defaultValue;

        if (Session["Changecust_SelectedRow"] != null)
        {
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            int irow = Convert.ToInt32(Session["Changecust_SelectedRow"]);
            int irowindex = Convert.ToInt32(Session["Changecust_SelectedRowIndex"]);
            DataTable dtGridcustdetails = new DataTable();
            dtGridcustdetails = GetCustomerDatasource(false);
            GridView gvshiptoinner = (GridView)gvCustomers.Rows[irowindex].FindControl("gvShiptosinner");
            InitializeColumnVisibility(gvshiptoinner, "gvShiptos_Columns_Visible");
            ColumnSelector2.GridView = gvshiptoinner;

            if (dtGridcustdetails != null)
            {
                if (dtGridcustdetails.Rows.Count > 0)
                {
                    dm.SetCache("CurrentCustomerRow", (dtGridcustdetails).Rows[irow]);
                }
            }

            AssignShiptosDataSource(false, gvshiptoinner, irowindex, 0);
        }
    }

    private void ColumnSelector1_OnResetClicked()
    {
        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvCustomers_Columns_Visible"].DefaultValue.ToString();
        Profile.gvCustomers_Columns_Visible = "1" + defaultValue;
        InitializeColumnVisibility(gvCustomers, "gvCustomers_Columns_Visible");

        this.AssignCustomersDataSource(false);
    }

    #endregion

    protected void Show_Hide_ChildGrid(object sender, EventArgs e)
    {
        this.gvCustomers.SelectedIndex = -1;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        ImageButton imgShowHide = (sender as ImageButton);
        imgShowHide.ToolTip = "Select";
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        string commandname = imgShowHide.CommandName;

        for (int i = 0; i <= gvCustomers.Rows.Count - 1; i++)
        {
            GridViewRow gvr = (GridViewRow)gvCustomers.Rows[i];
            ImageButton imgbutton = (ImageButton)gvr.FindControl("imgbtnplusminus");

            if (i != row.RowIndex)
            {
                int inextrow = row.RowIndex;
                inextrow = inextrow + 1;

                if (i == inextrow)
                {
                    gvr.CssClass = row.CssClass = "activealt-row-tr";
                    this.gvCustomers.SelectedIndex = -1;
                }
                else
                {
                    gvr.CssClass = "";
                }

                gvr.FindControl("innergrids").Visible = false;
                gvr.FindControl("innerplaceholder").Visible = false;

                imgbutton.CommandName = "Details";
                imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                imgbutton.ToolTip = "Select";
            }
            else
            {
                if (imgbutton.ImageUrl == "~/images/collapse-icon.png")
                {
                    gvr.FindControl("innergrids").Visible = false;
                    gvr.FindControl("innerplaceholder").Visible = false;

                    imgbutton.CommandName = "Hide";
                    imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                    imgbutton.ToolTip = "Select";
                }
            }
        }

        if (imgShowHide.CommandName == "Details")
        {
            imgShowHide.ToolTip = "Select";
            int rows = Convert.ToInt32(imgShowHide.CommandArgument);

            row.FindControl("innergrids").Visible = true;
            DropDownList ddlShiptosRows = (DropDownList)row.FindControl("ddlShiptosRows");

            //previously from the gridselect index change event we used to get the data source of customers but now we are calling to get customer details based on order and filter
            DataTable dtGridcustdetails = new DataTable();
            dtGridcustdetails = GetCustomerDatasource(false);
            imgShowHide.CommandName = "Hide";
            imgShowHide.ImageUrl = "~/images/collapse-icon.png";
            row.FindControl("innergrids").Visible = true;
            GridView gvshiptoinner = (GridView)row.FindControl("gvShiptosinner");

            if (dtGridcustdetails != null)
            {
                if (dtGridcustdetails.Rows.Count > 0)
                {
                    dm.SetCache("CurrentCustomerRow", (dtGridcustdetails).Rows[rows]);
                    dm.SetCache("CustomerRowForInformationPanel", (dtGridcustdetails).Rows[rows]);
                }
            }

            int pageSize = 0;
            ddlShiptosRows.SelectedIndex = Profile.ddlShiptosRows_SelectedIndex;

            if (ddlShiptosRows.SelectedValue == "All")
                pageSize = 32000;
            else
                pageSize = Convert.ToInt32(ddlShiptosRows.SelectedValue);

            gvshiptoinner.PageSize = pageSize;

            InitializeColumnVisibility(gvshiptoinner, "gvShiptos_Columns_Visible");
            ColumnSelector2.GridView = gvshiptoinner;
            Session["RecallFetchAttributes"] = 1;
            AssignShiptosDataSource(false, gvshiptoinner, row.RowIndex, 1);
            row.CssClass = "active-row-tr";

            this.gvCustomers.SelectedIndex = rows % this.gvCustomers.PageSize;

            SizeHiddenRow(row, gvshiptoinner, ddlShiptosRows);
        }
        else
        {
            row.FindControl("innergrids").Visible = false;
            row.FindControl("innerplaceholder").Visible = false;

            imgShowHide.CommandName = "Details";
            imgShowHide.ImageUrl = "~/Images/Grid_Plus.gif";
        }
    }

    private static void SizeHiddenRow(GridViewRow row, GridView gvshiptoinner, DropDownList ddlShiptosRows)
    {
        int detailrowcount = gvshiptoinner.Rows.Count;

        if (ddlShiptosRows.SelectedValue != "All")
        {
            if (detailrowcount > Convert.ToInt32(ddlShiptosRows.SelectedValue))
            {
                detailrowcount = Convert.ToInt32(ddlShiptosRows.SelectedValue);
            }
        }

        int pagerAddHeight = 0;

        if (gvshiptoinner.PageCount > 1)
            pagerAddHeight = 56;

        //Refs #121577 32767 is the upper limit for the Unit object type constructor below
        //so choose that as the height if more than 775 ship tos
        int panelheight = Math.Min((178 + (detailrowcount * 42) + pagerAddHeight), 32767);  

        row.FindControl("innergrids").Visible = true;
        Panel p = row.FindControl("innerplaceholder") as Panel;
        p.Visible = true;
        p.Height = new Unit(panelheight, UnitType.Pixel);
    }

    protected void lnkcustshipto_Click(object sender, EventArgs e)
    {
        LinkButton lnkcustshipto = (sender as LinkButton);
        GridView gvk = (lnkcustshipto.NamingContainer as GridView);
        GridViewRow row = (lnkcustshipto.NamingContainer as GridViewRow);
        int irowindex = row.RowIndex;
        Session["Changecust_SelectedRow"] = lnkcustshipto.CommandArgument;
        Session["Changecust_SelectedRowIndex"] = row.RowIndex;

        if (ColumnSelector2.GridView == null)
        {
            if (Session["shiptocolcount"] != null)
            {
                if ((int)Session["shiptocolcount"] == 1)
                {
                    GridView gvshiptoinner = (GridView)row.FindControl("gvShiptosinner");
                    if (Profile != null)
                    {
                        if (Profile.gvShiptos_Columns_Visible != null)
                        {
                            InitializeColumns(gvshiptoinner, Profile.gvShiptos_Columns_Visible);
                        }
                        else
                        {
                            InitializeColumnVisibility(gvshiptoinner, Profile.gvShiptos_Columns_Visible.ToString());
                        }
                    }
                    else
                    {
                        InitializeColumnVisibility(gvshiptoinner, "gvShiptos_Columns_Visible");
                    }

                    ColumnSelector2.GridView = gvshiptoinner;
                    Session["shiptocolcount"] = null;
                }
            }
        }

        ModalPopupExtender2.Show();
    }
    private void InitializeColumns(GridView gridView, string profileName)
    {
        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            Type t = gridView.Columns[x].GetType();
            if ((t.Name != "ButtonField") && (t.Name != "TemplateField"))
            {
                try
                {
                    bool isVisible = false;

                    if (profileName.Substring(x, 1) == "1")
                    {
                        isVisible = true;
                    }
                    gridView.Columns[x].Visible = isVisible;
                }
                catch (System.ArgumentOutOfRangeException aoor)
                {
                    string message = aoor.Message;
                    Profile.PropertyValues[profileName].PropertyValue = System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString();
                }
            }

            if ((gridView.ID == "gvCustomers") || (gridView.ID == "gvShiptos_Columns_Visible"))
            {
                if (x == 0)
                    gridView.Columns[x].Visible = true;
            }
        }
    }
    private DataTable GetCustomerDatasource(bool empty)
    {
        string searchCriteria = this.CustomersBuildFilter();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet ds;

        string sort = (string)dm.GetCache("gvCustomersSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "cust_name";
        else
            sort += dm.GetCache("gvCustomersSortDirection");

        if (empty)
            ds = new dsCustomerDataSet();
        else
        {
            if (dm.GetCache("dsCustomerDataSet") != null)
                searchCriteria = null;

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Customers custList = new Dmsi.Agility.Data.Customers(searchCriteria, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            ds = (dsCustomerDataSet)custList.ReferencedDataSet;

            // Copy the branch list to the worker dataset
            dsCustomerDataSet dsCU = (dsCustomerDataSet)dm.GetCache("dsCU");

            if (dsCU != null && !dsCU.Equals(ds)) // During login ds and dsCU will be the same
            {
                ds.ttbranch_cust.Clear();
                foreach (dsCustomerDataSet.ttbranch_custRow branch in dsCU.ttbranch_cust.Rows)
                {
                    ds.ttbranch_cust.ImportRow(branch);
                }
            }
        }

        DataView dv;

        if ((string)Session["ddlSearchField"] == "shipto_name")
        {
            dv = new DataView(ds.ttCustomer, null, "cust_name ASC", DataViewRowState.CurrentRows);
        }
        else
        {
            // Get the list using aascending sort
            dv = new DataView(ds.ttCustomer, "cust_obj>0", sort, DataViewRowState.CurrentRows);
        }

        DataTable dt = dv.ToTable("ttCustomerCopy");

        // "NoCustomersAvailable" is used during post back to make sure that the grid header is displayed

        return dt;
    }
}
