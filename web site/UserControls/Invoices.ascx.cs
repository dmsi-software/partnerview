using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Dmsi.Agility.Data;

public partial class UserControls_Invoices : System.Web.UI.UserControl
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);
        if ((string)Session["ddlInvoiceShowResultsBy"] == "Item")
            csCustomHeader.GridView = this.gvByItem;
        else
            csCustomHeader.GridView = this.gvInvoiceHeaders;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.csCustomHeader.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomHeader_OnOKClicked);
        this.csCustomHeader.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomHeader_OnResetClicked);
        this.csCustomDetail.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomDetail_OnOKClicked);
        this.csCustomDetail.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomDetail_OnResetClicked);

        if (!this.IsPostBack)
        {
            ddlSearchStyle.Enabled = false;

            //load data fields with today and 30 days prior...
            this.txtToDate.Text = DateTime.Now.ToDMSiDateFormat();
            this.txtFromDate.Text = (DateTime.Today.AddMonths(-1)).ToDMSiDateFormat();

            this.ddlInvoiceHeaderRows.SelectedIndex = Profile.ddlInvoiceHeaderRows_SelectedIndex;

            int pageSize = Convert.ToInt32(ddlInvoiceHeaderRows.SelectedValue);

            this.gvInvoiceHeaders.PageSize = pageSize;
            this.gvByItem.PageSize = pageSize;

            InitializeColumnVisibility(gvInvoiceHeaders, "gvInvoiceHeaders_Columns_Visible");
            InitializeColumnVisibility(gvByItem, "gvInvoiceItems_Columns_Visible");

            ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
            if (pc.INddlTranID_SelectedIndex > ddlTranID.Items.Count)
                ddlTranID.SelectedIndex = 0;
            else
                ddlTranID.SelectedIndex = pc.INddlTranID_SelectedIndex;

            SetDdlSearchTypeValues(ddlTranID.Text);

            if (pc.INddlSearchStyle_SelectedIndex > ddlSearchStyle.Items.Count)
                ddlSearchStyle.SelectedIndex = 0;
            else
                ddlSearchStyle.SelectedIndex = pc.INddlSearchStyle_SelectedIndex;
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // this is needed for the columns to show correctly
        if (!this.IsPostBack || dm.GetCache("NoInvoicesAvailable") != null)
        {
            if (!this.IsPostBack)
            {
                dm.SetCache("gvInvoiceHeadersSortColumn", "ref_num");
                dm.SetCache("gvInvoiceHeadersSortDirection", " DESC");
                dm.SetCache("gvInvoiceDetailSortColumn", "ITEM");
                dm.SetCache("gvInvoiceDetailSortDirection", " ASC");
                dm.SetCache("gvInvoiceByItemSortColumn", "ITEM");
                dm.SetCache("gvInvoiceByItemSortDirection", " ASC");
            }

            if ((string)dm.GetCache("ddlInvoiceShowResultsBy") == "Item")
                this.AssignHeaderDataSource(true, "ttinvoice_byitem");
            else
                this.AssignHeaderDataSource(true, "ttinvoice_header");
        }
        else if (dm.GetCache("DocViewerInvID") != null)
        {
            // ViewState setting can normally handle the display, but
            // dynamic contents (URL link) are not saved.
            if (Session["Invoicedetails_InnerInvoiceId"] != null)
            {
                int Rowid = Convert.ToInt32(Session["Invoicedetails_InnerInvoiceId"]);
                BindDocuments(Rowid);
            }
        }

        dm = null;
    }

    #region Private Methods

    /// <summary>
    /// Routine to build the string corresponding to the visible columns of the grid
    /// </summary>
    /// <param name="checkBoxList"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    private string BuildColumnVisibilityProperty(CheckBoxList checkBoxList, GridView gridView)
    {
        string outputString = "";
        char c;

        for (int x = 0; x < checkBoxList.Items.Count; x++)
        {
            if (checkBoxList.Items[x].Selected)
            {
                c = '1';
                gridView.Columns[x].Visible = true;
            }
            else
            {
                c = '0';
                gridView.Columns[x].Visible = false;
            }
            outputString += c;
        }

        return outputString;
    }

    /// <summary>
    /// Assign the column visibility based on profile settings
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="profileName"></param>
    private void InitializeColumnVisibility(GridView gridView, string profileName)
    {
        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            Type t = gridView.Columns[x].GetType();
            if ((t.Name != "ButtonField") && (t.Name != "TemplateField"))
            {
                try
                {
                    bool isVisible = false;

                    if ((Profile.PropertyValues[profileName] == null &&
                        System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString().Substring(x, 1) == "1")
                    || (Profile.PropertyValues[profileName] != null &&
                        Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(x, 1) == "1"))
                    {
                        isVisible = true;
                    }
                    gridView.Columns[x].Visible = isVisible;
                }
                catch (System.ArgumentOutOfRangeException aoor)
                {
                    string message = aoor.Message;
                    Profile.PropertyValues[profileName].PropertyValue = System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString();
                }
            }

            if ((gridView.ID == "gvInvoiceHeaders"))
            {
                if (x == 0)
                    gridView.Columns[x].Visible = true;
            }
        }
    }

    // This is a helper method used to determine the index of the
    // column being sorted. If no column is being sorted, -1 is returned.
    private int GetSortColumnIndex(GridView gv, string sessionSortColKey, int offSet)
    {
        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + offSet;
            }
        }

        return -1;
    }

    // This is a helper method used to add a sort direction
    // image to the header of the column being sorted.
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {
        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        Image sortImage = new Image();

        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
        ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

        // Add the image to the appropriate header cell.
        imageLink.CommandName = "Sort";
        imageLink.CommandArgument = (string)Session[sessionSortColKey];
        imageLink.Controls.Add(sortImage);
        headerRow.Cells[columnIndex].Controls.Add(imageLink);

        imageLink.Dispose();
        sortImage.Dispose();
    }

    /// <summary>
    /// Insert the sort image to the column header
    /// </summary>
    /// <param name="gv"></param>
    /// <param name="row"></param>
    /// <param name="sessionSortColKey"></param>
    /// <param name="sessionDirectionKey"></param>
    /// <param name="offSet">Refers to the number non-boundable columns before boundable columns</param>
    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey, int offSet)
    {
        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey, offSet);

        if (sortColumnIndex != -1)
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }
    }

    /// <summary>
    /// This must be called after the related session variable sare set or clear accordingly.
    /// </summary>
    /// <param name="empty"></param>
    /// <param name="tableName"></param>
    private void AssignHeaderDataSource(bool empty, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string sort = "";

        if (tableName == "ttinvoice_byitem")
        {
            sort = (string)dm.GetCache("gvInvoiceByItemSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "ITEM";
            else
                sort += dm.GetCache("gvInvoiceByItemSortDirection");

            this.gvByItem.Visible = true;
            this.gvInvoiceHeaders.Visible = false;

            this.lnkInvoiceDetailCustomView.Visible = false;
            this.pnlInvoiceDetailCustomView.Visible = false;

            FetchHeaderDataSource(empty, "ITEM LIKE '%'", sort, this.gvByItem, tableName);
        }
        else
        {
            sort = (string)dm.GetCache("gvInvoiceHeadersSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "ref_num";
            else
                sort += dm.GetCache("gvInvoiceHeadersSortDirection");

            this.gvInvoiceHeaders.Visible = true;
            this.gvByItem.Visible = false;

            this.lnkInvoiceDetailCustomView.Visible = true;
            this.pnlInvoiceDetailCustomView.Visible = true;

            FetchHeaderDataSource(empty, "ref_num LIKE '%'", sort, this.gvInvoiceHeaders, tableName);
        }
    }

    private void FetchHeaderDataSource(bool empty, string filter, string sort, GridView gv, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInvoiceDataSet invoiceList;

        // First column is the Select link, do this to force new arrangement of fields
        string searchCriteria = (string)dm.GetCache("InvoiceSearchCriteria");
        dm.RemoveCache("ttinvoice_header");

        if (!this.IsPostBack || searchCriteria == null || empty)
            invoiceList = new dsInvoiceDataSet();
        else
        {
            if (dm.GetCache("dsInvoiceDataSet") != null)
                invoiceList = (dsInvoiceDataSet)dm.GetCache("dsInvoiceDataSet");
            else
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

                siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

                foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
                {
                    siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
                }

                currentCust.Dispose();

                Dmsi.Agility.Data.Invoices invoices = new Dmsi.Agility.Data.Invoices(searchCriteria, siCallDS, out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                siCallDS.Dispose();
                invoiceList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsInvoiceDataSet)invoices.ReferencedDataSet;
                dm.SetCache("dsInvoiceDataSet", invoiceList);
                invoices.Dispose();
            }
        }

        // there is no dataset available, just initialize its schema
        if (invoiceList == null)
            invoiceList = new dsInvoiceDataSet();

        DataView dv = null;
        if (tableName == "ttinvoice_byitem")
            dv = new DataView(invoiceList.ttinvoice_byitem, filter, sort, DataViewRowState.CurrentRows);
        else
            dv = new DataView(invoiceList.ttinvoice_header, filter, sort, DataViewRowState.CurrentRows);

        DataTable dt = dv.ToTable(tableName + "Copy"); // this will give us the correct column arrangement

        if (dt.Rows.Count < 100)
            lblInvoiceHeaderError.Visible = false;
        else
        {
            lblInvoiceHeaderError.Visible = true;
            lblInvoiceHeaderError.Text = "First 100 invoices returned. Add criteria to refine search.";
        }

        if (dt.Rows.Count > 0)
        {
            dm.RemoveCache("NoInvoicesAvailable"); // this way the postback knows the grid is not empty
            gv.DataSource = dt;
            gv.DataBind();
            int ipageindex = gv.PageIndex;
            int igridcount = gv.Rows.Count;
            int itotalrecords = dt.Rows.Count;
            int iNoofrows = Convert.ToInt32(ddlInvoiceHeaderRows.SelectedValue);
            int iFirstrecordno = ipageindex * (iNoofrows);
            iFirstrecordno = iFirstrecordno + 1;
            int iLastRecord = (iFirstrecordno - 1) + igridcount;

            if (itotalrecords > 0)
            {
                lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
            }

            if (dt.Rows.Count == 1 && tableName == "ttinvoice_header")
            {
                dm.SetCache("ttinvoice_header", dt.Rows[0]);
                gvInvoiceHeaders.SelectedIndex = 0;
                GridViewRow gr = gvInvoiceHeaders.Rows[0];
                BindSelectedDetailGrid(gr.RowIndex);
                gr.CssClass = "active-row-tr";
            }
        }
        else
        {
            lblpageno.Text = "";
            // this is a work around so the grid will show when the datasource is empty
            dm.SetCache("NoInvoicesAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);
        }
    }

    private DataTable GetDetailSortDetails(string Invoiceid, bool empty)
    {
        string filter = "ref_num='" + Invoiceid.ToString() + "'";

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInvoiceDataSet ds;

        string sort = (string)dm.GetCache("gvInvoiceDetailSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "ITEM";
        else
            sort += dm.GetCache("gvInvoiceDetailSortDirection");

        if (!this.IsPostBack || empty)
            ds = new dsInvoiceDataSet();
        else
            ds = (dsInvoiceDataSet)dm.GetCache("dsInvoiceDataSet");

        // no fetch yet
        if (ds == null)
            ds = new dsInvoiceDataSet();

        DataView dv = new DataView(ds.ttinvoice_detail, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttinvoice_detailCopy"); // this will give us the correct column arrangement

        return dt;
    }

    private void AssignDetailDataSource(bool empty, GridViewRow Gvr, string InvoiceId)
    {
        string myDataItem = "";
        myDataItem = InvoiceId;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInvoiceDataSet ds;

        GridView gvInvoiceDetailInner = (GridView)Gvr.FindControl("gvInvoiceDetailInner");
        Label lblInvoiceDetailError = (Label)Gvr.FindControl("lblInvoiceDetailError");

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Invoices invoiceList = new Dmsi.Agility.Data.Invoices(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
        DataRow[] invoiceRows = invoiceList.ReferenceHeaderTable.Select("ref_num='" + myDataItem + "'");
        myDataManager.SetCache("ttinvoice_header", invoiceRows[0]);
        string filter = this.DetailBuildFilter();
        UpdateDetailInformation(Gvr);

        string sort = (string)dm.GetCache("gvInvoiceDetailSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "ITEM";
        else
            sort += dm.GetCache("gvInvoiceDetailSortDirection");

        if (!this.IsPostBack || empty)
            ds = new dsInvoiceDataSet();
        else
            ds = (dsInvoiceDataSet)dm.GetCache("dsInvoiceDataSet");

        // no fetch yet
        if (ds == null)
            ds = new dsInvoiceDataSet();

        DataView dv = new DataView(ds.ttinvoice_detail, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttinvoice_detailCopy"); // this will give us the correct column arrangement

        if (dt.Rows.Count < 200)
        {
            lblInvoiceDetailError.Visible = false;
        }
        else
        {
            lblInvoiceDetailError.Visible = true;
        }

        csCustomDetail.GridView = gvInvoiceDetailInner;

        if (dt.Rows.Count > 0)
        {
            gvInvoiceDetailInner.DataSource = dt;
            gvInvoiceDetailInner.DataBind();
        }
        else
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gvInvoiceDetailInner);
    }

    private string DetailBuildFilter()
    {
        string filter = "";
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ttinvoice_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ttinvoice_header");
            filter = "ref_num='" + row["ref_num"].ToString() + "'";
        }
        else
        {
            filter = "ref_num='ThisShouldNotReturnAnything'";
        }

        return filter;
    }

    private void UpdateDetailInformation()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ttinvoice_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ttinvoice_header");

            dm.SetCache("DocViewerInvID", (int)row["so_id"]);
            dm.SetCache("DocViewerInvShipmentNum", (int)row["shipment_num"]);
        }
        else
        {
            dm.RemoveCache("DocViewerInvID");
            dm.RemoveCache("DocViewerinvShipmentNum");
        }
    }

    private void UpdateDetailInformation(GridViewRow gvr)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gvr.FindControl("docViewer");
        Label lblInvoiceDetail = (Label)gvr.FindControl("lblInvoiceDetail");

        if (dm.GetCache("ttinvoice_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ttinvoice_header");
            lblInvoiceDetail.Text = "Invoice Details for ID " + row["so_id"].ToString();

            docViewer.DocumentTitle = "Documents for ID " + row["so_id"].ToString();
            docViewer.Type = "Invoice";
            docViewer.TransactionID = (int)row["so_id"];
            docViewer.ShipmentNumber = (int)row["shipment_num"];
            docViewer.BranchID = (string)row["branch_id"];

            dm.SetCache("DocViewerInvID", (int)row["so_id"]);
            dm.SetCache("DocViewerInvShipmentNum", (int)row["shipment_num"]);
        }
        else
        {
            lblInvoiceDetail.Text = "Invoice Details";

            docViewer.DocumentTitle = "Documents";
            docViewer.Type = "";
            docViewer.TransactionID = 0;
            docViewer.ShipmentNumber = 0;
            dm.RemoveCache("DocViewerInvID");
            dm.RemoveCache("DocViewerinvShipmentNum");
        }

        docViewer.Refresh();

        GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

        if (gvdoc.Rows.Count <= 0)
        {
            docViewer.Visible = false;
        }
    }

    private string BuildDocumentSearchCriteria(string invoiceReferenceNumber)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string branch = "";
        string referenceNumber = "";
        string sessionID = "";

        branch = "Branch=" + (string)dm.GetCache("currentBranchID");
        referenceNumber = "TranID=" + invoiceReferenceNumber;
        sessionID = "SessionID=" + Session.SessionID.ToString().Trim();

        searchCriteria = "DocType=Invoice";
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + referenceNumber;
        searchCriteria += "\x0003" + sessionID;

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        searchCriteria += "\x0003UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003Type=Invoice,Invoice Invoice";

        return searchCriteria;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Call this to clear bound data
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    /// <param name="customer">Current Customer Object</param>
    public void Reset()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("dsInvoiceDataSet");
        dm.RemoveCache("ttinvoice_header");
        dm.RemoveCache("InvoiceSearchCriteria");
        this.AssignHeaderDataSource(true, "ttinvoice_header");

        dm = null;
    }

    /// <summary>
    /// Call this to clear bound data when the branch or customer has changed
    /// </summary>
    /// <param name="branchID">Current Branch ID</param>
    /// <param name="shiptoObj">Current Shipto Object</param>
    public void Reset(string branchID, string shiptoObj)
    {
        this.Reset();
    }

    #endregion

    protected void btnFind_Click(object sender, EventArgs e)
    {
        ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
        pc.INddlTranID_SelectedIndex = ddlTranID.SelectedIndex;
        pc.INddlSearchStyle_SelectedIndex = ddlSearchStyle.SelectedIndex;
        pc.Save();

        ExecuteSearch();

        if (sender != null && e != null)
        {

            string analyticsLabel = "Search by = " + ddlTranID.SelectedItem + ", " +
                                    "Search Type = " + ddlSearchStyle.Text + ", " +
                                    "Paid Status = " + ddlStatus.SelectedItem + ", " +
                                    "Show Results = " + this.ddlInvoiceShowResultsBy.SelectedItem + ", " +
                                    "Include all Ship-tos = " + this.cbShipTo.Checked;

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Invoices - Search Button";
            aInput.Label = analyticsLabel;
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }
    }

    protected void gvInvoiceHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }

                e.Row.Cells[0].ToolTip = "View";
            }
        }
    }

    protected void gvInvoiceHeader_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvInvoiceHeaders.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "ttinvoice_header");
        this.gvInvoiceHeaders.SelectedIndex = -1;

        //Empty detail data...
        Session["Invoicedetails_InnerInvoiceId"] = null;
    }

    protected void gvByItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvByItem.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "ttinvoice_byitem");
        this.gvByItem.SelectedIndex = -1;
    }

    protected void gvInvoiceHeader_SelectedIndexChanged(object sender, EventArgs e)
    {
        string myDataItem = "";
        GridView gv = (GridView)sender;

        myDataItem = gv.SelectedDataKey.Value.ToString();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Invoices invoiceList = new Dmsi.Agility.Data.Invoices(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
        DataRow[] invoiceRows = invoiceList.ReferenceHeaderTable.Select("ref_num='" + myDataItem + "'");
        myDataManager.SetCache("ttinvoice_header", invoiceRows[0]);

        invoiceList.Dispose();
    }

    protected void ddlInvoiceHeaderRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlInvoiceHeaderRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);

        this.gvByItem.PageSize = pageSize;
        this.gvInvoiceHeaders.PageSize = pageSize;

        if ((string)Session["ddlInvoiceShowResultsBy"] == "Item")
        {
            this.AssignHeaderDataSource(false, "ttinvoice_byitem");
        }
        else
        {
            this.AssignHeaderDataSource(false, "ttinvoice_header");
        }

        Session["Invoicedetails_InnerInvoiceId"] = null;
    }

    protected void ddlInvoiceDetailRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlInvoiceDetailRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize;
        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        }

        DropDownList ddlsender = ((DropDownList)sender);

        LinkButton lnkInvoiceDetailCustomView = (LinkButton)ddlsender.Parent.FindControl("lnkInvoiceDetailCustomView");
        GridViewRow row = (lnkInvoiceDetailCustomView.NamingContainer as GridViewRow);
        int rowid = row.RowIndex;

        row.FindControl("innergrids").Visible = true;
        DropDownList ddlInvoiceDetailRows = (DropDownList)row.FindControl("ddlInvoiceDetailRows");
        string InvoiceId = gvInvoiceHeaders.DataKeys[row.RowIndex].Value.ToString();
        Label lblInvoiceDetail = (Label)row.FindControl("lblInvoiceDetail");
        lblInvoiceDetail.Text = "Invoice Details for Id " + InvoiceId;

        GridViewRow gr = gvInvoiceHeaders.Rows[row.RowIndex];
        GridView gvInvoiceDetailInner = (GridView)row.FindControl("gvInvoiceDetailInner");

        ddlInvoiceDetailRows.SelectedIndex = Profile.ddlInvoiceDetailRows_SelectedIndex;

        if (ddlInvoiceDetailRows.SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(ddlInvoiceDetailRows.SelectedValue);
        }

        gvInvoiceDetailInner.PageSize = pageSize;
        AssignDetailDataSource(false, gr, InvoiceId);

        SizeHiddenRow(gr, ddlInvoiceDetailRows, gvInvoiceDetailInner);
    }

    private void csCustomHeader_OnOKClicked(CheckBoxList checkBoxList)
    {
        string resultsView = (string)Session["ddlInvoiceShowResultsBy"] == null ? "Invoice" : (string)Session["ddlInvoiceShowResultsBy"];

        if (resultsView == "Invoice")
        {
            string builcolumnvisibility = BuildColumnVisibilityProperty(checkBoxList, gvInvoiceHeaders);
            Profile.gvInvoiceHeaders_Columns_Visible = "1" + builcolumnvisibility;
            InitializeColumnVisibility(gvInvoiceHeaders, "gvInvoiceHeaders_Columns_Visible");
            this.AssignHeaderDataSource(false, "ttinvoice_header");
        }
        else
        {
            Profile.gvInvoiceItems_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvByItem);
            this.AssignHeaderDataSource(false, "ttinvoice_byitem");
        }
    }

    private void csCustomDetail_OnOKClicked(CheckBoxList checkBoxList)
    {
        if (Session["Invoicedetails_InnerInvoiceId"] != null)
        {
            int irowid = Convert.ToInt32(Session["Invoicedetails_InnerInvoiceId"]);

            GridViewRow gvr = (GridViewRow)gvInvoiceHeaders.Rows[irowid];

            if (Session["Invoicedetails_InnerInvoiceId"] != null)
            {
                string invoiceid = gvInvoiceHeaders.DataKeys[irowid].Value.ToString();
                GridView gvInvoiceDetail = (GridView)gvr.FindControl("gvInvoiceDetailInner");
                string buildcolumns = BuildColumnVisibilityProperty(checkBoxList, gvInvoiceDetail);
                Profile.gvInvoiceDetail_Columns_Visible = buildcolumns;
                AssignDetailDataSource(false, gvr, invoiceid);
            }
        }
    }

    private void csCustomDetail_OnResetClicked()
    {
        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvInvoiceDetail_Columns_Visible"].DefaultValue.ToString();
        Profile.gvInvoiceDetail_Columns_Visible = defaultValue;

        if (Session["Invoicedetails_InnerInvoiceId"] != null)
        {
            int irowid = Convert.ToInt32(Session["Invoicedetails_InnerInvoiceId"]);

            GridViewRow gvr = (GridViewRow)gvInvoiceHeaders.Rows[irowid];

            if (Session["Invoicedetails_InnerInvoiceId"] != null)
            {
                string invoiceid = gvInvoiceHeaders.DataKeys[irowid].Value.ToString();
                GridView gvInvoiceDetail = (GridView)gvr.FindControl("gvInvoiceDetailInner");
                InitializeColumnVisibility(gvInvoiceDetail, "gvInvoiceDetail_Columns_Visible");
                AssignDetailDataSource(false, gvr, invoiceid);
            }
        }
    }

    private void csCustomHeader_OnResetClicked()
    {
        string defaultValue;
        string resultsView = (string)Session["ddlInvoiceShowResultsBy"] == null ? "Invoice" : (string)Session["ddlInvoiceShowResultsBy"];

        switch (resultsView)
        {
            case "Item":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvInvoiceItems_Columns_Visible"].DefaultValue.ToString();
                    Profile.gvInvoiceItems_Columns_Visible = defaultValue;
                    // Profile.gvInvoiceItems_Columns_Visible = "1" + defaultValue;
                    InitializeColumnVisibility(gvByItem, "gvInvoiceItems_Columns_Visible");
                    Profile.gvInvoiceItems_Columns_Visible = defaultValue;
                    break;
                }

            case "Invoice":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvInvoiceHeaders_Columns_Visible"].DefaultValue.ToString();
                    Profile.gvInvoiceHeaders_Columns_Visible = defaultValue;
                    InitializeColumnVisibility(gvInvoiceHeaders, "gvInvoiceHeaders_Columns_Visible");

                    break;
                }
        }

        btnFind_Click(null, null);
    }

    protected void ExecuteSearch()
    {
        // save the current search criteria until a new find is initiated
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // This is needed when building the header caption inside HeaderBuildSearchCriteria
        if (this.ddlInvoiceShowResultsBy.SelectedValue == "Item")
            dm.SetCache("ddlInvoiceShowResultsBy", "Item");
        else
            dm.SetCache("ddlInvoiceShowResultsBy", "Invoice");

        string searchCriteria = this.HeaderBuildSearchCriteria();

        if (searchCriteria == "Invalid input date.")
        {
            lblInvoiceHeaderError.Visible = true;
            lblInvoiceHeaderError.Text = searchCriteria;
            lblInvoiceHeader.Text = "Invoices";
            return;
        }

        if (searchCriteria == "Invalid date range.")
        {
            lblInvoiceHeaderError.Visible = true;
            lblInvoiceHeaderError.Text = searchCriteria;
            lblInvoiceHeader.Text = "Invoices";
            return;
        }

        if (searchCriteria == "")
        {
            lblInvoiceHeaderError.Visible = true;
            lblInvoiceHeaderError.Text = "Limit date range to 2 years or less.";
            lblInvoiceHeader.Text = "Invoices";
            return;
        }

        dm.SetCache("InvoiceSearchCriteria", searchCriteria);
        dm.RemoveCache("dsInvoiceDataSet");
        dm.RemoveCache("DocViewerInvID");
        dm.RemoveCache("DocViewerShipmentNum");

        this.gvInvoiceHeaders.SelectedIndex = -1;

        if (this.ddlInvoiceShowResultsBy.SelectedValue == "Item")
            this.AssignHeaderDataSource(false, "ttinvoice_byitem");
        else
            this.AssignHeaderDataSource(false, "ttinvoice_header");
    }

    private string HeaderBuildSearchCriteria()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string shipto = "";
        string shipToSequence = "";
        string searchType = "";
        string searchStyle = "";
        string searchTran = "";
        string customer = "";
        string branch = "";
        string status = "";
        string showResults = "";
        string startDateStr = "";
        string endDateStr = "";
        string gridTitle = "";
        DataRow shipToRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
        shipToSequence = shipToRow["seq_num"].ToString() + "";
        branch = "Branch=" + (string)dm.GetCache("currentBranchID");

        dsPartnerVuDataSet ds = (dsPartnerVuDataSet)dm.GetCache("dsPV");
        DataRow[] rows = ds.bPV_Action.Select("token_code= 'view_invoices_current_branch_only'");
        if (rows.Length == 0)
        {
            branch = "Branch=<ALL>";
        }

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer currentCustomer = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        customer = "Customer=" + currentCustomer.CustomerKey;
        currentCustomer.Dispose();

        searchType = "SearchType=" + ddlTranID.SelectedValue;

        if (searchType.Contains("PO ID"))
            searchType = "SearchType=Customer PO #";

        searchStyle = "SearchStyle=" + ddlSearchStyle.Text;

        if (txtTranID.Text.Trim().Length == 0)
        {
            searchTran = "SearchTran=<ALL>";
        }
        else
        {
            searchTran = "SearchTran=" + txtTranID.Text.Trim();
        }

        if (ddlStatus.SelectedValue == "All")
        {
            status = "Status=<ALL>";
        }
        else
        {
            status = "Status=" + ddlStatus.SelectedValue;
            gridTitle += status + " ";
        }

        if (this.cbShipTo.Checked)
        {
            shipto = "Ship To=<ALL>";
        }
        else
        {
            shipto = "Ship To=" + shipToSequence;
            gridTitle += shipto + " ";
        }

        showResults = "Show Results=" + this.ddlInvoiceShowResultsBy.SelectedValue;

        bool hasStartDate = false, hasEndDate = false;
        DateTime startDate = DateTime.Today.AddDays(-30), endDate = DateTime.Today;
        DateTimeValidator startDateValidator = new DateTimeValidator(this.txtFromDate.Text);
        DateTimeValidator endDateValidator = new DateTimeValidator(this.txtToDate.Text);

        hasStartDate = startDateValidator.InfoEntered();
        hasEndDate = endDateValidator.InfoEntered();

        if (hasStartDate && startDateValidator.ValidDate())
        {
            startDate = startDateValidator.ParsedDateTime();

            if (this.txtFromDate.Text != startDateValidator.FormattedDateTime())
            {
                this.txtFromDate.Text = startDateValidator.FormattedDateTime();
            }
        }
        else if (hasStartDate)
        {
            return startDateValidator.ValidationMessage();
        }

        if (hasEndDate && endDateValidator.ValidDate())
        {
            endDate = endDateValidator.ParsedDateTime();

            if (this.txtToDate.Text != endDateValidator.FormattedDateTime())
            {
                this.txtToDate.Text = endDateValidator.FormattedDateTime();
            }
        }
        else if (hasEndDate)
        {
            return endDateValidator.ValidationMessage();
        }

        startDateStr = "Start Date=" + (hasStartDate ? startDate.ToShortDateString() : "<ALL>");
        endDateStr = "End Date=" + (hasEndDate ? endDate.ToShortDateString() : "<ALL>");

        if (hasStartDate && hasEndDate)
        {
            if (startDate.AddYears(2) < endDate)
            {
                return "";
            }
            else if (startDate > endDate)
            {
                return "Invalid date range.";
            }
            else
                gridTitle = "Invoices from " + startDate.ToDMSiDateFormat() + " to " + endDate.ToDMSiDateFormat();
        }
        else if (hasStartDate)
        {
            if (startDate.AddYears(2) < DateTime.Now)
            {
                return "";
            }
            else
                gridTitle = "Invoices from " + startDate.ToDMSiDateFormat();
        }
        else if (hasEndDate)
        {
            gridTitle = "Invoices up to " + endDate.ToDMSiDateFormat();
            return "";
        }
        else
        {
            gridTitle = "Invoices";
            return "";
        }

        lblInvoiceHeader.Text = gridTitle;
        lblInvoiceHeader.Text.Trim();

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        searchCriteria = "UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + customer;

        searchCriteria += "\x0003" + shipto;
        searchCriteria += "\x0003" + status;
        searchCriteria += "\x0003" + searchType;
        searchCriteria += "\x0003" + searchStyle;
        searchCriteria += "\x0003" + searchTran;
        searchCriteria += "\x0003" + startDateStr;
        searchCriteria += "\x0003" + endDateStr;
        searchCriteria += "\x0003" + showResults;
        searchCriteria += "\x0003Type=Invoices";
        searchCriteria += "\x0003ExtendPrice=YES";

        if ((string)Session["ddlInvoiceShowResultsBy"] == "Invoice")
            searchCriteria += "\x0003OrderTotals=YES";

        return searchCriteria;
    }

    protected void gvInvoiceHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvInvoiceHeaders, e.Row, "gvInvoiceHeadersSortColumn", "gvInvoiceHeadersSortDirection", 0);
    }

    protected void gvInvoiceDetailInner_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(sender as GridView, e.Row, "gvInvoiceDetailSortColumn", "gvInvoiceDetailSortDirection", 0);
    }

    protected void gvInvoiceHeader_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvInvoiceHeadersSortColumn"] != e.SortExpression)
            Session["gvInvoiceHeadersSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvInvoiceHeadersSortDirection"] == " ASC")
                Session["gvInvoiceHeadersSortDirection"] = " DESC";
            else
                Session["gvInvoiceHeadersSortDirection"] = " ASC";
        }

        gvInvoiceHeaders.PageIndex = 0;
        Session["gvInvoiceHeadersSortColumn"] = e.SortExpression;

        if ((string)Session["ddlInvoiceShowResultsBy"] == "Item")
            this.AssignHeaderDataSource(false, "ttinvoice_byitem");
        else
            this.AssignHeaderDataSource(false, "ttinvoice_header");
    }

    protected void gvInvoiceDetail_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvInvoiceDetailSortColumn"] != e.SortExpression)
            Session["gvInvoiceDetailSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvInvoiceDetailSortDirection"] == " ASC")
                Session["gvInvoiceDetailSortDirection"] = " DESC";
            else
                Session["gvInvoiceDetailSortDirection"] = " ASC";
        }

        GridView gvInvoiceDetailInner = (sender as GridView);
        gvInvoiceDetailInner.PageIndex = 0;
        Session["gvInvoiceDetailSortColumn"] = e.SortExpression;

        LinkButton lnkInvoiceDetailCustomView = (LinkButton)gvInvoiceDetailInner.Parent.FindControl("lnkInvoiceDetailCustomView");
        string Datakey_item = "";
        Datakey_item = gvInvoiceDetailInner.DataKeys[0]["ref_num"].ToString();
        DataTable dtQuoteDetail = new DataTable();

        dtQuoteDetail = GetDetailSortDetails(Datakey_item, false);

        if (lnkInvoiceDetailCustomView != null)
        {
            GridViewRow gvrow = (lnkInvoiceDetailCustomView.NamingContainer as GridViewRow);
            UpdateDetailInformation(gvrow);
        }

        if (dtQuoteDetail != null)
        {
            gvInvoiceDetailInner.DataSource = dtQuoteDetail;
            gvInvoiceDetailInner.DataBind();
        }

        Session["Invoicedetails_InnerInvoiceId"] = null;
    }

    protected void gvByItem_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvInvoiceByItemSortColumn"] != e.SortExpression)
            Session["gvInvoiceByItemSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvInvoiceByItemSortDirection"] == " ASC")
                Session["gvInvoiceByItemSortDirection"] = " DESC";
            else
                Session["gvInvoiceByItemSortDirection"] = " ASC";
        }

        this.gvByItem.PageIndex = 0;
        Session["gvInvoiceByItemSortColumn"] = e.SortExpression;
        this.AssignHeaderDataSource(false, "ttinvoice_byitem");
    }

    protected void gvByItem_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvByItem, e.Row, "gvInvoiceByItemSortColumn", "gvInvoiceByItemSortDirection", 0);
    }

    protected void gvInvoiceDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvInvoiceDetailInner = (sender as GridView);

        LinkButton lnkInvoiceDetailCustomView = (LinkButton)gvInvoiceDetailInner.Parent.FindControl("lnkInvoiceDetailCustomView");
        GridViewRow gvrow = (lnkInvoiceDetailCustomView.NamingContainer as GridViewRow);

        gvInvoiceDetailInner.PageIndex = e.NewPageIndex; //apps

        string Datakey_item = "";
        Datakey_item = gvInvoiceDetailInner.DataKeys[0]["ref_num"].ToString();
        DataTable dtQuoteDetail = new DataTable();

        dtQuoteDetail = GetDetailSortDetails(Datakey_item, false);

        if (lnkInvoiceDetailCustomView != null)
        {
            UpdateDetailInformation(gvrow);
        }

        if (dtQuoteDetail != null)
        {
            gvInvoiceDetailInner.PageIndex = e.NewPageIndex;

            gvInvoiceDetailInner.DataSource = dtQuoteDetail;
            gvInvoiceDetailInner.DataBind();
        }

        Session["Invoicedetails_InnerInvoiceId"] = null;

        DropDownList ddlInvoiceDetailRows = (DropDownList)gvrow.FindControl("ddlInvoiceDetailRows");

        SizeHiddenRow(gvrow, ddlInvoiceDetailRows, gvInvoiceDetailInner);
    }

    protected void lnkQuoteDetailCustomView_Click(object sender, EventArgs e)
    {
        LinkButton imgShowHide = (sender as LinkButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        Session["Invoices_InnerGrid"] = row;
        string Invoiceid = gvInvoiceHeaders.DataKeys[row.RowIndex].Value.ToString();
        Session["Invoices_InnerInvoiceid"] = Invoiceid;

        ModalPopupExtender2.Show();
    }

    protected void Show_Hide_ChildGrid(object sender, EventArgs e)
    {
        this.gvInvoiceHeaders.SelectedIndex = -1;

        ImageButton imgShowHide = (sender as ImageButton);
        imgShowHide.ToolTip = "View";
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        int irowid = Convert.ToInt32(imgShowHide.CommandArgument);
        Session["Invoicedetails_InnerInvoiceId"] = row.RowIndex;

        //closes previously  opened inner grid
        for (int i = 0; i <= gvInvoiceHeaders.Rows.Count - 1; i++)
        {
            GridViewRow gvr = (GridViewRow)gvInvoiceHeaders.Rows[i];

            ImageButton imgbutton = (ImageButton)gvr.FindControl("imgbtnplusminus");

            if (i != row.RowIndex)
            {
                int inextrow = row.RowIndex;
                inextrow = inextrow + 1;

                if (i == inextrow)
                {
                    gvr.CssClass = row.CssClass = "activealt-row-tr";
                    this.gvInvoiceHeaders.SelectedIndex = -1;
                }
                else
                {
                    gvr.CssClass = "";
                }

                gvr.FindControl("innergrids").Visible = false;
                gvr.FindControl("innerplaceholder").Visible = false;

                imgbutton.CommandName = "Details";
                imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
            }
            else
            {
                if (imgbutton.ImageUrl == "~/images/collapse-icon.png")
                {
                    gvr.FindControl("innergrids").Visible = false;
                    gvr.FindControl("innerplaceholder").Visible = false;

                    imgbutton.CommandName = "Hide";
                    imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                }
            }
        }

        if (imgShowHide.CommandName == "Details")
        {
            BindSelectedDetailGrid(row.RowIndex);
            this.gvInvoiceHeaders.SelectedIndex = irowid % this.gvInvoiceHeaders.PageSize;
        }
        else
        {
            row.FindControl("innergrids").Visible = false;
            row.FindControl("innerplaceholder").Visible = false;

            imgShowHide.CommandName = "Details";
            imgShowHide.ImageUrl = "~/Images/Grid_Plus.gif";
        }
    }

    protected void lnkInvoiceDetailCustomView_Click(object sender, EventArgs e)
    {
        LinkButton imgShowHide = (sender as LinkButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);

        string invoiceid = gvInvoiceHeaders.DataKeys[row.RowIndex].Value.ToString();
        Session["Invoicedetails_InnerInvoiceId"] = row.RowIndex;

        ModalPopupExtender2.Show();
    }

    //this binds documents when page refresh happens
    protected void BindDocuments(int RowId)
    {
        GridViewRow gr = gvInvoiceHeaders.Rows[RowId];

        string Invoiceid = gvInvoiceHeaders.DataKeys[RowId].Value.ToString();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Invoices invoiceList = new Dmsi.Agility.Data.Invoices(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
        DataRow[] invoiceRows = invoiceList.ReferenceHeaderTable.Select("ref_num='" + Invoiceid + "'");
        myDataManager.SetCache("ttinvoice_header", invoiceRows[0]);

        UpdateDetailInformation(gr);
    }

    protected void BindSelectedDetailGrid(int irowindex)
    {
        GridViewRow gr = gvInvoiceHeaders.Rows[irowindex];
        gr.CssClass = "active-row-tr";
        gr.FindControl("innergrids").Visible = true;
        DropDownList ddlInvoiceDetailRows = (DropDownList)gr.FindControl("ddlInvoiceDetailRows");
        string Invoiceid = gvInvoiceHeaders.DataKeys[irowindex].Value.ToString();
        Label lblInvoiceDetail = (Label)gr.FindControl("lblInvoiceDetail");
        lblInvoiceDetail.Text = "Invoice Details for ID  " + Invoiceid;
        GridView gvInvoiceDetailInner = (GridView)gr.FindControl("gvInvoiceDetailInner");
        ddlInvoiceDetailRows.SelectedIndex = Profile.ddlInvoiceDetailRows_SelectedIndex;
        int pageSize;

        if (ddlInvoiceDetailRows.SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(ddlInvoiceDetailRows.SelectedValue);
        }

        gvInvoiceDetailInner.PageSize = pageSize;
        InitializeColumnVisibility(gvInvoiceDetailInner, "gvInvoiceDetail_Columns_Visible");
        this.AssignDetailDataSource(false, gr, Invoiceid);
        ImageButton imgShowHide = (ImageButton)gr.FindControl("imgbtnplusminus");
        imgShowHide.CommandName = "Hide";
        imgShowHide.ImageUrl = "~/images/collapse-icon.png";

        SizeHiddenRow(gr, ddlInvoiceDetailRows, gvInvoiceDetailInner);
    }

    private static void SizeHiddenRow(GridViewRow gr, DropDownList ddlInvoiceDetailRows, GridView gvInvoiceDetailInner)
    {
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("docViewer");

        int gvdocHeight = 0;

        GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

        if (gvdoc != null && gvdoc.Rows.Count > 0)
        {
            gvdocHeight = 42 + (gvdoc.Rows.Count * 42);
        }
        else
            gvdocHeight = -42;

        int detailrowcount = gvInvoiceDetailInner.Rows.Count;

        if (ddlInvoiceDetailRows.SelectedValue != "All")
        {
            if (detailrowcount > Convert.ToInt32(ddlInvoiceDetailRows.SelectedValue))
            {
                detailrowcount = Convert.ToInt32(ddlInvoiceDetailRows.SelectedValue);
            }
        }

        int pagerAddHeight = 0;

        if (gvInvoiceDetailInner.PageCount > 1)
            pagerAddHeight = 56;

        int panelheight = 232 + gvdocHeight + (detailrowcount * 42) + pagerAddHeight;

        gr.FindControl("innergrids").Visible = true;
        Panel p = gr.FindControl("innerplaceholder") as Panel;
        p.Visible = true;
        p.Height = new Unit(panelheight, UnitType.Pixel);
    }

    protected void ddlTranID_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDdlSearchTypeValues(ddlTranID.Text);
    }

    private void SetDdlSearchTypeValues(string ddlTranIDText)
    {
        switch (ddlTranIDText)
        {
            case "Invoice ID":
            case "Original Sales Order ID":
            case "Original Credit Memo ID":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.Enabled = false;
                break;

            case "Reference #":
            case "Item #":
            case "PO ID":
            case "Job #":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Beginning with"));
                ddlSearchStyle.Items.Add(new ListItem("Containing"));
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.Enabled = true;
                ddlSearchStyle.SelectedIndex = 2;
                break;
        }
    }
}
