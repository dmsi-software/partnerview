﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Configurator.ascx.cs"
    Inherits="UserControls_Configurator" %>
<table cellpadding="0" cellspacing="5" style="width: 100%; height: 450px">
    <tr>
        <td>
            <%--Hello World...--%>
            <div id="silverlightControlHost">
                <object data="data:application/x-silverlight-2," type="application/x-silverlight-2"
                    width="100%" height="100%">
                    <param name="source" value="../ClientBin/Configurator.xap" />
                    <param name="onError" value="onSilverlightError" />
                    <param name="background" value="white" />
                    <param name="minRuntimeVersion" value="3.0.40624.0" />
                    <param name="autoUpgrade" value="true" />
                    <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=3.0.40624.0" style="text-decoration: none">
                        <img src="http://go.microsoft.com/fwlink/?LinkId=108181" alt="Get Microsoft Silverlight"
                            style="border-style: none" />
                    </a>
                </object>
                <iframe id="_sl_historyFrame" style="visibility: hidden; height: 0px; width: 0px;
                    border: 0px"></iframe>
            </div>
        </td>
    </tr>
</table>
