﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PdfViewer.ascx.cs" Inherits="UserControls_PdfViewer" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr><td>&nbsp;</td></tr>
            <tr>
                
                <td>
                    <p style="text-align: center; vertical-align: middle">
                        <asp:Label ID="lblMessage" runat="server" Text="Generating document. Please wait . . ."
                            Font-Size="Larger"></asp:Label>
                        <br />
                        <img runat="server" id="imgProgress" alt="Loading document" src="~/Images/loadingbar.gif"
                            style="width: 220px; height: 19px" /></p>
                    <p style="text-align: center; vertical-align: middle">
                        <asp:Label ID="lblContext" runat="server" Visible="False"></asp:Label>
                    </p>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Timer ID="Timer1" runat="server" Interval="5000" OnTick="Timer1_Tick" 
    Enabled="True">
</asp:Timer>
