<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddInfoQuoteSelector.ascx.cs"
    Inherits="UserControls_AddInfoQuoteSelector" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Src="OtherChargesAndTaxes.ascx" TagName="OtherChargesAndTaxes" TagPrefix="uc" %>
<style type="text/css">
    .style4 {
        height: 23px;
        width: 100px;
    }

    .style5 {
        height: 23px;
        width: 154px;
    }

    .MarginLeft {
        margin-left: 45px;
    }
</style>
<table cellpadding="0" cellspacing="2">
    <tr>
        <td colspan="2" valign="top" style="width: 100%; text-align: left">
            <asp:Panel ID="pnlScroller" runat="server" ScrollBars="None">
                <ul class="retail poupEdit">
                    <li>
                        <asp:Label ID="lblCustomerId" runat="server" CssClass="editRetailCustLabel" Text="Customer ID"></asp:Label>
                        <asp:TextBox ID="txtCustomerId" runat="server" Width="24%" MaxLength="12" CssClass="marginRight2px"></asp:TextBox>

                        <asp:Label ID="Label4" runat="server" CssClass="PopupInfolabel" Text="Ship-to"></asp:Label>
                        <asp:TextBox ID="txtShipTo" runat="server" Width="27%" MaxLength="4"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label2" runat="server" CssClass="editRetailCustLabel" Text="Customer name"></asp:Label>
                        <asp:TextBox ID="txtCustomerName" runat="server" MaxLength="30" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label3" runat="server" CssClass="editRetailCustLabel" Text="Address 1"></asp:Label>
                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="30" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label5" runat="server" CssClass="editRetailCustLabel" Text="Address 2"></asp:Label>
                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="30" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label6" runat="server" CssClass="editRetailCustLabel" Text="Address 3"></asp:Label>
                        <asp:TextBox ID="txtAddress3" runat="server" MaxLength="30" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label7" runat="server" CssClass="editRetailCustLabel" Text="City"></asp:Label>
                        <asp:TextBox ID="txtCity" runat="server" MaxLength="20" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label12" runat="server" CssClass="editRetailCustLabel" Text="State"></asp:Label>
                        <asp:TextBox ID="txtState" runat="server" MaxLength="2" Width="15%" CssClass="marginRight2px"></asp:TextBox>

                        <asp:Label ID="Label13" runat="server" CssClass="PopupInfolabel" Text="ZIP"></asp:Label>
                        <asp:TextBox ID="txtZip" runat="server" MaxLength="10" Width="40%"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label8" runat="server" CssClass="editRetailCustLabel" Text="Country" Height="15px"></asp:Label>
                        <asp:TextBox ID="txtCountry" runat="server" MaxLength="8" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label9" runat="server" CssClass="editRetailCustLabel" Text="Phone"></asp:Label>
                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="30" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label11" runat="server" CssClass="editRetailCustLabel" Text="Contact name"></asp:Label>
                        <asp:TextBox ID="txtContactName" runat="server" MaxLength="40" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label10" runat="server" CssClass="editRetailCustLabel" Text="E-mail"></asp:Label>
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="100" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label1" runat="server" CssClass="editRetailCustLabel" Text="Reference"></asp:Label>
                        <asp:TextBox ID="txtReference" runat="server" MaxLength="40" CssClass="width62"></asp:TextBox>
                    </li>
                    <uc:OtherChargesAndTaxes runat="server" ID="otherChargesAndTaxesAddlInfo" />
                </ul>
            </asp:Panel>
        </td>
    </tr>
</table>
<div class="modal-footer" style="width: 100%; padding-right: 0px !important;">
    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="btn btnpopup-primary btnpopupleft" OnClick="btnOK_Click" />
    <asp:Button ID="btnReset" runat="server" Text="Delete" OnClick="btnReset_Click" CssClass="btn btnpopup-primary btnpopupleft" Visible="false" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btnpopup-default btnpopupleft" OnClick="btnCancel_Click" />
</div>
