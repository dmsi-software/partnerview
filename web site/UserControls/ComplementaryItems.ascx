﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ComplementaryItems.ascx.cs"
    Inherits="UserControls_ComplementaryItems" %>
<style type="text/css">
    .MarginRightTwo {
        margin-right: 2px;
    }

    .MarginRightTen {
        margin-right: 10px;
    }

    .MarginTopFour {
        padding-top: 4px;
    }

    .searchddlmargin {
        padding-top: 5px;
        padding-bottom: 5px;
    }
</style>

<table class="LightColor" cellpadding="0" cellspacing="0" style="width: 100%" border="0"
    runat="server" id="tblInventory">
    <tr runat="server" id="trHeader1" style="background: White">
        <td class="SimplePanelHeaderNoBG" align="left" style="background: White; width: 100%; padding-left: 8px; padding-top: 7px; padding-bottom: 2px">
            <asp:Label runat="server" ID="lblQuoteHeader" Text="Add Items to Quote XXXXXX" />
        </td>
    </tr>
    <tr runat="server" id="trHeader2" style="margin-left: 8px; margin-right: 8px">
        <td style="font-size: 1px; width: 6px; background: White">&nbsp;
        </td>
    </tr>
</table>
<div class="ComplementaryModalBackground" runat="server" id="dvcomplementary">
    <div class="modal fade in" style="display: block;">
        <div class="modal-dialog lgwidth">
            <div class="modal-content">
                <div class="modal-header">
                    <asp:Label runat="server" ID="lblHeader" Text="You May Wish to Consider" CssClass="youmaywish" />
                </div>
                <div class="modal-body">
                    <div class="sorting-section">
                        <ul class="sort">
                            <li>
                                <asp:Button runat="server" ID="btnAddToCart" Text="Add to Cart" CssClass="MarginRightTwo btn btn-primary"
                                    TabIndex="1" OnClick="btnAddToCart_Click" />
                                <asp:Button runat="server" ID="btnContinueShopping" Text="Continue Shopping" OnClick="btnContinueShopping_Click" CssClass="btn btn-default"
                                    TabIndex="2" />
                            </li>
                        </ul>
                    </div>
                    <div runat="server" id="trCompItemsValidationErrors" visible="false">
                        <asp:Label runat="server" ID="lblCompItemsValidationErrors" ForeColor="Red" />
                    </div>
                    <div runat="server" scrollbars="Vertical" style="display: block; float: left; width: 100%; overflow-y: auto" class="complementrypopup">
                        <asp:ListView ID="lvProducts" runat="server" ItemPlaceholderID="itemPlaceHolder"
                            OnItemDataBound="lvProducts_ItemDataBound" OnItemCommand="lvProducts_ItemCommand"
                            DataKeyNames="ITEM,thickness,WIDTH,LENGTH,Size,DESCRIPTION,price,price_uom,retail_price,stocking_uom,min_pak,qty_uom_conv_ptr,qty_uom_conv_ptr_sysid,TYPE">
                            <LayoutTemplate>
                                <table id="tblProducts" cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                    <thead>
                                        <tr style="visibility: collapse; display: none;" class="prodcut-row">
                                            <th>
                                                <asp:Label ID="lblProductImage" runat="server" Text="Product Image" Font-Bold="true"
                                                    ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <th style="width: 25px;">&nbsp;
                                            </th>
                                            <th style="width: 450px;">
                                                <asp:Label ID="lblProductDetails" runat="server" Text="Product Details" Font-Bold="true"
                                                    ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <th style="width: 25px;">&nbsp;
                                            </th>
                                            <th style="width: 250px;">
                                                <asp:Label ID="lblProductPriceDetails" runat="server" Text="Product Price Details"
                                                    Font-Bold="true" ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <div id="itemPlaceHolder" runat="server">
                                            </div>
                                        </tr>
                                    </thead>
                                    <tbody id="groupplaceholder" runat="server">
                                    </tbody>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr id="Tr1" style="height: 70px; width: 100%" runat="server">
                                    <td class="DarkBottomBorder">
                                        <div class="img-thumb">
                                            <asp:Image Visible="true" ID="imgProduct" runat="server" ImageAlign="Middle" Height="64px"
                                                CssClass="MarginRightTen" />
                                            <asp:HiddenField ID="hfID" runat="server" />
                                        </div>
                                    </td>
                                    <td class="DarkBottomBorder" style="height: 64px; vertical-align: bottom; padding-bottom: 4px">
                                        <div class="pr-info">
                                            <asp:Label ID="lbtProdDesc" runat="server" Text="Product Description" Font-Size="Large" CssClass="prodtitle" ForeColor="#000000"></asp:Label>
                                            <asp:Label ID="lblproddesc" runat="server" Font-Size="Large" Visible="false" ForeColor="#000000" />
                                            <br />
                                            <div style="width: 100%; float: left; padding-top: 4px;">
                                                <asp:Label ID="lblAvailable" runat="server" Text="Available&nbsp;" Font-Bold="true"></asp:Label>
                                                <asp:Label ID="lblAvilableCount" runat="server" Text="" Font-Bold="true" CssClass="lblGrdpartno"></asp:Label>
                                                <asp:Label ID="lblAvilableCountText" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                <asp:Label ID="lblAvailUOM" runat="server" Text="" Font-Bold="true" />
                                            </div>
                                            <br />
                                            <asp:Label ID="lblFrequentlyHeader" runat="server" Text="Frequently Bought With "></asp:Label>
                                            <asp:Label ID="lblFrequently" runat="server" Text="" CssClass="lblGrdpartno"></asp:Label>
                                        </div>
                                        <div class="ord-deatils">
                                            <asp:Label ID="lblPrice" runat="server" Text="Price &nbsp;"></asp:Label>
                                            <asp:Label ID="lblPriceText" runat="server" Text="" DataFormatString="{0:#,##0.00}"
                                                CssClass="lblGrdpartno"></asp:Label>
                                            <br />
                                            <asp:Label ID="lblOrder" runat="server" Text="Qty" Font-Bold="true"></asp:Label>
                                            <asp:TextBox ID="txtOrder" runat="server" CssClass="qty-txt qtymargin" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
                                            <asp:DropDownList OnSelectedIndexChanged="ddlUOM_SelectedIndexChanged" DataMember='<%# Eval("ITEM") %>' Visible="true" ID="ddlUOM" runat="server" Width="80px" Height="30px" CssClass="searchddl qtymargin searchddlmargin" AutoPostBack="True" />
                                            <br />
                                            <asp:Label ID="lblMinPack" runat="server" Text="Must be ordered in multiples of 1" CssClass="MarginTopFour"></asp:Label>
                                            <asp:HiddenField ID="hdnMinPack" runat="server" Visible="false" />
                                            <asp:HiddenField ID="hdnItem" runat="server" Visible="false" />
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
