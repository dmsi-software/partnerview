﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_OtherCharges : System.Web.UI.UserControl
{
    private short _tabIndex = 0;
    public string DescriptionLabelText
    {
        set { lblOtherChargesDescription.Text = value; }
    }
    public string Description
    {
        get { return txtOtherChargesDescription.Text; }
        set { txtOtherChargesDescription.Text = value; }
    }

    public decimal Amount
    {
        get { return Convert.ToDecimal(numericOtherChargesAmount.Text); }
        set { numericOtherChargesAmount.Text = value.ToTrimmedString(); }
    }

    public short TabIndex
    {
        get { return _tabIndex; }
        set
        {
            _tabIndex = value;
            txtOtherChargesDescription.TabIndex = (short)(value++);
            numericOtherChargesAmount.TabIndex = (short)(value);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public bool Valid() {
        throw new NotImplementedException("You haven't coded the OtherCharges.Valid method yet!");
    }
}