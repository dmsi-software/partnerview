﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_OtherChargesAndTaxes : System.Web.UI.UserControl
{
    private short _tabIndex;
    public decimal TaxRate
    {
        get { return Convert.ToDecimal(numericTaxRate.Text); }
        set { numericTaxRate.Text = value.ToTrimmedString(); }
    }
    public string TaxableDescription
    {
        get { return otherChargesTaxable.Description; }
        set { otherChargesTaxable.Description = value; }
    }
    public decimal TaxableAmount
    {
        get { return otherChargesTaxable.Amount; }
        set { otherChargesTaxable.Amount = value; }
    }
    public string NonTaxableDescription
    {
        get { return otherChargesNonTaxable.Description; }
        set { otherChargesNonTaxable.Description = value; }
    }
    public decimal NonTaxableAmount
    {
        get { return otherChargesNonTaxable.Amount; }
        set { otherChargesNonTaxable.Amount = value; }
    }
    public short TabIndex
    {
        set
        {
            _tabIndex = value;
            numericTaxRate.TabIndex = (short)(value++);
            otherChargesTaxable.TabIndex = (short)(value++);
            otherChargesNonTaxable.TabIndex = (short)(++value);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public bool ValidTaxRateAndChargeValues()
    {
        return ValidTaxRate() & otherChargesTaxable.Valid() & otherChargesNonTaxable.Valid();
    }
    private bool ValidTaxRate()
    {
        throw new NotImplementedException("You haven't coded the OtherChargesAndTaxes.ValidTaxRate method yet!");
        //return true;
    }

    public bool SaveTaxRateAndChargeValues()
    {
        try
        {
            throw new NotImplementedException("You haven't coded the OtherChargesAndTaxes.SaveTaxRateAndChargeValues method yet!");
            //= otherChargesTaxable.Description;
            //return true;
        }
        catch (Exception e)
        {
            return false;
            throw e;
        }

    }
}