﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OtherChargesAndTaxes.ascx.cs" Inherits="UserControls_OtherChargesAndTaxes" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Src="OtherCharges.ascx" TagName="OtherCharges" TagPrefix="uc" %>
<li>
    <asp:Label 
        ID="lblTaxRate" 
        runat="server" 
        Text="Tax rate %" 
        CssClass="editRetailCustLabel">
    </asp:Label>
    <ig:WebNumericEditor 
        ID="numericTaxRate"
        runat="server" 
        DataMode="Decimal"
        MaxDecimalPlaces="4"
        MinValue="0.0000"
        MaxValue="100.0000" 
        Nullable="False" 
        CssClass="textarea width62" 
        HideEnterKey="True" 
        NullText="A value between 0.0000 and 100.0000" 
        MaxLength="8">
    </ig:WebNumericEditor>
</li>
<li>
    <uc:OtherCharges 
        ID="otherChargesTaxable" 
        runat="server" 
        DescriptionLabelText="Taxable charges description"/>

</li>
<li>
    <uc:OtherCharges 
        ID="otherChargesNonTaxable" 
        runat="server" 
        DescriptionLabelText="Non-taxable charges description"/>
</li>
