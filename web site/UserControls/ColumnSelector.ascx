<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColumnSelector.ascx.cs"
    Inherits="UserControls_ColumnSelector" %>
<style type="text/css">
    .AlignLeft {
        text-align: left;
    }

    .specialPadding {
        padding-bottom: inherit;
        padding-top: inherit;
        padding-left: 10px !important;
        padding-right: 10px !important;
    }
</style>
<div>
    <table cellpadding="0" cellspacing="2">
        <tr>
            <td align="left" colspan="2" valign="top" style="width: 100%">
                <asp:Panel ID="pnlScroller" runat="server" CssClass="advsearch" Height="400px"
                    ScrollBars="Vertical" HorizontalAlign="Left">
                    <asp:CheckBoxList ID="cblColumns" runat="server" Width="308px" CellPadding="0" CellSpacing="0" CssClass="AlignLeft">
                    </asp:CheckBoxList>
                </asp:Panel>
            </td>
        </tr>

    </table>
</div>
<div class="modal-footer specialPadding">
    <asp:Button ID="btnOK" runat="server" Text="OK" OnClick="btnOK_Click" CssClass="btn btnpopup-default" />
    <asp:Button ID="btnReset" runat="server" Text="Reset to Default" OnClick="btnReset_Click"
        CssClass="btn btnpopup-primary" />

    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btnpopup-primary" />


</div>
