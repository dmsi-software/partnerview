<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CreditMemos.ascx.cs" Inherits="UserControls_CreditMemos" %>
<%@ Register Src="ClientSideCalendar.ascx" TagName="ClientSideCalendar" TagPrefix="uc2" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc3" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="DocViewer.ascx" TagName="DocViewer" TagPrefix="uc4" %>
<style type="text/css">
    .PanelPadding {
        padding-left: 4px;
        padding-right: 4px;
    }

    .child-div {
        position:absolute;
        left:2%;
        right:0px;
    }
</style>
<div class="left-section">
    <div class="lft-cont-sec">
        <div id="my-tab-content">
            <div class="search-opt">
                <div class="advsearch">
                    <asp:Panel ID="CreditMemosearchFieldPanel" runat="server" DefaultButton="btnFind">
                        <asp:Label ID="Label3" runat="server" Text="Search by" CssClass="alladvsearchlabel"></asp:Label>
                        <asp:DropDownList ID="ddlTranID" runat="server" CssClass="select" OnSelectedIndexChanged="ddlTranID_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem>Credit Memo ID</asp:ListItem>
                            <asp:ListItem>Reference #</asp:ListItem>
                            <asp:ListItem>Item #</asp:ListItem>
                            <asp:ListItem>PO ID</asp:ListItem>
                            <asp:ListItem>Job #</asp:ListItem>
                            <asp:ListItem>Original Invoice #</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSearchStyle" runat="server" CssClass="select">
                            <asp:ListItem>Equals</asp:ListItem>
                            <asp:ListItem>Starting at</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtTranID" runat="server" CssClass="select"></asp:TextBox><br />
                    </asp:Panel>
                    <asp:Panel ID="AdvancedSearchDiv" runat="server" DefaultButton="btnFind">
                        <asp:Label ID="Label2" runat="server" Text="Status" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select">
                            <asp:ListItem Value="All">All</asp:ListItem>
                            <asp:ListItem>Open</asp:ListItem>
                            <asp:ListItem>Invoiced</asp:ListItem>
                            <asp:ListItem>RMA</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:Label ID="Label5" runat="server" Text="From order date" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="select"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="cefromdate" runat="server" TargetControlID="txtFromDate"
                            Format="MM/dd/yyyy" PopupButtonID="txtFromDate" Animated="true" CssClass="cal_Theme1" />
                        <br />
                        <asp:Label ID="Label6" runat="server" CssClass="alladvsearchlabel" Text="To order date"></asp:Label><br />
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="select"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="cetodate" runat="server" TargetControlID="txtToDate"
                            Format="MM/dd/yyyy" PopupButtonID="txtToDate" Animated="true" CssClass="cal_Theme1" />
                        <br />
                        <asp:Label ID="Label4" runat="server" Text="Show results by" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:DropDownList ID="ddlCreditMemoShowResultsBy" runat="server" CssClass="select">
                            <asp:ListItem>Credit Memo</asp:ListItem>
                            <asp:ListItem>Item</asp:ListItem>
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:CheckBox ID="cbShipTo" runat="server" Text="Include all ship-tos" Checked="True" />
                    <br />
                    <asp:CheckBox ID="cbMyCreditsOnly" runat="server" Text="My credit memos only" Checked="False" />
                    <br />
                    <asp:Button ID="btnFind" runat="server" OnClick="btnFind_Click" Text="Search" CssClass="btn btn-primary" />
                    <br />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right-section">
    <div class="container-sec">
        <h2>
            <asp:Label ID="lblCreditMemoHeader" runat="server" Text="Credit Memos"></asp:Label>
        </h2>
        <div class="custom-grid abc">
            <div style="width: 100%">
                <asp:Label ID="lblCreditMemoHeaderError" runat="server" Text="First 100 credit memos returned. Add criteria to refine search."
                    Visible="False" CssClass="ErrorTextRed"></asp:Label>
            </div>
            <div style="float: left; width: 100%">
                <div class="optional" style="width: 280px !important;">
                    <asp:Label ID="Label11" runat="server" Text="Show"></asp:Label>
                    <asp:DropDownList ID="ddlCreditMemoHeaderRows" runat="server" AutoPostBack="true"
                        Width="50px" OnSelectedIndexChanged="ddlCreditMemoHeaderRows_SelectedIndexChanged" CssClass="select opt-size">
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>75</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblresult" Text="Rows" runat="server" />
                    <asp:LinkButton ID="lnkCreditMemoHeaderCustomView" runat="server" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
                        DropShadow="true" PopupControlID="pnlCreditMemoHeaderCustomView"
                        TargetControlID="lnkCreditMemoHeaderCustomView">
                    </cc1:ModalPopupExtender>
                </div>
            </div>
            <asp:Panel ID="Panel10" runat="server">
                <div class="custom-grid-overflow">
                    <asp:GridView ID="gvCreditMemoHeaders" runat="server" AllowPaging="True"
                        Width="100%" OnPageIndexChanging="gvCreditMemoHeader_PageIndexChanging" OnRowDataBound="gvCreditMemoHeader_RowDataBound"
                        OnSelectedIndexChanged="gvCreditMemoHeader_SelectedIndexChanged" AutoGenerateColumns="False"
                        DataKeyNames="so_id" AllowSorting="True" OnRowCreated="gvCreditMemoHeader_RowCreated" AutoGenerateSelectButton="false"
                        OnSorting="gvCreditMemoHeader_Sorting" PageSize="5" CssClass="grid-tabler">
                        <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="30px" />
                        <SelectedRowStyle CssClass="active-row-tr" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <itemstyle width="5%" />
                                    <headerstyle width="5%" />
                                    <asp:ImageButton OnClick="Show_Hide_ChildGrid" CommandName="Details" runat="server" ID="imgbtnplusminus" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/Images/Grid_Plus.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="branch_id" HeaderText="Branch" ReadOnly="True" SortExpression="branch_id"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="so_id" HeaderText="Credit Memo ID" ReadOnly="True" SortExpression="so_id">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cm_invoice_ref" HeaderText="Original Invoice" ReadOnly="True"
                                SortExpression="cm_invoice_ref" Visible="False">
                                <ItemStyle Wrap="False" />
                                <HeaderStyle Wrap="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="job" HeaderText="Job #" ReadOnly="True" SortExpression="job"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="reference" HeaderText="Reference #" ReadOnly="True" SortExpression="reference">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cust_po" HeaderText="PO ID" ReadOnly="True" SortExpression="cust_po">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordered_by" HeaderText="Ordered By" ReadOnly="True" SortExpression="ordered_by"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_date" HeaderText="Order Date" ReadOnly="True" SortExpression="order_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="route_id_char" HeaderText="Route" ReadOnly="True" SortExpression="route_id_char"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_via" HeaderText="Ship Via" ReadOnly="True" SortExpression="ship_via"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type" HeaderText="Sale Type" ReadOnly="True" SortExpression="sale_type"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type_desc" HeaderText="Sale Type Description" ReadOnly="True"
                                SortExpression="sale_type_desc">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="statusdescr" HeaderText="Status" ReadOnly="True" SortExpression="statusdescr">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="freight_terms_id" HeaderText="Freight Terms" ReadOnly="True"
                                SortExpression="freight_terms_id" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="pay_terms_code" HeaderText="Terms" ReadOnly="True" SortExpression="pay_terms_code"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_subtotal" HeaderText="Sub Total" ReadOnly="True"
                                SortExpression="order_subtotal" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_taxes" HeaderText="Total Taxes" ReadOnly="True"
                                SortExpression="order_taxes" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_charges" HeaderText="Total Charges" ReadOnly="True"
                                SortExpression="order_charges" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_total" HeaderText="Total" ReadOnly="True" SortExpression="order_total"
                                DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_1" HeaderText="Sales Rep 1" ReadOnly="True" SortExpression="rep_1"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_2" HeaderText="Sales Rep 2" ReadOnly="True" SortExpression="rep_2"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_3" HeaderText="Sales Rep 3" ReadOnly="True" SortExpression="rep_3"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shiptoname" HeaderText="Ship-to" ReadOnly="True" SortExpression="shiptoname"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr1" HeaderText="Ship-to Address 1" ReadOnly="True"
                                SortExpression="shipToAddr1" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr2" HeaderText="Ship-to Address 2" ReadOnly="True"
                                SortExpression="shipToAddr2" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr3" HeaderText="Ship-to Address 3" ReadOnly="True"
                                SortExpression="shipToAddr3" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToCity" HeaderText="Ship-to City" ReadOnly="True"
                                SortExpression="shipToCity" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToState" HeaderText="Ship-to State" ReadOnly="True"
                                SortExpression="shipToState" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToZip" HeaderText="Ship-to ZIP" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <tr class="tr-innergrid child-div">
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px;">
                                            <asp:Panel Visible="false" runat="server" ID="innergrids" Style="background-color: White; width: 100%" class="panel-collapse show-panel acd collapse in">
                                                <div class="docgrid childgrid" style="padding-left: 40px">
                                                    <uc4:DocViewer ID="gvdetailsdocViewer" runat="server" />
                                                </div>
                                                <h3 style="padding-left: 40px; padding-top: 20px">
                                                    <asp:Label ID="lblCreditMemoDetail" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="lblCreditMemoDetailError" runat="server" Text="Record limit has been exceeded. Add criteria to refine search."
                                                        Visible="False" CssClass="ErrorTextRed"></asp:Label>
                                                </h3>
                                                <br />
                                                <div style="width: 100%; float: left; padding-left: 40px; margin-top: -10px">
                                                    <div class="optional" style="width: 380px !important">
                                                        <asp:Label ID="lblCreditMemoDetailRows" runat="server" Text="Show" />
                                                        <asp:DropDownList ID="ddlCreditMemoDetailRows" runat="server" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlCreditMemoDetailRows_SelectedIndexChanged" Width="50px" CssClass="select opt-size">
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>10</asp:ListItem>
                                                            <asp:ListItem>15</asp:ListItem>
                                                            <asp:ListItem>20</asp:ListItem>
                                                            <asp:ListItem>25</asp:ListItem>
                                                            <asp:ListItem Value="1000">All</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lblinnerrecords" runat="server" Text="Rows" />
                                                        <asp:LinkButton ID="lnkCrMemoDetailCustomView" runat="server" CssClass="SettingsLink optionslinkorder" CommandArgument='<%#Container.DataItemIndex%>' OnClick="lnkCrMemoDetailCustomView_Click">Select Columns</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <br />
                                                <div style="width: 100%; float: left">
                                                    <div class="childgrid">
                                                        <asp:GridView ID="gvCreditMemoDetailCustomView" runat="server" PageSize="5" Width="100%" AutoGenerateColumns="False"
                                                            AllowPaging="True" AllowSorting="True" DataKeyNames="so_id"
                                                            OnRowCreated="gvCreditMemoDetailCustomView_RowCreated" OnSorting="gvCreditMemoDetailCustomView_Sorting"
                                                            OnPageIndexChanging="gvCreditMemoDetailCustomView_PageIndexChanging" CssClass="sub-grd-tabler">
                                                            <PagerStyle CssClass="PagerDetailGridView" HorizontalAlign="Left" Height="10px" />
                                                            <Columns>
                                                                <asp:BoundField DataField="display_seq" HeaderText="Seq #" ReadOnly="True" SortExpression="display_seq"
                                                                    Visible="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="so_desc" HeaderText="Description" ReadOnly="True" SortExpression="so_desc">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="qty_ordered" HeaderText="Quantity" ReadOnly="True" SortExpression="qty_ordered"
                                                                    HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="uom" HeaderText="Qty UOM" ReadOnly="True" SortExpression="uom">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="net_price" HeaderText="Price" ReadOnly="True" SortExpression="net_price"
                                                                    DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="price_uom_code" HeaderText="Price UOM" ReadOnly="True"
                                                                    SortExpression="price_uom_code">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="extended_price" HeaderText="Extended Amount" ReadOnly="True"
                                                                    SortExpression="extended_price" DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr class="tr-innergrid">
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px"">
                                            <asp:Panel ID="innerplaceholder" runat="server" Visible="false" Style="width: 100%; background-color: White;" class="panel-collapse show-panel acd collapse in">
                                                <div>&nbsp;</div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvByItem" runat="server" PageSize="5" Width="100%" OnRowDataBound="gvCreditMemoHeader_RowDataBound"
                        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnRowCreated="gvByItem_RowCreated"
                        OnSorting="gvByItem_Sorting" OnPageIndexChanging="gvByItem_PageIndexChanging" CssClass="grid-tabler">
                        <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                        <Columns>
                            <asp:BoundField DataField="branch_id" HeaderText="Branch" ReadOnly="True" SortExpression="branch_id"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="so_id" HeaderText="Credit Memo ID" ReadOnly="True" SortExpression="so_id">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="so_desc" HeaderText="Description" ReadOnly="True" SortExpression="so_desc"
                                Visible="False">
                                <ItemStyle Wrap="True" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="qty_ordered" HeaderText="Quantity" ReadOnly="True" SortExpression="qty_ordered"
                                HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="uom" HeaderText="Qty UOM" ReadOnly="True" SortExpression="uom"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="net_price" HeaderText="Price" ReadOnly="True" SortExpression="net_price"
                                DataFormatString="{0:F2}" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="price_uom_code" HeaderText="Price UOM" ReadOnly="True"
                                SortExpression="price_uom_code" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="extended_price" HeaderText="Extended Amount" ReadOnly="True"
                                SortExpression="extended_price" DataFormatString="{0:F2}" HtmlEncode="False"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="job" HeaderText="Job #" ReadOnly="True" SortExpression="job"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="reference" HeaderText="Reference #" ReadOnly="True" SortExpression="reference">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cust_po" HeaderText="PO ID" ReadOnly="True" SortExpression="cust_po">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_date" HeaderText="Order Date" ReadOnly="True" SortExpression="order_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="route_id_char" HeaderText="Route" ReadOnly="True" SortExpression="route_id_char"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_via" HeaderText="Ship Via" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type" HeaderText="Sale Type" ReadOnly="True" SortExpression="sale_type"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type_desc" HeaderText="Sale Type Description" ReadOnly="True"
                                SortExpression="sale_type_desc">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="statusdescr" HeaderText="Status" ReadOnly="True" SortExpression="statusdescr">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_1" HeaderText="Sales Rep 1" ReadOnly="True" SortExpression="rep_1"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_2" HeaderText="Sales Rep 2" ReadOnly="True" SortExpression="rep_2"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_3" HeaderText="Sales Rep 3" ReadOnly="True" SortExpression="rep_3"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shiptoname" HeaderText="Ship-to" ReadOnly="True" SortExpression="shiptoname"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr1" HeaderText="Ship-to Address 1" ReadOnly="True"
                                SortExpression="shipToAddr1" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr2" HeaderText="Ship-to Address 2" ReadOnly="True"
                                SortExpression="shipToAddr2" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr3" HeaderText="Ship-to Address 3" ReadOnly="True"
                                SortExpression="shipToAddr3" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToCity" HeaderText="Ship-to City" ReadOnly="True"
                                SortExpression="shipToCity" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToState" HeaderText="Ship-to State" ReadOnly="True"
                                SortExpression="shipToState" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToZip" HeaderText="Ship-to ZIP" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="pagination-sec" style="width: 320px">
                        <div class="info" style="width: 315px">
                            <asp:Label ID="lblpageno" runat="server" Text="" />
                        </div>
                    </div>
                </div>
                <br />
                <asp:Panel ID="pnlSaleOrderDetail" runat="server">
                    <div>
                        <div class="optional" style="display: none">
                            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlCreditMemoDetailCustomView"
                                TargetControlID="lnkCreditMemoDetailCustomView">
                            </cc1:ModalPopupExtender>
                            <asp:LinkButton ID="lnkCreditMemoDetailCustomView" runat="server" CssClass="SettingsLink">Select Columns</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </div>
    </div>
</div>
<asp:Panel ID="pnlCreditMemoHeaderCustomView" runat="server">
    <div id="pnlCreditMemoHeaderDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View</h4>
            </div>
            <div class="modal-body">
                <uc3:ColumnSelector ID="csCustomHeader" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlCreditMemoDetailCustomView" runat="server">
    <div id="pnlCreditMemoDetailDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View</h4>
            </div>
            <div class="modal-body">
                <uc3:ColumnSelector ID="csCustomDetail" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
