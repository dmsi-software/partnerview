using System;
using System.Data;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI;

public partial class UserControls_OptionsSelector : System.Web.UI.UserControl
{
    Dmsi.Agility.EntryNET.DataManager _dm;

    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    private string _DisplaySetting;
    public string DisplaySetting
    {
        get { return _DisplaySetting; }
        set
        {
            _DisplaySetting = value;
            cbxDisplay.Text = value;
        }
    }

    private string _MarkupSetting;
    public string MarkupSetting
    {
        get { return _MarkupSetting; }
        set
        {
            _MarkupSetting = value;
        }
    }

    private Boolean _Retain;
    public Boolean Retain
    {
        get { return _Retain; }
        set
        {
            _Retain = value;
            cbxRetain.Checked = value;
        }
    }

    private decimal _MarkupFactor;
    public decimal MarkupFactor
    {
        get { return _MarkupFactor; }
        set
        {
            _MarkupFactor = value;

            if ((string)Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
            {
                numericMarkupFactor_List.ValueDecimal = value;
            }
            else
                numericMarkupFactor.ValueDecimal = value;
        }
    }

    private int _MarginFactor;
    public int MarginFactor
    {
        get { return _MarginFactor; }
        set { _MarginFactor = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _dm = new DataManager();

        if (!IsPostBack)
        {
            txtURL.Text = Profile.RetailQuoteLogoURL;
        }

        if (Session["MarkupSetting"] != null)
            this.MarkupSetting = (string)Session["MarkupSetting"];

        if (Session["UserPref_Retain"] != null)
            this.Retain = (bool)Session["UserPref_Retain"];

        if (Session["MarginFactor"] != null)
            this.MarginFactor = (int)Session["MarginFactor"];
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        PriceModeHelper pmh = new PriceModeHelper(_dm);

        if (pmh.InAModeUserDoesntHaveAccessTo())
        {
            Profile.display_Setting = pmh.DefaultUserPreferenceDisplaySetting();
            System.Web.Profile.ProfileBase.Properties["display_Setting"].DefaultValue = Profile.display_Setting;
            this.DisplaySetting = Profile.display_Setting;
            Session["UserPref_DisplaySetting"] = Profile.display_Setting;
            Profile.Save();
        }

        if (Session["UserPref_DisplaySetting"] != null)
        {
            if (pmh.InRetailMode())
            {
                this.cbxDisplay.Text = "Retail price";
                hdfMode.Value = pmh.MARKUPONCOSTMODE;
                ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
            }
            else if (pmh.InRetailListMode())
            {
                this.cbxDisplay_List.Text = "Retail price - discount from list";
                hdfMode.Value = pmh.DISCOUNTFROMLISTMODE;
                ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
            }
            else if (pmh.InRetailMarkupMode())
            {
                this.cbxDisplay_List.Text = "Retail price - markup on cost";
                hdfMode.Value = pmh.MARKUPONCOSTMODE;
                ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
            }
            else if (pmh.InNetMode())
            {
                this.cbxDisplay.Text = "Net price";
                this.cbxDisplay_List.Text = "Net price";
                hdfMode.Value = pmh.NETMODE;
                ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
            }
        }

        numericDefaultTaxRate.Text = Profile.DefaultTaxRate;

        if (pmh.HaveRetailDiscountFromListModeEnabled())
        {
            SetDisplayForListBasedRetailPrice();
        }
        else
        {
            SetDisplayForNoListBasedRetailPrice();
        }

        SetDisplayBasedOnPriceModeActionAllocations(pmh);
    }

    private void SetDisplayForListBasedRetailPrice()
    {
        cbxDisplay.Style.Add("width", "1px");
        cbxDisplay.Style.Add("height", "1px");
        cbxDisplay.Style.Add("padding-top", "0px");
        cbxDisplay.Style.Add("padding-bottom", "0px");
        cbxDisplay.Style.Add("border", "none");
        cbxDisplay.CssClass = "";
        cbxDisplay.Style.Add("font-size", "0px");
        cbxDisplay.Style.Add("opacity", "0");
        cbxDisplay.TabIndex = 99;

        spanNonListModeOptions.Visible = false;

        cbxDisplay_List.Style.Remove("width");
        cbxDisplay_List.Style.Remove("height");
        cbxDisplay_List.Style.Remove("padding-top");
        cbxDisplay_List.Style.Remove("padding-bottom");
        cbxDisplay_List.Style.Remove("border");
        cbxDisplay_List.CssClass = "input igte_Edit";
        cbxDisplay_List.Style.Remove("font-size");
        cbxDisplay_List.Style.Add("font-size", "8pt");
        cbxDisplay_List.Style.Remove("opacity");
        cbxDisplay_List.TabIndex = 1;

        if (Session["UserPref_DisplaySetting"] != null)
        {
            if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost")
            {
                this.cbxDisplay_List.Text = "Retail price - markup on cost";

                numericMarkupFactor_List.Style.Remove("display");
                numericMarkupFactor_List.Style.Add("display", "none");

                numericMarkupFactor.Style.Remove("display");

                lblRetail_List.Style.Remove("display");
                lblRetail_List.Style.Add("display", "none");

                lblExample1.Style.Remove("display");
                lblExample1.Style.Add("display", "none");

                lblExample2.Style.Remove("display");
                lblExample2.Style.Add("display", "none");

                lblExample3.Style.Remove("display");
                lblExample3.Style.Add("display", "none");

                lblRetail.Style.Remove("display");

                cbxMarkupToggle.Style.Remove("display");

                lblMultiplier.Style.Remove("display");
                lblMultiplier.Style.Add("display", "none");

                ToggleMargin();
            }

            if ((string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
            {
                this.cbxDisplay_List.Text = "Retail price - discount from list";

                numericMarkupFactor.Style.Add("display", "none");
                numericMarkupFactor_List.Style.Remove("display");

                lblRetail.Style.Add("display", "none");
                lblRetail_List.Style.Remove("display");
                lblExample1.Style.Remove("display");
                lblExample2.Style.Remove("display");
                lblExample3.Style.Remove("display");
                lblMultiplier.Style.Remove("display");

                lblMargin.Style.Remove("display");
                lblMargin.Style.Add("display", "none");

                numericMarginFactor.Style.Remove("display");
                numericMarginFactor.Style.Add("display", "none");

                cbxMarkupToggle.Style.Remove("display");
                cbxMarkupToggle.Style.Add("display", "none");
            }

            if ((string)Session["UserPref_DisplaySetting"] == "Net price")
            {
                this.cbxDisplay_List.Text = "Net price";

                numericMarkupFactor_List.Style.Remove("display");
                numericMarkupFactor_List.Style.Add("display", "none");

                numericMarkupFactor.Style.Remove("display");
                numericMarkupFactor.Style.Add("display", "none");

                lblMultiplier.Style.Remove("display");
                lblMultiplier.Style.Add("display", "none");

                lblRetail_List.Style.Remove("display");
                lblRetail_List.Style.Add("display", "none");

                lblExample1.Style.Remove("display");
                lblExample1.Style.Add("display", "none");

                lblExample2.Style.Remove("display");
                lblExample2.Style.Add("display", "none");

                lblExample3.Style.Remove("display");
                lblExample3.Style.Add("display", "none");

                lblRetail.Style.Remove("display");
                lblRetail.Style.Add("display", "none");

                lblMargin.Style.Remove("display");
                lblMargin.Style.Add("display", "none");

                numericMarginFactor.Style.Remove("display");
                numericMarginFactor.Style.Add("display", "none");

                cbxMarkupToggle.Style.Remove("display");
                cbxMarkupToggle.Style.Add("display", "none");
            }
        }
    }

    private void SetDisplayForNoListBasedRetailPrice()
    {
        cbxDisplay.Style.Remove("width");
        cbxDisplay.Style.Remove("height");
        cbxDisplay.Style.Remove("padding-top");
        cbxDisplay.Style.Remove("padding-bottom");
        cbxDisplay.Style.Remove("border");
        cbxDisplay.CssClass = "input igte_Edit";
        cbxDisplay.Style.Remove("font-size");
        cbxDisplay.Style.Add("font-size", "8pt");
        cbxDisplay.Style.Remove("opacity");
        cbxDisplay.TabIndex = 1;

        cbxDisplay_List.Style.Add("width", "1px");
        cbxDisplay_List.Style.Add("height", "1px");
        cbxDisplay_List.Style.Add("padding-top", "0px");
        cbxDisplay_List.Style.Add("padding-bottom", "0px");
        cbxDisplay_List.Style.Add("border", "none");
        cbxDisplay_List.CssClass = "";
        cbxDisplay_List.Style.Add("font-size", "0px");
        cbxDisplay_List.Style.Add("opacity", "0");
        cbxDisplay_List.TabIndex = 99;

        spanListModeOptions.Visible = false;

        numericMarkupFactor.Style.Remove("display");
        numericMarkupFactor.Style.Add("display", "none");

        numericMarkupFactor_List.Style.Remove("display");
        numericMarkupFactor_List.Style.Add("display", "none");

        lblRetail_List.Style.Remove("display");
        lblRetail_List.Style.Add("display", "none");

        lblExample1.Style.Remove("display");
        lblExample1.Style.Add("display", "none");

        lblExample2.Style.Remove("display");
        lblExample2.Style.Add("display", "none");

        lblExample3.Style.Remove("display");
        lblExample3.Style.Add("display", "none");

        numericMarkupFactor_List.Style.Remove("display");
        numericMarkupFactor_List.Style.Add("display", "none");

        lblRetail_List.Style.Remove("display");
        lblRetail_List.Style.Add("display", "none");

        lblExample1.Style.Remove("display");
        lblExample1.Style.Add("display", "none");

        lblExample2.Style.Remove("display");
        lblExample2.Style.Add("display", "none");

        lblExample3.Style.Remove("display");
        lblExample3.Style.Add("display", "none");

        lblMultiplier.Style.Remove("display");
        lblMultiplier.Style.Add("display", "none");

        if ((string)Session["UserPref_DisplaySetting"] != "Net price")
        {
            cbxMarkupToggle.Style.Remove("display");
            ToggleMargin();
        }

        if ((string)Session["UserPref_DisplaySetting"] == "Net price")
        {
            lblRetail.Style.Remove("display");
            lblRetail.Style.Add("display", "none");

            lblMargin.Style.Remove("display");
            lblMargin.Style.Add("display", "none");

            lblRetail_List.Style.Remove("display");
            lblRetail_List.Style.Add("display", "none");

            lblExample1.Style.Remove("display");
            lblExample1.Style.Add("display", "none");

            lblExample2.Style.Remove("display");
            lblExample2.Style.Add("display", "none");

            lblExample3.Style.Remove("display");
            lblExample3.Style.Add("display", "none");

            lblMultiplier.Style.Remove("display");
            lblMultiplier.Style.Add("display", "none");

            cbxMarkupToggle.Style.Remove("display");
            cbxMarkupToggle.Style.Add("display", "none");

            numericMarkupFactor.Style.Remove("display");
            numericMarkupFactor.Style.Add("display", "none");

            numericMarginFactor.Style.Remove("display");
            numericMarginFactor.Style.Add("display", "none");

            numericMarkupFactor_List.Style.Remove("display");
            numericMarkupFactor_List.Style.Add("display", "none");
        }
    }

    private void SetDisplayBasedOnPriceModeActionAllocations(PriceModeHelper pmh)
    {
        bool lNetModeEnabled = pmh.HaveNetModeEnabled();
        bool lRetailPricingEnabled = pmh.HaveRetailPricingEnabled();
        bool lMarkupModeEnabled = pmh.HaveRetailMarkupModeEnabled();
        bool lListModeEnabled = pmh.HaveRetailDiscountFromListModeEnabled();

        if (!lNetModeEnabled)
        {
            cbxDisplay_List.Items.Remove(pmh.NETPRICE);
            cbxDisplay.Items.Remove(pmh.NETPRICE);
        }
        if (!lMarkupModeEnabled) { cbxDisplay_List.Items.Remove(pmh.RETAILPRICEMARKUPONCOST); }
        if (!lListModeEnabled) { cbxDisplay_List.Items.Remove(pmh.RETAILPRICEDISCOUNTFROMLIST); }

        if (pmh.OnlyOnePriceModeEnabled(lNetModeEnabled,
                                        lRetailPricingEnabled,
                                        lMarkupModeEnabled,
                                        lListModeEnabled))
        {
            lblPreferenceLabel.Visible = false;
            cbxDisplay_List.Visible = false;
            cbxDisplay.Visible = false;
            cbxRetain.Visible = false;
            SetHiddenModeField(pmh);
        }

        lblDescForNetPrice.Visible = lNetModeEnabled;
        lblDescForRetailPrice.Visible = lRetailPricingEnabled;
        lblDescForCostMarkup.Visible = lMarkupModeEnabled;
        lblDescForListDiscount.Visible = lListModeEnabled;
    }

    private void SetHiddenModeField(PriceModeHelper pmh)
    {
        string displaySetting = pmh.CurrentDisplaySetting();

        if (displaySetting == pmh.RETAILPRICEDISCOUNTFROMLIST) { hdfMode.Value = pmh.DISCOUNTFROMLISTMODE; }
        else if (displaySetting == pmh.RETAILPRICEMARKUPONCOST) { hdfMode.Value = pmh.MARKUPONCOSTMODE; }
        else if (displaySetting == pmh.NETPRICE) { hdfMode.Value = pmh.NETMODE; }
        else { hdfMode.Value = pmh.MARKUPONCOSTMODE; }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        Session["CachedMultiplier"] = null; //Need to do this for quote edit "Apply New Multiplier/Margin popup...
        Session["CachedMargin"] = null;
        PriceModeHelper pmh = new PriceModeHelper(_dm);

        if (pmh.HaveRetailDiscountFromListModeEnabled())
        {
            Session["UserPref_DisplaySetting"] = cbxDisplay_List.Text;
        }
        else
        {
            Session["UserPref_DisplaySetting"] = cbxDisplay.Text;
        }

        if (cbxRetain.Checked)
        {
            Profile.display_Setting = (string)Session["UserPref_DisplaySetting"];
        }

        string ddlValue = cbxMarkupToggle.SelectedItem.Text;
        Profile.ddlMarkup_Setting = ddlValue;
        Session["MarkupSetting"] = ddlValue;
        this.MarkupSetting = ddlValue;

        decimal marginValue = Convert.ToDecimal(numericMarginFactor.Text);
        this.MarginFactor = Convert.ToInt32(marginValue);
        Session["MarginFactor"] = Convert.ToInt32(marginValue);

        decimal markupValue = 1;

        if (Session["UserPref_DisplaySetting"] != null &&
            ((string)Session["UserPref_DisplaySetting"] == "Net price" ||
             (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
             (string)Session["UserPref_DisplaySetting"] == "Retail price"))
        {
            markupValue = Convert.ToDecimal(numericMarkupFactor.Text);
        }

        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
        {
            markupValue = Convert.ToDecimal(numericMarkupFactor_List.Text);
        }

        Session["MarkupFactor"] = markupValue;

        if (Session["UserPref_DisplaySetting"] != null && ((string)Session["UserPref_DisplaySetting"] == "Net price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)Session["UserPref_DisplaySetting"] == "Retail price"))
        {
            if (cbxMarkupToggle.SelectedItem.Text == "Multiplier")
            {
                //convert Multiplier to Margin and save both
                int marginFactor = Convert.ToInt32(Math.Round(((markupValue - 1) / (1 + (markupValue - 1))) * 100, 0));
                Profile.price_Margin_Factor = marginFactor;
                Profile.price_Markup_Factor = markupValue;
                Session["MarkupFactor"] = markupValue;
                Session["MarginFactor"] = marginFactor;
                this.MarkupFactor = markupValue;
                this.MarginFactor = marginFactor;
            }
            else
            {
                //convert Margin to Multiplier and save both
                decimal markupFactor = Math.Round(Convert.ToDecimal(1 / (1 - (marginValue / 100))), 4);
                Profile.price_Markup_Factor = markupFactor;
                Profile.price_Margin_Factor = Convert.ToInt32(marginValue);
                Session["MarkupFactor"] = markupFactor;
                Session["MarginFactor"] = Convert.ToInt32(marginValue);
                this.MarkupFactor = markupFactor;
                this.MarginFactor = Convert.ToInt32(marginValue);
            }
        }
        else
        {
            if (cbxDisplay_List.Visible == true && cbxDisplay_List.Text == "Retail price - discount from list")
            {
                Profile.price_Markup_Factor_List = markupValue;
            }
        }

        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Net price")
        {
            if (cbxMarkupToggle.Attributes["style"] == null)
            {
                Session["MarkupFactor"] = Convert.ToDecimal(numericMarkupFactor.Text);
                Profile.price_Markup_Factor = Convert.ToDecimal(numericMarkupFactor.Text);
            }
        }

        Profile.RetailQuoteLogoURL = txtURL.Text;
        Profile.DefaultTaxRate = (numericDefaultTaxRate.Text.Trim() == "" ? "0" : numericDefaultTaxRate.Text.Trim());

        Profile.Save();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtURL.Text = Profile.RetailQuoteLogoURL;
    }

    protected void btnModalPopup_Click(object sender, EventArgs e)
    {
        bool retainDisplay1 = true;
        bool dataHasChanged = true;

        string displaySetting = string.Empty;
        decimal markupFactor = 0;

        string PriceType = string.Empty;

        retainDisplay1 = cbxRetain.Checked;

        if (_dm.GetCache("AllowListBasedRetailPrice") != null && ((string)_dm.GetCache("AllowListBasedRetailPrice")).ToLower().Trim() == "yes")
        {
            markupFactor = Convert.ToDecimal(numericMarkupFactor_List.Text);
            displaySetting = cbxDisplay_List.SelectedItem.Text;
        }
        else
        {
            markupFactor = Convert.ToDecimal(numericMarkupFactor.Text);
            displaySetting = cbxDisplay.SelectedItem.Text;
        }

        if (Session["UserPref_DisplaySetting"] != null && displaySetting == (string)Session["UserPref_DisplaySetting"] && retainDisplay1 == (bool)Session["UserPref_Retain"] && markupFactor == Convert.ToDecimal(Session["MarkupFactor"]))
        {
            dataHasChanged = false;
        }

        if (dataHasChanged == true)
        {
            Session["btnReturnToItem_Click"] = 7;
        }
        else
        {
            Session["btnReturnToItem_Click"] = 6;
        }

        Session["UserPref_CheckboxRetain"] = retainDisplay1;
        Session["UserPref_NumericMarkup"] = markupFactor;

        if (displaySetting == "Retail price" || displaySetting == "Retail price - markup on cost" || displaySetting == "Retail price - discount from list")
        {
            Session["UserPref_PiceDisplaySetting"] = displaySetting;
            PriceType = "Retail price";
        }
        else if (displaySetting == "Net price")
        {
            Session["UserPref_PiceDisplaySetting"] = "Net price";
            PriceType = "Price";
        }
    }

    private void ToggleMargin()
    {
        if (this.MarkupSetting == "Margin")
        {
            cbxMarkupToggle.SelectedIndex = 1; //Margin
            numericMarginFactor.ValueInt = this.MarginFactor;

            numericMarkupFactor.Style.Remove("display");
            numericMarkupFactor_List.Style.Remove("display");
            numericMarginFactor.Style.Remove("display");

            numericMarkupFactor.Style.Add("display", "none");
            numericMarkupFactor_List.Style.Add("display", "none");

            lblRetail.Style.Remove("display");
            lblRetail_List.Style.Remove("display");
            lblExample1.Style.Remove("display");
            lblExample2.Style.Remove("display");
            lblExample3.Style.Remove("display");
            lblMargin.Style.Remove("display");

            lblRetail.Style.Add("display", "none");
            lblRetail_List.Style.Add("display", "none");
            lblExample1.Style.Add("display", "none");
            lblExample2.Style.Add("display", "none");
            lblExample3.Style.Add("display", "none");
        }
        else
        {
            cbxMarkupToggle.SelectedIndex = 0; //Multiplier
            numericMarkupFactor.ValueDecimal = this.MarkupFactor;

            numericMarkupFactor.Style.Remove("display");
            numericMarkupFactor_List.Style.Remove("display");
            numericMarginFactor.Style.Remove("display");

            numericMarginFactor.Style.Add("display", "none");
            numericMarkupFactor_List.Style.Add("display", "none");

            lblRetail.Style.Remove("display");
            lblRetail_List.Style.Remove("display");
            lblExample1.Style.Remove("display");
            lblExample2.Style.Remove("display");
            lblExample3.Style.Remove("display");
            lblMargin.Style.Remove("display");

            lblMargin.Style.Add("display", "none");
            lblRetail_List.Style.Add("display", "none");
            lblExample1.Style.Add("display", "none");
            lblExample2.Style.Add("display", "none");
            lblExample3.Style.Add("display", "none");
        }
    }
}
