using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_SimplePanel : System.Web.UI.UserControl
{
    private bool _HideHeader;
    private string _HeaderText;
    private string _BodyText;

    public bool HideHeader
    {
        get { return _HideHeader; }
        set { _HideHeader = value; }
    }

    public string HeaderText
    {
        get { return _HeaderText; }
        set { _HeaderText = value; }
    }

    public string BodyText
    {
        get { return _BodyText; }
        set { _BodyText = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Label content = new Label();
        content.Text = "<div style=\"padding-bottom:3px;marging-top:5px\"><div class=\"container-sec\">";

        if (!this.HideHeader)
        {
            content.Text += "<h2 style=\"padding-top:5px\">";
         //   content.Text += "<td style=\"width: 100%\" class=\"SimplePanelHeader\">";
            content.Text += this.HeaderText;
            //content.Text += "</td>";
            content.Text += "</h2>";
        }
        content.Text += "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\">";

        content.Text += "<tr>";
        content.Text += "<td class=\"SimplePanelBody\">";
        content.Text += this.BodyText; ;
        content.Text += "</td>";            
        content.Text += "</tr>";
        content.Text += "</table>";
        content.Text += "</div></div>";

        this.Controls.Add(content);
    }
}
