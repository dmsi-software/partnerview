using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI.HtmlControls;

public partial class UserControls_AddInfoQuoteEditSelector : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate();
    public event OKClickedDelegate OnOKClicked;

    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    public delegate void CancelClickedDelegate();
    public event CancelClickedDelegate OnCancelClicked;


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["Tmp_AddInfoQuoteEdit_txtCustomerId"] != null)
        {
            txtCustomerId.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtCustomerId"]);
            txtShipTo.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtShipTo"]);
            txtCustomerName.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtCustomerName"]);
            txtAddress1.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtAddress1"]);
            txtAddress2.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtAddress2"]);
            txtAddress3.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtAddress3"]);
            txtCity.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtCity"]);
            txtState.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtState"]);
            txtZip.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtZip"]);
            txtCountry.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtCountry"]);
            txtPhone.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtPhone"]);
            txtContactName.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtContactName"]);
            txtEmail.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtEmail"]);
            txtReference.Text = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_txtReference"]);
            otherChargesAndTaxes.TaxRate = Convert.ToDecimal(Session["Tmp_AddInfoQuoteEdit_taxRate"]);
            otherChargesAndTaxes.TaxableDescription = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_taxableDescription"]);
            otherChargesAndTaxes.TaxableAmount = Convert.ToDecimal(Session["Tmp_AddInfoQuoteEdit_taxableAmount"]);
            otherChargesAndTaxes.NonTaxableDescription = Convert.ToString(Session["Tmp_AddInfoQuoteEdit_nonTaxableDescription"]);
            otherChargesAndTaxes.NonTaxableAmount = Convert.ToDecimal(Session["Tmp_AddInfoQuoteEdit_nonTaxableAmount"]);
        }
        else if (Session["AddInfoQuoteEdit_txtCustomerId"] != null)
        {
            txtCustomerId.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtCustomerId"]);
            txtShipTo.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtShipTo"]);
            txtCustomerName.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtCustomerName"]);
            txtAddress1.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtAddress1"]);
            txtAddress2.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtAddress2"]);
            txtAddress3.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtAddress3"]);
            txtCity.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtCity"]);
            txtState.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtState"]);
            txtZip.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtZip"]);
            txtCountry.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtCountry"]);
            txtPhone.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtPhone"]);
            txtContactName.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtContactName"]);
            txtEmail.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtEmail"]);
            txtReference.Text = Convert.ToString(Session["AddInfoQuoteEdit_txtReference"]);
            otherChargesAndTaxes.TaxRate = Convert.ToDecimal(Session["AddInfoQuoteEdit_taxRate"]);
            otherChargesAndTaxes.TaxableDescription = Convert.ToString(Session["AddInfoQuoteEdit_taxableDescription"]);
            otherChargesAndTaxes.TaxableAmount = Convert.ToDecimal(Session["AddInfoQuoteEdit_taxableAmount"]);
            otherChargesAndTaxes.NonTaxableDescription = Convert.ToString(Session["AddInfoQuoteEdit_nonTaxableDescription"]);
            otherChargesAndTaxes.NonTaxableAmount = Convert.ToDecimal(Session["AddInfoQuoteEdit_nonTaxableAmount"]);
        }
        else
        {
            txtCustomerId.Text = "";
            txtShipTo.Text = "";
            txtCustomerName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtContactName.Text = "";
            txtEmail.Text = "";
            txtReference.Text = "";
            otherChargesAndTaxes.TaxRate = GetDefaultTaxRate();
            otherChargesAndTaxes.TaxableDescription = "";
            otherChargesAndTaxes.TaxableAmount = 0;
            otherChargesAndTaxes.NonTaxableDescription = "";
            otherChargesAndTaxes.NonTaxableAmount = 0;
        }
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
    }

    protected decimal GetDefaultTaxRate()
    {
        return Convert.ToDecimal(Profile.DefaultTaxRate == "" ? "0": Profile.DefaultTaxRate);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        Session["Tmp_AddInfoQuoteEdit_txtCustomerId"] = txtCustomerId.Text;
        Session["Tmp_AddInfoQuoteEdit_txtShipTo"] = txtShipTo.Text;
        Session["Tmp_AddInfoQuoteEdit_txtCustomerName"] = txtCustomerName.Text;
        Session["Tmp_AddInfoQuoteEdit_txtAddress1"] = txtAddress1.Text;
        Session["Tmp_AddInfoQuoteEdit_txtAddress2"] = txtAddress2.Text;
        Session["Tmp_AddInfoQuoteEdit_txtAddress3"] = txtAddress3.Text;
        Session["Tmp_AddInfoQuoteEdit_txtCity"] = txtCity.Text;
        Session["Tmp_AddInfoQuoteEdit_txtState"] = txtState.Text;
        Session["Tmp_AddInfoQuoteEdit_txtZip"] = txtZip.Text;
        Session["Tmp_AddInfoQuoteEdit_txtCountry"] = txtCountry.Text;
        Session["Tmp_AddInfoQuoteEdit_txtPhone"] = txtPhone.Text;
        Session["Tmp_AddInfoQuoteEdit_txtContactName"] = txtContactName.Text;
        Session["Tmp_AddInfoQuoteEdit_txtEmail"] = txtEmail.Text;
        Session["Tmp_AddInfoQuoteEdit_txtReference"] = txtReference.Text;
        Session["Tmp_AddInfoQuoteEdit_taxRate"] = otherChargesAndTaxes.TaxRate;
        Session["Tmp_AddInfoQuoteEdit_taxableDescription"] = otherChargesAndTaxes.TaxableDescription;
        Session["Tmp_AddInfoQuoteEdit_taxableAmount"] = otherChargesAndTaxes.TaxableAmount;
        Session["Tmp_AddInfoQuoteEdit_nonTaxableDescription"] = otherChargesAndTaxes.NonTaxableDescription;
        Session["Tmp_AddInfoQuoteEdit_nonTaxableAmount"] = otherChargesAndTaxes.NonTaxableAmount;

        if (OnOKClicked != null)
            OnOKClicked(); // will be caught by the parent
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (OnCancelClicked != null)
            OnCancelClicked(); // will be caught by the parent
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtCustomerId.Text = "";
        txtShipTo.Text = "";
        txtCustomerName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtZip.Text = "";
        txtCountry.Text = "";
        txtPhone.Text = "";
        txtContactName.Text = "";
        txtEmail.Text = "";
        txtReference.Text = "";
        otherChargesAndTaxes.TaxRate = GetDefaultTaxRate();
        otherChargesAndTaxes.TaxableDescription = "";
        otherChargesAndTaxes.TaxableAmount = 0;
        otherChargesAndTaxes.NonTaxableDescription = "";
        otherChargesAndTaxes.NonTaxableAmount = 0;

        Session["AddInfoQuoteEdit_txtCustomerId"] = null;
        Session["AddInfoQuoteEdit_txtShipTo"] = null;
        Session["AddInfoQuoteEdit_txtCustomerName"] = null;
        Session["AddInfoQuoteEdit_txtAddress1"] = null;
        Session["AddInfoQuoteEdit_txtAddress2"] = null;
        Session["AddInfoQuoteEdit_txtAddress3"] = null;
        Session["AddInfoQuoteEdit_txtCity"] = null;
        Session["AddInfoQuoteEdit_txtState"] = null;
        Session["AddInfoQuoteEdit_txtZip"] = null;
        Session["AddInfoQuoteEdit_txtCountry"] = null;
        Session["AddInfoQuoteEdit_txtPhone"] = null;
        Session["AddInfoQuoteEdit_txtContactName"] = null;
        Session["AddInfoQuoteEdit_txtEmail"] = null;
        Session["AddInfoQuoteEdit_txtReference"] = null;
        Session["AddInfoQuoteEdit_taxRate"] = null;
        Session["AddInfoQuoteEdit_taxableDescription"] = null;
        Session["AddInfoQuoteEdit_taxableAmount"] = null;
        Session["AddInfoQuoteEdit_nonTaxableDescription"] = null;
        Session["AddInfoQuoteEdit_nonTaxableAmount"] = null;

        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }
}
