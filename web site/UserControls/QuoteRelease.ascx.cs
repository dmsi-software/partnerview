﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Infragistics.Web.UI.GridControls;

public partial class UserControls_QuoteRelease : System.Web.UI.UserControl
{
    public delegate void QuoteReleased();
    public event QuoteReleased OnQuoteReleased;

    private Dmsi.Agility.Data.Quotes _quote;
    private const string STALEPVQUOTETITLE = "Quote Not Submitted";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CachedQuoteReleaseDataSet"] != null)
        {
            wdgQuoteDetails.ClearDataSource();
            wdgQuoteDetails.DataSource = null;

            wdgQuoteDetails.DataSource = (dsQuoteDataSet)Session["CachedQuoteReleaseDataSet"];
            wdgQuoteDetails.DataMember = "pvttquote_detail";
            wdgQuoteDetails.DataBind();
        }

        this.AddInfoQuoteSelector.OnOKClicked += new UserControls_AddInfoQuoteSelector.OKClickedDelegate(AddInfoQuoteSelector_OnOKClicked);
        this.AddInfoQuoteSelector.OnCancelClicked += new UserControls_AddInfoQuoteSelector.CancelClickedDelegate(AddInfoQuoteSelector_OnCancelClicked);

        if (!IsPostBack)
        {
            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
            txtEmail.Text = user.GetContextValue("currentUserEmail");
        
        }

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            AddInfoModalDiv.Visible = true;
            this.lnkAddInfo.Visible = true;
            this.lblCustInfo.Visible = true;
        }
        else
        {
            AddInfoModalDiv.Visible = false;
            this.lnkAddInfo.Visible = false;
            this.lblCustInfo.Visible = false;
        }

        if (Session["disable_edit_shipping_address"] != null && (bool)Session["disable_edit_shipping_address"] == true)
        {
            lblShiptoQuote.Visible = false;
            lnkShiptoQuote.Visible = false;
            ShiptoQuoteModalDiv.Visible = false;
            cbxShipComplete.CssClass = "editcheckbox1 editcheckrelease";
        }
        else
        {
            lblShiptoQuote.Visible = true;
            lnkShiptoQuote.Visible = true;
            ShiptoQuoteModalDiv.Visible = true;
            cbxShipComplete.CssClass = "editcheckbox1 editcheckrelease TopMargin10";
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        DataManager dm = new DataManager();

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            wdgQuoteDetails.Columns["price"].Hidden = true;
            wdgQuoteDetails.Columns["price_uom_code"].Hidden = true;
            wdgQuoteDetails.Columns["extended_price"].Hidden = true;

            wdgQuoteDetails.Columns["retail_price"].Hidden = false;
            wdgQuoteDetails.Columns["retail_price_uom_code"].Hidden = false;
            wdgQuoteDetails.Columns["retail_extended_price"].Hidden = false;

            lblPriceDisclaimer.Visible = false;
        }
        else
        {
            wdgQuoteDetails.Columns["price"].Hidden = false;
            wdgQuoteDetails.Columns["price_uom_code"].Hidden = false;
            wdgQuoteDetails.Columns["extended_price"].Hidden = false;

            wdgQuoteDetails.Columns["retail_price"].Hidden = true;
            wdgQuoteDetails.Columns["retail_price_uom_code"].Hidden = true;
            wdgQuoteDetails.Columns["retail_extended_price"].Hidden = true;

            lblPriceDisclaimer.Visible = true;
        }

        if (Session["partial_release_only"] != null && (bool)Session["partial_release_only"] == true)
        {
            Session["partial_release_only"] = false; //reset this on each use

            foreach (GridRecord record in wdgQuoteDetails.Rows)
            {
                GridRecordItem it = record.Items.FindItemByKey("qty_to_release");
                if (it != null)
                {
                    TextBox tbx = it.FindControl("txtQtyToRelease") as TextBox;
                    if (tbx != null)
                        tbx.Enabled = false;
                }
            }
        }

        dsQuoteDataSet dsQuote = new dsQuoteDataSet();
        dsRetailAddressDataSet dsRetailAddress;

        if (Session["QuoteRelease_dsQuote"] != null)
            dsQuote = (dsQuoteDataSet)Session["QuoteRelease_dsQuote"];

        if (dsQuote != null)
        {
            UserControls_ShiptoQuoteSelector selector = (UserControls_ShiptoQuoteSelector)FindControl("ShiptoQuoteSelector1");
            selector.QuoteDataSet = dsQuote;
        }

        if (Session["UIHasChangedAddInfo"] == null && Session["UIHasCanceledAddInfo"] == null)
        {
            if (Session["dsRetailAddressForQuotes"] != null && dsQuote.pvttquote_header.Rows.Count > 0)
            {
                dsRetailAddress = (dsRetailAddressDataSet)Session["dsRetailAddressForQuotes"];
                DataRow[] rows = dsRetailAddress.tt_retail_address.Select("tran_id=" + dsQuote.pvttquote_header[0]["quote_id"].ToString());

                if (rows.Length > 0)
                {
                    Session["AddInfoQuote_txtCustomerId"] = rows[0]["misc_1"].ToString();
                    Session["AddInfoQuote_txtShipTo"] = rows[0]["misc_2"].ToString();
                    Session["AddInfoQuote_txtCustomerName"] = rows[0]["cust_name"].ToString();
                    Session["AddInfoQuote_txtAddress1"] = rows[0]["address_1"].ToString();
                    Session["AddInfoQuote_txtAddress2"] = rows[0]["address_2"].ToString();
                    Session["AddInfoQuote_txtAddress3"] = rows[0]["address_3"].ToString();
                    Session["AddInfoQuote_txtCity"] = rows[0]["city"].ToString();
                    Session["AddInfoQuote_txtState"] = rows[0]["state"].ToString();
                    Session["AddInfoQuote_txtZip"] = rows[0]["zip"].ToString();
                    Session["AddInfoQuote_txtCountry"] = rows[0]["country"].ToString();
                    Session["AddInfoQuote_txtPhone"] = rows[0]["phone"].ToString();
                    Session["AddInfoQuote_txtContactName"] = rows[0]["misc_3"].ToString();
                    Session["AddInfoQuote_txtEmail"] = rows[0]["email"].ToString();
                    Session["AddInfoQuote_txtReference"] = rows[0]["misc_4"].ToString();
                    Session["AddInfoQuote_taxRate"] = Convert.ToDecimal(rows[0]["misc_5"].ToString() == "" ? GetDefaultTaxRate().ToString() : rows[0]["misc_5"].ToString());
                    Session["AddInfoQuote_taxableDescription"] = rows[0]["misc_6"].ToString();
                    Session["AddInfoQuote_taxableAmount"] = Convert.ToDecimal(rows[0]["misc_7"].ToString() == "" ? "0.00" : rows[0]["misc_7"].ToString());
                    Session["AddInfoQuote_nonTaxableDescription"] = rows[0]["misc_8"].ToString();
                    Session["AddInfoQuote_nonTaxableAmount"] = Convert.ToDecimal(rows[0]["misc_9"].ToString() == "" ? "0.00" : rows[0]["misc_9"].ToString());
                }
            }
        }

        decimal quoteRetailTotal = 0;

        if (dsQuote.pvttquote_header[0]["retail_order_total"] != DBNull.Value)
            quoteRetailTotal = Convert.ToDecimal(dsQuote.pvttquote_header[0]["retail_order_total"]);

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list") &&
            quoteRetailTotal > 0)
        {
            AddInfoModalDiv.Visible = true;
            this.lnkAddInfo.Visible = true;
            this.lblCustInfo.Visible = true;
        }
        else
        {
            AddInfoModalDiv.Visible = false;
            this.lnkAddInfo.Visible = false;
            this.lblCustInfo.Visible = false;
        }

        //TODO: Validation of fields; check for ship-to of current transaction
        int shiptoSequence = (int)dsQuote.pvttquote_header[0]["shipto_seq_num"];

        DataRow[] shiprows = ((dsCustomerDataSet.ttShiptoDataTable)dm.GetCache("AllCurrentCustomerShiptos")).Select("seq_num = " + shiptoSequence.ToString());

        if ((bool)shiprows[0]["cust_po_required"])
        {
            lblPOError.Visible = true;
        }
        else
            lblPOError.Visible = false;

        if ((bool)shiprows[0]["job_number_required"])
        {
            lblJobError.Visible = true;
        }
        else
            lblJobError.Visible = false;

        if ((bool)shiprows[0]["ordered_by_required"])
        {
            lblOrderedByError.Visible = true;
        }
        else
            lblOrderedByError.Visible = false;

        if ((bool)shiprows[0]["reference_required"])
        {
            lblReferenceError.Visible = true;
        }
        else
            lblReferenceError.Visible = false;

        if ((bool)shiprows[0]["ship_via_required"])
        {
            lblShipViaError.Visible = true;
        }
        else
            lblShipViaError.Visible = false;

        if (Session["QuoteReleaseDetailGridIsInEditMode"] == null || (bool)Session["QuoteReleaseDetailGridIsInEditMode"] == false)
        {
            wdgQuoteDetails.ClearDataSource();
            wdgQuoteDetails.DataSource = null;
            wdgQuoteDetails.DataBind();

            wdgQuoteDetails.DataSource = dsQuote;
            wdgQuoteDetails.DataMember = "pvttquote_detail";
            wdgQuoteDetails.DataBind();
            Session["CachedQuoteReleaseDataSet"] = dsQuote;
        }

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormatSingle();ComplementoryPopupHeight();", true);
    }
    protected decimal GetDefaultTaxRate()
    {
        return Convert.ToDecimal(Profile.DefaultTaxRate == "" ? "0" : Profile.DefaultTaxRate);
    }
    protected void AddInfoQuoteSelector_OnOKClicked()
    {
        Session["UIHasChangedAddInfo"] = true;
    }

    void AddInfoQuoteSelector_OnCancelClicked()
    {
        Session["UIHasCanceledAddInfo"] = true;
    }

    public void Initialize(dsQuoteDataSet dsQuote)
    {
        ddlShipVia.Enabled = true;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        OrderCart orderCart = null;
        if (dm.GetCache("OrderCart") != null)
            orderCart = (OrderCart)dm.GetCache("OrderCart");
        DataSet cartDS = orderCart.ReferencedDataSet;

        ddlShipVia.Items.Clear();
        foreach (DataRowView row in cartDS.Tables["ttship_via"].DefaultView)
        {
            ListItem li = new ListItem();
            li.Text = ((string)row["ship_via"]).ToUpper();
            li.Value = ((string)row["ship_via"]).ToUpper();
            ddlShipVia.Items.Add(li);
        }

        ListItem lisearch = new ListItem();
        lisearch.Text = dsQuote.pvttquote_header.Rows[0]["ship_via"].ToString().ToUpper();
        lisearch.Value = dsQuote.pvttquote_header.Rows[0]["ship_via"].ToString().ToUpper();
        if (ddlShipVia.Items.Contains(lisearch))
        {
            for (int x = 0; x < ddlShipVia.Items.Count; x++)
            {
                if (ddlShipVia.Items[x].Value == dsQuote.pvttquote_header.Rows[0]["ship_via"].ToString().ToUpper())
                {
                    ddlShipVia.SelectedIndex = x;
                    break;
                }
            }
        }
        else
        {
            if (dsQuote.pvttquote_header.Rows[0]["ship_via"].ToString().Length == 0)
                ddlShipVia.SelectedIndex = 0;
            else
            {
                ddlShipVia.Items.Add(lisearch);
                ddlShipVia.SelectedIndex = ddlShipVia.Items.Count - 1;
                ddlShipVia.Enabled = false;
            }
        }

        Session["QuoteRelease_dsQuote"] = null;

        QuoteDetailGridDiv.Visible = true;

        //IG Grid code...
        if (dsQuote != null)
        {
            dsQuote.pvttquote_detail.PrimaryKey = null;
            DataColumn[] keys = new DataColumn[1];
            keys[0] = dsQuote.pvttquote_detail.sequenceColumn;
            dsQuote.pvttquote_detail.PrimaryKey = keys;
        }

        txtQuoteID.Text = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
        txtOrderType.Text = dsQuote.pvttquote_header.Rows[0]["sale_type_desc"].ToString();
        txtJob.Text = dsQuote.pvttquote_header.Rows[0]["job"].ToString();
        txtReference.Text = dsQuote.pvttquote_header.Rows[0]["reference"].ToString();
        txtCustomerPO.Text = dsQuote.pvttquote_header.Rows[0]["cust_po"].ToString();
        txtMessage.Text = dsQuote.pvttquote_header.Rows[0]["quote_message"].ToString();

        //handle Ship Complete; setting the check box from current header value...
        bool shipcomplete = Convert.ToBoolean(dsQuote.pvttquote_header.Rows[0]["ship_complete"].ToString());
        if (shipcomplete)
            cbxShipComplete.Checked = true;
        else
            cbxShipComplete.Checked = false;


        string tempTxtRequestedDate;

        //if date is overridden display the date, otherwise blank the date.
        if (Convert.ToBoolean(dsQuote.pvttquote_header.Rows[0]["expect_date_override"].ToString().ToLower()) == true)
        {
            tempTxtRequestedDate = dsQuote.pvttquote_header.Rows[0]["expect_date"].ToString().Substring(0, dsQuote.pvttquote_header.Rows[0]["expect_date"].ToString().IndexOf(" "));
        }
        else
        {
            tempTxtRequestedDate = "";
        }

        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            DataRow shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
            string inputCustKey = shiptoRow["cust_key"].ToString();
            int inputShipto = Convert.ToInt32(shiptoRow["seq_num"].ToString());
            string inputSaleType = dsQuote.pvttquote_header.Rows[0]["sale_type"].ToString();

            if (Session["LastReleaseQuoteFetched"] == null)
                Session["LastReleaseQuoteFetched"] = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
            else if (dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString() != (string)Session["LastReleaseQuoteFetched"])
            {
                Session["quoteReleaseDateStrings"] = null;
                Session["LastReleaseQuoteFetched"] = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
            }

            List<string> dateStrings = new List<string>();

            if (dm.GetCache("use_route_dates") != null && (string)dm.GetCache("use_route_dates") == "Select date from available routes")
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.Customers custs = new Customers(out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                dateStrings = custs.FetchDeliveryList(inputCustKey, inputShipto, inputSaleType, true, out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }
            }

            if (dateStrings.Count > 0)
            {
                int foundIndex = 0;

                if (tempTxtRequestedDate.Length > 0)
                {
                    for (int x = 0; x < dateStrings.Count; x++)
                    {
                        if (tempTxtRequestedDate == dateStrings[x])
                        {
                            foundIndex = x;
                            break;
                        }
                    }

                    if (foundIndex == 0)
                    {
                        //On release (not Quote Edit) we don't want to add a date that is not in the route date list...
                        //keep this code, because this is where future code would be added to allow a random date, in addition to route dates.
                        //dateStrings.Insert(0, tempTxtRequestedDate);
                    }
                }

                ddlRequestedDate.Visible = true;
                txtRequestedDate.Visible = false;
                calRequestedDate.Enabled = false;
                if (Session["quoteReleaseDateStrings"] == null)
                {
                    ddlRequestedDate.DataSource = dateStrings;
                    ddlRequestedDate.DataBind();
                    Session["quoteReleaseDateStrings"] = dateStrings;
                }

                if (foundIndex > 0)
                {
                    ddlRequestedDate.SelectedIndex = foundIndex;
                    lblRequestdDateAsterisk.Visible = true;
                    lblRequestedDateMessage.Visible = true;
                }
                else
                {
                    ddlRequestedDate.SelectedIndex = 0;
                    lblRequestdDateAsterisk.Visible = false;
                    lblRequestedDateMessage.Visible = false;
                }
            }
            else
            {
                ddlRequestedDate.Visible = false;
                txtRequestedDate.Visible = true;

                lblRequestdDateAsterisk.Visible = false;
                lblRequestedDateMessage.Visible = false;

                if (tempTxtRequestedDate.Length > 0)
                {
                    txtRequestedDate.Text = tempTxtRequestedDate;
                    lblRequestdDateAsterisk.Visible = true;
                    lblRequestedDateMessage.Visible = true;
                }
                calRequestedDate.Enabled = true;

                ddlRequestedDate.DataSource = null;
                ddlRequestedDate.DataBind();
                if (Session["quoteReleaseDateStrings"] != null)
                    Session["quoteReleaseDateStrings"] = null;
            }
        }

        if (dsQuote.pvttquote_header.Rows[0]["quoted_for"].ToString().Length == 0)
        {
            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

            if (user.GetContextValue("currentUserName").Length > 12)
                txtOrderedBy.Text = user.GetContextValue("currentUserName").Substring(0, 12);
            else
                txtOrderedBy.Text = user.GetContextValue("currentUserName");
        }
        else
        {
            if (dsQuote.pvttquote_header.Rows[0]["quoted_for"].ToString().Length > 12)
                this.txtOrderedBy.Text = dsQuote.pvttquote_header.Rows[0]["quoted_for"].ToString().Substring(0, 12);
            else
                this.txtOrderedBy.Text = dsQuote.pvttquote_header.Rows[0]["quoted_for"].ToString();
        }

        dsQuote.AcceptChanges();

        //Reset all Columns to Hidden = false;
        UnHideAllColumns();
        wdgQuoteDetails.Columns["qty_to_release"].Hidden = true;

        //determine security of partial release...
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        bool allocation = true;
        if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
        {
            DataRow[] rows = dsPV.bPV_Action.Select("token_code='release_quotes_partial'");
            if (rows.Length == 0) //no allocation
            {
                allocation = false;
                wdgQuoteDetails.Columns["readonly_qty_to_release"].Hidden = false;
            }
            else
            {
                allocation = true;
                wdgQuoteDetails.Columns["readonly_qty_to_release"].Hidden = true;
            }
        }

        bool partiallyReleased = false;
        foreach (DataRow row in dsQuote.pvttquote_detail.Rows)
        {
            if (Convert.ToDecimal(row["qty_released"].ToString()) > 0)
            {
                partiallyReleased = true;
                break;
            }
        }

        if (allocation)
        {
            if (dsQuote.pvttquote_header.Rows[0]["orig_source"].ToString().ToLower() != "pv")
            {
                lblGridMessage.Text = "<b>Message:</b> This quote is available for full release only.";
                Session["partial_release_only"] = true;

                if (!partiallyReleased)
                {
                    FullRelease_ItemsNotPreviouslyReleased();
                }
                else
                {
                    FullRelease_ItemsPreviouslyReleased();
                }
            }
            else
            {
                lblGridMessage.Text = "";
                if (dsQuote.pvttquote_header.Rows[0]["allow_release"].ToString().ToLower() == "f")
                {
                    lblGridMessage.Text = "<b>Message:</b> This quote is available for full release only.";
                    Session["partial_release_only"] = true;

                    if (!partiallyReleased)
                    {
                        FullRelease_ItemsNotPreviouslyReleased();
                    }
                    else
                    {
                        FullRelease_ItemsPreviouslyReleased();
                    }
                }
                else
                {
                    Session["partial_release_only"] = false;

                    if (!partiallyReleased)
                    {
                        PartialRelease_ItemsNotPreviouslyReleased();
                    }
                    else
                    {
                        PartialRelease_ItemsPreviouslyReleased();
                    }
                }
            }
        }
        else
        {
            if (!partiallyReleased)
            {
                FullRelease_ItemsNotPreviouslyReleased();
            }
            else
            {
                FullRelease_ItemsPreviouslyReleased();
            }
        }

        //always do this last...
        Session["QuoteRelease_dsQuote"] = dsQuote;
    }

    private void FullRelease_ItemsNotPreviouslyReleased()
    {
        wdgQuoteDetails.Columns["readonly_qty_to_release"].Hidden = false;
        wdgQuoteDetails.Columns["qty_to_release"].Hidden = true;

    }

    private void PartialRelease_ItemsNotPreviouslyReleased()
    {
        wdgQuoteDetails.Columns["readonly_qty_to_release"].Hidden = false;
        wdgQuoteDetails.Columns["qty_to_release"].Hidden = true;

        btnRelease.Visible = true;
    }

    private void FullRelease_ItemsPreviouslyReleased()
    {
        wdgQuoteDetails.Columns["readonly_qty_to_release"].Hidden = false;
        wdgQuoteDetails.Columns["qty_to_release"].Hidden = true;

    }

    private void PartialRelease_ItemsPreviouslyReleased()
    {
        wdgQuoteDetails.Columns["readonly_qty_to_release"].Hidden = false;
        wdgQuoteDetails.Columns["qty_to_release"].Hidden = true;

        btnRelease.Visible = true;
    }

    private void UnHideAllColumns()
    {
        foreach (GridField f in wdgQuoteDetails.Columns)
        {
            f.Hidden = false;
        }

        //also hide Release button...
        btnRelease.Visible = false;
        btnRelease.Enabled = true;
    }

    public void Reset()
    {
        txtQuoteID.Text = "";

        txtOrderType.Text = "";

        Session["QuoteRelease_dsQuote"] = null;
        btnCancel_Click(null, null);
    }

    public void Reset(string source)
    {
        txtQuoteID.Text = "";

        txtOrderType.Text = "";

        Session["QuoteRelease_dsQuote"] = null;

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            lblPriceDisclaimer.Visible = false;
        }
        else
            lblPriceDisclaimer.Visible = true;

        SubmitTop.Visible = true;
        SubmitButtons.Visible = true;
        SubmitButtons2.Visible = true;
        QuoteDetailGridDiv.Visible = true;

        lblError.Visible = false;
        lblLine1.Visible = false;
        lblLine2.Visible = false;

        lblRequestedDateError.Visible = false;

        lblCcEmailError.Visible = false;
        lblFaxError.Visible = false;

        lblPOError.Text = "*";
        lblOrderedByError.Text = "*";
        lblEmailError.Text = "*";
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (lblRequestdDateAsterisk.Visible)
        {
            lblRequestdDateAsterisk.Visible = false;
            lblRequestedDateMessage.Visible = false;
        }

        txtRequestedDate.Text = "";

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            lblPriceDisclaimer.Visible = false;
        }
        else
            lblPriceDisclaimer.Visible = true;

        SubmitTop.Visible = true;
        SubmitButtons.Visible = true;
        SubmitButtons2.Visible = true;
        QuoteDetailGridDiv.Visible = true;

        lblError.Visible = false;
        lblLine1.Visible = false;
        lblLine2.Visible = false;

        lblRequestedDateError.Visible = false;

        lblCcEmailError.Visible = false;
        lblFaxError.Visible = false;

        lblPOError.Text = "*";
        lblOrderedByError.Text = "*";
        lblEmailError.Text = "*";
        lblJobError.Text = "*";
        lblReferenceError.Text = "*";
        lblShipViaError.Text = "*";

        UserControl quotes = Parent.FindControl("Quotes1") as UserControl;
        if (quotes != null)
        {
            quotes.Visible = true;
            this.Visible = false;
            Session["QuoteReleaseVisible"] = false;
        }

        Session["QuoteReleaseDetailGridIsInEditMode"] = null;
        Session["ShiptoQuote_txtCustomerName"] = null;

        /* Analytics Tracking */

        Button bt = sender as Button;

        if (bt != null && sender != null && e != null)
        {

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Value = "0";

            if (bt.ID == "btnEditCart2")
            {
                aInput.Action = "Quote Release - Cancel - Top";
            }
            else
            {
                aInput.Action = "Quote Release - Cancel - Bottom";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

    }

    protected void btnSubmitOrder_Click(object sender, EventArgs e)
    {
        bool ok = true;
        int invalidentry = 0;
        if (Page.Request.Url.ToString().Contains("bc.com"))
        {
            if (txtCustomerPO.Text.Trim().Length == 0)
            {
                ok = false;
                lblPOError.Visible = true;
                lblPOError.Text = "*&nbsp;Required";
            }
            else
            {
                lblPOError.Visible = false;
                lblPOError.Text = "*";
            }
        }

        //TODO: Validation of fields; check for ship-to of current transaction
        DataManager dm = new DataManager();
        dsQuoteDataSet dsQuote = (dsQuoteDataSet)Session["QuoteRelease_dsQuote"];
        dsQuoteDataSet dsAgilityQuote = FetchAgilityQuoteDS(dsQuote);
        int shiptoSequence = (int)dsQuote.pvttquote_header[0]["shipto_seq_num"];
        bool fetchChangedShiptoRequiredFields = false;

        DataRow row = null;

        //RTT 116025 If information has been updated in Agility, force user to refresh before releasing quote to prevent overwriting with stale data.
        if (StalePVDataset(dsAgilityQuote, dsQuote))
        {
            Session["ShiptoQuote_txtCustomerName"] = null; //this will trigger Update Address link button popup to reload info.
            Session["QuoteReleaseDetailGridIsInEditMode"] = null;
            Session["CachedQuoteReleaseDataSet"] = null;

            DisplayOrderMessage(STALEPVQUOTETITLE);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Popup", "ShowPopup('ctl00_cphMaster1_MainTabControl1_QuoteRelease_OrderConfirmation','ctl00_cphMaster1_MainTabControl1_QuoteRelease_pnlinnerorder');", true);

            return;
        }


        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            row = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

            if ((int)row["seq_num"] != shiptoSequence)
                fetchChangedShiptoRequiredFields = true;
        }

        if (fetchChangedShiptoRequiredFields)
        {
            //TODO: go find ship-to tt of all customer ship-tos to access required fields data
            DataRow[] rows = ((dsCustomerDataSet.ttShiptoDataTable)dm.GetCache("AllCurrentCustomerShiptos")).Select("seq_num = " + shiptoSequence.ToString());
            if (rows.Length == 1)
            {
                row = rows[0];
            }
        }

        if ((bool)row["ordered_by_required"] && txtOrderedBy.Text.Length == 0)
        {
            ok = false;
            lblOrderedByError.Text = "*&nbsp;Required";
        }

        if ((bool)row["ordered_by_required"] && txtOrderedBy.Text.Length > 0)
        {
            lblOrderedByError.Text = "*";
        }

        if ((bool)row["cust_po_required"] && txtCustomerPO.Text.Length == 0)
        {
            ok = false;
            lblPOError.Text = "*&nbsp;Required";
        }

        if ((bool)row["cust_po_required"] && txtCustomerPO.Text.Length > 0)
        {
            lblPOError.Text = "*";
        }

        if ((bool)row["job_number_required"] && txtJob.Text.Length == 0)
        {
            ok = false;
            lblJobError.Text = "*&nbsp;Required";
        }

        if ((bool)row["job_number_required"] && txtJob.Text.Length > 0)
        {
            lblJobError.Text = "*";
        }

        if ((bool)row["reference_required"] && txtReference.Text.Length == 0)
        {
            ok = false;
            lblReferenceError.Text = "*&nbsp;Required";
        }

        if ((bool)row["reference_required"] && txtReference.Text.Length > 0)
        {
            lblReferenceError.Text = "*";
        }

        if ((bool)row["ship_via_required"] && ddlShipVia.Text.Length == 0)
        {
            ok = false;
            lblShipViaError.Text = "*&nbsp;Required";
        }

        if ((bool)row["ship_via_required"] && ddlShipVia.Text.Length > 0)
        {
            lblShipViaError.Text = "*";
        }

        lblCcEmailError.Visible = false;
        if (txtCcEmail.Text.Length > 0)
        {
            RegexStringValidator val = new RegexStringValidator(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            try
            {
                val.Validate(txtCcEmail.Text);
            }
            catch
            {
                ok = false;
                lblCcEmailError.Visible = true;
                lblCcEmailError.Text = "Invalid";
            }
        }

        if (txtEmail.Text.Length == 0)
        {
            ok = false;
            lblEmailError.Visible = true;
            lblEmailError.Text = "*&nbsp;Required";
        }
        else
        {
            lblEmailError.Text = "*";
            RegexStringValidator val = new RegexStringValidator(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            try
            {
                val.Validate(txtEmail.Text);
            }
            catch
            {
                ok = false;
                lblEmailError.Visible = true;
                lblEmailError.Text = "*&nbsp;Invalid";
            }
        }

        if (ddlRequestedDate.Visible)
        {
            if (ddlRequestedDate.SelectedValue.Length > 0 && ddlRequestedDate.SelectedValue != "Next available")
            {
                try
                {
                    DateTime requestedDate = DateTime.Parse(ddlRequestedDate.SelectedValue);

                    if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                    {
                        ok = false;
                        lblRequestedDateError.Visible = true;
                    }
                    else
                        lblRequestedDateError.Visible = false;

                }
                catch
                {
                    ok = false;
                    lblRequestedDateError.Visible = true;
                }
            }
        }
        else
        {
            if (txtRequestedDate.Text.Trim() != "")
            {
                try
                {
                    DateTime requestedDate = DateTime.Parse(txtRequestedDate.Text);

                    if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                    {
                        ok = false;
                        lblRequestedDateError.Visible = true;
                    }
                    else
                        lblRequestedDateError.Visible = false;
                }
                catch
                {
                    ok = false;
                    lblRequestedDateError.Visible = true;
                }
            }
        }

        if (txtFax.Text == "(___) ___-____")
        {
            lblFaxError.Visible = false;
        }
        else if (txtFax.Text.Contains("_"))
        {
            ok = false;
            lblFaxError.Visible = true;
        }

        lblGridMessage.Text = ""; //reset the message line...

        foreach (GridRecord record in wdgQuoteDetails.Rows)
        {
            int sequence = Convert.ToInt32(((DataRowView)(record.DataItem)).Row["sequence"]);
            decimal d = 0;
            bool parsed = false;

            GridRecordItem it = record.Items.FindItemByKey("qty_to_release");
            if (it != null)
            {
                TextBox tbx = it.FindControl("txtQtyToRelease") as TextBox;
                if (tbx != null)
                {
                    parsed = Decimal.TryParse(tbx.Text, out d);
                    if (parsed)
                    {
                        DataRow[] rows = dsQuote.pvttquote_detail.Select("sequence = " + sequence.ToString());
                        if (rows != null && rows.Length == 1)
                        {
                            rows[0]["qty_to_release"] = d;
                        }
                    }
                    else
                    {
                        ok = false;
                        lblGridMessage.Text += "<b>Error:</b> Invalid value in Sequence " + sequence.ToString();
                        invalidentry = 1;
                    }
                }
            }
        }

        if (ok)
        {
            _quote = new Dmsi.Agility.Data.Quotes();

            if (txtRequestedDate.Visible)
            {
                if (txtRequestedDate.Text.Length > 0)
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DateTime.Parse(txtRequestedDate.Text);
                    dsQuote.pvttquote_header[0]["expect_date_override"] = true;
                }
                else
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DBNull.Value;
                    dsQuote.pvttquote_header[0]["expect_date_override"] = false;
                }
            }
            else
            {
                if (ddlRequestedDate.SelectedValue.Length > 0 && ddlRequestedDate.SelectedValue != "Next available")
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DateTime.Parse(ddlRequestedDate.SelectedValue);
                    dsQuote.pvttquote_header[0]["expect_date_override"] = true;
                }
                else
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DBNull.Value;
                    dsQuote.pvttquote_header[0]["expect_date_override"] = false;
                }
            }

            dsQuote.pvttquote_header[0]["quote_message"] = txtMessage.Text;
            dsQuote.pvttquote_header.Rows[0]["ship_via"] = ddlShipVia.Text;
            dsQuote.pvttquote_header.Rows[0]["ship_complete"] = cbxShipComplete.Checked;

            //MDM - Added as Ship-to overrides from popup control, but only if action is revoked (inverted action)
            if (Session["ShiptoQuote_txtCustomerName"] != null)
            {
                dsQuote.pvttquote_header.Rows[0]["shiptoname"] = ((string)Session["ShiptoQuote_txtCustomerName"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToAddr1"] = ((string)Session["ShiptoQuote_txtAddress1"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToAddr2"] = ((string)Session["ShiptoQuote_txtAddress2"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToAddr3"] = ((string)Session["ShiptoQuote_txtAddress3"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToCity"] = ((string)Session["ShiptoQuote_txtCity"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToState"] = ((string)Session["ShiptoQuote_txtState"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToZip"] = ((string)Session["ShiptoQuote_txtZip"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToCountry"] = ((string)Session["ShiptoQuote_txtCountry"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToPhone"] = ((string)Session["ShiptoQuote_txtPhone"]).Trim();
            }

            dsQuote.AcceptChanges();

            Session["QuoteRelease_dsQuote"] = dsQuote;

            string MessageText = _quote.ValidateQuoteRelease(txtReference.Text, txtJob.Text, txtOrderedBy.Text, txtCustomerPO.Text, dsQuote);

            if (MessageText.Length == 0)
            {
                bool fullRelease = true;
                foreach (DataRow detailRow in dsQuote.pvttquote_detail.Rows)
                {
                    if (Convert.ToDecimal(detailRow["qty_ordered"]) - (Convert.ToDecimal(detailRow["qty_released"]) + Convert.ToDecimal(detailRow["qty_to_release"])) > 0)
                    {
                        fullRelease = false;
                        break;
                    }
                }

                if (fullRelease)
                {
                    Session["QuoteFullyReleasedId" + dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString()] = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
                }

                //We want the call to timeout in 2 seconds...
                this.Page.AsyncTimeout = new TimeSpan(0, 0, 2);

                // create the asynchronous task instance
                PageAsyncTask asyncTask = new PageAsyncTask(
                  new BeginEventHandler(this.beginAsyncRequest),
                  new EndEventHandler(this.endAsyncRequest),
                  new EndEventHandler(this.asyncRequestTimeout),
                  null, true);

                // register the asynchronous task instance with the page
                this.Page.RegisterAsyncTask(asyncTask);

                this.Page.ExecuteRegisteredAsyncTasks();

                DisplayOrderMessage("Your order has been submitted.");

                //When we finally submit the Quote to an order, don't forget to reset the detail grid edit mode.
                Session["QuoteReleaseDetailGridIsInEditMode"] = null;
                Session["CachedQuoteReleaseDataSet"] = null;

                Session["ShiptoQuote_txtCustomerName"] = null;

                /* Analytics Tracking */

                Button bt = sender as Button;

                if (bt != null && sender != null && e != null)
                {

                    AnalyticsInput aInput = new AnalyticsInput();

                    aInput.Type = "event";
                    aInput.Value = "0";
                   
                    if (bt.ID == "btnSubmitOrder2")
                    {
                        aInput.Action = "Quote Release - Submit Button - Top";
                    }
                    else
                    {
                        aInput.Action = "Quote Release - Submit Button - Bottom";
                    }

                    AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                    am.BeginTrackEvent();

               }

            }
            else
            {
                if (MessageText.Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                //On an error, which is going to take us back one screen, we need to reset the edit mode, so we start fresh on next entry.
                Session["QuoteReleaseDetailGridIsInEditMode"] = null;
                Session["CachedQuoteReleaseDataSet"] = null;
                DisplayErrorMessage(MessageText);
            }
        }
        else
        {
            invalidentry = 1;
        }
        if (invalidentry == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Popup", "ShowPopup('ctl00_cphMaster1_MainTabControl1_QuoteRelease_OrderConfirmation','ctl00_cphMaster1_MainTabControl1_QuoteRelease_pnlinnerorder');", true);
        }
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {

        SubmitTop.Visible = true;
        QuoteDetailGridDiv.Visible = true;

        SubmitButtons.Visible = true;
        SubmitButtons2.Visible = true;

        UserControl quotes = Parent.FindControl("Quotes1") as UserControl;
        if (quotes != null)
        {
            quotes.Visible = true;
            this.Visible = false;
            Session["QuoteReleaseVisible"] = false;

            if (this.OnQuoteReleased != null)
                this.OnQuoteReleased();
        }
    }

    private void DisplayErrorMessage(string message)
    {
        SubmitTop.Visible = false;
        SubmitButtons.Visible = false;
        SubmitButtons2.Visible = false;
        QuoteDetailGridDiv.Visible = false;

        lblError.Visible = true;
        lblError.Text = message + "<br/>Select OK to return to previous screen.";

        lblPriceDisclaimer.Visible = false;

        lblLine1.Visible = false;
        lblLine2.Visible = false;
    }

    private void DisplayOrderMessage(string message)
    {
        btnRelease.Visible = false;

        SubmitTop.Visible = false;
        SubmitButtons2.Visible = false;
        SubmitButtons.Visible = false;
        lblPriceDisclaimer.Visible = false;
        QuoteDetailGridDiv.Visible = false;

        lblLine1.Visible = true;
        lblLine1.Text = message + "<br />";
        lblLine2.Visible = false;
        lblError.Visible = false;

        if (message.Length > 0 && message != "Unable to process the order.")
        {
            lblError.Visible = false;
            lblLine2.Visible = true;

            if (message == STALEPVQUOTETITLE)
            {
                lblLine1.Text = "<b>" + lblLine1.Text + "</b>";
                lblLine2.Text = "This quote changed since you started working with it.<br />" +
                                "Please select it again to work with the most recent version.";

                return;
            }

            if (txtCcEmail.Text.Length > 0)
                lblLine2.Text = "An email confirmation will be sent to " + txtEmail.Text + " and " + txtCcEmail.Text + ".";
            else
                lblLine2.Text = "An email confirmation will be sent to " + txtEmail.Text + ".";

            lblLine2.Text += "<br />If the confirmation is not received, please contact our office.";
            lblLine2.Text += "<br /><br /><b>Please Note:</b> There may be a few minutes delay processing this request.<br />Do not resubmit this quote or you may create duplicate sales orders.";
        }
    }

    private IAsyncResult beginAsyncRequest(object sender, EventArgs e, AsyncCallback callback, object state)
    {
        string formattedFax = "";
        if (txtFax.Text == "(___) ___-____")
        {
            formattedFax = "";
        }
        else if (txtFax.Text.Contains("_"))
        {
            formattedFax = "";
        }
        else
            formattedFax = txtFax.Text;


        dsRetailAddressDataSet dsRetailAddress = new dsRetailAddressDataSet();

        if (Session["dsRetailAddressForQuotes"] != null)
        {
            dsQuoteDataSet dsQuote = (dsQuoteDataSet)Session["QuoteRelease_dsQuote"];
            dsRetailAddress = (dsRetailAddressDataSet)Session["dsRetailAddressForQuotes"];

            string quoteId = dsQuote.pvttquote_header[0]["quote_id"].ToString();

            for (int x = dsRetailAddress.tt_retail_address.Rows.Count - 1; x >= 0; x--)
            {
                if (dsRetailAddress.tt_retail_address.Rows[x]["tran_id"].ToString() != quoteId)
                {
                    dsRetailAddress.tt_retail_address.Rows[x].Delete();
                }
            }

            dsRetailAddress.AcceptChanges();

            DataRow[] rows = dsRetailAddress.tt_retail_address.Select("tran_id=" + quoteId);

            if (rows.Length > 0)
            {
                rows[0]["misc_1"] = (string)Session["AddInfoQuote_txtCustomerId"];
                rows[0]["misc_2"] = (string)Session["AddInfoQuote_txtShipTo"];
                rows[0]["cust_name"] = (string)Session["AddInfoQuote_txtCustomerName"];
                rows[0]["address_1"] = (string)Session["AddInfoQuote_txtAddress1"];
                rows[0]["address_2"] = (string)Session["AddInfoQuote_txtAddress2"];
                rows[0]["address_3"] = (string)Session["AddInfoQuote_txtAddress3"];
                rows[0]["city"] = (string)Session["AddInfoQuote_txtCity"];
                rows[0]["state"] = (string)Session["AddInfoQuote_txtState"];
                rows[0]["zip"] = (string)Session["AddInfoQuote_txtZip"];
                rows[0]["country"] = (string)Session["AddInfoQuote_txtCountry"];
                rows[0]["phone"] = (string)Session["AddInfoQuote_txtPhone"];
                rows[0]["misc_3"] = (string)Session["AddInfoQuote_txtContactName"];
                rows[0]["email"] = (string)Session["AddInfoQuote_txtEmail"];
                rows[0]["misc_4"] = (string)Session["AddInfoQuote_txtReference"];

                if (Session["AddInfoQuote_taxRate"] == null)
                    Session["AddInfoQuote_taxRate"] = GetDefaultTaxRate().ToString();

                rows[0]["misc_5"] = (Convert.ToDecimal(Session["AddInfoQuote_taxRate"])).ToTrimmedString();
                rows[0]["misc_6"] = (string)Session["AddInfoQuote_taxableDescription"];

                if (Session["AddInfoQuote_taxableAmount"] == null)
                    Session["AddInfoQuote_taxableAmount"] = "0";

                rows[0]["misc_7"] = (Convert.ToDecimal(Session["AddInfoQuote_taxableAmount"])).ToTrimmedString();
                rows[0]["misc_8"] = (string)Session["AddInfoQuote_nonTaxableDescription"];

                if (Session["AddInfoQuote_nonTaxableAmount"] == null)
                    Session["AddInfoQuote_nonTaxableAmount"] = "0";

                rows[0]["misc_9"] = (Convert.ToDecimal(Session["AddInfoQuote_nonTaxableAmount"])).ToTrimmedString();
            }
            else
            {
                if (Session["AddInfoQuote_txtCustomerId"] != null &&
                        (
                            ((string)Session["AddInfoQuote_txtCustomerId"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtShipTo"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtCustomerName"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtAddress1"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtAddress2"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtAddress3"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtCity"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtState"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtZip"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtCountry"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtPhone"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtContactName"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtEmail"]).Length > 0 ||
                            ((string)Session["AddInfoQuote_txtReference"]).Length > 0 ||
                            ((decimal)Session["AddInfoQuote_taxRate"]).ToTrimmedString().Length > 0 ||
                            ((string)Session["AddInfoQuote_taxableDescription"]).Length > 0 ||
                            ((decimal)Session["AddInfoQuote_taxableAmount"]).ToTrimmedString().Length > 0 ||
                            ((string)Session["AddInfoQuote_nonTaxableDescription"]).Length > 0 ||
                            ((decimal)Session["AddInfoQuote_nonTaxableAmount"]).ToTrimmedString().Length > 0
                        )
                   )
                {
                    DataRow row = dsRetailAddress.tt_retail_address.NewRow();

                    row["misc_1"] = (string)Session["AddInfoQuote_txtCustomerId"];
                    row["misc_2"] = (string)Session["AddInfoQuote_txtShipTo"];
                    row["cust_name"] = (string)Session["AddInfoQuote_txtCustomerName"];
                    row["address_1"] = (string)Session["AddInfoQuote_txtAddress1"];
                    row["address_2"] = (string)Session["AddInfoQuote_txtAddress2"];
                    row["address_3"] = (string)Session["AddInfoQuote_txtAddress3"];
                    row["city"] = (string)Session["AddInfoQuote_txtCity"];
                    row["state"] = (string)Session["AddInfoQuote_txtState"];
                    row["zip"] = (string)Session["AddInfoQuote_txtZip"];
                    row["country"] = (string)Session["AddInfoQuote_txtCountry"];
                    row["phone"] = (string)Session["AddInfoQuote_txtPhone"];
                    row["misc_3"] = (string)Session["AddInfoQuote_txtContactName"];
                    row["email"] = (string)Session["AddInfoQuote_txtEmail"];
                    row["misc_4"] = (string)Session["AddInfoQuote_txtReference"];
                    row["misc_5"] = ((decimal)Session["AddInfoQuote_taxRate"]).ToTrimmedString();
                    row["misc_6"] = (string)Session["AddInfoQuote_taxableDescription"];
                    row["misc_7"] = ((decimal)Session["AddInfoQuote_taxableAmount"]).ToTrimmedString();
                    row["misc_8"] = (string)Session["AddInfoQuote_nonTaxableDescription"];
                    row["misc_9"] = ((decimal)Session["AddInfoQuote_nonTaxableAmount"]).ToTrimmedString();

                    row["tran_sysid"] = "";
                    row["TYPE"] = "";
                    row["tran_id"] = 0;
                    row["shipment_num"] = 0;
                    row["dispatch_tran_sysid"] = "";
                    row["dispatch_tran_type"] = "";
                    row["dispatch_tran_id"] = 0;
                    row["dispatch_shipment_num"] = 0;

                    dsRetailAddress.tt_retail_address.Rows.Add(row);
                }
            }

            dsRetailAddress.AcceptChanges();
        }

        _quote.BeginReleaseQuote(txtReference.Text, txtJob.Text, txtOrderedBy.Text, txtCustomerPO.Text, txtEmail.Text, txtCcEmail.Text, formattedFax, (dsQuoteDataSet)Session["QuoteRelease_dsQuote"], dsRetailAddress);

        //Weird positioning, but we need to blank the date field here according to our business rules...
        txtRequestedDate.Text = "";

        return new MyEmtpyIAsyncResult2();
    }

    private void endAsyncRequest(IAsyncResult result)
    {
        //Do Nothing...
    }

    void asyncRequestTimeout(IAsyncResult result)
    {
        //Do Nothing...
    }

    protected void btnRelease_Click(object sender, EventArgs e)
    {
        btnRelease.Enabled = false;
        wdgQuoteDetails.Columns["readonly_qty_to_release"].Hidden = true;
        wdgQuoteDetails.Columns["qty_to_release"].Hidden = false;

        Session["QuoteReleaseDetailGridIsInEditMode"] = true;
    }

    protected void txtRequestedDate_TextChanged(object sender, EventArgs e)
    {
        if (txtRequestedDate.Text.Length == 0)
        {
            lblRequestdDateAsterisk.Visible = false;
            lblRequestedDateMessage.Visible = false;

            if (lblRequestedDateError.Visible)
                lblRequestedDateError.Visible = false;
        }
        else
        {
            lblRequestdDateAsterisk.Visible = true;
            lblRequestedDateMessage.Visible = true;
        }

        try
        {
            if (txtRequestedDate.Text.Length > 0)
            {
                DateTime requestedDate = DateTime.Parse(txtRequestedDate.Text);

                if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                {
                    lblRequestedDateError.Visible = true;
                }
                else
                    lblRequestedDateError.Visible = false;
            }
        }
        catch
        {
            lblRequestedDateError.Visible = true;
        }
    }

    protected void ddlRequestedDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRequestedDate.SelectedItem.Text == "Next available")
        {
            lblRequestdDateAsterisk.Visible = false;
            lblRequestedDateMessage.Visible = false;

            if (lblRequestedDateError.Visible)
                lblRequestedDateError.Visible = false;
        }
        else
        {
            lblRequestdDateAsterisk.Visible = true;
            lblRequestedDateMessage.Visible = true;
        }
    }

    protected dsQuoteDataSet FetchAgilityQuoteDS(dsQuoteDataSet dsQuote) {
        return FetchAgilityQuoteDS(int.Parse(dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString()));
    }
    protected dsQuoteDataSet FetchAgilityQuoteDS(int iQuoteNum)
    {
        int ReturnCode = 0;
        string MessageText = "";
        DataManager dm = new DataManager();

        string searchCriteria = (string)dm.GetCache("QuoteSearchCriteria");
        string quoteId = iQuoteNum.ToString();

        if (searchCriteria.Contains("SearchType=Quote"))
        {
            searchCriteria = searchCriteria.Replace("SearchTran=<ALL>", "SearchTran=" + quoteId);
        }

        Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

        siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

        foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
        {
            siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
        }

        currentCust.Dispose();

        Dmsi.Agility.Data.Quotes Quotes = new Dmsi.Agility.Data.Quotes(searchCriteria, siCallDS, null, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        siCallDS.Dispose();

        dsQuoteDataSet quoteList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet)Quotes.ReferencedDataSet;

        for (int x = quoteList.pvttquote_header.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_header.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_header.Rows[x].Delete();
        }

        for (int x = quoteList.pvttquote_detail.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_detail.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_detail.Rows[x].Delete();
        }

        for (int x = quoteList.pvttquote_byitem.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_detail.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_detail.Rows[x].Delete();
        }

        quoteList.AcceptChanges();

        return quoteList;
    }
    protected bool StalePVDataset(dsQuoteDataSet dsAgilityQuote, dsQuoteDataSet dsPVQuote)
    {
        DateTime agilityDateParse;
        DateTime pvDateParse;

        DateTime agilityDateTime;
        DateTime pvDateTime;

        string dateFormat = "MM/dd/yyyy ";

        agilityDateParse = DateTime.MinValue;
        pvDateParse = DateTime.MaxValue;

        agilityDateTime = DateTime.MinValue;
        pvDateTime = DateTime.MaxValue;

        if (dsAgilityQuote.pvttquote_header.Rows.Count == 0) { return true; }

        DateTime.TryParse(dsAgilityQuote.pvttquote_header[0]["update_date"].ToString(), out agilityDateParse);
        DateTime.TryParse(dsPVQuote.pvttquote_header[0]["update_date"].ToString(), out pvDateParse);

        DateTime.TryParse(agilityDateParse.ToString(dateFormat) + dsAgilityQuote.pvttquote_header[0]["update_time"].ToString(), out agilityDateTime);
        DateTime.TryParse(pvDateParse.ToString(dateFormat) + dsPVQuote.pvttquote_header[0]["update_time"].ToString(), out pvDateTime);

        return agilityDateTime > pvDateTime;       
    }
}

public class MyEmtpyIAsyncResult2 : IAsyncResult
{
    public MyEmtpyIAsyncResult2() //Default ctor...
    {
    }

    public object AsyncState
    {
        get { return new object(); }
    }

    public System.Threading.WaitHandle AsyncWaitHandle
    {
        get { return null; }
    }

    public bool CompletedSynchronously
    {
        get { return true; }
    }

    public bool IsCompleted
    {
        get { return true; }
    }
}