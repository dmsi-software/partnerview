﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.Security;

using System.Data;
public partial class UserControls_ChangeTheme : System.Web.UI.UserControl
{
    protected DataManager _dataManager;
    protected void Page_Load(object sender, EventArgs e)
    {
        _dataManager = new DataManager();

        if (!this.IsPostBack)
        {
            dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");

            DataRow[] rows = dsSessionManager.ttSelectedProperty.Select("propertyName='pvthemeActiveTabTextColor' OR  " +
                                                                        "propertyName='pvthemeInactiveButtonTextColor' OR " +
                                                                        "propertyName='pvthemeGridHeaderTextColor' OR " +
                                                                        "propertyName='pvthemeActionButtonTextColor' OR " +
                                                                        "propertyName='pvthemeActionButtonColor' OR " +
                                                                        "propertyName='pvthemeActiveLinkTextColor' OR " +
                                                                        "propertyName='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                                        "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                        "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                        "propertyName='pvthemeGridHeaderColor' OR " +
                                                                        "propertyName='pvthemeHeaderBarBackGroundColor' OR " +
                                                                        "propertyName='pvthemeInactiveButtonColor' ");
            if (rows.Length > 0)
            {

                for (int i = 0; i < rows.Length; i++)
                {
                    switch (rows[i]["propertyName"].ToString())
                    {
                        case "pvthemeActionButtonColor":

                            txtbtnStyle.Value = rows[i]["propertyValue"].ToString();

                            highlightcolorch.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());
                            break;
                        case "pvthemeActiveLinkTextColor":

                            txtActiveLink.Value = rows[i]["propertyValue"].ToString();
                            actlink.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());
                            break;
                        case "pvthemeActiveTabColorandRelatedUnderlineBar":

                            txtActivetab.Value = rows[i]["propertyValue"].ToString();
                            dvacttab.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());

                            break;
                        case "pvthemeCustomerUserInformationTextColor":

                            txtuserinformation.Value = rows[i]["propertyValue"].ToString();
                            dvuserinformation.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());
                            break;
                        case "pvthemeGridHeaderColor":

                            txtGrids.Value = rows[i]["propertyValue"].ToString();
                            dvgridbg.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());
                            break;
                        case "pvthemeHeaderBarBackGroundColor":

                            txtHeaderBar.Value = rows[i]["propertyValue"].ToString();
                            headbrbg.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());
                            break;
                        case "pvthemeInactiveButtonColor":

                            txtInactivebutton.Value = rows[i]["propertyValue"].ToString();
                            highlightcolorch2.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());
                            break;
                        case "pvthemeInactiveButtonTextColor":


                            txtinactivebuttext.Value = rows[i]["propertyValue"].ToString();
                            dvInActbuttext.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());

                            break;
                        case "pvthemeActionButtonTextColor":


                            txtactivebuttext.Value = rows[i]["propertyValue"].ToString();
                            dvprimarytext.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());

                            break;
                        case "pvthemeGridHeaderTextColor":


                            txtGridtextcolor.Value = rows[i]["propertyValue"].ToString();
                            dvGridtext.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());

                            break;
                        case "pvthemeActiveTabTextColor":

                            txtactivetabtext.Value = rows[i]["propertyValue"].ToString();
                            dvActtabtext.Style.Add("background-color", "#" + rows[i]["propertyValue"].ToString());
                            break;



                    }
                }
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun", "Themepicker();", true);
    }

    protected void btndefaulttheme_Click(object sender, EventArgs e)
    {
        
        dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");

        DataTable dtProperties = new DataTable();
        dtProperties.Columns.Add("propertyname");
        dtProperties.Columns.Add("Color");
        
        DataRow[] rows = dsSessionManager.ttSelectedProperty.Select("propertyName='currentPvUseCustomTheme' OR " +
                                                                    "propertyName='pvthemeActiveTabTextColor' OR " +
                                                                    "propertyName='pvthemeInactiveButtonTextColor' OR " +
                                                                    "propertyName='pvthemeGridHeaderTextColor' OR " +
                                                                    "propertyName='pvthemeActionButtonTextColor' OR " +
                                                                    "propertyName='pvthemeActionButtonColor' OR " +
                                                                    "propertyName='pvthemeActiveLinkTextColor' OR " +
                                                                    "propertyName='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                    "propertyName='pvthemeGridHeaderColor' OR " +
                                                                    "propertyName='pvthemeHeaderBarBackGroundColor' OR " +
                                                                    "propertyName='pvthemeInactiveButtonColor' ");


        if (rows.Length > 0)
        {

            for (int i = 0; i < rows.Length; i++)
            {
                switch (rows[i]["propertyName"].ToString())
                {
                    case "pvthemeActionButtonColor":

                        rows[i]["propertyValue"] = "42748d";

                        break;
                    case "pvthemeActiveLinkTextColor":

                        rows[i]["propertyValue"] = "42748d";

                        break;
                    case "pvthemeActiveTabColorandRelatedUnderlineBar":

                        rows[i]["propertyValue"] = "42748d";

                        break;
                    case "pvthemeCustomerUserInformationTextColor":

                        rows[i]["propertyValue"] = "ffffff";

                        break;
                    case "pvthemeGridHeaderColor":

                        rows[i]["propertyValue"] = "42748d";

                        break;
                    case "pvthemeHeaderBarBackGroundColor":

                        rows[i]["propertyValue"] = "42748d";

                        break;
                    case "pvthemeInactiveButtonColor":

                        rows[i]["propertyValue"] = "dbd9d6";

                        break;
                    case "pvthemeInactiveButtonTextColor":


                        rows[i]["propertyValue"] = "565759";


                        break;
                    case "pvthemeActionButtonTextColor":


                        rows[i]["propertyValue"] = "ffffff";


                        break;
                    case "pvthemeGridHeaderTextColor":


                        rows[i]["propertyValue"] = "ffffff";


                        break;
                    case "pvthemeActiveTabTextColor":

                        rows[i]["propertyValue"] = "ffffff";

                        break;
                    case "currentPvUseCustomTheme":
                        {
                            rows[i]["propertyValue"] = "NO";
                        }
                        break;
                }
            }

            Session["IsCustomTheme"] = "no";

            dsSessionManager.AcceptChanges();
            _dataManager.SetCache("dsSessionManager", dsSessionManager);

            Dmsi.Agility.EntryNET.SecurityManager sm = new Dmsi.Agility.EntryNET.SecurityManager();

            int ReturnCode;
            string MessageText;

            sm.SaveCustomConfigurations(dsSessionManager, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            } 

            if (rows.Length > 0)
            {
                for (int i = 0; i < rows.Length; i++)
                {
                    DataRow drnew;
                    drnew = dtProperties.NewRow();
                    drnew["propertyname"] = rows[i]["propertyName"].ToString();
                    drnew["Color"] = rows[i]["propertyvalue"].ToString();
                    dtProperties.Rows.Add(drnew);
                    dtProperties.AcceptChanges();
                }
            }

            if (dtProperties.Rows.Count > 0)
            {
                Session["ChangeTheme"] = dtProperties;

            }

            HtmlLink hlk = (HtmlLink)this.Page.Master.FindControl("NewDemo");
            hlk.Href = "../Documents/DynamicThemes/DefaultTheme.css?v1.5";

            if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
            {
                System.IO.File.Delete(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css");
            }

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Change Theme - Default Theme Button";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
            
            Session["IsThemeChanged"] = "yes";
            Response.Redirect("~/Default.aspx");
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");

        DataTable dtProperties = new DataTable();
        dtProperties.Columns.Add("propertyname");
        dtProperties.Columns.Add("Color");

        DataRow[] rows = dsSessionManager.ttSelectedProperty.Select("propertyName='currentPvUseCustomTheme' OR " +
                                                                    "propertyName='pvthemeActiveTabTextColor' OR  " +
                                                                    "propertyName='pvthemeInactiveButtonTextColor' OR " +
                                                                    "propertyName='pvthemeGridHeaderTextColor' OR " +
                                                                    "propertyName='pvthemeActionButtonTextColor' OR " +
                                                                    "propertyName='pvthemeActionButtonColor' OR " +
                                                                    "propertyName='pvthemeActiveLinkTextColor' OR " +
                                                                    "propertyName='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                    "propertyName='pvthemeGridHeaderColor' OR " +
                                                                    "propertyName='pvthemeHeaderBarBackGroundColor' OR " +
                                                                    "propertyName='pvthemeInactiveButtonColor' ");

        if (rows.Length > 0)
        {
            for (int i = 0; i < rows.Length; i++)
            {
                switch (rows[i]["propertyName"].ToString())
                {
                    case "pvthemeActionButtonColor":
                        if (!string.IsNullOrEmpty(txtbtnStyle.Value))
                        {
                            if (txtbtnStyle.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtbtnStyle.Value.ToString().Substring(1, txtbtnStyle.Value.Length - 1);
                            }
                            else
                            {

                                rows[i]["propertyValue"] = txtbtnStyle.Value;
                            }
                        }
                        break;

                    case "pvthemeActiveLinkTextColor":
                        if (!string.IsNullOrEmpty(txtActiveLink.Value))
                        {


                            if (txtActiveLink.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtActiveLink.Value.ToString().Substring(1, txtActiveLink.Value.Length - 1);
                            }
                            else
                            {

                                rows[i]["propertyValue"] = txtActiveLink.Value;
                            }
                        }
                        break;

                    case "pvthemeActiveTabColorandRelatedUnderlineBar":
                        if (!string.IsNullOrEmpty(txtActivetab.Value))
                        {



                            if (txtActivetab.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtActivetab.Value.ToString().Substring(1, txtActivetab.Value.Length - 1);
                            }
                            else
                            {

                                rows[i]["propertyValue"] = txtActivetab.Value;
                            }
                        }
                        break;

                    case "pvthemeCustomerUserInformationTextColor":
                        if (!string.IsNullOrEmpty(txtuserinformation.Value))
                        {

                            if (txtuserinformation.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtuserinformation.Value.ToString().Substring(1, txtuserinformation.Value.Length - 1);
                            }
                            else
                            {

                                rows[i]["propertyValue"] = txtuserinformation.Value;
                            }
                        }
                        break;

                    case "pvthemeGridHeaderColor":
                        if (!string.IsNullOrEmpty(txtGrids.Value))
                        {

                            if (txtGrids.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtGrids.Value.ToString().Substring(1, txtGrids.Value.Length - 1);
                            }
                            else
                            {

                                rows[i]["propertyValue"] = txtGrids.Value;
                            }
                        }
                        break;

                    case "pvthemeHeaderBarBackGroundColor":
                        if (!string.IsNullOrEmpty(txtHeaderBar.Value))
                        {


                            if (txtHeaderBar.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtHeaderBar.Value.ToString().Substring(1, txtHeaderBar.Value.Length - 1);
                            }
                            else
                            {

                                rows[i]["propertyValue"] = txtHeaderBar.Value;
                            }
                        }
                        break;

                    case "pvthemeInactiveButtonColor":
                        if (!string.IsNullOrEmpty(txtInactivebutton.Value))
                        {


                            if (txtInactivebutton.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtInactivebutton.Value.ToString().Substring(1, txtInactivebutton.Value.Length - 1);
                            }
                            else
                            {

                                rows[i]["propertyValue"] = txtInactivebutton.Value;
                            }
                        }
                        break;

                    case "pvthemeInactiveButtonTextColor":

                        if (!string.IsNullOrEmpty(txtinactivebuttext.Value))
                        {
                            if (txtinactivebuttext.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtinactivebuttext.Value.ToString().Substring(1, txtinactivebuttext.Value.Length - 1);
                            }
                            else
                            {
                                rows[i]["propertyValue"] = txtinactivebuttext.Value;
                            }
                        }
                        break;

                    case "pvthemeActionButtonTextColor":

                        if (!string.IsNullOrEmpty(txtactivebuttext.Value))
                        {
                            if (txtactivebuttext.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtactivebuttext.Value.ToString().Substring(1, txtactivebuttext.Value.Length - 1);
                            }
                            else
                            {
                                rows[i]["propertyValue"] = txtactivebuttext.Value;
                            }
                        }
                        break;

                    case "pvthemeGridHeaderTextColor":

                        if (!string.IsNullOrEmpty(txtGridtextcolor.Value))
                        {
                            if (txtGridtextcolor.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtGridtextcolor.Value.ToString().Substring(1, txtGridtextcolor.Value.Length - 1);
                            }
                            else
                            {
                                rows[i]["propertyValue"] = txtGridtextcolor.Value;
                            }
                        }
                        break;

                    case "pvthemeActiveTabTextColor":
                        if (!string.IsNullOrEmpty(txtactivetabtext.Value))
                        {
                            if (txtactivetabtext.Value.StartsWith("#"))
                            {
                                rows[i]["propertyValue"] = txtactivetabtext.Value.ToString().Substring(1, txtactivetabtext.Value.Length - 1);
                            }
                            else
                            {
                                rows[i]["propertyValue"] = txtactivetabtext.Value;
                            }
                        }
                        break;

                    case "currentPvUseCustomTheme":
                        {
                            rows[i]["propertyValue"] = "YES";
                        }
                        break;
                }
            }

            Session["IsCustomTheme"] = "yes";

            dsSessionManager.AcceptChanges();
            _dataManager.SetCache("dsSessionManager", dsSessionManager);

            Dmsi.Agility.EntryNET.SecurityManager sm = new Dmsi.Agility.EntryNET.SecurityManager();

            int ReturnCode;
            string MessageText;

            sm.SaveCustomConfigurations(dsSessionManager, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            } 

            if (rows.Length > 0)
            {
                for (int i = 0; i < rows.Length; i++)
                {
                    DataRow drnew;
                    drnew = dtProperties.NewRow();
                    drnew["propertyname"] = rows[i]["propertyName"].ToString();
                    drnew["Color"] = rows[i]["propertyvalue"].ToString();
                    dtProperties.Rows.Add(drnew);
                    dtProperties.AcceptChanges();
                }
            }

            if (dtProperties.Rows.Count > 0)
            {
                Session["ChangeTheme"] = dtProperties;
            }

            CSSParser css = new CSSParser();

            css.ReadCSSFile(Server.MapPath(@"~/Documents/DynamicThemes/DefaultTheme.css"), dtProperties);

            string sparsed;
            sparsed = css.ToString();

            if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
            {
                System.IO.File.Delete(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css");
            }

            System.IO.File.WriteAllText(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css", sparsed.ToString());

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Change Theme - Change Theme Button";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

            //this re-caches the DataSet after AcceptChanges() commits the new value
            Session["IsThemeChanged"] = "yes";
            Response.Redirect("~/Default.aspx");
        }
    }

    public void refresh()
    {
        //update controls's property and rebind data inside the usercontrol.
    }

    public void reloaddetails()
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun", "Themepicker();", true);
    }
}