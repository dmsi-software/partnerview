﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocViewer.ascx.cs" Inherits="UserControls_DocViewer" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Src="SimplePanel.ascx" TagName="SimplePanel" TagPrefix="uc1" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    
          
    
       
                    <asp:Panel ID="DocumentsPanel" runat="server" Width="100%" >
                        <h3>
                     <asp:Label ID="lblDocumentsTitle" runat="server" Text="Documents"></asp:Label>
                        </h3>
                        <asp:GridView ID="gvDocView" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" HorizontalAlign="Left" OnRowDataBound="gvDocView_RowDataBound" class="sub-grd-tabler">
                            
                            <Columns>
                                <asp:BoundField DataField="DocStorageReportName" HeaderText="Type">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RefNum" HeaderText="Transaction ID">
                                    <HeaderStyle HorizontalAlign="Left"  />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Documents">
                                    
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridrightbordernone" />
                                     <ItemStyle CssClass="gridrightbordernone" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                     <HeaderStyle CssClass="gridrightbordernone" />
                                     <ItemStyle CssClass="gridrightbordernone" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                     <HeaderStyle CssClass="gridrightbordernone" />
                                    <ItemStyle CssClass="gridrightbordernone" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <span ID="spanZeroPriceWarning" runat="server" class="zeroPriceWarning">The retail quote has line items with a zero price.</span>
                    </asp:Panel>
               
              
    </ContentTemplate>
</asp:UpdatePanel>
