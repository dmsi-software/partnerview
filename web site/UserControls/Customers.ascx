<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Customers.ascx.cs" Inherits="UserControls_Customers" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc3" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc2" %>
<%@ Register Src="MenuBar.ascx" TagName="MenuBar" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .PanelPadding {
        padding-left: 4px;
        padding-right: 4px;
    }

    .child-div {
        position:absolute;
        left:2%;
        right:0px;
    }
</style>
<div class="left-section" style="min-height: 797px;">
    <div class="lft-cont-sec">
        <asp:Panel ID="CustomerSearchFieldPanel" runat="server" Width="100%" CssClass="my-tab-content" DefaultButton="btnFind">
            <div class="search-opt">
                <div class="advsearch">
                    <asp:Label ID="lblSearchField" runat="server" Text="Search by" CssClass="alladvsearchlabel"></asp:Label>
                    <asp:DropDownList ID="ddlSearchField" runat="server"></asp:DropDownList>
                    <asp:TextBox ID="txtSearchValue" runat="server"></asp:TextBox>
                    <asp:Button ID="btnFind" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnFind_Click" />
                </div>
            </div>
        </asp:Panel>
    </div>
</div>
<div class="right-section">
    <asp:Panel ID="Panel10" runat="server">
        <div class="container-sec">
            <h2>Accounts
            </h2>
            <div class="custom-grid abc" id="accordion">
                <div>
                    <asp:Label ID="lblCustomerError" CssClass="ErrorTextRed" runat="server" Text="First 200 accounts returned. Add criteria to refine search."
                        Visible="False"></asp:Label>
                </div>
                <div class="optional" style="width: 280px !important;">
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
                        DropShadow="true" PopupControlID="pnlCustomersCustomView" PopupDragHandleControlID="pnlCustomersDragHeader"
                        TargetControlID="lnkCustomersCustomView">
                    </cc1:ModalPopupExtender>
                    <asp:Label ID="Label9" runat="server" Text="Show"></asp:Label>
                    <asp:DropDownList ID="ddlCustomersRows" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerRows_SelectedIndexChanged"
                        Width="50px" CssClass="select opt-size">
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>75</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                        <asp:ListItem>200</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="Label1" runat="server" Text="Rows"></asp:Label>
                    <asp:LinkButton ID="lnkCustomersCustomView" runat="server" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                </div>
                <div class="custom-grid-overflow">
                    <asp:GridView ID="gvCustomers" runat="server" AllowPaging="True" CellPadding="4"
                        CssClass="grid-tabler" GridLines="None" ForeColor="#333333" Width="100%" OnSelectedIndexChanged="gvCustomer_SelectedIndexChanged"
                        HorizontalAlign="Left" OnPageIndexChanging="gvCustomer_PageIndexChanging"
                        EmptyDataText="No Accounts" AutoGenerateColumns="False" OnSorting="gvCustomer_Sorting"
                        AllowSorting="True" OnRowDataBound="gvCustomer_RowDataBound" OnRowCreated="gvCustomer_RowCreated" DataKeyNames="cust_key">
                        <PagerSettings Mode="NumericFirstLast" />
                        <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                        <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                        <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                            Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                            ForeColor="White" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemStyle Width="5%" />
                                <HeaderStyle Width="5%" />
                                <ItemTemplate>
                                    <asp:ImageButton OnClick="Show_Hide_ChildGrid" CommandName="Details" runat="server" ID="imgbtnplusminus" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/Images/Grid_Plus.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField AccessibleHeaderText="Customer Name" ConvertEmptyStringToNull="False"
                                DataField="cust_name" HeaderText="Name" ReadOnly="True" SortExpression="cust_name">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="Customer Code" ConvertEmptyStringToNull="False"
                                DataField="cust_code" HeaderText="Code" ReadOnly="True" SortExpression="cust_code">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="Address 1" ConvertEmptyStringToNull="False"
                                DataField="address_1" HeaderText="Address 1" ReadOnly="True" SortExpression="address_1">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="Address 2" ConvertEmptyStringToNull="False"
                                DataField="address_2" HeaderText="Address 2" ReadOnly="True" SortExpression="address_2"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="Address 3" ConvertEmptyStringToNull="False"
                                DataField="address_3" HeaderText="Address 3" ReadOnly="True" SortExpression="address_3"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="City" ConvertEmptyStringToNull="False" DataField="city"
                                HeaderText="City" HtmlEncode="False" SortExpression="city">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="State" ConvertEmptyStringToNull="False" DataField="state"
                                HeaderText="State" HtmlEncode="False" SortExpression="state">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="ZIP Code" ConvertEmptyStringToNull="False"
                                DataField="zip" HeaderText="ZIP Code" ReadOnly="True" SortExpression="zip">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="Country" ConvertEmptyStringToNull="False" DataField="country"
                                HeaderText="Country" ReadOnly="True" SortExpression="country" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="Phone Number" ConvertEmptyStringToNull="False"
                                DataField="phone" HeaderText="Phone" ReadOnly="True" SortExpression="phone">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField AccessibleHeaderText="Fax Number" ConvertEmptyStringToNull="False"
                                DataField="fax" HeaderText="Fax" ReadOnly="True" SortExpression="fax" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <tr class="tr-innergrid child-div">
                                        <td colspan="12" style="border: none; border-collapse: collapse; width: 100%; padding: 0px;">
                                            <asp:Panel Visible="false" runat="server" ID="innergrids" Style="width: 100%; background-color: White;" class="panel-collapse show-panel acd collapse in">
                                                <h3>
                                                    <asp:Label ID="lblShiptoGridView" runat="server" Text="Ship-tos"></asp:Label>
                                                </h3>
                                                <div style="width: 100%; float: left">
                                                    <div class="optional" style="width: 280px !important;">
                                                        <asp:Label ID="Label2" runat="server" Text="Show"></asp:Label>
                                                        <asp:DropDownList ID="ddlShiptosRows" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlShiptosRows_SelectedIndexChanged"
                                                            Width="50px" CssClass="select opt-size">
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>10</asp:ListItem>
                                                            <asp:ListItem>15</asp:ListItem>
                                                            <asp:ListItem>20</asp:ListItem>
                                                            <asp:ListItem>25</asp:ListItem>
                                                            <asp:ListItem Value="1000">All</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lblinnerresult" runat="server" Text="Rows"></asp:Label>
                                                        <asp:LinkButton CommandArgument='<%#Container.DataItemIndex%>' OnClick="lnkcustshipto_Click" ID="lnkShiptosCustomViewinner" runat="server" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <br />
                                                <div style="width: 100%; float: left">
                                                    <div class="childgrid">
                                                        <asp:GridView ID="gvShiptosinner" runat="server" AllowPaging="True" CellPadding="4" CssClass="sub-grd-tabler"
                                                            GridLines="None" ForeColor="#333333" Width="100%"
                                                            HorizontalAlign="Left" EmptyDataText="No Accounts" AutoGenerateColumns="False"
                                                            AllowSorting="True" OnRowCreated="gvShiptos_RowCreated" OnRowDataBound="gvShiptos_RowDataBound"
                                                            OnSelectedIndexChanged="gvShiptos_SelectedIndexChanged" OnPageIndexChanging="gvShiptos_PageIndexChanging"
                                                            OnSorting="gvShiptos_Sorting">
                                                            <PagerSettings Mode="NumericFirstLast" />
                                                            <FooterStyle CssClass="FooterGridView" Wrap="False" />
                                                            <RowStyle ForeColor="Black" Wrap="False" />
                                                            <EditRowStyle BackColor="#999999" Wrap="False" />
                                                            <SelectedRowStyle BackColor="#E2DED6" Wrap="False" />
                                                            <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" />
                                                            <HeaderStyle CssClass="HeaderGridView" Wrap="False" ForeColor="White" />
                                                            <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                                            <PagerStyle CssClass="PagerDetailGridView" HorizontalAlign="Left" Height="10px" />
                                                            <Columns>
                                                                <asp:CommandField ShowSelectButton="true" ButtonType="Link" ItemStyle-CssClass="selectlinkUnderline" />
                                                                <asp:BoundField ControlStyle-Width="15px" DataField="seq_num" HeaderText="Sequence" ReadOnly="True" SortExpression="seq_num">
                                                                    <ItemStyle HorizontalAlign="Right" Wrap="False" Width="15px" />
                                                                    <HeaderStyle HorizontalAlign="Right" Wrap="False" Width="15px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="shipto_name" HeaderText="Name" ReadOnly="True" SortExpression="shipto_name">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="nonsalable" HeaderText="Nonsalable" ReadOnly="True" SortExpression="nonsalable"
                                                                    Visible="false">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="address_1" HeaderText="Address 1" ReadOnly="True" SortExpression="address_1">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="address_2" HeaderText="Address 2" ReadOnly="True" SortExpression="address_2"
                                                                    Visible="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="address_3" HeaderText="Address 3" ReadOnly="True" SortExpression="address_3"
                                                                    Visible="False">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="city" HeaderText="City" ReadOnly="True" SortExpression="city">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" SortExpression="state">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="zip" HeaderText="ZIP Code" ReadOnly="True" SortExpression="zip">
                                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr class="tr-innergrid">
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px"">
                                            <asp:Panel ID="innerplaceholder" runat="server" Visible="false" Style="width: 100%; background-color: White;" class="panel-collapse show-panel acd collapse in">
                                                <div>&nbsp;</div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="pagination-sec">
                        <div class="info">
                            <asp:Label ID="lblpageno" runat="server" Text="" />
                        </div>
                    </div>
                </div>
                <div class="optional" style="display: none;">
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="ModalBackground"
                        DropShadow="true" PopupControlID="pnlShiptosCustomView" PopupDragHandleControlID="pnlShiptosDragHeader"
                        TargetControlID="lnkShiptosCustomView">
                    </cc1:ModalPopupExtender>
                    <asp:LinkButton ID="lnkShiptosCustomView" runat="server" CssClass="SettingsLink">Select Columns</asp:LinkButton>
                </div>
                <div>
                    <asp:GridView ID="gvShiptos" runat="server" AllowPaging="True" CellPadding="4" CssClass="grid-tabler"
                        Visible="false"
                        HorizontalAlign="Left" EmptyDataText="No Accounts" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="seq_num" HeaderText="Sequence" ReadOnly="True" SortExpression="seq_num">
                                <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipto_name" HeaderText="Name" ReadOnly="True" SortExpression="shipto_name">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="nonsalable" HeaderText="Nonsalable" ReadOnly="True" SortExpression="nonsalable"
                                Visible="false">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="address_1" HeaderText="Address 1" ReadOnly="True" SortExpression="address_1">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="address_2" HeaderText="Address 2" ReadOnly="True" SortExpression="address_2"
                                Visible="False">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="address_3" HeaderText="Address 3" ReadOnly="True" SortExpression="address_3"
                                Visible="False">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="city" HeaderText="City" ReadOnly="True" SortExpression="city">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="state" HeaderText="State" ReadOnly="True" SortExpression="state">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="zip" HeaderText="ZIP Code" ReadOnly="True" SortExpression="zip">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
<asp:Panel ID="pnlCustomersCustomView" runat="server">
    <div id="pnlCustomersDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View</h4>
            </div>
            <div class="modal-body">
                <uc3:ColumnSelector ID="ColumnSelector1" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlShiptosCustomView" runat="server">
    <div id="pnlShiptosDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View</h4>
            </div>
            <div class="modal-body">
                <uc3:ColumnSelector ID="ColumnSelector2" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
