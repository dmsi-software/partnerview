﻿using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Infragistics.Web.UI.NavigationControls;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class UserControls_QuoteEditRetail_Info : System.Web.UI.UserControl
{
    public delegate void QuoteEditModeDelegate();
    public event QuoteEditModeDelegate OnQuoteEditModeChanged;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.MultiplierSelector.OnOKClicked += new UserControls_MultiplierSelector.OKClickedDelegate(MultiplierSelector_OnOKClicked);

        lblGridMessage.Text = ""; //reset the message line...
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
        HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
        hypUserPreferences.Visible = false;

        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            if (Session["IsQuoteInUpdateModeRetail_Info"] == null || (bool)Session["IsQuoteInUpdateModeRetail_Info"] == false)
                FindGVBoundField(gvQuoteDetails, "retail_price").Visible = true;

            FindGVBoundField(gvQuoteDetails, "retail_override_price").Visible = true;
            FindGVBoundField(gvQuoteDetails, "retail_price_uom_code").Visible = true;

            if (Session["IsQuoteInUpdateModeRetail_Info"] == null || (bool)Session["IsQuoteInUpdateModeRetail_Info"] == false)
                FindGVBoundField(gvQuoteDetails, "retail_extended_price").Visible = true;
        }

        dsQuoteDataSet dsQuote = new dsQuoteDataSet();
        dsRetailAddressDataSet dsRetailAddress;

        if (Session["QuoteEditRetail_Info_dsQuote"] != null)
            dsQuote = (dsQuoteDataSet)Session["QuoteEditRetail_Info_dsQuote"];

        if (Session["dsRetailAddressForQuotes"] != null && dsQuote.pvttquote_header.Rows.Count > 0)
        {
            dsRetailAddress = (dsRetailAddressDataSet)Session["dsRetailAddressForQuotes"];
            DataRow[] rows = dsRetailAddress.tt_retail_address.Select("tran_id=" + dsQuote.pvttquote_header[0]["quote_id"].ToString());

            if (rows.Length > 0)
            {
                txtCustomerID.Text = rows[0]["misc_1"].ToString();
                txtShipto.Text = rows[0]["misc_2"].ToString();
                txtCompanyName.Text = rows[0]["cust_name"].ToString();
                txtAddress1.Text = rows[0]["address_1"].ToString();
                txtAddress2.Text = rows[0]["address_2"].ToString();
                txtAddress3.Text = rows[0]["address_3"].ToString();
                txtCity.Text = rows[0]["city"].ToString();
                txtState.Text = rows[0]["state"].ToString();
                txtZip.Text = rows[0]["zip"].ToString();
                txtCountry.Text = rows[0]["country"].ToString();
                txtPhone.Text = rows[0]["phone"].ToString();
                txtContact.Text = rows[0]["misc_3"].ToString();
                txtEmail.Text = rows[0]["email"].ToString();
                txtReference.Text = rows[0]["misc_4"].ToString();
                otherChargesAndTaxesRetailInfo.TaxRate = Convert.ToDecimal(rows[0]["misc_5"].ToString() == "" ? GetDefaultTaxRate().ToString() : rows[0]["misc_5"].ToString());
                otherChargesAndTaxesRetailInfo.TaxableDescription = rows[0]["misc_6"].ToString();
                otherChargesAndTaxesRetailInfo.TaxableAmount = Convert.ToDecimal(rows[0]["misc_7"].ToString() == "" ? "0.00" : rows[0]["misc_7"].ToString());
                otherChargesAndTaxesRetailInfo.NonTaxableDescription = rows[0]["misc_8"].ToString();
                otherChargesAndTaxesRetailInfo.NonTaxableAmount = Convert.ToDecimal(rows[0]["misc_9"].ToString() == "" ? "0.00" : rows[0]["misc_9"].ToString());
            }
            else
            {
                txtCustomerID.Text = "";
                txtShipto.Text = "";
                txtCompanyName.Text = "";
                txtAddress1.Text = "";
                txtAddress2.Text = "";
                txtAddress3.Text = "";
                txtCity.Text = "";
                txtState.Text = "";
                txtZip.Text = "";
                txtCountry.Text = "";
                txtPhone.Text = "";
                txtContact.Text = "";
                txtEmail.Text = "";
                txtReference.Text = "";
                otherChargesAndTaxesRetailInfo.TaxRate = GetDefaultTaxRate();
                otherChargesAndTaxesRetailInfo.TaxableDescription = "";
                otherChargesAndTaxesRetailInfo.TaxableAmount = 0;
                otherChargesAndTaxesRetailInfo.NonTaxableDescription = "";
                otherChargesAndTaxesRetailInfo.NonTaxableAmount = 0;
            }
        }

        decimal quoteRetailTotal = 0;

        if (dsQuote.pvttquote_header[0]["retail_order_total"] != DBNull.Value)
            quoteRetailTotal = Convert.ToDecimal(dsQuote.pvttquote_header[0]["retail_order_total"]);

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            if (Session["SaveQuoteEditHasErrorsRetail_Info"] == null || (bool)Session["SaveQuoteEditHasErrorsRetail_Info"] == false)
            {
                btnChangeMarkup.Visible = true;
                MultiplierModalDiv.Visible = true;
            }
        }

        gvQuoteDetails.DataSource = dsQuote;
        gvQuoteDetails.DataMember = "pvttquote_detail";
        gvQuoteDetails.DataBind();

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormatSingle();", true);
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme1", "ComplementoryPopupHeight();", true);
        gvQuoteDetails.Visible = true;

        //Change modal popup title bar text, based on mode; Project: 94713 - Pricing in PartnerView - Quotes
        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price")
        {
            this.lbleditretail.InnerText = " Edit retail price multiplier";

            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                lbleditretail.InnerText = lbleditretail.InnerText.Replace("multiplier", "margin");
                btnChangeMarkup.Text = "Apply New Margin";
            }
            else
                btnChangeMarkup.Text = "Apply New Multiplier";
        }
        else if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost")
        {
            this.lbleditretail.InnerText = " Edit retail price – markup on cost multiplier";
            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                lbleditretail.InnerText = lbleditretail.InnerText.Replace("multiplier", "margin");
                btnChangeMarkup.Text = "Apply New Margin on Cost";
            }
            else
                btnChangeMarkup.Text = "Apply New Multiplier on Cost";
        }
        else if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
        {
            this.lbleditretail.InnerText = " Edit retail price – discount from list multiplier";
            btnChangeMarkup.Text = "Apply New Multiplier on List Price";
        }
        else
            btnChangeMarkup.Text = "Apply New Multiplier"; //for Net mode

        //Project: 96293 - PV - Footnote missing in Edit Quote screen
        if (Session["UserPref_DisplaySetting"] != null &&
            ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            lblPriceDisclaimer.Visible = false;
        }
        else
            lblPriceDisclaimer.Visible = true;
    }

    void MultiplierSelector_OnOKClicked(decimal markupFactor)
    {
        Quotes q = new Quotes();

        /* 114521 - Users are updating the price in Agility while PV quote remains open.  This causes data in PV to become stale.
            Situation is remedied by updating data prior to applying multiplier.
        */
        FetchUpdatedCurrentQuoteDS((dsQuoteDataSet)Session["QuoteEditRetail_Info_dsQuote"]);

        dsQuoteDataSet dsq = (dsQuoteDataSet)Session["QuoteEditRetail_Info_dsQuote"];

        string returnMessage = q.ApplyMultiplier(markupFactor, ref dsq);

        if (returnMessage == "0")
        {
            Session["QuoteEditRetail_Info_dsQuote"] = null;
            dsq.AcceptChanges();
            Session["QuoteEditRetail_Info_dsQuote"] = dsq;
            gvQuoteDetails.DataSource = null;

            dsRetailAddressDataSet dsRetailAddress = PopulateDSRetailAddress(dsq);

            Dmsi.Agility.Data.Quotes quote = new Dmsi.Agility.Data.Quotes();
            string MessageText = quote.UpdateQuote(dsq, dsRetailAddress);

            if (MessageText == "0")
            {
                FetchUpdatedCurrentQuoteDS(dsq);
            }
            else
            {
                if (MessageText.Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                EventLog log = new EventLog();
                log.LogError(EventTypeFlag.AppServerDBerror, MessageText);
                throw new SystemException("Error occurred during Quote update save..." + MessageText);
            }
        }
        else
        {
            if (returnMessage.Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            lblGridMessage.Text = "<b>Error:</b> Applying the new multiplier has failed. Please try again.&nbsp;&nbsp;";
        }
    }

    public void Initialize(dsQuoteDataSet dsQuote)
    {
        lblGridMessage.Text = ""; //reset the message line...

        lblQuoteHeader.Text = "Edit Retail Information for Quote " + dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();

        Session["QuoteEditRetail_Info_dsQuote"] = null;

        //always do this last...
        Session["QuoteEditRetail_Info_dsQuote"] = dsQuote;
    }

    public void btnCancel_Click(object sender, EventArgs e)
    {
        this.Page.Master.FindControl("OptionsModalDiv").Visible = true;
        HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
        hypUserPreferences.Visible = true;

        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

        UserControl quotes = Parent.FindControl("Quotes1") as UserControl;
        if (quotes != null)
        {
            quotes.Visible = true;
            this.Visible = false;
            Session["QuoteEditVisible"] = false;
        }

        Session["IsQuoteInUpdateModeRetail_Info"] = false;
        Session["SaveQuoteEditHasErrorsRetail_Info"] = false;

        btnChangeMarkup.Enabled = true;

        lblGridMessage.Text = ""; //reset the message line...

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }

        Session["CachedMultiplier"] = null;

        MenuEnableDisable menuEnable = new MenuEnableDisable();
        menuEnable.EnableMenuItems(this.Page, this.Parent);
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        dsQuoteDataSet dsQuote = new dsQuoteDataSet();

        if (Session["QuoteEditRetail_Info_dsQuote"] != null)
        {
            FetchUpdatedCurrentQuoteDS((dsQuoteDataSet)Session["QuoteEditRetail_Info_dsQuote"]);
            dsQuote = (dsQuoteDataSet)Session["QuoteEditRetail_Info_dsQuote"];
        }

        if (Session["IsQuoteInUpdateModeRetail_Info"] != null && (bool)Session["IsQuoteInUpdateModeRetail_Info"] == true)
        {
            lblGridMessage.Text = ""; //reset the message line...
        }

        Session["IsQuoteInUpdateModeRetail_Info"] = false;

        btnChangeMarkup.Enabled = true;

        Session["CachedMultiplier"] = null;

        dsQuote.AcceptChanges();

        dsRetailAddressDataSet dsRetailAddress = new dsRetailAddressDataSet();
        dsRetailAddressDataSet.tt_retail_addressRow row = dsRetailAddress.tt_retail_address.Newtt_retail_addressRow();
        row.misc_1 = txtCustomerID.Text;
        row.misc_2 = txtShipto.Text;
        row.cust_name = txtCompanyName.Text;
        row.address_1 = txtAddress1.Text;
        row.address_2 = txtAddress2.Text;
        row.address_3 = txtAddress3.Text;
        row.city = txtCity.Text;
        row.state = txtState.Text;
        row.zip = txtZip.Text;
        row.country = txtCountry.Text;
        row.phone = txtPhone.Text;
        row.misc_3 = txtContact.Text;
        row.email = txtEmail.Text;
        row.misc_4 = txtReference.Text;
        row.misc_5 = otherChargesAndTaxesRetailInfo.TaxRate.ToTrimmedString();
        row.misc_6 = otherChargesAndTaxesRetailInfo.TaxableDescription;
        row.misc_7 = otherChargesAndTaxesRetailInfo.TaxableAmount.ToTrimmedString();
        row.misc_8 = otherChargesAndTaxesRetailInfo.NonTaxableDescription;
        row.misc_9 = otherChargesAndTaxesRetailInfo.NonTaxableAmount.ToTrimmedString();

        //MDM - required to be not null
        row.dispatch_shipment_num = 0;
        row.dispatch_tran_id = 0;
        row.dispatch_tran_sysid = "";
        row.dispatch_tran_type = "";
        row.shipment_num = 0;
        row.tran_id = Convert.ToInt32(dsQuote.pvttquote_header[0]["quote_id"]);
        row.tran_sysid = "";
        row.type = "";

        dsRetailAddress.tt_retail_address.Rows.Add(row);
        dsRetailAddress.AcceptChanges();

        Dmsi.Agility.Data.Quotes quote = new Dmsi.Agility.Data.Quotes();
        string MessageText = quote.UpdateQuote(dsQuote, dsRetailAddress);
        if (MessageText == "0")
        {
            Session["QuoteHeaderHasBeenSaved"] = true;
            FetchUpdatedCurrentQuoteDS(dsQuote);
        }
        else
        {
            if (MessageText.Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            if (OnQuoteEditModeChanged != null)
            {
                Session["QuoteEditModeChanged"] = false;
                OnQuoteEditModeChanged();
            }

            throw new SystemException("Error occurred during Quote Header save...");
        }
    }

    protected decimal GetDefaultTaxRate()
    {
        return Convert.ToDecimal(Profile.DefaultTaxRate == "" ? "0" : Profile.DefaultTaxRate);
    }
    public void Reset()
    {
        Session["QuoteEditRetail_Info_dsQuote"] = null;
        btnCancel_Click(null, null);

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }
    }

    public void Reset(string source)
    {
        Session["QuoteEditRetail_Info_dsQuote"] = null;

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }
    }

    private dsRetailAddressDataSet PopulateDSRetailAddress(dsQuoteDataSet dsQuote)
    {
        dsRetailAddressDataSet dsRetailAddress = new dsRetailAddressDataSet();

        if (Session["dsRetailAddressForQuotes"] != null)
        {
            dsRetailAddress = (dsRetailAddressDataSet)Session["dsRetailAddressForQuotes"];

            string quoteId = dsQuote.pvttquote_header[0]["quote_id"].ToString();

            for (int x = dsRetailAddress.tt_retail_address.Rows.Count - 1; x >= 0; x--)
            {
                if (dsRetailAddress.tt_retail_address.Rows[x]["tran_id"].ToString() != quoteId)
                {
                    dsRetailAddress.tt_retail_address.Rows[x].Delete();
                }
            }

            dsRetailAddress.AcceptChanges();

            DataRow[] rows = dsRetailAddress.tt_retail_address.Select("tran_id=" + quoteId);

            if (rows.Length > 0)
            {
                rows[0]["misc_1"] = txtCustomerID.Text;
                rows[0]["misc_2"] = txtShipto.Text;
                rows[0]["cust_name"] = txtCompanyName.Text;
                rows[0]["address_1"] = txtAddress1.Text;
                rows[0]["address_2"] = txtAddress2.Text;
                rows[0]["address_3"] = txtAddress3.Text;
                rows[0]["city"] = txtCity.Text;
                rows[0]["state"] = txtState.Text;
                rows[0]["zip"] = txtZip.Text;
                rows[0]["country"] = txtCountry.Text;
                rows[0]["phone"] = txtPhone.Text;
                rows[0]["misc_3"] = txtContact.Text;
                rows[0]["email"] = txtEmail.Text;
                rows[0]["misc_4"] = txtReference.Text;
                rows[0]["misc_5"] = otherChargesAndTaxesRetailInfo.TaxRate.ToTrimmedString();
                rows[0]["misc_6"] = otherChargesAndTaxesRetailInfo.TaxableDescription;
                rows[0]["misc_7"] = otherChargesAndTaxesRetailInfo.TaxableAmount.ToTrimmedString();
                rows[0]["misc_8"] = otherChargesAndTaxesRetailInfo.NonTaxableDescription;
                rows[0]["misc_9"] = otherChargesAndTaxesRetailInfo.NonTaxableAmount.ToTrimmedString();
            }
            else
            {
                DataRow row = dsRetailAddress.tt_retail_address.NewRow();

                row["misc_1"] = txtCustomerID.Text;
                row["misc_2"] = txtShipto.Text;
                row["cust_name"] = txtCompanyName.Text;
                row["address_1"] = txtAddress1.Text;
                row["address_2"] = txtAddress2.Text;
                row["address_3"] = txtAddress3.Text;
                row["city"] = txtCity.Text;
                row["state"] = txtState.Text;
                row["zip"] = txtZip.Text;
                row["country"] = txtCountry.Text;
                row["phone"] = txtPhone.Text;
                row["misc_3"] = txtContact.Text;
                row["email"] = txtEmail.Text;
                row["misc_4"] = txtReference.Text;
                row["misc_5"] = otherChargesAndTaxesRetailInfo.TaxRate.ToTrimmedString();
                row["misc_6"] = otherChargesAndTaxesRetailInfo.TaxableDescription;
                row["misc_7"] = otherChargesAndTaxesRetailInfo.TaxableAmount.ToTrimmedString();
                row["misc_8"] = otherChargesAndTaxesRetailInfo.NonTaxableDescription;
                row["misc_9"] = otherChargesAndTaxesRetailInfo.NonTaxableAmount.ToTrimmedString();

                row["tran_sysid"] = "";
                row["TYPE"] = "";
                row["tran_id"] = 0;
                row["shipment_num"] = 0;
                row["dispatch_tran_sysid"] = "";
                row["dispatch_tran_type"] = "";
                row["dispatch_tran_id"] = 0;
                row["dispatch_shipment_num"] = 0;

                dsRetailAddress.tt_retail_address.Rows.Add(row);
            }

            dsRetailAddress.AcceptChanges();
        }

        return dsRetailAddress;
    }

    private void FetchUpdatedCurrentQuoteDS(dsQuoteDataSet dsQuote)
    {
        Session["SaveQuoteEditHasErrorsRetail_Info"] = false;
        Session["QuoteHeaderHasBeenSaved"] = true;

        DataManager dm = new DataManager();
        string searchCriteria = (string)dm.GetCache("QuoteSearchCriteria");

        string quoteId = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

        siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

        foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
        {
            siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
        }

        currentCust.Dispose();

        Dmsi.Agility.Data.Quotes Quotes = new Dmsi.Agility.Data.Quotes(searchCriteria, siCallDS, null, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        siCallDS.Dispose();

        dsQuoteDataSet quoteList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet)Quotes.ReferencedDataSet;

        for (int x = quoteList.pvttquote_header.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_header.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_header.Rows[x].Delete();
        }

        for (int x = quoteList.pvttquote_detail.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_detail.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_detail.Rows[x].Delete();
        }

        for (int x = quoteList.pvttquote_byitem.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_detail.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_detail.Rows[x].Delete();
        }

        quoteList.AcceptChanges();

        Session["QuoteEditRetail_Info_dsQuote"] = quoteList;
    }

    private BoundField FindGVBoundField(GridView gv, string dataFieldName)
    {
        BoundField bf = new BoundField();

        for (int x = 0; x < gv.Columns.Count; x++)
        {
            bf = gvQuoteDetails.Columns[x] as BoundField;
            if (bf != null && bf.DataField == dataFieldName)
                return bf;
        }

        return bf;
    }

    #region gvQuoteDetails Grid View

    protected void gvQuoteDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvQuoteDetails.Columns[0].Visible = true;

        gvQuoteDetails.EditIndex = -1;

        Session["IsQuoteInUpdateModeRetail_Info"] = false;

        btnChangeMarkup.Enabled = true;

        lblGridMessage.Text = ""; //reset the message line...

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }

        btnSaveChanges.Enabled = true;
        btnGoBack.Enabled = true;
    }

    protected void gvQuoteDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem != null)
        {
            DataRowView rowView = e.Row.DataItem as DataRowView;
            System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[0]));

            if ((e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                imgedit.ToolTip = "Save";
            }
            else
            {
                imgedit.ToolTip = "Edit";
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }
            }

            e.Row.Attributes.Add("onmouseover", "this.className='GridViewRowMouseHover'");
            e.Row.Attributes.Add("onmouseout", "this.className='GridViewRowMouseOut'");

            // NOTE: a good sample of converting a cell to the databound field
            if (e.Row.RowIndex == gvQuoteDetails.EditIndex)
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    BoundField fld = ((DataControlFieldCell)cell).ContainingField as BoundField;

                    if (fld != null && fld.DataField == "location_reference")
                    {
                        foreach (Control ctl in cell.Controls)
                        {
                            TextBox txt = ctl as TextBox;
                            if (txt != null)
                            {
                                txt.MaxLength = 40;
                                txt.Width = 150;
                            }
                        }
                    }
                }
            }
        }
    }

    protected void gvQuoteDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        gvQuoteDetails.Columns[0].Visible = true;

        int rowSequenceKey = Convert.ToInt32(e.Keys["sequence"]);

        decimal newRetailPrice = 0;
        bool changedRetailPrice = false;

        bool ok = true;

        if (e.NewValues["retail_price"] != null)
        {
            changedRetailPrice = Decimal.TryParse(e.NewValues["retail_price"].ToString(), out newRetailPrice);

            if (!changedRetailPrice)
            {
                lblGridMessage.Text += "<b>Error:</b> Invalid price in Sequence " + rowSequenceKey.ToString();
                e.Cancel = true;
                ok = false;
            }
        }

        if (ok)
        {
            dsQuoteDataSet dsQuote = new dsQuoteDataSet();

            if (Session["QuoteEditRetail_Info_dsQuote"] != null)
            {
                FetchUpdatedCurrentQuoteDS((dsQuoteDataSet)Session["QuoteEditRetail_Info_dsQuote"]);
                dsQuote = (dsQuoteDataSet)Session["QuoteEditRetail_Info_dsQuote"];
            }

            DataRow[] rows = dsQuote.pvttquote_detail.Select("sequence = " + rowSequenceKey.ToString());
            if (rows.Length == 1)
            {
                if (changedRetailPrice)
                    rows[0]["retail_price"] = newRetailPrice;

                if (e.NewValues["location_reference"] != null)
                    rows[0]["location_reference"] = e.NewValues["location_reference"] as string;
                else if (e.Keys["location_reference"] == null)
                    rows[0]["location_reference"] = "";
            }

            dsQuote.AcceptChanges();

            dsRetailAddressDataSet dsRetailAddress = PopulateDSRetailAddress(dsQuote);

            Dmsi.Agility.Data.Quotes quote = new Dmsi.Agility.Data.Quotes();
            string MessageText = quote.UpdateQuote(dsQuote, dsRetailAddress);

            if (MessageText == "0")
            {
                FetchUpdatedCurrentQuoteDS(dsQuote);
            }
            else
            {
                if (MessageText.Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                EventLog log = new EventLog();
                log.LogError(EventTypeFlag.AppServerDBerror, MessageText);
                throw new SystemException("Error occurred during Quote update save..." + MessageText);
            }

            gvQuoteDetails.EditIndex = -1;

            Session["IsQuoteInUpdateModeRetail_Info"] = false;

            btnChangeMarkup.Enabled = true;

            lblGridMessage.Text = ""; //reset the message line...

            if (OnQuoteEditModeChanged != null)
            {
                Session["QuoteEditModeChanged"] = false;
                OnQuoteEditModeChanged();
            }

            btnSaveChanges.Enabled = true;
            btnGoBack.Enabled = true;
        }
    }

    protected void gvQuoteDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridViewRow row = gvQuoteDetails.Rows[e.NewEditIndex];

        gvQuoteDetails.Columns[0].Visible = false;

        this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
        HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
        hypUserPreferences.Visible = false;

        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;

        Session["IsQuoteInUpdateModeRetail_Info"] = true;

        btnChangeMarkup.Enabled = false;

        btnSaveChanges.Enabled = false;
        btnGoBack.Enabled = false;

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = true;
            OnQuoteEditModeChanged();
        }

        gvQuoteDetails.Columns[0].Visible = true;
        Session["QuoteEditedRowId"] = e.NewEditIndex;
        System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(gvQuoteDetails.Rows[e.NewEditIndex].Cells[0].Controls[0]));
        imgedit.ToolTip = "Save";
    }

    protected void gvQuoteDetails_DataBound(object sender, EventArgs e)
    {
        //Handle Edit allocation...
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        if (Session["UserPref_DisplaySetting"] != null &&
            ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            FindGVBoundField(gvQuoteDetails, "retail_price").ReadOnly = false;
        }
    }

    #endregion

}