using System;
using System.Data;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI;

public partial class UserControls_AddInfoSelector : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate();
    public event OKClickedDelegate OnOKClicked;
    
    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    public delegate void CancelClickedDelegate();
    public event CancelClickedDelegate OnCancelClicked;


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["AddInfo_txtCustomerId"] != null)
        {
            txtCustomerId.Text = Convert.ToString(Session["AddInfo_txtCustomerId"]);
            txtShipTo.Text = Convert.ToString(Session["AddInfo_txtShipTo"]);
            txtCustomerName.Text = Convert.ToString(Session["AddInfo_txtCustomerName"]);
            txtAddress1.Text = Convert.ToString(Session["AddInfo_txtAddress1"]);
            txtAddress2.Text = Convert.ToString(Session["AddInfo_txtAddress2"]);
            txtAddress3.Text = Convert.ToString(Session["AddInfo_txtAddress3"]);
            txtCity.Text = Convert.ToString(Session["AddInfo_txtCity"]);
            txtState.Text = Convert.ToString(Session["AddInfo_txtState"]);
            txtZip.Text = Convert.ToString(Session["AddInfo_txtZip"]);
            txtCountry.Text = Convert.ToString(Session["AddInfo_txtCountry"]);
            txtPhone.Text = Convert.ToString(Session["AddInfo_txtPhone"]);
            txtContactName.Text = Convert.ToString(Session["AddInfo_txtContactName"]);
            txtEmail.Text = Convert.ToString(Session["AddInfo_txtEmail"]);
            txtReference.Text = Convert.ToString(Session["AddInfo_txtReference"]);
            otherChargesAndTaxes.TaxRate = Convert.ToDecimal(Session["AddInfo_taxRate"]);
            otherChargesAndTaxes.TaxableDescription = Convert.ToString(Session["AddInfo_taxableDescription"]);
            otherChargesAndTaxes.TaxableAmount = Convert.ToDecimal(Session["AddInfo_taxableAmount"]);
            otherChargesAndTaxes.NonTaxableDescription = Convert.ToString(Session["AddInfo_nonTaxableDescription"]);
            otherChargesAndTaxes.NonTaxableAmount = Convert.ToDecimal(Session["AddInfo_nonTaxableAmount"]);
        }
        else
        {
            txtCustomerId.Text = "";
            txtShipTo.Text = "";
            txtCustomerName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtContactName.Text = "";
            txtEmail.Text = "";
            txtReference.Text = "";
            otherChargesAndTaxes.TaxRate = GetDefaultTaxRate();
            otherChargesAndTaxes.TaxableDescription = "";
            otherChargesAndTaxes.TaxableAmount = 0;
            otherChargesAndTaxes.NonTaxableDescription = "";
            otherChargesAndTaxes.NonTaxableAmount = 0;
        }
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);

    }
    protected decimal GetDefaultTaxRate()
    {
        return Convert.ToDecimal(Profile.DefaultTaxRate == "" ? "0" : Profile.DefaultTaxRate);
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        Session["AddInfo_txtCustomerId"] = txtCustomerId.Text;
        Session["AddInfo_txtShipTo"] = txtShipTo.Text;
        Session["AddInfo_txtCustomerName"] = txtCustomerName.Text;
        Session["AddInfo_txtAddress1"] = txtAddress1.Text;
        Session["AddInfo_txtAddress2"] = txtAddress2.Text;
        Session["AddInfo_txtAddress3"] = txtAddress3.Text;
        Session["AddInfo_txtCity"] = txtCity.Text;
        Session["AddInfo_txtState"] = txtState.Text;
        Session["AddInfo_txtZip"] = txtZip.Text;
        Session["AddInfo_txtCountry"] = txtCountry.Text;
        Session["AddInfo_txtPhone"] = txtPhone.Text;
        Session["AddInfo_txtContactName"] = txtContactName.Text;
        Session["AddInfo_txtEmail"] = txtEmail.Text;
        Session["AddInfo_txtReference"] = txtReference.Text;
        Session["AddInfo_taxRate"] = otherChargesAndTaxes.TaxRate;
        Session["AddInfo_taxableDescription"] = otherChargesAndTaxes.TaxableDescription;
        Session["AddInfo_taxableAmount"] = otherChargesAndTaxes.TaxableAmount;
        Session["AddInfo_nonTaxableDescription"] = otherChargesAndTaxes.NonTaxableDescription;
        Session["AddInfo_nonTaxableAmount"] = otherChargesAndTaxes.NonTaxableAmount;

        if (OnOKClicked != null)
            OnOKClicked(); // will be caught by the parent
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (OnCancelClicked != null)
            OnCancelClicked(); // will be caught by the parent
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtCustomerId.Text = "";
        txtShipTo.Text = "";
        txtCustomerName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtZip.Text = "";
        txtCountry.Text = "";
        txtPhone.Text = "";
        txtContactName.Text = "";
        txtEmail.Text = "";
        txtReference.Text = "";
        otherChargesAndTaxes.TaxRate = GetDefaultTaxRate();
        otherChargesAndTaxes.TaxableDescription = "";
        otherChargesAndTaxes.TaxableAmount = 0;
        otherChargesAndTaxes.NonTaxableDescription = "";
        otherChargesAndTaxes.NonTaxableAmount = 0;

        Session["AddInfo_txtCustomerId"] = null;
        Session["AddInfo_txtShipTo"] = null;
        Session["AddInfo_txtCustomerName"] = null;
        Session["AddInfo_txtAddress1"] = null;
        Session["AddInfo_txtAddress2"] = null;
        Session["AddInfo_txtAddress3"] = null;
        Session["AddInfo_txtCity"] = null;
        Session["AddInfo_txtState"] = null;
        Session["AddInfo_txtZip"] = null;
        Session["AddInfo_txtCountry"] = null;
        Session["AddInfo_txtPhone"] = null;
        Session["AddInfo_txtContactName"] = null;
        Session["AddInfo_txtEmail"] = null;
        Session["AddInfo_txtReference"] = null;
        Session["AddInfo_taxRate"] = null;
        Session["AddInfo_taxableDescription"] = null;
        Session["AddInfo_taxableAmount"] = null;
        Session["AddInfo_nonTaxableDescription"] = null;
        Session["AddInfo_nonTaxableAmount"] = null;

        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }
}
