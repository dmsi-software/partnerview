<%@ Control EnableTheming="true" Language="C#" AutoEventWireup="true" CodeFile="MainTabControl.ascx.cs"
    Inherits="MainTabControl" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.NavigationControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.ListControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.NavigationControls" TagPrefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControl" %>
<%@ Register Src="~/UserControls/Home.ascx" TagName="Home" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Customers.ascx" TagName="Customers" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/SalesOrders.ascx" TagName="SalesOrders" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/Quotes.ascx" TagName="Quotes" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/CreditMemos.ascx" TagName="CreditMemos" TagPrefix="uc5" %>
<%@ Register Src="~/UserControls/Invoices.ascx" TagName="Invoices" TagPrefix="uc6" %>
<%@ Register Src="~/UserControls/OrderTab.ascx" TagName="OrderTab" TagPrefix="uc8" %>
<%@ Register Src="~/UserControls/QuickList.ascx" TagName="QuickList" TagPrefix="uc9" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc10" %>
<%@ Register Src="~/UserControls/MenuBar.ascx" TagName="MenuBar" TagPrefix="uc11" %>
<%@ Register Src="~/UserControls/Configurator.ascx" TagName="Configurator" TagPrefix="uc12" %>
<%@ Register Src="~/UserControls/QuoteRelease.ascx" TagName="QuoteRelease" TagPrefix="uc13" %>
<%@ Register Src="~/UserControls/QuoteEdit.ascx" TagName="QuoteEdit" TagPrefix="uc14" %>
<%@ Register Src="~/UserControls/QuoteEditRetail_Info.ascx" TagName="QuoteEditRetail_Info" TagPrefix="uc21" %>
<%@ Register Src="~/UserControls/Inventory.ascx" TagName="Inventory" TagPrefix="uc15" %>
<%@ Register Src="~/UserControls/QuoteEditAddItems.ascx" TagName="QuoteEditAddItems" TagPrefix="uc16" %>
<%@ Register Src="~/UserControls/ComplementaryItems.ascx" TagName="ComplementaryItems" TagPrefix="uc17" %>
<%@ Register Src="~/UserControls/SettingsUserControl.ascx" TagName="Passwordchanges" TagPrefix="uc18" %>
<%@ Register Src="~/UserControls/ChangeTheme.ascx" TagName="ChangeTheme" TagPrefix="uc19" %>
<%@ Register Src="~/UserControls/Payments.ascx" TagName="Payments" TagPrefix="uc20" %>
<script type="text/javascript" language="javascript" id="ig">
    function FilterInput(oEdit, keyCode) {
        if (event.keyCode == 13) {

            if (document.getElementById('ctl00_cphMaster1_MainTabControl1_btnCustAddress') != null) {
                document.getElementById('ctl00_cphMaster1_MainTabControl1_btnCustAddress').click();
            }
        }
        else {
        }

        return;
    }
</script>


<style type="text/css">
    .MenuNoWrap {
        white-space: nowrap;
        height:auto !important;
    }

     .MenuNoWrap:focus

     {

         background: #eee !important;
     }
   

    .MenuNoWrapRightMargin {
        white-space: nowrap;
        margin-right: 2px;
    }

    .leftmargin {
        margin-left: 6px;
    }

    .marginbottom {
        margin-bottom: 0px;
        margin-left: 4px;
    }
    .menubarright{
        float:left !important;
        margin-left:4px;
    }
    .menulastlist{
        padding-right:0px !important;
    }
    .pnlContentContainer {
        background-color: White;
        border-left: 1px solid #949A9C;
        border-right: 1px solid #949A9C;
        border-bottom: 1px solid #949A9C;
        border-top: 1px solid #949A9C;
    }

    .RoundedCornerTextBox {
        height: 22px;
        /*width: 445px;*/
        padding-left: 5px;
        padding-top: 3px;
    }

    /*Search text box Styles Starts*/
    .searchTextbox {
        width: 350px;
        height: 22px;
        text-align: left;
        background-color: White;
        white-space: nowrap;
    }

    .searchTextbox .image {
        background-image: url('../../Images/Search_White.png');
        background-repeat: no-repeat;
        background-position: left;
        height: 22px;
        width: 23px;
        float: right;
        padding-top: 1px;
        padding-right: 2px;
        outline: none !important;
    }
</style>
<!-- header section -->

<div class="logo-header"  >
    <div class="col-sm-12" style="height:54px;">
        <div class="col-sm-5 pull-right">
            <div class="search" id="dvSearchTextBox" runat="server">

                <asp:Panel ID="pnlTextImage" runat="server" DefaultButton="btnCustAddress" >
                    
                   
                    <asp:Button ID="btnCustAddress" runat="server" Text="BB" Style="display: none" OnClick="btnCustAddress_OnClick" />
                    <ig:WebTextEditor ID="txtProductSearchKeyword" runat="server" NullText="  Product search by keyword(s)"
                        CssClass="RoundedCornerTextBox search-bar-text" BorderWidth="0px" EnableTheming="False">
                    </ig:WebTextEditor>
                    <asp:ImageButton ID="imgCustAddress" runat="server" ImageUrl="~/Images/search_icon.png"
                        CssClass="image" CausesValidation="false" AlternateText="Search" OnClick="imgCustAddress_OnClick"
                        UseSubmitBehavior="False" />
                   
                </asp:Panel>
            </div>
        </div>
    </div>

    <div class="col-sm-12" style="border-bottom:solid 1px #dadada !important;">
    <div class="col-sm-7" style="padding-bottom:20px;">
       
        <asp:ImageButton ID="imgLogo" runat="server" SkinID="TopLogo"
            CssClass="logo" OnClick="imgLogo_Click" />
    </div>
    <div class="col-sm-5">
        <div class="collapse navbar-collapse bs-navbar-collapse" >

            <ig:WebDataMenu ID="WebDataMenu1" runat="server" OnItemClick="WebDataMenu1_ItemClick"
                Font-Size="14px" ActivateOnHover="False" EnableScrolling="False"
                CssClass="nav navbar-nav navbar-right menubarright">
                <ItemSettings CssClass="MenuNoWrap" />
                <GroupSettings Orientation="Horizontal"  />
                <Items>
                    <ig:DataMenuItem Text="Products" >
                    </ig:DataMenuItem>
                    <ig:DataMenuItem Text="Quotes">
                    </ig:DataMenuItem>
                    <ig:DataMenuItem Text="Orders">
                    </ig:DataMenuItem>
                    <ig:DataMenuItem Text="Invoices">
                    </ig:DataMenuItem>
                    <ig:DataMenuItem Text="Credit Memos">
                    </ig:DataMenuItem>
                    <ig:DataMenuItem Text="Billing" CssClass="menulastlist">
                    </ig:DataMenuItem>
                </Items>
            </ig:WebDataMenu>
        </div>
    </div>
</div>
</div>


<!-- header section End -->
<div id="body-container">



                        <asp:Label ID="Label1" runat="server" CssClass="FieldLabel" Text="Branch:" Visible="false"></asp:Label>

                        <asp:DropDownList ID="ddlBranches" Visible="false" runat="server" Width="200px" Height="18px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged"
                            AutoPostBack="true" CssClass="marginbottom">
                        </asp:DropDownList>
                 
            
                    <asp:Panel runat="server" ID="pnlContentContainer"
                        Visible="False"  CssClass="mid-container">
                        <uc1:Home ID="Home1" runat="server" Visible="False" />
                        <uc2:Customers ID="CustomerTabPanel1" runat="server" Visible="False" />
                        <uc3:SalesOrders ID="SalesOrders1" runat="server" Visible="False" />
                        <uc5:CreditMemos ID="CreditMemos1" runat="server" Visible="false" />
                        <uc4:Quotes ID="Quotes1" runat="server" Visible="false" />
                        <uc13:QuoteRelease ID="QuoteRelease" runat="server" Visible="false" />
                        <uc14:QuoteEdit ID="QuoteEdit" runat="server" Visible="false" />
                        <uc6:Invoices ID="Invoices1" runat="server" Visible="false" />
                        <uc8:OrderTab ID="OrderTab1" runat="server" Visible="false" />
                        <uc15:Inventory ID="Inventory1" runat="server" Visible="false" Mode="Inventory" />
                        <uc16:QuoteEditAddItems ID="QuoteEditAddItems" runat="server" Visible="false" />
                        <uc17:ComplementaryItems ID="ComplementaryItems" runat="server" Visible="false" />
                        <uc18:Passwordchanges ID="PasswordChanges1" runat="server" Visible="false" />
                        <uc20:Payments ID="Payments1" runat="server" Visible="false" />
                        <uc21:QuoteEditRetail_Info ID="QuoteEditRetail_Info" runat="server" Visible="false" />
                    </asp:Panel>
               
        
            <AjaxControl:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" TargetControlID="UpdateProgress"
                Enabled="true" VerticalSide="bottom" VerticalOffset="20" HorizontalOffset="17"
                HorizontalSide="left" runat="server" />
            <div id="UpdateProgress" class="UpdateProgress" style="height: 19px; background-color: transparent"
                runat="server">
                <asp:UpdateProgress ID="UpdateProgress2" runat="server" DisplayAfter="100">
                    <ProgressTemplate>
                        <table cellpadding="0" cellspacing="0" style="background-color: transparent; bottom:20px; position:relative;" class="BorderedContainer">
                            <tr>
                                <td style="width: 158px; background-color: transparent">
                                    <img src="Images/loadingbar.gif" alt="Loading" height="19px" width="157px" align="left"
                                        style="vertical-align: middle; text-align: left; background-color: transparent" />
                                </td>
                                <td style="background-color: transparent; vertical-align:bottom; padding-bottom: 1px">Processing...&nbsp;
                                </td>
                            </tr>
                        </table>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
      
<asp:SqlDataSource ID="SqlDataSourcePVDatabase" runat="server" ConnectionString="Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\PVDatabase.mdf;Integrated Security=True;User Instance=True"
    SelectCommand="SELECT * FROM [tbl_user]" DataSourceMode="DataReader"></asp:SqlDataSource>
    

</div>