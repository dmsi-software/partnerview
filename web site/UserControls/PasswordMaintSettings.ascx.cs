﻿using System;
using System.Configuration;
using Dmsi.Agility.EntryNET;

public partial class UserControls_PasswordMaintSettings : System.Web.UI.UserControl
{
    private ExceptionManager _exceptionManager;
    private EventLog _eventLog;
    private SecurityManager _securityManager;
    private bool _errorMessageBoolean;
    private string _errorMessageText;
    private string _userName;
    private string _validationErrorText;

    protected void Page_Load(object sender, EventArgs e)
    {
        string PasswordRequirements = ConfigurationManager.AppSettings["PW_RequirementsText"].ToString();
        PasswordRequirements = PasswordRequirements.Replace("[", "<");
        PasswordRequirements = PasswordRequirements.Replace("]", ">");
        int PasswordReturnCode = Convert.ToInt32(Session["passRC"]);
        dverr.Visible = false;
        _exceptionManager = new ExceptionManager();
        _eventLog = new EventLog();
        _securityManager = new SecurityManager();
        if (Session["UserName"] != null)
        {
            _userName = Session["UserName"].ToString();
        }
        if (Session["LoginMessage"] != null)
        {
            if (string.IsNullOrEmpty(Session["LoginMessage"].ToString()))
            {
                this.ChangePasswordTitle.Text =  PasswordRequirements;
            
            }
            else
            {

                this.ChangePasswordTitle.Text = "<b>" + Session["LoginMessage"].ToString() + "</b><br /><br />" + PasswordRequirements;

            }
        }
        switch (PasswordReturnCode)
        {
            case -30:
            case -40:
                break;

            case -50:
                lblSuccess.Text = "";
                dvsuccess.Visible = false;
                break;
            case 0:
                lblSuccess.Text = "";
                dvsuccess.Visible = false;
                break;
            default:
                break;
        }
    }

    protected void btnChange_Click(object sender, EventArgs e)
    {
        Session["KeepModalOpen"] = true;

        switch (EntriesAreValid())
        {
            case true:
                if (txtNewPassword.Text == txtConfirmNew.Text)
                {
                    int ReturnCode;
                    string MessageText;

                    _securityManager.ChangePassword(_userName, this.txtOldPassword.Text, this.txtNewPassword.Text, ref _errorMessageBoolean, ref _errorMessageText, out ReturnCode, out MessageText);

                    if (_errorMessageBoolean)
                    {
                        _validationErrorText = "";
                        lblSuccess.Text = "Your password change was accepted.<br />";
                        dvsuccess.Visible = true;
                        int passwordReturnCode = Convert.ToInt32(Session["passRC"]);

                        /* Analytics Tracking */
                        AnalyticsInput aInput = new AnalyticsInput();

                        aInput.Type = "event";
                        aInput.Action = "Change Password Button on Change Password Screen";
                        aInput.Label = "";
                        aInput.Value = "0";

                        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                        am.BeginTrackEvent();

                        if (passwordReturnCode == -50 || passwordReturnCode == 0)
                        {
                            // Server context is already available, not need to force the login logic
                            Session["passRC"] = 0;
                            Session["LoggedIn"] = true;
                            Session["IsHome"] = true;
                            Response.Redirect("~/default.aspx?Display=Home");
                        }
                        else
                        {
                            // Force to run the login procedure, to get the proper server context
                            Session["LoggedIn"] = null;
                            Session["ChangePasswordBeforeLoginSuccessful"] = true;
                            Session["UserName"] = _userName;
                            Session["Password"] = txtConfirmNew.Text;
                            Session["passRC"] = 0;
                        }
                        dverr.Visible = false;
                    }
                    else
                    {
                        dverr.Visible = true;
                        this.lblValidationError.Text = "<strong> Please correct the following errors: </strong> <p>" + _errorMessageText + "</p>";
                    }
                }
                else
                {
                    dverr.Visible = true;
                    this.lblValidationError.Text = "<strong>New passwords did not match. Please try again...</strong>";
                }
                break;
            case false:
                {
                    this.lblValidationError.Text = "<strong>Please correct the following errors:</strong><p>" + _validationErrorText + "</p>";
                    dverr.Visible = true;
                }
                break;
        }
    }

    protected Boolean EntriesAreValid()
    {
        Boolean IsStatusValid;
        int iUpperCase = Convert.ToInt16(ConfigurationManager.AppSettings["PW_MinUpperCase"].ToString(), 10);
        String UpperCaseOptional = ConfigurationManager.AppSettings["PW_MinUpperCaseOptional"].ToString();
        int iAlpha = Convert.ToInt16(ConfigurationManager.AppSettings["PW_MinAlpha"].ToString(), 10);
        String AlphaOptional = ConfigurationManager.AppSettings["PW_MinAlphaOptional"].ToString();
        int iLowerCase = Convert.ToInt16(ConfigurationManager.AppSettings["PW_MinLowerCase"].ToString(), 10);
        String LowerCaseOptional = ConfigurationManager.AppSettings["PW_MinLowerCaseOptional"].ToString();
        int iNumeric = Convert.ToInt16(ConfigurationManager.AppSettings["PW_MinNumeric"].ToString(), 10);
        String NumericOptional = ConfigurationManager.AppSettings["PW_MinNumericOptional"].ToString();
        int iSymbol = Convert.ToInt16(ConfigurationManager.AppSettings["PW_MinSymbol"].ToString(), 10);
        String SymbolOptional = ConfigurationManager.AppSettings["PW_MinSymbolOptional"].ToString();
        int iMinLength = Convert.ToInt16(ConfigurationManager.AppSettings["PW_MinLength"].ToString(), 10);
        int iMaxLength = Convert.ToInt16(ConfigurationManager.AppSettings["PW_MaxLength"].ToString(), 10);
        int iExcludeUserName = Convert.ToInt16(ConfigurationManager.AppSettings["PW_ExcludeUserName"].ToString(), 10);
        String ExcludeUserNameOptional = ConfigurationManager.AppSettings["PW_ExcludeUserNameOptional"].ToString();
        int iExcludeRepeats = Convert.ToInt16(ConfigurationManager.AppSettings["PW_ExcludeRepeats"].ToString(), 10);
        String ExcludeRepeatsOptional = ConfigurationManager.AppSettings["PW_ExcludeRepeatsOptional"].ToString();
        int iOptionalScore = Convert.ToInt16(ConfigurationManager.AppSettings["PW_OptionalScore"].ToString(), 10);

        if (iMaxLength > 16) iMaxLength = 16;

        int FoundUpperCase = iUpperCase * -1;
        int FoundLowerCase = iLowerCase * -1;
        int FoundAlpha = iAlpha * -1;
        int FoundExcludeUserName = 0;
        int FoundNumeric = iNumeric * -1;
        int FoundSymbol = iSymbol * -1;

        int FoundExcludeRepeats = 0;// iExcludeRepeats * -1;
        int FoundMinLength = iMinLength * -1;
        int FoundMaxLength = iMaxLength;
        int FoundPasswordScore = 0;
        string newPassword = txtNewPassword.Text;
        string newPassword_UC = newPassword.ToUpper();

        char checkChar;

        FoundMinLength += newPassword.Length;
        FoundMaxLength -= newPassword.Length;
        if (FoundMinLength < 0)
        {
            FoundPasswordScore -= 99;
        }
        if (FoundMaxLength < 0)
        {
            FoundPasswordScore -= 99;
        }
        if ((newPassword_UC.Contains(_userName.ToUpper())) && (iExcludeUserName > 0))
        {
            switch (ExcludeUserNameOptional.ToUpper())
            {
                case ("YES"):
                    FoundExcludeUserName -= 1;
                    break;
                case ("NO"):
                    FoundExcludeUserName -= 99;

                    break;
            }
        };

        for (int i = 0; i < newPassword.Length; i++)
        {
            checkChar = newPassword[i];
            if (Char.IsNumber(newPassword[i])) FoundNumeric++;
            if (Char.IsUpper(newPassword[i])) FoundUpperCase++;
            if (Char.IsLower(newPassword[i])) FoundLowerCase++;
            if (Char.IsLetter(newPassword[i])) FoundAlpha++;
            if (Char.IsSymbol(newPassword[i])) FoundSymbol++;
            if (Char.IsPunctuation(newPassword[i])) FoundSymbol++;
        };
        FoundPasswordScore += ScoreThisCase(iMinLength, FoundMinLength, "No");
        FoundPasswordScore += ScoreThisCase(iMaxLength, FoundMaxLength, "No");
        FoundPasswordScore += ScoreThisCase(iNumeric, FoundNumeric, NumericOptional);
        FoundPasswordScore += ScoreThisCase(iUpperCase, FoundUpperCase, UpperCaseOptional);
        FoundPasswordScore += ScoreThisCase(iLowerCase, FoundLowerCase, LowerCaseOptional);
        FoundPasswordScore += ScoreThisCase(iSymbol, FoundSymbol, SymbolOptional);
        FoundPasswordScore += ScoreThisCase(iAlpha, FoundAlpha, AlphaOptional);
        FoundPasswordScore += ScoreThisCase(iExcludeUserName, FoundExcludeUserName, ExcludeUserNameOptional);
        FoundPasswordScore += ScoreThisCase(iExcludeRepeats, FoundExcludeRepeats, ExcludeRepeatsOptional);

        if (FoundPasswordScore >= iOptionalScore)
        {
            IsStatusValid = true;
        }
        else
        {
            if (FoundMinLength < 0) _validationErrorText += "<li>Password must contain a minimum of " + iMinLength.ToString() + " characters.</li>";
            if (FoundMaxLength < 0) _validationErrorText += "<li>Password exceeds the maximum of " + iMaxLength.ToString() + " characters.</li>";
            if (FoundNumeric < 0) _validationErrorText += "<li>Password " + OptText(NumericOptional) + " contain a minimum of " + iNumeric.ToString() + " numeric character" + pluralText(iNumeric) + ".</li>";
            if (FoundAlpha < 0) _validationErrorText += "<li>Password " + OptText(AlphaOptional) + " contain a minimum of " + iAlpha.ToString() + " letter character" + pluralText(iAlpha) + ".</li>";
            if (FoundUpperCase < 0) _validationErrorText += "<li>Password " + OptText(UpperCaseOptional) + " contain a minimum of " + iUpperCase.ToString() + " uppercase alpha character" + pluralText(iUpperCase) + ".</li>";
            if (FoundLowerCase < 0) _validationErrorText += "<li>Password " + OptText(LowerCaseOptional) + " contain a minimum of " + iLowerCase.ToString() + " lowercase alpha character" + pluralText(iLowerCase) + ".</li>";
            if (FoundSymbol < 0) _validationErrorText += "<li>Password " + OptText(SymbolOptional) + " contain a minimum of " + iSymbol.ToString() + " special (symbolic) character" + pluralText(iSymbol) + ".</li>";
            if (FoundExcludeUserName < 0) _validationErrorText += "<li>Password " + OptText(ExcludeUserNameOptional) + " not contain the login user name.";
            if (FoundExcludeRepeats < 0) _validationErrorText += "<li>Password " + OptText(ExcludeRepeatsOptional) + " not be a repetition of a password that has been used within the past " + iExcludeRepeats.ToString() + " times.</li>";
            _validationErrorText = "<ul class='validationmessageul'>" + _validationErrorText + "</ul>";
            IsStatusValid = false;
        };

        return IsStatusValid;
    }

    protected int ScoreThisCase(int RuleCount, int FoundItemCount, string IsOptional)
    {
        Boolean IsItOptional = true;
        if (IsOptional.Contains("N")) IsItOptional = false;
        int ReturnValue = 0;
        if (RuleCount == 0)
        {
            //do nothing
        }
        else if ((FoundItemCount >= 0) && (IsItOptional))
        {
            ReturnValue = 1;
        }
        else if ((FoundItemCount < 0) && (!IsItOptional))
        {
            ReturnValue = -99;
        };
        return ReturnValue;
    }

    protected string OptText(string IsOptional)
    {
        string returnText = "must";
        if (IsOptional.Contains("Y")) returnText = "should";

        return returnText;
    }

    protected string pluralText(int MinRequirement)
    {
        string returnText = "s";
        if (MinRequirement == 1) returnText = "";
        return returnText;
    }
}