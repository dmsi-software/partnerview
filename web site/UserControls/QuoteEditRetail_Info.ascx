﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuoteEditRetail_Info.ascx.cs" Inherits="UserControls_QuoteEditRetail_Info" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.GridControls" TagPrefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/MultiplierSelector.ascx" TagName="MultiplierSelector" TagPrefix="uc9" %>
<%@ Register Src="~/UserControls/Inventory.ascx" TagName="Inventory" TagPrefix="uc10" %>
<%@ Register Src="~/UserControls/ShiptoQuoteEditSelector.ascx" TagName="ShiptoQuoteEditSelector" TagPrefix="uc11" %>
<%@ Register Src="~/UserControls/OtherChargesAndTaxes.ascx" TagName="OtherChargesAndTaxes" TagPrefix="uc" %>

<style type="text/css">
    .wrapItem {
        white-space: normal;
    }

    .gridHeader {
        white-space: nowrap;
    }

    .style3 {
        width: 210px;
    }

    .style4 {
        width: 69px;
    }

    .style5 {
        width: 90px;
    }

    .Field {
        margin-left: 0px;
    }

    .TopMargin1 {
        margin-top: 1px;
    }

    .TopMargin10 {
        margin-top: 10px !important;
    }

    .ErrorTextRedWithTopMargin {
        font-family: Helvatica,Verdana,Tahoma,Arial;
        font-size: 8pt;
        color: Red;
        margin-top: 3px;
    }

    .BtnTextPlusMarginBottom {
        margin-bottom: 3px;
        margin-left: 8px;
    }

    .LinkButtonPlusMarginTop {
        margin-left: 8px;
        margin-top: 8px;
    }

    .SettingsLinkMarginTop {
        text-decoration: none;
        cursor: pointer;
        color: Blue;
        margin-left: 0px !important;
        margin-right: 6px;
        margin-top: 6px;
    }

    .style7 {
        height: 23px;
        width: 152px;
    }

    .style8 {
        width: 152px;
    }

    .marginleft0 {
        margin-left: 0px !important;
    }

    .width600 {
        width: 600px !important;
    }

    .marginright3 {
        margin-right: 3px !important;
    }
</style>
<div class="container-sec form">
    <div class="custom-grid">
        <h2 style="padding-bottom: 20px">
            <asp:Label ID="lblQuoteHeader" runat="server" Text="Edit Retail Information for Quote"></asp:Label>
            <asp:Button ID="btnGoBack" runat="server" Text="Return to Quotes"
                CssClass="pull-right btn btn-default btnquoterelease returntoQuote" TabIndex="12" OnClick="btnCancel_Click"
                ToolTip="Cancel unsaved changes; return to Quotes" />
        </h2>
        <div>
            <div class="col-sm-6 submit width600" style="padding-top: 20px;">
                <ul class="default-list">
                    <li>
                        <asp:Label ID="lblCustomerId" runat="server" CssClass="editRetailCustLabel" Text="Customer ID"></asp:Label>
                        <asp:TextBox ID="txtCustomerID" runat="server" Width="27%" MaxLength="12" TabIndex="13" CssClass="marginRight1px"></asp:TextBox>
                        <asp:Label ID="Label4" runat="server" CssClass="PopupInfolabel" Text="Ship-to"></asp:Label>
                        <asp:TextBox ID="txtShipto" runat="server" Width="24%" MaxLength="4" TabIndex="14"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label2" runat="server" CssClass="editRetailCustLabel" Text="Customer name"></asp:Label>
                        <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="30" TabIndex="15" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label3" runat="server" CssClass="editRetailCustLabel" Text="Address 1"></asp:Label>
                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="30" TabIndex="16" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label5" runat="server" CssClass="editRetailCustLabel" Text="Address 2"></asp:Label>
                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="30" TabIndex="17" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label6" runat="server" CssClass="editRetailCustLabel" Text="Address 3"></asp:Label>
                        <asp:TextBox ID="txtAddress3" runat="server" MaxLength="30" TabIndex="18" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label7" runat="server" CssClass="editRetailCustLabel" Text="City"></asp:Label>
                        <asp:TextBox ID="txtCity" runat="server" MaxLength="20" TabIndex="19" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label12" runat="server" CssClass="editRetailCustLabel" Text="State"></asp:Label>
                        <asp:TextBox ID="txtState" runat="server" Width="15%" MaxLength="2" TabIndex="20" CssClass="marginRight1px"></asp:TextBox>
                        <asp:Label ID="Label13" runat="server" CssClass="PopupInfolabel" Text="ZIP"></asp:Label>
                        <asp:TextBox ID="txtZip" runat="server" Width="40%" MaxLength="10" TabIndex="21"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label8" runat="server" CssClass="editRetailCustLabel" Text="Country"></asp:Label>
                        <asp:TextBox ID="txtCountry" runat="server" MaxLength="8" TabIndex="22" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label9" runat="server" CssClass="editRetailCustLabel" Text="Phone"></asp:Label>
                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="30" TabIndex="23" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label11" runat="server" CssClass="editRetailCustLabel" Text="Contact name"></asp:Label>
                        <asp:TextBox ID="txtContact" runat="server" MaxLength="40" TabIndex="24" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label10" runat="server" CssClass="editRetailCustLabel" Text="E-mail"></asp:Label>
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="100" TabIndex="25" CssClass="width62"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label1" runat="server" CssClass="editRetailCustLabel" Text="Reference"></asp:Label>
                        <asp:TextBox ID="txtReference" runat="server" MaxLength="40" TabIndex="26" CssClass="width62"></asp:TextBox>
                    </li>
                    <uc:OtherChargesAndTaxes ID="otherChargesAndTaxesRetailInfo" runat="server" TabIndex="27"/>
                    <li>
                        <asp:Button ID="btnSaveChanges" runat="server" Text="Save Changes"
                            CssClass="pull-right btn btn-primary returntoQuote marginright3" TabIndex="32" UseSubmitBehavior="False"
                            OnClick="btnSaveChanges_Click" ToolTip="Save changes in this section" />
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                
            </div>
            <br class="clear" />
            <hr />
        </div>
        <div style="float: left; padding-top: 7px;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:LinkButton runat="server" TabIndex="33" ID="btnChangeMarkup" CssClass="SettingsLink marginleft0" Text="Apply New Multiplier" EnableViewState="True" />
                        <div id="MultiplierModalDiv" runat="server">
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderMultiplier" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlMultiplierCustomView"
                                TargetControlID="btnChangeMarkup">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlMultiplierCustomView" runat="server" CssClass="modal-dialog"
                                Width="300px">
                                <div id="pnlDragMultiplier" runat="server" class="modal-content" style="width: 295px; text-align: left">
                                    <div class="modal-header" style="margin: 0px 15px; padding: 10px 0px;">
                                        <h4 class="modal-title" id="lbleditretail" runat="server">Edit Retail Price Multiplier</h4>
                                    </div>
                                    <div class="modal-body">
                                        <uc9:MultiplierSelector ID="MultiplierSelector" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblGridMessage" ForeColor="red" CssClass="BtnTextPlusMarginBottom" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="custom-grid" style="overflow-x: auto">
            <asp:Panel ID="QuoteDetailGridDiv" runat="server">
                <asp:GridView runat="server" ID="gvQuoteDetails" Width="99%"
                    AutoGenerateColumns="False" DataKeyNames="sequence,min_pak_disp"
                    OnRowCancelingEdit="gvQuoteDetails_RowCancelingEdit"
                    OnRowDataBound="gvQuoteDetails_RowDataBound"
                    OnRowEditing="gvQuoteDetails_RowEditing"
                    OnRowUpdating="gvQuoteDetails_RowUpdating"
                    OnDataBound="gvQuoteDetails_DataBound"
                    CssClass="custom-tabler">
                    <Columns>
                        <asp:CommandField ShowEditButton="true" ShowDeleteButton="false" HeaderText="Actions" DeleteImageUrl="~/Images/delete-icon.png" EditImageUrl="~/Images/edit-icon.png" CancelImageUrl="~/Images/Grid_Cancel.png" UpdateImageUrl="~/Images/Grid_UpdateSave.png" ButtonType="Image" UpdateText="Save" />
                        <asp:BoundField HeaderText="Sequence" DataField="sequence" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Item" DataField="ITEM" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Size" DataField="SIZE" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Description" DataField="quote_desc" HtmlEncode="false" ReadOnly="true" ItemStyle-CssClass="wrapItem">
                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            <ItemStyle Wrap="True" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapItem" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="WO Phrase/Quoted Tally" DataField="wo_phrase" HtmlEncode="false" ReadOnly="true" ItemStyle-CssClass="wrapItem">
                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            <ItemStyle Wrap="True" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapItem" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Location Reference" DataField="location_reference" HtmlEncode="false" ReadOnly="true" ItemStyle-CssClass="wrapItem">
                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            <ItemStyle Wrap="True" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapstyledecimal" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Order Qty" DataField="qty_ordered" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapstyledecimal" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Min Pack" DataField="min_pak_disp" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Previously Released" DataField="qty_released" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="UOM" DataField="uom" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Retail Price" DataField="retail_price" HtmlEncode="false" ReadOnly="true" DataFormatString="{0:C}">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapstyledecimal" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Retail UOM" DataField="retail_price_uom_code" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:CheckBoxField HeaderText="Retail Price Override" DataField="retail_override_price" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:CheckBoxField>
                        <asp:BoundField HeaderText="Retail Extension" DataField="retail_extended_price" HtmlEncode="false" ReadOnly="true" DataFormatString="{0:C}">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <p>
                    <asp:Label runat="server" ID="lblPriceDisclaimer" Text="* Prices are estimates and may change upon final submission." />
                </p>
            </asp:Panel>
        </div>
    </div>
</div>

