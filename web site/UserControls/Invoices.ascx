<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Invoices.ascx.cs" Inherits="UserControls_Invoices" %>
<%@ Register Src="ClientSideCalendar.ascx" TagName="ClientSideCalendar" TagPrefix="uc2" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc3" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="DocViewer.ascx" TagName="DocViewer" TagPrefix="uc4" %>
<style type="text/css">
    .PanelPadding {
        padding-left: 4px;
        padding-right: 4px;
    }

    .child-div {
        position:absolute;
        left:2%;
        right:0px;
    }
</style>
<div class="left-section">
    <div class="lft-cont-sec">
        <div id="my-tab-content">
            <div class="search-opt">
                <div class="advsearch">
                    <asp:Panel ID="InvoiceSearchFieldPanel" runat="server" DefaultButton="btnFind">
                        <asp:Label ID="Label3" runat="server" Text="Search by" CssClass="alladvsearchlabel"></asp:Label>
                        <asp:DropDownList ID="ddlTranID" runat="server" CssClass="select" OnSelectedIndexChanged="ddlTranID_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem>Invoice ID</asp:ListItem>
                            <asp:ListItem>PO ID</asp:ListItem>
                            <asp:ListItem>Reference #</asp:ListItem>
                            <asp:ListItem>Item #</asp:ListItem>
                            <asp:ListItem>Job #</asp:ListItem>
                            <asp:ListItem>Original Sales Order ID</asp:ListItem>
                            <asp:ListItem>Original Credit Memo ID</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSearchStyle" runat="server" CssClass="select">
                            <asp:ListItem>Equals</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtTranID" runat="server" CssClass="select"></asp:TextBox><br />
                    </asp:Panel>
                    <asp:Panel ID="SOAdvancedSearchDiv" runat="server" DefaultButton="btnFind">
                        <asp:Label ID="Label2" runat="server" Text="Paid status" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select">
                            <asp:ListItem Value="All">All</asp:ListItem>
                            <asp:ListItem>Open</asp:ListItem>
                            <asp:ListItem>Paid</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:Label ID="Label5" runat="server" Text="From invoice date" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="select"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="cefromdate" runat="server" TargetControlID="txtFromDate"
                            Format="MM/dd/yyyy" PopupButtonID="txtFromDate" Animated="true" CssClass="cal_Theme1" />
                        <br />
                        <asp:Label ID="Label6" runat="server" CssClass="alladvsearchlabel" Text="To invoice date"></asp:Label><br />
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="select"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="cetodate" runat="server" TargetControlID="txtToDate"
                            Format="MM/dd/yyyy" PopupButtonID="txtToDate" Animated="true" CssClass="cal_Theme1" />
                        <br />
                        <asp:Label ID="Label4" runat="server" Text="Show results by" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:DropDownList ID="ddlInvoiceShowResultsBy" runat="server" CssClass="select">
                            <asp:ListItem>Invoice</asp:ListItem>
                            <asp:ListItem>Item</asp:ListItem>
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:CheckBox ID="cbShipTo" runat="server" Text="Include all ship-tos" Checked="True" />
                    <br />
                    <asp:Button ID="btnFind" runat="server" CssClass="btn btn-primary" OnClick="btnFind_Click" Text="Search" />
                    <br />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right-section">
    <div class="container-sec">
        <h2>
            <asp:Label ID="lblInvoiceHeader" runat="server" Text="Invoices"></asp:Label>
        </h2>
        <div class="custom-grid abc">
            <div style="width: 100%">
                <asp:Label ID="lblInvoiceHeaderError" runat="server" Text="First 100 invoices returned. Add criteria to refine search."
                    Visible="False" CssClass="ErrorTextRed"></asp:Label>
            </div>
            <div style="float: left; width: 100%">
                <div class="optional" style="width: 280px !important;">
                    <asp:Label ID="lblheadershow" runat="server" Text="Show" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
                        DropShadow="true" PopupControlID="pnlInvoiceHeaderCustomView"
                        TargetControlID="lnkInvoiceHeaderCustomView">
                    </cc1:ModalPopupExtender>
                    <asp:DropDownList ID="ddlInvoiceHeaderRows" runat="server" AutoPostBack="true" Width="50px"
                        OnSelectedIndexChanged="ddlInvoiceHeaderRows_SelectedIndexChanged" CssClass="select opt-size">
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>75</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblheaderresult" Text="Rows" runat="server" />
                    <asp:LinkButton ID="lnkInvoiceHeaderCustomView" runat="server" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                </div>
            </div>
            <asp:Panel ID="Panel10" runat="server">
                <div class="custom-grid-overflow">
                    <asp:GridView ID="gvInvoiceHeaders" runat="server" AllowPaging="True"
                        Width="100%" OnPageIndexChanging="gvInvoiceHeader_PageIndexChanging" OnRowDataBound="gvInvoiceHeader_RowDataBound"
                        OnSelectedIndexChanged="gvInvoiceHeader_SelectedIndexChanged" AutoGenerateColumns="False"
                        DataKeyNames="ref_num" AllowSorting="True" OnRowCreated="gvInvoiceHeader_RowCreated"
                        OnSorting="gvInvoiceHeader_Sorting" PageSize="5" CssClass="grid-tabler">
                        <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <itemstyle width="5%" />
                                    <headerstyle width="5%" />
                                    <asp:ImageButton OnClick="Show_Hide_ChildGrid" CommandName="Details" runat="server" ID="imgbtnplusminus" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/Images/Grid_Plus.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="branch_id" HeaderText="Branch" ReadOnly="True" SortExpression="branch_id"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ref_num" HeaderText="Invoice ID" SortExpression="ref_num">
                                <ItemStyle Wrap="False" />
                                <HeaderStyle Wrap="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="so_id" HeaderText="Original Tran ID" ReadOnly="True" SortExpression="so_id"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TYPE" HeaderText="Original Tran Type" ReadOnly="True"
                                SortExpression="TYPE" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="job" HeaderText="Job #" ReadOnly="True" SortExpression="job"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="reference" HeaderText="Reference #" ReadOnly="True" SortExpression="reference">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cust_po" HeaderText="PO ID" ReadOnly="True" SortExpression="cust_po">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordered_by" HeaderText="Ordered By" ReadOnly="True" SortExpression="ordered_by"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_date" HeaderText="Order Date" ReadOnly="True" SortExpression="order_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_date" HeaderText="Ship Date" ReadOnly="True" SortExpression="ship_date"
                                Visible="False" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="invoice_date" HeaderText="Invoice Date" ReadOnly="True"
                                SortExpression="invoice_date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="due_date" HeaderText="Due Date" ReadOnly="True" SortExpression="due_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="route_id_char" HeaderText="Route" ReadOnly="True" SortExpression="route_id_char"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_via" HeaderText="Ship Via" ReadOnly="True" SortExpression="ship_via"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type" HeaderText="Sale Type" ReadOnly="True" SortExpression="sale_type"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type_desc" HeaderText="Sale Type Description" ReadOnly="True"
                                SortExpression="sale_type_desc">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="statusdescr" HeaderText="Paid Status" ReadOnly="True"
                                SortExpression="statusdescr">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="freight_terms_id" HeaderText="Freight Terms" ReadOnly="True"
                                SortExpression="freight_terms_id" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="pay_terms_code" HeaderText="Terms" ReadOnly="True" SortExpression="pay_terms_code"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_subtotal" HeaderText="Sub Total" ReadOnly="True"
                                SortExpression="order_subtotal" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_taxes" HeaderText="Total Taxes" ReadOnly="True"
                                SortExpression="order_taxes" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_charges" HeaderText="Total Charges" ReadOnly="True"
                                SortExpression="order_charges" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_total" HeaderText="Total" ReadOnly="True" SortExpression="order_total"
                                DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_balance" HeaderText="Balance" ReadOnly="True" SortExpression="order_balance"
                                Visible="False" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="terms_note" HeaderText="Terms Discount" ReadOnly="True"
                                SortExpression="terms_note" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_1" HeaderText="Sales Rep 1" ReadOnly="True" SortExpression="rep_1"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_2" HeaderText="Sales Rep 2" ReadOnly="True" SortExpression="rep_2"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_3" HeaderText="Sales Rep 3" ReadOnly="True" SortExpression="rep_3"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shiptoname" HeaderText="Ship-to" ReadOnly="True" SortExpression="shiptoname"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr1" HeaderText="Ship-to Address 1" ReadOnly="True"
                                SortExpression="shipToAddr1" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr2" HeaderText="Ship-to Address 2" ReadOnly="True"
                                SortExpression="shipToAddr2" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr3" HeaderText="Ship-to Address 3" ReadOnly="True"
                                SortExpression="shipToAddr3" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToCity" HeaderText="Ship-to City" ReadOnly="True"
                                SortExpression="shipToCity" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToState" HeaderText="Ship-to State" ReadOnly="True"
                                SortExpression="shipToState" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToZip" HeaderText="Ship-to ZIP" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <tr class="tr-innergrid child-div">
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px;">
                                            <div>
                                            </div>
                                            <asp:Panel runat="server" ID="innergrids" Visible="false" Style="background-color: White;" CssClass="panel-collapse show-panel acd collapse in">
                                                <div style="float: left; margin-top: 5px; padding-left: 40px" class="docgrid childgrid">
                                                    <uc4:DocViewer ID="docViewer" runat="server" />
                                                </div>
                                                <h3 style="padding-left: 40px; padding-top: 20px">
                                                    <asp:Label ID="lblInvoiceDetail" runat="server" Text=""></asp:Label>
                                                </h3>
                                                <div style="width: 100%; float: left; padding-left: 40px; margin-top: -10px">
                                                    <div class="optional" style="width: 280px !important;">
                                                        <asp:Label ID="lblInvoiceDetailRows" runat="server" Text="Show"></asp:Label>
                                                        <asp:DropDownList ID="ddlInvoiceDetailRows" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceDetailRows_SelectedIndexChanged"
                                                            Width="50px" CssClass="select opt-size">
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>10</asp:ListItem>
                                                            <asp:ListItem>15</asp:ListItem>
                                                            <asp:ListItem>20</asp:ListItem>
                                                            <asp:ListItem>25</asp:ListItem>
                                                            <asp:ListItem Value="1000">All</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lblinnerresult" runat="server" Text="Rows"></asp:Label>
                                                        <asp:LinkButton CommandArgument='<%#Container.DataItemIndex%>' ID="lnkInvoiceDetailCustomView" runat="server" OnClick="lnkInvoiceDetailCustomView_Click" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                                                        <asp:Label ID="lblInvoiceDetailError" runat="server" Text="Record limit has been exceeded. Add criteria to refine search."
                                                            Visible="False" CssClass="ErrorTextRed"></asp:Label>
                                                    </div>
                                                </div>
                                                <br />
                                                <div style="width: 100%; float: left">
                                                    <div class="childgrid">
                                                        <asp:GridView ID="gvInvoiceDetailInner" runat="server" PageSize="5" Width="100%" OnRowDataBound="gvInvoiceHeader_RowDataBound"
                                                            AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnRowCreated="gvInvoiceDetailInner_RowCreated"
                                                            OnSorting="gvInvoiceDetail_Sorting" DataKeyNames="ref_num" OnPageIndexChanging="gvInvoiceDetail_PageIndexChanging" CssClass="sub-grd-tabler">
                                                            <PagerStyle CssClass="PagerDetailGridView" HorizontalAlign="Left" Height="10px" />
                                                            <Columns>
                                                                <asp:BoundField DataField="display_seq" HeaderText="Seq #" ReadOnly="True" SortExpression="display_seq"
                                                                    Visible="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="so_desc" HeaderText="Description" ReadOnly="True" SortExpression="so_desc">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="qty_ordered" HeaderText="Ordered Qty" ReadOnly="True"
                                                                    SortExpression="qty_ordered" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="shipqty_order_uom" HeaderText="Shipped Qty" ReadOnly="True"
                                                                    SortExpression="shipqty_order_uom" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="uom" HeaderText="Qty UOM" ReadOnly="True" SortExpression="uom">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="net_price" HeaderText="Price" ReadOnly="True" SortExpression="net_price"
                                                                    DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="price_uom_code" HeaderText="Price UOM" ReadOnly="True"
                                                                    SortExpression="price_uom_code">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="extended_price" HeaderText="Extended Amount" ReadOnly="True"
                                                                    SortExpression="extended_price" DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr class="tr-innergrid">
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px"">
                                            <asp:Panel ID="innerplaceholder" runat="server" Visible="false" Style="width: 100%; background-color: White;" class="panel-collapse show-panel acd collapse in">
                                                <div>&nbsp;</div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvByItem" runat="server" PageSize="5" Width="100%" OnRowDataBound="gvInvoiceHeader_RowDataBound"
                        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnRowCreated="gvByItem_RowCreated"
                        OnSorting="gvByItem_Sorting" OnPageIndexChanging="gvByItem_PageIndexChanging" CssClass="grid-tabler">
                        <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                        <Columns>
                            <asp:BoundField DataField="branch_id" HeaderText="Branch" ReadOnly="True" SortExpression="branch_id"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ref_num" HeaderText="Invoice ID" ReadOnly="True" SortExpression="ref_num">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="so_desc" HeaderText="Description" ReadOnly="True" SortExpression="so_desc"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="qty_ordered" HeaderText="Ordered Qty" ReadOnly="True"
                                SortExpression="qty_ordered" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipqty_order_uom" HeaderText="Shipped Qty" ReadOnly="True"
                                SortExpression="shipqty_order_uom" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="uom" HeaderText="Qty UOM" ReadOnly="True" SortExpression="uom"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="net_price" HeaderText="Price" ReadOnly="True" SortExpression="net_price"
                                DataFormatString="{0:F2}" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="price_uom_code" HeaderText="Price UOM" ReadOnly="True"
                                SortExpression="price_uom_code" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="extended_price" HeaderText="Extended Amount" ReadOnly="True"
                                SortExpression="extended_price" DataFormatString="{0:F2}" HtmlEncode="False"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="job" HeaderText="Job #" ReadOnly="True" SortExpression="job"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="reference" HeaderText="Reference #" ReadOnly="True" SortExpression="reference">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cust_po" HeaderText="PO ID" ReadOnly="True" SortExpression="cust_po">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_date" HeaderText="Order Date" ReadOnly="True" SortExpression="order_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_date" HeaderText="Ship Date" ReadOnly="True" SortExpression="ship_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="invoice_date" HeaderText="Invoice Date" ReadOnly="True"
                                SortExpression="invoice_date" DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="route_id_char" HeaderText="Route" ReadOnly="True" SortExpression="route_id_char"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_via" HeaderText="Ship Via" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type" HeaderText="Sale Type" ReadOnly="True" SortExpression="sale_type"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type_desc" HeaderText="Sale Type Description" ReadOnly="True"
                                SortExpression="sale_type_desc">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="statusdescr" HeaderText="Status" ReadOnly="True" SortExpression="statusdescr">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_1" HeaderText="Sales Rep 1" ReadOnly="True" SortExpression="rep_1"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_2" HeaderText="Sales Rep 2" ReadOnly="True" SortExpression="rep_2"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_3" HeaderText="Sales Rep 3" ReadOnly="True" SortExpression="rep_3"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shiptoname" HeaderText="Ship-to" ReadOnly="True" SortExpression="shiptoname"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr1" HeaderText="Ship-to Address 1" ReadOnly="True"
                                SortExpression="shipToAddr1" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr2" HeaderText="Ship-to Address 2" ReadOnly="True"
                                SortExpression="shipToAddr2" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr3" HeaderText="Ship-to Address 3" ReadOnly="True"
                                SortExpression="shipToAddr3" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToCity" HeaderText="Ship-to City" ReadOnly="True"
                                SortExpression="shipToCity" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToState" HeaderText="Ship-to State" ReadOnly="True"
                                SortExpression="shipToState" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToZip" HeaderText="Ship-to ZIP" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px;">
                                            <div>
                                            </div>
                                            <asp:Panel runat="server" ID="innergrids" Visible="false" Style="background-color: White;" CssClass="panel-collapse show-panel acd collapse in">
                                                <asp:GridView ID="gvInvoiceDetailInner" runat="server" PageSize="5" Width="100%" OnRowDataBound="gvInvoiceHeader_RowDataBound"
                                                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnRowCreated="gvInvoiceDetailInner_RowCreated"
                                                    OnSorting="gvInvoiceDetail_Sorting" OnPageIndexChanging="gvInvoiceDetail_PageIndexChanging" CssClass="grid-tabler">
                                                    <Columns>
                                                        <asp:BoundField DataField="display_seq" HeaderText="Seq #" ReadOnly="True" SortExpression="display_seq"
                                                            Visible="False">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="so_desc" HeaderText="Description" ReadOnly="True" SortExpression="so_desc">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="qty_ordered" HeaderText="Ordered Qty" ReadOnly="True"
                                                            SortExpression="qty_ordered" HtmlEncode="False">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="shipqty_order_uom" HeaderText="Shipped Qty" ReadOnly="True"
                                                            SortExpression="shipqty_order_uom" HtmlEncode="False">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="uom" HeaderText="Qty UOM" ReadOnly="True" SortExpression="uom">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="net_price" HeaderText="Price" ReadOnly="True" SortExpression="net_price"
                                                            DataFormatString="{0:F2}" HtmlEncode="False">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="price_uom_code" HeaderText="Price UOM" ReadOnly="True"
                                                            SortExpression="price_uom_code">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="extended_price" HeaderText="Extended Amount" ReadOnly="True"
                                                            SortExpression="extended_price" DataFormatString="{0:F2}" HtmlEncode="False">
                                                            <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                            <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="pagination-sec" style="width: 320px">
                        <div class="info" style="width: 315px">
                            <asp:Label ID="lblpageno" runat="server" Text="" />
                        </div>
                    </div>
                </div>
                <br />
                <asp:Panel ID="pnlSaleOrderDetail" runat="server">
                    <div>
                        <div class="optional" style="display: none">
                            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlInvoiceDetailCustomView"
                                TargetControlID="lnkInvoiceDetailCustomView">
                            </cc1:ModalPopupExtender>
                            <asp:LinkButton ID="lnkInvoiceDetailCustomView" runat="server" CssClass="SettingsLink">Select Columns</asp:LinkButton>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </div>
    </div>
</div>
<asp:Panel ID="pnlInvoiceDetailCustomView" runat="server">
    <div id="pnlInvoiceDetailDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View</h4>
            </div>
            <div class="modal-body">
                <uc3:ColumnSelector ID="csCustomDetail" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlInvoiceHeaderCustomView" runat="server">
    <div id="pnlInvoiceHeaderDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View</h4>
            </div>
            <div class="modal-body">
                <uc3:ColumnSelector ID="csCustomHeader" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>


