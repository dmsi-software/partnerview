using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControls_ClientSideCalendar : System.Web.UI.UserControl
{
    public DateTime InitialValue
    {
        set
        {
            try
            {
                if (value != null)
                    this.TextBox1.Text = value.ToShortDateString();
                else
                    this.TextBox1.Text = DateTime.Now.ToShortDateString();
            }
            catch
            {
                this.TextBox1.Text = "";
            }
        }
    }

    public string ShortDateString
    {
        get 
        {
            if (this.TextBox1.Text.Length == 0)
                return DateTime.Now.ToShortDateString();
            else
            {
                try
                {
                    string[] parts = this.TextBox1.Text.Split(new char[] {'/'});
                    if (parts.Length == 3)
                    {
                        int year = Convert.ToInt32(parts[2]);
                        int month = Convert.ToInt32(parts[0]);
                        int day = Convert.ToInt32(parts[1]);

                        DateTime date = new DateTime(year, month, day);
                        return date.ToShortDateString();
                    }
                    else
                        return DateTime.Now.ToShortDateString();
                }
                catch
                {
                    return DateTime.Now.ToShortDateString();
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
