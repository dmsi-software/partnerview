﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Collections.Generic;
using Infragistics.Web.UI.GridControls;
using Infragistics.Web.UI.NavigationControls;
using System.Web.UI.HtmlControls;

public partial class UserControls_QuoteEdit : System.Web.UI.UserControl
{
    public delegate void QuoteEditModeDelegate();
    public event QuoteEditModeDelegate OnQuoteEditModeChanged;
    int configureexist = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddInfoQuoteEditSelector.OnOKClicked += new UserControls_AddInfoQuoteEditSelector.OKClickedDelegate(AddInfoQuoteEditSelector_OnOKClicked);
        this.AddInfoQuoteEditSelector.OnCancelClicked += new UserControls_AddInfoQuoteEditSelector.CancelClickedDelegate(AddInfoQuoteEditSelector_OnCancelClicked);
        this.MultiplierSelector.OnOKClicked += new UserControls_MultiplierSelector.OKClickedDelegate(MultiplierSelector_OnOKClicked);

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            AddInfoModalDiv.Visible = true;
            this.lnkAddInfo.Visible = true;
            this.lblCustInfo.Visible = true;
        }
        else
        {
            AddInfoModalDiv.Visible = false;
            this.lnkAddInfo.Visible = false;
            this.lblCustInfo.Visible = false;
        }

        lblGridMessage.Text = ""; //reset the message line...

        if (Session["disable_edit_shipping_address"] != null && (bool)Session["disable_edit_shipping_address"] == true)
        {
            lblShiptoQuoteEdit.Visible = false;
            lnkShiptoQuoteEdit.Visible = false;
            ShiptoQuoteEditModalDiv.Visible = false;
            cbxShipComplete.CssClass = "editcheckbox1 editcheckrelease";
        }
        else
        {
            lblShiptoQuoteEdit.Visible = true;
            lnkShiptoQuoteEdit.Visible = true;
            ShiptoQuoteEditModalDiv.Visible = true;
            cbxShipComplete.CssClass = "editcheckbox1 editcheckrelease TopMargin10";
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        //Handle Edit allocation first...
        bool editAllocation = true;
        bool editNetPriceAllocation = false; //this is an inverted disallow action allocation

        if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
        {
            DataRow[] rows = dsPV.bPV_Action.Select("token_code='edit_quotes'");

            if (rows.Length == 0) //no allocation
            {
                editAllocation = false;
            }

            rows = dsPV.bPV_Action.Select("token_code='edit_quotes_disallow_net_price_edit'");

            if (rows.Length == 0) //no allocation record, but it means we can edit net price because it's an inverted disallow-type action allocation
            {
                editNetPriceAllocation = true;
            }
        }

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            FindGVBoundField(gvQuoteDetails, "net_price").Visible = false;
            FindGVBoundField(gvQuoteDetails, "override_price").Visible = false;
            FindGVBoundField(gvQuoteDetails, "price_uom_code").Visible = false;
            FindGVBoundField(gvQuoteDetails, "extended_price").Visible = false;

            if (Session["IsQuoteInUpdateMode"] == null || (bool)Session["IsQuoteInUpdateMode"] == false)
                FindGVBoundField(gvQuoteDetails, "retail_price").Visible = true;

            FindGVBoundField(gvQuoteDetails, "retail_override_price").Visible = true;
            FindGVBoundField(gvQuoteDetails, "retail_price_uom_code").Visible = true;

            if (Session["IsQuoteInUpdateMode"] == null || (bool)Session["IsQuoteInUpdateMode"] == false)
                FindGVBoundField(gvQuoteDetails, "retail_extended_price").Visible = true;

            if (Session["IsQuoteInUpdateMode"] != null && (bool)Session["IsQuoteInUpdateMode"] == true && Session["UserPrefChangedInMiddleOfQuoteEdit"] != null && (bool)Session["UserPrefChangedInMiddleOfQuoteEdit"] == true)
            {
                Session["UserPrefChangedInMiddleOfQuoteEdit"] = false;

                FindGVBoundField(gvQuoteDetails, "net_price").Visible = false;
                FindGVBoundField(gvQuoteDetails, "override_price").Visible = false;
                FindGVBoundField(gvQuoteDetails, "price_uom_code").Visible = false;
                FindGVBoundField(gvQuoteDetails, "extended_price").Visible = false;

                FindGVBoundField(gvQuoteDetails, "retail_price").Visible = false;
                FindGVBoundField(gvQuoteDetails, "retail_priceedit").Visible = true;
                FindGVBoundField(gvQuoteDetails, "qty_orderededit").Visible = true;
                FindGVBoundField(gvQuoteDetails, "retail_override_price").Visible = true;
                FindGVBoundField(gvQuoteDetails, "retail_price_uom_code").Visible = true;
                FindGVBoundField(gvQuoteDetails, "retail_extended_price").Visible = false;
            }
        }
        else
        {
            if (Session["IsQuoteInUpdateMode"] == null || (bool)Session["IsQuoteInUpdateMode"] == false)
                FindGVBoundField(gvQuoteDetails, "net_price").Visible = true;

            FindGVBoundField(gvQuoteDetails, "override_price").Visible = true;
            FindGVBoundField(gvQuoteDetails, "price_uom_code").Visible = true;

            if (Session["IsQuoteInUpdateMode"] == null || (bool)Session["IsQuoteInUpdateMode"] == false)
                FindGVBoundField(gvQuoteDetails, "extended_price").Visible = true;

            FindGVBoundField(gvQuoteDetails, "retail_price").Visible = false;
            FindGVBoundField(gvQuoteDetails, "retail_override_price").Visible = false;
            FindGVBoundField(gvQuoteDetails, "retail_price_uom_code").Visible = false;
            FindGVBoundField(gvQuoteDetails, "retail_extended_price").Visible = false;

            if (Session["IsQuoteInUpdateMode"] != null && (bool)Session["IsQuoteInUpdateMode"] == true && Session["UserPrefChangedInMiddleOfQuoteEdit"] != null && (bool)Session["UserPrefChangedInMiddleOfQuoteEdit"] == true)
            {
                Session["UserPrefChangedInMiddleOfQuoteEdit"] = false;

                FindGVBoundField(gvQuoteDetails, "retail_price").Visible = false;
                FindGVBoundField(gvQuoteDetails, "retail_override_price").Visible = false;
                FindGVBoundField(gvQuoteDetails, "retail_price_uom_code").Visible = false;
                FindGVBoundField(gvQuoteDetails, "retail_extended_price").Visible = false;

                FindGVBoundField(gvQuoteDetails, "override_price").Visible = true;
                FindGVBoundField(gvQuoteDetails, "price_uom_code").Visible = true;
                FindGVBoundField(gvQuoteDetails, "extended_price").Visible = false;
            }
        }

        dsQuoteDataSet dsQuote = new dsQuoteDataSet();
        dsRetailAddressDataSet dsRetailAddress;

        if (Session["QuoteEdit_dsQuote"] != null)
            dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];

        if (dsQuote != null)
        {
            UserControls_ShiptoQuoteEditSelector selector = (UserControls_ShiptoQuoteEditSelector)FindControl("ShiptoQuoteEditSelector1");
            selector.QuoteDataSet = dsQuote;
        }

        if (Session["UIHasChangedAddInfoEdit"] == null && Session["UIHasCanceledAddInfoEdit"] == null)
        {
            if (Session["dsRetailAddressForQuotes"] != null && dsQuote.pvttquote_header.Rows.Count > 0)
            {
                dsRetailAddress = (dsRetailAddressDataSet)Session["dsRetailAddressForQuotes"];
                DataRow[] rows = dsRetailAddress.tt_retail_address.Select("tran_id=" + dsQuote.pvttquote_header[0]["quote_id"].ToString());

                if (rows.Length > 0)
                {
                    Session["AddInfoQuoteEdit_txtCustomerId"] = rows[0]["misc_1"].ToString();
                    Session["AddInfoQuoteEdit_txtShipTo"] = rows[0]["misc_2"].ToString();
                    Session["AddInfoQuoteEdit_txtCustomerName"] = rows[0]["cust_name"].ToString();
                    Session["AddInfoQuoteEdit_txtAddress1"] = rows[0]["address_1"].ToString();
                    Session["AddInfoQuoteEdit_txtAddress2"] = rows[0]["address_2"].ToString();
                    Session["AddInfoQuoteEdit_txtAddress3"] = rows[0]["address_3"].ToString();
                    Session["AddInfoQuoteEdit_txtCity"] = rows[0]["city"].ToString();
                    Session["AddInfoQuoteEdit_txtState"] = rows[0]["state"].ToString();
                    Session["AddInfoQuoteEdit_txtZip"] = rows[0]["zip"].ToString();
                    Session["AddInfoQuoteEdit_txtCountry"] = rows[0]["country"].ToString();
                    Session["AddInfoQuoteEdit_txtPhone"] = rows[0]["phone"].ToString();
                    Session["AddInfoQuoteEdit_txtContactName"] = rows[0]["misc_3"].ToString();
                    Session["AddInfoQuoteEdit_txtEmail"] = rows[0]["email"].ToString();
                    Session["AddInfoQuoteEdit_txtReference"] = rows[0]["misc_4"].ToString();

                    Session["AddInfoQuoteEdit_taxRate"] = Convert.ToDecimal(rows[0]["misc_5"].ToString() == "" ? GetDefaultTaxRate().ToString() : rows[0]["misc_5"].ToString());
                    Session["AddInfoQuoteEdit_taxableDescription"] = rows[0]["misc_6"].ToString();
                    Session["AddInfoQuoteEdit_taxableAmount"] = Convert.ToDecimal(rows[0]["misc_7"].ToString() == "" ? "0.00" : rows[0]["misc_7"].ToString());
                    Session["AddInfoQuoteEdit_nonTaxableDescription"] = rows[0]["misc_8"].ToString();
                    Session["AddInfoQuoteEdit_nonTaxableAmount"] = Convert.ToDecimal(rows[0]["misc_9"].ToString() == "" ? "0.00" : rows[0]["misc_9"].ToString());
                }
            }
        }

        decimal quoteRetailTotal = 0;

        if (dsQuote.pvttquote_header[0]["retail_order_total"] != DBNull.Value)
            quoteRetailTotal = Convert.ToDecimal(dsQuote.pvttquote_header[0]["retail_order_total"]);

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            AddInfoModalDiv.Visible = true;
            this.lnkAddInfo.Visible = true;
            this.lblCustInfo.Visible = true;
        }
        else
        {
            AddInfoModalDiv.Visible = false;
            this.lnkAddInfo.Visible = false;
            this.lblCustInfo.Visible = false;
        }

        if (editAllocation == false || editNetPriceAllocation == false)
        {
            if (Session["UserPref_DisplaySetting"] != null && Convert.ToString(Session["UserPref_DisplaySetting"]) == "Net price")
            {
                FindGVBoundField(gvQuoteDetails, "net_price").ReadOnly = true;

                btnChangeMarkup.Visible = false;
                MultiplierModalDiv.Visible = false;
            }
            else
            {
                if (Session["SaveQuoteEditHasErrors"] == null || (bool)Session["SaveQuoteEditHasErrors"] == false)
                {
                    btnChangeMarkup.Visible = true;
                    MultiplierModalDiv.Visible = true;
                }
            }
        }
        else
        {
            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                if (Session["SaveQuoteEditHasErrors"] == null || (bool)Session["SaveQuoteEditHasErrors"] == false)
                {
                    btnChangeMarkup.Visible = true;
                    MultiplierModalDiv.Visible = true;
                }
            }
            else
            {
                btnChangeMarkup.Visible = false;
                MultiplierModalDiv.Visible = false;
            }
        }

        DataRow[] dr = dsQuote.Tables["pvttquote_detail"].Select("orig_source = 'pv'");

        if (dr.Length > 0)
        {
            configureexist = 1;
        }

        gvQuoteDetails.DataSource = dsQuote;
        gvQuoteDetails.DataMember = "pvttquote_detail";
        gvQuoteDetails.DataBind();

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormatSingle();", true);
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme1", "ComplementoryPopupHeight();", true);
        gvQuoteDetails.Visible = true;

        //Change modal popup title bar text, based on mode; Project: 94713 - Pricing in PartnerView - Quotes
        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price")
        {
            this.lbleditretail.InnerText = " Edit retail price multiplier";

            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                lbleditretail.InnerText = lbleditretail.InnerText.Replace("multiplier", "margin");
                btnChangeMarkup.Text = "Apply New Margin";
            }
            else
                btnChangeMarkup.Text = "Apply New Multiplier";
        }
        else if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost")
        {
            this.lbleditretail.InnerText = " Edit retail price – markup on cost multiplier";
            if (Session["MarkupSetting"] != null && (string)Session["MarkupSetting"] == "Margin")
            {
                lbleditretail.InnerText = lbleditretail.InnerText.Replace("multiplier", "margin");
                btnChangeMarkup.Text = "Apply New Margin on Cost";
            }
            else
                btnChangeMarkup.Text = "Apply New Multiplier on Cost";
        }
        else if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
        {
            this.lbleditretail.InnerText = " Edit retail price – discount from list multiplier";
            btnChangeMarkup.Text = "Apply New Multiplier on List Price";
        }
        else
            btnChangeMarkup.Text = "Apply New Multiplier"; //for Net mode

        //Project: 96293 - PV - Footnote missing in Edit Quote screen
        if (Session["UserPref_DisplaySetting"] != null &&
            ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            lblPriceDisclaimer.Visible = false;
        }
        else
            lblPriceDisclaimer.Visible = true;
    }

    void MultiplierSelector_OnOKClicked(decimal markupFactor)
    {
        Quotes q = new Quotes();

        /* 114521 - Users are updating the price in Agility while PV quote remains open.  This causes data in PV to become stale.
            Situation is remedied by updating data prior to applying multiplier.
        */
        FetchUpdatedCurrentQuoteDS((dsQuoteDataSet)Session["QuoteEdit_dsQuote"]);

        dsQuoteDataSet dsq = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];

        string returnMessage = q.ApplyMultiplier(markupFactor, ref dsq);

        if (returnMessage == "0")
        {
            Session["QuoteEdit_dsQuote"] = null;
            dsq.AcceptChanges();
            Session["QuoteEdit_dsQuote"] = dsq;
            gvQuoteDetails.DataSource = null;

            dsRetailAddressDataSet dsRetailAddress = PopulateDSRetailAddress(dsq);

            Dmsi.Agility.Data.Quotes quote = new Dmsi.Agility.Data.Quotes();
            string MessageText = quote.UpdateQuote(dsq, dsRetailAddress);

            if (MessageText == "0")
            {
                FetchUpdatedCurrentQuoteDS(dsq);
            }
            else
            {
                if (MessageText.Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                EventLog log = new EventLog();
                log.LogError(EventTypeFlag.AppServerDBerror, MessageText);
                throw new SystemException("Error occurred during Quote update save..." + MessageText);
            }
        }
        else
        {
            if (returnMessage.Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            lblGridMessage.Text = "<b>Error:</b> Applying the new multiplier has failed. Please try again.&nbsp;&nbsp;";
        }
    }
    protected decimal GetDefaultTaxRate()
    {
        return Convert.ToDecimal(Profile.DefaultTaxRate == "" ? "0" : Profile.DefaultTaxRate);
    }
    void AddInfoQuoteEditSelector_OnOKClicked()
    {
        Session["UIHasChangedAddInfoEdit"] = true;
    }

    void AddInfoQuoteEditSelector_OnCancelClicked()
    {
        Session["UIHasCanceledAddInfoEdit"] = true;
    }

    public void Initialize(dsQuoteDataSet dsQuote)
    {
        Session["ShipViaHasBeenDisabledDueToDataCondition"] = false;

        lblGridMessage.Text = ""; //reset the message line...

        ddlShipVia.Enabled = true;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        OrderCart orderCart = null;
        if (dm.GetCache("OrderCart") != null)
            orderCart = (OrderCart)dm.GetCache("OrderCart");
        DataSet cartDS = orderCart.ReferencedDataSet;

        ddlShipVia.Items.Clear();
        foreach (DataRowView row in cartDS.Tables["ttship_via"].DefaultView)
        {
            ListItem li = new ListItem();
            li.Text = ((string)row["ship_via"]).ToUpper();
            li.Value = ((string)row["ship_via"]).ToUpper();
            ddlShipVia.Items.Add(li);
        }

        ListItem lisearch = new ListItem();
        lisearch.Text = dsQuote.pvttquote_header.Rows[0]["ship_via"].ToString().ToUpper();
        lisearch.Value = dsQuote.pvttquote_header.Rows[0]["ship_via"].ToString().ToUpper();
        if (ddlShipVia.Items.Contains(lisearch))
        {
            for (int x = 0; x < ddlShipVia.Items.Count; x++)
            {
                if (ddlShipVia.Items[x].Value == dsQuote.pvttquote_header.Rows[0]["ship_via"].ToString().ToUpper())
                {
                    ddlShipVia.SelectedIndex = x;
                    break;
                }
            }
        }
        else
        {
            ddlShipVia.SelectedIndex = 0;
        }

        //handle Ship Complete; setting the check box from current header value...
        bool shipcomplete = Convert.ToBoolean(dsQuote.pvttquote_header.Rows[0]["ship_complete"].ToString());
        if (shipcomplete)
            cbxShipComplete.Checked = true;
        else
            cbxShipComplete.Checked = false;

        lblQuoteHeader.Text = "Edit Quote " + dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();

        Session["QuoteEdit_dsQuote"] = null;

        txtRequestedDate.Text = "";

        if (dsQuote.pvttquote_header.Rows[0]["quoted_for"].ToString().Length > 12)
            txtOrderedBy.Text = dsQuote.pvttquote_header.Rows[0]["quoted_for"].ToString().Substring(0, 12);
        else
            txtOrderedBy.Text = dsQuote.pvttquote_header.Rows[0]["quoted_for"].ToString();

        txtJob.Text = dsQuote.pvttquote_header.Rows[0]["job"].ToString();
        txtReference.Text = dsQuote.pvttquote_header.Rows[0]["reference"].ToString();
        txtCustomerPO.Text = dsQuote.pvttquote_header.Rows[0]["cust_po"].ToString();
        txtMessage.Text = dsQuote.pvttquote_header.Rows[0]["quote_message"].ToString();
        txtOrderType.Text = dsQuote.pvttquote_header.Rows[0]["sale_type_desc"].ToString();

        string tempTxtRequestedDate;

        //if date is overridden display the date, otherwise blank the date.
        if (Convert.ToBoolean(dsQuote.pvttquote_header.Rows[0]["expect_date_override"].ToString().ToLower()) == true)
        {
            tempTxtRequestedDate = dsQuote.pvttquote_header.Rows[0]["expect_date"].ToString().Substring(0, dsQuote.pvttquote_header.Rows[0]["expect_date"].ToString().IndexOf(" "));
        }
        else
        {
            tempTxtRequestedDate = "";
        }

        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            DataRow shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
            string inputCustKey = shiptoRow["cust_key"].ToString();
            int inputShipto = Convert.ToInt32(shiptoRow["seq_num"].ToString());
            string inputSaleType = dsQuote.pvttquote_header.Rows[0]["sale_type"].ToString();

            if (Session["LastEditQuoteFetched"] == null)
                Session["LastEditQuoteFetched"] = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
            else if (dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString() != (string)Session["LastEditQuoteFetched"])
            {
                Session["LastEditQuoteFetched"] = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
            }

            List<string> dateStrings = new List<string>();

            if (dm.GetCache("use_route_dates") != null && (string)dm.GetCache("use_route_dates") == "Select date from available routes")
            {
                int ReturnCode = 0;
                string MessageText;

                Dmsi.Agility.Data.Customers custs = new Customers(out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                dateStrings = custs.FetchDeliveryList(inputCustKey, inputShipto, inputSaleType, true, out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }
            }

            if (dateStrings.Count > 0)
            {
                int foundIndex = 0;

                if (tempTxtRequestedDate.Length > 0)
                {
                    for (int x = 0; x < dateStrings.Count; x++)
                    {
                        if (tempTxtRequestedDate == dateStrings[x])
                        {
                            foundIndex = x;
                            break;
                        }
                    }

                    if (foundIndex == 0)
                    {
                        dateStrings.Insert(0, tempTxtRequestedDate);
                    }
                }

                ddlRequestedDate.Visible = true;
                txtRequestedDate.Visible = false;
                calRequestedDate.Enabled = false;

                ddlRequestedDate.DataSource = dateStrings;
                ddlRequestedDate.DataBind();

                if (foundIndex > 0)
                {
                    ddlRequestedDate.SelectedIndex = foundIndex;
                }
                else
                {
                    if (ddlRequestedDate.Items.Count > 0)
                        ddlRequestedDate.SelectedIndex = 0;
                }
            }
            else
            {
                ddlRequestedDate.Visible = false;
                txtRequestedDate.Visible = true;

                if (tempTxtRequestedDate.Length > 0)
                    txtRequestedDate.Text = tempTxtRequestedDate;

                calRequestedDate.Enabled = true;
                ddlRequestedDate.DataSource = null;
                ddlRequestedDate.DataBind();
            }
        }

        //always do this last...
        Session["QuoteEdit_dsQuote"] = dsQuote;

        //TODO: clear out Tmp_RetailAddress Session variables...
        Session["Tmp_AddInfoQuoteEdit_txtCustomerId"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtShipTo"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtCustomerName"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtAddress1"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtAddress2"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtAddress3"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtCity"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtState"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtZip"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtCountry"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtPhone"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtContactName"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtEmail"] = null;
        Session["Tmp_AddInfoQuoteEdit_txtReference"] = null;
        Session["Tmp_AddInfoQuoteEdit_taxRate"] = null;
        Session["Tmp_AddInfoQuoteEdit_taxableDescription"] = null;
        Session["Tmp_AddInfoQuoteEdit_taxableAmount"] = null;
        Session["Tmp_AddInfoQuoteEdit_nonTaxableDescription"] = null;
        Session["Tmp_AddInfoQuoteEdit_nonTaxableAmount"] = null;
    }

    public void btnCancel_Click(object sender, EventArgs e)
    {
        UserControl quotes = Parent.FindControl("Quotes1") as UserControl;
        if (quotes != null)
        {
            quotes.Visible = true;
            this.Visible = false;
            Session["QuoteEditVisible"] = false;
        }

        lblRequestedDateError.Visible = false;
        lblPOError.Visible = false;

        Session["IsQuoteInUpdateMode"] = false;
        Session["SaveQuoteEditHasErrors"] = false;

        btnChangeMarkup.Enabled = true;
        ddlRequestedDate.Enabled = true;
        ddlShipVia.Enabled = true;
        lnkAddInfo.Enabled = true;

        lblGridMessage.Text = ""; //reset the message line...

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }

        RetainUserPreferences();
        Session["CachedMultiplier"] = null;

        MenuEnableDisable menuEnable = new MenuEnableDisable();
        menuEnable.EnableMenuItems(this.Page, this.Parent);

        Session["ShiptoQuoteEdit_txtCustomerName"] = null;
    }

    public void UpdateQuoteAfterVisualCafeSave()
    {
        if (Session["QuoteEdit_dsQuote"] != null)
        {
            dsQuoteDataSet dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];

            dsRetailAddressDataSet dsRetailAddress = PopulateDSRetailAddress(dsQuote);

            FetchUpdatedCurrentQuoteDS(dsQuote);
        }
    }

    /// <summary>
    /// for making user preferences link hide or visible
    /// </summary>
    private void RetainUserPreferences()
    {
        if (Session["AllowRetailPricing"] != null)
        {
            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                hypUserPreferences.Visible = true;
                this.Page.Master.FindControl("OptionsModalDiv").Visible = true;

                if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                {
                    Page.Master.FindControl("liNetRetail").Visible = true;
                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

                    if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
                    }
                    else
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
                    }
                }
            }
            else
            {
                HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                hypUserPreferences.Visible = false;

                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
            }
        }

        if (Session["UIisOnInventoryDetail"] != null && (bool)Session["UIisOnInventoryDetail"] == true)
        {
            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
                    HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                    hpl.Visible = false;

                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                }
            }
            else
            {
                HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                hpl.Visible = false;

                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
            }
        }
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        bool ok = true;

        if (Page.Request.Url.ToString().Contains("bc.com"))
        {
            if (txtCustomerPO.Text.Trim().Length == 0)
            {
                ok = false;
                lblPOError.Visible = true;
            }
            else
            {
                lblPOError.Visible = false;
            }
        }

        RetainUserPreferences();

        if (ddlRequestedDate.Visible)
        {
            if (ddlRequestedDate.SelectedValue.Length > 0 && ddlRequestedDate.SelectedValue != "Next available")
            {
                try
                {
                    DateTime requestedDate = DateTime.Parse(ddlRequestedDate.SelectedValue);

                    if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                    {
                        ok = false;
                        lblRequestedDateError.Visible = true;
                    }
                    else
                        lblRequestedDateError.Visible = false;
                }
                catch
                {
                    ok = false;
                    lblRequestedDateError.Visible = true;
                }
            }
        }
        else
        {
            if (txtRequestedDate.Text.Trim() != "")
            {
                try
                {
                    DateTime requestedDate = DateTime.Parse(txtRequestedDate.Text);

                    if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                    {
                        ok = false;
                        lblRequestedDateError.Visible = true;
                    }
                    else
                        lblRequestedDateError.Visible = false;
                }
                catch
                {
                    ok = false;
                    lblRequestedDateError.Visible = true;
                }
            }
        }

        if (ok)
        {
            dsQuoteDataSet dsQuote = new dsQuoteDataSet();

            if (Session["QuoteEdit_dsQuote"] != null)
            {
                FetchUpdatedCurrentQuoteDS((dsQuoteDataSet)Session["QuoteEdit_dsQuote"]);
                dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
            }

            dsQuote.pvttquote_header.Rows[0]["quoted_for"] = txtOrderedBy.Text;
            dsQuote.pvttquote_header.Rows[0]["job"] = txtJob.Text;
            dsQuote.pvttquote_header.Rows[0]["reference"] = txtReference.Text;
            dsQuote.pvttquote_header.Rows[0]["cust_po"] = txtCustomerPO.Text;
            dsQuote.pvttquote_header.Rows[0]["ship_via"] = ddlShipVia.SelectedValue;
            dsQuote.pvttquote_header.Rows[0]["quote_message"] = txtMessage.Text;
            dsQuote.pvttquote_header.Rows[0]["ship_complete"] = cbxShipComplete.Checked;

            //MDM - Added as Ship-to overrides from popup control
            if (Session["ShiptoQuoteEdit_txtCustomerName"] != null)
            {
                dsQuote.pvttquote_header.Rows[0]["shiptoname"] = ((string)Session["ShiptoQuoteEdit_txtCustomerName"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToAddr1"] = ((string)Session["ShiptoQuoteEdit_txtAddress1"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToAddr2"] = ((string)Session["ShiptoQuoteEdit_txtAddress2"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToAddr3"] = ((string)Session["ShiptoQuoteEdit_txtAddress3"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToCity"] = ((string)Session["ShiptoQuoteEdit_txtCity"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToState"] = ((string)Session["ShiptoQuoteEdit_txtState"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToZip"] = ((string)Session["ShiptoQuoteEdit_txtZip"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToCountry"] = ((string)Session["ShiptoQuoteEdit_txtCountry"]).Trim();
                dsQuote.pvttquote_header.Rows[0]["shipToPhone"] = ((string)Session["ShiptoQuoteEdit_txtPhone"]).Trim();
            }

            if (txtRequestedDate.Visible)
            {
                if (txtRequestedDate.Text.Length > 0)
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DateTime.Parse(txtRequestedDate.Text);
                    dsQuote.pvttquote_header[0]["expect_date_override"] = true;
                }
                else
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DBNull.Value;
                    dsQuote.pvttquote_header[0]["expect_date_override"] = false;
                }
            }
            else
            {
                if (ddlRequestedDate.SelectedValue.Length > 0 && ddlRequestedDate.SelectedValue != "Next available")
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DateTime.Parse(ddlRequestedDate.SelectedValue);
                    dsQuote.pvttquote_header[0]["expect_date_override"] = true;
                }
                else
                {
                    dsQuote.pvttquote_header[0]["expect_date"] = DBNull.Value;
                    dsQuote.pvttquote_header[0]["expect_date_override"] = false;
                }
            }

            if (Session["IsQuoteInUpdateMode"] != null && (bool)Session["IsQuoteInUpdateMode"] == true)
            {
                lblGridMessage.Text = ""; //reset the message line...
            }

            //if ok to here...then continue...
            if (ok)
            {
                Session["IsQuoteInUpdateMode"] = false;

                btnChangeMarkup.Enabled = true;
                ddlRequestedDate.Enabled = true;

                //If data conditions have disabled ShipVia control then we must not re-enable it.
                if (Session["ShipViaHasBeenDisabledDueToDataCondition"] != null && (bool)Session["ShipViaHasBeenDisabledDueToDataCondition"] == false)
                    ddlShipVia.Enabled = true;

                lnkAddInfo.Enabled = true;

                Session["CachedMultiplier"] = null;

                btnChangeMarkup.Enabled = true;

                dsQuote.AcceptChanges();

                if (Session["Tmp_AddInfoQuoteEdit_txtCustomerId"] != null)
                {
                    Session["AddInfoQuoteEdit_txtCustomerId"] = Session["Tmp_AddInfoQuoteEdit_txtCustomerId"];
                    Session["AddInfoQuoteEdit_txtShipTo"] = Session["Tmp_AddInfoQuoteEdit_txtShipTo"];
                    Session["AddInfoQuoteEdit_txtCustomerName"] = Session["Tmp_AddInfoQuoteEdit_txtCustomerName"];
                    Session["AddInfoQuoteEdit_txtAddress1"] = Session["Tmp_AddInfoQuoteEdit_txtAddress1"];
                    Session["AddInfoQuoteEdit_txtAddress2"] = Session["Tmp_AddInfoQuoteEdit_txtAddress2"];
                    Session["AddInfoQuoteEdit_txtAddress3"] = Session["Tmp_AddInfoQuoteEdit_txtAddress3"];
                    Session["AddInfoQuoteEdit_txtCity"] = Session["Tmp_AddInfoQuoteEdit_txtCity"];
                    Session["AddInfoQuoteEdit_txtState"] = Session["Tmp_AddInfoQuoteEdit_txtState"];
                    Session["AddInfoQuoteEdit_txtZip"] = Session["Tmp_AddInfoQuoteEdit_txtZip"];
                    Session["AddInfoQuoteEdit_txtCountry"] = Session["Tmp_AddInfoQuoteEdit_txtCountry"];
                    Session["AddInfoQuoteEdit_txtPhone"] = Session["Tmp_AddInfoQuoteEdit_txtPhone"];
                    Session["AddInfoQuoteEdit_txtContactName"] = Session["Tmp_AddInfoQuoteEdit_txtContactName"];
                    Session["AddInfoQuoteEdit_txtEmail"] = Session["Tmp_AddInfoQuoteEdit_txtEmail"];
                    Session["AddInfoQuoteEdit_txtReference"] = Session["Tmp_AddInfoQuoteEdit_txtReference"];
                    Session["AddInfoQuoteEdit_taxRate"] = Session["Tmp_AddInfoQuoteEdit_taxRate"];
                    Session["AddInfoQuoteEdit_taxableDescription"] = Session["Tmp_AddInfoQuoteEdit_taxableDescription"];
                    Session["AddInfoQuoteEdit_taxableAmount"] = Session["Tmp_AddInfoQuoteEdit_taxableAmount"];
                    Session["AddInfoQuoteEdit_nonTaxableDescription"] = Session["Tmp_AddInfoQuoteEdit_nonTaxableDescription"];
                    Session["AddInfoQuoteEdit_nonTaxableAmount"] = Session["Tmp_AddInfoQuoteEdit_nonTaxableAmount"];
                }

                dsRetailAddressDataSet dsRetailAddress = PopulateDSRetailAddress(dsQuote);

                Dmsi.Agility.Data.Quotes quote = new Dmsi.Agility.Data.Quotes();
                string MessageText = quote.UpdateQuote(dsQuote, dsRetailAddress);
                if (MessageText == "0")
                {
                    Session["QuoteHeaderHasBeenSaved"] = true;
                    FetchUpdatedCurrentQuoteDS(dsQuote);

                    lblRequestedDateError.Visible = false;
                    lblPOError.Visible = false;

                    /* Analytics Tracking */

                    AnalyticsInput aInput = new AnalyticsInput();

                    aInput.Type = "event";
                    aInput.Action = "Quote Edit - Header - Save Changes Button Click";
                    aInput.Label = "Mode = Net";
                    aInput.Value = "0";

                    if (Session["UserPref_DisplaySetting"] != null &&
                         ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                          (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                          (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                    {
                        aInput.Label = "Mode = Retail";
                    }

                    AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                    am.BeginTrackEvent();

                }
                else
                {
                    if (MessageText.Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    if (OnQuoteEditModeChanged != null)
                    {
                        Session["QuoteEditModeChanged"] = false;
                        OnQuoteEditModeChanged();
                    }

                    throw new SystemException("Error occurred during Quote Header save...");
                }

                Session["SaveQuoteEditHasErrors"] = false;

                Session["ShiptoQuoteEdit_txtCustomerName"] = null;
            }
            else
                Session["SaveQuoteEditHasErrors"] = true;
        }
    }

    protected void btnAddItems_Click(object sender, EventArgs e)
    {
        UserControls_Inventory ucInventory = Parent.FindControl("QuoteEditAddItems").FindControl("QuoteEditInventory") as UserControls_Inventory;
        ucInventory.RetainSelectedDetails();

        HtmlGenericControl oDiv = ucInventory.FindControl("dvProdListView") as HtmlGenericControl;
        oDiv.Visible = false;

        this.Visible = false;
        Parent.FindControl("QuoteEditAddItems").Visible = true;
        Session["QuoteEditVisible"] = false;
        Session["QuoteEditAddItemsVisible"] = true;
        Session["BlockPopupMenu"] = true;

        MenuEnableDisable menuDisable = new MenuEnableDisable();
        menuDisable.DisableMenuItems(this.Page, this.Parent);

        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Enabled = false;

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Quote Edit - Add Items Button Click";
        aInput.Label = "Mode = Net";
        aInput.Value = "0";

        if (Session["UserPref_DisplaySetting"] != null &&
             ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
              (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
              (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            aInput.Label = "Mode = Retail";
        }

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
    }

    public void Reset()
    {
        Session["QuoteEdit_dsQuote"] = null;
        btnCancel_Click(null, null);

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }
    }

    public void Reset(string source)
    {
        Session["QuoteEdit_dsQuote"] = null;

        lblRequestedDateError.Visible = false;
        lblPOError.Visible = false;

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }
    }

    private dsRetailAddressDataSet PopulateDSRetailAddress(dsQuoteDataSet dsQuote)
    {
        dsRetailAddressDataSet dsRetailAddress = new dsRetailAddressDataSet();

        if (Session["dsRetailAddressForQuotes"] != null)
        {
            dsRetailAddress = (dsRetailAddressDataSet)Session["dsRetailAddressForQuotes"];

            string quoteId = dsQuote.pvttquote_header[0]["quote_id"].ToString();

            for (int x = dsRetailAddress.tt_retail_address.Rows.Count - 1; x >= 0; x--)
            {
                if (dsRetailAddress.tt_retail_address.Rows[x]["tran_id"].ToString() != quoteId)
                {
                    dsRetailAddress.tt_retail_address.Rows[x].Delete();
                }
            }

            dsRetailAddress.AcceptChanges();

            DataRow[] rows = dsRetailAddress.tt_retail_address.Select("tran_id=" + quoteId);

            if (rows.Length > 0)
            {
                rows[0]["misc_1"] = (string)Session["AddInfoQuoteEdit_txtCustomerId"];
                rows[0]["misc_2"] = (string)Session["AddInfoQuoteEdit_txtShipTo"];
                rows[0]["cust_name"] = (string)Session["AddInfoQuoteEdit_txtCustomerName"];
                rows[0]["address_1"] = (string)Session["AddInfoQuoteEdit_txtAddress1"];
                rows[0]["address_2"] = (string)Session["AddInfoQuoteEdit_txtAddress2"];
                rows[0]["address_3"] = (string)Session["AddInfoQuoteEdit_txtAddress3"];
                rows[0]["city"] = (string)Session["AddInfoQuoteEdit_txtCity"];
                rows[0]["state"] = (string)Session["AddInfoQuoteEdit_txtState"];
                rows[0]["zip"] = (string)Session["AddInfoQuoteEdit_txtZip"];
                rows[0]["country"] = (string)Session["AddInfoQuoteEdit_txtCountry"];
                rows[0]["phone"] = (string)Session["AddInfoQuoteEdit_txtPhone"];
                rows[0]["misc_3"] = (string)Session["AddInfoQuoteEdit_txtContactName"];
                rows[0]["email"] = (string)Session["AddInfoQuoteEdit_txtEmail"];
                rows[0]["misc_4"] = (string)Session["AddInfoQuoteEdit_txtReference"];

                if (Session["AddInfoQuoteEdit_taxRate"] == null)
                    Session["AddInfoQuoteEdit_taxRate"] = GetDefaultTaxRate().ToString();

                rows[0]["misc_5"] = (Convert.ToDecimal(Session["AddInfoQuoteEdit_taxRate"])).ToTrimmedString();
                rows[0]["misc_6"] = (string)Session["AddInfoQuoteEdit_taxableDescription"];

                if (Session["AddInfoQuoteEdit_taxableAmount"] == null)
                    Session["AddInfoQuoteEdit_taxableAmount"] = "0";

                rows[0]["misc_7"] = (Convert.ToDecimal(Session["AddInfoQuoteEdit_taxableAmount"])).ToTrimmedString();
                rows[0]["misc_8"] = (string)Session["AddInfoQuoteEdit_nonTaxableDescription"];

                if (Session["AddInfoQuoteEdit_nonTaxableAmount"] == null)
                    Session["AddInfoQuoteEdit_nonTaxableAmount"] = "0";

                rows[0]["misc_9"] = (Convert.ToDecimal(Session["AddInfoQuoteEdit_nonTaxableAmount"])).ToTrimmedString();
            }
            else
            {
                if (Session["AddInfoQuoteEdit_txtCustomerId"] != null &&
                        (
                            ((string)Session["AddInfoQuoteEdit_txtCustomerId"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtShipTo"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtCustomerName"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtAddress1"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtAddress2"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtAddress3"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtCity"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtState"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtZip"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtCountry"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtPhone"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtContactName"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtEmail"]).Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_txtReference"]).Length > 0 ||
                            ((decimal)Session["AddInfoQuoteEdit_taxRate"]).ToTrimmedString().Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_taxableDescription"]).Length > 0 ||
                            ((decimal)Session["AddInfoQuoteEdit_taxableAmount"]).ToTrimmedString().Length > 0 ||
                            ((string)Session["AddInfoQuoteEdit_nonTaxableDescription"]).Length > 0 ||
                            ((decimal)Session["AddInfoQuoteEdit_nonTaxableAmount"]).ToTrimmedString().Length > 0
                        )
                   )
                {
                    DataRow row = dsRetailAddress.tt_retail_address.NewRow();

                    row["misc_1"] = (string)Session["AddInfoQuoteEdit_txtCustomerId"];
                    row["misc_2"] = (string)Session["AddInfoQuoteEdit_txtShipTo"];
                    row["cust_name"] = (string)Session["AddInfoQuoteEdit_txtCustomerName"];
                    row["address_1"] = (string)Session["AddInfoQuoteEdit_txtAddress1"];
                    row["address_2"] = (string)Session["AddInfoQuoteEdit_txtAddress2"];
                    row["address_3"] = (string)Session["AddInfoQuoteEdit_txtAddress3"];
                    row["city"] = (string)Session["AddInfoQuoteEdit_txtCity"];
                    row["state"] = (string)Session["AddInfoQuoteEdit_txtState"];
                    row["zip"] = (string)Session["AddInfoQuoteEdit_txtZip"];
                    row["country"] = (string)Session["AddInfoQuoteEdit_txtCountry"];
                    row["phone"] = (string)Session["AddInfoQuoteEdit_txtPhone"];
                    row["misc_3"] = (string)Session["AddInfoQuoteEdit_txtContactName"];
                    row["email"] = (string)Session["AddInfoQuoteEdit_txtEmail"];
                    row["misc_4"] = (string)Session["AddInfoQuoteEdit_txtReference"];
                    row["misc_5"] = ((decimal)Session["AddInfoQuoteEdit_taxRate"]).ToTrimmedString();
                    row["misc_6"] = (string)Session["AddInfoQuoteEdit_taxableDescription"];
                    row["misc_7"] = ((decimal)Session["AddInfoQuoteEdit_taxableAmount"]).ToTrimmedString();
                    row["misc_8"] = (string)Session["AddInfoQuoteEdit_nonTaxableDescription"];
                    row["misc_9"] = ((decimal)Session["AddInfoQuoteEdit_nonTaxableAmount"]).ToTrimmedString();

                    row["tran_sysid"] = "";
                    row["TYPE"] = "";
                    row["tran_id"] = 0;
                    row["shipment_num"] = 0;
                    row["dispatch_tran_sysid"] = "";
                    row["dispatch_tran_type"] = "";
                    row["dispatch_tran_id"] = 0;
                    row["dispatch_shipment_num"] = 0;

                    dsRetailAddress.tt_retail_address.Rows.Add(row);
                }
            }

            dsRetailAddress.AcceptChanges();
        }

        return dsRetailAddress;
    }

    private void FetchUpdatedCurrentQuoteDS(dsQuoteDataSet dsQuote)
    {
        Session["SaveQuoteEditHasErrors"] = false;
        Session["QuoteHeaderHasBeenSaved"] = true;

        DataManager dm = new DataManager();
        string searchCriteria = (string)dm.GetCache("QuoteSearchCriteria");

        string quoteId = dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

        siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

        foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
        {
            siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
        }

        currentCust.Dispose();

        Dmsi.Agility.Data.Quotes Quotes = new Dmsi.Agility.Data.Quotes(searchCriteria, siCallDS, null, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        siCallDS.Dispose();

        dsQuoteDataSet quoteList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet)Quotes.ReferencedDataSet;

        for (int x = quoteList.pvttquote_header.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_header.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_header.Rows[x].Delete();
        }

        for (int x = quoteList.pvttquote_detail.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_detail.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_detail.Rows[x].Delete();
        }

        for (int x = quoteList.pvttquote_byitem.Rows.Count - 1; x >= 0; x--)
        {
            if (quoteList.pvttquote_detail.Rows[x]["quote_id"].ToString() != quoteId)
                quoteList.pvttquote_detail.Rows[x].Delete();
        }

        quoteList.AcceptChanges();

        Session["QuoteEdit_dsQuote"] = quoteList;
    }

    private BoundField FindGVBoundField(GridView gv, string dataFieldName)
    {
        BoundField bf = new BoundField();

        for (int x = 0; x < gv.Columns.Count; x++)
        {
            bf = gvQuoteDetails.Columns[x] as BoundField;
            if (bf != null && bf.DataField == dataFieldName)
                return bf;
        }

        return bf;
    }

    #region gvQuoteDetails Grid View

    protected void gvQuoteDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvQuoteDetails.Columns[0].Visible = true;

        gvQuoteDetails.EditIndex = -1;

        Session["IsQuoteInUpdateMode"] = false;

        btnChangeMarkup.Enabled = true;

        //If data conditions have disabled ShipVia control then we must not re-enable it.
        if (Session["ShipViaHasBeenDisabledDueToDataCondition"] != null && (bool)Session["ShipViaHasBeenDisabledDueToDataCondition"] == false)
            ddlShipVia.Enabled = true;

        ddlRequestedDate.Enabled = true;
        lnkAddInfo.Enabled = true;

        lblGridMessage.Text = ""; //reset the message line...

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = false;
            OnQuoteEditModeChanged();
        }

        RetainUserPreferences();

        btnSaveChanges.Enabled = true;
        btnGoBack.Enabled = true;
        btnAddItems.Enabled = true;
    }

    protected void gvQuoteDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem != null)
        {
            DataRowView rowView = e.Row.DataItem as DataRowView;
            System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[0]));
            System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[2]));

            if ((e.Row.RowState & DataControlRowState.Edit) == DataControlRowState.Edit)
            {
                imgedit.ToolTip = "Save";
                imgDelete.ToolTip = "Cancel";
            }
            else
            {
                imgedit.ToolTip = "Edit";
                imgDelete.ToolTip = "Delete";
            }

            dsQuoteDataSet.pvttquote_detailRow row = rowView.Row as dsQuoteDataSet.pvttquote_detailRow;

            if ((bool)row["complex_item"] == true)
            {
                //Turn off the Edit link button control; it's nested inside the first control on the row.
                e.Row.Controls[0].Controls[0].Visible = false;
            }

            //Set the boolean to false for release code...
            bool configurable = false;
            if (row["orig_source"] != null && row["orig_source"].ToString().ToLower() == "pv")
                configurable = true;

            Panel panelConfigure = (Panel)e.Row.FindControl("panel_Configure");
            if (panelConfigure != null)
            {
                if (configurable)
                {
                    panelConfigure.Width = new Unit(55, UnitType.Pixel);
                    panelConfigure.Visible = true;
                    gvQuoteDetails.Visible = true;
                    gvQuoteDetails.Columns[1].Visible = true;
                }
                else
                {
                    panelConfigure.Width = new Unit(0, UnitType.Pixel);
                    panelConfigure.Visible = false;
                    gvQuoteDetails.Visible = false;
                    if (configureexist == 1)
                    {
                        gvQuoteDetails.Columns[1].Visible = true;
                    }
                    else
                    {
                        gvQuoteDetails.Columns[1].Visible = false;
                    }
                }
            }

            if ((bool)row["complex_item"] == true &&
                 Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                e.Row.Controls[0].Controls[0].Visible = true;
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }
            }

            e.Row.Attributes.Add("onmouseover", "this.className='GridViewRowMouseHover'");
            e.Row.Attributes.Add("onmouseout", "this.className='GridViewRowMouseOut'");

            // NOTE: a good sample of converting a cell to the databound field
            if (e.Row.RowIndex == gvQuoteDetails.EditIndex)
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    BoundField fld = ((DataControlFieldCell)cell).ContainingField as BoundField;

                    if (fld != null && fld.DataField == "location_reference")
                    {
                        foreach (Control ctl in cell.Controls)
                        {
                            TextBox txt = ctl as TextBox;
                            if (txt != null)
                            {
                                txt.MaxLength = 40;
                                txt.Width = 150;
                            }
                        }
                    }
                }
            }
        }
    }

    protected void gvQuoteDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        dsQuoteDataSet dsQuote = new dsQuoteDataSet();

        if (Session["QuoteEdit_dsQuote"] != null)
            dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];

        dsQuoteDataSet siCallDS = new dsQuoteDataSet();

        siCallDS.pvttquote_header.ImportRow(dsQuote.pvttquote_header.Rows[0]);

        foreach (DataRow row in dsQuote.pvttquote_detail.Rows)
        {
            if (row["sequence"].ToString() == e.Keys["sequence"].ToString())
                siCallDS.pvttquote_detail.LoadDataRow(row.ItemArray, true);
        }

        siCallDS.AcceptChanges();

        Quotes q = new Quotes();
        string MessageText = q.DeleteQuoteDetailItem(siCallDS);

        if (MessageText == "0")
        {
            FetchUpdatedCurrentQuoteDS(dsQuote);

            /* Analytics Tracking */

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Quote Edit - Item Edit - Delete Icon";
            aInput.Label = "Mode = Net";
            aInput.Value = "0";

            if (Session["UserPref_DisplaySetting"] != null &&
                 ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                  (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                  (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                aInput.Label = "Mode = Retail";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }
        else
        {
            if (MessageText.Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            EventLog log = new EventLog();
            log.LogError(EventTypeFlag.AppServerDBerror, MessageText);
            throw new SystemException("Error occurred during Quote item delete..." + MessageText);
        }
    }

    protected void gvQuoteDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        gvQuoteDetails.Columns[0].Visible = true;

        int rowSequenceKey = Convert.ToInt32(e.Keys["sequence"]);

        decimal newQty = 0;
        decimal newNetPrice = 0;
        decimal newRetailPrice = 0;

        bool changedQty = false;
        bool changedNetPrice = false;
        bool changedRetailPrice = false;

        bool ok = true;

        if (e.NewValues["qty_ordered"] != null)
        {
            changedQty = Decimal.TryParse(e.NewValues["qty_ordered"].ToString(), out newQty);

            if (!changedQty)
            {
                lblGridMessage.Text += "<b>Error:</b> Invalid quantity in Sequence " + rowSequenceKey.ToString() + "&nbsp;&nbsp;";
                e.Cancel = true;
                ok = false;
            }
            else if (Convert.ToDecimal(e.Keys["min_pak_disp"]) != 0)
            {
                if (newQty % Convert.ToDecimal(e.Keys["min_pak_disp"]) != 0)
                {
                    lblGridMessage.Text += "<b>Error:</b> Invalid quantity in Sequence " + rowSequenceKey.ToString() + "; You must order in Min Pack multiples.&nbsp;&nbsp;";
                    e.Cancel = true;
                    ok = false;
                }
            }
        }

        if (e.NewValues["net_price"] != null)
        {
            changedNetPrice = Decimal.TryParse(e.NewValues["net_price"].ToString(), out newNetPrice);

            if (!changedNetPrice)
            {
                lblGridMessage.Text += "<b>Error:</b> Invalid price in Sequence " + rowSequenceKey.ToString();
                e.Cancel = true;
                ok = false;
            }
        }

        if (e.NewValues["retail_price"] != null)
        {
            changedRetailPrice = Decimal.TryParse(e.NewValues["retail_price"].ToString(), out newRetailPrice);

            if (!changedRetailPrice)
            {
                lblGridMessage.Text += "<b>Error:</b> Invalid price in Sequence " + rowSequenceKey.ToString();
                e.Cancel = true;
                ok = false;
            }
        }

        if (ok)
        {
            dsQuoteDataSet dsQuote = new dsQuoteDataSet();

            if (Session["QuoteEdit_dsQuote"] != null)
            {
                FetchUpdatedCurrentQuoteDS((dsQuoteDataSet)Session["QuoteEdit_dsQuote"]);
                dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
            }

            DataRow[] rows = dsQuote.pvttquote_detail.Select("sequence = " + rowSequenceKey.ToString());
            if (rows.Length == 1)
            {
                if (changedQty)
                    rows[0]["qty_ordered"] = newQty;

                if (changedNetPrice)
                    rows[0]["net_price"] = newNetPrice;

                if (changedRetailPrice)
                {
                    rows[0]["retail_price_calc_factor_encr"] = "";
                    rows[0]["retail_price_calc_mode_encr"] = "";
                    rows[0]["retail_price"] = newRetailPrice;
                }

                if (e.NewValues["location_reference"] != null)
                    rows[0]["location_reference"] = e.NewValues["location_reference"] as string;
                else if (e.Keys["location_reference"] == null)
                    rows[0]["location_reference"] = "";
            }

            dsQuote.AcceptChanges();

            dsRetailAddressDataSet dsRetailAddress = PopulateDSRetailAddress(dsQuote);

            Dmsi.Agility.Data.Quotes quote = new Dmsi.Agility.Data.Quotes();
            string MessageText = quote.UpdateQuote(dsQuote, dsRetailAddress);

            if (MessageText == "0")
            {
                FetchUpdatedCurrentQuoteDS(dsQuote);

                /* Analytics Tracking */

                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Quote Edit - Item Edit - Save Icon";
                aInput.Label = "Mode = Net";
                aInput.Value = "0";

                if (Session["UserPref_DisplaySetting"] != null &&
                     ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                      (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                      (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                {
                    aInput.Label = "Mode = Retail";
                }

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();
            }
            else
            {
                if (MessageText.Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                EventLog log = new EventLog();
                log.LogError(EventTypeFlag.AppServerDBerror, MessageText);
                throw new SystemException("Error occurred during Quote update save..." + MessageText);
            }

            gvQuoteDetails.EditIndex = -1;

            Session["IsQuoteInUpdateMode"] = false;

            btnChangeMarkup.Enabled = true;

            //If data conditions have disabled ShipVia control then we must not re-enable it.
            if (Session["ShipViaHasBeenDisabledDueToDataCondition"] != null && (bool)Session["ShipViaHasBeenDisabledDueToDataCondition"] == false)
                ddlShipVia.Enabled = true;

            ddlRequestedDate.Enabled = true;
            lnkAddInfo.Enabled = true;

            lblGridMessage.Text = ""; //reset the message line...

            if (OnQuoteEditModeChanged != null)
            {
                Session["QuoteEditModeChanged"] = false;
                OnQuoteEditModeChanged();
            }

            RetainUserPreferences();

            btnSaveChanges.Enabled = true;
            btnGoBack.Enabled = true;
            btnAddItems.Enabled = true;
        }
    }

    protected void gvQuoteDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridViewRow row = gvQuoteDetails.Rows[e.NewEditIndex];

        gvQuoteDetails.Columns[0].Visible = false;

        this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
        HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
        hypUserPreferences.Visible = false;

        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            lblGridMessage.Text = "Quantity changes on items with quantity break pricing do not update retail prices.";
        }

        Session["IsQuoteInUpdateMode"] = true;

        btnChangeMarkup.Enabled = false;
        ddlShipVia.Enabled = false;
        ddlRequestedDate.Enabled = false;
        lnkAddInfo.Enabled = false;

        btnSaveChanges.Enabled = false;
        btnGoBack.Enabled = false;
        btnAddItems.Enabled = false;

        if (OnQuoteEditModeChanged != null)
        {
            Session["QuoteEditModeChanged"] = true;
            OnQuoteEditModeChanged();
        }

        gvQuoteDetails.Columns[0].Visible = true;
        Session["QuoteEditedRowId"] = e.NewEditIndex;
        System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(gvQuoteDetails.Rows[e.NewEditIndex].Cells[0].Controls[0]));
        System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(gvQuoteDetails.Rows[e.NewEditIndex].Cells[0].Controls[2]));
        imgedit.ToolTip = "Save";
        imgDelete.ToolTip = "Cancel";

        Panel p = (Panel)gvQuoteDetails.Rows[e.NewEditIndex].Cells[1].FindControl("panel_Configure");
        if (p != null && p.Visible == false)
        {
            BoundField bf = gvQuoteDetails.Columns[7] as BoundField;
            bf.ReadOnly = true;
        }
        else if (p != null && p.Visible == true)
        {
            BoundField bf = gvQuoteDetails.Columns[7] as BoundField;
            bf.ReadOnly = false;
        }

        string rowSequenceKey = gvQuoteDetails.Rows[e.NewEditIndex].Cells[2].Text;
        dsQuoteDataSet dsQuote = new dsQuoteDataSet();
        bool iscomplex = false;

        if (Session["QuoteEdit_dsQuote"] != null)
            dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];

        DataRow[] rows = dsQuote.pvttquote_detail.Select("sequence = " + rowSequenceKey.ToString());

        if (rows.Length == 1)
        {
            iscomplex = (bool)rows[0]["complex_item"];
        }

        BoundField qtybf = gvQuoteDetails.Columns[8] as BoundField;

        if (iscomplex &&
            Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            qtybf.ReadOnly = true;
            lblGridMessage.Text = "";
        }
        else
            qtybf.ReadOnly = false;
    }

    protected void gvQuoteDetails_DataBound(object sender, EventArgs e)
    {
        //Handle Edit allocation...
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        bool editAllocation = true;
        bool editNetPriceAllocation = false; //this is an inverted disallow action allocation

        if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
        {
            DataRow[] rows = dsPV.bPV_Action.Select("token_code='edit_quotes'");
            if (rows.Length == 0) //no allocation
            {
                editAllocation = false;
            }

            rows = dsPV.bPV_Action.Select("token_code='edit_quotes_disallow_net_price_edit'");
            if (rows.Length == 0) //no allocation record, but it means we can edit net price because it's an inverted disallow-type action allocation
            {
                editNetPriceAllocation = true;
            }
        }

        if (editAllocation == false || editNetPriceAllocation == false)
        {
            //can edit, but *only* in Retail mode...
            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                FindGVBoundField(gvQuoteDetails, "retail_price").ReadOnly = false;
            }
        }
        else
        {
            if (Session["UserPref_DisplaySetting"] != null && Convert.ToString(Session["UserPref_DisplaySetting"]) == "Net price")
            {
                //swap column visibility, but only if action security allowed
                if (editNetPriceAllocation == true)
                {
                    FindGVBoundField(gvQuoteDetails, "net_price").ReadOnly = false;
                }
            }

            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                FindGVBoundField(gvQuoteDetails, "retail_price").ReadOnly = false;
            }
        }
    }

    #endregion

}