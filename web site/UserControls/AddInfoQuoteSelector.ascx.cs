using System;
using System.Data;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI.HtmlControls;
using System.Web.UI;
public partial class UserControls_AddInfoQuoteSelector : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate();
    public event OKClickedDelegate OnOKClicked;
    
    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    public delegate void CancelClickedDelegate();
    public event CancelClickedDelegate OnCancelClicked;


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["AddInfoQuote_txtCustomerId"] != null)
        {
            txtCustomerId.Text = Convert.ToString(Session["AddInfoQuote_txtCustomerId"]);
            txtShipTo.Text = Convert.ToString(Session["AddInfoQuote_txtShipTo"]);
            txtCustomerName.Text = Convert.ToString(Session["AddInfoQuote_txtCustomerName"]);
            txtAddress1.Text = Convert.ToString(Session["AddInfoQuote_txtAddress1"]);
            txtAddress2.Text = Convert.ToString(Session["AddInfoQuote_txtAddress2"]);
            txtAddress3.Text = Convert.ToString(Session["AddInfoQuote_txtAddress3"]);
            txtCity.Text = Convert.ToString(Session["AddInfoQuote_txtCity"]);
            txtState.Text = Convert.ToString(Session["AddInfoQuote_txtState"]);
            txtZip.Text = Convert.ToString(Session["AddInfoQuote_txtZip"]);
            txtCountry.Text = Convert.ToString(Session["AddInfoQuote_txtCountry"]);
            txtPhone.Text = Convert.ToString(Session["AddInfoQuote_txtPhone"]);
            txtContactName.Text = Convert.ToString(Session["AddInfoQuote_txtContactName"]);
            txtEmail.Text = Convert.ToString(Session["AddInfoQuote_txtEmail"]);
            txtReference.Text = Convert.ToString(Session["AddInfoQuote_txtReference"]);
            otherChargesAndTaxesAddlInfo.TaxRate = Convert.ToDecimal(Session["AddInfoQuote_taxRate"]);
            otherChargesAndTaxesAddlInfo.TaxableDescription = Convert.ToString(Session["AddInfoQuote_taxableDescription"]);
            otherChargesAndTaxesAddlInfo.TaxableAmount = Convert.ToDecimal(Session["AddInfoQuote_taxableAmount"]);
            otherChargesAndTaxesAddlInfo.NonTaxableDescription = Convert.ToString(Session["AddInfoQuote_nonTaxableDescription"]);
            otherChargesAndTaxesAddlInfo.NonTaxableAmount = Convert.ToDecimal(Session["AddInfoQuote_nonTaxableAmount"]);
        }
        else
        {
            txtCustomerId.Text = "";
            txtShipTo.Text = "";
            txtCustomerName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtContactName.Text = "";
            txtEmail.Text = "";
            txtReference.Text = "";
            otherChargesAndTaxesAddlInfo.TaxRate = GetDefaultTaxRate();
            otherChargesAndTaxesAddlInfo.TaxableDescription = "";
            otherChargesAndTaxesAddlInfo.TaxableAmount = 0;
            otherChargesAndTaxesAddlInfo.NonTaxableDescription = "";
            otherChargesAndTaxesAddlInfo.NonTaxableAmount = 0;
        }
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);

    }
    protected decimal GetDefaultTaxRate()
    {
        return Convert.ToDecimal(Profile.DefaultTaxRate == "" ? "0" : Profile.DefaultTaxRate);
    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        Session["AddInfoQuote_txtCustomerId"] = txtCustomerId.Text;
        Session["AddInfoQuote_txtShipTo"] = txtShipTo.Text;
        Session["AddInfoQuote_txtCustomerName"] = txtCustomerName.Text;
        Session["AddInfoQuote_txtAddress1"] = txtAddress1.Text;
        Session["AddInfoQuote_txtAddress2"] = txtAddress2.Text;
        Session["AddInfoQuote_txtAddress3"] = txtAddress3.Text;
        Session["AddInfoQuote_txtCity"] = txtCity.Text;
        Session["AddInfoQuote_txtState"] = txtState.Text;
        Session["AddInfoQuote_txtZip"] = txtZip.Text;
        Session["AddInfoQuote_txtCountry"] = txtCountry.Text;
        Session["AddInfoQuote_txtPhone"] = txtPhone.Text;
        Session["AddInfoQuote_txtContactName"] = txtContactName.Text;
        Session["AddInfoQuote_txtEmail"] = txtEmail.Text;
        Session["AddInfoQuote_txtReference"] = txtReference.Text;
        Session["AddInfoQuote_taxRate"] = otherChargesAndTaxesAddlInfo.TaxRate;
        Session["AddInfoQuote_taxableDescription"] = otherChargesAndTaxesAddlInfo.TaxableDescription;
        Session["AddInfoQuote_taxableAmount"] = otherChargesAndTaxesAddlInfo.TaxableAmount;
        Session["AddInfoQuote_nonTaxableDescription"] = otherChargesAndTaxesAddlInfo.NonTaxableDescription;
        Session["AddInfoQuote_nonTaxableAmount"] = otherChargesAndTaxesAddlInfo.NonTaxableAmount;

        if (sender != null && e != null)
        {
            /* Analytics Tracking */

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Quotes - Edit Retail Address Information Update - OK Button";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        if (OnOKClicked != null)
            OnOKClicked(); // will be caught by the parent
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (OnCancelClicked != null)
            OnCancelClicked(); // will be caught by the parent
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtCustomerId.Text = "";
        txtShipTo.Text = "";
        txtCustomerName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtZip.Text = "";
        txtCountry.Text = "";
        txtPhone.Text = "";
        txtContactName.Text = "";
        txtEmail.Text = "";
        txtReference.Text = "";
        otherChargesAndTaxesAddlInfo.TaxRate = GetDefaultTaxRate();
        otherChargesAndTaxesAddlInfo.TaxableDescription = "";
        otherChargesAndTaxesAddlInfo.TaxableAmount = 0;
        otherChargesAndTaxesAddlInfo.NonTaxableDescription = "";
        otherChargesAndTaxesAddlInfo.NonTaxableAmount = 0;

        Session["AddInfoQuote_txtCustomerId"] = null;
        Session["AddInfoQuote_txtShipTo"] = null;
        Session["AddInfoQuote_txtCustomerName"] = null;
        Session["AddInfoQuote_txtAddress1"] = null;
        Session["AddInfoQuote_txtAddress2"] = null;
        Session["AddInfoQuote_txtAddress3"] = null;
        Session["AddInfoQuote_txtCity"] = null;
        Session["AddInfoQuote_txtState"] = null;
        Session["AddInfoQuote_txtZip"] = null;
        Session["AddInfoQuote_txtCountry"] = null;
        Session["AddInfoQuote_txtPhone"] = null;
        Session["AddInfoQuote_txtContactName"] = null;
        Session["AddInfoQuote_txtEmail"] = null;
        Session["AddInfoQuote_txtReference"] = null;
        Session["AddInfoQuote_taxRate"] = null;
        Session["AddInfoQuote_taxableDescription"] = null;
        Session["AddInfoQuote_taxableAmount"] = null;
        Session["AddInfoQuote_nonTaxableDescription"] = null;
        Session["AddInfoQuote_nonTaxableAmount"] = null;

        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }
}
