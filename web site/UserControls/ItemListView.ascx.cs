﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;

public partial class UserControls_ItemListView : System.Web.UI.UserControl
{
    //Database db = DatabaseFactory.CreateDatabase("DMSIConnectionString");
    DataSet dsProducts = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }

    public void BindProductsGrid()
    {
        try
        {
            dsProducts = GetProductsDetails();

            if ((dsProducts != null && dsProducts.Tables.Count > 0 && dsProducts.Tables[0].Rows.Count > 0))
            {
                lvProducts.DataSource = dsProducts;
                lvProducts.DataBind();
            }
            else
            {
                lvProducts.DataSource = null;
                lvProducts.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet GetProductsDetails()
    {
        DataSet dsProd = new DataSet();
        try
        {
            //dbCmd = db.GetStoredProcCommand("GetProductDetails");
            //dsProd = db.ExecuteDataSet(dbCmd);
            if ((dsProd != null && dsProd.Tables.Count > 0))
            {
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ProductID", typeof(string));
                dt.Columns.Add("ProductDescription", typeof(string));
                dt.Columns.Add("StockAvailable", typeof(string));
                dt.Columns.Add("ProductPartNum", typeof(string));
                dt.Columns.Add("MajorMinor", typeof(string));
                dt.Columns.Add("Price", typeof(string));
                dt.Columns.Add("MinPack", typeof(string));
                dt.Columns.Add("ImageURL", typeof(string));
                dsProd.Tables.Add(dt);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dsProd;
    }

    protected void lvProducts_ItemDataBound(object sender, System.Web.UI.WebControls.ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                object obj = ((System.Web.UI.WebControls.ListViewDataItem)(e.Item)).DataItem;
                DataRow CurrentRow = ((DataRowView)obj).Row; ;
                if (!string.IsNullOrEmpty(Convert.ToString(CurrentRow["ProductID"])))
                {
                    if (Convert.ToString(CurrentRow["ProductID"]) == "0")
                    {

                    }
                    else
                    {
                        ImageButton imgProduct = (ImageButton)e.Item.FindControl("imgProduct");
                        if (!DBNull.Value.Equals(CurrentRow["ImageURL"]))
                        {
                            imgProduct.ImageUrl = (string)CurrentRow["ImageURL"];
                            imgProduct.PostBackUrl = (string)CurrentRow["ImageURL"];
                        }
                        LinkButton lbtProdDesc = (LinkButton)e.Item.FindControl("lbtProdDesc");
                        if (!DBNull.Value.Equals(CurrentRow["ProductDescription"]))
                        {
                            lbtProdDesc.Text = (string)CurrentRow["ProductDescription"];
                            lbtProdDesc.ToolTip = (string)CurrentRow["ProductDescription"];
                        }

                        Label lblAvilableCount = (Label)e.Item.FindControl("lblAvilableCount");
                        if (!DBNull.Value.Equals(CurrentRow["StockAvailable"]))
                        {
                            lblAvilableCount.Text = (string)Convert.ToString(CurrentRow["StockAvailable"]);
                            lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["StockAvailable"]);
                        }

                        Label lblPartNo = (Label)e.Item.FindControl("lblPartNo");
                        if (!DBNull.Value.Equals(CurrentRow["ProductPartNum"]))
                        {
                            lblPartNo.Text = (string)CurrentRow["ProductPartNum"];
                            lblPartNo.ToolTip = (string)CurrentRow["ProductPartNum"];
                        }

                        Label lblMajorMinor = (Label)e.Item.FindControl("lblMajorMinor");
                        if (!DBNull.Value.Equals(CurrentRow["MajorMinor"]))
                        {
                            lblMajorMinor.Text = (string)CurrentRow["MajorMinor"];
                            lblMajorMinor.ToolTip = (string)CurrentRow["MajorMinor"];
                        }

                        Label lblPriceText = (Label)e.Item.FindControl("lblPriceText");
                        if (!DBNull.Value.Equals(CurrentRow["Price"]))
                        {
                            lblPriceText.Text = (string)Convert.ToString(CurrentRow["Price"]);
                            lblPriceText.ToolTip = (string)Convert.ToString(CurrentRow["Price"]);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}