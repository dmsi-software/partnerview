<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Quotes.ascx.cs" Inherits="UserControls_Quotes" %>
<%@ Register Src="ClientSideCalendar.ascx" TagName="ClientSideCalendar" TagPrefix="uc2" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc3" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="DocViewer.ascx" TagName="DocViewer" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/QuoteRelease.ascx" TagName="QuoteRelease" TagPrefix="uc5" %>
<%@ Register Src="~/UserControls/QuoteEdit.ascx" TagName="QuoteEdit" TagPrefix="uc6" %>
<%@ Register Src="~/UserControls/QuoteEditRetail_Info.ascx" TagName="QuoteEditRetail_Info" TagPrefix="uc7" %>
<style type="text/css">
    .PanelPadding {
        padding-left: 4px;
        padding-right: 4px;
    }

    .child-div {
        position: absolute;
        left: 2%;
        right: 0px;
    }

    .MarginTop20 {
        margin-top: 20px !important;
    }
</style>
<script type="text/javascript">
</script>
<div class="left-section">
    <div class="lft-cont-sec">
        <div id="my-tab-content">
            <div class="search-opt">
                <div class="advsearch">
                    <asp:Panel ID="QuotesearchFieldPanel" runat="server" DefaultButton="btnFind">
                        <asp:Label ID="Label3" runat="server" Text="Search by" CssClass="alladvsearchlabel"></asp:Label>
                        <asp:DropDownList ID="ddlTranID" runat="server" CssClass="select" OnSelectedIndexChanged="ddlTranID_SelectedIndexChanged" AutoPostBack="True">
                            <asp:ListItem>Quote ID</asp:ListItem>
                            <asp:ListItem>Reference #</asp:ListItem>
                            <asp:ListItem>Item #</asp:ListItem>
                            <asp:ListItem>PO ID</asp:ListItem>
                            <asp:ListItem>Job #</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSearchStyle" runat="server" CssClass="select">
                            <asp:ListItem>Equals</asp:ListItem>
                            <asp:ListItem>Starting at</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtTranID" runat="server" CssClass="select"></asp:TextBox><br />
                    </asp:Panel>
                    <asp:Panel ID="SOAdvancedSearchDiv" runat="server" DefaultButton="btnFind">
                        <asp:Label ID="Label2" runat="server" Text="Status" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select">
                            <asp:ListItem>All</asp:ListItem>
                            <asp:ListItem>Active</asp:ListItem>
                            <asp:ListItem>Closed</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:Label ID="Label5" runat="server" Text="From quote date" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="select"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="ce_txtSdate" runat="server" TargetControlID="txtFromDate"
                            Format="MM/dd/yyyy" PopupButtonID="txtFromDate" Animated="true" CssClass="cal_Theme1" />
                        <br />
                        <asp:Label ID="Label6" runat="server" CssClass="alladvsearchlabel" Text="To quote date"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="select"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="cetodate" runat="server" TargetControlID="txtToDate"
                            Format="MM/dd/yyyy" PopupButtonID="txtToDate" Animated="true" CssClass="cal_Theme1" />
                        <br />
                        <asp:Label ID="Label4" runat="server" Text="Show results by" CssClass="alladvsearchlabel"></asp:Label><br />
                        <asp:DropDownList ID="ddlQuoteShowResultsBy" runat="server" CssClass="select">
                            <asp:ListItem>Quote</asp:ListItem>
                            <asp:ListItem>Item</asp:ListItem>
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:CheckBox ID="cbShipTo" runat="server" Text="Include all ship-tos" Checked="True" />
                    <br />
                    <asp:CheckBox ID="cbMyQuotes" runat="server" Text="My quotes only" Checked="False" />
                    <br />
                    <asp:Button ID="btnFind" runat="server" CssClass="btn btn-primary" OnClick="btnFind_Click" Text="Search" />
                    <br />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right-section">
    <div class="container-sec">
        <h2>
            <asp:Label ID="lblQuoteHeader" runat="server" Text="Quotes"></asp:Label>
        </h2>
        <div class="custom-grid abc">
            <div style="width: 100%">
                <asp:Label ID="lblQuoteHeaderError" runat="server" Text="First 100 quotes returned. Add criteria to refine search."
                    Visible="False" CssClass="ErrorTextRed"></asp:Label>
            </div>
            <div style="width: 100%; float: left;">
                <div class="optional" style="width: 280px !important;">
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
                        DropShadow="true" PopupControlID="pnlQuoteHeaderCustomView"
                        TargetControlID="lnkQuoteHeaderCustomView">
                    </cc1:ModalPopupExtender>
                    <asp:Label ID="lblshow" runat="server" Text="Show" />
                    <asp:DropDownList ID="ddlQuoteHeaderRows" runat="server" AutoPostBack="true" Width="50px" CssClass="select opt-size"
                        OnSelectedIndexChanged="ddlQuoteHeaderRows_SelectedIndexChanged">
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>75</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="Label11" runat="server" Text="Rows"></asp:Label>
                    <asp:LinkButton ID="lnkQuoteHeaderCustomView" runat="server" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                </div>
            </div>
            <asp:Panel ID="Panel10" runat="server">
                <div class="custom-grid-overflow">
                    <asp:GridView ID="gvQuoteHeaders" runat="server" AllowPaging="True" Width="100%"
                        OnPageIndexChanging="gvQuoteHeader_PageIndexChanging" OnRowDataBound="gvQuoteHeader_RowDataBound"
                        AutoGenerateColumns="False" DataKeyNames="quote_id" AllowSorting="True" OnRowCreated="gvQuoteHeader_RowCreated"
                        OnSorting="gvQuoteHeader_Sorting" PageSize="5" EnableModelValidation="True" OnRowCommand="gvQuoteHeaders_RowCommand"
                        OnRowEditing="gvQuoteHeaders_RowEditing" CssClass="grid-tabler" EmptyDataText="No Records found." OnSelectedIndexChanged="gvQuoteHeaders_SelectedIndexChanged">
                        <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <itemstyle width="5%" />
                                    <headerstyle width="5%" />
                                    <asp:ImageButton OnClick="Show_Hide_ChildGrid" CommandName="Details" runat="server" ID="imgbtnplusminus" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/Images/Grid_Plus.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Actions">
                                <ItemTemplate>
                                    <asp:ImageButton CommandArgument='<%#Container.DataItemIndex%>' runat="server" CommandName="Edit" ID="imgEdit" ImageUrl="~/Images/edit-icon.png" />
                                    <asp:ImageButton CommandArgument='<%#Container.DataItemIndex%>' runat="server" CommandName="EditRetailInfo" ID="imgEditRetailInfo" ImageUrl="~/Images/retail_quote_edit_v2.png" />
                                    <asp:ImageButton runat="server" CommandArgument='<%#Container.DataItemIndex%>' CommandName="Release" ID="imgRelease" ImageUrl="~/Images/release-icon.png" />
                                    <asp:ImageButton runat="server" CommandArgument='<%#Container.DataItemIndex%>' CommandName="CopyQuote" ID="imgCopyQuote" ImageUrl="~/Images/CopyQuote.png" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:ButtonField HeaderText="Release" ItemStyle-Width="16px" HeaderStyle-Width="16px" CommandName="Release" Text="Release" ImageUrl="~/Images/release-icon.png" ButtonType="Image" Visible="false" />
                            <asp:BoundField DataField="branch_id" HeaderText="Branch" ReadOnly="True" SortExpression="branch_id"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="quote_id" HeaderText="Quote ID" ReadOnly="True" SortExpression="quote_id">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="job" HeaderText="Job #" ReadOnly="True" SortExpression="job"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="reference" HeaderText="Reference #" ReadOnly="True" SortExpression="reference">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cust_po" HeaderText="PO ID" ReadOnly="True" SortExpression="cust_po">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="quoted_for" HeaderText="Quoted For" ReadOnly="True" SortExpression="quoted_for"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="quote_date" HeaderText="Quote Date" ReadOnly="True" SortExpression="quote_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="expect_date" HeaderText="Delivery Date" ReadOnly="True"
                                SortExpression="expect_date" Visible="False" DataFormatString="{0:MM/dd/yyyy}"
                                HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="route_id_char" HeaderText="Route" ReadOnly="True" SortExpression="route_id_char"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_via" HeaderText="Ship Via" ReadOnly="True" SortExpression="ship_via"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type" HeaderText="Sale Type" ReadOnly="True" SortExpression="sale_type"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type_desc" HeaderText="Sale Type Description" ReadOnly="True"
                                SortExpression="sale_type_desc">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="statusdescr" HeaderText="Status" ReadOnly="True" SortExpression="statusdescr">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="freight_terms_id" HeaderText="Freight Terms" ReadOnly="True"
                                SortExpression="freight_terms_id" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="pay_terms_code" HeaderText="Terms" ReadOnly="True" SortExpression="pay_terms_code"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_subtotal" HeaderText="Sub Total" ReadOnly="True"
                                SortExpression="order_subtotal" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_taxes" HeaderText="Total Taxes" ReadOnly="True"
                                SortExpression="order_taxes" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_charges" HeaderText="Total Charges" ReadOnly="True"
                                SortExpression="order_charges" DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="order_total" HeaderText="Total" ReadOnly="True" SortExpression="order_total"
                                DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="retail_order_total" HeaderText="Retail Total" ReadOnly="True" SortExpression="retail_order_total"
                                DataFormatString="{0:F2}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_1" HeaderText="Sales Rep 1" ReadOnly="True" SortExpression="rep_1"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_2" HeaderText="Sales Rep 2" ReadOnly="True" SortExpression="rep_2"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_3" HeaderText="Sales Rep 3" ReadOnly="True" SortExpression="rep_3"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shiptoname" HeaderText="Ship-to" ReadOnly="True" SortExpression="shiptoname"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr1" HeaderText="Ship-to Address 1" ReadOnly="True"
                                SortExpression="shipToAddr1" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr2" HeaderText="Ship-to Address 2" ReadOnly="True"
                                SortExpression="shipToAddr2" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr3" HeaderText="Ship-to Address 3" ReadOnly="True"
                                SortExpression="shipToAddr3" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToCity" HeaderText="Ship-to City" ReadOnly="True"
                                SortExpression="shipToCity" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToState" HeaderText="Ship-to State" ReadOnly="True"
                                SortExpression="shipToState" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToZip" HeaderText="Ship-to ZIP" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <tr class="tr-innergrid child-div">
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px;">
                                            <asp:Panel Visible="false" runat="server" ID="innergrids" Style="background-color: White; width: 100%" class="panel-collapse show-panel acd collapse in">
                                                <div class="docgrid childgrid" style="padding-left: 40px">
                                                    <uc4:DocViewer ID="docViewer" runat="server" />
                                                </div>
                                                <h3 style="padding-left: 40px; padding-top: 20px">
                                                    <asp:Label ID="lblQuoteInnerId" runat="server" />
                                                </h3>
                                                <br />
                                                <div style="                                                        width: 100%;
                                                        float: left;
                                                        padding-left: 40px;
                                                        margin-top: -10px">
                                                    <div class="optional" style="width: 280px !important;">
                                                        <asp:Label ID="lblQuoteDetailRows" runat="server" Text="Show"></asp:Label>
                                                        <asp:DropDownList ID="ddlQuoteDetailRows" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlQuoteDetailRows_SelectedIndexChanged"
                                                            Width="50px" CssClass="select opt-size">
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>10</asp:ListItem>
                                                            <asp:ListItem>15</asp:ListItem>
                                                            <asp:ListItem>20</asp:ListItem>
                                                            <asp:ListItem>25</asp:ListItem>
                                                            <asp:ListItem Value="1000">All</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lblinnerResults" Text="Rows" runat="server" />
                                                        <asp:LinkButton CommandArgument='<%#Container.DataItemIndex%>' ID="lnkQuoteDetailCustomView" runat="server" OnClick="lnkQuoteDetailCustomView_Click" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <br />
                                                <div style="width: 100%; float: left">
                                                    <div class="childgrid">
                                                        <asp:GridView ID="gvQuoteDetail" runat="server" PageSize="5" Width="100%"
                                                            AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnRowCreated="gvQuoteDetail_RowCreated"
                                                            OnSorting="gvQuoteDetail_Sorting" DataKeyNames="quote_id" OnPageIndexChanging="gvQuoteDetail_PageIndexChanging" CssClass="sub-grd-tabler">
                                                            <PagerStyle CssClass="PagerDetailGridView" HorizontalAlign="Left" Height="10px" />
                                                            <Columns>
                                                                <asp:BoundField DataField="display_seq" HeaderText="Seq #" ReadOnly="True" SortExpression="display_seq"
                                                                    Visible="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="quote_desc" HeaderText="Description" ReadOnly="True" SortExpression="quote_desc">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="location_reference" HeaderText="Location Reference" ReadOnly="True" SortExpression="location_reference" Visible="False">
                                                                    <ItemStyle Wrap="True" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="qty_ordered" HeaderText="Quantity" ReadOnly="True" SortExpression="qty_ordered"
                                                                    HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="staged_qty" HeaderText="Staged Qty" ReadOnly="True" SortExpression="staged_qty">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="uom" HeaderText="Qty UOM" ReadOnly="True" SortExpression="uom">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="net_price" HeaderText="Price" ReadOnly="True" SortExpression="net_price"
                                                                    DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="price_uom_code" HeaderText="Price UOM" ReadOnly="True"
                                                                    SortExpression="price_uom_code">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="extended_price" HeaderText="Extended Amount" ReadOnly="True"
                                                                    SortExpression="extended_price" DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="retail_price" HeaderText="Retail Price" ReadOnly="True" SortExpression="retail_price"
                                                                    DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="retail_price_uom_code" HeaderText="Retail Price UOM" ReadOnly="True"
                                                                    SortExpression="retail_price_uom_code">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="retail_extended_price" HeaderText="Retail Extended Amount" ReadOnly="True"
                                                                    SortExpression="retail_extended_price" DataFormatString="{0:F2}" HtmlEncode="False">
                                                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr class="tr-innergrid">
                                        <td colspan="100" style="border: none; border-collapse: collapse; width: 100%; padding: 0px"">
                                            <asp:Panel ID="innerplaceholder" runat="server" Visible="false" Style="width: 100%; background-color: White;" class="panel-collapse show-panel acd collapse in">
                                                <div>&nbsp;</div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvByItem" runat="server" PageSize="5" Width="100%" OnRowDataBound="gvQuoteHeader_RowDataBound"
                        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnRowCreated="gvByItem_RowCreated"
                        OnSorting="gvByItem_Sorting" OnPageIndexChanging="gvByItem_PageIndexChanging" CssClass="grid-tabler">
                        <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                        <Columns>
                            <asp:BoundField DataField="branch_id" HeaderText="Branch" ReadOnly="True" SortExpression="branch_id"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="quote_id" HeaderText="Quote ID" ReadOnly="True" SortExpression="quote_id">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="quote_desc" HeaderText="Description" ReadOnly="True" SortExpression="quote_desc"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="qty_ordered" HeaderText="Quantity" ReadOnly="True" SortExpression="qty_ordered"
                                HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="staged_qty" HeaderText="Staged Qty" ReadOnly="True" SortExpression="staged_qty"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="uom" HeaderText="Qty UOM" ReadOnly="True" SortExpression="uom"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="net_price" HeaderText="Price" ReadOnly="True" SortExpression="net_price"
                                DataFormatString="{0:F2}" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="price_uom_code" HeaderText="Price UOM" ReadOnly="True"
                                SortExpression="price_uom_code" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="extended_price" HeaderText="Extended Amount" ReadOnly="True"
                                SortExpression="extended_price" DataFormatString="{0:F2}" HtmlEncode="False"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="retail_price" HeaderText="Retail Price" ReadOnly="True" SortExpression="retail_price"
                                DataFormatString="{0:F2}" HtmlEncode="False" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="retail_price_uom_code" HeaderText="Retail Price UOM" ReadOnly="True"
                                SortExpression="retail_price_uom_code" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="retail_extended_price" HeaderText="Retail Extended Amount" ReadOnly="True"
                                SortExpression="retail_extended_price" DataFormatString="{0:F2}" HtmlEncode="False"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="job" HeaderText="Job #" ReadOnly="True" SortExpression="job"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="reference" HeaderText="Reference #" ReadOnly="True" SortExpression="reference">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="cust_po" HeaderText="PO ID" ReadOnly="True" SortExpression="cust_po">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="quote_date" HeaderText="Quote Date" ReadOnly="True" SortExpression="quote_date"
                                DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="route_id_char" HeaderText="Route" ReadOnly="True" SortExpression="route_id_char"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ship_via" HeaderText="Ship Via" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type" HeaderText="Sale Type" ReadOnly="True" SortExpression="sale_type"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="sale_type_desc" HeaderText="Sale Type Description" ReadOnly="True"
                                SortExpression="sale_type_desc">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="statusdescr" HeaderText="Status" ReadOnly="True" SortExpression="statusdescr">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_1" HeaderText="Sales Rep 1" ReadOnly="True" SortExpression="rep_1"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_2" HeaderText="Sales Rep 2" ReadOnly="True" SortExpression="rep_2"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="rep_3" HeaderText="Sales Rep 3" ReadOnly="True" SortExpression="rep_3"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shiptoname" HeaderText="Ship-to" ReadOnly="True" SortExpression="shiptoname"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr1" HeaderText="Ship-to Address 1" ReadOnly="True"
                                SortExpression="shipToAddr1" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr2" HeaderText="Ship-to Address 2" ReadOnly="True"
                                SortExpression="shipToAddr2" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToAddr3" HeaderText="Ship-to Address 3" ReadOnly="True"
                                SortExpression="shipToAddr3" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToCity" HeaderText="Ship-to City" ReadOnly="True"
                                SortExpression="shipToCity" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToState" HeaderText="Ship-to State" ReadOnly="True"
                                SortExpression="shipToState" Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="shipToZip" HeaderText="Ship-to ZIP" ReadOnly="True" SortExpression="shipToZip"
                                Visible="False">
                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="pagination-sec" style="width: 320px;">
                        <div class="info" style="width: 315px;">
                            <asp:Label ID="lblpageno" runat="server" Text="" />
                        </div>
                    </div>
                </div>
                <br />
                <asp:Panel ID="pnlQuoteDetail" runat="server">
                    <div>
                        <div class="optional" style="display: none">
                            <asp:LinkButton ID="lnkquotedetails" runat="server" CssClass="SettingsLink">Select Columns</asp:LinkButton>
                            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlQuoteDetailCustomView"
                                TargetControlID="lnkquotedetails">
                            </cc1:ModalPopupExtender>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </div>
    </div>
</div>
<asp:Panel ID="pnlQuoteHeaderCustomView" runat="server">
    <div id="pnlQuoteHeaderDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View</h4>
            </div>
            <div class="modal-body">
                <uc3:ColumnSelector ID="csCustomHeader" runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
<div id="pnlQuoteDetailCustomView" runat="server" style="display: none">
    <div id="pnlQuoteDetailDragHeader" runat="server" class="modal-dialog smlwidth">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Columns to View </h4>
                <div class="modal-body">
                    <uc3:ColumnSelector ID="csCustomDetail" runat="server" />
                </div>
            </div>
        </div>
    </div>
</div>
    <cc1:ModalPopupExtender ID="mpeCopyQuote" runat="server" BackgroundCssClass="ModalBackground"
        DropShadow="true" PopupControlID="pnlCopyQuote"
        TargetControlID="btnHidden">
    </cc1:ModalPopupExtender>
    <asp:Panel Width="350px" ID="pnlCopyQuote" runat="server" CssClass="modal-dialog" Style="display: none">
        <asp:Panel ID="pnlDragElement" runat="server" CssClass="modal-content">
            <div class="modal-header" style="height: 45px !important;">
                <h4 class="modal-title" id="myModalLabel" style="width: 95% !important;">Copy Quote</h4>
            </div>
            <div class="modal-body" style="height: 80px !important; vertical-align: middle !important">
                <asp:Label runat="server" ID="lblCopyMessage" Text="Are you sure you want to copy this quote?" CssClass="MarginTop20" /><br />
                <asp:Label runat="server" ID="lblNonstock" Text="<b>Non-stock/special order items will not be copied.</b>" />
            </div>
            <div class="modal-footer" style="width: 100%">
                <div style="float: right">
                    <asp:Button runat="server" ID="btnNo" Text="No" CssClass="btn btnpopup-default TallButton" UseSubmitBehavior="false"  Visible="true" />
                    <asp:Button runat="server" ID="btnCopyQuote" Text="Yes" CssClass="btn btnpopup-primary TallButton" OnClick="btnCopyQuote_Click" OnClientClick="HideCopyQuoteYesButtons()" />
                    <asp:Button runat="server" ID="btnCopyWithRetail" Text="Yes, with Retail Info" CssClass="btn btnpopup-primary TallButton" OnClick="btnCopyWithRetail_Click" OnClientClick="HideCopyQuoteYesButtons()" />
                </div>
            </div>
            <div style="clear: both"></div>
        </asp:Panel>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="mpeConfirmation" runat="server" BackgroundCssClass="ModalBackground"
        DropShadow="true" PopupControlID="pnlinnerorder"
        TargetControlID="btnHidden2">
    </cc1:ModalPopupExtender>
    <asp:Panel Width="500px" ID="pnlinnerorder" runat="server" CssClass="modal-dialog" Style="display: none">
        <asp:Panel ID="Panel1" runat="server" CssClass="modal-content">
            <div class="modal-body">
                <p>
                    <asp:Label ID="lblLine1" runat="server" CssClass="Field" />
                    <asp:Label ID="lblLine2" runat="server" CssClass="Field" />
                    <asp:Label ID="lblError" runat="server" CssClass="ErrorTextRed" />
                </p>
            </div>
            <div class="modal-footer" style="width: 100%">
                <asp:Button Width="68px" ID="btnClose" runat="server" Text="OK" OnClick="btnClose_Click" CssClass="btn btnpopup-primary quotereleasealignright" />
            </div>
            <div style="clear: both"></div>
        </asp:Panel>
    </asp:Panel>
    <asp:LinkButton runat="server" ID="btnHidden" Visible="true" Width="1px" Height="1px" Text="" />
    <asp:LinkButton runat="server" ID="btnHidden2" Visible="true" Width="1px" Height="1px" Text="" />
