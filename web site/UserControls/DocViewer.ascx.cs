﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class UserControls_DocViewer : System.Web.UI.UserControl
{
    private string _OrderStatus;

    public string OrderStatus
    {
        get { return _OrderStatus; }
        set { _OrderStatus = value; }
    }

    public bool _PriceHold;

    public bool PriceHold
    {
        get { return _PriceHold; }
        set { _PriceHold = value; }
    }

    public bool _AllowOrderAck;

    public bool AllowOrderAck
    {
        get { return _AllowOrderAck; }
        set { _AllowOrderAck = value; }
    }

    private string _documentTitle = "Documents";

    public string DocumentTitle
    {
        get { return _documentTitle; }
        set { _documentTitle = value; }
    }

    private string _type;

    public string Type
    {
        get { return _type; }
        set { _type = value; }
    }

    private int _transactionID;

    public int TransactionID
    {
        get { return _transactionID; }
        set { _transactionID = value; }
    }

    private int _shipmentNumber;

    public int ShipmentNumber
    {
        get { return _shipmentNumber; }
        set { _shipmentNumber = value; }
    }

    private string _branchID;

    public string BranchID
    {
        get { return _branchID; }
        set { _branchID = value; }
    }

    private bool _showZeroPriceWarning = false;

    public bool ShowZeroPriceWarning
    {
        get { return _showZeroPriceWarning; }
        set { _showZeroPriceWarning = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Refresh()
    {
        lblDocumentsTitle.Text = DocumentTitle;
        spanZeroPriceWarning.Visible = _showZeroPriceWarning;

        this.Visible = false;
        gvDocView.DataSource = null;
        gvDocView.DataBind();

        if (TransactionID > 0 && Type != "")
        {
            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Documents docs = new Dmsi.Agility.Data.Documents(Session.SessionID, this.BranchID, TransactionID, ShipmentNumber, Type, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            if (docs.ReferencedDataSet != null && docs.ReferencedDataSet.Tables[0].Rows.Count > 0)
            {
                this.Visible = true;
                gvDocView.DataSource = docs.ReferencedDataSet.Tables[0];
            }
            else
            {
                //...add code to create a docs.ReferencedDataSet.Tables[0] row for the XML icon, WHEN viewing SO doc has been removed via Action security...
                if (this.Type == "SalesOrder" && this.OrderStatus != "Canceled")
                {
                    Dmsi.Agility.EntryNET.StrongTypesNS.dsDocumentDataSet ds = new Dmsi.Agility.EntryNET.StrongTypesNS.dsDocumentDataSet();
                    DataRow row = ds.Tables[0].NewRow();
                    row["DocStorageReportName"] = "Sales Order";
                    row["RefNum"] = TransactionID;
                    ds.Tables[0].Rows.Add(row);
                    ds.AcceptChanges();
                    docs.ReferencedDataSet = ds;
                    this.Visible = true;
                    gvDocView.DataSource = docs.ReferencedDataSet.Tables[0];
                }
            }

            gvDocView.DataBind();
            docs.Dispose();
        }
    }

    protected void gvDocView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }

                DataRowView row = (DataRowView)e.Row.DataItem;

                if (i == 2)
                {
                    if (row["PDFUrl"].ToString() != "")
                    {
                        if (Type == "SalesOrder" && !AllowOrderAck)
                        {
                            //nope
                        }
                        else
                        {
                            string pdfURL = "";
                            Label pdf = new Label();
                            pdf.ToolTip = "View PDF";

                            pdfURL = "PdfViewer.aspx?";
                            pdfURL += "ProfName=" + row["ProfName"].ToString();
                            pdfURL += "&TranType=" + row["Type"].ToString();
                            pdfURL += "&DocStorageReportName=" + row["DocStorageReportName"].ToString();
                            pdfURL += "&RefNum=" + row["RefNum"].ToString();
                            pdfURL += "&TranID=" + row["TranID"].ToString();
                            pdfURL += "&ShipmentNum=" + row["ShipmentNum"].ToString();
                            pdfURL += "&Filename=" + row["PDFUrl"].ToString();

                            string altText = GetPdfAltText(row["DocStorageReportName"].ToString());
                            pdf.Text += "<a href=\"" + pdfURL + "\" target=\"_blank\"><img border=0 src=\"Images/pdf_icon.gif\" alt=\"" + altText + "\"></a>";

                            e.Row.Cells[2].Controls.Add(pdf);
                            e.Row.Cells[2].Width = Unit.Pixel(10);
                        }
                    }
                }

                if (i == 3)
                {
                    if (row["DocPopUrl"].ToString() != "")
                    {
                        Label docPop = new Label();
                        docPop.ToolTip = "View Document";
                        string altText = GetDocPopAltText(row["DocStorageReportName"].ToString());
                        docPop.Text = "<a href=" + row["DocPopUrl"].ToString() + " target=\"_blank\"><img border=\"0\" src=\"Images/docpop.gif\" alt=\"" + altText + "\"></a>";
                        e.Row.Cells[3].Controls.Add(docPop);
                        e.Row.Cells[3].Width = Unit.Pixel(10);
                    }
                }

                if (i == 4)
                {
                    if (row["DocStorageReportName"].ToString() == "Order Acknowledgement" || row["DocStorageReportName"].ToString() == "Sales Order")
                    {
                        Label xmlExport = new Label();
                        string altText = "Export XML";
                        xmlExport.Text = "<a href=\"XMLExport.aspx?Type=Order&TranID=" + TransactionID + "\" target=\"_blank\"><img border=\"0\" src=\"Images/xmlexport.gif\" alt=\"" + altText + "\"></a>";
                        xmlExport.ToolTip = "Export XML";
                        e.Row.Cells[4].Controls.Add(xmlExport);
                        e.Row.Cells[4].Width = Unit.Pixel(10);
                    }

                    if (row["DocStorageReportName"].ToString() == "Quotation")
                    {
                        Label xmlExport = new Label();
                        string altText = "Export XML";
                        xmlExport.Text = "<a href=\"XMLExport.aspx?Type=Quote&TranID=" + TransactionID + "\" target=\"_blank\"><img border=\"0\" src=\"Images/xmlexport.gif\" alt=\"" + altText + "\"></a>";
                        xmlExport.ToolTip = "Export XML";
                        e.Row.Cells[4].Controls.Add(xmlExport);
                        e.Row.Cells[4].Width = Unit.Pixel(10);
                    }
                }

                if (i == 5)
                {
                    if (row["PDFUrl"].ToString() != "" && row["DocStorageReportName"].ToString() == "Quotation" && ((string)Session["AllowRetailPricing"]).Trim().ToLower() != "no")
                    {
                        if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                        {
                            string CQpdfURL = "";

                            Label CQpdf = new Label();
                            CQpdf.ToolTip = "View Retail Quote";
                            CQpdfURL = "CQPdfViewer.aspx?";
                            CQpdfURL += "Filename=" + row["PDFUrl"].ToString();
                            CQpdfURL += "&TranID=" + this.TransactionID.ToString();
                            CQpdfURL += "&Type=" + this.Type;
                            CQpdf.Text += "<a href=\"" + CQpdfURL + "\" target=\"_blank\"><img border=0 src=\"Images/form_print.gif\"></a>";

                            e.Row.Cells[5].Controls.Add(CQpdf);
                            e.Row.Cells[5].Width = Unit.Pixel(10);
                        }
                    }
                }
            }
        }
    }

    private static string GetPdfAltText(string reportName)
    {
        string altText = "View Real-time " + reportName;
        return altText;
    }

    private static string GetDocPopAltText(string reportName)
    {
        string altText = "View Archived " + reportName;
        return altText;
    }
}
