﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Payments.ascx.cs" Inherits="UserControls_Payments" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.ListControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.GridControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="igEditors" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControl" %>
<style type="text/css">
    .marginright8 {
        margin-right: 8px;
    }

    .PanelPadding {
        padding-left: 4px;
        padding-right: 4px;
    }

    .NoWrap {
        white-space: nowrap;
    }

    .height40 {
        height: 40px;
    }

    .columnright {
        text-align: right !important;
    }

    .lightgraybackgroundnoimage {
        background-color: #9C9C9C !important;
        background-image: none !important;
    }

    .lightgraybackground {
        background-color: #9C9C9C !important;
    }

    .transparentbackground {
        background-color: transparent !important;
    }

    .valignmiddle {
        vertical-align: middle;
    }

        .valignmiddle img {
            padding-top: 2px;
        }

    .notvisible {
        visibility: hidden;
    }

    .borderright {
        border-right-style: none !important;
        padding: 8px;
    }

    .paddingtop {
        align-content: center;
    }

        .paddingtop img {
            padding-top: 3px !important;
        }

    .BillingFlyout {
        position: absolute;
        left: 0px;
        top: 177px;
        cursor: pointer;
    }

    .left-section-hidden {
        display: none;
    }

    .floatingBox {
        /* Make it stick to the bottom */
        position: fixed;
        bottom: 60px;
        /* styling */
        padding-bottom: 8px;
        padding-right: 20px;
        text-align: right;
        font-size: 16px;
    }

    .floatingBox--fullWidth {
        left: 0;
        bottom: 0;
        right: 0;
        background-color: lightgray;
    }

    .iframecontent {
        vertical-align: middle;
        align-content: center;
    }

    .hideexpansioncolumn {
        display: none;
        visibility: collapse;
    }

    tbody > tr > th.ighg_Office2007SilverExpansionColumnHeader {
        display: none;
        visibility: collapse;
    }

    .marginleft10 {
        margin-left: 8px;
    }

    .selectoverride {
        padding-top: 6px !important;
        margin-top: 20px !important;
    }

    .floatnone {
        float: none !important;
    }

    .yellowbackground {
        background-color: yellow !important;
    }

    .TallButton {
        height: 40px !important;
    }

    input[disabled] {
        background-color:#9C9C9C !important; 
        color:#F0F0F0 !important;
    }

    .LeftMargin15 {
        margin-top: 8px !important;
        margin-left: 15px !important;
        font-family: "Lato", sans-serif;
    }

    .flexStyle {
        display: flex;
        width: 180px;
        text-align: left;
    }

    .warningStyle {
        text-align: left !important;
        text-align-last: left !important;
        line-height: 14px !important;
    }

    .AlignRight {
        text-align:right !important;
    }

    .PaddingRight8 {
        padding-right: 8px;
    }

    .FloatRight {
        float: right !important;
    }

    .MarginRight10 {
        margin-right: 10px;
    }
</style>
<div runat="server" id="BillingLeftSection" class="left-section left-section-hidden">
    <div class="lft-cont-sec">
        <div id="my-tab-content">
            <div class="search-opt">
                <div class="advsearch">
                    <asp:Panel ID="PaymentSearchFieldPanel" runat="server" DefaultButton="btnFind">
                        <asp:Label ID="Label3" runat="server" Text="Search by" CssClass="alladvsearchlabel"></asp:Label>

                        <asp:DropDownList ID="ddlTranID" runat="server" CssClass="select">
                            <asp:ListItem>Invoice ID</asp:ListItem>
                            <asp:ListItem>PO ID</asp:ListItem>
                            <asp:ListItem>Job #</asp:ListItem>
                        </asp:DropDownList>

                        <asp:TextBox ID="txtTranID" runat="server" CssClass="select"></asp:TextBox><br />

                        <asp:Panel ID="SOAdvancedSearchDiv" runat="server" DefaultButton="btnFind">
                            <asp:Label ID="Label2" runat="server" Text="Date" CssClass="alladvsearchlabel"></asp:Label><br />
                            <asp:DropDownList ID="ddlDateChoice" runat="server" CssClass="select">
                                <asp:ListItem>Due Date</asp:ListItem>
                                <asp:ListItem>Invoice Date</asp:ListItem>
                            </asp:DropDownList>
                            <br />

                            <asp:Label ID="Label5" runat="server" Text="From" CssClass="alladvsearchlabel"></asp:Label><br />
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="select"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="cefromdate" runat="server" TargetControlID="txtFromDate"
                                Format="MM/dd/yyyy" PopupButtonID="txtFromDate" Animated="true" CssClass="cal_Theme1" />
                            <br />

                            <asp:Label ID="Label6" runat="server" CssClass="alladvsearchlabel" Text="To"></asp:Label><br />
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="select"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="cetodate" runat="server" TargetControlID="txtToDate"
                                Format="MM/dd/yyyy" PopupButtonID="txtToDate" Animated="true" CssClass="cal_Theme1" />
                            <br clear="all" />
                            <asp:Button ID="btnFind" runat="server" Text="Search" OnClick="btnFind_Click" CssClass="activesbutton btnprodsearch" />

                            <asp:Button ID="btStartOver" runat="server" Text="Start Over"
                                OnClick="btnStartOver_Click" CssClass="inactivesbutton btnprodsearch" />
                            <br />
                        </asp:Panel>

                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right-section">
    <img runat="server" id="BillingFlyoutImage" class="BillingFlyout" alt="Expand/Contract" src="~/Images/forward.png" onclick="BillingFlyout_Click(); return false;" />
    <div class="container-sec">
        <div runat="server" id="divFetchType">
            <h2>
                <asp:Label ID="lblPaymentMethod" runat="server" Text="How would you like to pay?"></asp:Label><br />
            </h2>
            <div style="margin-top: 8px !important">
                <asp:RadioButton runat="server" ID="rdoCC" GroupName="rdoPayments" Text="&nbsp;Credit card" AutoPostBack="True" Checked="True" OnCheckedChanged="rdo_CheckedChanged" Font-Size="Large" CssClass="LeftMargin15" ViewStateMode="Enabled" />
                <asp:RadioButton runat="server" ID="rdoACH" GroupName="rdoPayments" Text="&nbsp;Bank account" AutoPostBack="True" OnCheckedChanged="rdo_CheckedChanged" Font-Size="Large" CssClass="LeftMargin15" ViewStateMode="Enabled" />
                <br />
            </div>
        </div>
        <h2 style="margin-top: 36px !important">
            <asp:Label ID="lblPaymentHeader" runat="server" Text="Account Summary"></asp:Label>
        </h2>
        <div class="custom-grid abc">
            <table style="width: 296px">
                <tr>
                    <td style="padding-bottom: 20px; padding-top: 20px; padding-left: 20px">
                        <ig:WebDataGrid ID="WebDataGridAging" runat="server" Width="400px" AutoGenerateColumns="False" StyleSetName="Office2007Silver" OnInitializeRow="WebDataGridAging_InitializeRow" EnableAjax="False" BorderWidth="0px" OnRowSelectionChanged="WebDataGridAging_RowSelectionChanged">
                            <Columns>
                                <ig:BoundDataField DataFieldName="aging_label" Key="aging_label" Header-CssClass="gridHeader" DataType="System.String">
                                    <Header Text="" CssClass="height40">
                                    </Header>
                                    <Footer CssClass="height40"></Footer>
                                </ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="aging_amt" Key="aging_amt" Header-CssClass="gridHeader" DataFormatString="{0:N2}" DataType="System.Decimal" CssClass="columnright" Width="120px">
                                    <Header Text="Amount">
                                    </Header>
                                    <Footer CssClass="gridHeader"></Footer>
                                </ig:BoundDataField>
                                <ig:BoundDataField DataFieldName="aging_inv_count" Key="aging_inv_count" Header-CssClass="gridHeader" DataType="System.Int32" CssClass="columnright" Width="80px">
                                    <Header Text="Count">
                                    </Header>
                                    <Footer CssClass="gridHeader"></Footer>
                                </ig:BoundDataField>
                            </Columns>
                            <ClientEvents Click="ctl00_WebDataGridAging_Grid_Click" />
                            <Behaviors>
                                <ig:Selection CellClickAction="Row" RowSelectType="Single" SelectedCellCssClass="lightgraybackgroundnoimage" SelectedRowSelectorCssClass="lightgraybackground" EnableCrossPageSelection="True">
                                    <AutoPostBackFlags RowSelectionChanged="True" />
                                </ig:Selection>
                                <ig:RowSelectors RowNumbering="False" Enabled="true" />
                            </Behaviors>
                        </ig:WebDataGrid>
                    </td>
                </tr>
            </table>
            <h2 style="width: 98% !important; margin-top: 24px !important">
                <asp:Label ID="lblAccountDetail" runat="server" Text="Account Detail: All"></asp:Label>
            </h2>
            <ig:WebHierarchicalDataGrid ID="WebHierarchicalDataGridDetail" runat="server" Width="98%" AutoGenerateColumns="False" DataKeyFields="invoice_num,ref_num_seq,ref_num_sysid" OnDataBound="WebHierarchicalDataGridDetail_DataBound" AutoGenerateBands="False" ExpansionColumnCss="hideexpansioncolumn" EnableAjax="False" EnableAjaxViewState="False" EnableDataViewState="True" StyleSetName="Office2007Silver" TabIndex="97">
                <ClientEvents Click="WebHierarchicalDataGridDetail_Grid_Click" Initialize="WebHierarchicalDataGridDetail_Grid_Initialize" MouseDown="WebHierarchicalDataGridDetail_MouseDown" MouseUp="WebHierarchicalDataGridDetail_MouseUp" />
                <Columns>
                    <ig:BoundDataField DataFieldName="show_pdf" Key="show_pdf" Hidden="True">
                        <Header Text="Branch" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="branch" Key="branch" Hidden="True">
                        <Header Text="Branch" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="ship_num" Key="ship_num" Hidden="True">
                        <Header Text="Shipment Number" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="tran_id" Key="tran_id" Hidden="True">
                        <Header Text="Tran ID" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="ref_type" Key="ref_type" Hidden="True">
                        <Header Text="Ref Type" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:UnboundCheckBoxField Width="30px" Key="PayThis" HeaderCheckBoxMode="Off" CssClass="valignmiddle">
                    </ig:UnboundCheckBoxField>
                    <ig:BoundDataField DataFieldName="invoice_num" Key="invoice_num" CssClass="NoWrap" Width="140px">
                        <Header Text="Invoice ID" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:TemplateDataField Width="33px" Key="document" CssClass="NoWrap">
                        <ItemTemplate>
                            <asp:Label CssClass="paddingtop" runat="server" ID="lblPDF" />
                        </ItemTemplate>
                        <Header Text="" CssClass="gridHeader" />

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:TemplateDataField>
                    <ig:BoundDataField DataFieldName="cust_po" Key="cust_po">
                        <Header Text="PO ID" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="job" Key="job">
                        <Header Text="Job #" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="invoice_date" Key="invoice_date" DataType="System.DateTime">
                        <Header Text="Invoice Date" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="due_date" Key="due_date" DataType="System.DateTime">
                        <Header Text="Due Date" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="shipto_seq" Key="shipto_seq">
                        <Header Text="Ship-to Seq #" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="shipto_name" Key="shipto_name">
                        <Header Text="Ship-to Name" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="shipto_addr1" Key="shipto_addr1">
                        <Header Text="Ship-to Address" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="aging_label" Key="aging_label">
                        <Header Text="Aging" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="original_amt" Key="original_amt" DataFormatString="{0:N2}" DataType="System.Decimal" CssClass="columnright">
                        <Header Text="Original Amt" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="open_amt" Key="open_amt" DataFormatString="{0:N2}" DataType="System.Decimal" CssClass="columnright">
                        <Header Text="Unpaid Balance" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="discount_amt" Key="discount_amt" DataFormatString="{0:N2}" DataType="System.Decimal" CssClass="columnright">
                        <Header Text="Available Discount Amt" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="balance_due" Key="balance_due" DataFormatString="{0:N2}" DataType="System.Decimal" CssClass="columnright">
                        <Header Text="Balance Due" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="payment_amt" Key="payment_amt" DataFormatString="{0:N2}" DataType="System.Decimal" CssClass="columnright">
                        <Header Text="Amount to Pay" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="ref_num_sysid" Key="ref_num_sysid" Hidden="True">
                        <Header Text="" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="apply_surcharge" Key="apply_surcharge" Hidden="True">
                        <Header Text="" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="surcharge_basis_type" Key="surcharge_basis_type" Hidden="True">
                        <Header Text="" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                    <ig:BoundDataField DataFieldName="surcharge_basis_amt" Key="surcharge_basis_amt" Hidden="True">
                        <Header Text="" CssClass="gridHeader">
                        </Header>

                        <Footer CssClass="gridHeader"></Footer>
                    </ig:BoundDataField>
                </Columns>
                <Behaviors>
                    <ig:Sorting SortingMode="Multi">
                        <ColumnSettings>
                            <ig:SortingColumnSetting ColumnKey="PayThis" Sortable="False" />
                            <ig:SortingColumnSetting ColumnKey="document" Sortable="False" />
                        </ColumnSettings>
                        <SortingClientEvents ColumnSorting="WebHierarchicalDataGridDetail_ClientColumnSorting" />
                    </ig:Sorting>
                    <ig:EditingCore AutoCRUD="False">
                        <Behaviors>
                            <ig:CellEditing>
                                <ColumnSettings>
                                    <ig:EditingColumnSetting ColumnKey="PayThis" />
                                    <ig:EditingColumnSetting ColumnKey="invoice_num" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="document" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="cust_po" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="job" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="invoice_date" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="due_date" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="shipto_seq" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="shipto_name" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="shipto_addr1" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="aging_label" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="original_amt" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="open_amt" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="discount_amt" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="balance_due" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="branch" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="ship_num" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="tran_id" ReadOnly="True" />
                                    <ig:EditingColumnSetting ColumnKey="payment_amt" ReadOnly="False" />
                                </ColumnSettings>
                                <EditModeActions EnableF2="False" MouseClick="Single" />
                                <CellEditingClientEvents EnteringEditMode="enteringEditMode" ExitingEditMode="exitingEditMode" ExitedEditMode="exitedEditMode" />
                            </ig:CellEditing>
                        </Behaviors>
                    </ig:EditingCore>
                </Behaviors>
            </ig:WebHierarchicalDataGrid>
            <div runat="server" id="divFloatingPay" class="floatingBox floatingBox--fullWidth">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 98%">
                    <tr align="right">
                        <td align="left" style="padding-top: 10px; line-height: 11px; width: 100%; padding-left: 8px">
                            <asp:Label runat="server" Font-Size="7px" ID="lblDisclaimer"
                                Text="Credit card payments are posted to your account upon approval. Contact our office for further assistance or to cancel a payment.<br/>All confidential information is transmitted using https protocol.<br/>We use the personal information you provide only to safely process your online payment." />
                        </td>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 750px">
                                <tr>
                                    <td align="left" style="width: 100px; padding-top: 10px">
                                        <asp:Label ID="lblSurchargeTotalLabel" runat="server" Text="CC Surcharge Fee" Font-Size="11px" CssClass="NoWrap PaddingRight8" />
                                    </td>
                                    <td align="right" style="padding-top: 10px">
                                        <asp:Label ID="lblSurchargeTotal" runat="server" Text="$0.00" Font-Size="11px" CssClass="AlignRight" />
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td align="left" style="width: 130px; padding-top: 10px">
                                        <asp:Label ID="lblGrandTotalLabel" runat="server" Text="Total Payment Amount" Font-Size="11px" CssClass="NoWrap PaddingRight8" />
                                    </td>
                                    <td align="right" style="padding-top: 10px">
                                        <asp:Label ID="lblGrandTotal" runat="server" Text="$0.00" Font-Size="11px" CssClass="AlignRight PaddingRight8" />
                                    </td>
                                    <td align="right" style="padding-right: 8px">
                                        <asp:Button runat="server" ID="btnApplyCredits" Text="Apply Credits" CssClass="activesbutton btnprodsearch floatnone FloatRight" OnClick="btnApplyCredits_Click" OnClientClick="HidePayButtons()" />
                                    </td>
                                    <td align="right" style="padding-right: 8px">
                                        <asp:Button runat="server" ID="btnLumpSumPayment" Text="Payment on Account" CssClass="activesbutton btnprodsearch floatnone FloatRight" OnClick="btnLumpSumPayment_Click" />
                                    </td>
                                    <td align="right">
                                        <asp:Button Enabled="false" OnClick="btnPay_Click" OnClientClick="HidePayButtons()" runat="server" ID="btnPay" Text="Pay Via Credit Card" CssClass="activesbutton btnprodsearch FloatRight" TabIndex="100" />
                                    </td>
                                    <td align="right">
                                        <asp:Button Enabled="false" OnClick="btnPayBank_Click" OnClientClick="HidePayButtons()" runat="server" ID="btnPayBank" Text="Pay Via Bank Account" CssClass="activesbutton btnprodsearch marginright8 FloatRight" TabIndex="101" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <AjaxControl:ModalPopupExtender ID="mpeCC" runat="server" BackgroundCssClass="ModalBackground"
        DropShadow="true" PopupControlID="pnlElementSite"
        TargetControlID="btnHidden">
    </AjaxControl:ModalPopupExtender>
    <asp:Panel Width="430px" ID="pnlElementSite" runat="server" CssClass="modal-dialog" Style="display: none">
        <asp:Panel ID="pnlDragElement" runat="server" CssClass="modal-content">
            <div class="modal-header" style="height: 45px !important;">
                <h4 class="modal-title" id="myModalLabel" style="width: 95% !important;">Credit Card Payment</h4>
            </div>
            <div class="modal-body" style="height: 280px !important; vertical-align: middle !important">
                <iframe runat="server" id="iframeElement" frameborder="0" align="middle" height="250px" width="400px" class="iframecontent" />
            </div>
            <div class="modal-footer" style="width: 100%">
                <div style="float: left" class="flexStyle">
                    <asp:Label runat="server" ID="lblWarning1" Text="You must press the Close button to apply an approved payment." CssClass="warningStyle" />
                </div>
                <div style="float: right">
                    <asp:Button runat="server" ID="Button1" Text="Cancel" CssClass="btn btnpopup-default TallButton" UseSubmitBehavior="False" Visible="false" />
                    <asp:Button runat="server" ID="btnExitElement" Text="Close" CssClass="btn btnpopup-primary TallButton" OnClick="btnExitElement_Click" OnClientClick="ElementHideCloseButton()" />
                </div>
            </div>
            <div style="clear: both"></div>
        </asp:Panel>
    </asp:Panel>
    <asp:LinkButton runat="server" ID="btnHidden" Visible="true" Width="1px" Height="1px" Text="" />
    <asp:HiddenField ID="HiddenField" runat="server" />
    <asp:HiddenField ID="HiddenField2" runat="server" />
    <asp:HiddenField ID="HiddenField3" runat="server" />
    <asp:HiddenField ID="HiddenField4" runat="server" />
    <asp:HiddenField ID="HiddenFieldCCSurchargeTotal" runat="server" />
    <asp:HiddenField ID="HiddenFieldSurchargeUsage" runat="server" />
    <asp:HiddenField ID="HiddenFieldSurchargeType" runat="server" />
    <asp:HiddenField ID="HiddenFieldSurchargeValue" runat="server" />
    <AjaxControl:ModalPopupExtender ID="mpeACH" runat="server" BackgroundCssClass="ModalBackground"
        DropShadow="true" PopupControlID="pnlACHSite"
        TargetControlID="btnHidden">
    </AjaxControl:ModalPopupExtender>
    <asp:Panel Width="430px" ID="pnlACHSite" runat="server" CssClass="modal-dialog" Style="display: none">
        <asp:Panel ID="pnlACHDrag" runat="server" CssClass="modal-content">
            <div class="modal-header" style="height: 45px !important; padding-left: 20px">
                <h4 class="modal-title" id="ACHTitle" style="width: 95% !important;">Bank Account</h4>
            </div>
            <div class="modal-body" style="height: 280px !important; vertical-align: middle !important">
                <div class="lft-cont-sec">
                    <div class="search-opt">
                        <div class="advsearch">
                            <asp:DropDownList onchange="ddlBankAccount_SelectionChanged(this);" ID="ddlBankAccount" runat="server" Width="384px" CssClass="select selectoverride">
                                <asp:ListItem>Select bank account</asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <br />
                            <asp:Label runat="server" ID="lblBankTotal" Text="Amount: " CssClass="marginleft10" />
                            <br />
                            <br />
                            <asp:Label runat="server" ID="lblNoBanks" Text="No accounts stored for invoice(s) selected for payment." CssClass="marginleft10" Visible="false" ForeColor="Red" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="width: 100%">
                <div style="float: right">
                    <asp:Button runat="server" ID="btnACHCancel" Text="Cancel" CssClass="btn btnpopup-default TallButton" OnClick="btnACHCancel_Click" OnClientClick="ACHHideCloseButtons()" />
                    <asp:Button runat="server" ID="btnACHPay" Enabled="false" Text="Pay" CssClass="btn btnpopup-primary TallButton" OnClick="btnACHPay_Click" OnClientClick="ACHHideCloseButtons()" />
                </div>
            </div>
            <div style="clear: both"></div>
        </asp:Panel>
    </asp:Panel>
    <AjaxControl:ModalPopupExtender ID="mpeFixedPayment" runat="server" BackgroundCssClass="ModalBackground"
        DropShadow="true" PopupControlID="pnlFixedPayment"
        TargetControlID="btnHidden">
    </AjaxControl:ModalPopupExtender>
    <asp:Panel Width="430px" ID="pnlFixedPayment" runat="server" CssClass="modal-dialog" Style="display: none">
        <asp:Panel ID="pntFixedPaymentDrag" runat="server" CssClass="modal-content">
            <div class="modal-header" style="height: 45px !important; padding-left: 20px">
                <h4 class="modal-title" id="FixedPaymentTitle" style="width: 95% !important;">Payment on Account</h4>
            </div>
            <div class="modal-body" style="height: 280px !important; vertical-align: middle !important">
                <div class="lft-cont-sec">
                    <div class="search-opt">
                        <div class="advsearch">
                            <br />
                            <asp:Label runat="server" ID="Label1" Text="Enter amount to be applied as cash on account: " CssClass="marginleft10" /><br />
                            <asp:Label runat="server" ID ="labelDollar" Text="$" CssClass="marginleft10" />
                            <igEditors:WebNumericEditor runat="server" ID="wneFixedAmount" Width="130px" MinDecimalPlaces="2" MinValue="0" TabIndex="1" CssClass="marginleft10" ClientEvents-TextChanged="wneFixedAmountChanged" MaxLength="12" DataMode="Decimal">
                                <ClientEvents TextChanged="wneFixedAmount_TextChanged" Blur="wneFixedAmount_Blur" />
                            </igEditors:WebNumericEditor>
                            <br />
                            <br />
                            <table runat="server" id="tblSurcharge" border="0" style="margin-left: 10px">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID ="lblSurchargeAmtLabel" Text="CC Surcharge Fee" />
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label runat="server" ID ="lblSurchargeAmt" Text="$0.00" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID ="lblTotalPaymentLabel" Text="Total Payment Amount" CssClass="MarginRight10" />
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label runat="server" ID ="lblTotalPayment" Text="$0.00" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="width: 100%">
                <div style="float: right">
                    <asp:Button runat="server" ID="btnFixedPayCancel" Text="Cancel" CssClass="btn btnpopup-default TallButton" OnClick="btnACHCancel_Click" OnClientClick="FixedPayHideCloseButtons()" TabIndex="2" />
                    <asp:Button runat="server" Enabled="false" ID="btnFixedPayCC" Text="Pay Via Credit Card" CssClass="btn btnpopup-primary TallButton" OnClick="btnFixedPayCC_Click" OnClientClick="FixedPayHideCloseButtons()" TabIndex="3" />
                    <asp:Button runat="server" Enabled="false" ID="btnFixedPayACH" Text="Pay Via Bank Account" CssClass="btn btnpopup-primary TallButton" OnClick="btnFixedPayACH_Click" OnClientClick="FixedPayHideCloseButtons()" TabIndex="4" />
                </div>
            </div>
            <div style="clear: both"></div>
        </asp:Panel>
    </asp:Panel>
</div>



