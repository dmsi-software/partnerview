﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dmsi.Agility.EntryNET;

public partial class UserControls_MessagesMaint : System.Web.UI.UserControl
{
    private enum Direction
    { Up, Down }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Load Messages
        if (!this.IsPostBack)
        {
            AssignDataSource(SqlDataSource1, false, gvCategories);
            AssignMessagesDataSource();
        }
    }
    private void AssignMessagesDataSource()
    {
        if (Session["CurrentCategory"] != null)
        {
            DataRow categoryRow = (DataRow)Session["CurrentCategory"];
            SqlDataSource2.SelectParameters["Category_Name"].DefaultValue = categoryRow["Category_Name"].ToString();
            AssignDataSource(SqlDataSource2, false, gvMessages);
            lblMessages.Text = "Messages for " + categoryRow["Category_Name"].ToString();
        }
        else
        {
            AssignDataSource(SqlDataSource2, true, gvMessages);
            lblMessages.Text = "Messages";
        }

        this.btnUpMessage.Visible = Session["CurrentMessage"] != null;
        this.btnDownMessage.Visible = Session["CurrentMessage"] != null;
    }
    private void AssignDataSource(SqlDataSource source, bool empty, GridView gv)
    {
        DataTable dt = new DataTable(gv.ID);
        DataView originalDataView = (DataView)source.Select(DataSourceSelectArguments.Empty);

        if (originalDataView != null)
        {
            dt = originalDataView.ToTable("TableCopy"); // this will give us the correct column arrangement
        }
        else
        {
            // TODO: Have to change this to build the columns base on the database table definition
            if (gv.ID == "gvCategories")
            {
                dt.Columns.Add("Sequence_Number");
                dt.Columns.Add("Category_Name");
                dt.Columns.Add("Active");
            }
            else
            {
                dt.Columns.Add("Sequence_Number");
                dt.Columns.Add("Category_Name");
                dt.Columns.Add("Message_Text");
                dt.Columns.Add("Active");
            }
        }

        if (dt.Rows.Count > 0 && !empty)
        {
            if (gv.ID == "gvCategories")
            {

                if (Session["IsmessageCatchanged"] == null)
                {
                    Session["CurrentCategory"] = dt.Rows[0];
                    Session["Categories"] = dt;
                }
                else
                {
                    if ((string)Session["IsmessageCatchanged"] == "yes")
                    {
                        dt = (DataTable)Session["Categories"];
                        Session["IsmessageCatchanged"] = null;
                    }
                }
                btnUpCategory.Visible = true;
                btnDownCategory.Visible = true;
                btnAddMessage.Visible = true;
                gvMessages.Visible = true;
            }
            else
            {
                Session["CurrentMessage"] = dt.Rows[0];
                Session["Messages"] = dt;
            }
            DataRow dr = (DataRow)Session["CurrentCategory"];
            gv.DataSource = dt;
            gv.DataBind();
            if (Session["Catogery_SelectedInndex"] != null)
            {
                gvCategories.SelectedIndex = (int)Session["Catogery_SelectedInndex"];
            }
        }
        else
        {
            // this is a work around so the grid will show when the datasource is empty
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);

            if (gv.ID == "gvCategories")
            {
                gv.Rows[0].Cells[0].ColumnSpan = 1;
                Session.Remove("CurrentCategory");
                Session.Remove("Categories");
                btnUpCategory.Visible = false;
                btnDownCategory.Visible = false;
                btnAddMessage.Visible = false;
                gvMessages.Visible = false;
            }
            else
            {
                gv.Rows[0].Cells[0].ColumnSpan = 1;
                Session.Remove("CurrentMessage");
                Session.Remove("Messages");
            }
        }
    }
    protected void btnDone_Click(object sender, EventArgs e)
    {
        // Save Messages
        Redirect();
    }
    private void Redirect()
    {
        int PasswordReturnCode = Convert.ToInt32(Session["passRC"]);
        string urlName = Convert.ToString(Session["Pagefrom"]);

        switch (PasswordReturnCode)
        {
            case -30:
            case -40:
                Response.Redirect("./TimedOut.aspx");
                break;

            case -50:
                Session["passRC"] = 0;
                Session["LoggedIn"] = true;
                Response.Redirect("default.aspx?Display=Customers");
                break;

            default:
                Session["IsHome"] = true;
                Response.Redirect("default.aspx?Display=Home");
                break;

        }
    }

    protected void gvCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        string key = "";
        GridView gv = (GridView)sender;

        key = gv.SelectedDataKey.Value.ToString();
        int selectedrow = gv.SelectedRow.RowIndex;
        Session["Catogery_SelectedInndex"] = selectedrow;
        if (Session["Categories"] != null)
        {
            DataTable dt = (DataTable)Session["Categories"];
            DataRow[] categoryRow = dt.Select("Category_Name='" + key + "'");
            Session["CurrentCategory"] = categoryRow[0];
            AssignMessagesDataSource();

            btnUpCategory.Enabled = (int)categoryRow[0]["Sequence_Number"] > 1;
            btnDownCategory.Enabled = (int)categoryRow[0]["Sequence_Number"] < gv.Rows.Count;
            btnUpMessage.Enabled = false;
            btnDownMessage.Enabled = false;
            gvMessages.SelectedIndex = -1;

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Messages - Category Select Link";
            aInput.Label = "Category = " + categoryRow[0]["Category_Name"].ToString() + " Active = " + categoryRow[0]["Active"].ToString(); ;
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        HtmlDiv.Visible = false;
    }
    protected void gvCategories_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        RowDataBound(e, "ActiveCategory");
    }
    protected void gvMessages_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[e.Row.Cells.Count - 2].Text.Length > 60)
                e.Row.Cells[e.Row.Cells.Count - 2].Text = e.Row.Cells[e.Row.Cells.Count - 2].Text.Substring(0, 60);


            System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[0]));
            System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[2]));
            imgedit.ToolTip = "Edit";
            imgDelete.ToolTip = "Delete";
            RowDataBound(e, "ActiveMessage");
        }
        if (e.Row.RowState == DataControlRowState.Edit)
        {
            System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[0]));
            System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[2]));
            imgedit.ToolTip = "Save";
            imgDelete.ToolTip = "Cancel";
        }
    }
    private static void RowDataBound(GridViewRowEventArgs e, string checkBoxName)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }

                if (i == 2)
                {
                    CheckBox box = (CheckBox)e.Row.Cells[i].FindControl(checkBoxName);
                    DataRow currRow = ((DataRowView)e.Row.DataItem).Row;

                    if (currRow["Active"].ToString() != "")
                        box.Checked = (bool)currRow["Active"];
                    else
                        box.Checked = false;
                }
            }
            System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[0]));
            System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[2]));
            imgedit.ToolTip = "Edit";
            imgDelete.ToolTip = "Delete";

        }
        if (e.Row.RowState == DataControlRowState.Edit)
        {
            System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[0]));
            System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[2]));
            imgedit.ToolTip = "Update";
            imgDelete.ToolTip = "Cancel";
        }
    }
    protected void btnAddCategory_Click(object sender, EventArgs e)
    {
        if (this.txtCategory.Text != "")
        {
            int seq = 1;

            if (Session["Categories"] != null)
                seq = ((DataTable)Session["Categories"]).Rows.Count + 1;

            try
            {
                Dmsi.Agility.EntryNET.SecurityManager sm = new Dmsi.Agility.EntryNET.SecurityManager();
                this.txtCategory.Text = sm.InputFilter(this.txtCategory.Text, Dmsi.Agility.EntryNET.FilterFlag.NoMarkup);
                sm = null;

                SqlDataSource1.InsertParameters["Sequence_Number"].DefaultValue = seq.ToString();
                SqlDataSource1.InsertParameters["Category_Name"].DefaultValue = this.txtCategory.Text.Replace("'", "");
                SqlDataSource1.Insert();

                AssignDataSource(SqlDataSource1, false, gvCategories);
                DataTable dt = (DataTable)Session["Categories"];
                if (dt != null)
                {
                    if (dt.Rows.Count > 1)
                    {
                        Session["CurrentCategory"] = dt.Rows[dt.Rows.Count - 1];
                        gvCategories.SelectedIndex = seq - 1;
                        AssignMessagesDataSource();
                    }
                }

                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Messages - Category Add Button";
                aInput.Label = this.txtCategory.Text;
                aInput.Value = "0";

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();
                this.lblCategoryError.Text = "";
                this.txtCategory.Text = "";
            }
            catch (Exception ex)
            {
                this.lblCategoryError.Text = ex.Message;
            }
        }
        else
            this.lblCategoryError.Text = "Category is blank.";
    }
    protected void gvCategories_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvCategories.EditIndex = e.NewEditIndex;
        AssignDataSource(SqlDataSource1, false, gvCategories);
        System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(gvCategories.Rows[e.NewEditIndex].Cells[0].Controls[0]));
        System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(gvCategories.Rows[e.NewEditIndex].Cells[0].Controls[2]));

        imgedit.ToolTip = "Save";
        imgDelete.ToolTip = "Cancel";
    }
    protected void gvCategories_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvCategories.EditIndex = -1;
        AssignDataSource(SqlDataSource1, false, gvCategories);
    }
    protected void gvCategories_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        DataControlFieldCell cell = gvCategories.Rows[e.RowIndex].Cells[3] as DataControlFieldCell;

        gvCategories.Columns[2].ExtractValuesFromCell(
            e.NewValues,
            cell,
            DataControlRowState.Edit,
            true);

        string newCategory = e.NewValues[0] == null ? "" : e.NewValues[0].ToString();
        newCategory = newCategory.Replace("'", "");

        try
        {
            // OldValues is not getting updated correctly
            DataTable dt = (DataTable)Session["Categories"];
            DataRow[] categoryRow = dt.Select("Sequence_Number=" + (e.RowIndex + 1).ToString());

            SqlDataSource1.UpdateParameters["Old_Category_Name"].DefaultValue = categoryRow[0]["Category_Name"].ToString();
            SqlDataSource1.UpdateParameters["Sequence_Number"].DefaultValue = categoryRow[0]["Sequence_Number"].ToString();
            SqlDataSource1.UpdateParameters["Active"].DefaultValue = categoryRow[0]["Active"].ToString();
            SqlDataSource1.UpdateParameters["New_Category_Name"].DefaultValue = newCategory;
            SqlDataSource1.Update(); // This will cascade to all other tables using this as foreign key

            this.lblCategoryError.Text = "";
        }
        catch (Exception ex)
        {
            this.lblCategoryError.Text = ex.Message;
        }
        finally
        {
            gvCategories.EditIndex = -1;

            // AssignDataSource will reset the value
            DataRow currCategory = (DataRow)Session["CurrentCategory"];

            // The currently selected DataRow must be updated with the
            // new value if this is the same row as the one being updated.
            if (currCategory != null &&
                currCategory["Sequence_Number"].ToString() == (e.RowIndex + 1).ToString())
            {
                currCategory["Category_Name"] = newCategory;
            }

            AssignDataSource(SqlDataSource1, false, gvCategories);
            if (currCategory != null)
                Session["CurrentCategory"] = currCategory;
            AssignMessagesDataSource();
        }

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Messages - Category Edit Icon";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

    }
    protected void gvCategories_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            SqlDataSource1.DeleteParameters["Category_Name"].DefaultValue = gvCategories.DataKeys[e.RowIndex].Values["Category_Name"].ToString();
            SqlDataSource1.Delete(); // This will cascade to all other tables using this as foreign key

            // Renumber categories
            string updateCommand = SqlDataSource1.UpdateCommand;
            for (int i = e.RowIndex + 1; i < gvCategories.Rows.Count; i++)
            {
                SqlDataSource1.UpdateCommand = "UPDATE Category"
                                                 + " SET Sequence_Number=" + i.ToString()
                                                 + " WHERE (Sequence_Number=" + (i + 1).ToString() + ")";
                SqlDataSource1.Update();
            }
            SqlDataSource1.UpdateCommand = updateCommand;

            this.lblCategoryError.Text = "";
        }
        catch (Exception ex)
        {
            this.lblCategoryError.Text = ex.Message;
        }
        finally
        {
            gvCategories.EditIndex = -1;

            // AssignDataSource will reset the value
            DataRow currCategory = (DataRow)Session["CurrentCategory"];

            // The currently selected DataRow must be updated with the
            // new value if this is the same row as the one being updated.
            if (currCategory != null &&
                currCategory["Category_Name"].ToString() == gvCategories.DataKeys[e.RowIndex].Values["Category_Name"].ToString())
            {
                Session.Remove("CurrentCategory");
                currCategory = null;
                HtmlDiv.Visible = false;
                btnUpCategory.Enabled = false;
                btnDownCategory.Enabled = false;
                gvCategories.SelectedIndex = -1;
            }

            AssignDataSource(SqlDataSource1, false, gvCategories);

            if (currCategory != null)
                Session["CurrentCategory"] = currCategory;

            AssignMessagesDataSource();
        }

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Messages - Category Delete Icon";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

    }
    protected void gvMessages_SelectedIndexChanged(object sender, EventArgs e)
    {
        string key = "";
        GridView gv = (GridView)sender;

        key = gv.SelectedDataKey.Value.ToString();

        if (Session["Messages"] != null)
        {
            DataRow categoryRow = SetCurrentMessageRow(gv.SelectedIndex + 1);
            lblHtmlViewOfMessage.Text = categoryRow["Message_Text"].ToString();
            HtmlDiv.Visible = true;
            btnUpMessage.Enabled = gv.SelectedIndex + 1 > 1;
            btnDownMessage.Enabled = gv.SelectedIndex + 1 < gvMessages.Rows.Count;
        }
    }
    private DataRow SetCurrentMessageRow(int index)
    {
        DataTable dt = (DataTable)Session["Messages"];
        DataRow[] categoryRow = dt.Select("Sequence_Number=" + index.ToString());
        Session["CurrentMessage"] = categoryRow[0];
        return categoryRow[0];
    }
    private void MessageListView()
    {
        MessageListDiv.Visible = true;
        MessageListDivheaders.Visible = true;
    }
    protected void btnCancelEdit_Click(object sender, EventArgs e)
    {
        MessageListView();
        AssignMessagesDataSource();
    }
    protected void gvMessages_RowEditing(object sender, GridViewEditEventArgs e)
    {
        SetCurrentMessageRow(e.NewEditIndex + 1);
        DataRow row = (DataRow)Session["CurrentMessage"];
        System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(gvMessages.Rows[e.NewEditIndex].Cells[0].Controls[0]));
        System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(gvMessages.Rows[e.NewEditIndex].Cells[0].Controls[2]));

        imgedit.ToolTip = "Save";
        imgDelete.ToolTip = "Cancel";

        e.Cancel = true;
        Session["messageupdate"] = "true";

        Response.Redirect("~/UpdateMessages.aspx");

    }
    protected void gvMessages_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            SqlDataSource2.DeleteParameters["Category_Name"].DefaultValue = gvMessages.DataKeys[e.RowIndex].Values["Category_Name"].ToString();
            SqlDataSource2.DeleteParameters["Sequence_Number"].DefaultValue = gvMessages.DataKeys[e.RowIndex].Values["Sequence_Number"].ToString();
            SqlDataSource2.Delete();

            // Renumber categories
            string updateCommand = SqlDataSource2.UpdateCommand;
            for (int i = e.RowIndex + 1; i < gvMessages.Rows.Count; i++)
            {
                SqlDataSource2.UpdateCommand = "UPDATE Message"
                                                 + " SET Sequence_Number=" + i.ToString()
                                                 + " WHERE (Category_Name='" + gvMessages.DataKeys[e.RowIndex].Values["Category_Name"].ToString()
                                                 + "') AND (Sequence_Number=" + (i + 1).ToString() + ")";
                SqlDataSource2.Update();
            }
            SqlDataSource2.UpdateCommand = updateCommand;

            this.lblMessageError.Text = "";
        }
        catch (Exception ex)
        {
            this.lblMessageError.Text = ex.Message;
        }
        finally
        {
            gvMessages.EditIndex = -1;

            // AssignDataSource will reset the value
            DataRow currMessage = (DataRow)Session["CurrentMessage"];

            // The currently selected DataRow must be updated with the
            // new value if this is the same row as the one being deleted.
            if (currMessage != null &&
                currMessage["Sequence_Number"].ToString() == gvMessages.DataKeys[e.RowIndex].Values["Sequence_Number"].ToString())
            {
                Session.Remove("CurrentMessage");
                currMessage = null;
                HtmlDiv.Visible = false;
                btnDownMessage.Enabled = false;
                btnUpMessage.Enabled = false;
                gvMessages.SelectedIndex = -1;
            }

            if (currMessage != null)
                Session["CurrentMessage"] = currMessage;

            AssignMessagesDataSource();
        }

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Messages - Messages for Categories - Delete Icon";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

    }
    protected void btnAddMessage_Click(object sender, EventArgs e)
    {
        // RichTextEditor use HTML submit to process data.
        // We will just show the editor then catch the postback later
        // when the user click on save
        DataRow row = (DataRow)Session["CurrentCategory"];
        Session["MessageAddMode"] = true;

        Response.Redirect("~/UpdateMessages.aspx");
    }
    protected void btnUpCategory_Click(object sender, EventArgs e)
    {
        MoveCategory(Direction.Up);
    }
    private void MoveCategory(Direction movement)
    {
        int direction = movement == Direction.Up ? -1 : 1;

        if (Session["CurrentCategory"] != null)
        {
            DataRow row = (DataRow)Session["CurrentCategory"];
            int selectedIndex = (int)row["Sequence_Number"];

            if ((selectedIndex > 1 && movement == Direction.Up) ||
                (selectedIndex < gvCategories.Rows.Count && movement == Direction.Down))
            {
                string updateCommand = SqlDataSource1.UpdateCommand;
                // Set the above row to negative
                SqlDataSource1.UpdateCommand = "UPDATE Category"
                                             + " SET Sequence_Number=" + (-(selectedIndex + direction)).ToString()
                                             + " WHERE (Sequence_Number=" + (selectedIndex + direction).ToString() + ")";
                SqlDataSource1.Update();

                // Set current row to the new index
                SqlDataSource1.UpdateCommand = "UPDATE Category"
                                             + " SET Sequence_Number=" + (selectedIndex + direction).ToString()
                                             + " WHERE (Sequence_Number=" + selectedIndex.ToString() + ")";
                SqlDataSource1.Update();
                row["Sequence_Number"] = selectedIndex + direction;

                // Complete the swap
                SqlDataSource1.UpdateCommand = "UPDATE Category"
                                             + " SET Sequence_Number=" + selectedIndex.ToString()
                                             + " WHERE (Sequence_Number=" + (-(selectedIndex + direction)).ToString() + ")";

                SqlDataSource1.Update();

                SqlDataSource1.UpdateCommand = updateCommand;
                Session["Catogery_SelectedInndex"] = selectedIndex + direction - 1;
                gvCategories.SelectedIndex = selectedIndex + direction - 1;
                AssignDataSource(SqlDataSource1, false, gvCategories);
                Session["CurrentCategory"] = row;

                AssignMessagesDataSource();
            }

            btnUpCategory.Enabled = selectedIndex + direction > 1;
            btnDownCategory.Enabled = selectedIndex + direction < gvCategories.Rows.Count;
        }
    }
    protected void btnDownCategory_Click(object sender, EventArgs e)
    {
        MoveCategory(Direction.Down);
    }
    protected void btnUpMessage_Click(object sender, EventArgs e)
    {
        MoveMessage(Direction.Up);
    }
    protected void btnDownMessage_Click(object sender, EventArgs e)
    {
        MoveMessage(Direction.Down);
    }
    private void MoveMessage(Direction movement)
    {
        int direction = movement == Direction.Up ? -1 : 1;

        if (Session["CurrentMessage"] != null)
        {
            DataRow currCategory = (DataRow)Session["CurrentCategory"];
            DataRow row = (DataRow)Session["CurrentMessage"];
            int selectedIndex = (int)row["Sequence_Number"];

            if ((selectedIndex > 1 && movement == Direction.Up) ||
                (selectedIndex < gvMessages.Rows.Count && movement == Direction.Down))
            {
                string updateCommand = SqlDataSource2.UpdateCommand;
                // Set the above row to negative
                SqlDataSource2.UpdateCommand = "UPDATE Message"
                                             + " SET Sequence_Number=" + (-(selectedIndex + direction)).ToString()
                                             + " WHERE (Category_Name='" + currCategory["Category_Name"].ToString() + "')"
                                             + " AND (Sequence_Number=" + (selectedIndex + direction).ToString() + ")";
                SqlDataSource2.Update();

                // Set current row to the new index
                SqlDataSource2.UpdateCommand = "UPDATE Message"
                                             + " SET Sequence_Number=" + (selectedIndex + direction).ToString()
                                             + " WHERE (Category_Name='" + currCategory["Category_Name"].ToString() + "')"
                                             + " AND (Sequence_Number=" + selectedIndex.ToString() + ")";
                SqlDataSource2.Update();
                row["Sequence_Number"] = selectedIndex + direction;

                // Complete the swap
                SqlDataSource2.UpdateCommand = "UPDATE Message"
                                             + " SET Sequence_Number=" + selectedIndex.ToString()
                                             + " WHERE (Category_Name='" + currCategory["Category_Name"].ToString() + "')"
                                             + " AND (Sequence_Number=" + (-(selectedIndex + direction)).ToString() + ")";

                SqlDataSource2.Update();

                SqlDataSource2.UpdateCommand = updateCommand;
                gvMessages.SelectedIndex = selectedIndex + direction - 1;
                AssignMessagesDataSource();
                Session["CurrentMessage"] = row;
            }

            btnUpMessage.Enabled = selectedIndex + direction > 1;
            btnDownMessage.Enabled = selectedIndex + direction < gvMessages.Rows.Count;
        }
    }
    protected void ActiveCategory_CheckedChanged(object sender, EventArgs e)
    {
        string updateCommand = SqlDataSource1.UpdateCommand;

        try
        {
            for (int i = 0; i < gvCategories.DataKeys.Count; i++)
            {
                CheckBox checkBox = (CheckBox)gvCategories.Rows[i].FindControl("ActiveCategory");
                string active = checkBox.Checked ? "1" : "0";

                SqlDataSource1.UpdateCommand = "UPDATE Category"
                                             + " SET Active=" + active
                                             + " WHERE (Category_Name='" + gvCategories.DataKeys[i].Values["Category_Name"].ToString() + "')";
                SqlDataSource1.Update();

                if (Session["CurrentCategory"] != null)
                {
                    DataRow row = (DataRow)Session["CurrentCategory"];

                    if (row["Category_Name"].ToString() == gvCategories.DataKeys[i].Values["Category_Name"].ToString())
                        row["Active"] = checkBox.Checked ? 1 : 0;
                }
            }

            SqlDataSource1.UpdateCommand = updateCommand;
            AssignMessagesDataSource();
        }
        catch (Exception ex)
        {
            this.lblMessageError.Text = ex.Message;
        }
    }
    protected void ActiveMessage_CheckedChanged(object sender, EventArgs e)
    {
        string updateCommand = SqlDataSource2.UpdateCommand;

        try
        {
            for (int i = 0; i < gvMessages.DataKeys.Count; i++)
            {
                CheckBox checkBox = (CheckBox)gvMessages.Rows[i].FindControl("ActiveMessage");
                string active = checkBox.Checked ? "1" : "0";

                SqlDataSource2.UpdateCommand = "UPDATE Message"
                                             + " SET Active=" + active
                                             + " WHERE (Category_Name='" + gvMessages.DataKeys[i].Values["Category_Name"].ToString()
                                             + "') AND (Sequence_Number=" + gvMessages.DataKeys[i].Values["Sequence_Number"].ToString() + ")";
                SqlDataSource2.Update();

                if (Session["CurrentMessage"] != null)
                {
                    DataRow row = (DataRow)Session["CurrentMessage"];

                    if (row["Category_Name"].ToString() == gvMessages.DataKeys[i].Values["Category_Name"].ToString() &&
                        row["Sequence_Number"].ToString() == gvMessages.DataKeys[i].Values["Sequence_Number"].ToString())
                        row["Active"] = checkBox.Checked ? 1 : 0;
                }
            }

            SqlDataSource2.UpdateCommand = updateCommand;
        }
        catch (Exception ex)
        {
            this.lblMessageError.Text = ex.Message;
        }
    }
}
