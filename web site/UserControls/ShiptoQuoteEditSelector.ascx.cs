using System;
using System.Data;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI;

public partial class UserControls_ShiptoQuoteEditSelector : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate();
    public event OKClickedDelegate OnOKClicked;
    
    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    public delegate void CancelClickedDelegate();
    public event CancelClickedDelegate OnCancelClicked;

    private dsQuoteDataSet _quoteDataSet;
    public dsQuoteDataSet QuoteDataSet
    {
        get { return _quoteDataSet; }
        set { _quoteDataSet = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (_quoteDataSet != null)
        {
            if (Session["ShiptoQuoteEdit_txtCustomerName"] == null)
            {
                DataRow row = _quoteDataSet.pvttquote_header.Rows[0];

                Session["ShiptoQuoteEdit_txtCustomerName"] = row["shiptoname"];
                Session["ShiptoQuoteEdit_txtAddress1"] = row["shipToAddr1"];
                Session["ShiptoQuoteEdit_txtAddress2"] = row["shipToAddr2"];
                Session["ShiptoQuoteEdit_txtAddress3"] = row["shipToAddr3"];
                Session["ShiptoQuoteEdit_txtCity"] = row["shipToCity"];
                Session["ShiptoQuoteEdit_txtState"] = row["shipToState"];
                Session["ShiptoQuoteEdit_txtZip"] = row["shipToZip"];
                Session["ShiptoQuoteEdit_txtCountry"] = row["shipToCountry"];
                Session["ShiptoQuoteEdit_txtPhone"] = row["shipToPhone"];
            }
        }

        if (Session["ShiptoQuoteEdit_txtCustomerName"] != null)
        {
            txtCustomerName.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtCustomerName"]).Trim();
            txtAddress1.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtAddress1"]).Trim();
            txtAddress2.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtAddress2"]).Trim();
            txtAddress3.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtAddress3"]).Trim();
            txtCity.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtCity"]).Trim();
            txtState.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtState"]).Trim();
            txtZip.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtZip"]).Trim();
            txtCountry.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtCountry"]).Trim();
            txtPhone.Text = Convert.ToString(Session["ShiptoQuoteEdit_txtPhone"]).Trim();
        }
        else
        {
            txtCustomerName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
        }

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        Session["ShiptoQuoteEdit_txtCustomerName"] = txtCustomerName.Text.Trim();
        Session["ShiptoQuoteEdit_txtAddress1"] = txtAddress1.Text.Trim();
        Session["ShiptoQuoteEdit_txtAddress2"] = txtAddress2.Text.Trim();
        Session["ShiptoQuoteEdit_txtAddress3"] = txtAddress3.Text.Trim();
        Session["ShiptoQuoteEdit_txtCity"] = txtCity.Text.Trim();
        Session["ShiptoQuoteEdit_txtState"] = txtState.Text.Trim();
        Session["ShiptoQuoteEdit_txtZip"] = txtZip.Text.Trim();
        Session["ShiptoQuoteEdit_txtCountry"] = txtCountry.Text.Trim();
        Session["ShiptoQuoteEdit_txtPhone"] = txtPhone.Text.Trim();

        if (OnOKClicked != null)
            OnOKClicked(); // will be caught by the parent
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (OnCancelClicked != null)
            OnCancelClicked(); // will be caught by the parent
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtCustomerName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtZip.Text = "";
        txtCountry.Text = "";
        txtPhone.Text = "";

        Session["ShiptoQuoteEdit_txtCustomerName"] = null;
        Session["ShiptoQuoteEdit_txtAddress1"] = null;
        Session["ShiptoQuoteEdit_txtAddress2"] = null;
        Session["ShiptoQuoteEdit_txtAddress3"] = null;
        Session["ShiptoQuoteEdit_txtCity"] = null;
        Session["ShiptoQuoteEdit_txtState"] = null;
        Session["ShiptoQuoteEdit_txtZip"] = null;
        Session["ShiptoQuoteEdit_txtCountry"] = null;
        Session["ShiptoQuoteEdit_txtPhone"] = null;

        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }
}
