using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.IO;
public partial class UserControls_Quotes : System.Web.UI.UserControl
{
    string _myDataItem = ""; //MDM - This is the quote_id from a grid RowCommand click.

    public GridView GvQuoteHeaders
    {
        get { return this.gvQuoteHeaders; }
    }

    public GridView GvQuoteByItem
    {
        get { return this.gvByItem; }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormat();", true);

        if ((string)Session["ddlQuoteShowResultsBy"] == "Item")
            csCustomHeader.GridView = this.gvByItem;
        else
            csCustomHeader.GridView = this.gvQuoteHeaders;

        if (Session["QuoteHeaderHasBeenSaved"] != null && (bool)Session["QuoteHeaderHasBeenSaved"] == true)
        {
            Session["QuoteHeaderHasBeenSaved"] = null;
            btnFind_Click(null, null);
        }

        if (Session["UhOhPagingHasOccurred"] != null && (bool)Session["UhOhPagingHasOccurred"] == true)
        {
            Session["UhOhPagingHasOccurred"] = null;
            this.gvQuoteHeaders.SelectedIndex = -1;
            this.gvQuoteHeaders.EditIndex = -1;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _myDataItem = (string)Session["_myDataItem"];

        this.csCustomHeader.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomHeader_OnOKClicked);
        this.csCustomHeader.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomHeader_OnResetClicked);
        this.csCustomDetail.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(csCustomDetail_OnOKClicked);
        this.csCustomDetail.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(csCustomDetail_OnResetClicked);

        if (!this.IsPostBack)
        {
            ddlSearchStyle.SelectedIndex = 2;

            //load data fields with today and 30 days prior...
            this.txtToDate.Text = DateTime.Now.ToDMSiDateFormat();
            this.txtFromDate.Text = (DateTime.Today.AddMonths(-1)).ToDMSiDateFormat();

            this.ddlQuoteHeaderRows.SelectedIndex = Profile.ddlQuoteHeaderRows_SelectedIndex;

            int pageSize = Convert.ToInt32(ddlQuoteHeaderRows.SelectedValue);

            this.gvQuoteHeaders.PageSize = pageSize;
            this.gvByItem.PageSize = pageSize;

            InitializeColumnVisibility(gvQuoteHeaders, "gvQuoteHeaders_Columns_Visible", 3, false);
            InitializeColumnVisibility(gvByItem, "gvQuoteItems_Columns_Visible", 0, false);

            ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
            if (pc.QUddlTranID_SelectedIndex > ddlTranID.Items.Count)
                ddlTranID.SelectedIndex = 0;
            else
                ddlTranID.SelectedIndex = pc.QUddlTranID_SelectedIndex;

            SetDdlSearchTypeValues(ddlTranID.Text);

            if (pc.QUddlSearchStyle_SelectedIndex > ddlSearchStyle.Items.Count)
                ddlSearchStyle.SelectedIndex = 0;
            else
                ddlSearchStyle.SelectedIndex = pc.QUddlSearchStyle_SelectedIndex;
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // this is needed for the columns to show correctly
        if (!this.IsPostBack || dm.GetCache("NoQuotesAvailable") != null)
        {
            if (!this.IsPostBack)
            {
                dm.SetCache("gvQuoteHeadersSortColumn", "quote_id");
                dm.SetCache("gvQuoteHeadersSortDirection", " DESC");
                dm.SetCache("gvQuoteDetailSortColumn", "ITEM");
                dm.SetCache("gvQuoteDetailSortDirection", " ASC");
                dm.SetCache("gvQuoteByItemSortColumn", "ITEM");
                dm.SetCache("gvQuoteByItemSortDirection", " ASC");
            }

            if ((string)dm.GetCache("ddlQuoteShowResultsBy") == "Item")
                this.AssignHeaderDataSource(true, "pvttquote_byitem");
            else
                this.AssignHeaderDataSource(true, "pvttquote_header");
        }
        else if (dm.GetCache("DocViewerQuoteID") != null)
        {
            //for assigning data to inner grid doc viewer
            if (Session["Quotedetails_InnerDataRowid"] != null)
            {
                int irow = 0;

                if (!(GvQuoteHeaders.Rows.Count - 1 < Convert.ToInt32(Session["Quotedetails_InnerDataRowid"])))
                    irow = Convert.ToInt32(Session["Quotedetails_InnerDataRowid"]);

                GridViewRow grInner = GvQuoteHeaders.Rows[irow];
                UpdateDetailInformation(grInner);
            }
        }

        dm = null;
    }

    #region Private Methods

    /// <summary>
    /// Routine to build the string corresponding to the visible columns of the grid
    /// </summary>
    /// <param name="checkBoxList"></param>
    /// <param name="gridView"></param>
    /// <returns></returns>
    private string BuildColumnVisibilityProperty(CheckBoxList checkBoxList, GridView gridView, int offset)
    {
        string outputString = "";

        char c;

        for (int x = 0; x < checkBoxList.Items.Count; x++)
        {
            if (checkBoxList.Items[x].Selected)
            {
                c = '1';
                gridView.Columns[x + offset].Visible = true;
            }
            else
            {
                c = '0';
                gridView.Columns[x + offset].Visible = false;
            }

            outputString += c;
        }

        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Net price")
        {
            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_Quotes1_gvQuoteHeaders")
            {
                string oneString = outputString.Substring(0, 19);
                string twoString = outputString.Substring(20);

                outputString = oneString + "0" + twoString;

                gridView.Columns[22].Visible = false;
            }

            if (gridView.ID == "gvQuoteDetail")
            {
                string oneString = outputString.Substring(0, 11);

                outputString = oneString + "000";

                gridView.Columns[11].Visible = false;
                gridView.Columns[12].Visible = false;
                gridView.Columns[13].Visible = false;
            }

            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_Quotes1_gvQuoteHeaders_ctl02_gvQuoteDetail")
            {
                string oneString = outputString.Substring(0, 11);

                outputString = oneString + "000";

                gridView.Columns[11].Visible = false;
                gridView.Columns[12].Visible = false;
                gridView.Columns[13].Visible = false;
            }

            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_Quotes1_gvByItem")
            {
                string oneString = outputString.Substring(0, 11);
                string twoString = outputString.Substring(14);

                outputString = oneString + "000" + twoString;

                gridView.Columns[11].Visible = false;
                gridView.Columns[12].Visible = false;
                gridView.Columns[13].Visible = false;
            }
        }
        else
        {
            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_Quotes1_gvQuoteHeaders")
            {
                string oneString = outputString.Substring(0, 15);
                string twoString = outputString.Substring(19);

                outputString = oneString + "0000" + twoString;

                gridView.Columns[18].Visible = false;
                gridView.Columns[19].Visible = false;
                gridView.Columns[20].Visible = false;
                gridView.Columns[21].Visible = false;
            }

            if (gridView.ID == "gvQuoteDetail")
            {
                string oneString = outputString.Substring(0, 8);
                string twoString = outputString.Substring(11);

                outputString = oneString + "000" + twoString;

                gridView.Columns[8].Visible = false;
                gridView.Columns[9].Visible = false;
                gridView.Columns[10].Visible = false;
            }

            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_Quotes1_gvQuoteHeaders_ctl02_gvQuoteDetail")
            {
                string oneString = outputString.Substring(0, 8);
                string twoString = outputString.Substring(11);

                outputString = oneString + "000" + twoString;

                gridView.Columns[8].Visible = false;
                gridView.Columns[9].Visible = false;
                gridView.Columns[10].Visible = false;
            }

            if (gridView.ClientID == "ctl00_cphMaster1_MainTabControl1_Quotes1_gvByItem")
            {
                string oneString = outputString.Substring(0, 8);
                string twoString = outputString.Substring(11);

                outputString = oneString + "000" + twoString;

                gridView.Columns[8].Visible = false;
                gridView.Columns[9].Visible = false;
                gridView.Columns[10].Visible = false;
            }
        }

        return outputString;
    }

    /// <summary>
    /// Assign the column visibility based on profile settings
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="profileName"></param>
    public void InitializeColumnVisibility(GridView gridView, string profileName, int offset, bool calledFromUserPrefChange)
    {
        string quotedetail = Profile.gvQuoteDetail_Columns_Visible;
        string quotedetailsubstring = quotedetail.Substring(0, 8);

        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            Type t = gridView.Columns[x].GetType();
            if ((t.Name != "ButtonField") && (t.Name != "TemplateField"))
            {
                try
                {
                    bool isVisible = false;

                    if ((Profile.PropertyValues[profileName] == null &&
                        System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString().Substring(x - offset, 1) == "1")
                    || (Profile.PropertyValues[profileName] != null &&
                        Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(x - offset, 1) == "1"))
                    {
                        isVisible = true;
                    }

                    gridView.Columns[x].Visible = isVisible;
                }
                catch (System.ArgumentOutOfRangeException aoor)
                {
                    string message = aoor.Message;
                    Profile.PropertyValues[profileName].PropertyValue = System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString();
                }
            }
        }

        if (Session["UserPref_DisplaySetting"] != null && (string)Session["UserPref_DisplaySetting"] == "Net price")
        {
            if (profileName == "gvQuoteHeaders_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Retail Total")
                        col.Visible = false;

                    if ((col.HeaderText == "Sub Total" || col.HeaderText == "Total Taxes" || col.HeaderText == "Total Charges" || col.HeaderText == "Total") && calledFromUserPrefChange)
                    {
                        col.Visible = true;
                    }
                }
            }

            if (profileName == "gvQuoteDetail_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                        col.Visible = false;

                    if ((col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount"))
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;

                            Profile.gvQuoteDetail_Columns_Visible = quotedetailsubstring + "111000";
                        }
                        else
                        {
                            if (col.HeaderText == "Price")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(8, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Price UOM")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(9, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Extended Amount")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(10, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }
                        }
                    }
                }
            }

            if (profileName == "gvQuoteItems_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                        col.Visible = false;

                    if ((col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount"))
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;
                        }
                        else
                        {
                            if (col.HeaderText == "Price")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(11, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Price UOM")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(12, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Extended Amount")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(13, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (profileName == "gvQuoteHeaders_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Sub Total" || col.HeaderText == "Total Taxes" || col.HeaderText == "Total Charges" || col.HeaderText == "Total")
                        col.Visible = false;

                    if (col.HeaderText == "Retail Total")
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;
                        }
                        else
                        {
                            if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(19, 1) == "0")
                                col.Visible = false;
                            else
                                col.Visible = true;
                        }
                    }
                }
            }

            if (profileName == "gvQuoteDetail_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount")
                        col.Visible = false;

                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;

                            Profile.gvQuoteDetail_Columns_Visible = quotedetailsubstring + "000111";
                        }
                        else
                        {
                            if (col.HeaderText == "Retail Price")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(11, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Price UOM")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(12, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Extended Amount")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(13, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }
                        }
                    }
                }
            }

            if (profileName == "gvQuoteItems_Columns_Visible")
            {
                foreach (DataControlField col in gridView.Columns)
                {
                    if (col.HeaderText == "Price" || col.HeaderText == "Price UOM" || col.HeaderText == "Extended Amount")
                        col.Visible = false;

                    if (col.HeaderText == "Retail Price" || col.HeaderText == "Retail Price UOM" || col.HeaderText == "Retail Extended Amount")
                    {
                        if (calledFromUserPrefChange)
                        {
                            col.Visible = true;
                        }
                        else
                        {
                            if (col.HeaderText == "Retail Price")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(11, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Price UOM")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(12, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }

                            if (col.HeaderText == "Retail Extended Amount")
                            {
                                if (Profile.PropertyValues[profileName] != null && Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(13, 1) == "0")
                                    col.Visible = false;
                                else
                                    col.Visible = true;
                            }
                        }
                    }
                }
            }
        }
    }

    // This is a helper method used to determine the index of the
    // column being sorted. If no column is being sorted, -1 is returned.
    private int GetSortColumnIndex(GridView gv, string sessionSortColKey, int offSet)
    {
        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + offSet;
            }
        }

        return -1;
    }

    // This is a helper method used to add a sort direction
    // image to the header of the column being sorted.
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {
        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        Image sortImage = new Image();

        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
        ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

        // Add the image to the appropriate header cell.
        imageLink.CommandName = "Sort";
        imageLink.CommandArgument = (string)Session[sessionSortColKey];
        imageLink.Controls.Add(sortImage);
        headerRow.Cells[columnIndex].Controls.Add(imageLink);

        imageLink.Dispose();
        sortImage.Dispose();
    }

    /// <summary>
    /// Insert the sort image to the column header
    /// </summary>
    /// <param name="gv"></param>
    /// <param name="row"></param>
    /// <param name="sessionSortColKey"></param>
    /// <param name="sessionDirectionKey"></param>
    /// <param name="offSet">Refers to the number non-boundable columns before boundable columns</param>
    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey, int offSet)
    {
        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey, offSet);

        if (sortColumnIndex != -1)
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }
    }

    private void AssignHeaderDataSource(bool empty, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string sort = "";

        if (tableName == "pvttquote_byitem")
        {
            sort = (string)dm.GetCache("gvQuoteByItemSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "ITEM";
            else
                sort += dm.GetCache("gvQuoteByItemSortDirection");

            this.gvByItem.Visible = true;
            this.gvQuoteHeaders.Visible = false;
            this.pnlQuoteDetail.Visible = false; // Detail GridView container

            FetchHeaderDataSource(empty, "ITEM LIKE '%'", sort, this.gvByItem, tableName);
        }
        else
        {
            sort = (string)dm.GetCache("gvQuoteHeadersSortColumn");

            if (sort == null || sort.Length == 0)
                sort = "quote_id";
            else
                sort += dm.GetCache("gvQuoteHeadersSortDirection");

            this.gvQuoteHeaders.Visible = true;
            this.gvByItem.Visible = false;
            this.pnlQuoteDetail.Visible = true; // Detail GridView container

            this.pnlQuoteDetailCustomView.Visible = true;

            FetchHeaderDataSource(empty, "quote_id>0", sort, this.gvQuoteHeaders, tableName);
        }
    }

    private void FetchHeaderDataSource(bool empty, string filter, string sort, GridView gv, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet quoteList;

        // First column is the Select link, do this to force new arrangement of fields
        string searchCriteria = (string)dm.GetCache("QuoteSearchCriteria");
        dm.RemoveCache("pvttquote_header");

        if (!this.IsPostBack || searchCriteria == null || empty)
            quoteList = new dsQuoteDataSet();
        else
        {
            if (dm.GetCache("dsQuoteDataSet") != null)
                quoteList = (dsQuoteDataSet)dm.GetCache("dsQuoteDataSet");
            else
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

                siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

                foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
                {
                    siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
                }

                currentCust.Dispose();

                Dmsi.Agility.Data.Quotes Quotes = new Dmsi.Agility.Data.Quotes(searchCriteria, siCallDS, "dsQuoteDataSet", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                siCallDS.Dispose();
                quoteList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet)Quotes.ReferencedDataSet;
                dm.SetCache("dsQuoteDataSet", quoteList);
            }
        }

        // there is no dataset available, just initialize its schema
        if (quoteList == null)
            quoteList = new dsQuoteDataSet();

        DataView dv = null;
        if (tableName == "pvttquote_byitem")
            dv = new DataView(quoteList.pvttquote_byitem, filter, sort, DataViewRowState.CurrentRows);
        else
            dv = new DataView(quoteList.pvttquote_header, filter, sort, DataViewRowState.CurrentRows);

        DataTable dt = dv.ToTable(tableName + "Copy"); // this will give us the correct column arrangement

        if (dt.Rows.Count < 100)
            lblQuoteHeaderError.Visible = false;
        else
        {
            lblQuoteHeaderError.Visible = false;
        }

        if (dt.Rows.Count > 0)
        {
            dm.RemoveCache("NoQuotesAvailable"); // this way the postback knows the grid is not empty
            gv.DataSource = dt;
            gv.DataBind();
            int ipageindex = gv.PageIndex;
            int igridcount = gv.Rows.Count;
            int itotalrecords = dt.Rows.Count;
            int iNoofrows = Convert.ToInt32(ddlQuoteHeaderRows.SelectedValue);
            int iFirstrecordno = ipageindex * (iNoofrows);
            iFirstrecordno = iFirstrecordno + 1;
            int iLastRecord = (iFirstrecordno - 1) + igridcount;

            if (itotalrecords > 0)
            {
                lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
            }

            if (dt.Rows.Count == 1 && tableName == "pvttquote_header")
            {
                dm.SetCache("pvttquote_header", dt.Rows[0]);
                this.gvQuoteHeaders.SelectedIndex = 0;

                GridViewRow row = gvQuoteHeaders.Rows[0];
                row.FindControl("innergrids").Visible = true;

                string QuoteId = gvQuoteHeaders.DataKeys[row.RowIndex].Value.ToString();
                Label lblQuoteInnerId = (Label)row.FindControl("lblQuoteInnerId");
                lblQuoteInnerId.Text = "Quote Details for ID " + QuoteId;
                ImageButton imgShowHide = (ImageButton)row.FindControl("imgbtnplusminus");
                imgShowHide.CommandName = "Hide";
                imgShowHide.ImageUrl = "~/images/collapse-icon.png";
                GridView gvQuoteDetail = (GridView)row.FindControl("gvQuoteDetail");
                InitializeColumnVisibility(gvQuoteDetail, "gvQuoteDetail_Columns_Visible", 0, false);
                csCustomDetail.GridView = gvQuoteDetail;
                BindInnerGridDetails(row.RowIndex);
                row.CssClass = "active-row-tr";
            }
        }
        else
        {
            lblpageno.Text = "";
            // this is a work around so the grid will show when the datasource is empty
            dm.SetCache("NoQuotesAvailable", true);

            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);
        }
    }

    private void FetchHeaderDataSource1(bool empty, string filter, string sort, GridView gv, string tableName)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet quoteList;

        // First column is the Select link, do this to force new arrangement of fields
        string searchCriteria = (string)dm.GetCache("QuoteSearchCriteria");
        dm.RemoveCache("pvttquote_header");

        if (!this.IsPostBack || searchCriteria == null || empty)
            quoteList = new dsQuoteDataSet();
        else
        {
            if (dm.GetCache("dsQuoteDataSet") != null)
                quoteList = (dsQuoteDataSet)dm.GetCache("dsQuoteDataSet");
            else
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.AgilityCustomer currentCust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet siCallDS = new dsCustomerDataSet();

                siCallDS.ttCustomer.ImportRow(currentCust.ReferencedRow);

                foreach (DataRow row in ((DataTable)currentCust.BranchShipTos).Rows)
                {
                    siCallDS.ttShipto.LoadDataRow(row.ItemArray, true);
                }

                currentCust.Dispose();

                Dmsi.Agility.Data.Quotes Quotes = new Dmsi.Agility.Data.Quotes(searchCriteria, siCallDS, "dsQuoteDataSet", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                siCallDS.Dispose();
                quoteList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet)Quotes.ReferencedDataSet;
                dm.SetCache("dsQuoteDataSet", quoteList);
            }
        }

        // there is no dataset available, just initialize its schema
        if (quoteList == null)
            quoteList = new dsQuoteDataSet();

        DataView dv = null;
        if (tableName == "pvttquote_byitem")
            dv = new DataView(quoteList.pvttquote_byitem, filter, sort, DataViewRowState.CurrentRows);
        else
            dv = new DataView(quoteList.pvttquote_header, filter, sort, DataViewRowState.CurrentRows);

        DataTable dt = dv.ToTable(tableName + "Copy"); // this will give us the correct column arrangement

        if (dt.Rows.Count < 100)
            lblQuoteHeaderError.Visible = false;
        else
        {
            lblQuoteHeaderError.Visible = false;
        }

        if (dt.Rows.Count > 0)
        {
            if (dt.Rows.Count == 1 && tableName == "pvttquote_header")
            {
                dm.SetCache("pvttquote_header", dt.Rows[0]);
                this.gvQuoteHeaders.SelectedIndex = 0;
            }

            dm.RemoveCache("NoQuotesAvailable"); // this way the postback knows the grid is not empty
            gv.DataSource = dt;
            gv.DataBind();
            int ipageindex = gv.PageIndex;
            int igridcount = gv.Rows.Count;
            int itotalrecords = dt.Rows.Count;
            int iNoofrows = Convert.ToInt32(ddlQuoteHeaderRows.SelectedValue);
            int iFirstrecordno = ipageindex * (iNoofrows);
            iFirstrecordno = iFirstrecordno + 1;
            int iLastRecord = (iFirstrecordno - 1) + igridcount;

            if (itotalrecords > 0)
            {
                lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
            }
            else
            {
                lblpageno.Text = "";
            }
        }
        else
        {
            lblpageno.Text = "";
            // this is a work around so the grid will show when the datasource is empty
            dm.SetCache("NoQuotesAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gv);
        }
    }

    private DataTable GetDetailSortDetails(string quoteid, bool empty)
    {
        string filter = "quote_id=" + quoteid;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet ds;

        string sort = (string)dm.GetCache("gvQuoteDetailSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "ITEM";
        else
            sort += dm.GetCache("gvQuoteDetailSortDirection");

        if (!this.IsPostBack || empty)
            ds = new dsQuoteDataSet();
        else
            ds = (dsQuoteDataSet)dm.GetCache("dsQuoteDataSet");

        // no fetch yet
        if (ds == null)
            ds = new dsQuoteDataSet();

        DataView dv = new DataView(ds.pvttquote_detail, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttquote_detailCopy"); // this will give us the correct column arrangement
        Session["dt_ttquote_detailCopy"] = dt;
        return dt;
    }

    private void AssignDetailDataSource(bool empty, GridViewRow gvr, string quoteid)
    {
        string myDataItems = "";
        myDataItems = quoteid;

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Quotes QuoteLists = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.DataManager myDataManagers = new DataManager();
        DataRow[] QuoteRows = QuoteLists.ReferenceHeaderTable.Select("quote_id='" + myDataItems + "'");
        myDataManagers.SetCache("pvttquote_header", QuoteRows[0]);

        string filter = this.DetailBuildFilter();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsQuoteDataSet ds;

        UpdateDetailInformation(gvr); // if there is a transaction selected, refresh the labels and misc information

        string sort = (string)dm.GetCache("gvQuoteDetailSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "ITEM";
        else
            sort += dm.GetCache("gvQuoteDetailSortDirection");

        if (!this.IsPostBack || empty)
            ds = new dsQuoteDataSet();
        else
            ds = (dsQuoteDataSet)dm.GetCache("dsQuoteDataSet");

        // no fetch yet
        if (ds == null)
            ds = new dsQuoteDataSet();

        DataView dv = new DataView(ds.pvttquote_detail, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttquote_detailCopy"); // this will give us the correct column arrangement
        Session["dt_ttquote_detailCopy"] = dt;

        GridView gvQuoteDetail = (GridView)gvr.FindControl("gvQuoteDetail");

        if (dt.Rows.Count > 0)
        {
            gvQuoteDetail.DataSource = dt;
            gvQuoteDetail.DataBind();
        }
        else
        {
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dt, gvQuoteDetail);
        }
    }

    private string DetailBuildFilter()
    {
        string filter = "";
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("pvttquote_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("pvttquote_header");
            filter = "quote_id=" + row["quote_id"].ToString();
        }
        else
        {
            filter = "quote_id=1234567890";
        }

        return filter;
    }

    private void UpdateDetailInformation()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        UserControls_DocViewer docViewer = (UserControls_DocViewer)this.FindControl("gvQuoteHeaders").FindControl("docViewer");

        if (dm.GetCache("pvttquote_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("pvttquote_header");

            docViewer.DocumentTitle = "Documents for ID " + row["quote_id"].ToString();
            docViewer.Type = "Quote";
            docViewer.TransactionID = (int)row["quote_id"];
            docViewer.BranchID = (string)row["branch_id"];

            dm.SetCache("DocViewerQuoteID", (int)row["quote_id"]);
        }
        else
        {
            docViewer.DocumentTitle = "Documents";
            docViewer.Type = "";
            docViewer.TransactionID = 0;
            dm.RemoveCache("DocViewerQuoteID");
        }

        docViewer.Refresh();
    }

    private void UpdateDetailInformation(GridViewRow gr)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        UserControls_DocViewer docViewer = (UserControls_DocViewer)gr.FindControl("docViewer");

        PriceModeHelper pmh = new PriceModeHelper(dm);

        docViewer.ShowZeroPriceWarning = false;

        if (dm.GetCache("pvttquote_header") != null)
        {
            DataRow row = (DataRow)dm.GetCache("pvttquote_header");

            docViewer.DocumentTitle = "Documents for ID " + row["quote_id"].ToString();
            docViewer.Type = "Quote";
            docViewer.TransactionID = (int)row["quote_id"];
            docViewer.BranchID = (string)row["branch_id"];

            dm.SetCache("DocViewerQuoteID", (int)row["quote_id"]);

            if (dm.PVParameter("allow_retail_pricing", "yes") &&  dm.ActionAllocationExists("enable_retail_price_display") && !pmh.InNetMode())
            {
                docViewer.ShowZeroPriceWarning = ShouldShowZeroPriceWarning((int)row["quote_id"]);
            }
        }
        else
        {
            docViewer.DocumentTitle = "Documents";
            docViewer.Type = "";
            docViewer.TransactionID = 0;
            dm.RemoveCache("DocViewerQuoteID");
        }

        docViewer.Refresh();
        GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

        if (gvdoc.Rows.Count <= 0)
        {
            docViewer.Visible = false;
        }
    }

    private bool ShouldShowZeroPriceWarning(int piQuoteID)
    {
        int ReturnCode = 0;
        string MessageText = "";
        bool hasNonZeroRetailPriceOnALine = false;
        bool nonSundryItemWithZeroRetailPrice = false;

        Dmsi.Agility.Data.Quotes QuoteLists = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

        DataRow[] QuoteDetailRows = QuoteLists.ReferencedDetailTable.Select("quote_id='" + piQuoteID + "'");

        foreach (DataRow row in QuoteDetailRows)
        {
            if (!hasNonZeroRetailPriceOnALine && 
                Convert.ToInt32(row["retail_extended_price"]) != 0)
            {
                hasNonZeroRetailPriceOnALine = true;
            }

            if (!nonSundryItemWithZeroRetailPrice && 
                Convert.ToInt32(row["retail_extended_price"]) == 0 && 
                !Convert.ToBoolean(row["sundry_item_type"]))
            {
                nonSundryItemWithZeroRetailPrice = true;
            }

            if (hasNonZeroRetailPriceOnALine && nonSundryItemWithZeroRetailPrice) { return true; }
        }

        return false;
    }
    private string BuildDocumentSearchCriteria(string QuoteID)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string branch = "";
        string quoteID = "";
        string sessionID = "";

        branch = "Branch=" + (string)dm.GetCache("currentBranchID");
        quoteID = "TranID=" + QuoteID;
        sessionID = "SessionID=" + Session.SessionID.ToString().Trim();

        searchCriteria = "DocType=Quote";
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + quoteID;
        searchCriteria += "\x0003" + sessionID;

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        searchCriteria += "\x0003UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003Type=Quotation";

        return searchCriteria;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Call this to clear bound data
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    /// <param name="customer">Current Customer Object</param>
    public void Reset()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("dsQuoteDataSet");
        dm.RemoveCache("pvttquote_header");
        dm.RemoveCache("QuoteSearchCriteria");
        this.AssignHeaderDataSource(true, "pvttquote_header");

        dm = null;
    }

    /// <summary>
    /// Call this to clear bound data when the branch or customer has changed
    /// </summary>
    /// <param name="branchID">Current Branch ID</param>
    /// <param name="shiptoObj">Current Shipto Object</param>
    public void Reset(string branchID, string shiptoObj)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if ((dm.GetCache("currentBranchID") == null ||
            (dm.GetCache("currentBranchID") != null && (string)dm.GetCache("currentBranchID") != branchID)) ||
            (dm.GetCache("ShiptoRowForInformationPanel") == null ||
            (dm.GetCache("ShiptoRowForInformationPanel") != null && ((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["cust_shipto_obj"].ToString() != shiptoObj)))
        {
            this.Reset();
        }

        dm = null;
    }

    public void btnFind_Click(object sender, EventArgs e)
    {

        ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
        pc.QUddlTranID_SelectedIndex = ddlTranID.SelectedIndex;
        pc.QUddlSearchStyle_SelectedIndex = ddlSearchStyle.SelectedIndex;
        pc.Save();

        ExecuteSearch();

        if (sender != null && e != null)
        {
            string mode = "Net";

            if (Session["UserPref_DisplaySetting"] != null &&
                 ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                  (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                  (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
               )
            { mode = "Retail"; }

            string analyticsLabel =
                                    "Mode = " + mode + ", " +
                                    "Search by = " + ddlTranID.SelectedItem + ", " +
                                    "Search Type = " + ddlSearchStyle.Text + ", " +
                                    "Status = " + ddlStatus.SelectedItem + ", " +
                                    "Show Results = " + this.ddlQuoteShowResultsBy.SelectedItem.Text + ", " +
                                    "Include all Ship-tos = " + this.cbShipTo.Checked.ToString() + ", " +
                                    "My quotes only = " + this.cbMyQuotes.Checked.ToString();

            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Quotes - Search Button";
            aInput.Label = analyticsLabel;
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }
    }

    #endregion

    protected void gvQuoteHeader_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView gvGetid = (sender as GridView);

            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }
            }

            e.Row.Cells[0].ToolTip = "View";

            if (gvGetid.ID == "gvQuoteHeaders")
            {
                ImageButton imgedit = (ImageButton)e.Row.FindControl("imgEdit");
                ImageButton imgeditretailinfo = (ImageButton)e.Row.FindControl("imgEditRetailInfo");
                ImageButton imgDelete = (ImageButton)e.Row.FindControl("imgRelease");
                ImageButton imgCopyQuote = (ImageButton)e.Row.FindControl("imgCopyQuote");
                imgedit.ToolTip = "Edit";
                imgeditretailinfo.ToolTip = "Edit Retail Information";
                imgDelete.ToolTip = "Release";
                imgCopyQuote.ToolTip = "Copy";
            }

            DataRowView drv = e.Row.DataItem as DataRowView;

            if (drv != null)
            {
                bool copyQuoteParam = false;
                bool copyQuoteAction = false;

                Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
                dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

                dsPVParamDataSet dsPVParam = (dsPVParamDataSet)dm.GetCache("dsPVParam");
                DataRow[] pvparamrows = dsPVParam.ttparam_pv.Select("property_name='allow_copy_quote'");

                dsPartnerVuDataSet odsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");
                DataRow[] odsPVrows = odsPV.bPV_Action.Select("token_code='copy_quotes'");

                if (odsPVrows.Length == 1)
                    copyQuoteAction = true;

                if (pvparamrows.Length == 1)
                {
                    if ((e.Row.Cells[1].HasControls()) && ((string)(pvparamrows[0]["property_value"])).ToLower() != "yes")
                        copyQuoteParam = false;
                    else
                        copyQuoteParam = true;
                }

                if ((!copyQuoteParam || !copyQuoteAction) || (copyQuoteParam && !copyQuoteAction))
                {
                    System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;
                    ImageButton btn = (ImageButton)control.FindControl("imgCopyQuote");
                    btn.Visible = false;
                }

                //Handle Edit allocation first...
                bool editAllocation = true;

                if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
                {
                    DataRow[] rows = dsPV.bPV_Action.Select("token_code='edit_quotes'");
                    if (rows.Length == 0) //no allocation
                    {
                        editAllocation = false;
                    }
                }

                // Disable the Edit Retail Info butto if net mode 
                if ((e.Row.Cells[1].HasControls()) && editAllocation == false)
                {
                    System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;
                    ImageButton btn = (ImageButton)control.FindControl("imgEdit");
                    btn.Visible = false;
                }

                //edit Retail Information

                bool editRetailInfo = true;

                if (Session["UserPref_DisplaySetting"] != null &&
                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                {
                    editRetailInfo = true;
                }
                else
                {
                    editRetailInfo = false;
                }

                if ((dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0) && editRetailInfo)
                {
                    DataRow[] rows = dsPV.bPV_Action.Select("token_code='enable_retail_price_display'");
                    if (rows.Length == 0) //no allocation
                    {
                        editRetailInfo = false;
                    }
                }

                if (editAllocation == false &&
                    (Session["UserPref_DisplaySetting"] != null &&
                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")))
                {
                    editRetailInfo = true;
                }

                // Disable the Edit Retail Info button 
                if ((e.Row.Cells[1].HasControls()) && editRetailInfo == false)
                {
                    System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;
                    ImageButton btn = (ImageButton)control.FindControl("imgEditRetailInfo");
                    btn.Visible = false;
                }

                bool IsPV = false;
                string quoteId = "";
                string status = "";

                if (editAllocation || editRetailInfo)
                {
                    for (int x = 0; x < drv.Row.ItemArray.Length; x++)
                    {
                        if (drv.Row.Table.Columns[x].ColumnName == "statusdescr")
                        {
                            if (drv.Row.ItemArray[x].ToString() == "Closed")
                            {
                                status = "Closed";

                                if (e.Row.Cells[1].HasControls())
                                {
                                    System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;

                                    GridView gv = sender as GridView;

                                    if (editAllocation)
                                    {
                                        ImageButton btn = (ImageButton)control.FindControl("imgEdit");
                                        btn.Visible = false;
                                    }

                                    if (editRetailInfo)
                                    {
                                        ImageButton btn = (ImageButton)control.FindControl("imgEditRetailInfo");
                                        btn.Visible = false;
                                    }
                                }
                            }
                        }

                        if (drv.Row.Table.TableName != "ttquote_detailCopy" && drv.Row.Table.Columns[x].ColumnName == "orig_source")
                        {
                            if (drv.Row.ItemArray[x].ToString().ToLower() != "pv")
                            {
                                System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;

                                GridView gv = sender as GridView;

                                ImageButton btn = (ImageButton)control.FindControl("imgEdit");
                                btn.Visible = false;
                            }
                            else
                            {
                                IsPV = true;
                                System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;
                                ImageButton btn = (ImageButton)control.FindControl("imgEditRetailInfo");

                                if (editAllocation == false &&
                                    (Session["UserPref_DisplaySetting"] != null &&
                                    ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")))
                                {
                                    btn.Visible = true;
                                }
                                else
                                    btn.Visible = false;

                                if (status == "Closed")
                                    btn.Visible = false;
                            }
                        }

                        if (drv.Row.Table.Columns[x].ColumnName == "quote_id")
                        {
                            quoteId = drv.Row.ItemArray[x].ToString();
                        }
                    }

                    if (e.Row.Cells[1].Controls.Count > 0)
                    {
                        System.Web.UI.Control localcontrol = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;
                        ImageButton btnEditRetail = (ImageButton)localcontrol.FindControl("imgEditRetailInfo");

                        if (status == "Closed")
                            btnEditRetail.Visible = false;
                    }
                }
                else
                {
                    if (e.Row.Cells[1].HasControls())
                    {
                        System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;

                        GridView gv = sender as GridView;

                        ImageButton btn = (ImageButton)control.FindControl("imgEdit");
                        btn.Visible = false;

                        btn = (ImageButton)control.FindControl("imgEditRetailInfo");
                        btn.Visible = false;
                    }
                }

                if (IsPV)
                {
                    bool foundAgilityItem = false;
                    dsQuoteDataSet dsQuotes = (dsQuoteDataSet)dm.GetCache("dsQuoteDataSet");
                    DataRow[] rows = dsQuotes.pvttquote_detail.Select("quote_id='" + quoteId + "'");

                    foreach (DataRow row in rows)
                    {
                        if ((string)row["retail_price_uom_code"] == "")
                        {
                            foundAgilityItem = true;
                            break;
                        }
                    }

                    if (foundAgilityItem)
                    {
                        if (e.Row.Cells[1].HasControls())
                        {
                            System.Web.UI.Control control = e.Row.Cells[1].Controls[0] as System.Web.UI.Control;
                            GridView gv = sender as GridView;
                            if (((ButtonField)gv.Columns[1]).ButtonType == ButtonType.Link)
                            {
                                LinkButton btn = control as LinkButton;
                                btn.Text = "";
                            }
                        }
                    }
                }//end of edit_quotes allocation...

                bool releaseAllocation = true;

                if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
                {
                    DataRow[] rows = dsPV.bPV_Action.Select("token_code='release_quotes'");
                    if (rows.Length == 0) //no allocation
                    {
                        releaseAllocation = false;
                    }
                }

                if (releaseAllocation)
                {
                    for (int x = 0; x < drv.Row.ItemArray.Length; x++)
                    {
                        if (drv.Row.Table.Columns[x].ColumnName == "statusdescr")
                        {
                            if (drv.Row.ItemArray[x].ToString() == "Closed")
                            {
                                //if (e.Row.Cells[2].HasControls())
                                if (e.Row.Cells[1].HasControls())
                                {
                                    System.Web.UI.Control releasecontrol = e.Row.Cells[1].Controls[1] as System.Web.UI.Control;
                                    GridView gv = sender as GridView;

                                    ImageButton btn = (ImageButton)releasecontrol.FindControl("imgRelease");
                                    btn.Visible = false;
                                }
                            }
                            else if (drv.Row.ItemArray[x].ToString() == "Closed")
                            {
                                if (IsPV == false)
                                {
                                    System.Web.UI.Control releasecontrol = e.Row.Cells[1].Controls[1] as System.Web.UI.Control;

                                    ImageButton btn = (ImageButton)releasecontrol.FindControl("imgRelease");
                                    btn.Attributes.Add("Style", "Padding-left:20px");
                                }
                            }
                        }
                    }

                    //Add code for new allow_release column flag here...
                    for (int x = 0; x < drv.Row.ItemArray.Length; x++)
                    {
                        if (drv.Row.Table.Columns[x].ColumnName == "allow_release")
                        {
                            if (!(drv.Row.ItemArray[x].ToString().ToLower() == "f" || drv.Row.ItemArray[x].ToString().ToLower() == "p"))
                            {
                                if (e.Row.Cells[2].HasControls())
                                {
                                    System.Web.UI.Control releasecontrol = e.Row.Cells[1].Controls[1] as System.Web.UI.Control;
                                    GridView gv = sender as GridView;

                                    ImageButton btn = (ImageButton)releasecontrol.FindControl("imgRelease");
                                    btn.Visible = false;

                                }
                            }
                        }
                    }

                    if (Session["QuoteFullyReleasedId" + quoteId] != null && Session["QuoteFullyReleasedId" + quoteId].ToString() == quoteId)
                    {
                        if (e.Row.Cells[1].HasControls())
                        {
                            System.Web.UI.Control releasecontrol = e.Row.Cells[1].Controls[2] as System.Web.UI.Control;
                            ImageButton btn = (ImageButton)releasecontrol.FindControl("imgRelease");
                            btn.Visible = false;

                            System.Web.UI.Control imagecontrol = e.Row.Cells[1].Controls[1] as System.Web.UI.Control;
                            btn = (ImageButton)imagecontrol.FindControl("imgEditRetailInfo");
                            btn.Visible = false;

                            btn = (ImageButton)imagecontrol.FindControl("imgEdit");
                            btn.Visible = false;
                        }
                    }
                }
                else
                {
                    if (e.Row.Cells[1].HasControls())
                    {
                        System.Web.UI.Control releasecontrol = e.Row.Cells[1].Controls[1] as System.Web.UI.Control;
                        GridView gv = sender as GridView;

                        ImageButton btn = (ImageButton)releasecontrol.FindControl("imgRelease");
                        btn.Visible = false;
                    }
                }
            }
        }
    }

    protected void gvQuoteHeader_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvQuoteHeaders.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "pvttquote_header");

        Session["Quotedetails_InnerDataRowid"] = null;

        this.gvQuoteHeaders.SelectedIndex = -1;
        this.gvQuoteHeaders.EditIndex = -1;

        Session["UhOhPagingHasOccurred"] = true;
    }

    protected void gvByItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gvByItem.PageIndex = e.NewPageIndex;
        this.AssignHeaderDataSource(false, "pvttquote_byitem");
    }

    protected void ddlQuoteHeaderRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlQuoteHeaderRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        this.gvQuoteHeaders.PageSize = pageSize;
        this.gvByItem.PageSize = pageSize;

        if ((string)Session["ddlQuoteShowResultsBy"] == "Item")
        {
            this.AssignHeaderDataSource(false, "pvttquote_byitem");
        }
        else
        {
            this.AssignHeaderDataSource(false, "pvttquote_header");
        }

        Session["Quotedetails_InnerDataRowid"] = null;
    }

    protected void ddlQuoteDetailRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlQuoteDetailRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize;
        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        }

        DropDownList ddlsender = ((DropDownList)sender);

        LinkButton lnkQuoteDetailCustomView = (LinkButton)ddlsender.Parent.FindControl("lnkQuoteDetailCustomView");

        GridViewRow row = (lnkQuoteDetailCustomView.NamingContainer as GridViewRow);
        row.FindControl("innergrids").Visible = true;
        DropDownList ddlQuoteDetailRows = (DropDownList)row.FindControl("ddlQuoteDetailRows");
        string QuoteId = gvQuoteHeaders.DataKeys[row.RowIndex].Value.ToString();
        Label lblQuoteInnerId = (Label)row.FindControl("lblQuoteInnerId");
        lblQuoteInnerId.Text = "Quote details for Id  " + QuoteId;

        GridViewRow gr = gvQuoteHeaders.Rows[row.RowIndex];
        GridView gvQuoteDetail = (GridView)row.FindControl("gvQuoteDetail");

        ddlQuoteDetailRows.SelectedIndex = Profile.ddlQuoteDetailRows_SelectedIndex;

        if (ddlQuoteDetailRows.SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(ddlQuoteDetailRows.SelectedValue);
        }

        gvQuoteDetail.PageSize = pageSize;
        this.AssignDetailDataSource(false, gr, QuoteId);

        SizeHiddenRow(gr, ddlQuoteDetailRows, gvQuoteDetail);
    }

    private void csCustomHeader_OnOKClicked(CheckBoxList checkBoxList)
    {
        string resultsView = (string)Session["ddlQuoteShowResultsBy"] == null ? "Quote" : (string)Session["ddlQuoteShowResultsBy"];

        if (resultsView == "Quote")
        {
            Profile.gvQuoteHeaders_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvQuoteHeaders, 3);
            this.AssignHeaderDataSource(false, "pvttquote_header");
        }
        else
        {
            Profile.gvQuoteItems_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvByItem, 0);
            this.AssignHeaderDataSource(false, "pvttquote_byitem");
        }
    }

    private void csCustomDetail_OnOKClicked(CheckBoxList checkBoxList)
    {
        if (Session["Quotedetails_InnerDataRowid"] != null)
        {
            if (Session["Quotedetails_InnerQuoteId"] != null)
            {
                string QuoteId = (string)Session["Quotedetails_InnerQuoteId"];
                int irow = Convert.ToInt32(Session["Quotedetails_InnerDataRowid"]);
                GridViewRow gvr = gvQuoteHeaders.Rows[irow];
                GridView gvQuoteDetail = (GridView)gvr.FindControl("gvQuoteDetail");
                Profile.gvQuoteDetail_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvQuoteDetail, 0);
                csCustomDetail.GridView = gvQuoteDetail;
                AssignDetailDataSource(false, gvr, QuoteId);
                Session["Quotedetails_InnerGrid"] = null;
                Session["Quotedetails_InnerQuoteId"] = null;
            }
        }
    }

    private void csCustomDetail_OnResetClicked()
    {
        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteDetail_Columns_Visible"].DefaultValue.ToString();
        Profile.gvQuoteDetail_Columns_Visible = defaultValue;

        if (Session["Quotedetails_InnerDataRowid"] != null)
        {
            if (Session["Quotedetails_InnerQuoteId"] != null)
            {
                string QuoteId = (string)Session["Quotedetails_InnerQuoteId"];
                int irow = Convert.ToInt32(Session["Quotedetails_InnerDataRowid"]);
                GridViewRow gvr = gvQuoteHeaders.Rows[irow];
                GridView gvQuoteDetail = (GridView)gvr.FindControl("gvQuoteDetail");
                InitializeColumnVisibility(gvQuoteDetail, "gvQuoteDetail_Columns_Visible", 0, false);
                csCustomDetail.GridView = gvQuoteDetail;
                AssignDetailDataSource(false, gvr, QuoteId);

                Session["Quotedetails_InnerGrid"] = null;
                Session["Quotedetails_InnerQuoteId"] = null;
            }
        }
    }

    private void csCustomHeader_OnResetClicked()
    {
        string defaultValue;
        string resultsView = (string)Session["ddlQuoteShowResultsBy"] == null ? "Quote" : (string)Session["ddlQuoteShowResultsBy"];

        switch (resultsView)
        {
            case "Item":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteItems_Columns_Visible"].DefaultValue.ToString();
                    Profile.gvQuoteItems_Columns_Visible = defaultValue;
                    InitializeColumnVisibility(gvByItem, "gvQuoteItems_Columns_Visible", 0, false);
                    this.AssignHeaderDataSource(false, "pvttQuote_byitem");

                    break;
                }

            case "Quote":
                {
                    defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteHeaders_Columns_Visible"].DefaultValue.ToString();
                    Profile.gvQuoteHeaders_Columns_Visible = defaultValue;
                    InitializeColumnVisibility(gvQuoteHeaders, "gvQuoteHeaders_Columns_Visible", 3, false);
                    this.AssignHeaderDataSource(false, "pvttQuote_header");
                    break;
                }
        }

        btnFind_Click(null, null);
    }

    protected void ExecuteSearch()
    {
        // save the current search criteria until a new find is initiated
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // This is needed when building the header caption inside HeaderBuildSearchCriteria
        if (this.ddlQuoteShowResultsBy.SelectedValue == "Item")
            dm.SetCache("ddlQuoteShowResultsBy", "Item");
        else
            dm.SetCache("ddlQuoteShowResultsBy", "Quote");

        string searchCriteria = this.HeaderBuildSearchCriteria();

        if (searchCriteria == "Invalid input date.")
        {
            lblQuoteHeaderError.Visible = true;
            lblQuoteHeaderError.Text = searchCriteria;
            lblQuoteHeader.Text = "Quotes";
            return;
        }

        if (searchCriteria == "Invalid date range.")
        {
            lblQuoteHeaderError.Visible = true;
            lblQuoteHeaderError.Text = searchCriteria;
            lblQuoteHeader.Text = "Quotes";
            return;
        }

        if (searchCriteria == "")
        {
            lblQuoteHeaderError.Visible = true;
            lblQuoteHeaderError.Text = "Limit date range to 2 years or less.";
            lblQuoteHeader.Text = "Quotes";
            return;
        }

        dm.SetCache("QuoteSearchCriteria", searchCriteria);
        dm.RemoveCache("dsQuoteDataSet");
        dm.RemoveCache("DocViewerQuoteID");

        if (this.ddlQuoteShowResultsBy.SelectedValue == "Item")
            this.AssignHeaderDataSource(false, "pvttquote_byitem");
        else
            this.AssignHeaderDataSource(false, "pvttquote_header");
    }

    private string HeaderBuildSearchCriteria()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";
        string shipto = "";
        string myquotes = "";
        string shipToSequence = "";
        string searchType = "";
        string searchStyle = "";
        string searchTran = "";
        string customer = "";
        string branch = "";
        string status = "";
        string showResults = "";
        string startDateStr = "";
        string endDateStr = "";
        string gridTitle = "";

        DataRow shipToRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
        shipToSequence = shipToRow["seq_num"].ToString() + "";
        branch = "Branch=" + (string)dm.GetCache("currentBranchID");

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer currentCustomer = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        customer = "Customer=" + currentCustomer.CustomerKey;
        currentCustomer.Dispose();

        searchType = "SearchType=" + ddlTranID.SelectedValue;

        if (searchType.Contains("PO ID"))
            searchType = "SearchType=Customer PO #";

        searchStyle = "SearchStyle=" + ddlSearchStyle.Text;

        if (txtTranID.Text.Trim().Length == 0)
            searchTran = "SearchTran=<ALL>";
        else
            searchTran = "SearchTran=" + txtTranID.Text.Trim();

        if (ddlStatus.SelectedValue == "All")
            status = "Status=<ALL>";
        else
        {
            status = "Status=" + ddlStatus.SelectedValue;
            gridTitle += status + " ";
        }

        if (this.cbShipTo.Checked)
            shipto = "Ship To=<ALL>";
        else
        {
            shipto = "Ship To=" + shipToSequence;
            gridTitle += shipto + " ";
        }

        if (this.cbMyQuotes.Checked)
            myquotes = "MyQuotesOnly=YES";
        else
        {
            myquotes = "MyQuotesOnly=NO";
            gridTitle += myquotes + " ";
        }

        showResults = "Show Results=" + this.ddlQuoteShowResultsBy.SelectedValue;

        bool hasStartDate = false, hasEndDate = false;
        DateTime startDate = DateTime.Today.AddDays(-30), endDate = DateTime.Today;
        DateTimeValidator startDateValidator = new DateTimeValidator(this.txtFromDate.Text);
        DateTimeValidator endDateValidator = new DateTimeValidator(this.txtToDate.Text);

        hasStartDate = startDateValidator.InfoEntered();
        hasEndDate = endDateValidator.InfoEntered();

        if (hasStartDate && startDateValidator.ValidDate())
        {
            startDate = startDateValidator.ParsedDateTime();

            if (this.txtFromDate.Text != startDateValidator.FormattedDateTime())
            {
                this.txtFromDate.Text = startDateValidator.FormattedDateTime();
            }
        }
        else if (hasStartDate)
        {
            return startDateValidator.ValidationMessage();
        }

        if (hasEndDate && endDateValidator.ValidDate())
        {
            endDate = endDateValidator.ParsedDateTime();

            if (this.txtToDate.Text != endDateValidator.FormattedDateTime())
            {
                this.txtToDate.Text = endDateValidator.FormattedDateTime();
            }
        }
        else if (hasEndDate)
        {
            return endDateValidator.ValidationMessage();
        }

        startDateStr = "Start Date=" + (hasStartDate ? startDate.ToShortDateString() : "<ALL>");
        endDateStr = "End Date=" + (hasEndDate ? endDate.ToShortDateString() : "<ALL>");

        if (hasStartDate && hasEndDate)
        {
            if (startDate.AddYears(2) < endDate)
            {
                return "";
            }
            else if (startDate > endDate)
            {
                return "Invalid date range.";
            }
            else
                gridTitle = "Quotes from " + startDate.ToDMSiDateFormat() + " to " + endDate.ToDMSiDateFormat();
        }
        else if (hasStartDate)
        {
            if (startDate.AddYears(2) < DateTime.Now)
            {
                return "";
            }
            else
                gridTitle = "Quotes from " + startDate.ToDMSiDateFormat();
        }
        else if (hasEndDate)
        {
            gridTitle = "Quotes up to " + endDate.ToDMSiDateFormat();
            return "";
        }
        else
        {
            gridTitle = "Quotes";
            return "";
        }

        lblQuoteHeader.Text = gridTitle;
        lblQuoteHeader.Text.Trim();

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        searchCriteria = "UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + customer;

        searchCriteria += "\x0003" + shipto;
        searchCriteria += "\x0003" + myquotes;
        searchCriteria += "\x0003" + status;
        searchCriteria += "\x0003" + searchType;
        searchCriteria += "\x0003" + searchStyle;
        searchCriteria += "\x0003" + searchTran;
        searchCriteria += "\x0003" + startDateStr;
        searchCriteria += "\x0003" + endDateStr;
        searchCriteria += "\x0003" + showResults;
        searchCriteria += "\x0003Type=Quotes";
        searchCriteria += "\x0003ExtendPrice=YES";

        if ((string)Session["ddlQuoteShowResultsBy"] == "Quote")
            searchCriteria += "\x0003OrderTotals=YES";

        return searchCriteria;
    }

    protected void gvQuoteHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvQuoteHeaders, e.Row, "gvQuoteHeadersSortColumn", "gvQuoteHeadersSortDirection", 0);
    }

    protected void gvQuoteDetail_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(sender as GridView, e.Row, "gvQuoteDetailSortColumn", "gvQuoteDetailSortDirection", 0);//apps
    }

    protected void gvQuoteHeader_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvQuoteHeadersSortColumn"] != e.SortExpression)
            Session["gvQuoteHeadersSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvQuoteHeadersSortDirection"] == " ASC")
                Session["gvQuoteHeadersSortDirection"] = " DESC";
            else
                Session["gvQuoteHeadersSortDirection"] = " ASC";
        }

        gvQuoteHeaders.PageIndex = 0;
        Session["gvQuoteHeadersSortColumn"] = e.SortExpression;

        if ((string)Session["ddlQuoteShowResultsBy"] == "Item")
            this.AssignHeaderDataSource(false, "pvttquote_byitem");
        else
            this.AssignHeaderDataSource(false, "pvttquote_header");
    }

    protected void gvQuoteDetail_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvQuoteDetailSortColumn"] != e.SortExpression)
            Session["gvQuoteDetailSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvQuoteDetailSortDirection"] == " ASC")
                Session["gvQuoteDetailSortDirection"] = " DESC";
            else
                Session["gvQuoteDetailSortDirection"] = " ASC";
        }

        GridView gvQuoteDetail = (sender as GridView);
        LinkButton lnkQuoteDetailCustomView = (LinkButton)gvQuoteDetail.Parent.FindControl("lnkQuoteDetailCustomView");

        gvQuoteDetail.PageIndex = 0;
        Session["gvQuoteDetailSortColumn"] = e.SortExpression;
        string Datakey_item = "";
        Datakey_item = gvQuoteDetail.DataKeys[0]["quote_id"].ToString();
        DataTable dtQuoteDetail = new DataTable();
        dtQuoteDetail = GetDetailSortDetails(Datakey_item, false);

        if (lnkQuoteDetailCustomView != null)
        {
            GridViewRow gvrow = (lnkQuoteDetailCustomView.NamingContainer as GridViewRow);
            UpdateDetailInformation(gvrow);
        }

        if (dtQuoteDetail != null)
        {
            gvQuoteDetail.DataSource = dtQuoteDetail;
            gvQuoteDetail.DataBind();
        }
    }

    protected void gvByItem_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvQuoteByItemSortColumn"] != e.SortExpression)
            Session["gvQuoteByItemSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvQuoteByItemSortDirection"] == " ASC")
                Session["gvQuoteByItemSortDirection"] = " DESC";
            else
                Session["gvQuoteByItemSortDirection"] = " ASC";
        }

        this.gvByItem.PageIndex = 0;
        Session["gvQuoteByItemSortColumn"] = e.SortExpression;
        this.AssignHeaderDataSource(false, "pvttquote_byitem");
    }

    protected void gvByItem_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvByItem, e.Row, "gvQuoteByItemSortColumn", "gvQuoteByItemSortDirection", 0);
    }

    protected void gvQuoteDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvQuoteDetail = (sender as GridView);

        LinkButton lnkQuoteDetailCustomView = (LinkButton)gvQuoteDetail.Parent.FindControl("lnkQuoteDetailCustomView");
        GridViewRow gvrow = (lnkQuoteDetailCustomView.NamingContainer as GridViewRow);

        gvQuoteDetail.PageIndex = e.NewPageIndex; //apps

        string Datakey_item = "";
        Datakey_item = gvQuoteDetail.DataKeys[0]["quote_id"].ToString();
        DataTable dtQuoteDetail = new DataTable();

        dtQuoteDetail = GetDetailSortDetails(Datakey_item, false);

        if (lnkQuoteDetailCustomView != null)
        {
            UpdateDetailInformation(gvrow);
        }

        if (dtQuoteDetail != null)
        {
            gvQuoteDetail.DataSource = dtQuoteDetail;
            gvQuoteDetail.DataBind();
        }

        DropDownList ddlQuoteDetailRows = (DropDownList)gvrow.FindControl("ddlQuoteDetailRows");

        SizeHiddenRow(gvrow, ddlQuoteDetailRows, gvQuoteDetail);
    }

    protected void btnCopyQuote_Click(object sender, EventArgs e)
    {
        bool copyNS = false;

        //We want the call to timeout in 2 seconds...
        this.Page.AsyncTimeout = new TimeSpan(0, 0, 2);

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dsPVParamDataSet dsPVParam = (dsPVParamDataSet)dm.GetCache("dsPVParam");
        DataRow[] pvparamrows = dsPVParam.ttparam_pv.Select("property_name='allow_copy_ns'");

        if (pvparamrows.Length == 1)
        {
            if (((string)(pvparamrows[0]["property_value"])).ToLower() != "yes")
                copyNS = false;
            else
                copyNS = true;
        }

        CopyQuoteInputs state = new CopyQuoteInputs();
        state.CopyWithRetail = false;
        state.CopyNonstock = copyNS;

        // create the asynchronous task instance
        PageAsyncTask asyncTask = new PageAsyncTask(
          new BeginEventHandler(this.beginAsyncRequest),
          new EndEventHandler(this.endAsyncRequest),
          new EndEventHandler(this.asyncRequestTimeout),
          state, true);

        // register the asynchronous task instance with the page
        this.Page.RegisterAsyncTask(asyncTask);

        this.Page.ExecuteRegisteredAsyncTasks();

        DisplayOrderMessage("Your Quote has been submitted.");

        mpeConfirmation.Enabled = true;
        mpeConfirmation.Show();
    }

    protected void btnCopyWithRetail_Click(object sender, EventArgs e)
    {
        bool copyNS = false;

        //We want the call to timeout in 2 seconds...
        this.Page.AsyncTimeout = new TimeSpan(0, 0, 2);

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dsPVParamDataSet dsPVParam = (dsPVParamDataSet)dm.GetCache("dsPVParam");
        DataRow[] pvparamrows = dsPVParam.ttparam_pv.Select("property_name='allow_copy_ns'");

        if (pvparamrows.Length == 1)
        {
            if (((string)(pvparamrows[0]["property_value"])).ToLower() != "yes")
                copyNS = false;
            else
                copyNS = true;
        }

        CopyQuoteInputs state = new CopyQuoteInputs();
        state.CopyWithRetail = true;
        state.CopyNonstock = copyNS;

        // create the asynchronous task instance
        PageAsyncTask asyncTask = new PageAsyncTask(
          new BeginEventHandler(this.beginAsyncRequest),
          new EndEventHandler(this.endAsyncRequest),
          new EndEventHandler(this.asyncRequestTimeout),
          state, true);

        // register the asynchronous task instance with the page
        this.Page.RegisterAsyncTask(asyncTask);

        this.Page.ExecuteRegisteredAsyncTasks();

        DisplayOrderMessage("Your Quote has been submitted.");

        mpeConfirmation.Enabled = true;
        mpeConfirmation.Show();
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {

    }

    private void DisplayOrderMessage(string message)
    {
        lblLine1.Visible = true;
        lblLine1.Text = message + "<br />";
        lblLine2.Visible = false;
        lblError.Visible = false;

        DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        if (message.Length > 0)
        {
            lblError.Visible = false;
            lblLine2.Visible = true;

            lblLine2.Text = "An email confirmation will be sent to " + user.GetContextValue("currentUserEmail") + ".";
            //lblLine2.Text += "<br />If the confirmation is not received, please contact our office.";
            lblLine2.Text += "<br /><b>Use Search,</b> to find your newly created quote.";
            lblLine2.Text += "<br /><br /><b>Please Note:</b> There may be a few minutes delay processing this request.";
            //lblLine2.Text += "<br />Do not resubmit this quote or you may create more duplicates than desired.";
        }
    }

    protected void gvQuoteHeaders_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int pointer;
        GridView gv = null;
        int pageindex;
        int pagesize;
        int finalindex = 0;

        switch (e.CommandName)
        {
            case "CopyQuote":
            case "Hide":
            case "Edit":
            case "EditRetailInfo":
            case "Release":
                pointer = Int32.Parse(e.CommandArgument.ToString());
                gv = (GridView)sender;
                pageindex = gv.PageIndex;
                pagesize = gv.PageSize;

                finalindex = pointer - (pageindex * pagesize);
                _myDataItem = gv.DataKeys[finalindex]["quote_id"].ToString();

                Session["_myDataItem"] = _myDataItem;

                gv.SelectedIndex = finalindex;
                gv.EditIndex = -1;

                break;
        }

        if (e.CommandName == "CopyQuote")
        {
            /* Analytics Tracking */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Quotes - Copy quote Icon Click";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

            PriceModeHelper pmh = new PriceModeHelper(new DataManager());

            if (!pmh.InAnyRetailMode())
                btnCopyWithRetail.Visible = false;
            else
                btnCopyWithRetail.Visible = true;

            lblCopyMessage.Text = "Are you sure you want to copy quote " + _myDataItem + "?";

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

            dsPVParamDataSet dsPVParam = (dsPVParamDataSet)dm.GetCache("dsPVParam");
            DataRow[] pvparamrows = dsPVParam.ttparam_pv.Select("property_name='allow_copy_ns'");

            if (pvparamrows.Length == 1)
            {
                if (((string)(pvparamrows[0]["property_value"])).ToLower() != "yes")
                    lblNonstock.Visible = true;
                else
                    lblNonstock.Visible = false;
            }

            this.mpeCopyQuote.Enabled = true;
            this.mpeCopyQuote.Show();
        }

        //if (e.CommandName == "Hide")
        //{
        //    int pointer = Int32.Parse(e.CommandArgument.ToString());
        //    GridView gv = (GridView)sender;
        //    int pageindex = gv.PageIndex;
        //    int pagesize = gv.PageSize;

        //    int finalindex = pointer - (pageindex * pagesize);
        //    gv.SelectedIndex = finalindex;
        //    gv.EditIndex = -1;
        //}

        if (e.CommandName == "Details")
        {
            GridView gv2 = (GridView)sender;
            gv2.SelectedIndex = -1;
            gv2.EditIndex = -1;
        }

        if (e.CommandName == "Edit")
        {
            CollapseChildGridRows();

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Quotes QuoteLists = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            Dmsi.Agility.EntryNET.DataManager myDataManagers = new DataManager();
            DataRow[] QuoteRows = QuoteLists.ReferenceHeaderTable.Select("quote_id='" + _myDataItem + "'");
            myDataManagers.SetCache("pvttquote_header", QuoteRows[0]);
            UserControls_QuoteEdit quote_edit = Parent.FindControl("QuoteEdit") as UserControls_QuoteEdit;

            if (quote_edit != null)
            {
                Dmsi.Agility.Data.Quotes QuoteList = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
                DataRow[] QuoteHeaderRow = QuoteList.ReferenceHeaderTable.Select("quote_id='" + _myDataItem + "'");
                DataRow[] QuoteDetailRows = QuoteList.ReferencedDetailTable.Select("quote_id='" + _myDataItem + "'");

                dsQuoteDataSet ds = new dsQuoteDataSet();
                ds.pvttquote_header.Rows.Add(QuoteHeaderRow[0].ItemArray);

                foreach (DataRow r in QuoteDetailRows)
                {
                    ds.pvttquote_detail.Rows.Add(r.ItemArray);
                }

                foreach (DataRow byitem in QuoteList.ReferencedDataSet.Tables["pvttquote_byitem"].Rows)
                {
                    ds.pvttquote_byitem.Rows.Add(byitem.ItemArray);
                }

                foreach (DataRow ttmisc in QuoteList.ReferencedDataSet.Tables["ttmisc"].Rows)
                {
                    ds.ttmisc.Rows.Add(ttmisc.ItemArray);
                }

                ds.AcceptChanges();

                quote_edit.Initialize(ds);
                quote_edit.Visible = true;

                Session["QuoteEditVisible"] = true;
                this.Visible = false;

                Session["AddInfoQuoteEdit_txtCustomerId"] = null;
                Session["AddInfoQuoteEdit_txtShipTo"] = null;
                Session["AddInfoQuoteEdit_txtCustomerName"] = null;
                Session["AddInfoQuoteEdit_txtAddress1"] = null;
                Session["AddInfoQuoteEdit_txtAddress2"] = null;
                Session["AddInfoQuoteEdit_txtAddress3"] = null;
                Session["AddInfoQuoteEdit_txtCity"] = null;
                Session["AddInfoQuoteEdit_txtState"] = null;
                Session["AddInfoQuoteEdit_txtZip"] = null;
                Session["AddInfoQuoteEdit_txtCountry"] = null;
                Session["AddInfoQuoteEdit_txtPhone"] = null;
                Session["AddInfoQuoteEdit_txtContactName"] = null;
                Session["AddInfoQuoteEdit_txtEmail"] = null;
                Session["AddInfoQuoteEdit_txtReference"] = null;
                Session["AddInfoQuoteEdit_taxRate"] = null;
                Session["AddInfoQuoteEdit_taxableDescription"] = null;
                Session["AddInfoQuoteEdit_taxableAmount"] = null;
                Session["AddInfoQuoteEdit_nonTaxableDescription"] = null;
                Session["AddInfoQuoteEdit_nonTaxableAmount"] = null;

                Session["UIHasChangedAddInfoEdit"] = null;
                Session["UIHasCanceledAddInfoEdit"] = null;

                if (Session["IsQuoteInUpdateMode"] != null && (bool)Session["IsQuoteInUpdateMode"] == true)
                    quote_edit.btnCancel_Click(null, null);
            }

            this.Page.Master.FindControl("ImageButton1").Focus();
        }

        if (e.CommandName == "EditRetailInfo")
        {
            CollapseChildGridRows();

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Quotes QuoteLists = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            Dmsi.Agility.EntryNET.DataManager myDataManagers = new DataManager();
            DataRow[] QuoteRows = QuoteLists.ReferenceHeaderTable.Select("quote_id='" + _myDataItem + "'");
            myDataManagers.SetCache("pvttquote_header", QuoteRows[0]);
            UserControls_QuoteEditRetail_Info quote_editretail_info = Parent.FindControl("QuoteEditRetail_Info") as UserControls_QuoteEditRetail_Info;

            if (quote_editretail_info != null)
            {
                Dmsi.Agility.Data.Quotes QuoteList = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Quotes - Edit Retail Information Icon Click";
                aInput.Label = "";
                aInput.Value = "0";

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

                Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
                DataRow[] QuoteHeaderRow = QuoteList.ReferenceHeaderTable.Select("quote_id='" + _myDataItem + "'");
                DataRow[] QuoteDetailRows = QuoteList.ReferencedDetailTable.Select("quote_id='" + _myDataItem + "'");

                dsQuoteDataSet ds = new dsQuoteDataSet();
                ds.pvttquote_header.Rows.Add(QuoteHeaderRow[0].ItemArray);

                foreach (DataRow r in QuoteDetailRows)
                {
                    ds.pvttquote_detail.Rows.Add(r.ItemArray);
                }

                foreach (DataRow byitem in QuoteList.ReferencedDataSet.Tables["pvttquote_byitem"].Rows)
                {
                    ds.pvttquote_byitem.Rows.Add(byitem.ItemArray);
                }

                foreach (DataRow ttmisc in QuoteList.ReferencedDataSet.Tables["ttmisc"].Rows)
                {
                    ds.ttmisc.Rows.Add(ttmisc.ItemArray);
                }

                ds.AcceptChanges();

                quote_editretail_info.Initialize(ds);
                quote_editretail_info.Visible = true;

                Session["QuoteEditRetail_InfoVisible"] = true;
                this.Visible = false;

                if (Session["IsQuoteInUpdateModeRetail_Info"] != null && (bool)Session["IsQuoteInUpdateModeRetail_Info"] == true)
                    quote_editretail_info.btnCancel_Click(null, null);
            }

            this.Page.Master.FindControl("ImageButton1").Focus();
        }

        if (e.CommandName == "Release")
        {
            CollapseChildGridRows();

            UserControls_QuoteRelease quote_release = Parent.FindControl("QuoteRelease") as UserControls_QuoteRelease;

            if (quote_release != null)
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.Quotes QuoteList = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Quotes - Release quote Icon Click";
                aInput.Label = "";
                aInput.Value = "0";

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

                Dmsi.Agility.EntryNET.DataManager myDataManager = new DataManager();
                DataRow[] QuoteHeaderRow = QuoteList.ReferenceHeaderTable.Select("quote_id='" + _myDataItem + "'");
                DataRow[] QuoteDetailRows = QuoteList.ReferencedDetailTable.Select("quote_id='" + _myDataItem + "'");

                dsQuoteDataSet ds = new dsQuoteDataSet();
                ds.pvttquote_header.Rows.Add(QuoteHeaderRow[0].ItemArray);

                foreach (DataRow r in QuoteDetailRows)
                {
                    ds.pvttquote_detail.Rows.Add(r.ItemArray);
                }

                foreach (DataRow byitem in QuoteList.ReferencedDataSet.Tables["pvttquote_byitem"].Rows)
                {
                    ds.pvttquote_byitem.Rows.Add(byitem.ItemArray);
                }

                foreach (DataRow ttmisc in QuoteList.ReferencedDataSet.Tables["ttmisc"].Rows)
                {
                    ds.ttmisc.Rows.Add(ttmisc.ItemArray);
                }

                ds.AcceptChanges();

                quote_release.Initialize(ds);
                quote_release.Visible = true;

                Session["QuoteReleaseVisible"] = true;
                this.Visible = false;

                Session["AddInfoQuote_txtCustomerId"] = null;
                Session["AddInfoQuote_txtShipTo"] = null;
                Session["AddInfoQuote_txtCustomerName"] = null;
                Session["AddInfoQuote_txtAddress1"] = null;
                Session["AddInfoQuote_txtAddress2"] = null;
                Session["AddInfoQuote_txtAddress3"] = null;
                Session["AddInfoQuote_txtCity"] = null;
                Session["AddInfoQuote_txtState"] = null;
                Session["AddInfoQuote_txtZip"] = null;
                Session["AddInfoQuote_txtCountry"] = null;
                Session["AddInfoQuote_txtPhone"] = null;
                Session["AddInfoQuote_txtContactName"] = null;
                Session["AddInfoQuote_txtEmail"] = null;
                Session["AddInfoQuote_txtReference"] = null;
                Session["AddInfoQuote_taxRate"] = null;
                Session["AddInfoQuote_taxableDescription"] = null;
                Session["AddInfoQuote_taxableAmount"] = null;
                Session["AddInfoQuote_nonTaxableDescription"] = null;
                Session["AddInfoQuote_nonTaxableAmount"] = null;

                Session["UIHasChangedAddInfo"] = null;
                Session["UIHasCanceledAddInfo"] = null;

                this.Page.Master.FindControl("ImageButton1").Focus();
            }
        }

        if (e.CommandName == "Page")
        {

        }
    }

    protected void CollapseChildGridRows()
    {
        for (int i = 0; i <= gvQuoteHeaders.Rows.Count - 1; i++)
        {
            GridViewRow gvr = (GridViewRow)gvQuoteHeaders.Rows[i];
            gvr.FindControl("innergrids").Visible = false;

            Panel p = gvr.FindControl("innerplaceholder") as Panel;

            if (p != null)
                p.Visible = false;

            ImageButton imgbutton = (ImageButton)gvr.FindControl("imgbtnplusminus");
            imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
            imgbutton.CommandName = "Details";
        }

        gvQuoteHeaders.SelectedIndex = -1;
        gvQuoteHeaders.EditIndex = -1;
    }

    protected void gvQuoteHeaders_RowEditing(object sender, GridViewEditEventArgs e)
    {
        e.Cancel = true;
        //had to add this because of the "Edit" button column

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Quotes - Edit quote Icon Click";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();


    }

    protected void lnkQuoteDetailCustomView_Click(object sender, EventArgs e)
    {
        LinkButton lnkQuoteDetailCustom = (sender as LinkButton);
        GridViewRow row = (lnkQuoteDetailCustom.NamingContainer as GridViewRow);

        string QuoteId = gvQuoteHeaders.DataKeys[row.RowIndex].Value.ToString();
        Session["Quotedetails_InnerQuoteId"] = QuoteId;
        Session["Quotedetails_InnerDataRowid"] = row.RowIndex;
        ModalPopupExtender2.Show();
    }

    protected void Show_Hide_ChildGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        imgShowHide.ToolTip = "View";
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        Session["Quotedetails_InnerDataRowid"] = row.RowIndex;
        int irowsid = Convert.ToInt32(imgShowHide.CommandArgument);

        for (int i = 0; i <= gvQuoteHeaders.Rows.Count - 1; i++)
        {
            GridViewRow gvr = (GridViewRow)gvQuoteHeaders.Rows[i];
            ImageButton imgbutton = (ImageButton)gvr.FindControl("imgbtnplusminus");

            if (i != row.RowIndex)
            {
                int inextrow = row.RowIndex;
                inextrow = inextrow + 1;

                if (i == inextrow)
                {
                    gvr.CssClass = row.CssClass = "activealt-row-tr";
                }
                else
                {
                    gvr.CssClass = "";
                }

                gvr.FindControl("innergrids").Visible = false;
                gvr.FindControl("innerplaceholder").Visible = false;

                imgbutton.CommandName = "Details";
                imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                imgbutton.ToolTip = "View";
            }
            else
            {
                if (imgbutton.ImageUrl == "~/images/collapse-icon.png")
                {
                    gvr.FindControl("innergrids").Visible = false;
                    gvr.FindControl("innerplaceholder").Visible = false;

                    imgbutton.CommandName = "Hide";
                    imgbutton.ImageUrl = "~/images/Grid_Plus.gif";
                    imgbutton.ToolTip = "View";
                }
            }
        }

        if (imgShowHide.CommandName == "Details")
        {
            GridViewRow grinner = gvQuoteHeaders.Rows[row.RowIndex];
            row.FindControl("innergrids").Visible = true;

            string QuoteId = gvQuoteHeaders.DataKeys[row.RowIndex].Value.ToString();
            Label lblQuoteInnerId = (Label)row.FindControl("lblQuoteInnerId");
            lblQuoteInnerId.Text = "Quote Details for ID " + QuoteId;
            imgShowHide.CommandName = "Hide";
            imgShowHide.ImageUrl = "~/images/collapse-icon.png";

            GridView gvQuoteDetail = (GridView)grinner.FindControl("gvQuoteDetail");
            InitializeColumnVisibility(gvQuoteDetail, "gvQuoteDetail_Columns_Visible", 0, false);
            csCustomDetail.GridView = gvQuoteDetail;
            BindInnerGridDetails(row.RowIndex);
        }
        else
        {
            row.FindControl("innergrids").Visible = false;
            row.FindControl("innerplaceholder").Visible = false;

            imgShowHide.CommandName = "Details";
            imgShowHide.ImageUrl = "~/images/Grid_Plus.gif";
        }
    }

    protected void BindInnerGridDetails(int rowindex)
    {
        string QuoteId = "";
        GridViewRow grInner = gvQuoteHeaders.Rows[rowindex];
        grInner.CssClass = "active-row-tr";
        QuoteId = gvQuoteHeaders.DataKeys[rowindex]["quote_id"].ToString();
        DropDownList ddlQuoteDetailRows = (DropDownList)grInner.FindControl("ddlQuoteDetailRows");

        GridView gvQuoteDetail = (GridView)grInner.FindControl("gvQuoteDetail");
        int pageSize;
        ddlQuoteDetailRows.SelectedIndex = Profile.ddlQuoteDetailRows_SelectedIndex;

        if (ddlQuoteDetailRows.SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(ddlQuoteDetailRows.SelectedValue);
        }

        gvQuoteDetail.PageSize = pageSize;
        this.AssignDetailDataSource(false, grInner, QuoteId);

        SizeHiddenRow(grInner, ddlQuoteDetailRows, gvQuoteDetail);
    }

    private static void SizeHiddenRow(GridViewRow grInner, DropDownList ddlQuoteDetailRows, GridView gvQuoteDetail)
    {
        UserControls_DocViewer docViewer = (UserControls_DocViewer)grInner.FindControl("docViewer");

        int gvdocHeight = 0;

        GridView gvdoc = (GridView)docViewer.FindControl("gvDocView");

        if (gvdoc != null && gvdoc.Rows.Count > 0)
        {
            gvdocHeight = 42 + (gvdoc.Rows.Count * 42);
        }
        else
            gvdocHeight = -42;

        int detailrowcount = gvQuoteDetail.Rows.Count;

        if (ddlQuoteDetailRows.SelectedValue != "All")
        {
            if (detailrowcount > Convert.ToInt32(ddlQuoteDetailRows.SelectedValue))
            {
                detailrowcount = Convert.ToInt32(ddlQuoteDetailRows.SelectedValue);
            }
        }

        int pagerAddHeight = 0;

        if (gvQuoteDetail.PageCount > 1)
            pagerAddHeight = 56;

        int panelheight = 232 + gvdocHeight + (detailrowcount * 42) + pagerAddHeight;

        grInner.FindControl("innergrids").Visible = true;
        Panel p = grInner.FindControl("innerplaceholder") as Panel;
        p.Visible = true;
        p.Height = new Unit(panelheight, UnitType.Pixel);
    }

    protected void ddlTranID_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDdlSearchTypeValues(ddlTranID.Text);
    }

    private void SetDdlSearchTypeValues(string ddlTranIDText)
    {
        switch (ddlTranIDText)
        {
            case "Quote ID":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.Items.Add(new ListItem("Starting at"));
                break;

            case "Reference #":
            case "Item #":
            case "PO ID":
            case "Job #":
                ddlSearchStyle.Items.Clear();
                ddlSearchStyle.Items.Add(new ListItem("Beginning with"));
                ddlSearchStyle.Items.Add(new ListItem("Containing"));
                ddlSearchStyle.Items.Add(new ListItem("Equals"));
                ddlSearchStyle.SelectedIndex = 2;
                break;
        }
    }

    protected void gvQuoteHeaders_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private IAsyncResult beginAsyncRequest(object sender, EventArgs e, AsyncCallback callback, object state)
    {
        int ReturnCode;
        string MessageText;

        CopyQuoteInputs localState = (CopyQuoteInputs)state;

        Dmsi.Agility.Data.Quotes QuoteLists = new Dmsi.Agility.Data.Quotes("dsQuoteDataSet", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        DataRow[] QuoteRows = QuoteLists.ReferenceHeaderTable.Select("quote_id='" + _myDataItem + "'");

        if (QuoteRows.Length == 1)
        {
            QuoteLists.BeginCopyQuote((string)QuoteRows[0]["prof_name"], Convert.ToInt32(_myDataItem), localState.CopyWithRetail, localState.CopyNonstock);

            Session["_myDataItem"] = "";
        }

        return new MyEmtpyIAsyncResult2();
    }

    private void endAsyncRequest(IAsyncResult result)
    {
        //Do Nothing...
    }

    void asyncRequestTimeout(IAsyncResult result)
    {
        //Do Nothing...
    }

    public class MyEmtpyIAsyncResult2 : IAsyncResult
    {
        public MyEmtpyIAsyncResult2() //Default ctor...
        {
        }

        public object AsyncState
        {
            get { return new object(); }
        }

        public System.Threading.WaitHandle AsyncWaitHandle
        {
            get { return null; }
        }

        public bool CompletedSynchronously
        {
            get { return true; }
        }

        public bool IsCompleted
        {
            get { return true; }
        }
    }

    public class CopyQuoteInputs
    {
        public CopyQuoteInputs() //Default ctor...
        {
        }
        public bool CopyWithRetail
        {
            get;
            set;
        }

        public bool CopyNonstock
        {
            get;
            set;
        }
    }
}

