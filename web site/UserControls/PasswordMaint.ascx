<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PasswordMaint.ascx.cs"
    Inherits="UserControls_passwordMaint" %>

<style type="text/css">
    .blacktext {
        color: black;
    }
</style>

   
        <h3 style="font-size:25px; font-weight:normal;border-bottom: solid 1px #e9e9e9;padding-bottom: 10px;">Change Password</h3>
     <div class="msg-error" id="dverr" runat="server" visible="false" style="width:98%">
          <asp:Label ID="lblValidationError" runat="server" CssClass="ErrorTextRed"></asp:Label>
        </div>
     <asp:Label ID="lblinitialmsg" runat="server"  ForeColor="Red" Font-Size="13px" CssClass="reqpw"/>
<div class=" col-lg-12" style="width:96%">
    <div class="theme-row form" style="margin-left: 15px; margin-right: 15px;width: 98%;">
        
          
        <ul class="default-list"  ID="pnlChangePassword" runat="server">
           
            <li>
                <label>
                    Current password</label>
                <asp:TextBox ID="txtOldPassword" runat="server" Width="210px" TextMode="Password" TabIndex="1" CssClass="changepasstext"></asp:TextBox>&nbsp;
                            <asp:Label ID="lblOldPass" runat="server" CssClass="ErrorTextRed"></asp:Label>
               
            </li>
            <li>
                <label>
                    New password</label>
                 

                <asp:TextBox ID="txtNewPassword" runat="server" Width="210px" TextMode="Password" TabIndex="2" CssClass="changepasstext"></asp:TextBox>&nbsp;
                            <asp:Label ID="lblNewPass" runat="server" CssClass="ErrorTextRed"></asp:Label>

            </li>
            <li>
                <label>
                    Confirm new password</label>
                <asp:TextBox ID="txtConfirmNew" runat="server" Width="210px" TextMode="Password" TabIndex="3" CssClass="changepasstext"></asp:TextBox>
                            <asp:Label ID="lblNewConfirm" runat="server" CssClass="ErrorTextRed" ></asp:Label>
            </li>
        </ul>


       <div class="note" style="width:98%" id="notedetails" runat="server">
            <strong>
    <asp:Label ID="ChangePasswordTitle" runat="server" Text="Change your password" CssClass="CaptionText"></asp:Label>


            </strong>
        </div>
        
        <div class="update-row" runat="server" id="updatebuttons">


             <asp:Button ID="btnChange" runat="server" OnClick="btnChange_Click" Text="Change Password" class="btn btn-primary"
                              TabIndex="5"   />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-default" OnClick="btnCancel_Click" TabIndex="4" />


        </div>
       
        <div class="msg-success" id="pnlSuccess" runat="server" visible="false">
            <asp:Label CssClass="blacktext" ID="lblSuccess" runat="server" Text="lblSuccess" Width="100%" ForeColor="Black" Font-Bold="False"></asp:Label>
            <asp:LinkButton CssClass="brd-crumbactive" ID="lnkContinue" runat="server" PostBackUrl="../LogOn.aspx">Continue</asp:LinkButton>
        </div>
    </div>
</div>
       
