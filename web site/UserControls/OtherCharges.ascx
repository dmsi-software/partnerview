﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OtherCharges.ascx.cs" Inherits="UserControls_OtherCharges" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<asp:Label 
    ID="lblOtherChargesDescription" 
    runat="server" 
    Text="Description"
    CssClass="editRetailCustLabel">
</asp:Label>
<asp:TextBox
    ID="txtOtherChargesDescription"
    runat="server"
    MaxLength="40" 
    CssClass="otherChargesDescriptionInput">
</asp:TextBox>
<asp:Label 
    ID="lblOtherChargesAmount" 
    runat="server" 
    Text="Amount"
    CssClass="otherChargesAmountLabel">
</asp:Label>
<ig:WebNumericEditor 
    ID="numericOtherChargesAmount"
    runat="server" 
    DataMode="Decimal"
    MaxDecimalPlaces="2"
    MinValue="0.00"
    MaxValue="99999.99" 
    Nullable="False" 
    CssClass="textarea otherChargesAmountInput" 
    HideEnterKey="True" 
    MaxLength="8">
</ig:WebNumericEditor>
