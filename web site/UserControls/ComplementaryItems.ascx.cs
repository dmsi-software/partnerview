﻿using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Infragistics.Web.UI.NavigationControls;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class UserControls_ComplementaryItems : System.Web.UI.UserControl
{
    public delegate void AddToCartDelegate(dsOrderCartDataSet.ttorder_cartRow row);
    public event AddToCartDelegate OnAddToCart;

    private dsInventoryDataSet _dsInventory;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["ComplementarySource"] != null && (string)Session["ComplementarySource"] == "Inventory")
        {
            btnAddToCart.Text = "Add to Cart";
            trHeader1.Visible = false;
            trHeader2.Visible = false;
        }

        if (Session["ComplementarySource"] != null && (string)Session["ComplementarySource"] == "QuoteEditAddItems")
        {
            btnAddToCart.Text = "Add to Quote";
            trHeader1.Visible = true;
            trHeader2.Visible = true;
            dsQuoteDataSet dsQuoteCurrentQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
            lblQuoteHeader.Text = "Add Items to Quote " + dsQuoteCurrentQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
        }

        if (Session["CompItemsValidationError"] != null && (bool)Session["CompItemsValidationError"] == true)
        {
            //Don't rebind data if there are validation errors; but reset the flag
            Session["CompItemsValidationError"] = false;
        }
        else
        {
            trCompItemsValidationErrors.Visible = false;

            if (Session["dsInventoryComplementaryItems"] != null && ((dsInventoryDataSet)Session["dsInventoryComplementaryItems"]).ttinventory.Rows.Count > 0 && Session["CompItemsHaveBeenFetched"] != null)
            {
                Session["CompItemsHaveBeenFetched"] = null;
                //above Session variable was set in the DAL to keep the DataBind() from happening more than once.

                _dsInventory = (dsInventoryDataSet)Session["dsInventoryComplementaryItems"];

                DataView view = new DataView(_dsInventory.Tables["ttinventory"]);
                view.Sort = "comp_sort_seq";
                lvProducts.DataSource = view;
                lvProducts.DataBind();
            }
        }

        if (Session["BlockPopupMenu"] != null && (bool)Session["BlockPopupMenu"] == true)
        {
            HtmlImage img = Page.Master.FindControl("imgCustAddress") as HtmlImage;
            if (img != null)
                img.Attributes.Remove("onmouseover");

            ImageButton imgButton = Page.Master.FindControl("imgLogo") as ImageButton;
            if (imgButton != null)
                imgButton.Enabled = false;
        }

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
    }

    protected void btnAddToCart_Click(object sender, EventArgs e)
    {
        bool isValid = ValidateTextboxesInProductsListView();
      
        if (isValid)
        {
            AddItemsToCartOrQuote(btnAddToCart.Text);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
        }
    }

    protected void btnContinueShopping_Click(object sender, EventArgs e)
    {
        if (Session["ComplementarySource"] != null && (string)Session["ComplementarySource"] == "Inventory")
        {
            Session["ComplementarySource"] = null;
            Session["BlockPopupMenu"] = false;

            this.Visible = false;
            Control inventory1 = Parent.FindControl("Inventory1");
            inventory1.Visible = true;

            MenuEnableDisable menuEnable = new MenuEnableDisable();
            menuEnable.EnableMenuItems(this.Page, this.Parent);

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                    {
                        Page.Master.FindControl("liNetRetail").Visible = true;
                        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

                        if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || 
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || 
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                        {
                            ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
                        }
                        else
                        {
                            ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
                        }
                    }
                }
            }
        }

        if (Session["ComplementarySource"] != null && (string)Session["ComplementarySource"] == "QuoteEditAddItems")
        {
            Session["ComplementarySource"] = null;
            this.Visible = false;
            Control quoteeditadditems = Parent.Parent.FindControl("QuoteEditAddItems");
            quoteeditadditems.Visible = true;
        }

        AnalyticsInput aInput = new AnalyticsInput();
        aInput.Type = "event";
        aInput.Action = "Complementary Items - Continue Shopping Button";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();


    }

    protected void lvProducts_ItemDataBound(object sender, System.Web.UI.WebControls.ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            object obj = ((System.Web.UI.WebControls.ListViewDataItem)(e.Item)).DataItem;
            DataRow CurrentRow = ((DataRowView)obj).Row;
            DataManager dm = new DataManager();

            if (!string.IsNullOrEmpty(Convert.ToString(CurrentRow["ItemRecid"])))
            {
                if (Convert.ToString(CurrentRow["ItemRecid"]) == "0")
                {

                }
                else
                {
                    Image imgProduct = (Image)e.Item.FindControl("imgProduct");

                    if (!DBNull.Value.Equals(CurrentRow["image_file"]))
                    {
                        if (CurrentRow["image_file"].ToString().Length > 0)
                        {
                            imgProduct.ImageUrl = (string)CurrentRow["image_file"];
                        }
                        else
                        {
                            imgProduct.Visible = false;
                        }
                    }
                    else
                    {
                        imgProduct.Visible = false;
                    }

                    Label lbtProdDesc = (Label)e.Item.FindControl("lbtProdDesc");

                    if (!DBNull.Value.Equals(CurrentRow["DESCRIPTION"]))
                    {
                        string Size = string.Empty;

                        if (!DBNull.Value.Equals(CurrentRow["Size"]))
                        {
                            Size = (string)(CurrentRow["Size"]);
                        }

                        lbtProdDesc.Text = Size + " " + (string)CurrentRow["DESCRIPTION"];
                    }

                    Label lblFrequently = (Label)e.Item.FindControl("lblFrequently");

                    if (!DBNull.Value.Equals(CurrentRow["base_item_desc_list"]))
                    {
                        lblFrequently.Text = (string)CurrentRow["base_item_desc_list"];
                    }

                    Label lblAvailable = (Label)e.Item.FindControl("lblAvailable");
                    Label lblAvilableCount = (Label)e.Item.FindControl("lblAvilableCount");
                    Label lblAvailUOM = (Label)e.Item.FindControl("lblAvailUOM");

                    DropDownList ddluom = e.Item.FindControl("ddlUOM") as DropDownList;
                    PopulateUOMDropDown(ddluom, Convert.ToString(CurrentRow["item_ptr"]), Convert.ToString(CurrentRow["stocking_uom"]), Convert.ToString(CurrentRow["TYPE"]));

                    bool viewInvAvailQty = false;

                    if (Session["Show_Available"] != null && (bool)Session["Show_Available"])
                        viewInvAvailQty = true;

                    bool viewText = false;
                    bool viewQuantity = false;

                    string availableTextDisplay;

                    dsSessionMgrDataSet dsSession = (dsSessionMgrDataSet)dm.GetCache("dsSessionManager");
                    DataRow[] rows = dsSession.ttSelectedProperty.Select("propertyName='qtyAvailableDisplay'");

                    if (rows.Length == 1)
                    {
                        availableTextDisplay = rows[0]["propertyValue"].ToString();

                        if (availableTextDisplay == "Text")
                            viewText = true;

                        if (availableTextDisplay == "Quantity")
                            viewQuantity = true;
                    }
                    
                    if (viewInvAvailQty && (viewQuantity || viewText))
                    {
                        if (!DBNull.Value.Equals(CurrentRow["qty_text"]))
                        {
                            if (CurrentRow["qty_text"].ToString() != " ")
                            {
                                if (viewText == true)
                                {
                                    lblAvilableCount.Text = (string)Convert.ToString(CurrentRow["qty_text"]);
                                    lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_text"]);
                                }
                                else if (viewQuantity == true)
                                {
                                    decimal Quantity = (decimal)CurrentRow["qty_available"];
                                    lblAvilableCount.Text = Quantity.ToTrimmedString();
                                    lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_available"]);

                                    if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                                    {
                                        lblAvailUOM.Text = " " + (string)CurrentRow["stocking_uom"];
                                    }
                                }
                            }
                            else
                            {
                                decimal Quantity = (decimal)CurrentRow["qty_available"];
                                lblAvilableCount.Text = Quantity.ToTrimmedString();
                                lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_available"]);

                                if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                                {
                                    lblAvailUOM.Text = " " + (string)CurrentRow["stocking_uom"];
                                }
                            }
                        }
                        else
                        {
                            if (!DBNull.Value.Equals(CurrentRow["qty_available"]))
                            {
                                decimal sQuantity = (decimal)CurrentRow["qty_available"];
                                lblAvilableCount.Text = sQuantity.ToTrimmedString();
                            }
                        }
                    }
                    else
                    {
                        lblAvailable.Visible = false;
                    }

                    Label lblPriceText = (Label)e.Item.FindControl("lblPriceText");
                    Label lblPrice = (Label)e.Item.FindControl("lblPrice");

                    if (!DBNull.Value.Equals(CurrentRow["price"]))
                    {
                        string PriceUom = string.Empty;
                        decimal Price = (decimal)CurrentRow["price"];

                        if (!DBNull.Value.Equals(CurrentRow["price_uom"]))
                        {
                            PriceUom = (string)CurrentRow["price_uom"];
                        }

                        if (string.IsNullOrEmpty(PriceUom))
                        {
                            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && (bool)Session["ViewInventoryPrices"])
                            {
                                if (Session["UserPref_DisplaySetting"] != null &&
                                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                                {
                                    lblPrice.Text = "Retail price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["retail_price"]);
                                }
                                else
                                {
                                    lblPrice.Text = "Price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["price"]);
                                }
                            }
                        }
                        else
                        {
                            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && Session["ViewInventoryPrices"] != null && (bool)Session["ViewInventoryPrices"])
                            {
                                if (Session["UserPref_DisplaySetting"] != null &&
                                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                                {
                                    decimal dMarkup = 1;

                                    if (Session["MarkupFactor"] != null)
                                    {
                                        dMarkup = (decimal)Session["MarkupFactor"];
                                    }

                                    lblPrice.Text = "Retail price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["retail_price"]) + "/" + PriceUom; ;
                                }
                                else
                                {
                                    lblPrice.Text = "Price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["price"]) + "/" + PriceUom;
                                }
                            }

                            if (Session["ViewInventoryPrices"] == null)
                            {
                                lblPrice.Text = "";
                            }
                        }
                    }

                    HiddenField hdnItem = (HiddenField)e.Item.FindControl("hdnItem");
                    hdnItem.Value = CurrentRow["ITEM"].ToString();

                    Label lblMinPack = (Label)e.Item.FindControl("lblMinPack");
                    HiddenField hdnMinPack = (HiddenField)e.Item.FindControl("hdnMinPack");

                    if (!DBNull.Value.Equals(CurrentRow["min_pak"]))
                    {
                        if ((decimal)CurrentRow["min_pak"] > 0)
                        {
                            lblMinPack.Text = "Must be ordered in multiples of " + CurrentRow["min_pak"].ToString();
                            hdnMinPack.Value = CurrentRow["min_pak"].ToString();
                            lblMinPack.Visible = true;
                        }
                        else
                        {
                            lblMinPack.Text = "";
                            hdnMinPack.Value = "";
                            lblMinPack.Visible = false;
                        }
                    }
                }
            }
        }
    }

    protected void lvProducts_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

    }

    private bool ValidateTextboxesInProductsListView()
    {
        bool isValid = true;

        if (lvProducts.Items.Count > 0)
        {
            for (int i = 0; i < lvProducts.Items.Count; i++)
            {
                ListViewItem row = lvProducts.Items[i];
                Label lblMinPack = (Label)row.FindControl("lblMinPack");
                TextBox enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

                if (enteredQtyTextBox.Text.Trim().Length > 0) // if there is no text in the quantity input textbox, then loop.
                {
                    decimal parsedDecimal = 0;
                    bool parsed = Decimal.TryParse(enteredQtyTextBox.Text, out parsedDecimal);

                    if (!parsed)
                    {
                        isValid = false;
                        Session["CompItemsValidationError"] = true;
                        trCompItemsValidationErrors.Visible = true;
                        lblCompItemsValidationErrors.Text = "Item \"" + ((Label)lvProducts.Items[i].FindControl("lbtProdDesc")).Text + "\" has invalid quantity.";

                        break;
                    }
                    else if (parsedDecimal <= 0)
                    {
                        isValid = false;
                        Session["CompItemsValidationError"] = true;
                        trCompItemsValidationErrors.Visible = true;
                        lblCompItemsValidationErrors.Text = "Item \"" + ((Label)lvProducts.Items[i].FindControl("lbtProdDesc")).Text + "\" zero or negative values are not valid.";
                        break;
                    }
                    //else if (lblMinPack.Text.Length > 0)
                    //{
                    //    HiddenField hdnMinPack = (HiddenField)row.FindControl("hdnMinPack");
                    //    decimal modulus = Decimal.Parse(enteredQtyTextBox.Text) % Decimal.Parse(hdnMinPack.Value);

                    //    if (modulus != 0)
                    //    {
                    //        isValid = false;
                    //        Session["CompItemsValidationError"] = true;
                    //        trCompItemsValidationErrors.Visible = true;
                    //        lblCompItemsValidationErrors.Text = "Item \"" + ((Label)lvProducts.Items[i].FindControl("lbtProdDesc")).Text + "\" must be ordered in multiples of " + hdnMinPack.Value.Trim() + "."; //+ " " + ((Label)lvProducts.Items[i].FindControl("lblqtyuom")).Text.Trim() 
                    //        break;
                    //    }
                    //}
                }
            }
        }
        else
            isValid = false;

        return isValid;
    }

    private void AddItemsToCartOrQuote(string methodMode)
    {
        string item, size, description, uom, priceUom, qty_uom_conv_ptr_sysid;
        int qty_uom_conv_ptr;
        decimal quantity, price, minpack, extension;
        decimal netprice = 0;
        decimal retailprice = 0;
        decimal thickness, width, length;
        TextBox enteredQtyTextBox;
        ListViewItem row;

        dsQuoteDataSet dsQuoteCurrentQuote = new dsQuoteDataSet();
        dsQuoteDataSet dsQuoteAddNewItems = new dsQuoteDataSet();

        if (methodMode == "Add to Quote")
        {
            if (Session["QuoteEdit_dsQuote"] != null)
            {
                dsQuoteCurrentQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
                dsQuoteAddNewItems = dsQuoteCurrentQuote.Copy() as dsQuoteDataSet;
                dsQuoteAddNewItems.pvttquote_detail.Clear();
                dsQuoteAddNewItems.AcceptChanges();
            }
        }

        for (int x = 0; x < lvProducts.Items.Count; x++)
        {
            row = lvProducts.Items[x];
            enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

            if (enteredQtyTextBox.Text.Trim().Length == 0)
                continue;

            quantity = Decimal.Parse(enteredQtyTextBox.Text);
            item = lvProducts.DataKeys[x]["ITEM"].ToString();
            thickness = (decimal)lvProducts.DataKeys[x]["thickness"];
            width = (decimal)lvProducts.DataKeys[x]["WIDTH"];
            length = (decimal)lvProducts.DataKeys[x]["LENGTH"];
            size = lvProducts.DataKeys[x]["Size"].ToString();
            description = lvProducts.DataKeys[x]["DESCRIPTION"].ToString();

            //MDM - Need to fetch new UOM data here...
            DropDownList ddluom = lvProducts.Items[x].FindControl("ddlUOM") as DropDownList;
            string itemtype = Convert.ToString(lvProducts.DataKeys[x]["TYPE"]);

            dsInventoryDataSet dsCopy = new dsInventoryDataSet();

            if (!(itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good"))
                dsCopy = FetchAllDataForChangedUOM(item, Convert.ToInt32(ddluom.SelectedItem.Value));

            decimal uomConvFactor = 0;

            if (dsCopy.ttinventory.Rows.Count > 0)
            {
                DataRow[] rows = dsCopy.ttitem_uomconv.Select("uom_ptr = " + Convert.ToInt32(ddluom.SelectedItem.Value));

                if (rows.Length == 1)
                {
                    uomConvFactor = Convert.ToDecimal(rows[0]["conv_factor"]);
                }
            }

            if (dsCopy.ttinventory.Rows.Count == 0)
            {
                uom = lvProducts.DataKeys[x]["stocking_uom"].ToString();
                qty_uom_conv_ptr = Convert.ToInt32(lvProducts.DataKeys[x]["qty_uom_conv_ptr"]);
                qty_uom_conv_ptr_sysid = Convert.ToString(lvProducts.DataKeys[x]["qty_uom_conv_ptr_sysid"]);
                netprice = Convert.ToDecimal(lvProducts.DataKeys[x]["price"]);
                retailprice = Convert.ToDecimal(lvProducts.DataKeys[x]["retail_price"]);
                minpack = Convert.ToDecimal(lvProducts.DataKeys[x]["min_pak"]);
            }
            else
            {
                uom = (string)dsCopy.ttinventory.Rows[0]["qty_uom"];
                qty_uom_conv_ptr = Convert.ToInt32(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr"]);
                qty_uom_conv_ptr_sysid = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr_sysid"]);
                netprice = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["price_disp"]);
                retailprice = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["retail_price_disp"]);
                minpack = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
            }

            if (Session["ViewInventoryPrices"] != null && (bool)Session["ViewInventoryPrices"] == true)
            {
                if (Session["UserPref_DisplaySetting"] != null &&
                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                {
                    price = retailprice;
                }
                else
                    price = netprice;
            }
            else
            {
                price = 0;
            }

            if (itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good")
                priceUom = uom;
            else if (dsCopy.ttinventory.Rows.Count > 0)
                priceUom = (string)dsCopy.ttinventory.Rows[0]["price_uom_disp"];
            else
                priceUom = uom;

            extension = price * quantity;

            bool minimumQtyViolation = false;

            if (minpack > 0)
            {
                if (Convert.ToDecimal(minpack) > 0 && Convert.ToDecimal(minpack) < 1)
                {
                    if (!quantity.ToString().Contains("."))
                    {
                        minimumQtyViolation = false;
                    }
                    else
                    {
                        decimal decimalPortion = Convert.ToDecimal(quantity.ToString().Substring(quantity.ToString().IndexOf(".")));
                        minimumQtyViolation = Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(decimalPortion), 0), 4) % Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(minpack), 0), 4) > 0;
                    }
                }
                else
                {
                    if (!quantity.ToString().Contains("."))
                    {
                        minimumQtyViolation = quantity % Convert.ToDecimal(minpack) > 0;
                    }
                    else
                    {
                        minimumQtyViolation = quantity % Decimal.Round(Convert.ToDecimal(1) / uomConvFactor, 4) > 0;
                    }
                }
            }

            if (minimumQtyViolation)
            {
                Session["CompItemsValidationError"] = true;
                trCompItemsValidationErrors.Visible = true;
                lblCompItemsValidationErrors.Text = "Item \"" + ((Label)lvProducts.Items[x].FindControl("lbtProdDesc")).Text + "\" must be ordered in multiples of " + Convert.ToString(minpack).Trim() + ".";
                return; //MDM - bail out because we failed min-pack validation
            }

            if (methodMode == "Add to Cart")
            {
                int ReturnCode;
                string MessageText;

                CartAdd(quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length, out ReturnCode, out MessageText);
            }

            if (methodMode == "Add to Quote")
            {
                QuoteDataSetAddItem(ref dsQuoteAddNewItems, quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length);
            }
        }

        if (methodMode == "Add to Quote")
        {
            //After all the items are added to the dsQuoteAddNewItems DataSet, above, now add all the items to the Quote.

            decimal markupFactorValue = 0;
            string retailMode = "Net price";

            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                retailMode = (string)Session["UserPref_DisplaySetting"];

                if (Session["MarkupFactor"] != null)
                {
                    markupFactorValue = Convert.ToDecimal(Session["MarkupFactor"]);
                }
            }

            if (dsQuoteAddNewItems.pvttquote_detail.Rows.Count > 0)
            {
                foreach (dsQuoteDataSet.pvttquote_detailRow dtRow in dsQuoteAddNewItems.pvttquote_detail.Rows)
                {
                    dtRow["retail_mode"] = retailMode;
                }

                dsQuoteAddNewItems.AcceptChanges();

                Quotes q = new Quotes();
                string messageText = q.AddQuoteDetailItem(markupFactorValue, dsQuoteAddNewItems);

                if (messageText != "0")
                {
                    if (messageText.Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    Session["CompItemsValidationError"] = true;
                    trCompItemsValidationErrors.Visible = true;
                    lblCompItemsValidationErrors.Text = messageText;
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
                }
            }
        }
    }

    private void CartAdd(decimal qty, string item, string size, string description, string uom, int qty_uom_conv_ptr, string qty_uom_conv_ptr_sysid, decimal price, string priceUOM, decimal minpack, decimal extension, decimal thickness, decimal width, decimal length, out int ReturnCode, out string MessageText)
    {
        ReturnCode = 0;
        MessageText = "";

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        string branchID = ((dsCustomerDataSet.ttbranch_custRow)dm.GetCache("CurrentBranchRow")).branch_id;
        string userID = ((AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser")).GetContextValue("currentUserLogin");

        Shipto currentShipto = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        OrderCart myOrderCart = null;

        decimal markupFactorValue = 1;
        if (Session["MarkupFactor"] != null)
        {
            markupFactorValue = Convert.ToDecimal(Session["MarkupFactor"]);
        }

        if (dm.GetCache("OrderCart") != null)
            myOrderCart = (OrderCart)dm.GetCache("OrderCart");
        else
        {
            myOrderCart = new OrderCart(branchID, userID, currentShipto.ShiptoObj.ToString(), "<all>", markupFactorValue, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            dm.SetCache("OrderCart", myOrderCart);
        }

        // This session variable must be cleared when, branch, customer or shipto has changed.
        // The above myOrderCart will check the session variable first before called the back-end logic.
        // OrderForm.Reset() should clear this session variable.
        if (dm.GetCache("dsOrderCartDataSet") == null)
            dm.SetCache("dsOrderCartDataSet", myOrderCart.ReferencedDataSet);

        // NOTE: The price should be figured out when the order/cart line is created for display.
        //       The cart table doesn't store price because price could change.
        dsOrderCartDataSet.ttorder_cartDataTable cart = new dsOrderCartDataSet.ttorder_cartDataTable();
        dsOrderCartDataSet.ttorder_cartRow row = cart.Newttorder_cartRow();
        row.branch_id = branchID;
        row.user_id = userID;
        row.cust_obj = currentShipto.CustomerObj;
        row.shipto_obj = currentShipto.ShiptoObj;
        row.ITEM = item;
        row.line_message = "";
        row.SIZE = size;
        row.DESCRIPTION = description;
        row.qty = qty;
        row.uom = uom;
        row.qty_uom_conv_ptr = qty_uom_conv_ptr;
        row.qty_uom_conv_ptr_sysid = qty_uom_conv_ptr_sysid;
        row.price_uom = priceUOM;
        row.price = price;
        row.min_pak = minpack;
        row.extension = extension;
        row.thickness = thickness;
        row.WIDTH = width;
        row.LENGTH = length;
        row.location_reference = "";

        //MDM -- add record_type and control_no for keys
        row.record_type = "";
        row.control_no = 0;
        
        myOrderCart.AddToCart(row, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }  

        // This event should be caught by the parent then the parent should call a public method
        // of the Order Form to refresh its contents.
        if (this.OnAddToCart != null)
            this.OnAddToCart(row);

        currentShipto.Dispose();
        row = null;
        cart.Dispose();
        dm = null;
    }

    private void QuoteDataSetAddItem(ref dsQuoteDataSet ds, decimal qty, string item, string size, string description, string uom, int qty_uom_conv_ptr, string qty_uom_conv_ptr_sysid, decimal price, string priceUOM, decimal minpack, decimal extension, decimal thickness, decimal width, decimal length)
    {

        string rowRetailMode = "";

        if (Session["UserPref_DisplaySetting"] != null)
        {
            if ((string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost")
            {
                rowRetailMode = "cost";
            }
            else if ((string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
            {
                rowRetailMode = "list";
            }
        }

        dsInventoryDataSet dsInv = (dsInventoryDataSet)Session["dsInventoryComplementaryItems"];
        DataRow[] rows = dsInv.ttinventory.Select("item='" + item + "' AND thickness=" + thickness.ToString() + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString());

        dsQuoteDataSet.pvttquote_detailRow row = ds.pvttquote_detail.Newpvttquote_detailRow();
        row.system_id = (string)ds.pvttquote_header.Rows[0]["system_id"];
        row.ITEM_ptr = (int)rows[0]["item_ptr"];
        row.quote_id = (int)ds.pvttquote_header.Rows[0]["quote_id"];
        row.sequence = 0;
        row.ITEM = item;
        row.SIZE = size;
        row.quote_desc = description;
        row.qty_ordered = qty;
        row.uom = uom;
        row.ordqty_uom_conv_ptr = qty_uom_conv_ptr;
        row.ordqty_uom_conv_ptr_sysid = qty_uom_conv_ptr_sysid;
        row.price_uom_code = priceUOM;
        row.price = price;
        row.retail_mode = rowRetailMode;
        row.extended_price = extension;
        row.thickness = thickness;
        row.width = width;
        row.length = length;
        row.location_reference = "";

        ds.pvttquote_detail.Rows.Add(row);
        ds.AcceptChanges();
    }

    protected void ddlUOM_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        string itemcode = ddl.DataMember;
        int uompointer = Convert.ToInt32(ddl.SelectedItem.Value);
        FetchAllDataForChangedUOM(itemcode, uompointer);
    }

    private dsInventoryDataSet FetchAllDataForChangedUOM(string itemcode, int uompointer)
    {
        DataManager dm = new DataManager();
        string currentbranch = (string)dm.GetCache("currentBranchID");

        bool showavailable = false;

        if (Session["Show_Available"] != null)
            showavailable = (bool)Session["Show_Available"];

        decimal markupfactor = 0;

        if (Session["MarkupFactor"] != null)
            markupfactor = Convert.ToDecimal(Session["MarkupFactor"]);

        dsInventoryDataSet ds = new dsInventoryDataSet();
        dsInventoryDataSet dsCopy = new dsInventoryDataSet();

        ds = dm.GetCache("dsInventoryComplementaryItems") as dsInventoryDataSet;

        DataRow[] rows = ds.ttinventory.Select("ITEM='" + itemcode + "'");

        if (rows.Length == 1)
        {
            dsCopy.ttinventory.Rows.Add(rows[0].ItemArray);
        }

        DataRow[] uomrows = ds.ttitem_uomconv.Select("item_ptr = " + (Int32)rows[0]["item_ptr"] + " AND uom_ptr = '" + uompointer + "'");

        if (uomrows.Length == 1)
        {
            dsCopy.ttitem_uomconv.Rows.Add(uomrows[0].ItemArray);
        }

        dsCopy.AcceptChanges();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory invdal = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        invdal.FetchUOM(currentbranch, markupfactor, ref dsCopy, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        UpdateUIForChangedUOM(itemcode, uompointer, dsCopy);

        return dsCopy;
    }

    private void UpdateUIForChangedUOM(string itemcode, int uompointer, dsInventoryDataSet dsCopy)
    {
        DataManager dm = new DataManager();

        bool viewText = false;
        bool viewQuantity = false;

        string availableTextDisplay;

        dsSessionMgrDataSet dsSession = (dsSessionMgrDataSet)dm.GetCache("dsSessionManager");
        DataRow[] rows = dsSession.ttSelectedProperty.Select("propertyName='qtyAvailableDisplay'");

        if (rows.Length == 1)
        {
            availableTextDisplay = rows[0]["propertyValue"].ToString();

            if (availableTextDisplay == "Text")
                viewText = true;

            if (availableTextDisplay == "Quantity")
                viewQuantity = true;
        }

        if (lvProducts.Items.Count > 0)
        {
            for (int x = 0; x < lvProducts.Items.Count; x++)
            {
                if (itemcode == (lvProducts.Items[x].FindControl("hdnItem") as HiddenField).Value)
                {
                    decimal price = 0;

                    if ((string)Session["UserPref_DisplaySetting"] == "Net price")
                        price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["price_disp"]);
                    else
                        price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["retail_price_disp"]);

                    (lvProducts.Items[x].FindControl("lblPriceText") as Label).Text = string.Format("{0:C4}", price) + "/" + (string)dsCopy.ttinventory.Rows[0]["price_uom_disp"];

                    if (viewQuantity)
                    {
                        (lvProducts.Items[x].FindControl("lblAvilableCount") as Label).Text = ((decimal)dsCopy.ttinventory.Rows[0]["qty_available_disp"]).ToTrimmedString();
                    }

                    (lvProducts.Items[x].FindControl("lblAvailUOM") as Label).Text = (string)dsCopy.ttinventory.Rows[0]["qty_uom"];

                    if (viewText)
                    {
                        (lvProducts.Items[x].FindControl("lblAvilableCount") as Label).Text = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_text"]);
                        (lvProducts.Items[x].FindControl("lblAvailUOM") as Label).Text = "";
                    }

                    if ((decimal)dsCopy.ttinventory.Rows[0]["min_pak_disp"] > 0)
                    {
                        (lvProducts.Items[x].FindControl("lblMinPack") as Label).Text = "Must be ordered in multiples of " + String.Format("{0:#,###0.0000}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);

                        if ((lvProducts.Items[x].FindControl("lblMinPack") as Label).Text.EndsWith(".0000"))
                            (lvProducts.Items[x].FindControl("lblMinPack") as Label).Text = "Must be ordered in multiples of " + String.Format("{0:#,###0}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                    }
                }
            }
        }
    }

    private void PopulateUOMDropDown(DropDownList ddluom, string itempointer, string originalstockinguom, string itemtype)
    {
        ddluom.Items.Clear();

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsInventoryDataSet ds = dm.GetCache("dsInventoryComplementaryItems") as dsInventoryDataSet;

        if (!(itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good"))
        {
            DataRow[] rows = ds.ttitem_uomconv.Select("item_ptr='" + itempointer + "'");

            foreach (DataRow row in rows)
            {
                string padding = "";

                switch (((string)row["uom_code"]).Length)
                {
                    case 1:
                        padding = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        break;
                    case 2:
                        padding = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        break;
                    case 3:
                        padding = "&nbsp;&nbsp;&nbsp;&nbsp;";
                        break;
                    case 4:
                        padding = "&nbsp;&nbsp;";
                        break;
                }

                ListItem it = new ListItem((string)row["uom_code"] + Server.HtmlDecode(padding) + row["description"], Convert.ToString(row["uom_ptr"]));
                ddluom.Items.Add(it);
            }
        }
        else
        {
            ListItem it = new ListItem(originalstockinguom);
            ddluom.Items.Add(it);
        }

        for (int x = 0; x < ddluom.Items.Count; x++)
        {
            if (ddluom.Items[x].Text.StartsWith(originalstockinguom))
            {
                ddluom.SelectedIndex = x;
                break;
            }
        }
    }
}