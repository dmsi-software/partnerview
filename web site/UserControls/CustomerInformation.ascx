<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerInformation.ascx.cs" Inherits="UserControls_CustomerInformation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                        TargetControlID="CustomerInformationFieldPanel"
                        CollapsedSize="0"
                        Collapsed="False"
                        ExpandControlID="CustomerInformationPanel"
                        CollapseControlID="CustomerInformationPanel"
                        AutoCollapse="False"
                        AutoExpand="false"
                        ScrollContents="False"
                        CollapsedText="Customer Information"
                        ExpandedText="Customer Information"
                        ImageControlID="CustomerInformationImage"
                        ExpandedImage="~/images/expand_blue.jpg"
                        CollapsedImage="~/images/collapse_blue.jpg"
                        ExpandDirection="Vertical"
                        SuppressPostBack="true">
</cc1:CollapsiblePanelExtender>
<div>
    <asp:Panel ID="CustomerInformationPanel" runat="server" CssClass="CollapsibleHeaderPanel"
        onmouseover="this.className='CollapsibleHeaderPanelHover'" onmouseout="this.className='CollapsibleHeaderPanel'">
        <div class="CollapsibleHeaderDIV">
            <asp:Image ID="CustomerInformationImage" runat="server" ImageAlign="Right" ImageUrl="~/images/collapse_blue.jpg" />Account
            Information</div>
    </asp:Panel>
    <asp:Panel ID="CustomerInformationFieldPanel" runat="server" CssClass="SimplePanelBody">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="50%" style="width: 50%">
                <asp:Label ID="lblCustomerNameTitle" runat="server" CssClass="FieldLabel"
                    Text="Account:"></asp:Label>
                </td>
                <td>
                <asp:Label ID="lblCustomerCode" runat="server" CssClass="Field"></asp:Label>
                </td>
            </tr>
        </table>
        &nbsp;
        <asp:Label ID="lblCustomerName" runat="server" CssClass="Field" Width="90%"></asp:Label>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="50%" style="width: 50%">
                <asp:Label ID="Label2" runat="server" CssClass="FieldLabel" Text="Ship-to:"></asp:Label>
                </td>
                <td>
                <asp:Label ID="lblShiptoSequence" runat="server" CssClass="Field"></asp:Label>
                </td>
            </tr>
        </table>
        &nbsp;
        <asp:Label ID="lblShiptoName" runat="server" CssClass="Field" Width="90%"></asp:Label><br />
        <br />
        <asp:Label ID="lblAddress1Title" runat="server" CssClass="FieldLabel"
            Text="Address:" Width="121px"></asp:Label><br />
        &nbsp;
        <asp:Label ID="lblAddress1" runat="server" CssClass="Field" Width="90%"></asp:Label><br />
        &nbsp;
        <asp:Label ID="lblAddress2" runat="server" CssClass="Field" Width="90%"></asp:Label><br />
        &nbsp;
        <asp:Label ID="lblAddress3" runat="server" CssClass="Field" Width="90%"></asp:Label><br />
        &nbsp;
        <asp:Label ID="lblCityStateZip" runat="server" CssClass="Field" Width="90%"></asp:Label></asp:Panel>
</div>
