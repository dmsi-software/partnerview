﻿using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using Infragistics.Web.UI.NavigationControls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class UserControls_Inventory : System.Web.UI.UserControl
{
    private readonly object _hitCounterLock = new object(); //MDM - Special object for locking that is not used anywhere else.

    public DataSet _dsReferencedCategoryDataSet = new DataSet();

    private const string NORESULTSFOUND = "Your search did not find any records. Consider refining your search criteria.";
    private const string NORESULTSFOUNDSEARCHTIPS = "Your search did not find any records. Consider refining your search criteria. For help click Search Tips.";

    public enum Data
    {
        Cached,
        Fetch,
        New,
        AdvSearch
    }

    public enum ModeType
    {
        Inventory,
        QuoteEditAddItems
    }

    public delegate void AddToCartDelegate(dsOrderCartDataSet.ttorder_cartRow row);
    public event AddToCartDelegate OnAddToCart;

    public delegate void ItemSelectionChangedDelegate();
    public event ItemSelectionChangedDelegate OnItemSelectionChanged;

    public delegate void PriceSelectionChangedDelegate();
    public event PriceSelectionChangedDelegate OnPriceSelectionChanged;

    public delegate void UserPrefModeDelegate(bool isInEditMode);
    public event UserPrefModeDelegate OnUserPrefModeChanged;

    public delegate Button FindTopText();

    private DateTime _startMethodTime;
    private int _exucutionCounter = 0;

    private bool _overrideProductGroupQuickList;
    private bool _viewInvPrices = false;
    //MDM -- price detail grid action allocation mod
    private bool _viewInvPriceDetail = false;
    //MDM -- price detail grid action allocation mod
    private bool _viewInvAvailQty = false;
    private bool _viewText = false;
    private bool _viewQuantity = false;

    private string _availableTextDisplay;
    private int _inventorySearchCounter = 0;
    Dmsi.Agility.EntryNET.DataManager dmfetch = new DataManager();
    Dmsi.Agility.EntryNET.DataManager _dm;
    ImageButton _img;

    private ModeType _Mode;
    public ModeType Mode
    {
        get { return _Mode; }
        set { _Mode = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    /// <summary>
    /// Page load event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (hdnBannerId.Value != "")
        {
            Session[this.Mode.ToString() + "ImageClicked"] = "Yes_" + hdnBannerId.Value;
            hdnBannerId.Value = "";
        }

        HiddenField h = Parent.Parent.Parent.FindControl("hdnExternalLinkClick") as HiddenField;

        if (h != null && h.Value != "")
        {
            h.Value = "";

            AnalyticsInput aInput = new AnalyticsInput();
            aInput.Type = "event";
            aInput.Action = "Products - Navigate External Link";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        _dm = new DataManager();

        string strUserAgent = Request.UserAgent.ToString().ToLower();

        WebDataMenu1.StyleSetName = System.Configuration.ConfigurationManager.AppSettings["Theme"];

        if (System.Configuration.ConfigurationManager.AppSettings["Theme"].ToString() == "DMSiBlue")
            WebDataMenu1.BackColor = Color.FromArgb(198, 219, 247);

        dsSessionMgrDataSet dsSession = (dsSessionMgrDataSet)_dm.GetCache("dsSessionManager");
        DataRow[] rows = dsSession.ttSelectedProperty.Select("propertyName='qtyAvailableDisplay'");

        if (rows.Length == 1)
        {
            _availableTextDisplay = rows[0]["propertyValue"].ToString();
            if (_availableTextDisplay == "Text")
                _viewText = true;
            if (_availableTextDisplay == "Quantity")
                _viewQuantity = true;
        }

        this.ColumnSelectorPrice.OnOKClicked += new UserControls_ColumnSelector.OKClickedDelegate(ColumnSelectorPrice_OnOKClicked);
        this.ColumnSelectorPrice.OnResetClicked += new UserControls_ColumnSelector.ResetClickedDelegate(ColumnSelectorPrice_OnResetClicked);

        if (!Page.IsPostBack)
        {
            hypNavigateExternalSite.Attributes.Add("onclick", "javascript:return hypNavigateExternalSite_click(this.id)");

            lnkBreadCrumb.CssClass = "brd-crumbactive";

            lnkBreadCategory.CssClass = "brd-crumbaInctive";

            lnkBreadgroup.CssClass = "brd-crumbaInctive";
            lnkBreadCatValue.CssClass = "brd-crumbaInctive";
            dvImageBanners.Visible = false;

            //TODO: MDM - Use the next lines for the image viewer story, after WebImageViewer1 is added back to the UI.
            tablerowImageViewer.Visible = false;
            WebImageViewer1.StyleSetName = ConfigurationManager.AppSettings["Theme"].ToString();

            divContent.InnerHtml = ConfigurationManager.AppSettings["SearchTipsText"].ToString();
            lnkBreadCrumb.Text = "All Products";

            if ((string)Session["GenericLogin"] == "True")
            {
                btnAddToQList.Visible = false;
                btnAddAll2.Visible = false;
                btnViewQuickList.Visible = false;
                chkQuickList.Visible = false;
                btnAdvViewQuickList.Visible = false;
                this.NewQlistDiv.Visible = false;
            }

            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 0;

            int pageSize;

            this.ddlPricing.SelectedIndex = Profile.ddlInventoryPricingRows_SelectedIndex;

            if (ddlPricing.SelectedValue == "All")
                pageSize = 32000;
            else
                pageSize = Convert.ToInt32(ddlPricing.SelectedValue);

            this.gvInventoryPricing.PageSize = pageSize;

            this.ddlAvailable.SelectedIndex = Profile.ddlInventoryAvailableRows_SelectedIndex;

            if (ddlAvailable.SelectedValue == "All")
                pageSize = 32000;
            else
                pageSize = Convert.ToInt32(ddlAvailable.SelectedValue);

            this.gvInventoryAvailableBranch.PageSize = pageSize;
            this.gvInventoryAvailableTally.PageSize = pageSize;

            this.ddlOnOrder.SelectedIndex = Profile.ddlInventoryOnOrderRows_SelectedIndex;

            if (ddlOnOrder.SelectedValue == "All")
                pageSize = 32000;
            else
                pageSize = Convert.ToInt32(ddlOnOrder.SelectedValue);

            this.gvOnOrderBranch.PageSize = pageSize;
            this.gvOnOrderTran.PageSize = pageSize;

            DataMenuItem pricing = this.WebDataMenu1.Items.FindDataMenuItemByText("Pricing");
            pricing.Visible = false;
            this.PricingParentDiv.Visible = false;
            this.PricingModalDiv.Visible = false;

            DataMenuItem available = this.WebDataMenu1.Items.FindDataMenuItemByText("Available");
            available.Visible = false;
            this.AvailableParentDiv.Visible = false;

            DataMenuItem onorder = this.WebDataMenu1.Items.FindDataMenuItemByText("On Order");
            onorder.Visible = false;
            this.OnOrderParentDiv.Visible = false;

            ddlLevelAvailable.Items[1].Enabled = false;
            ddlLevelAvailable.Items[2].Enabled = false;

            ddlLevelOnOrder.Items[1].Enabled = false;
            ddlLevelOnOrder.Items[2].Enabled = false;

            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

            // When grand total only, branch column will be hidden

            if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
            {
                foreach (DataRow row in dsPV.bPV_Action.Rows)
                {
                    switch (row["token_code"].ToString())
                    {
                        case "view_inv_prices":
                            _viewInvPrices = true;
                            dm.SetCache("ViewInventoryPrices", true);
                            pricing.Visible = true;
                            this.PricingParentDiv.Visible = true;
                            this.PricingModalDiv.Visible = true;
                            break;

                        case "view_inv_price_detail":
                            dm.SetCache("ViewInventoryPriceDetail", true);
                            _viewInvPriceDetail = true;
                            break;

                        case "view_avail_inv":
                            this.AvailableParentDiv.Visible = true;
                            _viewInvAvailQty = true;
                            Session["Show_Available"] = true;
                            available.Visible = true;
                            break;

                        case "view_avail_inv_by_branch":
                            this.ddlLevelAvailable.Items[1].Enabled = true;
                            Session["Show_Available_By_Branch"] = true;
                            break;

                        case "view_avail_inv_tallies":
                            this.ddlLevelAvailable.Items[2].Enabled = true;
                            Session["Show_Available_Detail"] = true;
                            break;

                        case "view_on_order":
                            this.OnOrderParentDiv.Visible = true;
                            Session["Show_OnOrder"] = true;
                            onorder.Visible = true;
                            break;

                        case "view_on_order_inv_by_branch":
                            this.ddlLevelOnOrder.Items[1].Enabled = true;
                            Session["Show_OnOrder_By_Branch"] = true;
                            break;

                        case "view_on_order_inv_trans":
                            this.ddlLevelOnOrder.Items[2].Enabled = true;
                            Session["Show_OnOrder_Detail"] = true;
                            break;

                        case "view_avail_inv_locations":
                            foreach (DataControlField col in gvInventoryAvailableTally.Columns)
                            {
                                if (col.HeaderText == "Location")
                                {
                                    col.Visible = true;
                                    break;
                                }
                            }
                            break;

                        case "view_avail_inv_lots":
                            foreach (DataControlField col in gvInventoryAvailableTally.Columns)
                            {
                                if (col.HeaderText == "Lot")
                                {
                                    col.Visible = true;
                                    break;
                                }
                            }
                            break;
                    }
                }

                //MDM -- If no action allocation to view prices, then turn off the visibility of the lblCartTotal up a few layers in the master page.
                if (_viewInvPrices == false)
                {
                    Control parentTemplate = this.Parent.Parent.Parent.Parent.Parent.Parent.TemplateControl;
                    if (parentTemplate != null)
                    {
                        Label l = (Label)parentTemplate.FindControl("lblCartTotal");
                        if (l != null)
                            l.Visible = false;
                    }
                }

                //MDM -- price detail grid action allocation mod
                if (dm.GetCache("ViewInventoryPrices") != null && (bool)dm.GetCache("ViewInventoryPrices"))
                {
                    if (dm.GetCache("ViewInventoryPriceDetail") != null && (bool)dm.GetCache("ViewInventoryPriceDetail"))
                    {
                        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
                        {
                            pricing.Visible = false;
                            this.PricingParentDiv.Visible = false;
                            this.PricingModalDiv.Visible = false;
                        }
                        else
                        {
                            pricing.Visible = true;
                            this.PricingParentDiv.Visible = true;
                            this.PricingModalDiv.Visible = true;
                        }
                    }
                    else
                    {
                        pricing.Visible = false;
                        this.PricingParentDiv.Visible = false;
                        this.PricingModalDiv.Visible = false;
                    }
                }
                //MDM -- price detail grid action allocation mod
            }

            this.ddlHeader.SelectedIndex = Profile.ddlInventoryHeaderRows_SelectedIndex;
            dm.SetCache(this.Mode.ToString() + "OrderFormAllowed", false);
            this.SetDefaultColumnVisibility(true);
            InitializeColumnVisibility(gvInventoryPricing, "gvInventoryPricing_Columns_Visible");

            if (dsPV.bPV_userMenus != null && dsPV.bPV_userMenus.Rows.Count > 0)
            {
                foreach (DataRow row in dsPV.bPV_userMenus.Rows)
                {
                    if (row["menu_item_description"].ToString() == "Shopping Cart")
                    {
                        dm.SetCache(this.Mode.ToString() + "OrderFormAllowed", true);
                        this.SetDefaultColumnVisibility(false);
                    }
                }
            }

            //Turn off order capability if Ship-to is non-saleable...
            if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
            {
                btnAddAll2.Visible = false;
                pricing = this.WebDataMenu1.Items.FindDataMenuItemByText("Pricing");
                pricing.Visible = false;
                this.PricingParentDiv.Visible = false;
                this.PricingModalDiv.Visible = false;
            }

            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && _viewInvPrices)
            {
                btnAddAll2.Visible = true;
                pricing = this.WebDataMenu1.Items.FindDataMenuItemByText("Pricing");
                pricing.Visible = true;
                this.PricingParentDiv.Visible = true;
                this.PricingModalDiv.Visible = true;
            }

            //prior login
            if (Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] != null)
            {
                //when data is searched by global or normal search
                if ((int)Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] == 2)
                {
                    Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                    string SearchName = (string)Session[this.Mode.ToString() + "priorLoginSearchValue"];
                    FinDetails(SearchName);
                    dvImageBanners.Visible = false;
                    dvProdListView.Visible = true;
                    Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = null;

                }
                //if check box items are checked in prior loging 
                else if ((int)Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] == 1)
                {
                    Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                    Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet dsSelectedAttribs = new Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet();
                    dsSelectedAttribs = (Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet)Session[this.Mode.ToString() + "dsSelectedAttribsPriorLogin"];
                    string Group = (string)Session[this.Mode.ToString() + "PriorLogin_Group"];
                    string BreadCrumbGroupDetails = (string)Session[this.Mode.ToString() + "PriorLoginBreadCrumb_GroupDetails"];
                    DataSet dsProducts = new DataSet();

                    if (Group != string.Empty)
                    {
                        dsProducts = GetfetchMultipleItems(Group, dsSelectedAttribs);
                    }

                    if (dsProducts != null)
                    {
                        if (dsProducts.Tables.Count > 0)
                        {
                            if (dsProducts.Tables[0].Rows.Count > 0)
                            {
                                BindProductGridViewDetails(dsProducts.Tables[0], 1);
                            }
                            else
                            {
                                GetDetails();

#if DEBUG
        TimeSpan elapsedtimeReturn = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeReturn, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
                                return;
                            }
                        }
                        else
                        {
                            dsProducts = GetfetchMultipleItems(Group, dsSelectedAttribs);
                            BindProductGridViewDetails(dsProducts.Tables[0], 1);
                        }
                    }
                    else
                    {
                        dsProducts = GetfetchMultipleItems(Group, dsSelectedAttribs);
                        BindProductGridViewDetails(dsProducts.Tables[0], 1);
                    }

                    SelectedNodeBind(BreadCrumbGroupDetails, "");

                    string[] SplitBindedValue;

                    SplitBindedValue = BreadCrumbGroupDetails.Split('_');
                    if (SplitBindedValue.Length > 0)
                    {
                        string NodeType = SplitBindedValue[0].ToString();
                        //based on selected node redirects it to respective method

                        //if this method is fired then breadcrumbs with prod view grid needs to be displayed
                        if (NodeType == "AttributeVal")
                        {
                            Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

                            LoadAttributesDetails(SplitBindedValue[1], SplitBindedValue[2], "", SplitBindedValue[3], SplitBindedValue[4]);
                        }
                    }

                    dvImageBanners.Visible = false;
                    dvProdListView.Visible = true;
                    Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = null;
                }
                else if ((int)Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] == 3)
                {
                    Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                    string SelectedNodeGroup;
                    SelectedNodeGroup = (string)Session[this.Mode.ToString() + "priorLoginSelectedNodeCatGroup"];

                    SelectedNodeBind(SelectedNodeGroup, "");
                    Session[this.Mode.ToString() + "priorLoginSelectedNodeCatGroup"] = null;
                }
                else if ((int)Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] == 4)
                {
                    Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                    string SelectedImg_Id = (string)Session[this.Mode.ToString() + "priorLoginSelectedImageDetails"];
                    SelectedImageDetails(SelectedImg_Id);

                    Session[this.Mode.ToString() + "priorLoginSelectedImageDetails"] = null;
                }
                else
                {
                    GetDetails();
                    Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = null;
                }
            }
            else
            {
                GetDetails();
            }

            // to make the previous selected bannerimage url as null
            Session[this.Mode.ToString() + "RetainPreviousUrl"] = null;
            btnAddAll2.OnClientClick += "javascript:return GetControlIds(this.id)";
            btnViewQuickList.OnClientClick += "javascript:return GetControlIds(this.id)";

            ddlQuicklistType.Items.Clear();

            if (Session["currentUserType"] != null && ((string)Session["currentUserType"]).ToLower() == "guest")
            {
                ddlQuicklistType.Items.Add(new ListItem("My quicklist", "User"));
                ddlQuicklistType.Items.Add(new ListItem("Top items by hits", "CustomerHit"));
            }

            if (Session["currentUserType"] != null && ((string)Session["currentUserType"]).ToLower() == "internal")
            {
                ddlQuicklistType.Items.Add(new ListItem("My quicklist", "User"));
                ddlQuicklistType.Items.Add(new ListItem("Top items by hits", "CustomerHit"));
                ddlQuicklistType.Items.Add(new ListItem("Top items by total sales", "CustomerSales"));
                ddlQuicklistType.Items.Add(new ListItem("Top items by GM%", "CustomerGM"));
            }

            ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
            if (pc.ddlQuicklistType_SelectedIndex > ddlQuicklistType.Items.Count)
                ddlQuicklistType.SelectedIndex = 0;
            else
                ddlQuicklistType.SelectedIndex = pc.ddlQuicklistType_SelectedIndex;
        } // end if (!Page.IsPostBack)
        else
        {
            //for  setting cache
            _viewInvPrices = Session["ViewInventoryPrices"] != null ? (bool)Session["ViewInventoryPrices"] : false;
            _viewInvAvailQty = Session["Show_Available"] != null ? (bool)Session["Show_Available"] : false;

            //MDM -- price detail grid action allocation mod
            _viewInvPriceDetail = Session["ViewInventoryPriceDetail"] != null ? (bool)Session["ViewInventoryPriceDetail"] : false;
            //MDM -- price detail grid action allocation mod

            DataMenuItem pricing = this.WebDataMenu1.Items.FindDataMenuItemByText("Pricing");
            //MDM -- price detail grid action allocation mod
            if (dm.GetCache("ViewInventoryPrices") != null && (bool)dm.GetCache("ViewInventoryPrices"))
            {
                if (dm.GetCache("ViewInventoryPriceDetail") != null && (bool)dm.GetCache("ViewInventoryPriceDetail"))
                {
                    if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
                    {
                        pricing.Visible = false;
                        this.PricingParentDiv.Visible = false;
                        this.PricingModalDiv.Visible = false;
                    }
                    else
                    {
                        pricing.Visible = true;
                        this.PricingParentDiv.Visible = true;
                        this.PricingModalDiv.Visible = true;
                    }
                }
                else
                {
                    pricing.Visible = false;
                    this.PricingParentDiv.Visible = false;
                    this.PricingModalDiv.Visible = false;
                }
            }

            if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] != 6)
            {
                if (Mode == ModeType.Inventory)
                    dvImageBanners.Visible = true;

                if (Mode == ModeType.QuoteEditAddItems)
                    dvImageBanners.Visible = false;

                string sImageButtonClicked = (string)Session[Mode.ToString() + "ImageClicked"];

                string ctrlname = Request.Params.Get("__EVENTTARGET");
                Control control = null;
                DataTable dtGroupDetails = new DataTable();
                dtGroupDetails = (DataTable)Session[this.Mode.ToString() + "RetainFetchGroupDetails"];

                if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 1 || (int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 2 || (int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 3 || ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 4) || ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 5))
                {
                    if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 1)
                    {
                        control = this.btnReturnToItem;
                        string[] Tabname;
                        string ControlName = string.Empty;

                        if (!string.IsNullOrEmpty(ctrlname))
                        {
                            Tabname = ctrlname.Split('_');
                            if (Tabname.Length > 0)
                            {
                                ControlName = Tabname[Tabname.Length - 1];

                                if (ControlName.ToLower() == "igtree")
                                    Session[this.Mode.ToString() + "SearchContentIsHome"] = null;
                            }
                        }

                        if ((ControlName == "WebDataMenu1"))
                        {
                            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                        }
                    }
                    else if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 2)
                    {
                        this.trSearch.Visible = true;
                        this.trSearch1.Visible = true;
                        trDetails.Visible = false;
                        dvProdListView.Visible = true;
                        dvImageBanners.Visible = false;
                        control = this.btnAddAll2;
                        Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 0;
                    }
                    else if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 3)
                    {
                        this.trSearch.Visible = true;
                        this.trSearch1.Visible = true;
                        trDetails.Visible = false;
                        dvProdListView.Visible = true;
                        dvImageBanners.Visible = false;
                        control = this.btnViewQuickList;
                        Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 0;
                    }
                    else if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 4)
                    {
                        this.trSearch.Visible = true;
                        this.trSearch1.Visible = true;

                        if (igTree.CheckedNodes.Count > 0)
                        {
                            dvProdListView.Visible = true;
                        }
                        else
                        {
                            dvProdListView.Visible = false;
                            dvImageBanners.Visible = true;
                        }
                    }
                    else if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 5)
                    {
                        Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                    }
                }

                if (ctrlname != null && ctrlname != String.Empty)
                {
                    control = Page.FindControl(ctrlname);
                }

                //if the branch ischanged
                if ((ctrlname == "ctl00$cphMaster1$MainTabControl1$CustomerTabPanel1$gvShiptos") || (ctrlname == "ctl00$cphMaster1$MainTabControl1$CustomerTabPanel1$gvCustomers"))
                {
                    Session[this.Mode.ToString() + "RetainFetchGroupDetails"] = null;

                    lnkBreadCategory.CommandArgument = "";
                    lnkBreadgroup.CommandArgument = "";
                    lnkBreadCategory.Text = "";
                    lnkBreadCategory.Text = "";
                    igTree.Nodes.Clear();
                    DataTable dtempty = new DataTable();
                    dtempty = BindEmptyData();
                    lvProducts.DataSource = dtempty;
                    lvProducts.DataBind();
                    lvNoImagesProductsList.DataSource = dtempty;
                    lvNoImagesProductsList.DataBind();
                    lblBreadCatSep.Visible = false;
                }

                if ((!string.IsNullOrEmpty(sImageButtonClicked)))
                {
                    Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

                    SelectedImageDetails(sImageButtonClicked);
                }
                //while click on different menu tab (i.e products)
                else if ((string.IsNullOrEmpty(sImageButtonClicked)))
                {
                    if (control != null)
                    {
                        //if control click is either BreadcrumbCategoery or BreadcrumbGroup then it should not enter into this
                        if (control.ID != null)
                        {
                            int RestrictGetDetails = 0;
                            string clientpage = control.ClientID;
                            string[] splitclientpage;
                            string[] splitclientpageDetails;
                            splitclientpage = clientpage.Split('_');
                            splitclientpageDetails = clientpage.Split('&');

                            if (splitclientpage.Length > 2)
                            {
                                string splitclientpageVal = splitclientpage[splitclientpage.Length - 2];
                                if (splitclientpageVal == "SalesOrders1" || splitclientpageVal == "CreditMemos1" || splitclientpageVal == "Quotes1" || splitclientpageVal == "QuoteRelease" || splitclientpageVal == "QuoteEdit" || splitclientpageVal == "Invoices1" || splitclientpageVal == "OrderTab1")
                                {
                                    RestrictGetDetails = 1;
                                }
                            }

                            if ((control.ID != "hypNetRetail") &&
                                (control.ID != "lnkBreadCategory") &&
                                (control.ID != "lnkBreadgroup") &&
                                (control.ID != "btnReturnToItem") &&
                                (control.ID != "btnAddAll2") &&
                                (control.ID != "lbtnQuicklist") &&
                                (control.ID != "lnkBreadCatValue") &&
                                (control.ID != "lbtProdDesc") &&
                                (control.ID != "TabContainer1") &&
                                (control.ID != "btnContinueShopping2") &&
                                (control.ID != "btnContinueShopping") &&
                                (control.ID != "lblCart") &&
                                (control.ID != "ddlHeader") &&
                                (control.ID != "lnknumber") &&
                                (control.ID != "btnAdvViewQuickList") &&
                                (control.ID != "btnViewQuickList") &&
                                (control.ID != "ddlProductSorting") &&
                                (control.ID != "gvQuoteDetail") &&
                                (control.ID != "gvByItem") &&
                                (control.ID != "gvQuoteHeaders") &&
                                (control.ID != "gvCreditMemoHeaders") &&
                                (control.ID != "gvCreditMemoDetail") &&
                                (control.ID != "gvSalesOrderDetailInner") &&
                                (control.ID != "gvSalesOrderHeaders") &&
                                (control.ID != "gvOrderCart") &&
                                (control.ID != "linkbutton_Configure") &&
                                (control.ID != "gvShiptos") &&
                                (control.ID != "gvCustomers") &&
                                (control.ID != "lnkBreadCrumb") &&
                                (control.ID != "btnCartCheckout2") &&
                                (control.ID != "btnSubmitOrder") &&
                                (control.ID != "btnCartCheckout") &&
                                (control.ID != "ddlOrderType") &&
                                (control.ID != "btnDeleteAll") &&
                                (control.ID != "ddlOrderHeaderRows") &&
                                (control.ID != "txtRequestedDate") &&
                                (control.ID != "ddlShipVia") &&
                                (control.ID != "cbxShipComplete") &&
                                (control.ID != "txtFax") &&
                                (control.ID != "btnEditCart2") &&
                                (control.ID != "btnEditCart") &&
                                (control.ID != "btnSubmitOrder2") &&
                                (RestrictGetDetails != 1) &&
                                (control.ID != "ddlLevelAvailable") &&
                                (control.ID != "ddlLevelOnOrder") &&
                                (control.ID != "btnAddXref") &&
                                (control.ID != "btnSaveXref") &&
                                (control.ID != "Button1") &&
                                (control.ID != "ddlPricing") &&
                                (control.ID != "ddlUOM") &&
                                (control.ID != "ddlUOM2") &&
                                (control.ID != "lnkClearAll") &&
                                (control.ID != "lnkClearAll2") &&
                                (control.ID != "lnkExtDesc"))
                            {
                                GetDetails();
                            }

                            if (control.ID == "ddlBranch")
                            {
                                Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 0;
                                txtKeywordSearchValue.Text = "";
                                TabContainer1.ActiveTab = tbSearch;
                                lvProducts.Items.Clear();
                                lvProducts.DataBind();
                                lvNoImagesProductsList.Items.Clear();
                                lvNoImagesProductsList.DataBind();
                                lblBreadGroupSep.Visible = false;
                                lnkBreadgroup.Text = "";
                                lnkBreadgroup.Visible = false;
                                lblBreadCatSep.Visible = false;
                                lnkBreadCategory.Text = "";
                                lnkBreadCategory.Visible = false;

                                Session[this.Mode.ToString() + "RetainFetchGroupDetails"] = null;
                            }

                            if (control.ID.ToString() == "lbtnQuicklist")
                            {
                                this.trSearch.Visible = true;
                                this.trSearch1.Visible = true;
                                trDetails.Visible = false;
                                dvProdListView.Visible = true;
                                dvImageBanners.Visible = false;
                            }

                            if (control.ID == "btnContinueShopping" || control.ID == "btnContinueShopping2")
                            {
                                if ((igTree.CheckedNodes.Count == 0) && (lvProducts.Items.Count == 0) && (lvNoImagesProductsList.Items.Count == 0) && (txtKeywordSearchValue.Text == "") && (txtInventorySearchValue.Text == ""))
                                {
                                    RetainSelectedDetails();
                                    dvImageBanners.Visible = true;
                                    dvProdListView.Visible = false;
                                    plAddImages.Visible = true;
                                }
                                else if ((lvProducts.Items.Count == 0) && (lvNoImagesProductsList.Items.Count == 0))
                                {
                                    RetainSelectedDetails();
                                }
                                else if ((lvProducts.Items.Count > 0) || (lvNoImagesProductsList.Items.Count > 0) || (igTree.CheckedNodes.Count > 0) || (txtKeywordSearchValue.Text != "") || (txtInventorySearchValue.Text != ""))
                                {
                                    RetainSelectedDetails();
                                }
                            }

                            if (control.ID == "btnReturnToItem")
                            {
                                RetainSelectedDetails();
                            }

                            if (control.ID == "linkbutton_Configure")
                            {
                                if ((lvProducts.Items.Count > 0) || (lvNoImagesProductsList.Items.Count > 0))
                                {
                                    dvImageBanners.Visible = false;
                                    dvProdListView.Visible = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        string[] Tabname;
                        string ControlName = string.Empty;

                        if (!string.IsNullOrEmpty(ctrlname))
                        {
                            Tabname = ctrlname.Split('_');

                            if (Tabname.Length > 0)
                            {
                                ControlName = Tabname[Tabname.Length - 1];
                            }
                        }

                        if ((ControlName != "txtProductSearchKeyword") && (ControlName != ",") && (ControlName == "WebDataMenu1"))
                        {
                            //if diffrerent menu tabs are clicked Products tab details should not get changed

                            Control ctl = new Control();

                            if (Mode == ModeType.Inventory)
                            {
                                ctl = this.Parent;
                            }
                            else if (Mode == ModeType.QuoteEditAddItems)
                            {
                                ctl = this.Parent.Parent;
                            }

                            Infragistics.Web.UI.NavigationControls.WebDataMenu wbDatamenu = new Infragistics.Web.UI.NavigationControls.WebDataMenu();

                            wbDatamenu = (Infragistics.Web.UI.NavigationControls.WebDataMenu)ctl.FindControl("WebDataMenu1");
                            string SelMenu = "Products";

                            for (int imenucount = 0; imenucount <= wbDatamenu.Items.Count - 1; imenucount++)
                            {
                                if (wbDatamenu.Items[imenucount].Selected == true)
                                {
                                    SelMenu = wbDatamenu.Items[imenucount].Text;
                                }
                            }

                            if (Session[this.Mode.ToString() + "ActiveProductsPage"] != null)
                            {
                                if ((SelMenu != "Products") && ((int)Session[this.Mode.ToString() + "ActiveProductsPage"] == 1))
                                {
                                    Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                                }
                            }

                            if (((ControlName == "WebDataMenu1") && ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 1)))
                            {
                                Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

                                if (Session[this.Mode.ToString() + "SearchContentIsHome"] == null)
                                    RetainSelectedDetails();
                            }
                            else
                            {
                                GetDetails();
                            }
                        }
                        else
                        {
                            //if user preferences are changed then
                            if ((ControlName != "txtProductSearchKeyword") && (ControlName != ",") && (ControlName != "igTree"))
                            {
                                if (Session[this.Mode.ToString() + "btnReturnToItem_Click"] != null)
                                {
                                    //if cancel button is clicked then value is 6 if no list items in grid then value is 9
                                    if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 9)
                                    {
                                        GetPriceChangeRelatedDetails();
                                    }
                                    else if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 6)
                                    {
                                        //RetainSelectedDetails();
                                        Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                                    }
                                    else if ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 7)
                                    {
                                        if (igTree.CheckedNodes.Count > 0)
                                        {
                                            RetainPriceChange();
                                            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                                        }
                                        else
                                        {
                                            Control controldetails = null;

                                            if (Mode == ModeType.Inventory)
                                                controldetails = this.Parent;

                                            if (Mode == ModeType.QuoteEditAddItems)
                                                controldetails = this.Parent.Parent;

                                            WebDataMenu wbDatamenu = controldetails.FindControl("WebDataMenu1") as WebDataMenu;
                                            string SelMenu = "Home1";
                                            for (int menucount = 0; menucount <= wbDatamenu.Items.Count - 1; menucount++)
                                            {
                                                if (wbDatamenu.Items[menucount].Selected == true)
                                                {
                                                    SelMenu = wbDatamenu.Items[menucount].Text;
                                                }
                                            }

                                            if (txtKeywordSearchValue.Text != string.Empty)
                                            {
                                                FinDetails(txtKeywordSearchValue.Text);
                                            }
                                            else
                                            {
                                                if (Session[this.Mode.ToString() + "btnViewQuickList_Click"] == null)
                                                {
                                                    Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;
                                                }

                                                if ((int)Session[this.Mode.ToString() + "btnViewQuickList_Click"] == 1)
                                                {

                                                    CascadingDropDown1.SelectedValue = ":::";
                                                    // Global Variable to override dropdownlist value that is
                                                    // not updated until client html is refreshed
                                                    _overrideProductGroupQuickList = true;
                                                    cbxOnly.Checked = false;

                                                    bool isQuicklist = chkQuickList.Checked;
                                                    chkQuickList.Checked = true;

                                                    LoadQuickListDetails();
                                                    BtnFindDetails();

                                                    _overrideProductGroupQuickList = false;
                                                    chkQuickList.Checked = isQuicklist;

                                                    Session[this.Mode.ToString() + "LastSearchExecuted"] = "QuickList";
                                                }
                                                else
                                                {
                                                    if (lvProducts.Items.Count > 0 || lvNoImagesProductsList.Items.Count > 0)
                                                    {
                                                        FinDetails("");
                                                    }
                                                }
                                            }

                                            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                                        }
                                    }
                                }
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }
            else
            {
                Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
            }
        }

        if (strUserAgent != null)
        {
            if (Request.Browser.IsMobileDevice == true ||
                strUserAgent.Contains("iphone") ||
                strUserAgent.Contains("blackberry") ||
                strUserAgent.Contains("mobile") ||
                strUserAgent.Contains("windows ce") ||
                strUserAgent.Contains("android"))
            {
                txtDetailItemQty.FocusOnInitialization = false;
            }

            if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iPad") ||
                 strUserAgent.Contains("ipod"))
            {
                txtDetailItemQty.FocusOnInitialization = false;
            }
        }

        if (!IsPostBack)
        {
            RegenerateProductXMLFile();
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (Session["DoNotRunPreRender"] != null && (bool)Session["DoNotRunPreRender"] == true)
        {
            Session["DoNotRunPreRender"] = null;
        }
        else
        {
            if (Session["DisplayCustItemQlist"] != null && ((string)Session["DisplayCustItemQlist"]).ToLower().Trim() == "yes")
            {
                ddlQuicklistType.Enabled = true;
                this.NewQlistDiv.Visible = true;
                this.OriginalQlistDiv.Visible = false;
            }

            if (Session["DisplayCustItemQlist"] != null && ((string)Session["DisplayCustItemQlist"]).ToLower().Trim() == "no")
            {
                ddlQuicklistType.SelectedIndex = 0;
                ddlQuicklistType.Enabled = false;
                this.NewQlistDiv.Visible = false;
                this.OriginalQlistDiv.Visible = true;
            }

            if (Session["singleview"] == null)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "PrRespFormat();", true);
            }
            else
            {
                if ((string)Session["singleview"] == "true")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormatSingle();", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "PrRespFormat();", true);
                }
            }

            DataView dv = new DataView();

            if (Session[this.Mode.ToString() + "CachedImageListDataView"] != null)
            {
                dv = (DataView)Session[this.Mode.ToString() + "CachedImageListDataView"];
                WebImageViewer1.DataSource = dv;
                WebImageViewer1.ImageItemBinding.ImageUrlField = "image_file";
                WebImageViewer1.ImageItemBinding.KeyField = "image_file";
            }

            ColumnSelectorPrice.GridView = gvInventoryPricing;

            if (AvailableParentDiv.Visible == true || OnOrderParentDiv.Visible == true || tableXrefs.Visible == true || tableDocuments.Visible == true)
                PricingParentDiv.Visible = false;

            DataMenuItem available = this.WebDataMenu1.Items.FindDataMenuItemByText("Available");
            available.Visible = false;

            DataMenuItem onorder = this.WebDataMenu1.Items.FindDataMenuItemByText("On Order");
            onorder.Visible = false;

            DataMenuItem pricing = WebDataMenu1.Items.FindDataMenuItemByText("Pricing");
            pricing.Visible = false;

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

            bool parentPricing = false;

            if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
            {
                foreach (DataRow row in dsPV.bPV_Action.Rows)
                {
                    switch (row["token_code"].ToString())
                    {
                        case "view_inv_prices":
                            pricing.Visible = true;
                            parentPricing = true;

                            break;

                        //MDM -- price detail grid action allocation mod
                        case "view_inv_price_detail":
                            if (parentPricing)
                                pricing.Visible = true;
                            break;
                        //MDM -- price detail grid action allocation mod

                        case "view_avail_inv":
                            available.Visible = true;
                            break;

                        case "view_on_order":
                            onorder.Visible = true;
                            break;
                    }
                }
            }

            if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
            {
                parentPricing = false;
                pricing.Visible = false;
                this.PricingParentDiv.Visible = false;
                this.PricingModalDiv.Visible = false;
            }

            foreach (DataMenuItem it in WebDataMenu1.Items)
            {
                if (!it.Selected)
                    it.CssClass = "";
            }

            if (_viewInvPriceDetail == false)
            {
                pricing.Visible = false;
            }

            //MDM - Moved this code from Page_Load so it runs after the change branch event handler. That gives change branch the opportunity to reset
            //Session["SavedInventoryBranch"] so a new Product Group fetch will run.
            //Fetch Major Product Groups and bind them...
            if (Session["SavedInventoryBranch"] == null || Session["SavedProductGroupMajorGUID"] == null)
            {
                RegenerateProductXMLFile();
            }
            else
            {
                if (Session["SavedInventoryBranch"] == null || ((string)Session["SavedInventoryBranch"] != (string)dm.GetCache("currentBranchID")))
                {
                    LoadProductGroupMajors();
                }
            }

            if (Session["AllowRetailPricing"] != null && ((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if (this.Visible && this.trDetails.Visible)
                {
                    this.Page.Master.FindControl("hypUserPreferences").Visible = false;
                    this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
                    if (Session["UserAction_DisableOnScreenUserPreferences"] != null)
                    {
                        if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                        {
                            if (Mode == ModeType.Inventory)
                            {
                                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                            }

                            if (Mode == ModeType.QuoteEditAddItems)
                            {
                                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                            }
                        }
                    }
                }
            }

            //Set different button Text based on Mode
            if (Mode == ModeType.Inventory)
            {
                btnAddAll2.Text = "Add to Cart";
                btnDetailAddToCart.Text = "Add to Cart";
            }
            else if (Mode == ModeType.QuoteEditAddItems)
            {
                btnAddAll2.Text = "Add to Quote";
                btnDetailAddToCart.Text = "Add to Quote";
            }

            if (Session["NeedToClickQuoteEditAddItemsQuicklist"] != null && (bool)Session["NeedToClickQuoteEditAddItemsQuicklist"] == true)
            {
                Session["NeedToClickQuoteEditAddItemsQuicklist"] = false;
                btnViewQuickList_Click(null, null);
            }

            if (Session["NeedToClickQuoteEditAddItemsSearch"] != null && (bool)Session["NeedToClickQuoteEditAddItemsSearch"] == true)
            {
                Session["NeedToClickQuoteEditAddItemsSearch"] = false;
                btnFind_Click(null, null);
            }

            if (Session["BlockPopupMenu"] != null && (bool)Session["BlockPopupMenu"] == true)
            {
                HtmlImage img = Page.Master.FindControl("imgCustAddress") as HtmlImage;
                if (img != null)
                    img.Attributes.Remove("onmouseover");

                ImageButton imgButton = this.Parent.FindControl("imgLogo") as ImageButton;
                if (imgButton != null)
                    imgButton.Enabled = false;
            }

            if (tbSearch.Visible == true &&
                tbAdvSearch.Visible == true &&
                TabContainer1.ActiveTab.ID == "tbSearch")
            {
                if (lvProducts.Items.Count == 0 &&
                    lvNoImagesProductsList.Items.Count == 0 &&
                    Session["RunningClearSelections"] == null)
                {
                    dvImageBanners.Visible = true;
                }
            }
            else
            {
                dvImageBanners.Visible = false;
                dvProdListView.Visible = true;
            }

            DeleteEmptyTreenodes();
            string strUserAgent = Request.UserAgent.ToString().ToLower();

            if (strUserAgent != null)
            {
                if (Request.Browser.IsMobileDevice == true ||
                    strUserAgent.Contains("iphone") ||
                    strUserAgent.Contains("blackberry") ||
                    strUserAgent.Contains("mobile") ||
                    strUserAgent.Contains("windows ce") ||

                    strUserAgent.Contains("android"))
                {
                    txtDetailItemQty.FocusOnInitialization = false;
                }

                if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iPad") ||
                     strUserAgent.Contains("ipod"))
                {
                    txtDetailItemQty.FocusOnInitialization = false;
                }
            }

            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "PrRespFormat();", true);

            if (Session["BranchHasChanged"] != null && (bool)Session["BranchHasChanged"] == true)
            {
                Session["BranchHasChanged"] = null;

                if (tbSearch.Visible)
                {
                    Div3.Visible = true;
                    plAddImages.Visible = true;
                }
            }

            if (Session["ItemAndAddToQlistValues"] != null)
            {
                string[] strValues = ((string)Session["ItemAndAddToQlistValues"]).Split(new string[] { "|" }, 2, StringSplitOptions.None);
                string itemnumber = strValues[0];
                string btnQlistText = strValues[1];

                if (lvNoImagesProductsList.Items.Count > 0 && lvNoImagesProductsList.Visible == true)
                {
                    for (int x = 0; x < lvNoImagesProductsList.Items.Count; x++)
                    {
                        string partNo = (lvNoImagesProductsList.Items[x].FindControl("lblPartNo") as Label).Text.Replace("Part # ", "");

                        if (partNo == itemnumber)
                        {
                            (lvNoImagesProductsList.Items[x].FindControl("lbtnQuicklist") as LinkButton).Text = btnQlistText;
                            break;
                        }
                    }
                }

                if (lvProducts.Items.Count > 0 && lvProducts.Visible == true)
                {
                    for (int x = 0; x < lvProducts.Items.Count; x++)
                    {
                        string partNo = (lvProducts.Items[x].FindControl("lblPartNo") as Label).Text.Replace("Part # ", "");

                        if (partNo == itemnumber)
                        {
                            (lvProducts.Items[x].FindControl("lbtnQuicklist") as LinkButton).Text = btnQlistText;
                            break;
                        }
                    }
                }

                Session["ItemAndAddToQlistValues"] = null; //null it out before leaving
            }

            if (!(lblDetailMinPack.Text.Length == 0 || lblDetailMinPack.Text == "&nbsp;<br/>&nbsp;") && _viewInvAvailQty)
            {
                divDetailMinPack.Visible = true;
                lblDetailMinPack.Visible = true;
            }
            else
            {
                divDetailMinPack.Visible = false;
                lblDetailMinPack.Visible = false;
            }

            if (Session["UOMonInventoryHasJustBeenChanged"] != null && (bool)Session["UOMonInventoryHasJustBeenChanged"] == true)
            {
                this.trSearch.Visible = true;
                this.trSearch1.Visible = true;
                trDetails.Visible = false;
                dvProdListView.Visible = true;
                dvImageBanners.Visible = false;

                Session["UOMonInventoryHasJustBeenChanged"] = null;
            }

            if ((string)Session["GenericLogin"].ToString() != "True" && Session[this.Mode.ToString() + "priorLoginCheckedItems"] != null)
            {
                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = null;
            }
        }

        Session["RunningClearSelections"] = null; //MDM - Should do this last; variable is only used once each pass if Clear Selections link was clicked.

        //MDM - visibility bug workaround, July 2017
        if (Session["ExtDescCommand"] != null && (bool)Session["ExtDescCommand"] == true)
        {
            Session["ExtDescCommand"] = null;

            if (dvImageBanners.Visible == true && dvProdListView.Visible == true)
            {
                dvImageBanners.Visible = false;
            }
        }

        if (lblHeaderError.Visible == true)
            dvProdListView.Visible = true;
        else if ((lvProducts.Visible == true && lvProducts.Items.Count == 0) || (lvNoImagesProductsList.Visible == true && lvNoImagesProductsList.Items.Count == 0))
            dvProdListView.Visible = false;

        //MDM - Added for external link project
        string customerCode = ((DataRow)Session["CustomerRowForInformationPanel"])["cust_code"].ToString();
        string shiptoSeq = ((DataRow)Session["ShiptoRowForInformationPanel"])["seq_num"].ToString();

        if (Session["GenericLogin"] != null && (string)Session["GenericLogin"] == "True")
        {
            hypNavigateExternalSite.Visible = false;
        }
        else if (Session["link_external_website"] != null)
        {
            hypNavigateExternalSite.Visible = true;

            hypNavigateExternalSite.Text = (string)Session["external_link"];
            string externalURL = (string)Session["external_url"];

            if (externalURL.EndsWith("/"))
                externalURL = externalURL.Substring(0, externalURL.Length - 1);

            if (externalURL.EndsWith(@"\"))
                externalURL = externalURL.Substring(0, externalURL.Length - 1);

            if (externalURL.EndsWith("?"))
                externalURL = externalURL.Substring(0, externalURL.Length - 1);

            if (Session["external_web_ticket"] != null && ((string)Session["external_web_ticket"]).ToLower() == "yes")
            {
                if (Session["WeSucceedUserID"] == null)
                {
                    //Make WCF call to WeSucceed API for first userid
                    string userid = GetWeSucceedUserID();

                    Session["WeSucceedUserID"] = userid;
                }
            }

            if (Session["external_web_ticket"] != null && ((string)Session["external_web_ticket"]).ToLower() == "yes")
            {
                if (Session["TicketUsed"] != null && (bool)Session["TicketUsed"] == true)
                {
                    //Make WCF call to WeSucceed API for new userid after one has been used
                    string userid = GetWeSucceedUserID();

                    Session["WeSucceedUserID"] = userid;
                    Session["TicketUsed"] = false;
                }
            }

            hypNavigateExternalSite.NavigateUrl = externalURL + "?" + HttpUtility.UrlEncode("CompanyDomain=" + Parent.Page.Request.Url.Host + "&Branch=" + (string)Session["currentBranchID"] + "&CustCode=" + customerCode + "&ShiptoSeq=" + shiptoSeq);

            if (Session["external_web_ticket"] != null && ((string)Session["external_web_ticket"]).ToLower() == "yes" && (string)Session["WeSucceedUserID"] != "0")
                hypNavigateExternalSite.NavigateUrl += "&UserID=" + (string)Session["WeSucceedUserID"];
        }
        else
            hypNavigateExternalSite.Visible = false;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// getting the default search details for normal search tab
    /// </summary>
    protected void GetDetails()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        int ReturnCode = 0;
        string MessageText = "";

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        if (Session["RecallFetchAttributes"] != null)
        {
            if ((int)Session["RecallFetchAttributes"] == 1)
            {
                dmfetch = new DataManager();
                dmfetch.SetCache("FetchBannerDetails", null);
                dmfetch.SetCache("FetchCatDetails", null);
                Session["RecallFetchAttributes"] = null;
                Session[this.Mode.ToString() + "RetainFetchGroupDetails"] = null;
            }
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dmfetch.GetCache("FetchCatDetails") == null)
        {
            inventory.FetchHomeCategories(out ReturnCode, out MessageText);

            dmfetch.SetCache("FetchBannerDetails", inventory.ReferencedBannerDataSet);
            dmfetch.SetCache("FetchCatDetails", inventory.ReferencedCategoryDataSet);
        }

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        lnkBreadgroup.Visible = false;
        lblBreadGroupSep.Visible = false;
        lnkBreadCatValue.Visible = false;

        DataTable dtGroupDetails = new DataTable();

        dtGroupDetails = (DataTable)Session[this.Mode.ToString() + "RetainFetchGroupDetails"];

        if (dtGroupDetails != null)
        {
            if (dtGroupDetails.Rows.Count > 0)
            {
                GetRetainDetails();

#if DEBUG
        TimeSpan elapsedtimeInner = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeInner, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

                return;
            }
        }

        DataSet dsReferencedBannerDataSet = new DataSet();
        DataSet dsFetchItemByAttribute = new DataSet();
        DataSet dsFetchgroups = new DataSet();
        DataSet dsAttributesdataset = new DataSet();
        DataSet dsbannerDataset = new DataSet();
        DataTable dtRetainFetchGroups = new DataTable();
        dtRetainFetchGroups.Columns.Add("CategoryGuid");
        dtRetainFetchGroups.Columns.Add("CategoryDescription");
        dtRetainFetchGroups.Columns.Add("GroupDescription");
        dtRetainFetchGroups.Columns.Add("GroupGuid");

        if (dmfetch.GetCache("FetchCatDetails") == null)
        {
            _dsReferencedCategoryDataSet = inventory.ReferencedCategoryDataSet;
        }
        else
        {
            _dsReferencedCategoryDataSet = (DataSet)(dmfetch.GetCache("FetchCatDetails"));
        }

        Session[this.Mode.ToString() + "dsReferencedCategoryDataSet"] = _dsReferencedCategoryDataSet;

        if (dmfetch.GetCache("FetchBannerDetails") == null)
        {
            dsReferencedBannerDataSet = inventory.ReferencedBannerDataSet;
        }
        else
        {
            dsReferencedBannerDataSet = (DataSet)(dmfetch.GetCache("FetchBannerDetails"));
        }

        RenderImageButtons(dsReferencedBannerDataSet, 0, inventory.ReferencedDataSet);

        if (igTree.Nodes.Count > 0)
        {
            igTree.Nodes.Clear();
        }

        if (_dsReferencedCategoryDataSet.Tables.Count > 0)
        {
            if (_dsReferencedCategoryDataSet.Tables[0].Rows.Count > 0)
            {
                int CatCount = _dsReferencedCategoryDataSet.Tables[0].Rows.Count;
                for (int catogeries = 0; catogeries <= _dsReferencedCategoryDataSet.Tables[0].Rows.Count - 1; catogeries++)
                {
                    ReturnCode = 0;
                    MessageText = "";
                    inventory.FetchGroups(_dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString(), out ReturnCode, out MessageText);

                    if (ReturnCode != 0)
                    {
                        if ((string)Session["GenericLogin"] == "True")
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Default.aspx");
                        }
                        else
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Logon.aspx");
                        }
                    }

                    dsFetchgroups = inventory.ReferencedGroupDataSet;
                    dsReferencedBannerDataSet = inventory.ReferencedBannerDataSet;

                    TreeNode type;
                    DataTreeNode dType = new DataTreeNode();
                    type = new TreeNode();
                    string CatName = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    string HomeIndicator = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["HomeIndicator"].ToString();
                    type.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    type.ToolTip = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    type.Value = "Catid_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString();

                    if (_dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Length > 26)
                    {
                        dType.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Substring(0, 26);
                    }
                    else
                    {
                        dType.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    }

                    dType.ToolTip = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    dType.Value = "Catid_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString();

                    if (dsFetchgroups.Tables.Count > 0)
                    {
                        if (dsFetchgroups.Tables[0].Rows.Count > 0)
                        {
                            for (int ChildGroup = 0; ChildGroup <= dsFetchgroups.Tables[0].Rows.Count - 1; ChildGroup++)
                            {
                                DataRow drRetain;
                                drRetain = dtRetainFetchGroups.NewRow();
                                drRetain["CategoryGuid"] = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString();
                                drRetain["CategoryDescription"] = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                                drRetain["GroupDescription"] = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                drRetain["GroupGuid"] = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupGuid"].ToString();
                                dtRetainFetchGroups.Rows.Add(drRetain);
                                TreeNode childGroup = new TreeNode();
                                DataTreeNode dchildGroup = new DataTreeNode();

                                if (dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString().Length > 21)
                                {
                                    childGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString().Substring(0, 21);
                                    dchildGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString().Substring(0, 21);
                                }
                                else
                                {
                                    childGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                    dchildGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                }

                                childGroup.ToolTip = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                childGroup.Value = "GroupId_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString() + "_" + dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupGuid"].ToString();
                                dchildGroup.ToolTip = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                dchildGroup.Value = "GroupId_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString() + "_" + dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupGuid"].ToString();

                                if (HomeIndicator.ToUpper() != "TRUE")
                                {
                                    type.ChildNodes.Add(childGroup);
                                    dType.Nodes.Add(dchildGroup);
                                }
                                else if ((CatCount.ToString() == "1") && (HomeIndicator.ToString().ToUpper() == "TRUE"))
                                {
                                    igTree.Nodes.Add(dchildGroup);
                                    igTree.NodeSettings.HoverCssClass = "treelinkHover";
                                }
                            }
                        }
                    }

                    if (HomeIndicator.ToUpper() != "TRUE")
                    {
                        igTree.Nodes.Add(dType);
                        igTree.NodeSettings.HoverCssClass = "treelinkHover";
                    }

                    igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.Off;
                    divFloatingSearch1.Visible = false;
                    divFloatingSearch2.Visible = false;
                }

                tbSearch.Visible = true;
                tblbreadCrums.Visible = true;
            }
            else
            {
                ReturnCode = 0;
                MessageText = "";
                inventory.FetchGroups("", out ReturnCode, out MessageText);

                if (ReturnCode != 0)
                {
                    if ((string)Session["GenericLogin"] == "True")
                    {
                        Session.RemoveAll();
                        Response.Redirect("~/Default.aspx");
                    }
                    else
                    {
                        Session.RemoveAll();
                        Response.Redirect("~/Logon.aspx");
                    }
                }

                dsFetchgroups = inventory.ReferencedGroupDataSet;

                if (dsFetchgroups.Tables.Count > 0)
                {
                    if (dsFetchgroups.Tables[0].Rows.Count > 0)
                    {
                        for (int ChildGroup = 0; ChildGroup <= dsFetchgroups.Tables[0].Rows.Count - 1; ChildGroup++)
                        {
                            DataTreeNode dchildGroup = new DataTreeNode();

                            dchildGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                            dchildGroup.ToolTip = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                            dchildGroup.Value = "GroupId_" + " " + "_" + dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupGuid"].ToString();
                            igTree.Nodes.Add(dchildGroup);
                        }

                        igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.Off;
                        divFloatingSearch1.Visible = false;
                        divFloatingSearch2.Visible = false;

                        tbSearch.Visible = true;
                        tblbreadCrums.Visible = true;
                    }
                    else
                    {
                        TabContainer1.ActiveTab = tbAdvSearch;
                        tbSearch.Visible = false;
                        tblbreadCrums.Visible = false;
                    }
                }
            }
        }

        if (dtRetainFetchGroups.Rows.Count > 0)
        {
            Session[this.Mode.ToString() + "RetainFetchGroupDetails"] = dtRetainFetchGroups;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void GetRetainDetails()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        int ReturnCode = 0;
        string MessageText = "";

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        if (dmfetch.GetCache("FetchCatDetails") == null)
        {
            inventory.FetchHomeCategories(out ReturnCode, out MessageText);
            dmfetch.SetCache("FetchBannerDetails", inventory.ReferencedBannerDataSet);
            dmfetch.SetCache("FetchCatDetails", inventory.ReferencedCategoryDataSet);
        }

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        lnkBreadgroup.Visible = false;
        lblBreadGroupSep.Visible = false;
        lnkBreadCatValue.Visible = false;

        DataSet dsReferencedBannerDataSet = new DataSet();
        DataSet dsFetchItemByAttribute = new DataSet();
        DataSet dsFetchgroups = new DataSet();
        DataSet dsAttributesdataset = new DataSet();
        DataSet dsbannerDataset = new DataSet();
        DataTable dtRetainFetchGroups = new DataTable();

        if (dmfetch.GetCache("FetchCatDetails") == null)
        {
            _dsReferencedCategoryDataSet = inventory.ReferencedCategoryDataSet;
        }
        else
        {
            _dsReferencedCategoryDataSet = (DataSet)(dmfetch.GetCache("FetchCatDetails"));
        }

        Session[this.Mode.ToString() + "dsReferencedCategoryDataSet"] = _dsReferencedCategoryDataSet;

        if (dmfetch.GetCache("FetchBannerDetails") == null)
        {
            dsReferencedBannerDataSet = inventory.ReferencedBannerDataSet;
        }
        else
        {
            dsReferencedBannerDataSet = (DataSet)(dmfetch.GetCache("FetchBannerDetails"));
        }

        RenderImageButtons(dsReferencedBannerDataSet, 0, inventory.ReferencedDataSet);

        if (igTree.Nodes.Count > 0)
        {
            if (Session["UOMonInventoryHasJustBeenChanged"] == null)
                igTree.Nodes.Clear();
        }

        DataTable dtGroupDetails = new DataTable();

        dtGroupDetails = (DataTable)Session[this.Mode.ToString() + "RetainFetchGroupDetails"];

        if (_dsReferencedCategoryDataSet != null)
        {
            if (_dsReferencedCategoryDataSet.Tables.Count > 0)
            {
                if (_dsReferencedCategoryDataSet.Tables[0].Rows.Count > 0)
                {
                    int CatCount = _dsReferencedCategoryDataSet.Tables[0].Rows.Count;
                    for (int catogeries = 0; catogeries <= _dsReferencedCategoryDataSet.Tables[0].Rows.Count - 1; catogeries++)
                    {
                        TreeNode type;
                        DataTreeNode dType = new DataTreeNode();
                        type = new TreeNode();
                        string CatName = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                        string HomeIndicator = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["HomeIndicator"].ToString();

                        if (_dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Length > 26)
                        {
                            type.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Substring(0, 26);
                        }
                        else
                        {
                            type.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                        }

                        type.ToolTip = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                        type.Value = "Catid_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString();

                        if (_dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Length > 26)
                        {
                            dType.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Substring(0, 26);
                        }
                        else
                        {
                            dType.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                        }

                        dType.ToolTip = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();

                        dType.Value = "Catid_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString();

                        if (dtGroupDetails != null)
                        {
                            if (dtGroupDetails.Rows.Count > 0)
                            {
                                DataView dvGroups;
                                dvGroups = dtGroupDetails.DefaultView;
                                dvGroups.RowFilter = "CategoryGuid='" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString() + "'";

                                if (dvGroups.Table.Rows.Count > 0)
                                {
                                    for (int ChildGroup = 0; ChildGroup <= dvGroups.ToTable().Rows.Count - 1; ChildGroup++)
                                    {
                                        TreeNode childGroup = new TreeNode();
                                        DataTreeNode dchildGroup = new DataTreeNode();

                                        if (dvGroups.Table.Rows[ChildGroup]["GroupDescription"].ToString().Length > 21)
                                        {
                                            childGroup.Text = dvGroups.Table.Rows[ChildGroup]["GroupDescription"].ToString().Substring(0, 21);
                                        }
                                        else
                                        {
                                            childGroup.Text = dvGroups.Table.Rows[ChildGroup]["GroupDescription"].ToString();
                                        }

                                        childGroup.ToolTip = dvGroups.Table.Rows[ChildGroup]["GroupDescription"].ToString();
                                        childGroup.Value = "GroupId_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString() + "_" + dvGroups.ToTable().Rows[ChildGroup]["GroupGuid"].ToString();

                                        if (dvGroups.ToTable().Rows[ChildGroup]["GroupDescription"].ToString().Length > 21)
                                        {
                                            dchildGroup.Text = dvGroups.ToTable().Rows[ChildGroup]["GroupDescription"].ToString().Substring(0, 21);
                                        }
                                        else
                                        {
                                            dchildGroup.Text = dvGroups.ToTable().Rows[ChildGroup]["GroupDescription"].ToString();
                                        }

                                        dchildGroup.ToolTip = dvGroups.ToTable().Rows[ChildGroup]["GroupDescription"].ToString();
                                        dchildGroup.Value = "GroupId_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString() + "_" + dvGroups.ToTable().Rows[ChildGroup]["GroupGuid"].ToString();

                                        if (HomeIndicator.ToUpper() != "TRUE")
                                        {
                                            type.ChildNodes.Add(childGroup);
                                            dType.Nodes.Add(dchildGroup);
                                        }
                                        else if ((CatCount.ToString() == "1") && (HomeIndicator.ToString().ToUpper() == "TRUE"))
                                        {
                                            igTree.Nodes.Add(dchildGroup);
                                            igTree.NodeSettings.HoverCssClass = "treelinkHover";
                                        }
                                    }
                                }
                            }
                        }

                        if (HomeIndicator.ToUpper() != "TRUE")
                        {
                            igTree.Nodes.Add(dType);
                            igTree.NodeSettings.HoverCssClass = "treelinkHover";
                        }

                        igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.Off;
                        divFloatingSearch1.Visible = false;
                        divFloatingSearch2.Visible = false;
                    }

                    tbSearch.Visible = true;
                    tblbreadCrums.Visible = true;
                }
                else
                {
                    ReturnCode = 0;
                    MessageText = "";
                    inventory.FetchGroups("", out ReturnCode, out MessageText);

                    if (ReturnCode != 0)
                    {
                        if ((string)Session["GenericLogin"] == "True")
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Default.aspx");
                        }
                        else
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Logon.aspx");
                        }
                    }

                    dsFetchgroups = inventory.ReferencedGroupDataSet;

                    if (dsFetchgroups.Tables.Count > 0)
                    {
                        if (dsFetchgroups.Tables[0].Rows.Count > 0)
                        {
                            for (int ChildGroup = 0; ChildGroup <= dsFetchgroups.Tables[0].Rows.Count - 1; ChildGroup++)
                            {
                                DataTreeNode dchildGroup = new DataTreeNode();

                                dchildGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                dchildGroup.ToolTip = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                dchildGroup.Value = "GroupId_" + " " + "_" + dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupGuid"].ToString();
                                igTree.Nodes.Add(dchildGroup);
                            }

                            igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.Off;
                            divFloatingSearch1.Visible = false;
                            divFloatingSearch2.Visible = false;

                            tbSearch.Visible = true;
                            tblbreadCrums.Visible = true;
                        }
                        else
                        {
                            TabContainer1.ActiveTab = tbAdvSearch;
                            tbSearch.Visible = false;
                            tblbreadCrums.Visible = false;
                        }
                    }
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void clearProductDetails()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        lnkBreadCategory.Text = "";
        lnkBreadCrumb.CommandArgument = "";
        lnkBreadCategory.CommandArgument = "";
        lnkBreadgroup.CommandArgument = "";
        lnkBreadCatValue.CommandArgument = "";

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void RetainSelectedDetails()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataTable dtEmpty = new DataTable();

        //if attributes are select
        if (igTree.CheckedNodes.Count > 0)
        {
            dvProdListView.Visible = true;
            dvImageBanners.Visible = false;

            if (lvPaging.Items.Count == 1)
            {
                lvPaging.Visible = false;
            }
            else if ((lvPaging.Items.Count == 0) && (lvProducts.DataSource == null) && (lvNoImagesProductsList.DataSource == null) && (lvProducts.Items.Count == 0) && (lvNoImagesProductsList.Items.Count == 0))
            {
                lvProducts.Items.Clear();
                lvProducts.DataBind();
                lvNoImagesProductsList.Items.Clear();
                lvNoImagesProductsList.DataBind();
            }
        }
        //for search and advaced search options for retaining we are using this && lnkBreadCrumb.Visible == true
        else if ((igTree.CheckedNodes.Count == 0 && lnkBreadCategory.Visible == false) && (lvNoImagesProductsList.Items.Count > 0 || lvProducts.Items.Count > 0))
        {
            dvImageBanners.Visible = false;

            if (lvPaging.Items.Count == 1)
            {
                lvPaging.Visible = false;
            }
            else if ((lvPaging.Items.Count == 0) && (lvProducts.Items.Count == 0) && (lvNoImagesProductsList.Items.Count == 0))
            {
                lvProducts.Items.Clear();
                lvProducts.DataBind();
                lvNoImagesProductsList.Items.Clear();
                lvNoImagesProductsList.DataBind();
            }
        }
        //if groups or catogeries are clicked then we are using 
        else
        {
            int SelectLinkVal = 0;

            if (!string.IsNullOrEmpty(lnkBreadCrumb.CommandArgument))
            {
                SelectLinkVal = 1;
            }

            if (!string.IsNullOrEmpty(lnkBreadCategory.CommandArgument))
            {
                SelectLinkVal = 2;
            }

            if (!string.IsNullOrEmpty(lnkBreadgroup.CommandArgument))
            {
                SelectLinkVal = 3;
            }

            if (!string.IsNullOrEmpty(lnkBreadCatValue.CommandArgument))
            {
                SelectLinkVal = 4;
            }

            if (Session[this.Mode.ToString() + "btnViewQuickList_Click"] != null)
            {
                if ((int)Session[this.Mode.ToString() + "btnViewQuickList_Click"] == 1)
                {
                    SelectLinkVal = 5;
                }
            }

            if (SelectLinkVal == 1)
            {
                SelectedNodeBind(lnkBreadCrumb.CommandArgument, lnkBreadCrumb.Text);
            }
            else if (SelectLinkVal == 2)
            {
                SelectedNodeBind(lnkBreadCategory.CommandArgument, lnkBreadCategory.Text);
            }
            else if (SelectLinkVal == 3)
            {
                SelectedNodeBind(lnkBreadgroup.CommandArgument, lnkBreadgroup.Text);
            }
            else if (SelectLinkVal == 4)
            {
                SelectedNodeBind(lnkBreadCatValue.CommandArgument, lnkBreadCatValue.Text);
            }
            else if (SelectLinkVal == 5)
            {
                Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
                CascadingDropDown1.SelectedValue = ":::";
                // Global Variable to override dropdownlist value that is
                // not updated until client html is refreshed
                _overrideProductGroupQuickList = true;
                cbxOnly.Checked = false;

                bool isQuicklist = chkQuickList.Checked;
                chkQuickList.Checked = true;
                txtInventorySearchValue.Text = "";
                txtKeywordSearchValue.Text = "";
                LoadQuickListDetails();
                BtnFindDetails();
                plAddImages.Visible = false;
                dvProdListView.Visible = true;
                dvImageBanners.Visible = false;
                chkQuickList.Checked = isQuicklist;

#if DEBUG
        TimeSpan elapsedtimeReturn = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeReturn, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
                return;
            }
            else if (SelectLinkVal == 0)
            {
                GetDetails();
            }

            plAddImages.Visible = true;
            dvProdListView.Visible = false;

            if (tbSearch.Visible == true)
            {
                dvImageBanners.Visible = true;
            }
            else
            {
                if (lvProducts.Items.Count > 0 || lvNoImagesProductsList.Items.Count > 0)
                    dvImageBanners.Visible = false;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void GetPriceChangeRelatedDetails()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (igTree.CheckedNodes.Count > 0)
        {
            RetainPriceChange();
            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
        }
        else
        {
            Control controldetails = new Control();

            if (Mode == ModeType.Inventory)
            {
                controldetails = this.Parent;
            }
            else if (Mode == ModeType.QuoteEditAddItems)
            {
                controldetails = this.Parent.Parent;
            }

            Infragistics.Web.UI.NavigationControls.WebDataMenu wbDatamenu = new Infragistics.Web.UI.NavigationControls.WebDataMenu();

            wbDatamenu = (Infragistics.Web.UI.NavigationControls.WebDataMenu)controldetails.FindControl("WebDataMenu1");
            string SelMenu = "Home1";

            for (int menucount = 0; menucount <= wbDatamenu.Items.Count - 1; menucount++)
            {
                if (wbDatamenu.Items[menucount].Selected == true)
                {
                    SelMenu = wbDatamenu.Items[menucount].Text;
                }
            }

            if (txtKeywordSearchValue.Text != string.Empty)
            {
                FinDetails(txtKeywordSearchValue.Text);
            }
            else
            {
                if (Session[this.Mode.ToString() + "btnViewQuickList_Click"] == null)
                {
                    Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;
                }

                if ((int)Session[this.Mode.ToString() + "btnViewQuickList_Click"] == 1)
                {
                    CascadingDropDown1.SelectedValue = ":::";
                    // Global Variable to override dropdownlist value that is
                    // not updated until client html is refreshed
                    _overrideProductGroupQuickList = true;
                    cbxOnly.Checked = false;

                    bool isQuicklist = chkQuickList.Checked;
                    chkQuickList.Checked = true;

                    LoadQuickListDetails();
                    BtnFindDetails();

                    _overrideProductGroupQuickList = false;
                    chkQuickList.Checked = isQuicklist;

                    Session[this.Mode.ToString() + "LastSearchExecuted"] = "QuickList";
                }
                else
                {
                    if ((lvProducts.Items.Count > 0) || (lvNoImagesProductsList.Items.Count > 0))
                    {
                        FinDetails("");
                    }
                    else
                    {
                        GetDetails();
                    }
                }
            }

            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void SelectedNodeBind(string BindedValue, string BindedText)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string[] SplitBindedValue;
        SplitBindedValue = BindedValue.Split('_');

        if (SplitBindedValue.Length > 0)
        {
            string sNodeSearch = SplitBindedValue[0].ToString();

            //based on selected node redirects it to respective method
            if (sNodeSearch == "Catid")
            {
                LoadSelectedCategoryDetails(SplitBindedValue[1].ToString());
                igTree.CssClass = "varigdt_NodeHolder";
                lvNoImagesProductsList.Items.Clear();
                lvNoImagesProductsList.DataBind();
                lvProducts.Items.Clear();
                lvProducts.DataBind();
            }
            else if (sNodeSearch == "GroupId")
            {
                if (SplitBindedValue.Length > 1)
                {

                    LoadGroupdetails(SplitBindedValue[1].ToString(), SplitBindedValue[2].ToString(), BindedText);
                    igTree.CssClass = "var2igdt_NodeHolder";
                    lvNoImagesProductsList.Items.Clear();
                    lvNoImagesProductsList.DataBind();
                    lvProducts.Items.Clear();
                    lvProducts.DataBind();
                }
            }
            //if this method is fired then breadcrumbs with prod view grid needs to be displayed
            else if (sNodeSearch == "AttributeVal")
            {
                Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

                LoadAttributesDet(BindedValue, "AttributeVal");
                lblErrorMessage.Text = "";
            }
            else if (sNodeSearch == "AttributeLabel")
            {
                LoadAttributesDet(BindedValue, "AttributeLabel");
            }
            else
            {
                dvImageBanners.Visible = false;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Load respective catogeries groups and attributes which is selected
    /// </summary>
    /// <param name="sSelCategory"></param>
    protected void LoadSelectedCategoryDetails(string sSelCategory)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataSet dsCatDataset = new DataSet();
        DataSet dsFetchgroups = new DataSet();
        DataSet dsAttributesdataset = new DataSet();
        DataSet dsReferencedBannerDataSet = new DataSet();

        dsCatDataset = (DataSet)Session[this.Mode.ToString() + "dsReferencedCategoryDataSet"];

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        lblAdvSearchAllProd.Visible = false;
        lnkBreadCrumb.Visible = true;

        if (dsCatDataset.Tables.Count > 0)
        {
            DataView dvFilterSelCategory;
            dvFilterSelCategory = dsCatDataset.Tables[0].DefaultView;
            dvFilterSelCategory.RowFilter = "CategoryGuid='" + sSelCategory + "'";

            if (dvFilterSelCategory.ToTable().Rows.Count > 0)
            {
                igTree.Nodes.Clear();
                igTree.AllNodes.Clear();
                TreeNode tncategory = new TreeNode();
                tncategory.Text = dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();
                tncategory.ToolTip = dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();
                tncategory.Value = "Catid_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString();

                DataTreeNode dtrncategory = new DataTreeNode();

                if (dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString().Length > 26)
                {
                    dtrncategory.Text = dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString().Substring(0, 26);
                }

                dtrncategory.Text = dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();
                dtrncategory.ToolTip = dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();
                dtrncategory.Value = "Catid_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString();

                lnkBreadCategory.CommandArgument = "Catid_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString();
                lnkBreadCategory.Text = " " + dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();
                lnkBreadCategory.Visible = true;
                lblBreadCatSep.Visible = true;
                lnkBreadgroup.Text = "";
                lnkBreadgroup.Visible = false;
                lblBreadGroupSep.Visible = false;
                lnkBreadCatValue.Visible = false;
                lnkBreadCatValue.CommandArgument = "";

                //getting catguid
                inventory.FetchGroups(dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString(), out ReturnCode, out MessageText);

                if (ReturnCode != 0)
                {
                    if ((string)Session["GenericLogin"] == "True")
                    {
                        Session.RemoveAll();
                        Response.Redirect("~/Default.aspx");
                    }
                    else
                    {
                        Session.RemoveAll();
                        Response.Redirect("~/Logon.aspx");
                    }
                }

                dsFetchgroups = inventory.ReferencedGroupDataSet;
                dsReferencedBannerDataSet = inventory.ReferencedBannerDataSet;
                RenderImageButtons(dsReferencedBannerDataSet, 1, inventory.ReferencedDataSet);

                if (dsFetchgroups.Tables[0].Rows.Count > 0)
                {
                    for (int iGroupCount = 0; iGroupCount <= dsFetchgroups.Tables[0].Rows.Count - 1; iGroupCount++)
                    {
                        TreeNode childGroup = new TreeNode();
                        DataTreeNode dtrchildGroup = new DataTreeNode();
                        childGroup.Text = dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupDescription"].ToString();
                        childGroup.Value = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupGuid"].ToString();
                        childGroup.ToolTip = dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupDescription"].ToString();

                        if (dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupDescription"].ToString().Length > 21)
                        {
                            dtrchildGroup.Text = dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupDescription"].ToString().Substring(0, 21);
                        }
                        else
                        {
                            dtrchildGroup.Text = dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupDescription"].ToString();
                        }

                        dtrchildGroup.Value = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupGuid"].ToString();
                        dtrchildGroup.ToolTip = dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupDescription"].ToString();

                        ReturnCode = 0;
                        MessageText = "";
                        inventory.FetchAttributes(dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString(), dsFetchgroups.Tables[0].Rows[iGroupCount]["GroupGuid"].ToString(), out ReturnCode, out MessageText);

                        if (ReturnCode != 0)
                        {
                            if ((string)Session["GenericLogin"] == "True")
                            {
                                Session.RemoveAll();
                                Response.Redirect("~/Default.aspx");
                            }
                            else
                            {
                                Session.RemoveAll();
                                Response.Redirect("~/Logon.aspx");
                            }
                        }

                        dsAttributesdataset = inventory.ReferencedAttributeDataSet;

                        igTree.Nodes.Add(dtrchildGroup);
                        igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.Off;
                        divFloatingSearch1.Visible = false;
                        divFloatingSearch2.Visible = false;

                        igTree.NodeSettings.HoverCssClass = "treelinkHover";
                    }
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// for loading selected group details with respective banners 
    /// </summary>
    /// <param name="sSelCategory"></param>
    /// <param name="sSelgroup"></param>
    /// <param name="sSelGroupName"></param>
    protected void LoadGroupdetails(string SelCategory, string Selgroup, string SelGroupName)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        lblAdvSearchAllProd.Visible = false;
        lnkBreadCrumb.Visible = true;
        DataSet dsCatDataset = new DataSet();
        DataSet dsFetchgroups = new DataSet();
        DataSet dsAttributesdataset = new DataSet();
        DataSet dsReferencedBannerDataSet = new DataSet();

        dsCatDataset = (DataSet)Session[this.Mode.ToString() + "dsReferencedCategoryDataSet"];

        DataView dvFilterSelCategory;
        dvFilterSelCategory = dsCatDataset.Tables[0].DefaultView;
        dvFilterSelCategory.RowFilter = "CategoryGuid='" + SelCategory + "'";

        igTree.Nodes.Clear();
        igTree.AllNodes.Clear();

        if (dvFilterSelCategory.ToTable().Rows.Count > 0)
        {
            if (dvFilterSelCategory.ToTable().Rows[0]["HomeIndicator"].ToString().ToUpper() != "TRUE")
            {
                lnkBreadCategory.CommandArgument = "Catid_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString();
                lnkBreadCategory.Text = " " + dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();
                lblBreadCatSep.Visible = true;
            }
            else
            {
                lblBreadCatSep.Visible = false;
            }
        }
        else
        {
            lblBreadCatSep.Visible = false;
        }

        lnkBreadCategory.Visible = true;

        int ReturnCode = 0;
        string MessageText = "";

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        inventory.FetchAttributes(SelCategory, Selgroup, out ReturnCode, out MessageText);

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        dsAttributesdataset = inventory.ReferencedAttributeDataSet;
        dsReferencedBannerDataSet = inventory.ReferencedBannerDataSet;
        RenderImageButtons(dsReferencedBannerDataSet, 2, inventory.ReferencedDataSet);

        Session["cachedBannerDataSet"] = dsReferencedBannerDataSet;

        if (dsAttributesdataset.Tables.Count > 0)
        {
            if (dsAttributesdataset.Tables[0].Rows.Count > 0)
            {
                lnkBreadgroup.Text = " " + dsAttributesdataset.Tables[0].Rows[0]["GroupDescription"].ToString();

                if (dvFilterSelCategory.ToTable().Rows.Count > 0)
                {
                    lnkBreadgroup.CommandArgument = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString();
                }
                else
                {
                    lnkBreadgroup.CommandArgument = "GroupId_" + "" + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString();
                }

                lnkBreadgroup.Visible = true;
                lblBreadGroupSep.Visible = true;

                if (lnkBreadCategory.Text.ToString().Trim().ToUpper() != "HOME")
                {
                    lnkBreadCategory.Visible = true;
                }
                else
                {
                    lnkBreadCategory.Visible = false;
                    lblBreadCatSep.Visible = false;
                }

                DataTable dtAttributelabel = new DataTable();
                dtAttributelabel = dsAttributesdataset.Tables[0].DefaultView.ToTable(true, "AttributeLabel", "Description").Copy();

                if (dtAttributelabel.Rows.Count > 0)
                {
                    igTree.Nodes.Clear();

                    for (int iChilAttributelabel = 0; iChilAttributelabel <= dtAttributelabel.Rows.Count - 1; iChilAttributelabel++)
                    {
                        TreeNode tnChilAttributeLabel = new TreeNode();
                        tnChilAttributeLabel.Text = dtAttributelabel.Rows[iChilAttributelabel]["Description"].ToString();
                        tnChilAttributeLabel.ToolTip = dtAttributelabel.Rows[iChilAttributelabel]["Description"].ToString();
                        tnChilAttributeLabel.SelectAction = TreeNodeSelectAction.None;

                        DataTreeNode dtrtnChilAttributeLabel = new DataTreeNode();

                        if (dtAttributelabel.Rows[iChilAttributelabel]["Description"].ToString().Length > 25)
                        {
                            dtrtnChilAttributeLabel.Text = dtAttributelabel.Rows[iChilAttributelabel]["Description"].ToString().Substring(0, 25);
                        }
                        else
                        {
                            dtrtnChilAttributeLabel.Text = dtAttributelabel.Rows[iChilAttributelabel]["Description"].ToString();
                        }

                        string CatGuid = string.Empty;

                        if (dvFilterSelCategory.ToTable().Rows.Count > 0)
                        {
                            CatGuid = dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString();
                        }

                        dtrtnChilAttributeLabel.Value = "AttributeLabel_" + CatGuid + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString() + "_" +
                        dtAttributelabel.Rows[iChilAttributelabel]["AttributeLabel"].ToString();
                        dtrtnChilAttributeLabel.ToolTip = dtAttributelabel.Rows[iChilAttributelabel]["Description"].ToString();

                        DataView dvChildAttributevalue;
                        dvChildAttributevalue = dsAttributesdataset.Tables[0].DefaultView;
                        dvChildAttributevalue.RowFilter = "AttributeLabel='" + dtAttributelabel.Rows[iChilAttributelabel]["AttributeLabel"].ToString() + "'";

                        if (dvChildAttributevalue.ToTable().Rows.Count > 0)
                        {
                            for (int ChildAttribute = 0; ChildAttribute <= dvChildAttributevalue.ToTable().Rows.Count - 1; ChildAttribute++)
                            {
                                TreeNode tnchilAttributeValue = new TreeNode();
                                tnchilAttributeValue.Text = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                tnchilAttributeValue.ToolTip = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                tnchilAttributeValue.Value = "AttributeVal_" + CatGuid + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString() + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValueGuid"].ToString() + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeLabel"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                tnchilAttributeValue.ShowCheckBox = true;
                                tnchilAttributeValue.SelectAction = TreeNodeSelectAction.None;
                                tnChilAttributeLabel.ChildNodes.Add(tnchilAttributeValue);
                                tnchilAttributeValue.SelectAction = TreeNodeSelectAction.Select;
                                DataTreeNode dtrtnchilAttributeValue = new DataTreeNode();

                                if (dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString().Length > 21)
                                {
                                    dtrtnchilAttributeValue.Text = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString().Substring(0, 21);
                                }
                                else
                                {
                                    dtrtnchilAttributeValue.Text = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                }

                                dtrtnchilAttributeValue.ToolTip = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                dtrtnchilAttributeValue.Value = "AttributeVal_" + CatGuid + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString() + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeGuid"].ToString()
                                             + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValueGuid"].ToString() + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeLabel"].ToString()
                                             + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                dtrtnChilAttributeLabel.Nodes.Add(dtrtnchilAttributeValue);
                                string CompareValue = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                DataTable dtCheckVal = new DataTable();
                                if (Session[this.Mode.ToString() + "priorLoginCheckedItems"] != null && (string)Session["GenericLogin"] != "True")
                                {
                                    dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];
                                }
                                else
                                {
                                    if (Session[this.Mode.ToString() + "BackButton_priorLoginCheckedItems"] != null)
                                        dtCheckVal = (DataTable)Session[this.Mode.ToString() + "BackButton_priorLoginCheckedItems"];
                                }

                                if (dtCheckVal != null)
                                {
                                    if (dtCheckVal.Rows.Count > 0)
                                    {
                                        DataRow[] drCheckVal;
                                        drCheckVal = dtCheckVal.Select("Guid ='" + dtrtnchilAttributeValue.Value + "'");

                                        if (drCheckVal.Length > 0)
                                        {
                                            dtrtnchilAttributeValue.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                        }

                                        DataRow[] drCheckLabel;
                                        drCheckLabel = dtCheckVal.Select("Guid ='" + dtrtnChilAttributeLabel.Value + "'");

                                        if (drCheckLabel.Length > 0)
                                        {
                                            dtrtnChilAttributeLabel.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                        }
                                    }
                                }
                            }
                        }

                        tnChilAttributeLabel.ChildNodes.Add(tnChilAttributeLabel);

                        dtrtnChilAttributeLabel.ExpandChildren();

                        igTree.Nodes.Add(dtrtnChilAttributeLabel);
                    }
                }

                igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.BiState;
                divFloatingSearch1.Visible = true;
                divFloatingSearch2.Visible = true;

                igTree.NodeSettings.HoverCssClass = "treelinkHover";

                lnkBreadgroup.Visible = true;
                lblBreadGroupSep.Visible = true;
            }
            else
            {
                DataSet dsGetGroups = new DataSet();

                if (string.IsNullOrEmpty(SelGroupName))
                {
                    ReturnCode = 0;
                    MessageText = "";
                    inventory.FetchGroups(SelCategory, out ReturnCode, out MessageText);

                    if (ReturnCode != 0)
                    {
                        if ((string)Session["GenericLogin"] == "True")
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Default.aspx");
                        }
                        else
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Logon.aspx");
                        }
                    }

                    dsGetGroups = inventory.ReferencedGroupDataSet;
                    DataRow[] drgroup;

                    if (!DBNull.Value.Equals(dsGetGroups))
                    {
                        if (dsGetGroups.Tables.Count > 0)
                        {
                            drgroup = dsGetGroups.Tables[0].Select("GroupGuid='" + Selgroup + "'");

                            if (drgroup.Length > 0)
                            {
                                SelGroupName = drgroup[0]["GroupDescription"].ToString();
                                lnkBreadgroup.Visible = true;
                                lblBreadGroupSep.Visible = true;
                            }
                            else
                            {
                                lblBreadGroupSep.Visible = false;
                            }
                        }
                    }
                }
                else
                {
                    if (SelGroupName.StartsWith(">"))
                    {
                        SelGroupName = SelGroupName.Replace(">", "");
                    }

                    lnkBreadgroup.Visible = true;
                    lblBreadGroupSep.Visible = true;
                }

                lnkBreadgroup.Text = " " + SelGroupName;
                lnkBreadgroup.CommandArgument = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + Selgroup;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sSelCategory"></param>
    /// <param name="sSelgroup"></param>
    /// <param name="sSelGroupName"></param>
    /// <param name="sAttLabel"></param>
    /// <param name="sAttName"></param>
    protected void LoadAttributesDetails(string sSelCategory, string sSelgroup, string sSelGroupName, string sAttLabel, string sAttName)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataSet dsCatDataset = new DataSet();
        DataSet dsFetchgroups = new DataSet();
        DataSet dsAttributesdataset = new DataSet();
        DataSet dsReferencedBannerDataSet = new DataSet();

        dsCatDataset = (DataSet)Session[this.Mode.ToString() + "dsReferencedCategoryDataSet"];

        DataView dvFilterSelCategory;
        dvFilterSelCategory = dsCatDataset.Tables[0].DefaultView;
        dvFilterSelCategory.RowFilter = "CategoryGuid='" + sSelCategory + "'";

        igTree.Nodes.Clear();

        int ReturnCode = 0;
        string MessageText = "";

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        inventory.FetchAttributes(sSelCategory, sSelgroup, out ReturnCode, out MessageText);

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        dsAttributesdataset = inventory.ReferencedAttributeDataSet;

        if (dsAttributesdataset.Tables.Count > 0)
        {
            if (dsAttributesdataset.Tables[0].Rows.Count > 0)
            {
                lnkBreadgroup.Text = " " + dsAttributesdataset.Tables[0].Rows[0]["GroupDescription"].ToString();
                lnkBreadgroup.CommandArgument = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString();
                lnkBreadgroup.Visible = true;
                lblBreadGroupSep.Visible = true;

                if (lnkBreadCategory.Text == string.Empty)
                {
                    lnkBreadCategory.CommandArgument = "Catid_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString();
                    lnkBreadCategory.Text = " " + dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();

                    if (dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString().ToUpper() != "HOME")
                    {
                        lnkBreadCategory.Visible = true;
                        lblBreadCatSep.Visible = true;
                    }
                    else
                    {
                        lnkBreadCategory.Visible = false;
                        lblBreadCatSep.Visible = false;
                    }
                }

                DataTable dtAttributelabel = new DataTable();

                DataRow[] drAttributelabel;
                dtAttributelabel = dsAttributesdataset.Tables[0].DefaultView.ToTable(true, "AttributeLabel", "Description").Copy();
                drAttributelabel = dsAttributesdataset.Tables[0].DefaultView.ToTable().Select("AttributeGuid='" + sAttLabel + "'" + "AND AttributeValueGuid='" + sAttName + "'");

                if (drAttributelabel.Length > 0)
                {
                    lnkBreadCatValue.Text = " " + drAttributelabel[0]["AttributeValue"].ToString();

                    lnkBreadCatValue.CommandArgument = "AttributeVal_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString() + "_" + drAttributelabel[0]["AttributeGuid"].ToString()
                                + "_" + drAttributelabel[0]["AttributeValueGuid"].ToString() + "_" + drAttributelabel[0]["AttributeLabel"].ToString()
                                + "_" + drAttributelabel[0]["AttributeValue"].ToString();
                }

                if (dtAttributelabel.Rows.Count > 0)
                {
                    for (int ChilAttributelabel = 0; ChilAttributelabel <= dtAttributelabel.Rows.Count - 1; ChilAttributelabel++)
                    {
                        DataView dvChildAttributevalue;
                        dvChildAttributevalue = dsAttributesdataset.Tables[0].DefaultView;
                        dvChildAttributevalue.RowFilter = "AttributeLabel='" + dtAttributelabel.Rows[ChilAttributelabel]["AttributeLabel"].ToString() + "'";

                        TreeNode tnChilAttributeLabel = new TreeNode();
                        tnChilAttributeLabel.Text = dtAttributelabel.Rows[ChilAttributelabel]["Description"].ToString();
                        tnChilAttributeLabel.ToolTip = dtAttributelabel.Rows[ChilAttributelabel]["Description"].ToString();
                        tnChilAttributeLabel.SelectAction = TreeNodeSelectAction.None;

                        DataTreeNode dtrtnChilAttributeLabel = new DataTreeNode();
                        dtrtnChilAttributeLabel.Text = dtAttributelabel.Rows[ChilAttributelabel]["Description"].ToString();
                        dtrtnChilAttributeLabel.ToolTip = dtAttributelabel.Rows[ChilAttributelabel]["Description"].ToString();
                        dtrtnChilAttributeLabel.Value = "AttributeLabel_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString() + "_" +
                           dtAttributelabel.Rows[ChilAttributelabel]["AttributeLabel"].ToString();

                        if (dvChildAttributevalue.ToTable().Rows.Count > 0)
                        {
                            for (int ChildAttribute = 0; ChildAttribute <= dvChildAttributevalue.ToTable().Rows.Count - 1; ChildAttribute++)
                            {
                                TreeNode tnchilAttributeValue = new TreeNode();
                                tnchilAttributeValue.Text = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                tnchilAttributeValue.ToolTip = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                tnchilAttributeValue.Value = "AttributeVal_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValueGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeLabel"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                tnchilAttributeValue.ShowCheckBox = true;
                                tnchilAttributeValue.SelectAction = TreeNodeSelectAction.Select;

                                DataTreeNode dtrtnchilAttributeValue = new DataTreeNode();
                                dtrtnchilAttributeValue.Text = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                dtrtnchilAttributeValue.ToolTip = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                dtrtnchilAttributeValue.Value = "AttributeVal_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValueGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeLabel"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                string CompareValue = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                int Compareval = string.Compare(CompareValue, sAttName);

                                if (Compareval == 0)
                                {
                                    dtrtnchilAttributeValue.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                }

                                //if node is clicked and that node needs to cheked
                                if (dtrtnchilAttributeValue.Value == sAttName)
                                {
                                    dtrtnchilAttributeValue.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                }
                                else
                                {
                                    if (Compareval == 1)
                                    {
                                        string[] SplitAttributevalue;
                                        SplitAttributevalue = sAttName.Split('_');
                                        if (SplitAttributevalue.Length > 0)
                                        {
                                            if (sAttName == dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValueGuid"].ToString())
                                            {
                                                dtrtnchilAttributeValue.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                            }
                                        }
                                    }
                                }

                                DataTable dtCheckVal = new DataTable();
                                dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

                                if (dtCheckVal != null)
                                {
                                    if (dtCheckVal.Rows.Count > 0)
                                    {
                                        DataRow[] drCheckVal;
                                        drCheckVal = dtCheckVal.Select("Guid ='" + dtrtnchilAttributeValue.Value + "'");

                                        if (drCheckVal.Length > 0)
                                        {
                                            dtrtnchilAttributeValue.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                        }

                                        DataRow[] drCheckLabel;
                                        drCheckLabel = dtCheckVal.Select("Guid ='" + dtrtnChilAttributeLabel.Value + "'");

                                        if (drCheckLabel.Length > 0)
                                        {
                                            dtrtnChilAttributeLabel.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                        }
                                    }
                                }

                                tnChilAttributeLabel.ChildNodes.Add(tnchilAttributeValue);
                                dtrtnChilAttributeLabel.Nodes.Add(dtrtnchilAttributeValue);
                            }
                        }

                        DataTable dtCheckLabel = new DataTable();
                        dtCheckLabel = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

                        if (dtCheckLabel != null)
                        {
                            if (dtCheckLabel.Rows.Count > 0)
                            {
                                DataRow[] drCheckLabel;
                                drCheckLabel = dtCheckLabel.Select("Guid ='" + dtrtnChilAttributeLabel.Value + "'");

                                if (drCheckLabel.Length > 0)
                                {
                                    dtrtnChilAttributeLabel.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                }
                            }
                        }

                        dtrtnChilAttributeLabel.ExpandChildren();
                        igTree.Nodes.Add(dtrtnChilAttributeLabel);
                    }
                }

                igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.BiState;
                divFloatingSearch1.Visible = true;
                divFloatingSearch2.Visible = true;

                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = null;
            }
            else
            {
                lnkBreadgroup.Text = " " + sSelGroupName;
                lnkBreadgroup.CommandArgument = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + sSelgroup;
                lnkBreadgroup.Visible = true;
                lblBreadGroupSep.Visible = true;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sSelVal"></param>
    /// <param name="sNodeType"></param>
    protected void LoadAttributesDet(string sSelVal, string sNodeType)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 1;
        }

        if (sNodeType == "AttributeVal")
        {
            for (int Attributeval = 0; Attributeval <= igTree.Nodes.Count - 1; Attributeval++)
            {
                for (int k = 0; k <= igTree.Nodes[Attributeval].Nodes.Count - 1; k++)
                {
                    if (igTree.Nodes[Attributeval].Nodes[k].Value == sSelVal)
                    {
                        if (igTree.Nodes[Attributeval].Nodes[k].CheckState == Infragistics.Web.UI.CheckBoxState.Unchecked)
                        {
                            igTree.Nodes[Attributeval].Nodes[k].CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                        }
                        else
                        {
                            igTree.Nodes[Attributeval].Nodes[k].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                            igTree.Nodes[Attributeval].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                        }
                    }
                }
            }
        }
        else if (sNodeType == "AttributeLabel")
        {
            for (int AttributeLabel = 0; AttributeLabel < igTree.Nodes.Count; AttributeLabel++)
            {
                if (igTree.Nodes[AttributeLabel].Value == sSelVal)
                {
                    if (igTree.Nodes[AttributeLabel].CheckState == Infragistics.Web.UI.CheckBoxState.Unchecked)
                    {
                        igTree.Nodes[AttributeLabel].CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                    }
                    else
                    {
                        igTree.Nodes[AttributeLabel].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                    }

                    if (igTree.Nodes[AttributeLabel].Nodes.Count > 0)
                    {
                        for (int k = 0; k < igTree.Nodes[AttributeLabel].Nodes.Count; k++)
                        {
                            if (igTree.Nodes[AttributeLabel].CheckState == Infragistics.Web.UI.CheckBoxState.Checked)
                            {
                                igTree.Nodes[AttributeLabel].Nodes[k].CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                            }
                            else
                            {
                                igTree.Nodes[AttributeLabel].Nodes[k].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                            }
                        }
                    }
                }
            }
        }

        int CheckCount = igTree.CheckedNodes.Count;

        if (CheckCount > 0)
        {
            Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet dsSelectedAttribs = new Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet();
            DataTable dtPriorLoginCheckedItems = new DataTable();
            dtPriorLoginCheckedItems.Columns.Add("Guid");

            string Group = string.Empty;

            for (int i = 0; i <= CheckCount - 1; i++)
            {
                string[] AttributeDetails = null;
                string AttributeVal;

                AttributeVal = igTree.CheckedNodes[i].Value;
                DataRow drNewRow;
                drNewRow = dtPriorLoginCheckedItems.NewRow();
                drNewRow["Guid"] = igTree.CheckedNodes[i].Value;
                dtPriorLoginCheckedItems.Rows.Add(drNewRow);
                dtPriorLoginCheckedItems.AcceptChanges();
                dtPriorLoginCheckedItems.GetChanges();

                if (!string.IsNullOrEmpty(AttributeVal))
                {
                    AttributeDetails = AttributeVal.Split('_');
                    Group = AttributeDetails[2];
                }

                if (AttributeDetails != null)
                {
                    if (AttributeDetails.Length > 0)
                    {
                        if (AttributeDetails[0].ToString() != "AttributeLabel")
                        {
                            DataRow drSelAttributes = dsSelectedAttribs.ttSelectedAttributes.NewRow();
                            drSelAttributes["SelectedLabel"] = AttributeDetails[5];
                            drSelAttributes["SelectedValue"] = AttributeDetails[6];
                            dsSelectedAttribs.ttSelectedAttributes.Rows.Add(drSelAttributes);
                            dsSelectedAttribs.AcceptChanges();
                            dsSelectedAttribs.GetChanges();
                        }
                    }
                }
            }

            if ((string)Session["GenericLogin"] == "True")
            {
                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = dtPriorLoginCheckedItems;
            }

            if (Group != string.Empty)
            {
                DataSet dsProducts = new DataSet();
                dsProducts = GetfetchMultipleItems(Group, dsSelectedAttribs);

                if ((dsProducts != null && dsProducts.Tables.Count > 0 && dsProducts.Tables[0].Rows.Count > 0))
                {
                    btnAddAll2.Visible = true;
                    lblErrorMessage.Text = "";

                    BindProductGridViewDetails(dsProducts.Tables[0], 1);

                    Label2.Visible = true;
                    dvImageBanners.Visible = false;
                    dvProdListView.Visible = true;
                    lvProducts.Visible = true;
                    lvNoImagesProductsList.Visible = true;

                    Session[this.Mode.ToString() + "CachedInventoryDataSet"] = dsProducts;
                    Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = dsProducts;

                    lblHeaderError.Text = "";
                    lnkBreadCrumb.Visible = true;

                    if (lnkBreadCategory.Text.Trim().ToUpper() != "HOME")
                    {
                        lnkBreadCategory.Visible = true;

                        if (lnkBreadCategory.Text.Trim().Length > 0)
                        {
                            lblBreadCatSep.Visible = true;
                        }
                        else
                        {
                            lblBreadCatSep.Visible = false;
                        }
                    }
                    else
                    {
                        lnkBreadCategory.Visible = false;
                        lblBreadCatSep.Visible = false;
                    }

                    lblBreadGroupSep.Visible = true;
                    lnkBreadgroup.Visible = true;
                }
                else
                {
                    dvImageBanners.Visible = false;

                    btnAddAll2.Visible = false;
                    DataTable dtEmpty = new DataTable();

                    Label2.Visible = true;
                    lvProducts.Items.Clear();
                    lvProducts.DataBind();
                    lvNoImagesProductsList.Items.Clear();
                    lvNoImagesProductsList.DataBind();
                    lvProducts.Visible = false;
                    lvNoImagesProductsList.Visible = false;

                    if (TabContainer1.ActiveTab == tbSearch)
                    {
                        lblHeaderError.Text = NORESULTSFOUND;
                    }
                    else
                    {
                        lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
                    }

                    lblHeaderError.Visible = true;
                    dvProdListView.Visible = true;
                    lvPaging.Visible = false;
                    lblpageno.Text = "";
                    dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
                }
            }
        }
        else
        {
            DataTable dtCheckVal = new DataTable();
            dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

            if (dtCheckVal != null)
            {
                dtCheckVal.Rows.Clear();
                dtCheckVal.AcceptChanges();
                dtCheckVal.GetChanges();
                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = dtCheckVal;
            }

            SelectedNodeBind(lnkBreadgroup.CommandArgument, lnkBreadgroup.Text);
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sSelCategory"></param>
    /// <param name="sSelgroup"></param>
    /// <param name="sSelGroupName"></param>
    /// <param name="sAttLabel"></param>
    /// <param name="sAttName"></param>
    protected void LoadPriorLoginAttributesDetails(string SelCategory, string Selgroup, string SelGroupName, string AttLabel, string AttName)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataSet dsCatDataset = new DataSet();
        DataSet dsFetchgroups = new DataSet();
        DataSet dsAttributesdataset = new DataSet();
        DataSet dsReferencedBannerDataSet = new DataSet();

        dsCatDataset = (DataSet)Session[this.Mode.ToString() + "dsReferencedCategoryDataSet"];

        DataView dvFilterSelCategory;
        dvFilterSelCategory = dsCatDataset.Tables[0].DefaultView;
        dvFilterSelCategory.RowFilter = "CategoryGuid='" + SelCategory + "'";

        igTree.Nodes.Clear();

        int ReturnCode = 0;
        string MessageText = "";

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        inventory.FetchAttributes(SelCategory, Selgroup, out ReturnCode, out MessageText);

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        dsAttributesdataset = inventory.ReferencedAttributeDataSet;

        if (dsAttributesdataset.Tables.Count > 0)
        {
            if (dsAttributesdataset.Tables[0].Rows.Count > 0)
            {
                lnkBreadgroup.Text = " " + dsAttributesdataset.Tables[0].Rows[0]["GroupDescription"].ToString();
                lnkBreadgroup.CommandArgument = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString();
                lnkBreadgroup.Visible = true;
                lblBreadGroupSep.Visible = true;

                if (lnkBreadCategory.Text == string.Empty)
                {
                    lnkBreadCategory.CommandArgument = "Catid_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString();
                    lnkBreadCategory.Text = " " + dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString();

                    if (dvFilterSelCategory.ToTable().Rows[0]["CategoryDescription"].ToString().ToUpper() != "HOME")
                    {
                        lnkBreadCategory.Visible = true;
                        lblBreadCatSep.Visible = true;
                    }
                    else
                    {
                        lnkBreadCategory.Visible = false;
                        lblBreadCatSep.Visible = false;
                    }
                }

                DataTable dtAttributelabel = new DataTable();

                DataRow[] drAttributelabel;
                dtAttributelabel = dsAttributesdataset.Tables[0].DefaultView.ToTable(true, "AttributeLabel", "Description").Copy();
                drAttributelabel = dsAttributesdataset.Tables[0].DefaultView.ToTable().Select("AttributeGuid='" + AttLabel + "'" + "AND AttributeValueGuid='" + AttName + "'");

                if (drAttributelabel.Length > 0)
                {
                    lnkBreadCatValue.Text = " " + drAttributelabel[0]["AttributeValue"].ToString();

                    lnkBreadCatValue.CommandArgument = "AttributeVal_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString() + "_" + drAttributelabel[0]["AttributeGuid"].ToString()
                                + "_" + drAttributelabel[0]["AttributeValueGuid"].ToString() + "_" + drAttributelabel[0]["AttributeLabel"].ToString()
                                + "_" + drAttributelabel[0]["AttributeValue"].ToString();
                }

                if (dtAttributelabel.Rows.Count > 0)
                {
                    for (int ChilAttributelabel = 0; ChilAttributelabel <= dtAttributelabel.Rows.Count - 1; ChilAttributelabel++)
                    {
                        DataView dvChildAttributevalue;
                        dvChildAttributevalue = dsAttributesdataset.Tables[0].DefaultView;
                        dvChildAttributevalue.RowFilter = "AttributeLabel='" + dtAttributelabel.Rows[ChilAttributelabel]["AttributeLabel"].ToString() + "'";

                        DataTreeNode dtrtnChilAttributeLabel = new DataTreeNode();
                        dtrtnChilAttributeLabel.Text = dtAttributelabel.Rows[ChilAttributelabel]["Description"].ToString();
                        dtrtnChilAttributeLabel.ToolTip = dtAttributelabel.Rows[ChilAttributelabel]["Description"].ToString();

                        if (dvChildAttributevalue.ToTable().Rows.Count > 0)
                        {
                            for (int ChildAttribute = 0; ChildAttribute <= dvChildAttributevalue.ToTable().Rows.Count - 1; ChildAttribute++)
                            {
                                DataTreeNode dtrtnchilAttributeValue = new DataTreeNode();
                                dtrtnchilAttributeValue.Text = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                dtrtnchilAttributeValue.ToolTip = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                dtrtnchilAttributeValue.Value = "AttributeVal_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + dsAttributesdataset.Tables[0].Rows[0]["GroupGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValueGuid"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeLabel"].ToString()
                                    + "_" + dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();

                                string CompareValue = dvChildAttributevalue.ToTable().Rows[ChildAttribute]["AttributeValue"].ToString();
                                int Compareval = string.Compare(CompareValue, AttName);

                                if (Compareval == 0)
                                {
                                    dtrtnchilAttributeValue.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                }

                                DataTable dtCheckVal = new DataTable();
                                dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

                                if (dtCheckVal != null)
                                {
                                    if (dtCheckVal.Rows.Count > 0)
                                    {
                                        DataRow[] drCheckVal;
                                        drCheckVal = dtCheckVal.Select("Guid ='" + dtrtnchilAttributeValue.Value + "'");

                                        if (drCheckVal.Length > 0)
                                        {
                                            dtrtnchilAttributeValue.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                                        }
                                    }
                                }

                                dtrtnChilAttributeLabel.Nodes.Add(dtrtnchilAttributeValue);
                            }
                        }

                        dtrtnChilAttributeLabel.ExpandChildren();

                        igTree.Nodes.Add(dtrtnChilAttributeLabel);
                    }
                }

                igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.BiState;
                divFloatingSearch1.Visible = true;
                divFloatingSearch2.Visible = true;

                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = null;
            }
            else
            {
                lnkBreadgroup.Text = " " + SelGroupName;
                lnkBreadgroup.CommandArgument = "GroupId_" + dvFilterSelCategory.ToTable().Rows[0]["CategoryGuid"].ToString() + "_" + Selgroup;
                lnkBreadgroup.Visible = true;
                lblBreadGroupSep.Visible = true;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// dynamically creating imagebuttons
    /// </summary>
    private void RenderImageButtons(DataSet dsBanners, int iRefVal, DataSet dsReferenceDataset)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (dsBanners != null)
        {
            if (dsBanners.Tables.Count > 0)
            {
                if (dsBanners.Tables[0].Rows.Count > 0)
                {
                    DataTable dtBanner = new DataTable();
                    dtBanner = dsBanners.Tables[0].DefaultView.ToTable(true, "CategoryGuid", "GroupGuid", "LinkTypeReference", "LinkGroupGuid", "LinkAttrLabel", "LinkAttrValue", "ImageUrl", "Linktype", "EndofRow");

                    plAddImages.Controls.Clear();

                    for (int BannerCount = 0; BannerCount <= dtBanner.Rows.Count - 1; BannerCount++)
                    {
                        _img = new ImageButton();
                        _img.Attributes.Add("runat", "server");

                        string slinkType;

                        if (string.IsNullOrEmpty(dtBanner.Rows[BannerCount]["Linktype"].ToString()))
                        {
                            slinkType = "None";
                        }
                        else
                        {
                            slinkType = dtBanner.Rows[BannerCount]["Linktype"].ToString();
                        }

                        _img.ID = "img" + "_" + slinkType + "_" + dtBanner.Rows[BannerCount]["CategoryGuid"].ToString() + "_" + dtBanner.Rows[BannerCount]["GroupGuid"].ToString() + "_" + dtBanner.Rows[BannerCount]["LinkTypeReference"].ToString()
                            + "_" + dtBanner.Rows[BannerCount]["LinkGroupGuid"].ToString() + "_" + dtBanner.Rows[BannerCount]["LinkAttrLabel"].ToString() + "_" + dtBanner.Rows[BannerCount]["LinkAttrValue"].ToString() + "_" + dtBanner.Rows[BannerCount]["ImageUrl"].ToString();

                        _img.ImageUrl = dtBanner.Rows[BannerCount]["ImageUrl"].ToString();
                        _img.Click += new ImageClickEventHandler(img_Click);
                        _img.Attributes.Add("onclick", "javascript:return SetBannerId(this.id);");
                        _img.Attributes.Add("class", "FloatingImage");

                        this.plAddImages.Controls.Add(_img);

                        if ((bool)dtBanner.Rows[BannerCount]["EndofRow"] == true)
                        {
                            plAddImages.Controls.Add(new LiteralControl("</div><div class=\"pnlVertical\">"));
                        }
                    }

                    this.dvImageBanners.Visible = true;
                }
                else
                {
                    this.dvImageBanners.Visible = false;
                }
            }
            else
            {
                this.dvImageBanners.Visible = false;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }
    protected void SelectedImageDetails(string sDetails)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 4;
            Session[this.Mode.ToString() + "priorLoginSelectedImageDetails"] = sDetails;
        }

        string[] ImgDetails;
        string[] Imagetype;

        ImgDetails = sDetails.Split(new[] { "_img_" }, StringSplitOptions.None);

        if (ImgDetails.Length > 0)
        {
            Imagetype = ImgDetails[1].Split('_');

            AnalyticsInput aInput = new AnalyticsInput();
            aInput.Type = "event";
            aInput.Action = "Products - Banner Image Click";
            aInput.Label = Imagetype[0].ToUpper();
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

            if (Imagetype.Length > 0)
            {
                if (Imagetype[0].ToUpper() == "URL")
                {
                    String fileNQuery = "OpenNewURL('" + Imagetype[3].ToString() + "');";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "new pop up", "OpenNewURL('" + Imagetype[3].ToString() + "');", true);
                    string sPreviousId = (string)Session[this.Mode.ToString() + "RetainPreviousUrl"];

                    if (!string.IsNullOrEmpty(sPreviousId))
                    {
                        SelectedNodeBind(sPreviousId, "");
                    }
                    else
                    {
                        GetDetails();
                    }
                }
                else if (Imagetype[0].ToUpper() == "ATTRIBUTE GROUP")
                {
                    LoadGroupdetails(Imagetype[1].ToString(), Imagetype[3].ToString(), "");
                    lnkBreadgroup.CssClass = "brd-crumbactive";
                    lnkBreadCrumb.CssClass = "brd-crumbaInctive";
                    lnkBreadCategory.CssClass = "brd-crumbaInctive";
                    lnkBreadCatValue.CssClass = "brd-crumbaInctive";
                }
                else if (Imagetype[0].ToUpper() == "ATTRIBUTE CATEGORY")
                {
                    //LoadAttributesDetais
                    lnkBreadgroup.Text = "";
                    lnkBreadgroup.CommandArgument = "";
                    LoadSelectedCategoryDetails(Imagetype[3].ToString());
                    igTree.CssClass = "varigdt_NodeHolder";
                    lnkBreadCrumb.CssClass = "brd-crumbaInctive";
                    lnkBreadCategory.CssClass = "brd-crumbactive";
                    lnkBreadgroup.CssClass = "brd-crumbaInctive";
                    lnkBreadCatValue.CssClass = "brd-crumbaInctive";
                }
                else if (Imagetype[0].ToUpper() == "ATTRIBUTE VALUE")
                {
                    Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

                    string sSelGroup = Imagetype[2];

                    if (string.IsNullOrEmpty(sSelGroup))
                    {
                        sSelGroup = Imagetype[4];
                    }

                    LoadAttributesDetails(Imagetype[1], sSelGroup, "", Imagetype[5], Imagetype[6]);
                    igTree.CssClass = "var2igdt_NodeHolder";
                    BindProductsGrid(Imagetype, 2);
                }
                //if attribute type is empty then we need to show previously existing data
                else if (Imagetype[0].ToUpper() == "NONE")
                {
                    if (!string.IsNullOrEmpty(lnkBreadCategory.Text))
                    {
                        //here we are checking previous data is from category or group
                        string sGroup;
                        sGroup = lnkBreadCategory.Text.Replace(">", "");

                        if (!string.IsNullOrEmpty(sGroup))
                        {
                            SelectedNodeBind(lnkBreadgroup.CommandArgument, lnkBreadgroup.Text);
                        }
                        else
                        {
                            SelectedNodeBind(lnkBreadCategory.CommandArgument, lnkBreadCategory.Text);
                        }
                    }
                    else
                    {
                        GetDetails();
                    }
                }
            }

            Session[Mode.ToString() + "ImageClicked"] = null;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// while quick list button is pressed 
    /// </summary>
    protected void LoadQuickListDetails()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        int ReturnCode = 0;
        string MessageText = "";

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        inventory.FetchHomeCategories(out ReturnCode, out MessageText);

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        lnkBreadgroup.Visible = false;
        lblBreadGroupSep.Visible = false;
        lnkBreadCatValue.Visible = false;
        lnkBreadCategory.Visible = false;
        lnkBreadCategory.CommandArgument = "";
        lnkBreadgroup.CommandArgument = "";
        lnkBreadCrumb.Visible = true;
        lblBreadCatSep.Visible = false;
        DataSet dsReferencedBannerDataSet = new DataSet();
        DataSet dsFetchItemByAttribute = new DataSet();
        DataSet dsFetchgroups = new DataSet();
        DataSet dsAttributesdataset = new DataSet();
        DataSet dsbannerDataset = new DataSet();
        _dsReferencedCategoryDataSet = inventory.ReferencedCategoryDataSet;

        Session[this.Mode.ToString() + "dsReferencedCategoryDataSet"] = _dsReferencedCategoryDataSet;

        dsReferencedBannerDataSet = inventory.ReferencedBannerDataSet;

        if (igTree.Nodes.Count > 0)
        {
            igTree.Nodes.Clear();
        }

        DataTable dtGroupDetails = (DataTable)Session[this.Mode.ToString() + "RetainFetchGroupDetails"];

        if (dtGroupDetails != null)
        {
            if (dtGroupDetails.Rows.Count > 0)
            {
                GetRetainDetails();

#if DEBUG
        TimeSpan elapsedtimeReturn = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeReturn, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
                return;
            }
        }

        if (_dsReferencedCategoryDataSet.Tables.Count > 0)
        {
            if (_dsReferencedCategoryDataSet.Tables[0].Rows.Count > 0)
            {
                for (int catogeries = 0; catogeries <= _dsReferencedCategoryDataSet.Tables[0].Rows.Count - 1; catogeries++)
                {
                    ReturnCode = 0;
                    MessageText = "";
                    inventory.FetchGroups(_dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString(), out ReturnCode, out MessageText);

                    if (ReturnCode != 0)
                    {
                        if ((string)Session["GenericLogin"] == "True")
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Default.aspx");
                        }
                        else
                        {
                            Session.RemoveAll();
                            Response.Redirect("~/Logon.aspx");
                        }
                    }

                    dsFetchgroups = inventory.ReferencedGroupDataSet;
                    dsReferencedBannerDataSet = inventory.ReferencedBannerDataSet;

                    TreeNode type;

                    type = new TreeNode();
                    string CatName = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    string homeIndicator = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["HomeIndicator"].ToString();

                    type.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    type.ToolTip = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    type.Value = "Catid_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString();
                    DataTreeNode dtrncategory = new DataTreeNode();

                    if (_dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Length > 26)
                    {
                        dtrncategory.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString().Substring(0, 26);
                    }
                    else
                    {
                        dtrncategory.Text = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    }

                    dtrncategory.ToolTip = _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryDescription"].ToString();
                    dtrncategory.Value = "Catid_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString();

                    if (dsFetchgroups.Tables.Count > 0)
                    {
                        if (dsFetchgroups.Tables[0].Rows.Count > 0)
                        {
                            for (int ChildGroup = 0; ChildGroup <= dsFetchgroups.Tables[0].Rows.Count - 1; ChildGroup++)
                            {
                                TreeNode childGroup = new TreeNode();
                                childGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                childGroup.ToolTip = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                childGroup.Value = "GroupId_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString() + "_" + dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupGuid"].ToString();

                                DataTreeNode dtrchildGroup = new DataTreeNode();

                                if (dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString().Length > 21)
                                {
                                    dtrchildGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString().Substring(0, 21);
                                }
                                else
                                {
                                    dtrchildGroup.Text = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                }

                                dtrchildGroup.ToolTip = dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupDescription"].ToString();
                                dtrchildGroup.Value = "GroupId_" + _dsReferencedCategoryDataSet.Tables[0].Rows[catogeries]["CategoryGuid"].ToString() + "_" + dsFetchgroups.Tables[0].Rows[ChildGroup]["GroupGuid"].ToString();

                                if (homeIndicator.ToUpper() != "TRUE")
                                {
                                    type.ChildNodes.Add(childGroup);
                                    dtrncategory.Nodes.Add(dtrchildGroup);
                                }
                            }

                            tbSearch.Visible = true;
                        }
                    }
                    if (homeIndicator.ToUpper() != "TRUE")
                    {
                        igTree.Nodes.Add(dtrncategory);
                        igTree.CheckBoxMode = Infragistics.Web.UI.CheckBoxMode.Off;
                        divFloatingSearch1.Visible = false;
                        divFloatingSearch2.Visible = false;
                    }
                }

                tbSearch.Visible = true;
                Div3.Visible = true;
            }
            else
            {
                tbSearch.Visible = false;
                Div3.Visible = false;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #region 'Click Events'

    protected void img_Click(object sender, ImageClickEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Category bread crumb click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void lnkCategory_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 3;

            Session[this.Mode.ToString() + "priorLoginSelectedNodeCatGroup"] = lnkBreadCategory.CommandArgument;
        }

        Session[this.Mode.ToString() + "BackButton_SelectedNodeCatGroup"] = lnkBreadCategory.CommandArgument;
        Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

        plAddImages.Controls.Clear();
        DataTable dtCheckVal = new DataTable();
        dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

        if (dtCheckVal != null)
        {
            if (dtCheckVal.Rows.Count > 0)
            {
                dtCheckVal.Rows.Clear();
                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = dtCheckVal;
            }
        }

        Session[this.Mode.ToString() + "RetainPreviousUrl"] = lnkBreadCategory.CommandArgument;
        SelectedNodeBind(lnkBreadCategory.CommandArgument, lnkBreadCategory.Text);
        igTree.AllNodes.Clear();
        TabContainer1.ActiveTab = tbSearch;
        lnkBreadCrumb.CssClass = "brd-crumbaInctive";

        lnkBreadCategory.CssClass = "brd-crumbactive";

        lnkBreadgroup.CssClass = "brd-crumbaInctive";
        lnkBreadCatValue.CssClass = "brd-crumbaInctive";

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Group Bread crumb click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void lnkBreadgroup_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 3;

            Session[this.Mode.ToString() + "priorLoginSelectedNodeCatGroup"] = lnkBreadgroup.CommandArgument;
        }

        Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

        plAddImages.Controls.Clear();
        Session[this.Mode.ToString() + "RetainPreviousUrl"] = lnkBreadgroup.CommandArgument;

        SelectedNodeBind(lnkBreadgroup.CommandArgument, lnkBreadgroup.Text);

        for (int j = 0; j <= igTree.Nodes.Count - 1; j++)
        {
            igTree.Nodes[j].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;

            for (int k = 0; k <= igTree.Nodes[j].Nodes.Count - 1; k++)
            {
                igTree.Nodes[j].Nodes[k].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
            }
        }

        igTree.AllNodes.Clear();

        DataTable dtCheckVal = new DataTable();
        dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

        if (dtCheckVal != null)
        {
            if (dtCheckVal.Rows.Count > 0)
            {
                dtCheckVal.Rows.Clear();
                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = dtCheckVal;
            }
        }

        lnkBreadCatValue.Visible = false;
        lnkBreadCatValue.CommandArgument = "";
        TabContainer1.ActiveTab = tbSearch;
        lnkBreadCrumb.CssClass = "brd-crumbaInctive";

        lnkBreadCategory.CssClass = "brd-crumbaInctive";

        lnkBreadgroup.CssClass = "brd-crumbactive";
        lnkBreadCatValue.CssClass = "brd-crumbaInctive";

        if (dvImageBanners.Visible == true)
            dvProdListView.Visible = false;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void lnkBreadCatVal_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Session[this.Mode.ToString() + "RetainPreviousUrl"] = lnkBreadCatValue.CommandArgument;

        Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

        SelectedNodeBind(lnkBreadCatValue.CommandArgument, lnkBreadCatValue.Text);
        TabContainer1.ActiveTab = tbSearch;
        lnkBreadCrumb.CssClass = "brd-crumbaInctive";

        lnkBreadCategory.CssClass = "brd-crumbaInctive";

        lnkBreadgroup.CssClass = "brd-crumbaInctive";
        lnkBreadCatValue.CssClass = "brd-crumbactive";

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// click event of home 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void lnkHome_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (plAddImages.Controls.Count > 0)
        {
            plAddImages.Controls.Clear();
        }

        Session[this.Mode.ToString() + "RetainPreviousUrl"] = string.Empty;
        lnkBreadCategory.Text = "";
        lnkBreadCategory.Visible = false;
        lblBreadCatSep.Visible = false;

        Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

        if (Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] != null)
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = null;
        }

        dmfetch = new DataManager();
        dmfetch.SetCache("FetchCatDetails", null);
        GetDetails();
        DataTable dtempty = new DataTable();
        dtempty = BindEmptyData();
        lvNoImagesProductsList.Items.Clear();
        lvNoImagesProductsList.DataSource = dtempty;
        lvNoImagesProductsList.DataBind();
        lvProducts.Items.Clear();
        lvProducts.DataSource = dtempty;
        lvProducts.DataBind();
        lvProducts.Visible = false;
        lvNoImagesProductsList.Visible = false;
        igTree.CssClass = "igdt_NodeGroup";
        DataTable dtCheckVal = new DataTable();
        dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

        if (dtCheckVal != null)
        {
            if (dtCheckVal.Rows.Count > 0)
            {
                dtCheckVal.Rows.Clear();
                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = dtCheckVal;
            }
        }

        TabContainer1.ActiveTab = tbSearch;
        lnkBreadCrumb.CssClass = "brd-crumbactive";

        lnkBreadCategory.CssClass = "brd-crumbaInctive";

        lnkBreadgroup.CssClass = "brd-crumbaInctive";
        lnkBreadCatValue.CssClass = "brd-crumbaInctive";

        Session[this.Mode.ToString() + "SearchContentIsHome"] = true;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// /
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void btnFind_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        //MDM - New search, so reset lvPaging ListView.
        lvPaging.Items.Clear();
        lvPaging.DataBind();

        Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;
        Session[this.Mode.ToString() + "SearchContentIsHome"] = null;

        BtnFindDetails();
        DeleteEmptyTreenodes();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void BtnFindDetails()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        //while prior login we will be 
        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 2;
            Session[this.Mode.ToString() + "priorLoginSearchValue"] = txtKeywordSearchValue.Text;
        }

        Control ctl = new Control();

        if (Mode == ModeType.Inventory)
        {
            ctl = this.Parent;
        }
        else if (Mode == ModeType.QuoteEditAddItems)
        {
            ctl = this.Parent.Parent;
        }

        Session[this.Mode.ToString() + "btnFind_ClickCalled"] = true;
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryList = new dsInventoryDataSet();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        Session[this.Mode.ToString() + "LastSearchExecuted"] = "Search";

        dsInventoryDataSet ds = (dsInventoryDataSet)dm.GetCache(this.Mode.ToString() + "dsInventoryDataSet");

        if (txtInventorySearchValue.Text.Length > 0 && txtKeywordSearchValue.Text.Length > 0)
        {
            lblHeaderError.Text = "ERROR: you can only search by keyword(s) OR part number.";
            lblHeaderError.Visible = true;

            lvProducts.Items.Clear();
            lvProducts.DataBind();
            lvNoImagesProductsList.Items.Clear();
            lvNoImagesProductsList.DataBind();
            ddlHeader.Visible = true;
            Label2.Visible = true;
            dvProdListView.Visible = true;
            lvPaging.Items.Clear();
            lvPaging.DataBind();
            lblpageno.Text = "";
            dvImageBanners.Visible = false;
            dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
        }
        else
        {
            executeSearch("SS");
            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

            DataSet dsInventory = (DataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];

            if (!DBNull.Value.Equals(dsInventory))
            {
                if (dsInventory.Tables.Count > 0)
                {
                    if (dsInventory.Tables["ttinventory"].Rows.Count > 0)
                    {
                        lblErrorMessage.Text = "";
                        BindProductGridViewDetails(dsInventory.Tables["ttinventory"], 1);

                        Label2.Visible = true;
                        dvImageBanners.Visible = false;
                        dvProdListView.Visible = true;
                        this.trSearch.Visible = true;
                        this.trSearch1.Visible = true;
                        this.trDetails.Visible = false;
                    }
                    else
                    {
                        if (TabContainer1.ActiveTab == tbSearch)
                        {
                            lblHeaderError.Text = NORESULTSFOUND;
                        }
                        else
                        {
                            lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
                        }

                        lblHeaderError.Visible = true;
                        lvProducts.Items.Clear();
                        lvProducts.DataBind();
                        lvNoImagesProductsList.Items.Clear();
                        lvNoImagesProductsList.DataBind();
                        dvImageBanners.Visible = false;
                        dvProdListView.Visible = true;
                        this.trSearch.Visible = true;
                        this.trSearch1.Visible = true;
                        this.trDetails.Visible = false;
                        Label2.Visible = true;
                        ddlHeader.Visible = true;
                        lvPaging.Visible = false;
                        lblpageno.Text = "";
                        dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
                    }

                    lnkBreadCategory.Visible = false;
                    lblBreadGroupSep.Visible = false;
                    lblBreadCatSep.Visible = false;
                    lnkBreadgroup.Visible = false;

                    if ((string)Session["GenericLogin"] == "True")
                    {
                        btnAddToQList.Visible = false;
                        btnAddAll2.Visible = false;
                        btnViewQuickList.Visible = false;
                        chkQuickList.Visible = false;
                        btnDetailAddToCart.Visible = false;
                        txtDetailItemQty.Visible = false;
                        lbltrdetailqty.Visible = false;
                        lblDetailMinPack.Visible = false;
                        btnAddXref.Visible = false;
                        pnlAddXref.Visible = false;
                        btnAdvViewQuickList.Visible = false;
                        this.NewQlistDiv.Visible = false;
                    }
                }
                else
                {
                    if (TabContainer1.ActiveTab == tbSearch)
                    {
                        lblHeaderError.Text = NORESULTSFOUND;
                    }
                    else
                    {
                        lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
                    }

                    lblHeaderError.Visible = true;
                    lvProducts.Items.Clear();
                    lvProducts.DataBind();
                    lvNoImagesProductsList.Items.Clear();
                    lvNoImagesProductsList.DataBind();
                    ddlHeader.Visible = true;
                    Label2.Visible = true;
                    dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
                }
            }
        }

        Infragistics.Web.UI.EditorControls.WebTextEditor txtSearch = new Infragistics.Web.UI.EditorControls.WebTextEditor();

        txtSearch = (Infragistics.Web.UI.EditorControls.WebTextEditor)ctl.FindControl("txtProductSearchKeyword");
        txtSearch.Text = "";

        Session[this.Mode.ToString() + "LastSearchExecuted"] = "Search";

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sName"></param>
    /// 
    public void FinDetails(string sName)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string strUserAgent = Request.UserAgent.ToString().ToLower();

        Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

        Control ctl = new Control();

        if (Mode == ModeType.Inventory)
        {
            ctl = this.Parent;
        }
        else if (Mode == ModeType.QuoteEditAddItems)
        {
            ctl = this.Parent.Parent;
        }

        if (!string.IsNullOrEmpty(sName))
        {
            GetRetainDetails();
            txtKeywordSearchValue.Text = sName;
            if (strUserAgent != null)
            {
                if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iPad") ||
                    strUserAgent.Contains("ipod"))
                {
                    txtKeywordSearchValue.FocusOnInitialization = false;
                }
                else if (Request.Browser.IsMobileDevice == true ||
                    strUserAgent.Contains("iphone") ||
                    strUserAgent.Contains("blackberry") ||
                    strUserAgent.Contains("mobile") ||
                    strUserAgent.Contains("windows ce") ||

                    strUserAgent.Contains("android"))
                {

                    txtKeywordSearchValue.FocusOnInitialization = false;
                    btnFind.Focus();
                }

                else
                {
                    txtKeywordSearchValue.Focus();
                }

            }

            txtInventorySearchValue.Text = "";
            cbxOnly.Checked = false;
            chkQuickList.Checked = false;
            CascadingDropDown1.SelectedValue = ":::";
            CascadingDropDown2.SelectedValue = ":::";
            _overrideProductGroupQuickList = true;
        }
        else
        {
            GetDetails();
        }

        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 2;
            Session[this.Mode.ToString() + "priorLoginSearchValue"] = txtKeywordSearchValue.Text;
        }

        TabContainer1.ActiveTab = tbAdvSearch;

        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryList = new dsInventoryDataSet();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        executeSearch("SS");

        dsInventoryDataSet ds = (dsInventoryDataSet)dm.GetCache(this.Mode.ToString() + "dsInventoryDataSet");

        DataSet dsInventory = (DataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];

        if (!DBNull.Value.Equals(dsInventory))
        {
            if (dsInventory.Tables.Count > 0)
            {
                if (dsInventory.Tables["ttinventory"].Rows.Count > 0)
                {
                    DataView dvFilter;
                    dvFilter = dsInventory.Tables["ttinventory"].DefaultView;
                    BindProductGridViewDetails(dsInventory.Tables["ttinventory"], 1);

                    Label2.Visible = true;
                    dvProdListView.Visible = true;
                    dvImageBanners.Visible = false;
                }
                else
                {
                    if (TabContainer1.ActiveTab == tbSearch)
                    {
                        lblHeaderError.Text = NORESULTSFOUND;
                    }
                    else
                    {
                        lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
                    }

                    lblHeaderError.Visible = true;
                    dvProdListView.Visible = true;
                    lvProducts.Items.Clear();
                    lvProducts.DataBind();
                    DataTable dtempty = new DataTable();
                    dtempty = BindEmptyData();
                    lvProducts.DataSource = dtempty;
                    lvProducts.DataBind();
                    lvNoImagesProductsList.DataSource = dtempty;
                    lvNoImagesProductsList.DataBind();
                    ddlHeader.Visible = true;
                    Label2.Visible = true;
                    lvPaging.Visible = false;
                    lblpageno.Visible = false;
                    dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
                }

                if ((string)Session["GenericLogin"] == "True")
                {
                    btnAddToQList.Visible = false;
                    btnAddAll2.Visible = false;
                    btnViewQuickList.Visible = false;
                    chkQuickList.Visible = false;
                    btnDetailAddToCart.Visible = false;
                    txtDetailItemQty.Visible = false;
                    lbltrdetailqty.Visible = false;
                    lblDetailMinPack.Visible = false;
                    btnAddXref.Visible = false;
                    pnlAddXref.Visible = false;
                    btnAdvViewQuickList.Visible = false;
                    this.NewQlistDiv.Visible = false;
                }

                dvImageBanners.Visible = false;
                dvProdListView.Visible = true;
                this.trSearch.Visible = true;
                this.trSearch1.Visible = true;
                this.trDetails.Visible = false;

                lblBreadGroupSep.Visible = false;
                lblBreadCatSep.Visible = false;
                lnkHome.Visible = false;
                lnkBreadCatValue.Visible = false;
                lnkBreadCategory.Visible = false;
                lnkBreadgroup.Visible = false;
                lnkBreadCrumb.Visible = true;
            }
        }

        _overrideProductGroupQuickList = false;
        Infragistics.Web.UI.EditorControls.WebTextEditor txtSearch = new Infragistics.Web.UI.EditorControls.WebTextEditor();

        txtSearch = (Infragistics.Web.UI.EditorControls.WebTextEditor)ctl.FindControl("txtProductSearchKeyword");
        txtSearch.Text = "";
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "PrRespFormat();", true);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Call this to clear bound data
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    /// <param name="customer">Current Customer Object</param>
    /// 
    public void Reset()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.trSearch.Visible = true;
        this.trSearch1.Visible = true;
        this.trDetails.Visible = false;

        gvInventoryAvailableTally.SelectedIndex = -1;
        gvOnOrderTran.SelectedIndex = -1;

        gvInventoryPricing.Visible = false;

        gvInventoryAvailableTotal.Visible = false;
        gvInventoryAvailableBranch.Visible = false;
        gvInventoryAvailableTally.Visible = false;
        gvInventoryAvailableTallyDetail.Visible = false;

        gvOnOrderTotal.Visible = false;
        gvOnOrderBranch.Visible = false;
        gvOnOrderTran.Visible = false;
        gvOnOrderTranDetail.Visible = false;

        lblErrorMessage.Text = "";

        //reset buttons
        btnAddAll2.Visible = false;

        Session[this.Mode.ToString() + "dsInventoryDataSet"] = null;

        this.AssignDataSource(Data.New);
        this.AssignPriceDataSource(Data.New);

        this.AssignAvailableTotalDataSource(Data.New);
        this.AssignAvailableBranchDataSource(Data.New);
        this.AssignAvailableTallyDataSource(Data.New);
        this.AssignAvailableTallyDetailDataSource(Data.New, "");

        this.AssignOnOrderTotalDataSource(Data.New);
        this.AssignOnOrderBranchDataSource(Data.New);
        this.AssignOnOrderTranDataSource(Data.New);
        this.AssignOnOrderTranDetailDataSource(Data.New, "");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void Reset(string branchID, string shiptoObj, bool nonsalable)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if ((dm.GetCache("currentBranchID") == null ||
            (dm.GetCache("currentBranchID") != null && (string)dm.GetCache("currentBranchID") != branchID)) ||
            (dm.GetCache("ShiptoRowForInformationPanel") == null ||
            (dm.GetCache("ShiptoRowForInformationPanel") != null && ((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["cust_shipto_obj"].ToString() != shiptoObj)))
        {
            this.Reset();
        }

        dm.SetCache("nonsalable", nonsalable);
        this.SetDefaultColumnVisibility(nonsalable);
        dm = null;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void SetDefaultColumnVisibility(bool visible)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataManager dm = new DataManager();

        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            SetVisibilityOfCart(false);
        }

        dm = null;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void SetVisibilityOfCart(bool visible)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Control parentTemplate = this.Parent.TemplateControl;
        Control parentparentTemplate = parentTemplate.Parent;
        Control parentparentparentTemplate = parentparentTemplate.Parent;

        if (parentparentparentTemplate != null)
        {
            Panel p = (Panel)parentparentparentTemplate.FindControl("CartPanel");
            if (p != null)
                p.Visible = visible;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// tree view Node list onclick event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void treenode_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string TreenodeVal = ((System.Web.UI.WebControls.TreeView)(sender)).SelectedNode.Value;
        Session[this.Mode.ToString() + "RetainPreviousUrl"] = TreenodeVal;
        string SelNodeText = ((System.Web.UI.WebControls.TreeView)(sender)).SelectedNode.Text;
        SelectedNodeBind(TreenodeVal, SelNodeText);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void treenodeChecked_Click(object sender, TreeNodeEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string TreenodeVal = ((System.Web.UI.WebControls.TreeView)(sender)).SelectedNode.Value;
        Session[this.Mode.ToString() + "RetainPreviousUrl"] = TreenodeVal;
        string SelNodeText = ((System.Web.UI.WebControls.TreeView)(sender)).SelectedNode.Text;
        SelectedNodeBind(TreenodeVal, SelNodeText);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    private DataSet GetFetchItem(string sGroup, string sAttributeLabel, string sAttributeValue)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataManager _dataManager = new DataManager();

        dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");
        dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dataManager.GetCache("dsPVParam");
        dsCustomerDataSet dsCU = (dsCustomerDataSet)_dataManager.GetCache("dsCU");
        string searchCriteria = this.HeaderBuildSearchCriteria("ss");

        _dataManager.SetCache(this.Mode.ToString() + "InventorySearchCriteria", searchCriteria);

        _dataManager.SetCache("dsCustomerDataSet", dsCU);
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dataManager.GetCache("dsPV");

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        AgilityEntryNetUser user = (AgilityEntryNetUser)_dataManager.GetCache("AgilityEntryNetUser");

        Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string customer = cust.CustomerKey;
        cust.Dispose();

        Dmsi.Agility.Data.Shipto shipTo = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        int shipto = shipTo.ShiptoSequence;
        shipTo.Dispose();
        string branch = "Branch=" + (string)_dataManager.GetCache("currentBranchID");

        StoreCustBranchTable(dsCU);

        foreach (dsPVParamDataSet.ttparam_pvRow paramrow in dsPVParam.ttparam_pv.Rows)
        {
            user.AddContext(paramrow.property_name, paramrow.property_value);
        }

        foreach (dsSessionMgrDataSet.ttSelectedPropertyRow row in dsSessionManager.ttSelectedProperty.Rows)
        {
            // MDM user.AddContext(row.propertyName, row.propertyValue, row.contextID);
            user.AddContext(row.propertyName, row.propertyValue);
        }

        string default_cust_key = customer;
        int default_shipto_seq = shipto;

        Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet dsSelectedAttribs = new Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet();
        DataRow drSelAttributes = dsSelectedAttribs.ttSelectedAttributes.NewRow();
        drSelAttributes["SelectedLabel"] = sAttributeLabel;
        drSelAttributes["SelectedValue"] = sAttributeValue;
        dsSelectedAttribs.ttSelectedAttributes.Rows.Add(drSelAttributes);
        dsSelectedAttribs.AcceptChanges();
        dsSelectedAttribs.GetChanges();

        //CustomerID(cust_key) and CustomerShipToSequence are being read from defaults, above...
        decimal dMarkupFactor = 1;

        if (Session["MarkupFactor"] != null)
        {
            dMarkupFactor = (decimal)Session["MarkupFactor"];
        }

        string saleType = "";

        DataManager dm = new DataManager();

        if (Mode == ModeType.Inventory)
        {
            if (dm.GetCache("SaleType") != null)
                saleType = (string)dm.GetCache("SaleType");
            else
                saleType = "<ALL>";
        }

        if (Mode == ModeType.QuoteEditAddItems)
        {
            dsQuoteDataSet dsQuote = new dsQuoteDataSet();

            if (Session["QuoteEdit_dsQuote"] != null)
            {
                dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
                saleType = dsQuote.pvttquote_header[0]["sale_type"].ToString();
            }
            else
                saleType = "<ALL>";
        }

        string retailMode = "";

        if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
            dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - discount from list")
        {
            retailMode = "List";
        }

        if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
            dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - markup on cost")
        {
            retailMode = "Net";
        }

        inventory.FetchItemsByAttribute(sGroup, default_cust_key, default_shipto_seq, saleType, retailMode, dMarkupFactor, dsSelectedAttribs, out ReturnCode, out MessageText);

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList = new dsInventoryDataSet();
        inventoryDetailList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet)inventory.ReferencedDataSet;

        Session[this.Mode.ToString() + "CachedInventoryDataSet"] = inventoryDetailList;
        Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = inventoryDetailList;

        dm.SetCache(this.Mode.ToString() + "InventorySearchCriteria", searchCriteria);
        dm.SetCache(this.Mode.ToString() + "dsInventoryDataSet", inventoryDetailList);

        dm.SetCache("dsCU", dsCU);
        dm.SetCache("dsPV", dsPV);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return inventory.ReferencedDataSet;
    }

    private DataSet GetfetchMultipleItems(string sGroup, Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet dsSelectedAttribs)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        //if it is prior login we are saving data because again if user loging with same account then we need to maintain same session
        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "dsSelectedAttribsPriorLogin"] = dsSelectedAttribs;
            Session[this.Mode.ToString() + "PriorLogin_Group"] = sGroup;
            Session[this.Mode.ToString() + "PriorLoginBreadCrumb_GroupDetails"] = lnkBreadgroup.CommandArgument;
        }

        DataManager _dataManager = new DataManager();

        dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");
        dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dataManager.GetCache("dsPVParam");
        dsCustomerDataSet dsCU = (dsCustomerDataSet)_dataManager.GetCache("dsCU");
        string searchCriteria = this.HeaderBuildSearchCriteria("SS");

        _dataManager.SetCache(this.Mode.ToString() + "InventorySearchCriteria", searchCriteria);

        _dataManager.SetCache("dsCustomerDataSet", dsCU);
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dataManager.GetCache("dsPV");

        _dataManager.SetCache(this.Mode.ToString() + "InventorySearchCriteria", searchCriteria);

        _dataManager.SetCache("dsCustomerDataSet", dsCU);

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        AgilityEntryNetUser user = (AgilityEntryNetUser)_dataManager.GetCache("AgilityEntryNetUser");

        Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string customer = cust.CustomerKey;

        Dmsi.Agility.Data.Shipto shipTo = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        int shipto = shipTo.ShiptoSequence;

        string branch = "Branch=" + (string)_dataManager.GetCache("currentBranchID");

        StoreCustBranchTable(dsCU);

        foreach (dsPVParamDataSet.ttparam_pvRow paramrow in dsPVParam.ttparam_pv.Rows)
        {
            user.AddContext(paramrow.property_name, paramrow.property_value);
        }

        foreach (dsSessionMgrDataSet.ttSelectedPropertyRow row in dsSessionManager.ttSelectedProperty.Rows)
        {
            // MDM user.AddContext(row.propertyName, row.propertyValue, row.contextID);
            user.AddContext(row.propertyName, row.propertyValue);
        }

        string default_cust_key = customer;
        int default_shipto_seq = shipto;

        decimal dMarkupFactor = 1;

        if (Session["MarkupFactor"] != null)
        {
            dMarkupFactor = (decimal)Session["MarkupFactor"];
        }

        string saleType = "";

        DataManager dm = new DataManager();

        if (Mode == ModeType.Inventory)
        {
            if (dm.GetCache("SaleType") != null)
                saleType = (string)dm.GetCache("SaleType");
            else
                saleType = "<ALL>";
        }

        if (Mode == ModeType.QuoteEditAddItems)
        {
            dsQuoteDataSet dsQuote = new dsQuoteDataSet();

            if (Session["QuoteEdit_dsQuote"] != null)
            {
                dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
                saleType = dsQuote.pvttquote_header[0]["sale_type"].ToString();
            }
            else
                saleType = "<ALL>";
        }

        string retailMode = "";

        if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
            dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - discount from list")
        {
            retailMode = "List";
        }

        if (dm.GetCache("AllowListBasedRetailPrice") != null && ((string)dm.GetCache("AllowListBasedRetailPrice")).ToLower() == "yes" &&
            dm.GetCache("UserPref_DisplaySetting") != null && (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - markup on cost")
        {
            retailMode = "Net";
        }

        inventory.FetchItemsByAttribute(sGroup, default_cust_key, default_shipto_seq, saleType, retailMode, dMarkupFactor, dsSelectedAttribs, out ReturnCode, out MessageText);

        if (ReturnCode != 0)
        {
            if ((string)Session["GenericLogin"] == "True")
            {
                Session.RemoveAll();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("~/Logon.aspx");
            }
        }

        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList = new dsInventoryDataSet();
        inventoryDetailList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet)inventory.ReferencedDataSet;

        Session[this.Mode.ToString() + "CachedInventoryDataSet"] = inventoryDetailList;
        Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = inventoryDetailList;

        dm.SetCache("dsCU", dsCU);
        dm.SetCache("dsPV", dsPV);

        dm.SetCache(this.Mode.ToString() + "InventorySearchCriteria", searchCriteria);
        dm.SetCache(this.Mode.ToString() + "dsInventoryDataSet", inventoryDetailList);

        cust.Dispose();
        shipTo.Dispose();


#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return inventory.ReferencedDataSet;
    }

    private void StoreCustBranchTable(dsCustomerDataSet dsCU)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        dsCustomerDataSet.ttbranch_custDataTable table = new dsCustomerDataSet.ttbranch_custDataTable();

        foreach (dsCustomerDataSet.ttbranch_custRow row in dsCU.ttbranch_cust.Rows)
        {
            table.LoadDataRow(row.ItemArray, true);
        }

        Session["ttbranch_cust"] = table;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void ClearProductTextboxes()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        TextBox enteredQtyTextBox;
        ListViewItem row;

        for (int i = 0; i < lvProducts.Items.Count; i++)
        {
            HiddenField hdnItem = (HiddenField)lvProducts.Items[i].FindControl("hdnItem");
            row = lvProducts.Items[i];
            enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

            if (enteredQtyTextBox.Text == "")
                continue;

            if (Mode == ModeType.Inventory)
                hdnAddCart.Value = "0";

            if (Mode == ModeType.QuoteEditAddItems)
                hdnAddCart_QEAI.Value = "0";

            enteredQtyTextBox.Text = "";
        }

        for (int i = 0; i < this.lvNoImagesProductsList.Items.Count; i++)
        {
            HiddenField hdnItem = (HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnItem");
            row = lvNoImagesProductsList.Items[i];
            enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

            if (enteredQtyTextBox.Text == "")
                continue;

            if (Mode == ModeType.Inventory)
                hdnAddCart.Value = "0";

            if (Mode == ModeType.QuoteEditAddItems)
                hdnAddCart_QEAI.Value = "0";

            enteredQtyTextBox.Text = "";
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void btnAddAll_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (ValidateTextboxesInProductLists())
        {
            DataManager dm = new DataManager();
            dsInventoryDataSet ds = (dsInventoryDataSet)dm.GetCache(this.Mode.ToString() + "dsInventoryDataSet");
            dsInventoryDataSet dsCopyForComplementaryInput = ds.Copy() as dsInventoryDataSet;
            dsCopyForComplementaryInput.ttinventory.Clear();
            dsCopyForComplementaryInput.AcceptChanges();

            string SearchCriteraForComplementaryFetch = (string)dm.GetCache(this.Mode.ToString() + "InventorySearchCriteria");

            if (SearchCriteraForComplementaryFetch.Contains("SearchType="))
            {
                string SearchCriteriaLeft = SearchCriteraForComplementaryFetch.Substring(0, SearchCriteraForComplementaryFetch.IndexOf("SearchType="));
                string SearchCriteriaRight = SearchCriteraForComplementaryFetch.Substring(SearchCriteraForComplementaryFetch.IndexOf("SearchTran="));
                SearchCriteraForComplementaryFetch = SearchCriteriaLeft + "SearchType=CompItems\x0003" + SearchCriteriaRight;
            }
            else
            {
                SearchCriteraForComplementaryFetch = SearchCriteraForComplementaryFetch + "\x0003SearchType=CompItems";
            }

            dsQuoteDataSet dsQuoteCurrentQuote = new dsQuoteDataSet();
            dsQuoteDataSet dsQuoteAddNewItems = new dsQuoteDataSet();

            if (Mode == ModeType.QuoteEditAddItems)
            {
                if (Session["QuoteEdit_dsQuote"] != null)
                {
                    dsQuoteCurrentQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
                    dsQuoteAddNewItems = dsQuoteCurrentQuote.Copy() as dsQuoteDataSet;
                    dsQuoteAddNewItems.pvttquote_detail.Clear();
                    dsQuoteAddNewItems.AcceptChanges();
                }
            }

            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

            string item, size, description, uom, priceUom, qty_uom_conv_ptr_sysid;
            int qty_uom_conv_ptr;
            decimal quantity, price, minpack, extension;
            decimal thickness, width, length;
            TextBox enteredQtyTextBox;
            bool minimumQtyViolation;
            ListViewItem row;

            lblErrorMessage.Text = "";
            dvProdListView.Visible = true;
            dvImageBanners.Visible = false;

            if (Mode == ModeType.Inventory)
            {
                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Product Results - Add to Cart Button";
                aInput.Label = "Mode = Net";
                aInput.Value = "0";

                if (Session["UserPref_DisplaySetting"] != null &&
                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                {
                    aInput.Label = "Mode = Retail";
                }

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();
            }

            for (int i = 0; i < lvProducts.Items.Count; i++)
            {
                HiddenField hdnItem = (HiddenField)lvProducts.Items[i].FindControl("hdnItem");
                row = lvProducts.Items[i];
                enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

                if (enteredQtyTextBox.Text.Trim().Length == 0)
                    continue;

                if (Mode == ModeType.Inventory)
                    hdnAddCart.Value = "0";

                if (Mode == ModeType.QuoteEditAddItems)
                    hdnAddCart_QEAI.Value = "0";

                try
                {
                    quantity = Decimal.Parse(enteredQtyTextBox.Text);
                }
                catch
                {
                    if (enteredQtyTextBox.Text != "")
                        lblErrorMessage.Text = "Item " + hdnItem.Value + " has invalid quantity.";

                    continue;
                }

                quantity = Decimal.Parse(enteredQtyTextBox.Text);

                //MDM - Need to fetch new UOM data here...
                DropDownList ddluom = lvProducts.Items[i].FindControl("ddlUOM") as DropDownList;
                string itemtype = (lvProducts.Items[i].FindControl("hdnItemType") as HiddenField).Value;

                dsInventoryDataSet dsCopy = new dsInventoryDataSet();

                if (!(itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good"))
                    dsCopy = FetchAllDataForChangedUOM("cart", hdnItem.Value, Convert.ToInt32(ddluom.SelectedItem.Value));

                decimal uomConvFactor = 0;

                if (dsCopy.ttinventory.Rows.Count > 0)
                {
                    DataRow[] rows = dsCopy.ttitem_uomconv.Select("uom_ptr = " + Convert.ToInt32(ddluom.SelectedItem.Value));

                    if (rows.Length == 1)
                    {
                        uomConvFactor = Convert.ToDecimal(rows[0]["conv_factor"]);
                    }
                }

                HiddenField hdnminpack = new HiddenField();
                HiddenField hdnMinpack = new HiddenField();
                HiddenField hdnUomVal = new HiddenField();
                HiddenField hdnRetPrice = new HiddenField();
                HiddenField hdnPriceVal = new HiddenField();

                if (dsCopy.ttinventory.Rows.Count == 0)
                {
                    hdnminpack.Value = ((HiddenField)lvProducts.Items[i].FindControl("hdnminpackDetails")).Value;
                    hdnMinpack.Value = ((HiddenField)lvProducts.Items[i].FindControl("hdnminpackDetails")).Value;
                    hdnUomVal.Value = ((HiddenField)lvProducts.Items[i].FindControl("hdnStockinguom")).Value;
                    hdnPriceVal.Value = ((HiddenField)lvProducts.Items[i].FindControl("hdnPrice")).Value;
                    hdnRetPrice.Value = ((HiddenField)lvProducts.Items[i].FindControl("hdnRetailprice")).Value;

                    qty_uom_conv_ptr = (Int32)lvProducts.DataKeys[i]["qty_uom_conv_ptr"];
                    qty_uom_conv_ptr_sysid = (string)lvProducts.DataKeys[i]["qty_uom_conv_ptr_sysid"];
                }
                else
                {
                    hdnminpack.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                    hdnMinpack.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                    hdnUomVal.Value = (string)dsCopy.ttinventory.Rows[0]["qty_uom"];
                    hdnPriceVal.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["price_disp"]);
                    hdnRetPrice.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["retail_price_disp"]);

                    qty_uom_conv_ptr = 0;

                    try
                    {
                        qty_uom_conv_ptr = Convert.ToInt32(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr"]);
                    }
                    catch { }

                    qty_uom_conv_ptr_sysid = "";

                    try
                    {
                        qty_uom_conv_ptr_sysid = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr_sysid"]);
                    }
                    catch { }
                }

                HiddenField hdnsize = (HiddenField)lvProducts.Items[i].FindControl("hdnsize");

                minimumQtyViolation = false;

                if (Convert.ToDecimal(hdnminpack.Value) > 0)
                {
                    if (Convert.ToDecimal(hdnminpack.Value) > 0 && Convert.ToDecimal(hdnminpack.Value) < 1)
                    {
                        if (!quantity.ToString().Contains("."))
                        {
                            minimumQtyViolation = false;
                        }
                        else
                        {
                            decimal decimalPortion = Convert.ToDecimal(quantity.ToString().Substring(quantity.ToString().IndexOf(".")));
                            minimumQtyViolation = Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(decimalPortion), 0), 4) % Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(hdnminpack.Value), 0), 4) > 0;
                        }
                    }
                    else
                    {
                        if (!quantity.ToString().Contains("."))
                        {
                            minimumQtyViolation = quantity % Convert.ToDecimal(hdnminpack.Value) > 0;
                        }
                        else
                        {
                            minimumQtyViolation = quantity % Decimal.Round(Convert.ToDecimal(1) / uomConvFactor, 4) > 0;
                        }
                    }
                }

                if (!minimumQtyViolation)
                {
                    item = lvProducts.DataKeys[i]["ITEM"].ToString();
                    thickness = (decimal)lvProducts.DataKeys[i]["thickness"];
                    width = (decimal)lvProducts.DataKeys[i]["WIDTH"];
                    length = (decimal)lvProducts.DataKeys[i]["LENGTH"];

                    size = hdnsize.Value;
                    LinkButton lblDescVal = (LinkButton)lvProducts.Items[i].FindControl("lbtProdDesc");
                    description = lblDescVal.Text;

                    uom = hdnUomVal.Value;

                    if (_viewInvPrices)
                    {
                        if (Session["UserPref_DisplaySetting"] != null &&
                           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                        {
                            price = Convert.ToDecimal(hdnRetPrice.Value);
                        }
                        else
                            price = Convert.ToDecimal(hdnPriceVal.Value);
                    }
                    else
                    {
                        price = 0;
                    }

                    if (itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good")
                        priceUom = uom;
                    else if (dsCopy.ttinventory.Rows.Count > 0)
                        priceUom = (string)dsCopy.ttinventory.Rows[0]["price_uom_disp"];
                    else
                        priceUom = uom;

                    minpack = Convert.ToDecimal(hdnMinpack.Value);
                    extension = price * quantity;

                    if (Mode == ModeType.Inventory)
                    {
                        CartAdd(quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length);
                    }

                    if (Mode == ModeType.QuoteEditAddItems)
                    {
                        QuoteDataSetAddItem(ref dsQuoteAddNewItems, quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length);
                    }
                }

                if (minimumQtyViolation)
                {
                    lblErrorMessage.Text = "Item " + hdnItem.Value + " must be ordered in multiples of " + hdnMinpack.Value + " " + hdnUomVal.Value + ".";
                }

                if (quantity <= 0 || minimumQtyViolation)
                {

#if DEBUG
        TimeSpan elapsedtimeReturn = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeReturn, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
                    return;
                }
            }

            for (int i = 0; i < lvNoImagesProductsList.Items.Count; i++)
            {
                HiddenField hdnItem = (HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnItem");
                row = lvNoImagesProductsList.Items[i];
                enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

                if (enteredQtyTextBox.Text.Trim().Length == 0)
                    continue;

                if (Mode == ModeType.Inventory)
                    hdnAddCart.Value = "0";

                if (Mode == ModeType.QuoteEditAddItems)
                    hdnAddCart_QEAI.Value = "0";

                try
                {
                    quantity = Decimal.Parse(enteredQtyTextBox.Text);
                }
                catch
                {
                    if (enteredQtyTextBox.Text != "")
                        lblErrorMessage.Text = "Item " + hdnItem.Value + " has invalid quantity.";

                    continue;
                }

                quantity = Decimal.Parse(enteredQtyTextBox.Text);

                //MDM - Need to fetch new UOM data here...
                DropDownList ddluom = lvNoImagesProductsList.Items[i].FindControl("ddlUOM2") as DropDownList;
                string itemtype = (lvNoImagesProductsList.Items[i].FindControl("hdnItemType") as HiddenField).Value;

                dsInventoryDataSet dsCopy = new dsInventoryDataSet();

                if (!(itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good"))
                    dsCopy = FetchAllDataForChangedUOM("cart", hdnItem.Value, Convert.ToInt32(ddluom.SelectedItem.Value));

                decimal uomConvFactor = 0;

                if (dsCopy.ttinventory.Rows.Count > 0)
                {
                    DataRow[] rows = dsCopy.ttitem_uomconv.Select("uom_ptr = " + Convert.ToInt32(ddluom.SelectedItem.Value));

                    if (rows.Length == 1)
                    {
                        uomConvFactor = Convert.ToDecimal(rows[0]["conv_factor"]);
                    }
                }

                HiddenField hdnminpack = new HiddenField();
                HiddenField hdnMinpack = new HiddenField();
                HiddenField hdnUomVal = new HiddenField();
                HiddenField hdnRetPrice = new HiddenField();
                HiddenField hdnPriceVal = new HiddenField();

                if (dsCopy.ttinventory.Rows.Count == 0)
                {
                    hdnminpack.Value = ((HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnminpackDetails")).Value;
                    hdnMinpack.Value = ((HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnminpackDetails")).Value;
                    hdnUomVal.Value = ((HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnStockinguom")).Value;
                    hdnPriceVal.Value = ((HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnPrice")).Value;
                    hdnRetPrice.Value = ((HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnRetailprice")).Value;

                    qty_uom_conv_ptr = (Int32)lvNoImagesProductsList.DataKeys[i]["qty_uom_conv_ptr"];
                    qty_uom_conv_ptr_sysid = (string)lvNoImagesProductsList.DataKeys[i]["qty_uom_conv_ptr_sysid"];
                }
                else
                {
                    hdnminpack.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                    hdnMinpack.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                    hdnUomVal.Value = (string)dsCopy.ttinventory.Rows[0]["qty_uom"];
                    hdnPriceVal.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["price_disp"]);
                    hdnRetPrice.Value = Convert.ToString(dsCopy.ttinventory.Rows[0]["retail_price_disp"]);

                    qty_uom_conv_ptr = 0;

                    try
                    {
                        qty_uom_conv_ptr = Convert.ToInt32(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr"]);
                    }
                    catch { }

                    qty_uom_conv_ptr_sysid = "";

                    try
                    {
                        qty_uom_conv_ptr_sysid = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr_sysid"]);
                    }
                    catch { }
                }

                HiddenField hdnsize = (HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnsize");

                minimumQtyViolation = false;

                if (Convert.ToDecimal(hdnminpack.Value) > 0)
                {
                    if (Convert.ToDecimal(hdnminpack.Value) > 0 && Convert.ToDecimal(hdnminpack.Value) < 1)
                    {
                        if (!quantity.ToString().Contains("."))
                        {
                            minimumQtyViolation = false;
                        }
                        else
                        {
                            decimal decimalPortion = Convert.ToDecimal(quantity.ToString().Substring(quantity.ToString().IndexOf(".")));
                            minimumQtyViolation = Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(decimalPortion), 0), 4) % Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(hdnminpack.Value), 0), 4) > 0;
                        }
                    }
                    else
                    {
                        if (!quantity.ToString().Contains("."))
                        {
                            minimumQtyViolation = quantity % Convert.ToDecimal(hdnminpack.Value) > 0;
                        }
                        else
                        {
                            minimumQtyViolation = quantity % Decimal.Round(Convert.ToDecimal(1) / uomConvFactor, 4) > 0;
                        }
                    }
                }

                if (!minimumQtyViolation)
                {
                    item = lvNoImagesProductsList.DataKeys[i]["ITEM"].ToString();
                    thickness = (decimal)lvNoImagesProductsList.DataKeys[i]["thickness"];
                    width = (decimal)lvNoImagesProductsList.DataKeys[i]["WIDTH"];
                    length = (decimal)lvNoImagesProductsList.DataKeys[i]["LENGTH"];

                    size = hdnsize.Value;
                    LinkButton lblDescVal = (LinkButton)lvNoImagesProductsList.Items[i].FindControl("lbtProdDesc");
                    description = lblDescVal.Text;

                    uom = hdnUomVal.Value;

                    if (_viewInvPrices)
                    {
                        if (Session["UserPref_DisplaySetting"] != null &&
                           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                        {
                            price = Convert.ToDecimal(hdnRetPrice.Value);
                        }
                        else
                            price = Convert.ToDecimal(hdnPriceVal.Value);
                    }
                    else
                    {
                        price = 0;
                    }

                    if (itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good")
                        priceUom = uom;
                    else if (dsCopy.ttinventory.Rows.Count > 0)
                        priceUom = (string)dsCopy.ttinventory.Rows[0]["price_uom_disp"];
                    else
                        priceUom = uom;

                    minpack = Convert.ToDecimal(hdnMinpack.Value);
                    extension = price * quantity;

                    if (Mode == ModeType.Inventory)
                    {
                        CartAdd(quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length);
                    }

                    if (Mode == ModeType.QuoteEditAddItems)
                    {
                        QuoteDataSetAddItem(ref dsQuoteAddNewItems, quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length);
                    }
                }

                if (minimumQtyViolation)
                {
                    lblErrorMessage.Text = "Item " + hdnItem.Value + " must be ordered in multiples of " + hdnMinpack.Value + " " + hdnUomVal.Value + ".";
                }

                if (quantity <= 0 || minimumQtyViolation)
                {

#if DEBUG
        TimeSpan elapsedtimeReturn = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeReturn, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
                    return;
                }
            }

            bool OkToClearTextBoxes = true;

            if (Mode == ModeType.QuoteEditAddItems)
            {
                decimal markupFactorValue = 0;
                string retailMode = "Net price";

                if (Session["UserPref_DisplaySetting"] != null &&
                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                {
                    retailMode = (string)Session["UserPref_DisplaySetting"];

                    if (Session["MarkupFactor"] != null)
                    {
                        markupFactorValue = Convert.ToDecimal(Session["MarkupFactor"]);
                    }
                }

                if (dsQuoteAddNewItems.pvttquote_detail.Rows.Count > 0)
                {
                    foreach (dsQuoteDataSet.pvttquote_detailRow dtRow in dsQuoteAddNewItems.pvttquote_detail.Rows)
                    {
                        dtRow["retail_mode"] = retailMode;
                    }

                    dsQuoteAddNewItems.AcceptChanges();

                    Quotes q = new Quotes();
                    string messageText = q.AddQuoteDetailItem(markupFactorValue, dsQuoteAddNewItems);

                    if (messageText != "0")
                    {
                        if (messageText.Contains("979"))
                        {
                            Session["LoggedIn"] = null;
                            Response.Redirect("~/Logon.aspx");
                        }

                        lblErrorMessage.Text = messageText;
                        OkToClearTextBoxes = false;
                    }
                }
            }

            //MDM - Figure out if we need to call for complementary items.
            if (OkToClearTextBoxes)
            {
                for (int i = 0; i < lvProducts.Items.Count; i++)
                {
                    row = lvProducts.Items[i];
                    TextBox QtyTextBox = (TextBox)row.FindControl("txtOrder");

                    item = lvProducts.DataKeys[i]["ITEM"].ToString();
                    thickness = (decimal)lvProducts.DataKeys[i]["thickness"];
                    width = (decimal)lvProducts.DataKeys[i]["WIDTH"];
                    length = (decimal)lvProducts.DataKeys[i]["LENGTH"];

                    if (QtyTextBox.Text.Length > 0)
                    {
                        Label l = (Label)row.FindControl("lblPartNo");
                        string itemText = l.Text;
                        DataRow[] rows = ds.ttinventory.Select("ITEM='" + itemText + "' AND thickness=" + thickness + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString());

                        if (rows.Length == 1)
                        {
                            if ((bool)rows[0]["display_comp_items_pv"] == true)
                            {
                                DataRow newRow = dsCopyForComplementaryInput.ttinventory.NewRow();
                                newRow.ItemArray = rows[0].ItemArray;
                                dsCopyForComplementaryInput.ttinventory.Rows.Add(newRow);
                            }
                        }
                    }
                }

                for (int i = 0; i < lvNoImagesProductsList.Items.Count; i++)
                {
                    row = lvNoImagesProductsList.Items[i];
                    TextBox QtyTextBox = (TextBox)row.FindControl("txtOrder");

                    item = lvNoImagesProductsList.DataKeys[i]["ITEM"].ToString();
                    thickness = (decimal)lvNoImagesProductsList.DataKeys[i]["thickness"];
                    width = (decimal)lvNoImagesProductsList.DataKeys[i]["WIDTH"];
                    length = (decimal)lvNoImagesProductsList.DataKeys[i]["LENGTH"];

                    if (QtyTextBox.Text.Length > 0)
                    {
                        Label l = (Label)row.FindControl("lblPartNo");
                        string itemText = l.Text;
                        DataRow[] rows = ds.ttinventory.Select("ITEM='" + itemText + "' AND thickness=" + thickness + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString());

                        if (rows.Length == 1)
                        {
                            if ((bool)rows[0]["display_comp_items_pv"] == true)
                            {
                                DataRow newRow = dsCopyForComplementaryInput.ttinventory.NewRow();
                                newRow.ItemArray = rows[0].ItemArray;
                                dsCopyForComplementaryInput.ttinventory.Rows.Add(newRow);
                            }
                        }
                    }
                }

                dsCopyForComplementaryInput.AcceptChanges();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
                if (dsCopyForComplementaryInput.ttinventory.Rows.Count > 0)
                {
                    AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

                    int ReturnCode;
                    string MessageText;

                    Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

                    if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    inv.FetchItemsComplementaryItems(SearchCriteraForComplementaryFetch, (dsPartnerVuDataSet)dm.GetCache("dsPV"), ref dsCopyForComplementaryInput, out ReturnCode, out MessageText);

                    if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    Session["dsInventoryComplementaryItems"] = dsCopyForComplementaryInput;

                    if (dsCopyForComplementaryInput.ttinventory.Rows.Count > 0)
                    {
                        if (this.Mode == ModeType.Inventory)
                        {
                            /* Analytics Tracking */
                            AnalyticsInput aInput = new AnalyticsInput();

                            aInput.Type = "event";
                            aInput.Action = "Complementary Items - Add to Cart Button";
                            aInput.Label = "";
                            aInput.Value = "0";

                            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                            am.BeginTrackEvent();

                            Parent.FindControl("ComplementaryItems").Visible = true;
                            Session["ComplementarySource"] = "Inventory";
                            Session["BlockPopupMenu"] = true;
                            this.Visible = false;

                            MenuEnableDisable menuDisable = new MenuEnableDisable();
                            menuDisable.DisableMenuItems(this.Page, this.Parent);

                            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
                            {
                                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                                {
                                    if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                                    {
                                        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                                    }
                                }
                            }
                        }

                        if (this.Mode == ModeType.QuoteEditAddItems)
                        {
                            Parent.Parent.FindControl("ComplementaryItems").Visible = true;
                            Session["ComplementarySource"] = "QuoteEditAddItems";
                            this.Parent.Visible = false;
                        }
                    }
                }
            }

            //MDM - Clear values in textboxes *LAST*...
            if (OkToClearTextBoxes)
            {
                for (int i = 0; i < lvProducts.Items.Count; i++)
                {
                    row = lvProducts.Items[i];
                    enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");
                    enteredQtyTextBox.Text = "";
                }

                for (int i = 0; i < lvNoImagesProductsList.Items.Count; i++)
                {
                    row = lvNoImagesProductsList.Items[i];
                    enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");
                    enteredQtyTextBox.Text = "";
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private bool ValidateTextboxesInProductLists()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        bool isValid = true;

        if (lvProducts.Items.Count > 0)
        {
            for (int i = 0; i < lvProducts.Items.Count; i++)
            {
                HiddenField hdnItem = (HiddenField)lvProducts.Items[i].FindControl("hdnItem");
                HiddenField lbluomVal = (HiddenField)lvProducts.Items[i].FindControl("hdnStockinguom");
                ListViewItem row = lvProducts.Items[i];
                Label lblMinPack = (Label)row.FindControl("lblMinPack");
                TextBox enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

                if (enteredQtyTextBox.Text.Trim().Length > 0) // if there is no text in the quantity input textbox, then loop.
                {
                    decimal parsedDecimal = 0;
                    bool parsed = Decimal.TryParse(enteredQtyTextBox.Text, out parsedDecimal);

                    if (!parsed)
                    {
                        isValid = false;
                        lblErrorMessage.Text = "Item " + hdnItem.Value + " has invalid quantity.";
                        break;
                    }
                    else if (parsedDecimal <= 0)
                    {
                        isValid = false;
                        lblErrorMessage.Text = "Item " + hdnItem.Value + "; zero or negative values are not valid.";
                        break;
                    }
                }
            }
        }

        if (lvNoImagesProductsList.Items.Count > 0)
        {
            for (int i = 0; i < lvNoImagesProductsList.Items.Count; i++)
            {
                HiddenField hdnItem = (HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnItem");
                HiddenField lbluomVal = (HiddenField)lvNoImagesProductsList.Items[i].FindControl("hdnStockinguom");
                ListViewItem row = lvNoImagesProductsList.Items[i];
                Label lblMinPack = (Label)row.FindControl("lblMinPack");
                TextBox enteredQtyTextBox = (TextBox)row.FindControl("txtOrder");

                if (enteredQtyTextBox.Text.Trim().Length > 0) // if there is no text in the quantity input textbox, then loop.
                {
                    decimal parsedDecimal = 0;
                    bool parsed = Decimal.TryParse(enteredQtyTextBox.Text, out parsedDecimal);

                    if (!parsed)
                    {
                        isValid = false;
                        lblErrorMessage.Text = "Item " + hdnItem.Value + " has invalid quantity.";
                        break;
                    }
                    else if (parsedDecimal <= 0)
                    {
                        isValid = false;
                        lblErrorMessage.Text = "Item " + hdnItem.Value + "; zero or negative values are not valid.";
                        break;
                    }
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return isValid;
    }

    /// <summary>
    /// for viewing quick list details 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void btnViewQuickList_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif


        Session["ViewQuickListHasBeenClicked"] = true;
        Session[this.Mode.ToString() + "SearchContentIsHome"] = null;

        ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
        pc.ddlQuicklistType_SelectedIndex = ddlQuicklistType.SelectedIndex;
        pc.Save();

        Button bt = sender as Button;

        if (bt != null && sender != null && e != null)
        {
            AnalyticsInput aInput = new AnalyticsInput();
            aInput.Type = "event";
            aInput.Action = "View Quicklist Click - Top Right";
            aInput.Label = ddlQuicklistType.SelectedItem.ToString();
            aInput.Value = "0";

            if (bt.ID == "btnAdvViewQuickList2")
            {
                aInput.Action = "View Quicklist Click - Bottom Left";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

        if (Mode == ModeType.Inventory)
            hdnAddCart.Value = "0";

        if (Mode == ModeType.QuoteEditAddItems)
            hdnAddCart_QEAI.Value = "0";

        Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 1;

        //for maintaining previous state
        Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;
        CascadingDropDown1.SelectedValue = ":::";

        // Global Variable to override dropdownlist value that is
        // not updated until client html is refreshed
        _overrideProductGroupQuickList = true;
        cbxOnly.Checked = false;

        bool isQuicklist = chkQuickList.Checked;
        chkQuickList.Checked = true;
        txtInventorySearchValue.Text = "";
        txtKeywordSearchValue.Text = "";
        LoadQuickListDetails();
        BtnFindDetails();

        _overrideProductGroupQuickList = false;
        chkQuickList.Checked = isQuicklist;
        igTree.CssClass = "treelink";
        Session[this.Mode.ToString() + "LastSearchExecuted"] = "QuickList";

        if (TabContainer1.ActiveTabIndex == 0)
            TabContainer1.ActiveTabIndex = 1;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// while adding cart details from 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDetailAddToCart_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        bool OkToContinue = true;

        DataManager dm = new DataManager();
        dsInventoryDataSet ds = (dsInventoryDataSet)dm.GetCache(this.Mode.ToString() + "dsInventoryDataSet");
        dsInventoryDataSet dsCopyForComplementaryInput = ds.Copy() as dsInventoryDataSet;
        dsCopyForComplementaryInput.ttinventory.Clear();
        dsCopyForComplementaryInput.AcceptChanges();

        string SearchCriteraForComplementaryInput = (string)dm.GetCache(this.Mode.ToString() + "InventorySearchCriteria");

        if (SearchCriteraForComplementaryInput.Contains("SearchType="))
        {
            string SearchCriteriaLeft = SearchCriteraForComplementaryInput.Substring(0, SearchCriteraForComplementaryInput.IndexOf("SearchType="));
            string SearchCriteriaRight = SearchCriteraForComplementaryInput.Substring(SearchCriteraForComplementaryInput.IndexOf("SearchTran="));
            SearchCriteraForComplementaryInput = SearchCriteriaLeft + "SearchType=CompItems\x0003" + SearchCriteriaRight;
        }
        else
        {
            SearchCriteraForComplementaryInput = SearchCriteraForComplementaryInput + "\x0003SearchType=CompItems";
        }

        dsQuoteDataSet dsQuoteCurrentQuote = new dsQuoteDataSet();
        dsQuoteDataSet dsQuoteAddNewItems = new dsQuoteDataSet();

        if (Mode == ModeType.QuoteEditAddItems)
        {
            if (Session["QuoteEdit_dsQuote"] != null)
            {
                dsQuoteCurrentQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
                dsQuoteAddNewItems = dsQuoteCurrentQuote.Copy() as dsQuoteDataSet;
                dsQuoteAddNewItems.pvttquote_detail.Clear();
                dsQuoteAddNewItems.AcceptChanges();
            }
        }

        Session["UIisOnInventoryDetail"] = true;

        lblDetailMessage.Text = "&nbsp;<br/>&nbsp;";

        dsInventoryDataSet.ttinventoryRow inventoryRow = null;
        if (Session[this.Mode.ToString() + "CachedDetailInventoryRow"] != null)
            inventoryRow = (dsInventoryDataSet.ttinventoryRow)Session[this.Mode.ToString() + "CachedDetailInventoryRow"];

        if (inventoryRow != null)
        {
            string item, size, description, uom, qty_uom_conv_ptr_sysid, priceUom;
            int qty_uom_conv_ptr;
            decimal quantity, price, netprice, retailprice, minpack, extension;
            decimal thickness, width, length;
            bool minimumQtyViolation;

            try
            {
                quantity = Decimal.Parse(txtDetailItemQty.Text);
            }
            catch
            {
                lblDetailMessage.Text = "<br/>Invalid quantity";

#if DEBUG
        TimeSpan elapsedtimeReturn = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeReturn, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
                return;
            }

            quantity = Decimal.Parse(txtDetailItemQty.Text);

            dsInventoryDataSet dsCopy = new dsInventoryDataSet();

            if (!(inventoryRow.TYPE.ToLower() == "specific length lumber" || inventoryRow.TYPE.ToLower() == "sheet good"))
                dsCopy = FetchAllDataForChangedUOM("itemdetail", inventoryRow.ITEM, Convert.ToInt32(ddlDetailUOM.SelectedItem.Value));

            decimal uomConvFactor = 0;

            if (dsCopy.ttinventory.Rows.Count > 0)
            {
                DataRow[] rows = dsCopy.ttitem_uomconv.Select("uom_ptr = " + Convert.ToInt32(ddlDetailUOM.SelectedItem.Value));

                if (rows.Length == 1)
                {
                    uomConvFactor = Convert.ToDecimal(rows[0]["conv_factor"]);
                }
            }

            if (dsCopy.ttinventory.Rows.Count == 0)
            {
                minpack = inventoryRow.min_pak;
                uom = inventoryRow.stocking_uom;
                qty_uom_conv_ptr = inventoryRow.qty_uom_conv_ptr;
                qty_uom_conv_ptr_sysid = inventoryRow.qty_uom_conv_ptr_sysid;
                netprice = inventoryRow.price;
                retailprice = inventoryRow.retail_price;
                priceUom = uom;
            }
            else
            {
                minpack = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                uom = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_uom"]);
                qty_uom_conv_ptr = Convert.ToInt32(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr"]);
                qty_uom_conv_ptr_sysid = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_uom_conv_ptr_sysid"]);
                netprice = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["price"]);
                retailprice = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["retail_price"]);
                priceUom = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_uom"]);
            }

            minimumQtyViolation = false;

            if (minpack > 0)
            {
                if (Convert.ToDecimal(minpack) > 0 && Convert.ToDecimal(minpack) < 1)
                {
                    if (!quantity.ToString().Contains("."))
                    {
                        minimumQtyViolation = false;
                    }
                    else
                    {
                        decimal decimalPortion = Convert.ToDecimal(quantity.ToString().Substring(quantity.ToString().IndexOf(".")));
                        minimumQtyViolation = Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(decimalPortion), 0), 4) % Decimal.Round(Convert.ToDecimal(1) / Decimal.Round(Convert.ToDecimal(1) / Convert.ToDecimal(minpack), 0), 4) > 0;
                    }
                }
                else
                {
                    if (!quantity.ToString().Contains("."))
                    {
                        minimumQtyViolation = quantity % Convert.ToDecimal(minpack) > 0;
                    }
                    else
                    {
                        minimumQtyViolation = quantity % Decimal.Round(Convert.ToDecimal(1) / uomConvFactor, 4) > 0;
                    }
                }
            }

            if (!minimumQtyViolation)
            {
                item = inventoryRow.ITEM;
                thickness = inventoryRow.thickness;
                width = inventoryRow.WIDTH;
                length = inventoryRow.LENGTH;
                size = inventoryRow.SIZE;
                description = inventoryRow.DESCRIPTION;

                price = 0;

                if (_viewInvPrices)
                {
                    if (Session["UserPref_DisplaySetting"] != null &&
                       ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                        (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                        (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                    {
                        price = retailprice;
                    }
                    else
                        price = netprice;
                }
                else
                {
                    price = 0;
                }

                extension = price * quantity;

                if (Mode == ModeType.Inventory)
                {
                    CartAdd(quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length);
                }

                if (Mode == ModeType.QuoteEditAddItems)
                {
                    QuoteDataSetAddItem(ref dsQuoteAddNewItems, quantity, item, size, description, uom, qty_uom_conv_ptr, qty_uom_conv_ptr_sysid, price, priceUom, minpack, extension, thickness, width, length);
                }

                txtDetailItemQty.Text = "";
            }

            if (minimumQtyViolation)
            {
                lblDetailMessage.Text = "<br/>Invalid multiple";
                OkToContinue = false;
            }

            if (AvailableParentDiv.Visible == true || OnOrderParentDiv.Visible == true || tableXrefs.Visible == true || tableDocuments.Visible == true)
                PricingParentDiv.Visible = false;
        }

        if (((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 1) || ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 0))
        {
            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 5;
        }

        if (OkToContinue == false)
        {

#if DEBUG
        TimeSpan elapsedtimeReturn = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtimeReturn, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
            return;
        }

        if (Mode == ModeType.QuoteEditAddItems)
        {
            decimal markupFactorValue = 0;
            string retailMode = "Net price";

            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                retailMode = (string)Session["UserPref_DisplaySetting"];

                if (Session["MarkupFactor"] != null)
                {
                    markupFactorValue = Convert.ToDecimal(Session["MarkupFactor"]);
                }
            }

            if (dsQuoteAddNewItems.pvttquote_detail.Rows.Count > 0)
            {
                foreach (dsQuoteDataSet.pvttquote_detailRow row in dsQuoteAddNewItems.pvttquote_detail.Rows)
                {
                    row["retail_mode"] = retailMode;
                }

                dsQuoteAddNewItems.AcceptChanges();

                Quotes q = new Quotes();
                string messageText = q.AddQuoteDetailItem(markupFactorValue, dsQuoteAddNewItems);

                if (messageText != "0")
                {
                    if (messageText.Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    lblErrorMessage.Text = messageText;
                    OkToContinue = false;
                }
            }
        }


        if (Mode == ModeType.Inventory)
        {
            /* Analytics Tracking */

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Product Details - Add to Cart Button";
            aInput.Label = "Mode = Net";
            aInput.Value = "0";

            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                aInput.Label = "Mode = Retail";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        if (OkToContinue && (bool)inventoryRow["display_comp_items_pv"] == true)
        {
            DataRow newRow = dsCopyForComplementaryInput.ttinventory.NewRow();
            newRow.ItemArray = inventoryRow.ItemArray;
            dsCopyForComplementaryInput.ttinventory.Rows.Add(newRow);
            dsCopyForComplementaryInput.AcceptChanges();

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            inv.FetchItemsComplementaryItems(SearchCriteraForComplementaryInput, (dsPartnerVuDataSet)dm.GetCache("dsPV"), ref dsCopyForComplementaryInput, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

            Session["dsInventoryComplementaryItems"] = dsCopyForComplementaryInput;

            if (dsCopyForComplementaryInput.ttinventory.Rows.Count > 0)
            {
                if (this.Mode == ModeType.Inventory)
                {
                    Parent.FindControl("ComplementaryItems").Visible = true;
                    Session["ComplementarySource"] = "Inventory";
                    Session["BlockPopupMenu"] = true;
                    this.Visible = false;

                    MenuEnableDisable menuDisable = new MenuEnableDisable();
                    menuDisable.DisableMenuItems(this.Page, this.Parent);

                    if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
                    {
                        if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                        {
                            if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                            {
                                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                            }
                        }
                    }
                }

                if (this.Mode == ModeType.QuoteEditAddItems)
                {
                    Parent.Parent.FindControl("ComplementaryItems").Visible = true;
                    Session["ComplementarySource"] = "QuoteEditAddItems";
                    this.Parent.Visible = false;
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void ddlHeader_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"].ToString().ToLower() == "false")
        {
            Profile.ddlInventoryHeaderRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);
        }

        int pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        this.AssignDataSource(Data.Cached);

        AnalyticsInput aInput = new AnalyticsInput();
        aInput.Type = "event";
        aInput.Action = "Products - Show Rows Selection";
        aInput.Label = ((System.Web.UI.WebControls.DropDownList)sender).SelectedItem.ToString();
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlProductSorting_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataSet dsInventory = new DataSet();
        dsInventory = (DataSet)Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"];
        lblErrorMessage.Text = "";
        BindProductGridViewDetails(dsInventory.Tables["ttInventory"], 1);

        AnalyticsInput aInput = new AnalyticsInput();
        aInput.Type = "event";
        aInput.Action = "Products - Sort By Selection";
        aInput.Label = ddlProductSorting.SelectedItem.ToString();
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();



#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dtProductList"></param>
    protected void BindProductGridViewDetails(DataTable dtProductList, int iPageNo)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataTable dtBindPagingData = new DataTable();
        DataTable dtpaging = new DataTable();

        if (dtProductList.Rows.Count > 0)
        {
            if (ddlProductSorting.SelectedValue.ToString() == "1" || ddlProductSorting.SelectedValue.ToString() == "0")
            {
                DataTable dt = new DataTable();
                var distinctRows = (from DataRow dRow in dtProductList.Rows
                                    orderby dRow["ITEM"] ascending
                                    select dRow);
                dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();
                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));

                dtpaging = PagerPartition(dtProductList);
            }
            else if (ddlProductSorting.SelectedValue.ToString() == "2")
            {
                DataTable dt = new DataTable();
                var distinctRows = (from DataRow dRow in dtProductList.Rows
                                    orderby dRow["ITEM"] descending
                                    select dRow);
                dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();
                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));

                dtpaging = PagerPartition(dtProductList);
            }
            else if (ddlProductSorting.SelectedValue.ToString() == "3")
            {
                DataTable dt = new DataTable();
                var distinctRows = (from DataRow dRow in dtProductList.Rows
                                    orderby dRow["DESCRIPTION"]
                                    select dRow);
                dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();
                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));

                dtpaging = PagerPartition(dtProductList);
            }
            else if (ddlProductSorting.SelectedValue.ToString() == "4")
            {
                DataTable dt = new DataTable();
                var distinctRows = (from DataRow dRow in dtProductList.Rows
                                    orderby dRow["DESCRIPTION"] descending
                                    select dRow);
                dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();

                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));

                dtpaging = PagerPartition(dtProductList);
            }
            else if (ddlProductSorting.SelectedValue.ToString() == "5")
            {
                DataTable dt = new DataTable();

                if (_viewText == true)
                {
                    var distinctRows = (from DataRow dRow in dtProductList.Rows
                                        orderby dRow["qty_available"] descending
                                        select dRow);
                    dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();
                }
                else if (_viewQuantity == true)
                {
                    var distinctRows = (from DataRow dRow in dtProductList.Rows
                                        orderby dRow["qty_available"] descending
                                        select dRow);
                    dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();
                }

                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));
                dtpaging = PagerPartition(dtProductList);
            }
            else if (ddlProductSorting.SelectedValue.ToString() == "6")
            {
                DataTable dt = new DataTable();

                if (_viewText == true)
                {
                    var distinctRows = (from DataRow dRow in dtProductList.Rows
                                        orderby dRow["qty_available"] ascending
                                        select dRow);
                    dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();
                }
                else if (_viewQuantity == true)
                {
                    var distinctRows = (from DataRow dRow in dtProductList.Rows
                                        orderby dRow["qty_available"] ascending
                                        select dRow);
                    dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();
                }

                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));
                dtpaging = PagerPartition(dtProductList);
            }
            else if (ddlProductSorting.SelectedValue.ToString() == "7")
            {
                DataTable dt = new DataTable();

                var distinctRows = (from DataRow dRow in dtProductList.Rows
                                    orderby dRow["price"] descending
                                    select dRow);
                dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();

                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));
                dtpaging = PagerPartition(dtProductList);
            }
            else if (ddlProductSorting.SelectedValue.ToString() == "8")
            {
                DataTable dt = new DataTable();

                var distinctRows = (from DataRow dRow in dtProductList.Rows
                                    orderby dRow["price"] ascending
                                    select dRow);
                dt = distinctRows.ToList().AsEnumerable().CopyToDataTable();

                dtBindPagingData = PagingData(dt, Convert.ToInt16(iPageNo), Convert.ToInt16(ddlHeader.SelectedValue));
                dtpaging = PagerPartition(dtProductList);
            }
        }

        if (dtBindPagingData.Rows.Count > 0)
        {
            DataRow[] drFindImage;
            drFindImage = dtBindPagingData.Select("image_file <>'" + string.Empty + "'");

            if (drFindImage.Length > 0)
            {
                dvImgQuickList.Attributes["class"] = "DivImagesalign";
                this.lvNoImagesProductsList.Items.Clear();
                this.lvNoImagesProductsList.DataBind();
                lvNoImagesProductsList.Visible = false;
                this.lvProducts.DataSource = dtBindPagingData;
                this.lvProducts.DataBind();

                lvProducts.Visible = true;
            }
            else
            {
                dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
                this.lvNoImagesProductsList.DataSource = dtBindPagingData;
                this.lvNoImagesProductsList.DataBind();

                this.lvProducts.Items.Clear();
                this.lvProducts.DataBind();
                lvProducts.Visible = false;
                lvNoImagesProductsList.Visible = true;
            }

            int ipageindex = Convert.ToInt16(iPageNo);

            if (ipageindex > 0)
            {
                ipageindex = ipageindex - 1;
            }

            int igridcount = dtBindPagingData.Rows.Count;
            int itotalrecords = dtProductList.Rows.Count;
            int iNoofrows = Convert.ToInt32(ddlHeader.SelectedValue);
            int iFirstrecordno = ipageindex * (iNoofrows);
            iFirstrecordno = iFirstrecordno + 1;
            int iLastRecord = (iFirstrecordno - 1) + igridcount;

            if (itotalrecords > 0)
            {
                lblpageno.Text = "Showing " + iFirstrecordno.ToString() + " to " + iLastRecord.ToString() + " of " + itotalrecords.ToString() + " Rows";
            }

            if (dtpaging.Rows.Count > 0)
            {
                if (dtpaging.Rows.Count > 1)
                {
                    this.lvPaging.DataSource = dtpaging;
                    this.lvPaging.DataBind();

                    int iPageNos = iPageNo;
                    ListViewItem dataItem1 = new ListViewItem(ListViewItemType.DataItem);
                    dataItem1 = lvPaging.Items[iPageNos - 1];
                    LinkButton lnknumber1 = (LinkButton)dataItem1.FindControl("lnknumber");

                    lnknumber1.CssClass = "listPaging";
                    lnknumber1.Attributes.Add("readonly", "readonly");

                    this.lvPaging.Visible = true;
                }
                else
                {
                    this.lvPaging.Visible = false;
                }

                lblpageno.Visible = true;
            }
            else
            {
                this.lvPaging.Visible = false;
                lblpageno.Visible = false;
            }
        }
        else
        {
            this.lvPaging.Visible = false;
            lblpageno.Visible = false;
        }

        dvProdListView.Visible = true;
        dvImageBanners.Visible = false;
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            btnAddAll2.Visible = false;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// for binding list view items
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lvProducts_ItemDataBound(object sender, System.Web.UI.WebControls.ListViewItemEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            object obj = ((System.Web.UI.WebControls.ListViewDataItem)(e.Item)).DataItem;
            DataRow CurrentRow = ((DataRowView)obj).Row;
            DataManager dm = new DataManager();
            dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dm.GetCache("dsPVParam");

            if (!string.IsNullOrEmpty(Convert.ToString(CurrentRow["ItemRecid"])))
            {
                Label lblAvailable = (Label)e.Item.FindControl("lblAvailable");

                HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("trExtendedDescription");
                tr.Visible = false;

                if (CurrentRow["ext_description"].ToString().Length > 0)
                {
                    tr.Visible = true;

                    Label lblExtDesc = (Label)e.Item.FindControl("lblExtDesc");
                    LinkButton lnkExtDesc = (LinkButton)e.Item.FindControl("lnkExtDesc");

                    if (CurrentRow["ext_description"].ToString().Length > 100)
                    {
                        lblExtDesc.Text = CurrentRow["ext_description"].ToString().Substring(0, 100) + "... ";
                        lnkExtDesc.Visible = true;
                        lnkExtDesc.CommandArgument = CurrentRow["item"].ToString() + "|" + CurrentRow["thickness"].ToString() + "|" + CurrentRow["width"].ToString() + "|" + CurrentRow["length"].ToString();

                        HiddenField hdnExtDesc = (HiddenField)e.Item.FindControl("hdnExtDesc");
                        hdnExtDesc.Value = CurrentRow["ext_description"].ToString();
                    }
                    else
                    {
                        lblExtDesc.Text = CurrentRow["ext_description"].ToString() + " ";
                        lnkExtDesc.Visible = false;
                    }
                }

                if (Convert.ToString(CurrentRow["ItemRecid"]) == "0")
                {

                }
                else
                {
                    ImageButton imgProduct = (ImageButton)e.Item.FindControl("imgProduct");

                    if (!DBNull.Value.Equals(CurrentRow["image_file"]))
                    {
                        if (CurrentRow["image_file"].ToString().Length > 0)
                        {
                            imgProduct.ImageUrl = (string)CurrentRow["image_file"];
                            imgProduct.CommandName = "Image";
                            imgProduct.CommandArgument = "Image";
                        }
                        else
                        {
                            HtmlTableCell tc = (HtmlTableCell)e.Item.FindControl("tdImagebtn");
                            imgProduct.Visible = false;
                        }
                    }
                    else
                    {
                        imgProduct.Visible = false;
                        tdimageItemDetail.Visible = false;
                    }

                    LinkButton lbtProdDesc = (LinkButton)e.Item.FindControl("lbtProdDesc");

                    if (!DBNull.Value.Equals(CurrentRow["DESCRIPTION"]))
                    {
                        string Size = string.Empty;

                        if (!DBNull.Value.Equals(CurrentRow["Size"]))
                        {
                            Size = (string)(CurrentRow["Size"]);
                        }

                        lbtProdDesc.Text = Size + " " + (string)CurrentRow["DESCRIPTION"];
                        lbtProdDesc.CommandArgument = (string)CurrentRow["DESCRIPTION"];
                        lbtProdDesc.CommandName = "Product";
                    }

                    Label lblAvilableCount = (Label)e.Item.FindControl("lblAvilableCount");
                    Label lblAvailUOM = (Label)e.Item.FindControl("lblAvailUOM");
                    Label lblMinPack = (Label)e.Item.FindControl("lblMinPack");

                    if (_viewInvAvailQty && (_viewQuantity || _viewText))
                    {
                        if (!DBNull.Value.Equals(CurrentRow["qty_text"]))
                        {
                            if (CurrentRow["qty_text"].ToString() != " ")
                            {
                                if (_viewText == true)
                                {
                                    lblAvilableCount.Text = (string)Convert.ToString(CurrentRow["qty_text"]);
                                    lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_text"]);
                                }
                                else if (_viewQuantity == true)
                                {
                                    decimal Quantity = (decimal)CurrentRow["qty_available"];
                                    lblAvilableCount.Text = Quantity.ToTrimmedString();
                                    lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_available"]);

                                    if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                                    {
                                        lblAvailUOM.Text = " " + (string)CurrentRow["stocking_uom"];
                                    }
                                }
                            }
                            else
                            {
                                decimal Quantity = (decimal)CurrentRow["qty_available"];
                                lblAvilableCount.Text = Quantity.ToTrimmedString();
                                lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_available"]);

                                if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                                {
                                    lblAvailUOM.Text = " " + (string)CurrentRow["stocking_uom"];
                                }
                            }
                        }
                        else
                        {
                            if (!DBNull.Value.Equals(CurrentRow["qty_available"]))
                            {
                                decimal sQuantity = (decimal)CurrentRow["qty_available"];
                                lblAvilableCount.Text = sQuantity.ToTrimmedString();
                            }
                        }
                    }
                    else
                    {
                        lblAvailable.Visible = false;
                        lblAvilableCount.Visible = false;
                        lblMinPack.Visible = false;
                    }

                    Label lblPartNo = (Label)e.Item.FindControl("lblPartNo");

                    if (!DBNull.Value.Equals(CurrentRow["ITEM"]))
                    {
                        lblPartNo.Text = (string)CurrentRow["ITEM"];
                        lblPartNo.ToolTip = (string)CurrentRow["ITEM"];
                    }

                    Label lblMajorMinor = (Label)e.Item.FindControl("lblMajorMinor");

                    if (!DBNull.Value.Equals(CurrentRow["major_description"]))
                    {
                        string minordesc = string.Empty;
                        string majordesc = string.Empty;

                        if (!DBNull.Value.Equals((string)CurrentRow["minor_description"]))
                        {
                            minordesc = (string)CurrentRow["minor_description"];
                        }

                        if (!DBNull.Value.Equals(CurrentRow["major_description"]))
                        {
                            majordesc = (string)CurrentRow["major_description"];
                        }

                        lblMajorMinor.Text = majordesc + " / " + minordesc;

                        if ((majordesc.Trim() == string.Empty) && (minordesc.Trim() == string.Empty))
                        {
                            lblMajorMinor.Text = "";
                        }

                        lblMajorMinor.ToolTip = (string)CurrentRow["major_description"];
                    }

                    Label lblOrder = (Label)e.Item.FindControl("lblOrder");
                    Label lblproddesc = (Label)e.Item.FindControl("lblproddesc");
                    bool configurable = false;

                    if (CurrentRow["configurable"].ToString().Length > 0)
                        configurable = Convert.ToBoolean(CurrentRow["configurable"]);

                    DropDownList ddluom = (DropDownList)e.Item.FindControl("ddlUOM");

                    if (configurable)
                        ddluom.Visible = false;

                    if (Session["GenericLogin"] != null && (string)Session["GenericLogin"] == "True")
                        ddluom.Visible = false;

                    PopulateUOMDropDown(ddluom, Convert.ToString(CurrentRow["item_ptr"]), (string)CurrentRow["stocking_uom"], (string)CurrentRow["TYPE"]);

                    TextBox enteredQtyTextBox = (TextBox)e.Item.FindControl("txtOrder");
                    enteredQtyTextBox.Attributes.Add("onkeyup", "javascript:return fnAddTocartTextchanged(this.id);");

                    Panel p = (Panel)e.Item.FindControl("panel_Configure");
                    if (configurable)
                    {
                        if (enteredQtyTextBox != null)
                        {
                            enteredQtyTextBox.Visible = false;
                        }

                        if (p != null)
                        {
                            p.Visible = true;
                            imgProduct.Enabled = false;
                            lbtProdDesc.Visible = false;

                            if (Mode == ModeType.QuoteEditAddItems)
                            {
                                LinkButton link = p.FindControl("linkbutton_Configure") as LinkButton;
                                link.PostBackUrl += "&source=pvquote";
                            }

                            if (!DBNull.Value.Equals(CurrentRow["DESCRIPTION"]))
                            {
                                string Size = string.Empty;

                                if (!DBNull.Value.Equals(CurrentRow["Size"]))
                                {
                                    Size = (string)(CurrentRow["Size"]);
                                }

                                lblproddesc.Text = Size + " " + (string)CurrentRow["DESCRIPTION"];

                                lblproddesc.Visible = true;
                            }
                        }

                        ddluom.Visible = false;
                        lblOrder.Visible = false;
                    }

                    //security to show ordercart text box,buttons based on this value quantity is visible 
                    if (dm.GetCache(this.Mode.ToString() + "OrderFormAllowed") != null && (bool)dm.GetCache(this.Mode.ToString() + "OrderFormAllowed"))
                    {
                        if (!configurable)
                        {
                            enteredQtyTextBox.Visible = true;
                            lblOrder.Visible = true;
                        }
                    }
                    else
                    {
                        enteredQtyTextBox.Visible = false;
                        lblOrder.Visible = false;
                        ddluom.Visible = false;
                        p.Visible = false;
                        lblMinPack.Visible = true;
                    }

                    if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
                    {
                        enteredQtyTextBox.Visible = false;
                        lblOrder.Visible = false;
                        ddluom.Visible = false;
                        p.Visible = false;
                        lblMinPack.Visible = false;
                    }

                    Label lblPriceText = (Label)e.Item.FindControl("lblPriceText");
                    Label lblPrice = (Label)e.Item.FindControl("lblPrice");

                    if (!DBNull.Value.Equals(CurrentRow["price"]))
                    {
                        string PriceUom = string.Empty;
                        decimal Price = (decimal)CurrentRow["price"];

                        if (!DBNull.Value.Equals(CurrentRow["price_uom"]))
                        {
                            PriceUom = (string)CurrentRow["price_uom"];
                        }

                        if (string.IsNullOrEmpty(PriceUom))
                        {
                            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && _viewInvPrices)
                            {
                                if (Session["UserPref_DisplaySetting"] != null &&
                                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                                {
                                    lblPrice.Text = "Retail price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["retail_price"]);
                                }
                                else
                                {
                                    lblPrice.Text = "Price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["price"]);
                                }
                            }
                        }
                        else
                        {
                            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && _viewInvPrices)
                            {
                                if (Session["UserPref_DisplaySetting"] != null &&
                                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                                {
                                    decimal dMarkup = 1;

                                    if (Session["MarkupFactor"] != null)
                                    {
                                        dMarkup = (decimal)Session["MarkupFactor"];
                                    }

                                    lblPrice.Text = "Retail price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["retail_price"]) + "/" + PriceUom; ;
                                }
                                else
                                {
                                    lblPrice.Text = "Price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["price"]) + "/" + PriceUom;
                                }
                            }
                        }
                    }

                    HiddenField hdnStockinguom = (HiddenField)e.Item.FindControl("hdnStockinguom");

                    if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                    {
                        hdnStockinguom.Value = (string)Convert.ToString(CurrentRow["stocking_uom"]);
                    }

                    HiddenField hdnminpackDetails = (HiddenField)e.Item.FindControl("hdnminpackDetails");

                    if (!DBNull.Value.Equals(CurrentRow["min_pak"]))
                    {
                        hdnminpackDetails.Value = (string)Convert.ToString(CurrentRow["min_pak"]);
                    }

                    HiddenField hdnRetailprice = (HiddenField)e.Item.FindControl("hdnRetailprice");

                    if (!DBNull.Value.Equals(CurrentRow["retail_price"]))
                    {
                        hdnRetailprice.Value = (string)Convert.ToString(CurrentRow["retail_price"]);
                    }

                    HiddenField hdnsize = (HiddenField)e.Item.FindControl("hdnsize");

                    if (!DBNull.Value.Equals(CurrentRow["Size"]))
                    {
                        hdnsize.Value = (string)Convert.ToString(CurrentRow["Size"]);
                    }

                    HiddenField hdnItem = (HiddenField)e.Item.FindControl("hdnItem");

                    if (!DBNull.Value.Equals(CurrentRow["ITEM"]))
                    {
                        hdnItem.Value = (string)Convert.ToString(CurrentRow["ITEM"]);
                    }

                    HiddenField hdnItemPtr = (HiddenField)e.Item.FindControl("hdnItemPtr");

                    if (!DBNull.Value.Equals(CurrentRow["item_ptr"]))
                    {
                        hdnItemPtr.Value = (string)Convert.ToString(CurrentRow["item_ptr"]);
                    }

                    HiddenField hdnItemType = (HiddenField)e.Item.FindControl("hdnItemType");

                    if (!DBNull.Value.Equals(CurrentRow["TYPE"]))
                    {
                        hdnItemType.Value = (string)Convert.ToString(CurrentRow["TYPE"]);
                    }

                    HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");

                    if (!DBNull.Value.Equals(CurrentRow["price"]))
                    {
                        hdnPrice.Value = (string)Convert.ToString(CurrentRow["price"]);
                    }

                    LinkButton lbtnQuicklist = (LinkButton)e.Item.FindControl("lbtnQuicklist");
                    int command = (int)e.Item.DisplayIndex;
                    lbtnQuicklist.CommandArgument = command.ToString();
                    lbtnQuicklist.OnClientClick += "javascript:return GetControlIds(this.id)";

                    if (!DBNull.Value.Equals(CurrentRow["min_Pak"]))
                    {
                        decimal Minpack = (decimal)CurrentRow["min_Pak"];
                        if (Minpack > 0)
                        {
                            lblMinPack.Text = "Must be ordered in multiples of " + (string)CurrentRow["min_Pak"].ToString();
                        }
                    }

                    if (!DBNull.Value.Equals(CurrentRow["quick_list"]))
                    {
                        bool istrue = (bool)CurrentRow["quick_list"];

                        if (istrue == true)
                        {
                            lbtnQuicklist.Text = "- Remove from my quicklist";
                        }
                        else
                        {
                            lbtnQuicklist.Text = "+ Add to my quicklist";
                        }
                    }
                }

                if (dm.GetCache(this.Mode.ToString() + "OrderFormAllowed") != null && (bool)dm.GetCache(this.Mode.ToString() + "OrderFormAllowed"))
                {
                    btnAddAll2.Visible = true;
                }
                else
                {
                    btnAddAll2.Visible = false;
                }

                if ((string)Session["GenericLogin"] == "True")
                {
                    TextBox enteredQtyTextBox = (TextBox)e.Item.FindControl("txtOrder");

                    enteredQtyTextBox.Visible = false;
                    LinkButton lbtnQuicklist = (LinkButton)e.Item.FindControl("lbtnQuicklist");
                    lbtnQuicklist.Visible = false;
                    Label lblOrder = (Label)e.Item.FindControl("lblOrder");
                    lblOrder.Visible = false;
                    Label lblMinPack = (Label)e.Item.FindControl("lblMinPack");
                    lblMinPack.Visible = false;
                    LinkButton linkbutton_Configure = (LinkButton)e.Item.FindControl("linkbutton_Configure");
                    linkbutton_Configure.Visible = false;
                    btnAddAll2.Visible = false;

                    // Mas adding code
                    DataRow[] drAllowPublicAccesstoConfig = dsPVParam.ttparam_pv.Select("property_name='" + "allow_public_access_to_configurator" + "'");

                    if (drAllowPublicAccesstoConfig.Length > 0)
                        Session["allow_public_access_to_configurator"] = drAllowPublicAccesstoConfig[0]["property_value"].ToString();

                    if (((string)Session["allow_public_access_to_configurator"]).ToLower() == "yes")
                    {
                        bool configurable = false;

                        if (CurrentRow["configurable"].ToString().Length > 0)
                            configurable = Convert.ToBoolean(CurrentRow["configurable"]);

                        Panel p = (Panel)e.Item.FindControl("panel_Configure");

                        if (configurable && p != null)
                        {
                            p.Visible = true;
                            linkbutton_Configure.Visible = true;
                        }

                    }  //end of allow_public_access_to_configurator

                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// List view item command
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lvProducts_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.CommandName != null)
        {
            string btnAddToQlistText = (e.Item.FindControl("lbtnQuicklist") as LinkButton).Text;
            Label lblMinPackLabel = e.Item.FindControl("lblMinPack") as Label;

            DropDownList ddluom = e.Item.FindControl("ddlUOM") as DropDownList;
            string itemuompointer = ddluom.SelectedItem.Value;
            string originalstockinguom = (e.Item.FindControl("hdnStockinguom") as HiddenField).Value;
            string itempointer = (e.Item.FindControl("hdnItemPtr") as HiddenField).Value;
            string itemtype = (e.Item.FindControl("hdnItemType") as HiddenField).Value;
            string itemForDatamember = (e.Item.FindControl("hdnItem") as HiddenField).Value;
            ddlDetailUOM.DataMember = itemForDatamember;
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            int icommand = dataItem.DisplayIndex;

            string item = lvProducts.DataKeys[dataItem.DisplayIndex].Values["ITEM"].ToString();
            string size = lvProducts.DataKeys[dataItem.DisplayIndex].Values["Size"].ToString();

            decimal thickness = (decimal)lvProducts.DataKeys[dataItem.DisplayIndex].Values["thickness"];
            decimal width = (decimal)lvProducts.DataKeys[dataItem.DisplayIndex].Values["WIDTH"];
            decimal length = (decimal)lvProducts.DataKeys[dataItem.DisplayIndex].Values["LENGTH"];
            lvProducts.SelectedIndex = icommand;
            ListViewDataItem ldv = lvProducts.Items[icommand];

            if (e.CommandName == "Product" || e.CommandName == "Image")
            {
                if ((string)Session["GenericLogin"] == "True")
                {
                    btnAddToQList.Visible = false;
                    btnAddAll2.Visible = false;
                    btnViewQuickList.Visible = false;
                    chkQuickList.Visible = false;
                    btnDetailAddToCart.Visible = false;
                    txtDetailItemQty.Visible = false;
                    lbltrdetailqty.Visible = false;
                    lblDetailMinPack.Visible = false;
                    btnAddXref.Visible = false;
                    pnlAddXref.Visible = false;
                    btnAdvViewQuickList.Visible = false;
                    this.NewQlistDiv.Visible = false;
                    this.ddlDetailUOM.Visible = false;
                }
                else
                {
                    this.ClearProductTextboxes();

                    btnAddToQList.Visible = true;

                    txtDetailItemQty.Visible = true;
                    lbltrdetailqty.Visible = true;

                    if (_viewInvAvailQty)
                        lblDetailMinPack.Visible = true;
                    else
                        lblDetailMinPack.Visible = false;

                    btnAddXref.Visible = true;
                    pnlAddXref.Visible = true;
                    btnAdvViewQuickList.Visible = true;
                    this.ddlDetailUOM.Visible = true;

                    if (Session["DisplayCustItemQlist"] != null && ((string)Session["DisplayCustItemQlist"]).ToLower().Trim() == "no")
                    {
                        btnViewQuickList.Visible = true;
                    }
                    else
                        this.NewQlistDiv.Visible = true;
                }

                if (_viewInvPrices == false)
                    lblDetailPriceAndUom.Visible = false;

                if (_viewInvAvailQty == false)
                {
                    lblDetailAvailableQty.Visible = false;
                    lblMinPackLabel.Visible = false;
                    lblDetailMinPack.Visible = false;
                }

                string code = lvProducts.DataKeys[dataItem.DisplayIndex].Values[0].ToString();

                this.trSearch.Visible = false;
                this.trSearch1.Visible = false;
                this.trDetails.Visible = true;
                Session["singleview"] = "true";
                BuildItemDetail(item, ldv, thickness, width, length, size, btnAddToQlistText);

                if (e.CommandName == "Product" || e.CommandName == "Image")
                {
                    PopulateUOMDropDown(this.ddlDetailUOM, itempointer, originalstockinguom, itemtype);

                    for (int x = 0; x < ddlDetailUOM.Items.Count; x++)
                    {
                        if (ddlDetailUOM.Items[x].Value == itemuompointer)
                        {
                            ddlDetailUOM.SelectedIndex = x;
                            break;
                        }
                    }

                    if (!(itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good"))
                        FetchAllDataForChangedUOM("detail", itemForDatamember, Convert.ToInt32(itemuompointer));
                }

                Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

                if (OnUserPrefModeChanged != null)
                {
                    Session["UIisOnInventoryDetail"] = true;
                    OnUserPrefModeChanged(true);
                }

                if (e.CommandName == "Image")
                {

                    /* Analytics Tracking */
                    AnalyticsInput aInput = new AnalyticsInput();

                    aInput.Type = "event";
                    aInput.Action = "Product Results - Image Click";
                    aInput.Label = "";
                    aInput.Value = "0";

                    AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                    am.BeginTrackEvent();

                    string[] SelBreadCat, SelGroup, SelAttribute;
                    SelBreadCat = lnkBreadCategory.CommandArgument.Split('_');
                    SelGroup = lnkBreadgroup.CommandArgument.Split('_');
                    SelAttribute = lnkBreadCatValue.CommandArgument.Split('_');
                }

                string strUserAgent = Request.UserAgent.ToString().ToLower();

                if (strUserAgent != null)
                {
                    if (Request.Browser.IsMobileDevice == true ||
                         strUserAgent.Contains("iphone") ||
                         strUserAgent.Contains("blackberry") ||
                         strUserAgent.Contains("mobile") ||
                         strUserAgent.Contains("windows ce") ||
                         strUserAgent.Contains("android") || strUserAgent.Contains("iPad") ||
                         strUserAgent.Contains("ipod"))
                    {
                        txtDetailItemQty.FocusOnInitialization = false;
                    }
                }
            }
            else if (e.CommandName == "QuickList")
            {

                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Product Results - Add to Quicklist";
                aInput.Label = "";
                aInput.Value = "0";

                if (btnAddToQlistText.Contains("Remove"))
                {
                    aInput.Action = "Product Results - Remove from Quicklist";
                }

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

                BindQuickListDetails(ldv, 1, btnAddToQlistText);
            }
            else if (e.CommandName == "ExtDesc")
            {
                Session["ExtDescCommand"] = true;

                string extDesc = (e.Item.FindControl("hdnExtDesc") as HiddenField).Value;
                Label lblExtDesc = e.Item.FindControl("lblExtDesc") as Label;
                LinkButton lnkExtDesc = e.Item.FindControl("lnkExtDesc") as LinkButton;

                if (lnkExtDesc.Text == "show more")
                {
                    lnkExtDesc.Text = "show less";
                    lblExtDesc.Text = extDesc + " ";
                }
                else if (lnkExtDesc.Text == "show less")
                {
                    lnkExtDesc.Text = "show more";
                    lblExtDesc.Text = extDesc.Substring(0, 100) + "... ";
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void lvNoImagesProductsList_ItemDataBound(object sender, System.Web.UI.WebControls.ListViewItemEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            object obj = ((System.Web.UI.WebControls.ListViewDataItem)(e.Item)).DataItem;
            DataRow CurrentRow = ((DataRowView)obj).Row;
            DataManager dm = new DataManager();
            dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dm.GetCache("dsPVParam");

            if (!string.IsNullOrEmpty(Convert.ToString(CurrentRow["ItemRecid"])))
            {
                Label lblAvailable = (Label)e.Item.FindControl("lblAvailable");

                HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("trExtendedDescription");
                tr.Visible = false;

                if (CurrentRow["ext_description"].ToString().Length > 0)
                {
                    tr.Visible = true;

                    Label lblExtDesc = (Label)e.Item.FindControl("lblExtDesc");
                    LinkButton lnkExtDesc = (LinkButton)e.Item.FindControl("lnkExtDesc");

                    if (CurrentRow["ext_description"].ToString().Length > 100)
                    {
                        lblExtDesc.Text = CurrentRow["ext_description"].ToString().Substring(0, 100) + "... ";
                        lnkExtDesc.Visible = true;
                        lnkExtDesc.CommandArgument = CurrentRow["item"].ToString() + "|" + CurrentRow["thickness"].ToString() + "|" + CurrentRow["width"].ToString() + "|" + CurrentRow["length"].ToString();

                        HiddenField hdnExtDesc = (HiddenField)e.Item.FindControl("hdnExtDesc");
                        hdnExtDesc.Value = CurrentRow["ext_description"].ToString();
                    }
                    else
                    {
                        lblExtDesc.Text = CurrentRow["ext_description"].ToString() + " ";
                        lnkExtDesc.Visible = false;
                    }
                }

                if (Convert.ToString(CurrentRow["ItemRecid"]) == "0")
                {

                }
                else
                {
                    LinkButton lbtProdDesc = (LinkButton)e.Item.FindControl("lbtProdDesc");

                    if (!DBNull.Value.Equals(CurrentRow["DESCRIPTION"]))
                    {
                        string Size = string.Empty;

                        if (!DBNull.Value.Equals(CurrentRow["Size"]))
                        {
                            Size = (string)(CurrentRow["Size"]);
                        }

                        lbtProdDesc.Text = Size + " " + (string)CurrentRow["DESCRIPTION"];
                        lbtProdDesc.CommandArgument = (string)CurrentRow["DESCRIPTION"];
                        lbtProdDesc.CommandName = "Product";
                    }

                    Label lblAvilableCount = (Label)e.Item.FindControl("lblAvilableCount");
                    Label lblAvailUOM = (Label)e.Item.FindControl("lblAvailUOM");
                    Label lblMinPack = (Label)e.Item.FindControl("lblMinPack");

                    if (_viewInvAvailQty && (_viewQuantity || _viewText))
                    {
                        if (!DBNull.Value.Equals(CurrentRow["qty_text"]))
                        {
                            if (CurrentRow["qty_text"].ToString() != " ")
                            {
                                if (_viewText == true)
                                {
                                    lblAvilableCount.Text = (string)Convert.ToString(CurrentRow["qty_text"]);
                                    lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_text"]);
                                }
                                else if (_viewQuantity == true)
                                {
                                    decimal Quantity = (decimal)CurrentRow["qty_available"];
                                    lblAvilableCount.Text = Quantity.ToTrimmedString();
                                    lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_available"]);

                                    if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                                    {
                                        lblAvailUOM.Text = " " + (string)CurrentRow["stocking_uom"];
                                    }
                                }
                            }
                            else
                            {
                                decimal Quantity = (decimal)CurrentRow["qty_available"];
                                lblAvilableCount.Text = Quantity.ToTrimmedString();
                                lblAvilableCount.ToolTip = (string)Convert.ToString(CurrentRow["qty_available"]);

                                if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                                {
                                    lblAvailUOM.Text = " " + (string)CurrentRow["stocking_uom"];
                                }
                            }
                        }
                        else
                        {
                            if (!DBNull.Value.Equals(CurrentRow["qty_available"]))
                            {
                                decimal Quantity = (decimal)CurrentRow["qty_available"];
                                lblAvilableCount.Text = Quantity.ToTrimmedString();
                            }
                        }
                    }
                    else
                    {
                        lblAvailable.Visible = false;
                        lblAvilableCount.Visible = false;
                        lblMinPack.Visible = false;
                    }

                    Label lblPartNo = (Label)e.Item.FindControl("lblPartNo");

                    if (!DBNull.Value.Equals(CurrentRow["ITEM"]))
                    {
                        lblPartNo.Text = (string)CurrentRow["ITEM"];
                        lblPartNo.ToolTip = (string)CurrentRow["ITEM"];
                    }

                    Label lblMajorMinor = (Label)e.Item.FindControl("lblMajorMinor");

                    if (!DBNull.Value.Equals(CurrentRow["major_description"]))
                    {
                        string minordesc = string.Empty;
                        string majordesc = string.Empty;

                        if (!DBNull.Value.Equals((string)CurrentRow["minor_description"]))
                        {
                            minordesc = (string)CurrentRow["minor_description"];
                        }

                        if (!DBNull.Value.Equals(CurrentRow["major_description"]))
                        {
                            majordesc = (string)CurrentRow["major_description"];
                        }

                        lblMajorMinor.Text = majordesc + " / " + minordesc;

                        if ((majordesc.Trim() == string.Empty) && (minordesc.Trim() == string.Empty))
                        {
                            lblMajorMinor.Text = "";
                        }

                        lblMajorMinor.ToolTip = (string)CurrentRow["major_description"];
                    }

                    Label lblOrder = (Label)e.Item.FindControl("lblOrder");
                    Label lblproddesc = (Label)e.Item.FindControl("lblproddesc");
                    bool configurable = false;

                    if (CurrentRow["configurable"].ToString().Length > 0)
                        configurable = Convert.ToBoolean(CurrentRow["configurable"]);

                    DropDownList ddluom = (DropDownList)e.Item.FindControl("ddlUOM2");

                    if (configurable)
                        ddluom.Visible = false;

                    if (Session["GenericLogin"] != null && (string)Session["GenericLogin"] == "True")
                        ddluom.Visible = false;

                    PopulateUOMDropDown(ddluom, Convert.ToString(CurrentRow["item_ptr"]), (string)CurrentRow["stocking_uom"], (string)CurrentRow["TYPE"]);

                    TextBox enteredQtyTextBox = (TextBox)e.Item.FindControl("txtOrder");
                    enteredQtyTextBox.Attributes.Add("onkeyup", "javascript:return fnAddTocartTextchanged(this.id);");
                    Panel pconfigure = (Panel)e.Item.FindControl("panel_Configure");

                    if (configurable)
                    {
                        if (enteredQtyTextBox != null)
                            enteredQtyTextBox.Visible = false;

                        if (pconfigure != null)
                        {
                            pconfigure.Visible = true;

                            lbtProdDesc.Visible = false;

                            if (Mode == ModeType.QuoteEditAddItems)
                            {
                                LinkButton link = pconfigure.FindControl("linkbutton_Configure") as LinkButton;
                                link.PostBackUrl += "&source=pvquote";
                            }

                            if (!DBNull.Value.Equals(CurrentRow["DESCRIPTION"]))
                            {
                                string Size = string.Empty;

                                if (!DBNull.Value.Equals(CurrentRow["Size"]))
                                {
                                    Size = (string)(CurrentRow["Size"]);
                                }

                                lblproddesc.Text = Size + " " + (string)CurrentRow["DESCRIPTION"];
                                lblproddesc.Visible = true;
                            }
                        }

                        ddluom.Visible = false;
                        lblOrder.Visible = false;
                    }

                    //security to show ordercart text box,buttons based on this value quantity is visible 
                    if (dm.GetCache(this.Mode.ToString() + "OrderFormAllowed") != null && (bool)dm.GetCache(this.Mode.ToString() + "OrderFormAllowed"))
                    {
                        if (!configurable)
                        {
                            enteredQtyTextBox.Visible = true;
                            lblOrder.Visible = true;
                        }
                    }
                    else
                    {
                        enteredQtyTextBox.Visible = false;
                        lblOrder.Visible = false;
                        ddluom.Visible = false;
                        pconfigure.Visible = false;
                        lblMinPack.Visible = true;
                    }

                    if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
                    {
                        enteredQtyTextBox.Visible = false;
                        lblOrder.Visible = false;
                        ddluom.Visible = false;
                        pconfigure.Visible = false;
                        lblMinPack.Visible = false;
                    }

                    Label lblPriceText = (Label)e.Item.FindControl("lblPriceText");
                    Label lblPrice = (Label)e.Item.FindControl("lblPrice");

                    if (!DBNull.Value.Equals(CurrentRow["price"]))
                    {
                        string PriceUom = string.Empty;
                        decimal Price = (decimal)CurrentRow["price"];

                        if (!DBNull.Value.Equals(CurrentRow["price_uom"]))
                        {
                            PriceUom = (string)CurrentRow["price_uom"];
                        }

                        if (string.IsNullOrEmpty(PriceUom))
                        {
                            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && _viewInvPrices)
                            {
                                if (Session["UserPref_DisplaySetting"] != null &&
                                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                                {
                                    decimal Markup = 1;

                                    if (Session["MarkupFactor"] != null)
                                    {
                                        Markup = (decimal)Session["MarkupFactor"];
                                    }

                                    lblPrice.Text = "Retail price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["retail_price"]);
                                }
                                else
                                {
                                    lblPrice.Text = "Price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["price"]);
                                }
                            }
                        }
                        else
                        {
                            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && _viewInvPrices)
                            {
                                if (Session["UserPref_DisplaySetting"] != null &&
                                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                                {
                                    decimal Markup = 1;

                                    if (Session["MarkupFactor"] != null)
                                    {
                                        Markup = (decimal)Session["MarkupFactor"];
                                    }

                                    lblPrice.Text = "Retail price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["retail_price"]) + "/" + PriceUom; ;
                                }
                                else
                                {
                                    lblPrice.Text = "Price";
                                    lblPriceText.Text = string.Format("{0:C4}", (decimal)CurrentRow["price"]) + "/" + PriceUom;
                                }
                            }
                        }
                    }

                    HiddenField hdnStockinguom = (HiddenField)e.Item.FindControl("hdnStockinguom");

                    if (!DBNull.Value.Equals(CurrentRow["stocking_uom"]))
                    {
                        hdnStockinguom.Value = (string)Convert.ToString(CurrentRow["stocking_uom"]);
                    }

                    HiddenField hdnminpackDetails = (HiddenField)e.Item.FindControl("hdnminpackDetails");

                    if (!DBNull.Value.Equals(CurrentRow["min_pak"]))
                    {
                        hdnminpackDetails.Value = (string)Convert.ToString(CurrentRow["min_pak"]);
                    }

                    HiddenField hdnRetailprice = (HiddenField)e.Item.FindControl("hdnRetailprice");

                    if (!DBNull.Value.Equals(CurrentRow["retail_price"]))
                    {
                        hdnRetailprice.Value = (string)Convert.ToString(CurrentRow["retail_price"]);
                    }

                    HiddenField hdnsize = (HiddenField)e.Item.FindControl("hdnsize");

                    if (!DBNull.Value.Equals(CurrentRow["Size"]))
                    {
                        hdnsize.Value = (string)Convert.ToString(CurrentRow["Size"]);
                    }

                    HiddenField hdnItem = (HiddenField)e.Item.FindControl("hdnItem");

                    if (!DBNull.Value.Equals(CurrentRow["ITEM"]))
                    {
                        hdnItem.Value = (string)Convert.ToString(CurrentRow["ITEM"]);
                    }

                    HiddenField hdnItemPtr = (HiddenField)e.Item.FindControl("hdnItemPtr");

                    if (!DBNull.Value.Equals(CurrentRow["item_ptr"]))
                    {
                        hdnItemPtr.Value = (string)Convert.ToString(CurrentRow["item_ptr"]);
                    }

                    HiddenField hdnItemType = (HiddenField)e.Item.FindControl("hdnItemType");

                    if (!DBNull.Value.Equals(CurrentRow["TYPE"]))
                    {
                        hdnItemType.Value = (string)Convert.ToString(CurrentRow["TYPE"]);
                    }

                    HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");

                    if (!DBNull.Value.Equals(CurrentRow["price"]))
                    {
                        hdnPrice.Value = (string)Convert.ToString(CurrentRow["price"]);
                    }

                    LinkButton lbtnQuicklist = (LinkButton)e.Item.FindControl("lbtnQuicklist");
                    int command = (int)e.Item.DisplayIndex;
                    lbtnQuicklist.CommandArgument = command.ToString();
                    lbtnQuicklist.OnClientClick += "javascript:return GetControlIds(this.id)";

                    if (!DBNull.Value.Equals(CurrentRow["min_Pak"]))
                    {
                        decimal dMinpack = (decimal)CurrentRow["min_Pak"];

                        if (dMinpack > 0)
                        {
                            lblMinPack.Text = "Must be ordered in multiples of " + (string)CurrentRow["min_Pak"].ToString();
                        }
                    }

                    if (!DBNull.Value.Equals(CurrentRow["quick_list"]))
                    {
                        bool istrue = (bool)CurrentRow["quick_list"];

                        if (istrue == true)
                        {
                            lbtnQuicklist.Text = "- Remove from my quicklist";
                        }
                        else
                        {
                            lbtnQuicklist.Text = "+ Add to my quicklist";
                        }
                    }

                    if (dm.GetCache(this.Mode.ToString() + "OrderFormAllowed") != null && (bool)dm.GetCache(this.Mode.ToString() + "OrderFormAllowed"))
                    {
                        btnAddAll2.Visible = true;
                    }
                    else
                    {
                        btnAddAll2.Visible = false;
                    }
                }
            }

            if ((string)Session["GenericLogin"] == "True")
            {
                TextBox enteredQtyTextBox = (TextBox)e.Item.FindControl("txtOrder");

                enteredQtyTextBox.Visible = false;
                LinkButton lbtnQuicklist = (LinkButton)e.Item.FindControl("lbtnQuicklist");
                lbtnQuicklist.Visible = false;
                Label lblOrder = (Label)e.Item.FindControl("lblOrder");
                lblOrder.Visible = false;
                Label lblMinPack = (Label)e.Item.FindControl("lblMinPack");
                lblMinPack.Visible = false;
                LinkButton linkbutton_Configure = (LinkButton)e.Item.FindControl("linkbutton_Configure");
                linkbutton_Configure.Visible = false;
                btnAddAll2.Visible = false;

                // Mas adding code
                DataRow[] drAllowPublicAccesstoConfig = dsPVParam.ttparam_pv.Select("property_name='" + "allow_public_access_to_configurator" + "'");

                if (drAllowPublicAccesstoConfig.Length > 0)
                    Session["allow_public_access_to_configurator"] = drAllowPublicAccesstoConfig[0]["property_value"].ToString();

                if (((string)Session["allow_public_access_to_configurator"]).ToLower() == "yes")
                {
                    bool configurable = false;

                    if (CurrentRow["configurable"].ToString().Length > 0)
                        configurable = Convert.ToBoolean(CurrentRow["configurable"]);

                    Panel p = (Panel)e.Item.FindControl("panel_Configure");

                    if (configurable && p != null)
                    {
                        p.Visible = true;
                        linkbutton_Configure.Visible = true;
                    }

                }  //end of allow_public_access_to_configurator

            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void lvNoImagesProductsList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.CommandName != null)
        {
            string btnAddToQlistText = (e.Item.FindControl("lbtnQuicklist") as LinkButton).Text;
            Label lblMinPackLabel = e.Item.FindControl("lblMinPack") as Label;

            DropDownList ddluom = e.Item.FindControl("ddlUOM2") as DropDownList;
            string itemuompointer = ddluom.SelectedItem.Value;
            string originalstockinguom = (e.Item.FindControl("hdnStockinguom") as HiddenField).Value;
            string itempointer = (e.Item.FindControl("hdnItemPtr") as HiddenField).Value;
            string itemtype = (e.Item.FindControl("hdnItemType") as HiddenField).Value;
            string itemForDataMember = (e.Item.FindControl("hdnItem") as HiddenField).Value;
            ddlDetailUOM.DataMember = itemForDataMember;
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            int icommand = dataItem.DisplayIndex;

            string item = lvNoImagesProductsList.DataKeys[dataItem.DisplayIndex].Values["ITEM"].ToString();
            string size = lvNoImagesProductsList.DataKeys[dataItem.DisplayIndex].Values["Size"].ToString();

            decimal thickness = (decimal)lvNoImagesProductsList.DataKeys[dataItem.DisplayIndex].Values["thickness"];
            decimal width = (decimal)lvNoImagesProductsList.DataKeys[dataItem.DisplayIndex].Values["WIDTH"];
            decimal length = (decimal)lvNoImagesProductsList.DataKeys[dataItem.DisplayIndex].Values["LENGTH"];
            lvProducts.SelectedIndex = icommand;
            ListViewDataItem ldv = lvNoImagesProductsList.Items[icommand];

            if (e.CommandName == "Product" || e.CommandName == "Image")
            {
                imgproddiscription.Text = e.CommandArgument.ToString();

                if ((string)Session["GenericLogin"] == "True")
                {
                    btnAddToQList.Visible = false;
                    btnAddAll2.Visible = false;
                    btnViewQuickList.Visible = false;
                    chkQuickList.Visible = false;
                    btnDetailAddToCart.Visible = false;
                    txtDetailItemQty.Visible = false;
                    lbltrdetailqty.Visible = false;
                    lblDetailMinPack.Visible = false;
                    btnAddXref.Visible = false;
                    pnlAddXref.Visible = false;
                    btnAdvViewQuickList.Visible = false;
                    this.NewQlistDiv.Visible = false;
                    this.ddlDetailUOM.Visible = false;
                }
                else
                {
                    this.ClearProductTextboxes();

                    btnAddToQList.Visible = true;
                    btnViewQuickList.Visible = true;
                    txtDetailItemQty.Visible = true;
                    lbltrdetailqty.Visible = true;

                    if (_viewInvAvailQty)
                        lblDetailMinPack.Visible = true;
                    else
                        lblDetailMinPack.Visible = false;

                    btnAddXref.Visible = true;
                    pnlAddXref.Visible = true;
                    this.ddlDetailUOM.Visible = true;

                    if (Session["DisplayCustItemQlist"] != null && ((string)Session["DisplayCustItemQlist"]).ToLower().Trim() == "no")
                    {
                        btnAdvViewQuickList.Visible = true;
                    }
                    else
                        this.NewQlistDiv.Visible = true;
                }

                if (_viewInvPrices == false)
                    lblDetailPriceAndUom.Visible = false;

                if (_viewInvAvailQty == false)
                {
                    lblDetailAvailableQty.Visible = false;
                    lblMinPackLabel.Visible = false;
                    lblDetailMinPack.Visible = false;
                }

                string code = lvNoImagesProductsList.DataKeys[dataItem.DisplayIndex].Values[0].ToString();

                this.trSearch.Visible = false;
                this.trSearch1.Visible = false;
                this.trDetails.Visible = true;
                Session["singleview"] = "true";
                BuildItemDetail(item, ldv, thickness, width, length, size, btnAddToQlistText);

                if (e.CommandName == "Product" || e.CommandName == "Image")
                {
                    PopulateUOMDropDown(this.ddlDetailUOM, itempointer, originalstockinguom, itemtype);

                    for (int x = 0; x < ddlDetailUOM.Items.Count; x++)
                    {
                        if (ddlDetailUOM.Items[x].Value == itemuompointer)
                        {
                            ddlDetailUOM.SelectedIndex = x;
                            break;
                        }
                    }

                    if (!(itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good"))
                        FetchAllDataForChangedUOM("detail", itemForDataMember, Convert.ToInt32(itemuompointer));
                }

                Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

                if (OnUserPrefModeChanged != null)
                {
                    Session["UIisOnInventoryDetail"] = true;
                    OnUserPrefModeChanged(true);
                }

                if (e.CommandName == "Image")
                {
                    string[] SelBreadCat, SelGroup, SelAttribute;
                    SelBreadCat = lnkBreadCategory.CommandArgument.Split('_');
                    SelGroup = lnkBreadgroup.CommandArgument.Split('_');
                    SelAttribute = lnkBreadCatValue.CommandArgument.Split('_');

                    if (SelAttribute.Length > 3)
                    {
                        LoadAttributesDetails(SelBreadCat[1], SelGroup[2], "", SelAttribute[3], SelAttribute[4]);
                    }
                }
            }
            else if (e.CommandName == "QuickList")
            {
                BindQuickListDetails(ldv, 2, btnAddToQlistText);

                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Product Results - Add to Quicklist";
                aInput.Label = "";
                aInput.Value = "0";

                if (btnAddToQlistText.Contains("Remove"))
                {
                    aInput.Action = "Product Results - Remove from Quicklist";
                }

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

            }
            else if (e.CommandName == "ExtDesc")
            {
                Session["ExtDescCommand"] = true;

                string extDesc = (e.Item.FindControl("hdnExtDesc") as HiddenField).Value;
                Label lblExtDesc = e.Item.FindControl("lblExtDesc") as Label;
                LinkButton lnkExtDesc = e.Item.FindControl("lnkExtDesc") as LinkButton;

                if (lnkExtDesc.Text == "show more")
                {
                    lnkExtDesc.Text = "show less";
                    lblExtDesc.Text = extDesc + " ";
                }
                else if (lnkExtDesc.Text == "show less")
                {
                    lnkExtDesc.Text = "show more";
                    lblExtDesc.Text = extDesc.Substring(0, 100) + "... ";
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lvPaging_ItemDataBound(object sender, System.Web.UI.WebControls.ListViewItemEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            object obj = ((System.Web.UI.WebControls.ListViewDataItem)(e.Item)).DataItem;
            DataRow CurrentRow = ((DataRowView)obj).Row;

            if (!string.IsNullOrEmpty(Convert.ToString(CurrentRow["PageNo"])))
            {
                if (!DBNull.Value.Equals(CurrentRow["PageNo"]))
                {
                    LinkButton lnknumber = (LinkButton)e.Item.FindControl("lnknumber");
                    lnknumber.Text = (string)CurrentRow["PageNo"];
                    lnknumber.Attributes.Add("onclick", "javascript:return fnPagingNotification();");
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// /
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lvPaging_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.CommandName != null)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;

            if (Session[this.Mode.ToString() + "CachedInventoryDataSet"] != null)
            {
                LinkButton lnknumber = (LinkButton)dataItem.FindControl("lnknumber");
                lnknumber.ForeColor = System.Drawing.Color.Red;

                Session[this.Mode.ToString() + "CachedInventoryDataSet"] = Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"];
                dsInventoryDataSet dsInv = (dsInventoryDataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];

                int iPageNo = Convert.ToInt32(lnknumber.Text);
                lblErrorMessage.Text = "";
                hdnAddCart.Value = "0";

                BindProductGridViewDetails(dsInv.Tables["ttInventory"], iPageNo);

                dvProdListView.Visible = true;
                dvImageBanners.Visible = false;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void DataPagerProducts_PreRender(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.lvProducts.DataBind();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// method for binding Product list view 
    /// </summary>
    /// <param name="sAttributeDetails"></param>
    /// <param name="iVal"></param>
    public void BindProductsGrid(string[] AttributeDetails, int Val)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataSet dsProducts = new DataSet();

        if (AttributeDetails.Length > 5)
        {
            string sSelGroup = AttributeDetails[2].ToString();

            if (string.IsNullOrEmpty(sSelGroup))
            {
                sSelGroup = AttributeDetails[4].ToString();
            }

            dsProducts = GetFetchItem(sSelGroup, AttributeDetails[5], AttributeDetails[6]);
        }

        if ((dsProducts != null && dsProducts.Tables.Count > 0 && dsProducts.Tables[0].Rows.Count > 0))
        {
            if (Val == 2)
            {
                lnkBreadCatValue.Text = " " + dsProducts.Tables[0].Rows[0]["Description"].ToString();
                lnkBreadCatValue.CommandArgument = "AttributeVal_" + AttributeDetails[1].ToString() + "_" + AttributeDetails[2].ToString() + "_" + AttributeDetails[3].ToString()
                                + "_" + AttributeDetails[4].ToString() + "_" + AttributeDetails[5].ToString()
                                + "_" + AttributeDetails[6].ToString();
            }

            btnAddAll2.Visible = true;
            lblErrorMessage.Text = "";
            BindProductGridViewDetails(dsProducts.Tables[0], 1);
            Label2.Visible = true;
            dvImageBanners.Visible = false;
            dvProdListView.Visible = true;

            Session[this.Mode.ToString() + "CachedInventoryDataSet"] = dsProducts;
            Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = dsProducts;

            lblHeaderError.Text = "";
        }
        else
        {
            if (Val == 2)
            {
                lnkBreadCatValue.Text = " " + AttributeDetails[6].ToString();

                lnkBreadCatValue.CommandArgument = "AttributeVal_" + AttributeDetails[1].ToString() + "_" + AttributeDetails[2].ToString() + "_" + AttributeDetails[3].ToString()
                                + "_" + AttributeDetails[4].ToString() + "_" + AttributeDetails[5].ToString()
                                + "_" + AttributeDetails[6].ToString();
            }

            dvImageBanners.Visible = false;
            btnAddAll2.Visible = false;
            DataTable dtEmpty = new DataTable();

            lvProducts.Items.Clear();
            lvProducts.DataBind();

            if (TabContainer1.ActiveTab == tbSearch)
            {
                lblHeaderError.Text = NORESULTSFOUND;
                lvProducts.Visible = false;
                lvNoImagesProductsList.Visible = false;
            }
            else
            {
                lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
                lvProducts.Visible = false;
                lvNoImagesProductsList.Visible = false;
            }

            lblHeaderError.Visible = true;
            dvProdListView.Visible = true;
            lvPaging.Visible = false;
            lblpageno.Visible = false;
            dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
        }

        //for prior to login
        if ((string)Session["GenericLogin"] == "True")
        {
            btnAddToQList.Visible = false;
            btnAddAll2.Visible = false;
            btnViewQuickList.Visible = false;
            chkQuickList.Visible = false;
            btnDetailAddToCart.Visible = false;
            txtDetailItemQty.Visible = false;
            lbltrdetailqty.Visible = false;
            lblDetailMinPack.Visible = false;
            btnAddXref.Visible = false;
            pnlAddXref.Visible = false;
            btnAdvViewQuickList.Visible = false;
            this.NewQlistDiv.Visible = false;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #region "Inventory"

    private string BuildColumnVisibilityProperty(CheckBoxList checkBoxList, GridView gridView)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string outputString = "";
        char[] ColumnFlags = null;
        char c;
        int offset = 0;

        if (gridView.ID == "gvInventory")
        {
            offset = 8;
            outputString = Profile.gvInventoryHeader_Columns_Visible;
            ColumnFlags = outputString.ToCharArray();
        }

        if (gridView.ID == "gvInventoryPricing")
        {
            int y = 0;
            for (int x = 0; x < checkBoxList.Items.Count; x++)
            {
                if (x == 3)
                {
                    outputString += "11";
                    y = 2;
                }

                if (checkBoxList.Items[x].Selected)
                {
                    c = '1';
                    gridView.Columns[x + y].Visible = true;
                }
                else
                {
                    c = '0';
                    gridView.Columns[x + y].Visible = false;
                }

                outputString += c;
            }

            return outputString;
        }

        for (int x = 0; x < checkBoxList.Items.Count; x++)
        {
            if (checkBoxList.Items[x].Selected)
            {
                c = '1';
                if (gridView.Columns[x + offset].HeaderText == "Available")
                {
                    if (checkBoxList.Items[x].Text == "Available")
                        gridView.Columns[x + offset].Visible = _viewInvAvailQty;
                    if (checkBoxList.Items[x].Text == "Qty Available")
                        gridView.Columns[x + offset + 1].Visible = _viewInvAvailQty;
                }
                else
                    gridView.Columns[x + offset].Visible = true;
            }
            else
            {
                c = '0';
                if (gridView.Columns[x + offset].HeaderText == "Available")
                {
                    if (checkBoxList.Items[x].Text == "Available")
                        gridView.Columns[x + offset].Visible = false;
                    if (checkBoxList.Items[x].Text == "Qty Available")
                        gridView.Columns[x + offset + 1].Visible = false;
                }
                else
                    gridView.Columns[x + offset].Visible = false;
            }

            if (gridView.ID == "gvInventory")
                ColumnFlags[x + offset] = c;
            else
                outputString += c;
        }

        if (gridView.ID == "gvInventory")
        {
            outputString = "";
            foreach (char ch in ColumnFlags)
            {
                outputString += ch;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return outputString;
    }

    private static void ManipulateRow(GridViewRow row, string largeorsmall, string gridViewName)
    {
        for (int i = 0; i < row.Cells.Count; i++)
        {
            if (row.Cells[i] != null && row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
            {
                row.Cells[i].Text = row.Cells[i].Text.Replace(" ", "&nbsp;");
            }

            if (i == 2 && gridViewName == "gvInventory")
            {
                DataRow currRow = ((DataRowView)row.DataItem).Row;

                bool imageExists = false;
                if (currRow["image_file"].ToString().Length > 0)
                {
                    imageExists = true;
                }

                if (!imageExists)
                {
                    row.Cells[i].Controls[0].Visible = false;
                }
            }

            if (i == 5 && gridViewName == "gvInventory")
            {
                CheckBox box = (CheckBox)row.Cells[i].FindControl("quickList");
                DataRow currRow = ((DataRowView)row.DataItem).Row;
                if (currRow["quick_list"].ToString() != "")
                    box.Checked = (bool)currRow["quick_list"];
            }

            if (i == 0 && gridViewName == "gvInventory")
            {
                DataRow currRow = ((DataRowView)row.DataItem).Row;

                bool configurable = false;
                if (currRow["configurable"].ToString().Length > 0)
                    configurable = Convert.ToBoolean(currRow["configurable"]);

                if (configurable)
                {
                    TextBox enteredQtyTextBox = (TextBox)row.FindControl("qty");
                    if (enteredQtyTextBox != null)
                        enteredQtyTextBox.Visible = false;

                    Panel pnlconfigure = (Panel)row.FindControl("panel_Configure");
                    if (pnlconfigure != null)
                        pnlconfigure.Visible = true;

                    if (row.Cells[3].Controls.Count > 0)
                        row.Cells[3].Controls[0].Visible = false;
                }
            }
        }

        if (largeorsmall == "large")
        {
            row.Attributes.Add("onmouseover", "this.className='InventoryGridViewRowMouseHover'");
        }
        else
        {
            row.Attributes.Add("onmouseover", "this.className='GridViewRowMouseHover'");
        }

        row.Attributes.Add("onmouseout", "this.className='GridViewRowMouseOut'");
    }

    /// <summary>
    /// ForBindingQuick List Details
    /// </summary>
    /// <param name="lv"></param>
    protected void BindQuickListDetails(ListViewDataItem lv, int iSelListView, string btnAddToQlistText)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        decimal thickness = 0, width = 0, length = 0;
        string item = string.Empty, Compare, size;

        if (iSelListView == 1)
        {
            item = lvProducts.DataKeys[lv.DisplayIndex].Values["ITEM"].ToString();

            Compare = lvProducts.DataKeys[lv.DisplayIndex].Values["ITEM"].ToString();
            size = lvProducts.DataKeys[lv.DisplayIndex].Values["Size"].ToString();
            thickness = (decimal)lvProducts.DataKeys[lv.DisplayIndex].Values["thickness"];
            width = (decimal)lvProducts.DataKeys[lv.DisplayIndex].Values["WIDTH"];
            length = (decimal)lvProducts.DataKeys[lv.DisplayIndex].Values["LENGTH"];
        }
        else if (iSelListView == 2)
        {
            item = lvNoImagesProductsList.DataKeys[lv.DisplayIndex].Values["ITEM"].ToString();

            Compare = lvNoImagesProductsList.DataKeys[lv.DisplayIndex].Values["ITEM"].ToString();
            size = lvNoImagesProductsList.DataKeys[lv.DisplayIndex].Values["Size"].ToString();
            thickness = (decimal)lvNoImagesProductsList.DataKeys[lv.DisplayIndex].Values["thickness"];
            width = (decimal)lvNoImagesProductsList.DataKeys[lv.DisplayIndex].Values["WIDTH"];
            length = (decimal)lvNoImagesProductsList.DataKeys[lv.DisplayIndex].Values["LENGTH"];
        }

        hdnAddCart.Value = "0";

        dsInventoryDataSet dsInv = new dsInventoryDataSet();

        Session[this.Mode.ToString() + "CachedInventoryDataSet"] = Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"];
        dsInv = (dsInventoryDataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];

        string quoteSR_Item = QuoteSearchReplace(item);

        Dmsi.Agility.Data.Shipto currentShipto = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        Branch branch = new Branch();

        if (!DBNull.Value.Equals(dsInv))
        {
            DataRow[] rows = dsInv.Tables["ttInventory"].Select("ITEM='" + quoteSR_Item + "' AND thickness=" + thickness.ToString() + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString());

            if (rows.Length > 0)
            {
                if (btnAddToQlistText.Contains("Remove"))
                {
                    rows[0]["quick_list"] = false;
                }
                else
                {
                    rows[0]["quick_list"] = true;
                }
            }

            inventory.UpdateQuickList(branch.BranchId, user.GetContextValue("currentUserLogin"), currentShipto.ShiptoObj, dsInv, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }
        }

        inventory.Dispose();
        branch.Dispose();
        currentShipto.Dispose();
        DataSet dsProducts = new DataSet();
        string AttributeDetails;
        string[] SPlit;
        AttributeDetails = lnkBreadCatValue.CommandArgument.ToString();
        SPlit = AttributeDetails.Split('_');

        if (Session[this.Mode.ToString() + "btnViewQuickList_Click"] != null && (int)Session[this.Mode.ToString() + "btnViewQuickList_Click"] != 0)
        {
            Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 1;
        }
        else
        {
            if (dsInv.ttinventory.Rows.Count > 0)
            {
                Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 1;
            }
            else
            {
                Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;
            }
        }

        if ((int)Session[this.Mode.ToString() + "btnViewQuickList_Click"] == 1)
        {
            dsProducts = dsInv.Copy();
        }
        else
        {
            if (SPlit.Length > 5)
            {
                dsProducts = GetFetchItem(SPlit[2].ToString(), SPlit[5].ToString(), SPlit[6].ToString());
            }
            else
            {
                dsProducts = dsInv.Copy();
            }
        }

        int PageNo = 1;

        if (lvPaging.Items.Count > 0)
        {
            for (int pagecount = 0; pagecount <= lvPaging.Items.Count; pagecount++)
            {
                LinkButton lnkpgno = (LinkButton)lvPaging.Items[pagecount].FindControl("lnknumber");

                if (lnkpgno.CssClass == "listPaging")
                {
                    PageNo = Convert.ToInt32(lnkpgno.Text);
                    break;
                }
            }
        }

        if (!DBNull.Value.Equals(dsProducts))
        {
            if (dsProducts.Tables.Count > 0)
            {
                lblErrorMessage.Text = "";
                BindProductGridViewDetails(dsProducts.Tables[0], PageNo);

                if ((string)Session["GenericLogin"] == "True")
                {
                    btnAddToQList.Visible = false;
                    btnAddAll2.Visible = false;
                    btnViewQuickList.Visible = false;
                    chkQuickList.Visible = false;
                    btnDetailAddToCart.Visible = false;
                    txtDetailItemQty.Visible = false;
                    lbltrdetailqty.Visible = false;
                    lblDetailMinPack.Visible = false;
                    btnAddXref.Visible = false;
                    pnlAddXref.Visible = false;
                    btnAdvViewQuickList.Visible = false;
                    this.NewQlistDiv.Visible = false;
                }

                Session[this.Mode.ToString() + "CachedInventoryDataSet"] = dsProducts;
                Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = dsProducts;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    /// <param name="selectedRow"></param>
    /// <param name="thickness"></param>
    /// <param name="width"></param>
    /// <param name="length"></param>
    /// <param name="size"></param>
    protected void BuildItemDetail(string item, ListViewDataItem selectedRow, decimal thickness, decimal width, decimal length, string size, string btnAddToQListText)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        btnAddToQList.Text = btnAddToQListText;

        DataManager dm = new DataManager();

        dm.SetCache(this.Mode.ToString() + "dsInventoryAvailableTotalDataSet", null);
        dm.SetCache(this.Mode.ToString() + "dsInventoryAvailableBranchDataSet", null);
        dm.SetCache(this.Mode.ToString() + "dsInventoryAvailableTallyDataSet", null);

        dm.SetCache(this.Mode.ToString() + "dsInventoryOnOrderTotalDataSet", null);
        dm.SetCache(this.Mode.ToString() + "dsInventoryOnOrderBranchDataSet", null);
        dm.SetCache(this.Mode.ToString() + "dsInventoryOnOrderTranDataSet", null);

        gvItemXrefs.Columns[2].Visible = true;

        //set object visibility...
        tableXrefs.Visible = false;
        gvItemXrefs.DataSource = null;
        gvItemXrefs.DataBind();

        tableDocuments.Visible = false;

        //MDM -- price detail grid action allocation mod
        if (dm.GetCache("ViewInventoryPrices") != null && (bool)dm.GetCache("ViewInventoryPrices"))
        {
            if (dm.GetCache("ViewInventoryPriceDetail") != null && (bool)dm.GetCache("ViewInventoryPriceDetail"))
                PricingParentDiv.Visible = true;
            else
                PricingParentDiv.Visible = false;
        }
        //MDM -- price detail grid action allocation mod

        this.AvailableParentDiv.Visible = false;
        this.OnOrderParentDiv.Visible = false;
        //end of set object visibility...

        //blank Detail xref input.
        txtInputXref.Text = "";

        lblDetailMessage.Text = "&nbsp;<br/>&nbsp;";

        DataRow[] rows = null;
        dsInventoryDataSet dsInv = new dsInventoryDataSet();

        if (Session[this.Mode.ToString() + "CachedInventoryDataSet"] != null)
        {
            Session[this.Mode.ToString() + "CachedInventoryDataSet"] = Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"];
            dsInv = (dsInventoryDataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];
            string quoteSR_Item = QuoteSearchReplace(item);
            rows = dsInv.ttinventory.Select("ITEM='" + quoteSR_Item + "' AND thickness=" + thickness.ToString() + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString());
        }

        LinkButton lnkdescription = (LinkButton)selectedRow.FindControl("lbtProdDesc");
        HiddenField hdnminpack = (HiddenField)selectedRow.FindControl("hdnminpackDetails");

        if (rows != null && rows.Length > 0)
        {
            if (dm.GetCache(this.Mode.ToString() + "OrderFormAllowed") != null && !(bool)dm.GetCache(this.Mode.ToString() + "OrderFormAllowed"))
            {
                trDetailQty.Visible = false;
                btnDetailAddToCart.Visible = false;
            }
            else
            {
                //make product list visible
                trDetailQty.Visible = true;

                if ((string)Session["GenericLogin"] == "True")
                {
                    btnDetailAddToCart.Visible = false;
                }
                else
                {
                    btnDetailAddToCart.Visible = true;
                }
            }

            dsInventoryDataSet.ttinventoryRow inventoryRow = rows[0] as dsInventoryDataSet.ttinventoryRow;
            Session[this.Mode.ToString() + "CachedDetailInventoryRow"] = inventoryRow;

            bool imageExists = false;

            if (inventoryRow.image_file.Length > 0)
                imageExists = RemoteFileExists(inventoryRow.image_file);

            if (imageExists)
            {
                imageItemDetail.ImageUrl = inventoryRow.image_file;
                imgLarge.ImageUrl = inventoryRow.image_file;
                imageItemDetail.Visible = true;
                tdimageItemDetail.Visible = true;
                Bitmap bitImage;
                if (FetchRemoteURL(inventoryRow.image_file, out bitImage))
                {
                    if (bitImage != null)
                    {
                        double scaleRatio = 128.0 / bitImage.Height;

                        double scaledWidth = bitImage.Width * scaleRatio;

                        if (scaledWidth > 230)
                            WebImageViewer1.CssClass = "thumb-viewper";
                        else
                            WebImageViewer1.CssClass = "thumb-view";
                    }
                }
                else
                    WebImageViewer1.CssClass = "thumb-view";
            }
            else
            {
                imageItemDetail.Visible = false;
                tdimageItemDetail.Visible = false;
            }

            lblPGMajorMinor.Text = "Product Group: " + inventoryRow.major_description;

            if (inventoryRow.minor_description.Length > 0)
                lblPGMajorMinor.Text += " / " + inventoryRow.minor_description;

            lblSizePlusDescription.Text = size + " " + lnkdescription.CommandArgument;
            lblXrefPopUpSizeDesc.Text = size + " " + lnkdescription.CommandArgument;
            imgproddiscription.Text = size + " " + lnkdescription.CommandArgument;

            lblDetailPartNo.Text = "Part # " + inventoryRow.ITEM;
            lblXrefPopUpItem.Text = inventoryRow.ITEM;
            lblDetailAvailableQty.Text = inventoryRow.qty_available.ToString();
            lblDetailPriceAndUom.Text = inventoryRow.price.ToString();

            if (inventoryRow.ext_description.Length > 0)
            {
                lblDetailExtDesc.Visible = true;
                hdnDetailExtDesc.Value = inventoryRow.ext_description;

                if (inventoryRow.ext_description.Length > 100)
                {
                    lblDetailExtDesc.Text = inventoryRow.ext_description.Substring(0, 100) + "... ";
                    lnkExtDesc.Visible = true;
                    lnkExtDesc.Text = "show more";
                }
                else
                {
                    lblDetailExtDesc.Text = inventoryRow.ext_description + " ";
                    lnkExtDesc.Visible = false;
                }
            }
            else
            {
                lblDetailExtDesc.Visible = false;
                lnkExtDesc.Visible = false;
                hdnDetailExtDesc.Value = "";
            }

            if (!DBNull.Value.Equals(inventoryRow["ecommerce_description"]))
                this.divExtendedDescription.InnerHtml = inventoryRow.ecommerce_description.ToString();
            else
                this.divExtendedDescription.InnerHtml = "";

            if (dm.GetCache("ShiptoRowForInformationPanel") != null && !((bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"]) && _viewInvPrices)
            {
                if (Session["UserPref_DisplaySetting"] != null &&
                   ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                    (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                {
                    lblDetailPriceAndUom.Text = String.Format("{0:C}", inventoryRow.retail_price) + " / " + inventoryRow.price_uom;
                }
                else
                    lblDetailPriceAndUom.Text = String.Format("{0:C}", inventoryRow.price) + " / " + inventoryRow.price_uom;
            }
            else
                lblDetailPriceAndUom.Text = "&nbsp;";


            if (_viewInvAvailQty && (_viewQuantity || _viewText))
            {
                if (_viewText == true)
                {
                    lblDetailAvailableQty.Text = "Available qty " + inventoryRow.qty_text;
                }
                else if (_viewQuantity == true)
                {
                    lblDetailAvailableQty.Text = "Available qty " + inventoryRow.qty_available.ToString("#,###0");
                }
            }
            else
                lblDetailAvailableQty.Text = "&nbsp;";

            if (inventoryRow.min_pak > 0)
            {
                lblDetailMinPack.Text = "Must be ordered in multiples of " + inventoryRow.min_pak.ToString();
            }
            else
            {
                lblDetailMinPack.Text = "&nbsp;<br/>&nbsp;";
            }
        }

        dm.SetCache(this.Mode.ToString() + "CurrentItem", item + '\n' + size + '\n' + lnkdescription.CommandArgument + '\n' + hdnminpack.Value);

        //MDM - For document fetch, where we only need the item code.
        dm.SetCache(this.Mode.ToString() + "CurrentItemCode", item);
        Session[this.Mode.ToString() + "CachedItemDocumentsDataSet"] = null; //reset so fetch happens.

        FetchItemImages(item);
        FetchPopupItemImages(item);
        string searchCriteria = this.DetailBuildSearchCriteria(item, thickness, width, length);
        dm.SetCache(this.Mode.ToString() + "InventoryPriceSearchCriteria", searchCriteria);

        dm.SetCache(this.Mode.ToString() + "InventoryAvailableTotalSearchCriteria", searchCriteria.Replace("view_inv_prices", "view_avail_inv"));
        dm.SetCache(this.Mode.ToString() + "InventoryAvailableBranchSearchCriteria", searchCriteria.Replace("view_inv_prices", "view_avail_inv_by_branch"));
        dm.SetCache(this.Mode.ToString() + "InventoryAvailableTallySearchCriteria", searchCriteria.Replace("view_inv_prices", "view_avail_inv_tallies"));
        dm.SetCache(this.Mode.ToString() + "InventoryAvailableTallyDetailSearchCriteria", searchCriteria.Replace("view_inv_prices", "view_avail_inv_tallies"));

        dm.SetCache(this.Mode.ToString() + "InventoryOnOrderTotalSearchCriteria", searchCriteria.Replace("view_inv_prices", "view_on_order"));
        dm.SetCache(this.Mode.ToString() + "InventoryOnOrderBranchSearchCriteria", searchCriteria.Replace("view_inv_prices", "view_on_order_inv_by_branch"));
        dm.SetCache(this.Mode.ToString() + "InventoryOnOrderTranSearchCriteria", searchCriteria.Replace("view_inv_prices", "view_on_order_inv_trans"));
        dm.SetCache(this.Mode.ToString() + "InventoryOnOrderTranDetailSearchCriteria", searchCriteria.Replace("view_inv_prices", "view_on_order_inv_trans"));

        //add item # to label(s)
        string itemDescription = "Item " + item + ": " + size + " - " + lnkdescription.CommandArgument;

        // default sorts to the grids - actual field names
        dm.SetCache(this.Mode.ToString() + "gvInventoryPriceSortColumn", "saleType");
        dm.SetCache(this.Mode.ToString() + "gvInventoryPriceSortDirection", " ASC");

        dm.SetCache(this.Mode.ToString() + "gvInventoryAvailableBranchSortColumn", "branch_id");
        dm.SetCache(this.Mode.ToString() + "gvInventoryAvailableBranchSortDirection", " ASC");

        dm.SetCache(this.Mode.ToString() + "gvInventoryAvailableTallySortColumn", "tag");
        dm.SetCache(this.Mode.ToString() + "gvInventoryAvailableTallySortDirection", " ASC");

        dm.SetCache(this.Mode.ToString() + "gvInventoryAvailableTallyDetailSortColumn", "LENGTH");
        dm.SetCache(this.Mode.ToString() + "gvInventoryAvailableTallyDetailSortDirection", " ASC");

        dm.SetCache(this.Mode.ToString() + "gvInventoryOnOrderBranchSortColumn", "branch_id");
        dm.SetCache(this.Mode.ToString() + "gvInventoryOnOrderBranchSortDirection", " ASC");

        dm.SetCache(this.Mode.ToString() + "gvInventoryOnOrderTranSortColumn", "tran_id");
        dm.SetCache(this.Mode.ToString() + "gvInventoryOnOrderTranSortDirection", " ASC");

        dm.SetCache(this.Mode.ToString() + "gvInventoryOnOrderTranDetailSortColumn", "LENGTH");
        dm.SetCache(this.Mode.ToString() + "gvInventoryOnOrderTranDetailSortDirection", " ASC");

        dm.RemoveCache(this.Mode.ToString() + "dsInventoryDetailDataSet");

        //MDM -- price detail grid action allocation mod
        if (_viewInvPrices == true && _viewInvPriceDetail == true)
            this.AssignPriceDataSource(Data.Fetch);
        else
            FetchAvailable();
        //MDM -- price detail grid action allocation mod

        if (OnItemSelectionChanged != null)
            OnItemSelectionChanged(); // will be caught by the parent

        if (OnPriceSelectionChanged != null)
            OnPriceSelectionChanged(); // will be caught by the parent

        //Hide child grids of Tally and Tran
        gvInventoryAvailableTallyDetail.Visible = false;
        gvOnOrderTranDetail.Visible = false;

        //Deselect Tally and Tran Grids
        gvInventoryAvailableTally.SelectedIndex = -1;
        gvOnOrderTran.SelectedIndex = -1;

        //MDM -- price detail grid action allocation mod
        if (_viewInvPrices == true && _viewInvPriceDetail == true)
            gvInventoryPricing.Visible = true;
        else
        {
            if (_viewInvAvailQty == true)
                AvailableParentDiv.Visible = true;
            else
                AvailableParentDiv.Visible = false;
        }
        //MDM -- price detail grid action allocation mod

        switch (ddlLevelAvailable.SelectedValue)
        {
            case "Totals":
                gvInventoryAvailableTotal.Visible = true;
                break;

            case "Totals By Branch":
                gvInventoryAvailableBranch.Visible = true;
                break;

            case "Inventory Details":
                gvInventoryAvailableTally.Visible = true;
                break;
        }

        switch (ddlLevelOnOrder.SelectedValue)
        {
            case "Totals":
                gvOnOrderTotal.Visible = true;
                break;

            case "Totals By Branch":
                gvOnOrderBranch.Visible = true;
                break;

            case "Transaction Details":
                gvOnOrderTran.Visible = true;
                break;
        }

        //MDM -- price detail grid action allocation mod
        if (_viewInvPrices == true && _viewInvPriceDetail == true && !(bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            WebDataMenu1.SelectedItem = WebDataMenu1.Items.FindDataMenuItemByText("Pricing");

            foreach (DataMenuItem it in WebDataMenu1.Items)
            {
                it.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRoot";
            }

            WebDataMenu1.SelectedItem.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRootSelected";
        }
        else
        {
            WebDataMenu1.SelectedItem = WebDataMenu1.Items.FindDataMenuItemByText("Available");

            foreach (DataMenuItem it in WebDataMenu1.Items)
            {
                it.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRoot";
            }

            WebDataMenu1.SelectedItem.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRootSelected";

            if (_viewInvPriceDetail == false)
            {
                DataMenuItem pricing = WebDataMenu1.Items.FindDataMenuItemByText("Pricing");
                pricing.Visible = false;
            }
        }

        if ((_viewInvPrices == false || _viewInvPriceDetail == false) && Session["Show_Available"] == null)
        {
            WebDataMenu1.SelectedItem = WebDataMenu1.Items.FindDataMenuItemByText("On Order");

            foreach (DataMenuItem it in WebDataMenu1.Items)
            {
                it.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRoot";
            }

            WebDataMenu1.SelectedItem.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRootSelected";

            this.OnOrderParentDiv.Visible = true;

            this.AssignOnOrderTotalDataSource(Data.Fetch);
            this.AssignOnOrderBranchDataSource(Data.Fetch);
            this.AssignOnOrderTranDataSource(Data.Fetch);
        }

        if ((_viewInvPrices == false || _viewInvPriceDetail == false) && Session["Show_Available"] == null && Session["Show_OnOrder"] == null)
        {
            WebDataMenu1.SelectedItem = WebDataMenu1.Items.FindDataMenuItemByText("Part Numbers");

            foreach (DataMenuItem it in WebDataMenu1.Items)
            {
                it.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRoot";
            }

            WebDataMenu1.SelectedItem.CssClass = "igdm_" + ConfigurationManager.AppSettings["Theme"].ToString() + "MenuItemHorizontalRootSelected";

            LoadItemXrefs();
            PricingParentDiv.Visible = false;
            AvailableParentDiv.Visible = false;
            OnOrderParentDiv.Visible = false;
        }

        //MDM -- price detail grid action allocation mod

        //DE31207 fix other items that need to be hidden when nonsalable...
        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            trDetailQty.Visible = false;
            btnDetailAddToCart.Visible = false;
            PricingParentDiv.Visible = false;
            FetchAvailable();
            DataMenuItem pricing = this.WebDataMenu1.Items.FindDataMenuItemByText("Pricing");
            pricing.Visible = false;
            WebDataMenu1.SelectedItem = WebDataMenu1.Items.FindDataMenuItemByText("Available");
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private Control FindControlRecursive(Control Root, string Id)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return null;
    }

    private void FetchItemImages(string item)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        //there may not be any images, so hide first.
        tablerowImageViewer.Visible = false;

        Session[this.Mode.ToString() + "CachedImageListDataView"] = null;

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        inv.FetchItemImagesOrDocuments("Image", item, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        if (inv.ReferencedItemImageDataSet.ttItemImageDoc.Rows.Count > 0)
        {
            DataView dv = new DataView();
            dv.Table = inv.ReferencedItemImageDataSet.ttItemImageDoc;
            dv.RowFilter = "((image_file like 'http*') OR (image_file like 'https*'))";

            if (dv.Count > 0)
            {
                tablerowImageViewer.Visible = true;
                WebImageViewer1.DataSource = dv;
                WebImageViewer1.ImageItemBinding.ImageUrlField = "image_file";


                Session[this.Mode.ToString() + "CachedImageListDataView"] = dv;
            }

            DataRow[] rows = inv.ReferencedItemImageDataSet.ttItemImageDoc.Select("PRIMARY = True", "sequence");
            if (rows.Length > 0)
            {

            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// For converting HTML to CHAr
    /// </summary>
    /// <param name="rawString"></param>
    /// <returns></returns>
    private string ConvertHtmlToChar(string rawString)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string converted = rawString.Replace("&amp;", "&");
        converted = converted.Replace("&nbsp;", " ");
        converted = converted.Replace("&lt;", "<");
        converted = converted.Replace("&gt;", ">");
        converted = converted.Replace("&quot;", "\"");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return converted;
    }

    private void FetchAvailable()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataManager dm = new DataManager();

        AvailableParentDiv.Visible = true;
        PricingParentDiv.Visible = false;
        tableXrefs.Visible = false;
        tableDocuments.Visible = false;
        OnOrderParentDiv.Visible = false;

        if (dm.GetCache(this.Mode.ToString() + "dsInventoryAvailableTotalDataSet") != null)
            this.AssignAvailableTotalDataSource(Data.Cached);
        else
            this.AssignAvailableTotalDataSource(Data.Fetch);

        if (Session["Show_Available_By_Branch"] != null && (bool)Session["Show_Available_By_Branch"])
        {
            if (dm.GetCache(this.Mode.ToString() + "dsInventoryAvailableBranchDataSet") != null)
                this.AssignAvailableBranchDataSource(Data.Cached);
            else
                this.AssignAvailableBranchDataSource(Data.Fetch);
        }

        if (Session["Show_Available_Detail"] != null && (bool)Session["Show_Available_Detail"])
        {
            if (dm.GetCache(this.Mode.ToString() + "dsInventoryAvailableTallyDataSet") != null)
                this.AssignAvailableTallyDataSource(Data.Cached);
            else
                this.AssignAvailableTallyDataSource(Data.Fetch);
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private string QuoteSearchReplace(string inputString)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string outputString;

        outputString = inputString.Replace("'", "''");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return outputString;
    }

    protected void ProdDetails_Click(Object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        dvProdListView.Visible = true;

        if (sender != null && e != null)
        {

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Product Results - Description Link Click";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private bool RemoteFileExists(string url)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        try
        {
            //Creating the HttpWebRequest
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            //Setting the Request method HEAD, you can also use GET too.
            request.Method = "HEAD";

            //Getting the Web Response
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

            //Returns TURE if the Status code == 200
            return (response.StatusCode == HttpStatusCode.OK);
        }
        catch (System.Exception ex)
        {
            // string m = ex.Message + ex.StackTrace;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

            if (ex.Message.Contains("403"))
                return true;
            else

                //Any other exception will returns false.
                return false;
        }
    }

    private bool FetchRemoteURL(string url, out Bitmap bitImage)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        bitImage = null;

        try
        {
            //Creating the HttpWebRequest
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            request.Method = "GET";
            request.Timeout = 1000 * 15; //15 seconds
            request.ReadWriteTimeout = 1000 * 15; //15 seconds

            //Getting the Web Response
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            response.GetResponseStream().ReadTimeout = 1000 * 15; //15 seconds

            byte[] buffer = new byte[response.ContentLength];

            int bytesRead = 0;
            int totalBytesRead = bytesRead;

            DateTime startTime = DateTime.Now;
            DateTime bufferTime = new DateTime();
            TimeSpan elapsedTime = new TimeSpan();

            while (totalBytesRead < buffer.Length)
            {
                bytesRead = response.GetResponseStream().Read(buffer, bytesRead, buffer.Length - bytesRead);
                totalBytesRead += bytesRead;

                bufferTime = DateTime.Now;
                elapsedTime = bufferTime - startTime;
                if (elapsedTime.Seconds > 15)
                    return false;
            }

            MemoryStream ms = new MemoryStream(buffer);
            bitImage = new Bitmap(ms);

            ms.Close();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

            //Returns TURE if the Status code == 200
            return (response.StatusCode == HttpStatusCode.OK);
        }
        catch
        {
#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

            //Any exception will returns false.
            return false;
        }
    }

    /// <summary>
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReturnToItem_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Button bt = sender as Button;

        if (bt != null && sender != null && e != null)
        {

            /* Analytics Tracking */

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Product Details - Back to Product List - Top";
            aInput.Label = "";
            aInput.Value = "0";

            if (bt.ID == "btnReturnToItem2")
            {
                aInput.Action = "Product Details - Back to Product List - Bottom";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        Session["singleview"] = "false";

        this.trSearch.Visible = true;
        this.trSearch1.Visible = true;
        this.dvProdListView.Visible = true;
        this.dvImageBanners.Visible = false;

        this.trDetails.Visible = false;

        Session["ItemAndAddToQlistValues"] = lblDetailPartNo.Text.Replace("Part # ", "") + "|" + btnAddToQList.Text;

        if (OnUserPrefModeChanged != null)
        {
            Session["UIisOnInventoryDetail"] = false;
            OnUserPrefModeChanged(false);
        }

        if (Session["CurrentItemDetailUOM"] != null)
        {
            string[] stra = ((string)Session["CurrentItemDetailUOM"]).Split('|');
            FindItemAndSetUOM(stra[0], stra[1]);
            Session["CurrentItemDetailUOM"] = null;
        }

        txtDetailItemQty.Text = "";

        try
        {
            FetchAllDataForChangedUOM("list", ddlDetailUOM.DataMember, Convert.ToInt32(ddlDetailUOM.SelectedItem.Value));
        }
        catch { } //needed because Convert.ToInt32(ddlDetailUOM.SelectedItem.Value) will fail on a specific length or sheet good item; i.e. value will be text, not numeric

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void FindItemAndSetUOM(string itemcode, string uompointer)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (lvProducts.Items.Count > 0)
        {
            for (int x = 0; x < lvProducts.Items.Count; x++)
            {
                string hdnItem = (lvProducts.Items[x].FindControl("hdnItem") as HiddenField).Value;
                if (hdnItem == itemcode)
                {
                    DropDownList ddluom = lvProducts.Items[x].FindControl("ddlUOM") as DropDownList;

                    for (int y = 0; y < ddluom.Items.Count; y++)
                    {
                        if (ddluom.Items[y].Value == uompointer)
                        {
                            ddluom.SelectedIndex = y;
                            break;
                        }
                    }

                    break;
                }
            }
        }

        if (lvNoImagesProductsList.Items.Count > 0)
        {
            for (int x = 0; x < lvNoImagesProductsList.Items.Count; x++)
            {
                string hdnItem = (lvNoImagesProductsList.Items[x].FindControl("hdnItem") as HiddenField).Value;
                if (hdnItem == itemcode)
                {
                    DropDownList ddluom = lvNoImagesProductsList.Items[x].FindControl("ddlUOM2") as DropDownList;

                    for (int y = 0; y < ddluom.Items.Count; y++)
                    {
                        if (ddluom.Items[y].Value == uompointer)
                        {
                            ddluom.SelectedIndex = y;
                            break;
                        }
                    }

                    break;
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// For adding to the cart
    /// </summary>
    /// <param name="qty"></param>
    /// <param name="item"></param>
    /// <param name="size"></param>
    /// <param name="description"></param>
    /// <param name="uom"></param>
    /// <param name="price"></param>
    /// <param name="priceUOM"></param>
    /// <param name="minpack"></param>
    /// <param name="extension"></param>
    /// <param name="thickness"></param>
    /// <param name="width"></param>
    /// <param name="length"></param>
    protected void CartAdd(decimal qty, string item, string size, string description, string uom, int qty_uom_conv_ptr, string qty_uom_conv_ptr_sysid, decimal price, string priceUOM, decimal minpack, decimal extension, decimal thickness, decimal width, decimal length)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        string branchID = ((dsCustomerDataSet.ttbranch_custRow)dm.GetCache("CurrentBranchRow")).branch_id;
        string userID = ((AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser")).GetContextValue("currentUserLogin");

        int ReturnCode;
        string MessageText;

        Shipto currentShipto = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        OrderCart myOrderCart = null;

        decimal markupFactorValue = 1;
        if (Session["MarkupFactor"] != null)
        {
            markupFactorValue = Convert.ToDecimal(Session["MarkupFactor"]);
        }

        if (dm.GetCache("OrderCart") != null)
            myOrderCart = (OrderCart)dm.GetCache("OrderCart");
        else
        {
            myOrderCart = new OrderCart(branchID, userID, currentShipto.ShiptoObj.ToString(), "<all>", markupFactorValue, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            dm.SetCache("OrderCart", myOrderCart);
        }

        // This session variable must be cleared when, branch, customer or shipto has changed.
        // The above myOrderCart will check the session variable first before called the back-end logic.
        // OrderForm.Reset() should clear this session variable.
        if (dm.GetCache("dsOrderCartDataSet") == null)
            dm.SetCache("dsOrderCartDataSet", myOrderCart.ReferencedDataSet);

        // NOTE: The price should be figured out when the order/cart line is created for display.
        //       The cart table doesn't store price because price could change.
        dsOrderCartDataSet.ttorder_cartDataTable cart = new dsOrderCartDataSet.ttorder_cartDataTable();
        dsOrderCartDataSet.ttorder_cartRow row = cart.Newttorder_cartRow();
        row.branch_id = branchID;
        row.user_id = userID;
        row.cust_obj = currentShipto.CustomerObj;
        row.shipto_obj = currentShipto.ShiptoObj;
        row.ITEM = item;
        row.line_message = "";
        row.SIZE = size;
        row.DESCRIPTION = description;
        row.qty = qty;
        row.uom = uom;
        row.qty_uom_conv_ptr = qty_uom_conv_ptr;
        row.qty_uom_conv_ptr_sysid = qty_uom_conv_ptr_sysid;
        row.price_uom = priceUOM;
        row.price = price;
        row.min_pak = minpack;
        row.extension = extension;
        row.thickness = thickness;
        row.WIDTH = width;
        row.LENGTH = length;
        row.location_reference = "";

        //MDM -- add record_type and control_no for keys
        row.record_type = "";
        row.control_no = 0;

        int ReturnCode2;
        string MessageText2;

        myOrderCart.AddToCart(row, out ReturnCode2, out MessageText2);

        if (ReturnCode2 == 2 && MessageText2.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        // This event should be caught by the parent then the parent should call a public method
        // of the Order Form to refresh its contents.
        if (this.OnAddToCart != null)
            this.OnAddToCart(row);

        currentShipto.Dispose();
        row = null;
        cart.Dispose();
        dm = null;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void QuoteDataSetAddItem(ref dsQuoteDataSet ds, decimal qty, string item, string size, string description, string uom, int qty_uom_conv_ptr, string qty_uom_conv_ptr_sysid, decimal price, string priceUOM, decimal minpack, decimal extension, decimal thickness, decimal width, decimal length)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string rowRetailMode = "";
        decimal rowMarkupFactor = 0;

        if (Session["UserPref_DisplaySetting"] != null)
        {
            if ((string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost")
            {
                rowRetailMode = "cost";
                rowMarkupFactor = Convert.ToDecimal(Session["MarkupFactor"]);
            }
            else if ((string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
            {
                rowRetailMode = "list";
                rowMarkupFactor = Convert.ToDecimal(Session["MarginFactor"]);
            }
        }

        dsInventoryDataSet dsInv = (dsInventoryDataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];
        DataRow[] rows = dsInv.ttinventory.Select("item='" + item + "' AND thickness=" + thickness.ToString() + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString());

        dsQuoteDataSet.pvttquote_detailRow row = ds.pvttquote_detail.Newpvttquote_detailRow();
        row.system_id = (string)ds.pvttquote_header.Rows[0]["system_id"];
        row.ITEM_ptr = (int)rows[0]["item_ptr"];
        row.quote_id = (int)ds.pvttquote_header.Rows[0]["quote_id"];
        row.sequence = 0;
        row.ITEM = item;
        row.SIZE = size;
        row.quote_desc = description;
        row.qty_ordered = qty;
        row.uom = uom;
        row.ordqty_uom_conv_ptr = qty_uom_conv_ptr;
        row.ordqty_uom_conv_ptr_sysid = qty_uom_conv_ptr_sysid;
        row.price = price;
        row.retail_mode = rowRetailMode;
        row.extended_price = extension;
        row.thickness = thickness;
        row.width = width;
        row.length = length;
        row.location_reference = "";
        row.retail_price_calc_factor = rowMarkupFactor;

        ds.pvttquote_detail.Rows.Add(row);
        ds.AcceptChanges();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// For adding the quick list details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAddToQList_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        dsInventoryDataSet.ttinventoryRow inventoryRow = null;
        if (Session[this.Mode.ToString() + "CachedDetailInventoryRow"] != null)
            inventoryRow = (dsInventoryDataSet.ttinventoryRow)Session[this.Mode.ToString() + "CachedDetailInventoryRow"];

        if (inventoryRow != null)
        {
            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            string item = "";
            decimal thickness = 0, width = 0, length = 0;

            item = inventoryRow.ITEM;
            thickness = inventoryRow.thickness;
            width = inventoryRow.WIDTH;
            length = inventoryRow.LENGTH;

            string quoteSR_Item = QuoteSearchReplace(item);

            Session[this.Mode.ToString() + "CachedInventoryDataSet"] = Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"];
            dsInventoryDataSet dsInv = (dsInventoryDataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];

            Dmsi.Agility.Data.Shipto currentShipto = new Shipto(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
            Branch branch = new Branch();
            DataRow[] rows = null;

            if (!DBNull.Value.Equals(dsInv))
            {
                if (dsInv.Tables.Count > 0)
                {
                    if (dsInv.Tables["ttInventory"].Rows.Count > 0)
                    {
                        rows = dsInv.Tables["ttInventory"].Select("ITEM='" + quoteSR_Item + "' AND thickness=" + thickness.ToString() + " AND WIDTH=" + width.ToString() + " AND LENGTH=" + length.ToString());
                    }
                }

                if (rows != null)
                {
                    if (rows.Length > 0)
                    {
                        if (btnAddToQList.Text.Contains("Remove"))
                            rows[0]["quick_list"] = false;
                        else
                            rows[0]["quick_list"] = true;
                    }
                }

                /* Analytics Tracking */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Quotes - Search Button";
                aInput.Label = "";
                aInput.Value = "0";


                if (btnAddToQList.Text == "+ Add to my quicklist")
                {
                    aInput.Action = "Product Details - Add to Quicklist";
                    btnAddToQList.Text = "- Remove from my quicklist";
                }
                else
                {
                    aInput.Action = "Product Details - Remove from Quicklist";
                    btnAddToQList.Text = "+ Add to my quicklist";
                }

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

                dsInv.AcceptChanges();

                inventory.UpdateQuickList(branch.BranchId, user.GetContextValue("currentUserLogin"), currentShipto.ShiptoObj, dsInv, out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                dm.SetCache(this.Mode.ToString() + "dsInventoryDataSet", dsInv);

                Session[this.Mode.ToString() + "CachedInventoryDataSet"] = dsInv;
                Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = dsInv;

                if (!DBNull.Value.Equals(dsInv))
                {
                    if (dsInv.Tables.Count > 0)
                    {
                        if (dsInv.Tables["ttInventory"].Rows.Count > 0)
                        {
                            lblErrorMessage.Text = "";
                            BindProductGridViewDetails(dsInv.Tables["ttInventory"], 1);
                        }
                    }
                }
            }

            inventory.Dispose();
            branch.Dispose();
            currentShipto.Dispose();

            if (AvailableParentDiv.Visible == true || OnOrderParentDiv.Visible == true || tableXrefs.Visible == true || tableDocuments.Visible == true)
                PricingParentDiv.Visible = false;
        }

        if (((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 1) || ((int)Session[this.Mode.ToString() + "btnReturnToItem_Click"] == 0))
        {
            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 5;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Selected Index change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        PricingParentDiv.Visible = false;
        if ((string)Session["GenericLogin"].ToString().ToLower() == "false")
        {
            Profile.ddlInventoryAvailableRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);
        }

        int pageSize;

        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
            pageSize = 32000;
        else
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);

        gvInventoryAvailableTally.PageSize = pageSize;
        this.AssignAvailableTallyDataSource(Data.Cached);

        gvInventoryAvailableBranch.PageSize = pageSize;
        this.AssignAvailableBranchDataSource(Data.Cached);

        /* Analytics Tracking  */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Product Details - Available - Show Rows Selection";
        aInput.Label = ((System.Web.UI.WebControls.DropDownList)sender).SelectedItem.ToString();
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void ddlLevelAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        PricingParentDiv.Visible = false;

        switch (ddlLevelAvailable.SelectedValue)
        {
            case "Totals":
                gvInventoryAvailableTotal.Visible = true;
                gvInventoryAvailableBranch.Visible = false;
                gvInventoryAvailableTally.Visible = false;
                gvInventoryAvailableTallyDetail.Visible = false;
                break;

            case "Totals By Branch":
                gvInventoryAvailableTotal.Visible = false;
                gvInventoryAvailableBranch.Visible = true;
                gvInventoryAvailableTally.Visible = false;
                gvInventoryAvailableTallyDetail.Visible = false;
                break;

            case "Inventory Details":
                gvInventoryAvailableTotal.Visible = false;
                gvInventoryAvailableBranch.Visible = false;
                gvInventoryAvailableTally.Visible = true;
                gvInventoryAvailableTallyDetail.Visible = false;
                break;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void btnSaveXref_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (txtInputXref.Text.Length > 0)
        {
            string searchCriteria = (string)Session[this.Mode.ToString() + "CachedInventoryDetailSearchCriteria"];

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

            string addtoSearchCriteria = "";

            if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
            {
                DataRow[] rows = dsPV.bPV_Action.Select("token_code='view_system_part_numbers'");

                if (rows.Length == 0) //no allocation
                {
                    addtoSearchCriteria = "no";
                }
                else
                    addtoSearchCriteria = "yes";
            }

            searchCriteria = searchCriteria + "\x003" + "lViewXrefs=" + addtoSearchCriteria;

            string opcMessage = inv.AddItemXref(searchCriteria, txtInputXref.Text, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            /* Analytics Tracking  */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Product Details - Part Numbers Save Button";
            aInput.Label = "";
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

            dsPVItemXrefDataSet ds = inv.XrefDataSet;
            Session[this.Mode.ToString() + "CachedItemDetailXrefDataSet"] = ds;

            dm.SetCache(this.Mode.ToString() + "gvItemXrefsSortColumn", "xref_num");
            dm.SetCache(this.Mode.ToString() + "gvItemXrefsSortDirection", " ASC");

            this.AssignXrefDataSource();

            if (opcMessage.Length > 0)
            {
                lblAddXrefMessage.Text = opcMessage;
            }
            else
            {
                lblAddXrefMessage.Text = "";
                txtInputXref.Text = "";
            }

            PricingParentDiv.Visible = false;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void WebDataMenu1_ItemClick(object sender, Infragistics.Web.UI.NavigationControls.DataMenuItemEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataManager dm = new DataManager();

        foreach (DataMenuItem it in WebDataMenu1.Items)
        {
            if (it.Text != e.Item.Text)
                it.CssClass = "";
        }

        switch (e.Item.Text.ToLower())
        {
            case "documents":
                LoadItemDocuments();
                PricingParentDiv.Visible = false;
                AvailableParentDiv.Visible = false;
                OnOrderParentDiv.Visible = false;
                tableXrefs.Visible = false;
                tableDocuments.Visible = true;
                break;

            case "part numbers":
                LoadItemXrefs();
                PricingParentDiv.Visible = false;
                AvailableParentDiv.Visible = false;
                OnOrderParentDiv.Visible = false;
                tableDocuments.Visible = false;

                if (((string)Session["GenericLogin"]).ToLower() == "true")
                    gvItemXrefs.Columns[0].Visible = false;
                else
                    gvItemXrefs.Columns[0].Visible = true;

                break;

            case "pricing":
                PricingParentDiv.Visible = true;
                tableXrefs.Visible = false;
                tableDocuments.Visible = false;
                AvailableParentDiv.Visible = false;
                OnOrderParentDiv.Visible = false;
                break;

            //MDM -- price detail grid action allocation mod
            case "available":
                FetchAvailable();
                break;
            //MDM -- price detail grid action allocation mod

            case "on order":
                OnOrderParentDiv.Visible = true;
                AvailableParentDiv.Visible = false;
                PricingParentDiv.Visible = false;
                tableXrefs.Visible = false;
                tableDocuments.Visible = false;

                if (dm.GetCache(this.Mode.ToString() + "dsInventoryOnOrderTotalDataSet") != null)
                    this.AssignOnOrderTotalDataSource(Data.Cached);
                else
                    this.AssignOnOrderTotalDataSource(Data.Fetch);

                if (Session["Show_OnOrder_By_Branch"] != null && (bool)Session["Show_OnOrder_By_Branch"])
                {
                    if (dm.GetCache(this.Mode.ToString() + "dsInventoryOnOrderBranchDataSet") != null)
                        this.AssignOnOrderBranchDataSource(Data.Cached);
                    else
                        this.AssignOnOrderBranchDataSource(Data.Fetch);
                }

                if (Session["Show_OnOrder_Detail"] != null && (bool)Session["Show_OnOrder_Detail"])
                {
                    if (dm.GetCache(this.Mode.ToString() + "dsInventoryOnOrderTranDataSet") != null)
                        this.AssignOnOrderTranDataSource(Data.Cached);
                    else
                        this.AssignOnOrderTranDataSource(Data.Fetch);
                }
                break;
        }

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Product Details - Tab Click";
        aInput.Label = e.Item.Text;
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #region OnOrderTranDetail Inventory Grid

    protected void gvOnOrderTranDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvOnOrderTranDetail.PageIndex = e.NewPageIndex;
        this.AssignOnOrderTranDetailDataSource(Data.Cached, (string)Session[this.Mode.ToString() + "OnOrderTranFilter"]);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderTranDetail_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvOnOrderTranDetail, e.Row, "gvInventoryOnOrderTranDetailSortColumn", "gvInventoryOnOrderTranDetailSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderTranDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvOnOrderTranDetail");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderTranDetail_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryOnOrderTranDetailSortColumn", "gvInventoryOnOrderTranDetailSortDirection");
        this.AssignOnOrderTranDetailDataSource(Data.Cached, (string)Session[this.Mode.ToString() + "OnOrderTranFilter"]);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    protected void ddlLevelOnOrder_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        PricingParentDiv.Visible = false;

        switch (ddlLevelOnOrder.SelectedValue)
        {
            case "Totals":
                gvOnOrderTotal.Visible = true;
                gvOnOrderBranch.Visible = false;
                gvOnOrderTran.Visible = false;
                gvOnOrderTranDetail.Visible = false;
                break;

            case "Totals By Branch":
                gvOnOrderTotal.Visible = false;
                gvOnOrderBranch.Visible = true;
                gvOnOrderTran.Visible = false;
                gvOnOrderTranDetail.Visible = false;
                break;

            case "Transaction Details":
                gvOnOrderTotal.Visible = false;
                gvOnOrderBranch.Visible = false;
                gvOnOrderTran.Visible = true;
                gvOnOrderTranDetail.Visible = false;
                break;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void ddlOnOrder_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        PricingParentDiv.Visible = false;

        if ((string)Session["GenericLogin"].ToString().ToLower() == "false")
        {
            Profile.ddlInventoryOnOrderRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);
        }

        int pageSize;

        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
            pageSize = 32000;
        else
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);

        this.gvOnOrderTran.PageSize = pageSize;
        this.AssignOnOrderTranDataSource(Data.Cached);

        this.gvOnOrderBranch.PageSize = pageSize;
        this.AssignOnOrderBranchDataSource(Data.Cached);

        /* Analytics Tracking  */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Product Details - On Order - Show Rows Selection";
        aInput.Label = ((System.Web.UI.WebControls.DropDownList)sender).SelectedItem.ToString();
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();



#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void ddlPricing_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"].ToString().ToLower() == "false")
        {
            Profile.ddlInventoryPricingRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);
        }

        int pageSize;

        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
            pageSize = 32000;
        else
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);

        gvInventoryPricing.PageSize = pageSize;
        this.AssignPriceDataSource(Data.Cached);

        /* Analytics Tracking  */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Product Details - Pricing - Show Rows Selection";
        aInput.Label = ((System.Web.UI.WebControls.DropDownList)sender).SelectedItem.ToString();
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvInventoryAvailableBranch.PageIndex = e.NewPageIndex;
        this.AssignAvailableBranchDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableBranch_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvInventoryAvailableBranch, e.Row, "gvInventoryAvailableBranchSortColumn", "gvInventoryAvailableBranchSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableBranch_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvInventoryAvailableBranch");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableBranch_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryAvailableBranchSortColumn", "gvInventoryAvailableBranchSortDirection");
        this.AssignAvailableBranchDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableBranch_Load(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        foreach (DataControlField col in gvInventoryAvailableBranch.Columns)
        {
            switch (col.HeaderText)
            {
                case "Available":
                    col.Visible = _viewText;
                    break;

                case "Quantity":
                    col.Visible = _viewQuantity;
                    break;

                case "UOM":
                    if (_viewText)
                        col.Visible = false;
                    if (_viewQuantity)
                        col.Visible = true;
                    break;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #region AvailableTally Inventory Grid

    protected void gvInventoryAvailableTally_Load(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        foreach (DataControlField col in gvInventoryAvailableTally.Columns)
        {
            switch (col.HeaderText)
            {
                case "Available":
                    col.Visible = _viewText;
                    break;

                case "Quantity":
                    col.Visible = _viewQuantity;
                    break;

                case "UOM":
                    if (_viewText)
                        col.Visible = false;
                    if (_viewQuantity)
                        col.Visible = true;
                    break;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTally_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        gvInventoryAvailableTallyDetail.Visible = false;

        this.gvInventoryAvailableTally.PageIndex = e.NewPageIndex;
        this.AssignAvailableTallyDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTally_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvInventoryAvailableTally, e.Row, "gvInventoryAvailableTallySortColumn", "gvInventoryAvailableTallySortDirection", 1);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTally_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvInventoryAvailableTally");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTally_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryAvailableTallySortColumn", "gvInventoryAvailableTallySortDirection");
        this.AssignAvailableTallyDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTally_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        gvInventoryAvailableTallyDetail.Visible = true;

        GridView gv = (GridView)sender;
        Session[this.Mode.ToString() + "AvailableTallyFilter"] = gv.SelectedDataKey["tag"].ToString();

        this.AssignAvailableTallyDetailDataSource(Data.Fetch, (string)Session[Mode.ToString() + "AvailableTallyFilter"]);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region AvailableTallyDetail Inventory Grid

    protected void gvInventoryAvailableTallyDetail_Load(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        foreach (DataControlField col in gvInventoryAvailableTallyDetail.Columns)
        {
            switch (col.HeaderText)
            {
                case "Available":
                    col.Visible = _viewText;
                    break;

                case "Quantity":
                    col.Visible = _viewQuantity;
                    break;

                case "UOM":
                    if (_viewText)
                        col.Visible = false;
                    if (_viewQuantity)
                        col.Visible = true;
                    break;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTallyDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvInventoryAvailableTallyDetail.PageIndex = e.NewPageIndex;
        this.AssignAvailableTallyDetailDataSource(Data.Cached, (string)Session[this.Mode.ToString() + "AvailableTallyFilter"]);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTallyDetail_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvInventoryAvailableTallyDetail, e.Row, "gvInventoryAvailableTallyDetailSortColumn", "gvInventoryAvailableTallyDetailSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTallyDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvInventoryAvailableTallyDetail");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTallyDetail_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryAvailableTallyDetailSortColumn", "gvInventoryAvailableTallyDetailSortDirection");
        this.AssignAvailableTallyDetailDataSource(Data.Cached, (string)Session[this.Mode.ToString() + "AvailableTallyFilter"]);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region OnOrderTotal Inventory Grid

    protected void gvInventoryOnOrderTotal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvOnOrderTotal.PageIndex = e.NewPageIndex;
        this.AssignOnOrderTotalDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryOnOrderTotal_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvOnOrderTotal, e.Row, "gvInventoryOnOrderTotalSortColumn", "gvInventoryOnOrderTotalSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryOnOrderTotal_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvInventoryOnOrderTotal");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryOnOrderTotal_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryOnOrderTotalSortColumn", "gvInventoryOnOrderTotalSortDirection");
        this.AssignOnOrderTotalDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region OnOrderBranch Inventory Grid

    protected void gvOnOrderBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvOnOrderBranch.PageIndex = e.NewPageIndex;
        this.AssignOnOrderBranchDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderBranch_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvOnOrderBranch, e.Row, "gvInventoryOnOrderBranchSortColumn", "gvInventoryOnOrderBranchSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderBranch_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvOnOrderBranch");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderBranch_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryOnOrderBranchSortColumn", "gvInventoryOnOrderBranchSortDirection");
        this.AssignOnOrderBranchDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region OnOrderTran Inventory Grid

    protected void gvOnOrderTran_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        gvOnOrderTranDetail.Visible = false;

        this.gvOnOrderTran.PageIndex = e.NewPageIndex;
        this.AssignOnOrderTranDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderTran_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvOnOrderTran, e.Row, "gvInventoryOnOrderTranSortColumn", "gvInventoryOnOrderTranSortDirection", 1);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderTran_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvOnOrderTran");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderTran_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryOnOrderTranSortColumn", "gvInventoryOnOrderTranSortDirection");
        this.AssignOnOrderTranDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvOnOrderTran_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        gvOnOrderTranDetail.Visible = true;

        GridView gv = (GridView)sender;
        Session[this.Mode.ToString() + "OnOrderTranFilter"] = gv.SelectedDataKey["PARENT_key"].ToString();

        this.AssignOnOrderTranDetailDataSource(Data.Fetch, (string)Session[this.Mode.ToString() + "OnOrderTranFilter"]);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region AvailableTotal Inventory Grid

    protected void gvInventoryAvailableTotal_Load(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        foreach (DataControlField col in gvInventoryAvailableTotal.Columns)
        {
            switch (col.HeaderText)
            {
                case "Available":
                    col.Visible = _viewText;
                    break;

                case "Quantity":
                    col.Visible = _viewQuantity;
                    break;

                case "UOM":
                    if (_viewText)
                        col.Visible = false;
                    if (_viewQuantity)
                        col.Visible = true;
                    break;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTotal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvInventoryAvailableTotal.PageIndex = e.NewPageIndex;
        this.AssignAvailableTotalDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTotal_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvInventoryAvailableTotal, e.Row, "gvInventoryAvailableTotalSortColumn", "gvInventoryAvailableTotalSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTotal_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvInventoryAvailableTotal");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryAvailableTotal_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryAvailableTotalSortColumn", "gvInventoryAvailableTotalSortDirection");
        this.AssignAvailableTotalDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region Pricing Inventory Grid

    protected void gvInventoryPricing_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvInventoryPricing.PageIndex = e.NewPageIndex;
        this.AssignPriceDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryPricing_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvInventoryPricing, e.Row, "gvInventoryPriceSortColumn", "gvInventoryPriceSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryPricing_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvInventoryPricing");

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvInventoryPricing_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvInventoryPriceSortColumn", "gvInventoryPriceSortDirection");
        this.AssignPriceDataSource(Data.Cached);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region Item Xref Grid

    protected void gvItemXrefs_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        this.gvItemXrefs.PageIndex = e.NewPageIndex;
        this.AssignXrefDataSource();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvItemXrefs_RowCreated(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        //Use the RowType property to determine whether the 
        //row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvItemXrefs, e.Row, "gvItemXrefsSortColumn", "gvItemXrefsSortDirection", 0);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvItemXrefs_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
            ManipulateRow(e.Row, "small", "gvItemXrefs");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRow currRow = ((DataRowView)e.Row.DataItem).Row;

            if (currRow["xref_source"] != null)
            {
                if (currRow["xref_source"].ToString().ToLower() != "pv")
                {
                    if (e.Row.Cells[0].Controls.Count > 0)
                        e.Row.Cells[0].Controls[0].Visible = false;
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvItemXrefs_Sorting(object sender, GridViewSortEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Grid_Sorting((GridView)sender, e, "gvItemXrefsSortColumn", "gvItemXrefsSortDirection");
        this.AssignXrefDataSource();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvItemXrefs_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        //has to be here because the custom Delete button fires it automatically...

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void gvItemXrefs_RowCommand(object sender, GridViewCommandEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.CommandName == "Delete")
        {
            GridView gv = sender as GridView;
            if (gv != null)
            {
                int index = 0;
                bool parsed = false;
                if (e.CommandArgument as String != "")
                    parsed = Int32.TryParse(e.CommandArgument as String, out index);

                if (index > gv.Rows.Count - 1)
                    index = 0;

                GridViewRow selectedRow = gv.Rows[index];

                string s = selectedRow.Cells[1].Text;
                DeleteXref(s);

                /* Analytics Tracking  */
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Action = "Product Details - Part Number Delete Button";
                aInput.Label = "";
                aInput.Value = "0";

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

                PricingParentDiv.Visible = false;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    #region Item Document Grid

    protected void gvItemDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }

                DataRowView row = (DataRowView)e.Row.DataItem;

                if (i == 0)
                {
                    string newsb = "<a href=\"" + row["image_file"] + "\" target=\"_blank\">" + e.Row.Cells[i].Text + "</a>";
                    e.Row.Cells[i].Text = newsb;
                }
            }

            e.Row.Attributes.Add("onmouseover", "this.className='InventoryGridViewRowMouseHover'");
            e.Row.Attributes.Add("onmouseout", "this.className='GridViewRowMouseOut'");
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #endregion

    private void DeleteXref(string xref_number)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string searchCriteria = (string)Session[this.Mode.ToString() + "CachedInventoryDetailSearchCriteria"];

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        string addtoSearchCriteria = "";

        if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
        {
            DataRow[] rows = dsPV.bPV_Action.Select("token_code='view_system_part_numbers'");

            if (rows.Length == 0) //no allocation
            {
                addtoSearchCriteria = "no";
            }
            else
                addtoSearchCriteria = "yes";
        }

        searchCriteria = searchCriteria + "\x003" + "lViewXrefs=" + addtoSearchCriteria;

        string opcMessage = inv.DeleteItemXref(searchCriteria, xref_number, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        dsPVItemXrefDataSet ds = inv.XrefDataSet;
        Session[this.Mode.ToString() + "CachedItemDetailXrefDataSet"] = ds;

        dm.SetCache(this.Mode.ToString() + "gvItemXrefsSortColumn", "xref_num");
        dm.SetCache(this.Mode.ToString() + "gvItemXrefsSortDirection", " ASC");

        this.AssignXrefDataSource();

        if (opcMessage.Length > 0)
        {
            lblAddXrefMessage.Text = opcMessage;
        }
        else
        {
            lblAddXrefMessage.Text = "";
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #region DataSources

    /// <summary>
    /// Retrieve customer DataSet and setup GridView. This depends on several Session variables that 
    /// dictate the filter and sort of the Datasource. Reset those variables to re-evaluate the content of
    /// the GridView
    /// </summary>
    /// <param name="empty">Create an empty grid</param>
    private void AssignDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        lblErrorMessage.Text = "";
        string filter = this.BuildFilter();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryList = new dsInventoryDataSet();

        string sort = (string)dm.GetCache(this.Mode.ToString() + "gvInventorySortColumn");

        if (sort == null || sort.Length == 0)
            sort = "ITEM";
        else
            sort += dm.GetCache(this.Mode.ToString() + "gvInventorySortDirection");


        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventorySearchCriteria") == null)
        {
            inventoryList = new dsInventoryDataSet();
        }
        else
        {
            inventoryList = FetchDataSource(this.Mode.ToString() + "dsInventoryDataSet", this.Mode.ToString() + "InventorySearchCriteria", callMode);
        }

        Session[this.Mode.ToString() + "CachedInventoryDataSet"] = inventoryList;
        Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = inventoryList;

        DataView dv = new DataView(inventoryList.ttinventory, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttInventoryCopy");

        int productSearchQty = 100;

        if (Session["product_search_qty"] != null)
            productSearchQty = Convert.ToInt32(Session["product_search_qty"]);

        if (dt.Rows.Count > 0 && dt.Rows.Count < productSearchQty)
        {
            lblHeaderError.Visible = false;
        }

        if (Session["Ship-toChanged"] != null && (bool)Session["Ship-toChanged"] == true)
        {
            Session["Ship-toChanged"] = null;
            lblHeaderError.Visible = false;
        }

        if (dt.Rows.Count > productSearchQty - 1)
        {
            if (TabContainer1.ActiveTab == tbSearch)
            {
                lblHeaderError.Text = "Your search returned too many records to be displayed. Consider refining your search criteria.";
                lblHeaderError.Visible = true;
            }
            else
            {
                lblHeaderError.Text = "Your search returned too many records to be displayed. Consider refining your search criteria. For help click Search Tips.";
                lblHeaderError.Visible = true;
            }
        }

        if (dt.Rows.Count == 0)
        {
            if (Session[this.Mode.ToString() + "INVSearchCount"] != null && (int)Session[this.Mode.ToString() + "INVSearchCount"] == 2)
            {
                if (Session[this.Mode.ToString() + "btnFind_ClickCalled"] != null && (bool)Session[this.Mode.ToString() + "btnFind_ClickCalled"] == true)
                {
                    Session[this.Mode.ToString() + "btnFind_ClickCalled"] = false;

                    if (TabContainer1.ActiveTab == tbSearch)
                    {
                        lblHeaderError.Text = NORESULTSFOUND;
                    }
                    else
                    {
                        lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;

                    }
                    lblHeaderError.Visible = true;
                }
            }
            else
            {
                _inventorySearchCounter++;
                Session[this.Mode.ToString() + "INVSearchCount"] = _inventorySearchCounter;
                lblHeaderError.Visible = false;
            }
        }
        else
        {
            if (TabContainer1.ActiveTab == tbSearch)
            {
                lblHeaderError.Text = "Your search returned too many records to be displayed. Consider refining your search criteria.";
            }
            else
            {
                lblHeaderError.Text = "Your search returned too many records to be displayed. Consider refining your search criteria. For help click Search Tips.";
            }
        }

        // "NoInventoryAvailable" is used during post back to make sure that the grid header is displayed
        if (dv.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryAvailable");

            BindProductGridViewDetails(dt, 1);

            this.dvProdListView.Visible = true;
            this.dvImageBanners.Visible = false;
            Label2.Visible = true;
        }
        else
        {
            DataTable dtempty = new DataTable();
            dtempty = BindEmptyData();
            lvNoImagesProductsList.Items.Clear();
            lvNoImagesProductsList.DataSource = dtempty;
            lvNoImagesProductsList.DataBind();

            if (TabContainer1.ActiveTab == tbSearch)
            {
                lblHeaderError.Text = NORESULTSFOUND;
            }
            else
            {
                lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
            }

            lblHeaderError.Visible = true;
            lblSort.Visible = true;
            Label2.Visible = true;
            ddlHeader.Visible = true;
            lvProducts.Items.Clear();
            lvProducts.DataSource = dtempty;
            lvProducts.DataBind();
            dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
            dm.SetCache(this.Mode.ToString() + "NoInventoryAvailable", true);

            dvImageBanners.Visible = false;
            dvProdListView.Visible = true;
            this.trSearch.Visible = true;
            this.trSearch1.Visible = true;
            this.trDetails.Visible = false;
            Label2.Visible = true;
            ddlHeader.Visible = true;
            lvPaging.Visible = false;
            lblpageno.Visible = false;
            dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
        }

        if (!Page.IsPostBack)
        {
            lblHeaderError.Visible = false;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Get paging related data
    /// </summary>
    /// <param name="dtPagingData"></param>
    /// <param name="iPageno"></param>
    /// <param name="iNoOfRecords"></param>
    /// <returns></returns>
    private DataTable PagingData(DataTable dtPagingData, int iPageno, int iNoOfRecords)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataTable dtReturnPagingData = new DataTable();

        if (dtPagingData.Rows.Count > 0)
        {
            int iMinValue, iMaxRange;
            iMinValue = (iPageno - 1) * iNoOfRecords;

            if (iMinValue == 0)
            {
                iMaxRange = (iMinValue + 1) * iNoOfRecords;
            }
            else
            {
                iMaxRange = iNoOfRecords;
            }

            var distinctRows = (from DataRow dRow in dtPagingData.Rows
                                select dRow).Skip(iMinValue).Take(iMaxRange);

            try
            {
                dtReturnPagingData = distinctRows.ToList().AsEnumerable().CopyToDataTable();
            }
            catch
            {
                distinctRows = (from DataRow dRow in dtPagingData.Rows
                                select dRow).Skip(0).Take(iNoOfRecords);
                dtReturnPagingData = distinctRows.ToList().AsEnumerable().CopyToDataTable();
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return dtReturnPagingData;
    }

    /// <summary>
    /// For paging of Product details
    /// </summary>
    /// <param name="dtProducts"></param>
    /// <returns></returns>
    private DataTable PagerPartition(DataTable dtProducts)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        int iTotalCount = dtProducts.Rows.Count;
        int iPageDivident = iTotalCount / Convert.ToInt16(ddlHeader.SelectedValue);
        int iDivident = iTotalCount % Convert.ToInt16(ddlHeader.SelectedValue);

        if (iDivident != 0)
        {
            iPageDivident = iPageDivident + 1;
        }

        DataTable dtPaging = new DataTable();
        dtPaging.Columns.Add("PageNo");

        for (int iPageCount = 1; iPageCount <= iPageDivident; iPageCount++)
        {
            DataRow drPage;
            drPage = dtPaging.NewRow();
            drPage["PageNo"] = iPageCount;
            dtPaging.Rows.Add(drPage);
            dtPaging.AcceptChanges();
            dtPaging.GetChanges();
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return dtPaging;
    }

    private void AssignItemDocumentsDataSource()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        dsItemImageDocDataSet ds = (dsItemImageDocDataSet)Session[this.Mode.ToString() + "CachedItemDocumentsDataSet"];
        DataView dv = new DataView();
        dv.Table = ds.ttItemImageDoc;
        dv.RowFilter = "(image_file like 'http*') OR (image_file like 'https*')";
        gvItemDocuments.DataSource = dv;
        gvItemDocuments.DataBind();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignXrefDataSource()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string filter = "";
        dsPVItemXrefDataSet ds = (dsPVItemXrefDataSet)Session[this.Mode.ToString() + "CachedItemDetailXrefDataSet"];
        bool NoDimensionData = true;

        foreach (DataRow row in ds.ttPVItem_xref.Rows)
        {
            if (row["dimension"].ToString().Length > 0)
            {
                NoDimensionData = false;
                break;
            }
        }

        if (NoDimensionData)
            gvItemXrefs.Columns[2].Visible = false;

        string sort = (string)dm.GetCache(this.Mode.ToString() + "gvItemXrefsSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "xref_num";
        else
            sort += dm.GetCache(this.Mode.ToString() + "gvItemXrefsSortDirection");

        DataView dv = new DataView(ds.ttPVItem_xref, filter, sort, DataViewRowState.CurrentRows);
        DataTable dt = dv.ToTable("ttPVItem_xrefCopy");

        gvItemXrefs.DataSource = dt;
        gvItemXrefs.DataBind();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignPriceDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string filterPrice = "ITEM LIKE '%'";

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        //setup sort for Price
        string sortPrice = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryPriceSortColumn");
        if (sortPrice == null || sortPrice.Length == 0)
            sortPrice = "saleType";
        else
            sortPrice += (string)dm.GetCache(this.Mode.ToString() + "gvInventoryPriceSortDirection");

        //sorts for price_type
        if (sortPrice == "price_type ASC")
        {
            sortPrice = "ITEM ASC, recordType ASC, PromoHierarchy ASC, orderHierarchy ASC, saleType ASC, StartDate DESC, endDate ASC, LEVEL ASC, qtyBreak ASC, iShiptoSort ASC, custShipToNum ASC, LENGTH ASC";
        }

        if (sortPrice == "price_type DESC")
        {
            sortPrice = "ITEM ASC, recordType DESC, PromoHierarchy DESC, orderHierarchy DESC, saleType DESC, StartDate ASC, endDate DESC, LEVEL DESC, qtyBreak DESC, iShiptoSort DESC, custShipToNum DESC, LENGTH DESC";
        }

        //setup sort for UOM
        string sortUOM = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryUOMSortColumn");

        if (sortUOM == null || sortUOM.Length == 0)
            sortUOM = "conv_factor";
        else
            sortUOM += (string)dm.GetCache(this.Mode.ToString() + "gvInventoryUOMSortDirection");

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryPriceSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryPriceDataSet", this.Mode.ToString() + "InventoryPriceSearchCriteria", callMode);

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            foreach (DataControlField col in this.gvInventoryPricing.Columns)
            {
                if (col.HeaderText == "Retail Price")
                {
                    col.Visible = true;
                }

                if (col.HeaderText == "Price")
                {
                    col.Visible = false;
                }
            }
        }
        else
        {
            foreach (DataControlField col in this.gvInventoryPricing.Columns)
            {
                if (col.HeaderText == "Retail Price")
                {
                    col.Visible = false;
                }

                if (col.HeaderText == "Price")
                {
                    col.Visible = true;
                }
            }
        }

        //DV for Price
        DataView dvPrice = new DataView(inventoryDetailList.ttPriceInfo, filterPrice, sortPrice, DataViewRowState.CurrentRows);
        DataTable dtPrice = dvPrice.ToTable("ttPriceInfoCopy");

        // "NoInventoryPriceAvailable" is used during post back to make sure that the grid header is displayed
        if (dvPrice.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryPriceAvailable");
            int pageSize;

            if (((System.Web.UI.WebControls.DropDownList)ddlPricing).SelectedValue == "All")
                pageSize = 32000;
            else
                pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)ddlPricing).SelectedValue);

            if (pageSize == 5)
            {
                this.gvInventoryPricing.PageSize = pageSize;
            }
            else
            {
                this.gvInventoryPricing.PageSize = pageSize;
            }

            this.gvInventoryPricing.DataSource = dtPrice;
            this.gvInventoryPricing.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryPriceAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtPrice, this.gvInventoryPricing);
        }
        //End of DV for Price

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignAvailableTotalDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryAvailableTotalSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryAvailableTotalDataSet", this.Mode.ToString() + "InventoryAvailableTotalSearchCriteria", callMode);

        //DV for AvailableTotal
        DataView dvAvailableTotal = new DataView(inventoryDetailList.ttavail, "", "", DataViewRowState.CurrentRows);
        DataTable dtAvailableTotal = dvAvailableTotal.ToTable("ttAvailableTotalInfoCopy");

        // "NoInventoryAvailableTotalAvailable" is used during post back to make sure that the grid header is displayed
        if (dvAvailableTotal.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryAvailableTotalAvailable");
            this.gvInventoryAvailableTotal.DataSource = dtAvailableTotal;
            this.gvInventoryAvailableTotal.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryAvailableTotalAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtAvailableTotal, this.gvInventoryAvailableTotal);
        }
        //End of DV for AvailableTotal

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignAvailableBranchDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        //setup sort for Branch
        string sortBranch = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryAvailableBranchSortColumn");

        if (sortBranch == null || sortBranch.Length == 0)
            sortBranch = "branch_id";
        else
            sortBranch += dm.GetCache(this.Mode.ToString() + "gvInventoryAvailableBranchSortDirection");

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryAvailableBranchSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryAvailableBranchDataSet", this.Mode.ToString() + "InventoryAvailableBranchSearchCriteria", callMode);

        //DV for AvailableBranch
        DataView dvAvailableBranch = new DataView(inventoryDetailList.ttavail_branch, "", sortBranch, DataViewRowState.CurrentRows);
        DataTable dtAvailableBranch = dvAvailableBranch.ToTable("ttAvailableBranchInfoCopy");

        // "NoInventoryAvailableBranchAvailable" is used during post back to make sure that the grid header is displayed
        if (dvAvailableBranch.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryAvailableBranchAvailable");
            this.gvInventoryAvailableBranch.DataSource = dtAvailableBranch;
            this.gvInventoryAvailableBranch.DataBind();

        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryAvailableBranchAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtAvailableBranch, this.gvInventoryAvailableBranch);
        }
        //End of DV for AvailableBranch

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignAvailableTallyDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        //setup sort for AvailableTally
        string sortTally = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryAvailableTallySortColumn");

        if (sortTally == null || sortTally.Length == 0)
            sortTally = "tag";
        else
            sortTally += dm.GetCache(this.Mode.ToString() + "gvInventoryAvailableTallySortDirection");

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryAvailableTallySearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryAvailableTallyDataSet", this.Mode.ToString() + "InventoryAvailableTallySearchCriteria", callMode);

        //DV for AvailableTally
        DataView dvAvailableTally = new DataView(inventoryDetailList.ttavail_header, "", sortTally, DataViewRowState.CurrentRows);
        DataTable dtAvailableTally = dvAvailableTally.ToTable("ttAvailableTallyInfoCopy");

        // "NoInventoryAvailableTallyAvailable" is used during post back to make sure that the grid header is displayed
        if (dvAvailableTally.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryAvailableTallyAvailable");
            this.gvInventoryAvailableTally.DataSource = dtAvailableTally;
            this.gvInventoryAvailableTally.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryAvailableTallyAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtAvailableTally, this.gvInventoryAvailableTally);
        }
        //End of DV for AvailableTally

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignAvailableTallyDetailDataSource(Data callMode, string Tag)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string filter = "tag LIKE '" + Tag + "'";

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        //setup sort for AvailableTallyDetail
        string sortTallyDetail = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryAvailableTallyDetailSortColumn");

        if (sortTallyDetail == null || sortTallyDetail.Length == 0)
            sortTallyDetail = "LENGTH";
        else
            sortTallyDetail += dm.GetCache(this.Mode.ToString() + "gvInventoryAvailableTallyDetailSortDirection");

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryAvailableTallyDetailSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryAvailableTallyDetailDataSet", this.Mode.ToString() + "InventoryAvailableTallyDetailSearchCriteria", callMode);

        //DV for AvailableTallyDetail
        DataView dvAvailableTallyDetail = new DataView(inventoryDetailList.ttavail_detail, filter, sortTallyDetail, DataViewRowState.CurrentRows);
        DataTable dtAvailableTallyDetail = dvAvailableTallyDetail.ToTable("ttAvailableTallyDetailInfoCopy");

        // "NoInventoryAvailableTallyDetailAvailable" is used during post back to make sure that the grid header is displayed
        if (dvAvailableTallyDetail.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryAvailableTallyDetailAvailable");
            this.gvInventoryAvailableTallyDetail.DataSource = dtAvailableTallyDetail;
            this.gvInventoryAvailableTallyDetail.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryAvailableTallyDetailAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtAvailableTallyDetail, this.gvInventoryAvailableTallyDetail);
        }
        //End of DV for AvailableTallyDetail

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignOnOrderTotalDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryOnOrderTotalSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryOnOrderTotalDataSet", this.Mode.ToString() + "InventoryOnOrderTotalSearchCriteria", callMode);

        //DV for OnOrderTotal
        DataView dvOnOrderTotal = new DataView(inventoryDetailList.tton_order, "", "", DataViewRowState.CurrentRows);
        DataTable dtOnOrderTotal = dvOnOrderTotal.ToTable("ttOnOrderTotalInfoCopy");

        // "NoInventoryOnOrderTotalAvailable" is used during post back to make sure that the grid header is displayed
        if (dvOnOrderTotal.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryOnOrderTotalAvailable");
            this.gvOnOrderTotal.DataSource = dtOnOrderTotal;
            this.gvOnOrderTotal.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryOnOrderTotalAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtOnOrderTotal, this.gvOnOrderTotal);
        }
        //End of DV for OnOrderTotal

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignOnOrderBranchDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        //setup sort for Branch
        string sortBranch = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryOnOrderBranchSortColumn");

        if (sortBranch == null || sortBranch.Length == 0)
            sortBranch = "branch_id";
        else
            sortBranch += dm.GetCache(this.Mode.ToString() + "gvInventoryOnOrderBranchSortDirection");

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryOnOrderBranchSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryOnOrderBranchDataSet", this.Mode.ToString() + "InventoryOnOrderBranchSearchCriteria", callMode);

        //DV for OnOrderBranch
        DataView dvOnOrderBranch = new DataView(inventoryDetailList.tton_order_branch, "", sortBranch, DataViewRowState.CurrentRows);
        DataTable dtOnOrderBranch = dvOnOrderBranch.ToTable("ttOnOrderBranchInfoCopy");

        // "NoInventoryOnOrderBranchAvailable" is used during post back to make sure that the grid header is displayed
        if (dvOnOrderBranch.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryOnOrderBranchAvailable");
            this.gvOnOrderBranch.DataSource = dtOnOrderBranch;
            this.gvOnOrderBranch.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryOnOrderBranchAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtOnOrderBranch, this.gvOnOrderBranch);
        }
        //End of DV for OnOrderBranch

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignOnOrderTranDataSource(Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        //setup sort for OnOrderTran
        string sortTran = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryOnOrderTranSortColumn");

        if (sortTran == null || sortTran.Length == 0)
            sortTran = "tran_id";
        else
            sortTran += dm.GetCache(this.Mode.ToString() + "gvInventoryOnOrderTranSortDirection");

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryOnOrderTranSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryOnOrderTranDataSet", this.Mode.ToString() + "InventoryOnOrderTranSearchCriteria", callMode);

        //DV for OnOrderTran
        DataView dvOnOrderTran = new DataView(inventoryDetailList.tton_order_header, "", sortTran, DataViewRowState.CurrentRows);
        DataTable dtOnOrderTran = dvOnOrderTran.ToTable("ttOnOrderTranInfoCopy");

        // "NoInventoryOnOrderTranAvailable" is used during post back to make sure that the grid header is displayed
        if (dvOnOrderTran.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryOnOrderTranAvailable");
            this.gvOnOrderTran.DataSource = dtOnOrderTran;
            this.gvOnOrderTran.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryOnOrderTranAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtOnOrderTran, this.gvOnOrderTran);
        }
        //End of DV for OnOrderTran

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void AssignOnOrderTranDetailDataSource(Data callMode, string parentKey)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string filter = "PARENT_key='" + parentKey + "'";

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList;

        //setup sort for OnOrderTranDetail
        string sortTranDetail = (string)dm.GetCache(this.Mode.ToString() + "gvInventoryOnOrderTranDetailSortColumn");

        if (sortTranDetail == null || sortTranDetail.Length == 0)
            sortTranDetail = "LENGTH";
        else
            sortTranDetail += dm.GetCache(this.Mode.ToString() + "gvInventoryOnOrderTranDetailSortDirection");

        if (callMode == Data.New || dm.GetCache(this.Mode.ToString() + "InventoryOnOrderTranDetailSearchCriteria") == null)
            inventoryDetailList = new dsInventoryDataSet();
        else
            inventoryDetailList = FetchDataSource(this.Mode.ToString() + "dsInventoryOnOrderTranDetailDataSet", this.Mode.ToString() + "InventoryOnOrderTranDetailSearchCriteria", callMode);

        //DV for OnOrderTranDetail
        DataView dvOnOrderTranDetail = new DataView(inventoryDetailList.tton_order_detail, filter, sortTranDetail, DataViewRowState.CurrentRows);
        DataTable dtOnOrderTranDetail = dvOnOrderTranDetail.ToTable("ttOnOrderTranDetailInfoCopy");

        // "NoInventoryOnOrderTranDetailAvailable" is used during post back to make sure that the grid header is displayed
        if (dvOnOrderTranDetail.Count > 0)
        {
            dm.RemoveCache(this.Mode.ToString() + "NoInventoryOnOrderTranDetailAvailable");
            this.gvOnOrderTranDetail.DataSource = dtOnOrderTranDetail;
            this.gvOnOrderTranDetail.DataBind();
        }
        else
        {
            dm.SetCache(this.Mode.ToString() + "NoInventoryOnOrderTranDetailAvailable", true);
            Dmsi.Agility.EntryNET.WebGridViewManager.CreateEmptyGridView(dtOnOrderTranDetail, this.gvOnOrderTranDetail);
        }
        //End of DV for OnOrderTranDetail

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet FetchDataSource(string sessionVariable, string searchSessionVariable, Data callMode)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet inventoryDetailList = new dsInventoryDataSet();
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (dm.GetCache(sessionVariable) != null && callMode == Data.Cached)
        {
            inventoryDetailList = (dsInventoryDataSet)dm.GetCache(sessionVariable);
        }

        if (callMode == Data.Fetch)
        {
            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");
            string searchCriteria = (string)dm.GetCache(searchSessionVariable);

            if (searchCriteria.Length == 0 && callMode == Data.Fetch)
            {
                throw (new Exception("Search criteria must be supplied to do a back-end fetch."));
            }

            int ReturnCode = 0;
            string MessageText = "";

            Dmsi.Agility.Data.Inventory inventory = new Dmsi.Agility.Data.Inventory(searchCriteria, dsPV, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            inventoryDetailList = (Dmsi.Agility.EntryNET.StrongTypesNS.dsInventoryDataSet)inventory.ReferencedDataSet;

            dm.SetCache(sessionVariable, inventoryDetailList);
            inventory.Dispose();
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return inventoryDetailList;
    }

    #endregion

    #region "Methods"

    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey, int columnOffset)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey, columnOffset);

        if (sortColumnIndex != -1)
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private int GetSortColumnIndex(GridView gv, string sessionSortColKey, int columnOffset)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + columnOffset;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return -1;
    }

    // This is a helper method used to add a sort direction
    // image to the header of the column being sorted.
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();

        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
        ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

        // Add the image to the appropriate header cell.
        imageLink.CommandName = "Sort";
        imageLink.CommandArgument = (string)Session[sessionSortColKey];
        imageLink.Controls.Add(sortImage);
        headerRow.Cells[columnIndex].Controls.Add(imageLink);

        imageLink.Dispose();
        sortImage.Dispose();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void Grid_Sorting(GridView gv, GridViewSortEventArgs e, string sortColumn, string sortDirection)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session[sortColumn] != e.SortExpression)
        {
            Session[sortDirection] = " ASC";
        }
        else
        {
            if ((string)Session[sortDirection] == " ASC")
            {
                Session[sortDirection] = " DESC";
            }
            else
            {
                Session[sortDirection] = " ASC";
            }
        }

        gv.PageIndex = 0;
        Session[sortColumn] = e.SortExpression;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void LoadItemDocuments()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (Session[this.Mode.ToString() + "CachedItemDocumentsDataSet"] == null)
        {
            string itemCode = (string)_dm.GetCache(this.Mode.ToString() + "CurrentItemCode");

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            inv.FetchItemImagesOrDocuments("Document", itemCode, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            dsItemImageDocDataSet ds = inv.ReferencedItemDocDataSet;
            Session[this.Mode.ToString() + "CachedItemDocumentsDataSet"] = ds;
        }

        tableDocuments.Visible = true;
        this.AssignItemDocumentsDataSource();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void LoadItemXrefs()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (Session[this.Mode.ToString() + "CachedInventoryDetailSearchCriteria"] != null)
        {
            string searchCriteria = (string)Session[this.Mode.ToString() + "CachedInventoryDetailSearchCriteria"];

            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

            string addtoSearchCriteria = "";

            if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
            {
                DataRow[] rows = dsPV.bPV_Action.Select("token_code='view_system_part_numbers'");

                if (rows.Length == 0) //no allocation
                {
                    addtoSearchCriteria = "no";
                }
                else
                    addtoSearchCriteria = "yes";
            }

            searchCriteria = searchCriteria + "\x003" + "lViewXrefs=" + addtoSearchCriteria;

            inv.FetchItemXrefs(searchCriteria, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            dsPVItemXrefDataSet ds = inv.XrefDataSet;
            Session[this.Mode.ToString() + "CachedItemDetailXrefDataSet"] = ds;

            dm.SetCache(this.Mode.ToString() + "gvItemXrefsSortColumn", "xref_num");
            dm.SetCache(this.Mode.ToString() + "gvItemXrefsSortDirection", " ASC");

            tableXrefs.Visible = true;
            this.AssignXrefDataSource();
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void executeSearch(string searchType)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        // save the current search criteria until a new find is initiated
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = this.HeaderBuildSearchCriteria(searchType);

        // default sort to the dropdown list value - actual field name
        dm.SetCache(this.Mode.ToString() + "gvInventorySortColumn", "ITEM");
        dm.SetCache(this.Mode.ToString() + "gvInventorySortDirection", " ASC");
        dm.SetCache(this.Mode.ToString() + "InventorySearchCriteria", searchCriteria);

        this.AssignDataSource(Data.Fetch);

        dsInventoryDataSet ds = (dsInventoryDataSet)dm.GetCache(this.Mode.ToString() + "dsInventoryDataSet");

        if (ds.ttinventory.Rows.Count > 1)
        {
            btnAddAll2.Visible = true;
        }
        else
        {
            DataTable dtEmpty = new DataTable();
            dtEmpty = BindEmptyData();
            lvNoImagesProductsList.Items.Clear();
            lvNoImagesProductsList.DataBind();
            lvNoImagesProductsList.DataSource = dtEmpty;
            lvNoImagesProductsList.DataBind();
            lvProducts.Items.Clear();
            lvProducts.DataBind();
            lvProducts.DataSource = dtEmpty;
            lvProducts.DataBind();
        }

        if (dm.GetCache(this.Mode.ToString() + "OrderFormAllowed") != null)
        {
            if ((bool)dm.GetCache(this.Mode.ToString() + "OrderFormAllowed"))
            {
                btnAddAll2.Visible = true;
            }
            else
            {
                btnAddAll2.Visible = false;
            }
        }

        if (ds.ttinventory.Rows.Count == 0)
        {
            this.btnAddAll2.Visible = false;
        }

        //Turn off order capability if Ship-to is non-saleable...
        if (dm.GetCache("ShiptoRowForInformationPanel") != null && (bool)((DataRow)dm.GetCache("ShiptoRowForInformationPanel"))["nonsalable"])
        {
            btnAddAll2.Visible = false;
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    #region Search Criteria

    private string HeaderBuildSearchCriteria(string searchType)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string type = "";
        string searchTran = "";
        string major = "";
        string minor = "";
        string available = "";
        string quickListOnly = "";

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string customer = "Customer=" + cust.CustomerKey;
        cust.Dispose();

        Dmsi.Agility.Data.Shipto shipTo = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string shipto = "ShipTo=" + shipTo.ShiptoSequence.ToString();
        shipTo.Dispose();

        string mode = "Mode=<ALL>";
        string extendPrice = "ExtendPrice=YES";

        // Sale Type will be used if ExtendPrice is set to YES
        string saleType = "SaleType=";

        if (Mode == ModeType.Inventory)
        {
            if (dm.GetCache("SaleType") != null)
                saleType += (string)dm.GetCache("SaleType");
            else
                saleType += "<ALL>";
        }

        if (Mode == ModeType.QuoteEditAddItems)
        {
            dsQuoteDataSet dsQuote = new dsQuoteDataSet();

            if (Session["QuoteEdit_dsQuote"] != null)
            {
                dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
                saleType += dsQuote.pvttquote_header[0]["sale_type"].ToString();
            }
            else
                saleType += "<ALL>";
        }

        string branch = "Branch=" + (string)dm.GetCache("currentBranchID");
        string status = "Status=";

        string analyticsSearchString = "Search by = ";

        switch (searchType)
        {
            case "SS":
                if (txtInventorySearchValue.Text.Trim().Length > 0)
                {
                    type = "SearchType=Item #";
                    searchTran = "SearchTran=" + txtInventorySearchValue.Text.SuppressProgressSpecialCharacters().Trim();
                    analyticsSearchString = analyticsSearchString + "Item #, ";
                }

                if (txtInventorySearchValue.Text.Trim().Length == 0 && txtKeywordSearchValue.Text.Trim().Length > 0)
                {
                    type = "SearchType=Keyword";
                    searchTran = "SearchTran=" + txtKeywordSearchValue.Text.Trim();
                    analyticsSearchString = analyticsSearchString + "Keyword, ";
                }

                if (txtInventorySearchValue.Text.Trim().Length == 0 && txtKeywordSearchValue.Text.Trim().Length == 0)
                {
                    type = "SearchType=Item #";
                    searchTran = "SearchTran=";
                }

                if (_overrideProductGroupQuickList)
                {
                    major = "Major=";
                    minor = "Minor=";
                }
                else
                {
                    if (ddlMajor.SelectedValue == "")
                        major = "Major=";
                    else
                        major = "Major=" + ddlMajor.SelectedValue;

                    if (ddlMinor.SelectedValue == "")
                        minor = "Minor=";
                    else
                        minor = "Minor=" + ddlMinor.SelectedValue;

                    if (major == "Major=" && minor == "Minor=")
                    {
                        analyticsSearchString = analyticsSearchString + "All Product Group, ";
                    }
                    else
                    {
                        analyticsSearchString = analyticsSearchString + "Specific Product Group, ";
                    }
                }

                if (cbxOnly.Checked)
                {
                    available = "Available=Yes";

                    analyticsSearchString = analyticsSearchString + "Available Products Only, ";

                }
                else
                    available = "Available=No";

                if (chkQuickList.Checked)
                {
                    quickListOnly = "QuickListOnly=Yes";

                    analyticsSearchString = analyticsSearchString + "My Quick List Items Only";

                }
                else
                    quickListOnly = "QuickListOnly=No";

                analyticsSearchString.TrimEnd(',');

                if (Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] != null)
                {
                    if ((int)Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] == 2)
                    {
                        type = "SearchType=Keyword";
                        searchTran = "SearchTran=" + txtKeywordSearchValue.Text.Trim();

                        if (txtInventorySearchValue.Text.Trim().Length > 0)
                        {
                            type = "SearchType=Item #";
                            searchTran = "SearchTran=" + txtInventorySearchValue.Text.Trim();
                        }
                    }
                }

                break;
        }

        searchCriteria = "UserID=" + user.GetContextValue("currentUserLogin");
        searchCriteria += "\x0003" + customer;
        searchCriteria += "\x0003" + shipto;
        searchCriteria += "\x0003" + mode;
        searchCriteria += "\x0003" + extendPrice;
        searchCriteria += "\x0003" + saleType;
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + major;
        searchCriteria += "\x0003" + minor;
        searchCriteria += "\x0003" + status;
        searchCriteria += "\x0003" + type;
        searchCriteria += "\x0003" + searchTran;
        searchCriteria += "\x0003" + available;
        searchCriteria += "\x0003" + quickListOnly;


        if (Session["MarkupFactor"] != null)
        {
            searchCriteria += "\x0003MarkupFactor=" + (Convert.ToDecimal(Session["MarkupFactor"])).ToString();
        }

        if (chkQuickList.Checked && Session["ViewQuickListHasBeenClicked"] != null && (bool)Session["ViewQuickListHasBeenClicked"] == true)
        {
            Session["ViewQuickListHasBeenClicked"] = null;
            searchCriteria += "\x0003QuickListType=" + ddlQuicklistType.SelectedValue;
        }
        else
        {
            searchCriteria += "\x0003QuickListType=";

            /* Analytics Tracking.  When you click view quick list that event is handled separately , this one is the search click button */
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Products - Advanced Search - Search Button";
            aInput.Label = analyticsSearchString;
            aInput.Value = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return searchCriteria;
    }

    private string DetailBuildSearchCriteria(string item, decimal thickness, decimal width, decimal length)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        string searchCriteria = "";

        string secLevel = "SecLevel=view_inv_prices";

        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string customer = "Customer=" + cust.CustomerKey;
        cust.Dispose();

        Dmsi.Agility.Data.Shipto shipTo = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        string shipto = "ShipTo=" + shipTo.ShiptoSequence.ToString();
        shipTo.Dispose();

        string mode = "Mode=Single";

        string branch = "Branch=" + (string)dm.GetCache("currentBranchID");

        string searchType = "SearchType=Item #";

        string searchTran = "SearchTran=" + item;

        // Sale Type will be used if ExtendPrice is set to YES
        string saleType = "SaleType=";

        if (dm.GetCache("SaleType") != null)
            saleType += (string)dm.GetCache("SaleType");
        else
            saleType += "<ALL>";

        string dimension = "thickness=" + thickness.ToString();
        dimension += "\x0003width=" + width.ToString();
        dimension += "\x0003length=" + length.ToString();

        searchCriteria = secLevel;
        searchCriteria += "\x0003" + customer;
        searchCriteria += "\x0003" + shipto;
        searchCriteria += "\x0003" + mode;
        searchCriteria += "\x0003" + branch;
        searchCriteria += "\x0003" + searchType;
        searchCriteria += "\x0003" + searchTran;
        searchCriteria += "\x0003" + saleType;
        searchCriteria += "\x0003" + dimension;

        if (Session["MarkupFactor"] != null)
        {
            searchCriteria += "\x0003MarkupFactor=" + (Convert.ToDecimal(Session["MarkupFactor"])).ToString();
        }

        Session[this.Mode.ToString() + "CachedInventoryDetailSearchCriteria"] = searchCriteria;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return searchCriteria;
    }

    private string BuildFilter()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string filter = "";
        string searchField, searchValue;
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        // Get the values save during the Find button click event
        searchField = (string)dm.GetCache(this.Mode.ToString() + "ddlInventorySearchField");
        searchValue = (string)dm.GetCache(this.Mode.ToString() + "txtInventorySearchValue");

        dm = null;

        if (searchField != null &&
            searchField.Length > 0 &&
            searchField != "All" &&
            searchValue != null)
        {
            filter = searchField + " LIKE '" + searchValue + "%'";
        }

        // Default to all, DataView needs a filter
        if (filter.Length == 0)
            filter = "ITEM LIKE '%'";

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return filter;
    }

    #region "Product Major/Minor CascadingDropDowns"
    private void LoadProductGroupMajors()
    {
#if DEBUG
            lock(_hitCounterLock) { _exucutionCounter++;
            _startMethodTime = DateTime.Now; }
#endif
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        Dmsi.Agility.Data.ProductGroups productGroups = null;
        ProductMajorClassGenerator xmlGenerator = new ProductMajorClassGenerator(dm, productGroups);

        if (!xmlGenerator.GetProductGroups())
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        xmlGenerator.GenerateXMLFileForSession();
        xmlGenerator.SetSessionVariable();

        if (Session["SavedProductGroupMajorGUID"] != null)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

            if (fi.Exists)
            {
                fi.Delete();
            }
        }

        xmlGenerator.dsPG.WriteXml(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

        CascadingDropDown1.UseContextKey = true;
        CascadingDropDown1.ContextKey = (string)Session["SavedProductGroupMajorGUID"];
        CascadingDropDown2.UseContextKey = true;
        CascadingDropDown2.ContextKey = (string)Session["SavedProductGroupMajorGUID"];

        Session["SavedInventoryBranch"] = (string)dm.GetCache("currentBranchID");

#if DEBUG
            TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
            DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }
    protected void RegenerateProductXMLFile()
    {
        // When switching theme after login, the CascadingDropDown settings are lost.
        // So if the XML file is present for the current sessionID, don't regenerate the XML file
        if (Session["SavedProductGroupMajorGUID"] != null)
        {
            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~//App_Data//") + (string)Session["SavedProductGroupMajorGUID"] + ".xml");

            if (fi.Exists)
            {
                CascadingDropDown1.UseContextKey = true;
                CascadingDropDown1.ContextKey = (string)Session["SavedProductGroupMajorGUID"];
                CascadingDropDown2.UseContextKey = true;
                CascadingDropDown2.ContextKey = (string)Session["SavedProductGroupMajorGUID"];
            }
            else
                LoadProductGroupMajors();
        }
        else
            LoadProductGroupMajors();
    }
    #endregion
    #endregion
    #endregion

    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"] == "True")
        {
            Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 1;
        }

        Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

        int CheckCount = igTree.CheckedNodes.Count;

        if (CheckCount > 0)
        {
            Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet dsSelectedAttribs = new Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet();
            DataTable dtPriorLoginCheckedItems = new DataTable();
            dtPriorLoginCheckedItems.Columns.Add("Guid");

            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

            string Group = string.Empty;

            for (int i = 0; i <= CheckCount - 1; i++)
            {
                string[] AttributeDetails = null;
                string AttributeVal;

                AttributeVal = igTree.CheckedNodes[i].Value;
                DataRow drNewRow;
                drNewRow = dtPriorLoginCheckedItems.NewRow();
                drNewRow["Guid"] = igTree.CheckedNodes[i].Value;
                dtPriorLoginCheckedItems.Rows.Add(drNewRow);
                dtPriorLoginCheckedItems.AcceptChanges();
                dtPriorLoginCheckedItems.GetChanges();

                if (!string.IsNullOrEmpty(AttributeVal))
                {
                    AttributeDetails = AttributeVal.Split('_');
                    Group = AttributeDetails[2];
                }

                if (AttributeDetails != null)
                {
                    if (AttributeDetails.Length > 0)
                    {
                        if (AttributeDetails[0] != "AttributeLabel")
                        {
                            DataRow drSelAttributes = dsSelectedAttribs.ttSelectedAttributes.NewRow();
                            drSelAttributes["SelectedLabel"] = AttributeDetails[5];
                            drSelAttributes["SelectedValue"] = AttributeDetails[6];
                            dsSelectedAttribs.ttSelectedAttributes.Rows.Add(drSelAttributes);
                            dsSelectedAttribs.AcceptChanges();
                            dsSelectedAttribs.GetChanges();
                        }
                    }
                }
            }

            if ((string)Session["GenericLogin"].ToString() == "True")
            {
                Session[this.Mode.ToString() + "priorLoginCheckedItems"] = dtPriorLoginCheckedItems;
            }

            if (Group != string.Empty)
            {
                DataSet dsProducts = new DataSet();
                dsProducts = GetfetchMultipleItems(Group, dsSelectedAttribs);

                if ((dsProducts != null && dsProducts.Tables.Count > 0 && dsProducts.Tables[0].Rows.Count > 0))
                {
                    btnAddAll2.Visible = true;
                    lblErrorMessage.Text = "";

                    BindProductGridViewDetails(dsProducts.Tables[0], 1);

                    Label2.Visible = true;
                    dvImageBanners.Visible = false;
                    dvProdListView.Visible = true;

                    Session[this.Mode.ToString() + "CachedInventoryDataSet"] = dsProducts;
                    Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = dsProducts;

                    lblHeaderError.Text = "";

                    int productSearchQty = 100;

                    if (Session["product_search_qty"] != null)
                        productSearchQty = Convert.ToInt32(Session["product_search_qty"]);

                    if (dsProducts.Tables[0].Rows.Count > 0 && dsProducts.Tables[0].Rows.Count < productSearchQty)
                    {
                        lblHeaderError.Visible = false;
                    }

                    if (dsProducts.Tables[0].Rows.Count > productSearchQty - 1)
                    {
                        if (TabContainer1.ActiveTab == tbSearch)
                        {
                            lblHeaderError.Text = "Your search returned too many records to be displayed. Consider refining your search criteria.";
                            lblHeaderError.Visible = true;
                        }
                        else
                        {
                            lblHeaderError.Text = "Your search returned too many records to be displayed. Consider refining your search criteria. For help click Search Tips.";
                            lblHeaderError.Visible = true;
                        }
                    }

                    lnkBreadCrumb.Visible = true;

                    if (lnkBreadCategory.Text.Trim().ToUpper() != "HOME")
                    {
                        lnkBreadCategory.Visible = true;
                        lblBreadCatSep.Visible = true;
                        if (lnkBreadCategory.Text.Trim().Length == 0)
                        {
                            lblBreadCatSep.Visible = false;
                        }
                    }
                    else
                    {
                        lnkBreadCategory.Visible = false;
                        lblBreadCatSep.Visible = false;
                    }

                    lblBreadGroupSep.Visible = true;
                    lnkBreadgroup.Visible = true;
                }
                else
                {
                    dvImageBanners.Visible = false;

                    btnAddAll2.Visible = false;
                    DataTable dtEmpty = new DataTable();

                    Label2.Visible = true;
                    lvProducts.Items.Clear();

                    dtEmpty = BindEmptyData();
                    lvProducts.DataSource = dtEmpty;
                    lvProducts.DataBind();
                    lvNoImagesProductsList.Items.Clear();
                    lvNoImagesProductsList.DataSource = dtEmpty;
                    lvNoImagesProductsList.DataBind();

                    if (TabContainer1.ActiveTab == tbSearch)
                    {
                        lblHeaderError.Text = NORESULTSFOUND;
                    }
                    else
                    {
                        lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
                    }

                    lblHeaderError.Visible = true;
                    dvProdListView.Visible = true;
                    lvPaging.Visible = false;
                    lblpageno.Visible = false;
                    dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
                }
            }
        }
        else
        {
            for (int j = 0; j < igTree.Nodes.Count; j++)
            {
                igTree.Nodes[j].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;

                for (int k = 0; k <= igTree.Nodes[j].Nodes.Count - 1; k++)
                {
                    igTree.Nodes[j].Nodes[k].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;

                    igTree.Nodes[j].Nodes[k].ImageUrl = "~/ig_res/Default/images/ig_checkbox_off.gif";

                }
            }

            DataTable dtCheckVal = new DataTable();
            dtCheckVal = (DataTable)Session[this.Mode.ToString() + "priorLoginCheckedItems"];

            if (dtCheckVal != null)
            {
                if (dtCheckVal.Rows.Count > 0)
                {
                    dtCheckVal.Rows.Clear();
                    dtCheckVal.AcceptChanges();
                    dtCheckVal.GetChanges();
                }
            }

            Session[this.Mode.ToString() + "priorLoginCheckedItems"] = dtCheckVal;
            SelectedNodeBind(lnkBreadgroup.CommandArgument, lnkBreadgroup.Text);
            lnkBreadCrumb.CssClass = "brd-crumbaInctive";

            lnkBreadCategory.CssClass = "brd-crumbaInctive";

            lnkBreadgroup.CssClass = "brd-crumbaInctive";
            lnkBreadCatValue.CssClass = "brd-crumbactive";

            dvProdListView.Visible = false;
        }

        hdnScrollToTop.Value = "true";

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void lnkClearAll_Click(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        for (int j = 0; j <= igTree.Nodes.Count - 1; j++)
        {
            igTree.Nodes[j].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;

            for (int k = 0; k <= igTree.Nodes[j].Nodes.Count - 1; k++)
            {
                igTree.Nodes[j].Nodes[k].CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
            }
        }

        Session["RunningClearSelections"] = true;

        if (!dvProdListView.Visible) //MDM - We have to run RenderImageButtons if there are banners in the view, to keep from losing the banners.
        {
            DataSet ds = Session["cachedBannerDataSet"] as DataSet;
            RenderImageButtons(ds, 2, null);
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Tree node click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void igTree_NodeClick(object sender, DataTreeNodeClickEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (btnSearch1.Visible == true)
        {
            if (!dvProdListView.Visible) //MDM - We have to run RenderImageButtons if there are banners in the view, to keep from losing the banners.
            {
                DataSet ds = Session["cachedBannerDataSet"] as DataSet;
                RenderImageButtons(ds, 2, null);
            }

            Session["DoNotRunPreRender"] = true;

            if (e.Node.CheckState == Infragistics.Web.UI.CheckBoxState.Checked)
            {
                e.Node.CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;

                if (e.Node.HasChildren)
                {
                    foreach (DataTreeNode n in e.Node.Nodes)
                        n.CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                }
                else
                {
                    if (e.Node.ParentNode != null)
                    {
                        bool notSameChecked = false;
                        foreach (DataTreeNode n2 in e.Node.ParentNode.Nodes)
                        {
                            if (n2.CheckState != e.Node.CheckState)
                                notSameChecked = true;
                        }

                        if (notSameChecked)
                        {
                            e.Node.ParentNode.CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                        }
                        else
                        {
                            if (e.Node.CheckState == Infragistics.Web.UI.CheckBoxState.Checked)
                                e.Node.ParentNode.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                            else
                                e.Node.ParentNode.CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                        }
                    }
                }
            }
            else if (e.Node.CheckState == Infragistics.Web.UI.CheckBoxState.Unchecked)
            {
                e.Node.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;

                if (e.Node.HasChildren)
                {
                    foreach (DataTreeNode n in e.Node.Nodes)
                        n.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                }
                else
                {
                    if (e.Node.ParentNode != null)
                    {
                        bool notSameChecked = false;
                        foreach (DataTreeNode n2 in e.Node.ParentNode.Nodes)
                        {
                            if (n2.CheckState != e.Node.CheckState)
                                notSameChecked = true;
                        }

                        if (notSameChecked)
                        {
                            e.Node.ParentNode.CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                        }
                        else
                        {
                            if (e.Node.CheckState == Infragistics.Web.UI.CheckBoxState.Checked)
                                e.Node.ParentNode.CheckState = Infragistics.Web.UI.CheckBoxState.Checked;
                            else
                                e.Node.ParentNode.CheckState = Infragistics.Web.UI.CheckBoxState.Unchecked;
                        }
                    }
                }
            }

            DeleteEmptyTreenodes();
        }

        if (btnSearch1.Visible == false)
        {
            string ActiveNodeVal = (((Infragistics.Web.UI.NavigationControls.WebDataTree)(sender))).ActiveNode.Value;
            Session[this.Mode.ToString() + "RetainPreviousUrl"] = ActiveNodeVal;

            Session[this.Mode.ToString() + "btnViewQuickList_Click"] = 0;

            string SelNodeText = (((Infragistics.Web.UI.NavigationControls.WebDataTree)(sender))).ActiveNode.Text;
            SelectedNodeBind(ActiveNodeVal, SelNodeText);
            string[] SplitBindedValue;

            SplitBindedValue = ActiveNodeVal.Split('_');
            string NodeSearchType = SplitBindedValue[0].ToString();
            //clear when checked check boxes 
            Session[this.Mode.ToString() + "btnReturnToItem_Click"] = 1;

            if ((string)Session["GenericLogin"] == "True")
            {
                if (NodeSearchType == "Catid" || NodeSearchType == "GroupId")
                {
                    Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 3;
                    Session[this.Mode.ToString() + "priorLoginSelectedNodeCatGroup"] = ActiveNodeVal;

                }
                else if (NodeSearchType == "AttributeVal" || NodeSearchType == "AttributeLabel")
                {
                    Session[this.Mode.ToString() + "RetainpriorLogin_SelectedDetails"] = 1;

                }
            }

            if (NodeSearchType == "Catid")
            {
                lnkBreadCrumb.CssClass = "brd-crumbaInctive";

                lnkBreadCategory.CssClass = "brd-crumbactive";

                lnkBreadgroup.CssClass = "brd-crumbaInctive";
                lnkBreadCatValue.CssClass = "brd-crumbaInctive";
            }
            else if (NodeSearchType == "GroupId")
            {
                lnkBreadCrumb.CssClass = "brd-crumbaInctive";

                lnkBreadCategory.CssClass = "brd-crumbaInctive";

                lnkBreadgroup.CssClass = "brd-crumbactive";
                lnkBreadCatValue.CssClass = "brd-crumbaInctive";
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void RetainPriceChange()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        int CheckCount = igTree.CheckedNodes.Count;

        if (CheckCount > 0)
        {
            Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet dsSelectedAttribs = new Dmsi.Agility.EntryNET.StrongTypesNS.dsSelectedAttributesDataSet();
            DataTable dtPriorLoginCheckedItems = new DataTable();
            dtPriorLoginCheckedItems.Columns.Add("Guid");

            string Group = string.Empty;

            for (int i = 0; i <= CheckCount - 1; i++)
            {
                string[] AttributeDetails = null;
                string AttributeVal;

                AttributeVal = igTree.CheckedNodes[i].Value;
                DataRow drNewRow;
                drNewRow = dtPriorLoginCheckedItems.NewRow();
                drNewRow["Guid"] = igTree.CheckedNodes[i].Value;
                dtPriorLoginCheckedItems.Rows.Add(drNewRow);
                dtPriorLoginCheckedItems.AcceptChanges();
                dtPriorLoginCheckedItems.GetChanges();

                if (!string.IsNullOrEmpty(AttributeVal))
                {
                    AttributeDetails = AttributeVal.Split('_');
                    Group = AttributeDetails[2];
                }

                if (AttributeDetails != null)
                {
                    if (AttributeDetails.Length > 0)
                    {
                        if (AttributeDetails[0] != "AttributeLabel")
                        {
                            DataRow drSelAttributes = dsSelectedAttribs.ttSelectedAttributes.NewRow();
                            drSelAttributes["SelectedLabel"] = AttributeDetails[5];
                            drSelAttributes["SelectedValue"] = AttributeDetails[6];
                            dsSelectedAttribs.ttSelectedAttributes.Rows.Add(drSelAttributes);
                            dsSelectedAttribs.AcceptChanges();
                            dsSelectedAttribs.GetChanges();
                        }
                    }
                }
            }

            if (Group != string.Empty)
            {
                DataSet dsProducts = new DataSet();
                dsProducts = GetfetchMultipleItems(Group, dsSelectedAttribs);

                if ((dsProducts != null && dsProducts.Tables.Count > 0 && dsProducts.Tables[0].Rows.Count > 0))
                {
                    btnAddAll2.Visible = true;
                    lblErrorMessage.Text = "";

                    BindProductGridViewDetails(dsProducts.Tables[0], 1);

                    Label2.Visible = true;
                    dvImageBanners.Visible = false;
                    dvProdListView.Visible = true;

                    Session[this.Mode.ToString() + "CachedInventoryDataSet"] = dsProducts;
                    Session[this.Mode.ToString() + "CachedInventoryDataSetCopy"] = dsProducts;

                    lblHeaderError.Text = "";
                    lnkBreadCrumb.Visible = true;

                    if (lnkBreadCategory.Text.Trim().ToUpper() != "HOME")
                    {
                        lnkBreadCategory.Visible = true;
                        lblBreadCatSep.Visible = true;
                    }
                    else
                    {
                        lnkBreadCategory.Visible = false;
                        lblBreadCatSep.Visible = false;
                    }

                    lblBreadGroupSep.Visible = true;
                    lnkBreadgroup.Visible = true;
                    lblpageno.Visible = true;
                }
                else
                {
                    dvImageBanners.Visible = false;

                    btnAddAll2.Visible = false;
                    DataTable dtEmpty = new DataTable();
                    dtEmpty = BindEmptyData();
                    Label2.Visible = true;
                    lvProducts.Items.Clear();
                    lvProducts.DataSource = dtEmpty;
                    lvProducts.DataBind();
                    lvNoImagesProductsList.Items.Clear();
                    lvNoImagesProductsList.DataSource = dtEmpty;
                    lvNoImagesProductsList.DataBind();

                    if (TabContainer1.ActiveTab == tbSearch)
                    {
                        lblHeaderError.Text = NORESULTSFOUND;
                    }
                    else
                    {
                        lblHeaderError.Text = NORESULTSFOUNDSEARCHTIPS;
                    }

                    lblHeaderError.Visible = true;
                    dvProdListView.Visible = true;
                    lvPaging.Visible = false;
                    lblpageno.Visible = false;
                    dvImgQuickList.Attributes["class"] = "DivNoImagesalign";
                }
            }
        }
        else
        {
            SelectedNodeBind(lnkBreadgroup.CommandArgument, lnkBreadgroup.Text);
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected DataTable BindEmptyData()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataTable dtEmpty = new DataTable();

        dtEmpty.Columns.Add("ITEM");
        dtEmpty.Columns.Add("thickness");
        dtEmpty.Columns.Add("WIDTH");
        dtEmpty.Columns.Add("LENGTH");
        dtEmpty.Columns.Add("Size");
        dtEmpty.Columns.Add("DESCRIPTION");

        dtEmpty.Columns.Add("qty_text");
        dtEmpty.Columns.Add("qty_available");
        dtEmpty.Columns.Add("stocking_uom");
        dtEmpty.Columns.Add("major_description");
        dtEmpty.Columns.Add("minor_description");
        dtEmpty.Columns.Add("configurable");
        dtEmpty.Columns.Add("price");
        dtEmpty.Columns.Add("price_uom");
        dtEmpty.Columns.Add("retail_price");

        dtEmpty.Columns.Add("min_pak");

        dtEmpty.Columns.Add("quick_list");

        dtEmpty.Columns.Add("ItemRecid");
        dtEmpty.Columns.Add("image_file");

        DataRow drNewRow;
        drNewRow = dtEmpty.NewRow();
        drNewRow["ITEM"] = "";
        drNewRow["thickness"] = "";
        drNewRow["WIDTH"] = "";
        drNewRow["LENGTH"] = "";
        drNewRow["Size"] = "";
        drNewRow["DESCRIPTION"] = "";
        drNewRow["qty_text"] = "";
        drNewRow["qty_available"] = "";
        drNewRow["stocking_uom"] = "";
        drNewRow["major_description"] = "";
        drNewRow["minor_description"] = "";
        drNewRow["configurable"] = "";
        drNewRow["price"] = "";
        drNewRow["price_uom"] = "";
        drNewRow["retail_price"] = "";
        drNewRow["stocking_uom"] = "";
        drNewRow["min_pak"] = "";
        drNewRow["retail_price"] = "";
        drNewRow["quick_list"] = "";
        drNewRow["ItemRecid"] = "";
        drNewRow["image_file"] = "";

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return dtEmpty;
    }

    private void ColumnSelectorItems_OnOKClicked(CheckBoxList checkBoxList)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataManager dm = new DataManager();

        this.AssignDataSource(Data.Fetch);
        dm = null;

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void ColumnSelectorPrice_OnOKClicked(CheckBoxList checkBoxList)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if ((string)Session["GenericLogin"].ToString().ToLower() == "false")
        {
            Profile.gvInventoryPricing_Columns_Visible = BuildColumnVisibilityProperty(checkBoxList, gvInventoryPricing);
        }
        else
        {
            BuildColumnVisibilityProperty(checkBoxList, gvInventoryPricing);
        }

        this.AssignPriceDataSource(Data.Fetch);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void ColumnSelectorPrice_OnResetClicked()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvInventoryPricing_Columns_Visible"].DefaultValue.ToString();

        Profile.gvInventoryPricing_Columns_Visible = defaultValue;

        InitializeColumnVisibility(gvInventoryPricing, "gvInventoryPricing_Columns_Visible");

        this.AssignPriceDataSource(Data.Fetch);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void ColumnSelectorItems_OnResetClicked()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvInventoryHeader_Columns_Visible"].DefaultValue.ToString();
        Profile.gvInventoryHeader_Columns_Visible = defaultValue;

        this.AssignPriceDataSource(Data.Fetch);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    /// <summary>
    /// Assign the column visibility based on profile settings
    /// </summary>
    /// <param name="gridView"></param>
    /// <param name="profileName"></param>
    private void InitializeColumnVisibility(GridView gridView, string profileName)
    {

#if DEBUG
    lock(_hitCounterLock) { _exucutionCounter++;
    _startMethodTime = DateTime.Now; }
#endif

        DataManager dm = new DataManager();

        for (int x = 0; x < gridView.Columns.Count; x++)
        {
            if (profileName == "gvInventoryHeader_Columns_Visible" && x != 8 && x != 9 && x != 10 && x != 11 && x != 12)
                continue;

            if (profileName == "gvInventoryPricing_Columns_Visible" && x == 3 || x == 4)
                continue;

            try
            {
                bool isVisible = false;

                if (Profile.PropertyValues[profileName].PropertyValue.ToString().Substring(x, 1) == "1")
                {
                    isVisible = true;
                }

                if (gridView.Columns[x].HeaderText == "Available" || gridView.Columns[x].HeaderText == "Qty Available" || gridView.Columns[x].HeaderText == "UOM")
                {
                    if (gridView.Columns[x].HeaderText == "Available")
                        gridView.Columns[x].Visible = isVisible && _viewInvAvailQty && _viewText;
                    if (gridView.Columns[x].HeaderText == "Qty Available")
                        gridView.Columns[x].Visible = isVisible && _viewInvAvailQty && _viewQuantity;
                    if (gridView.Columns[x].HeaderText == "UOM" && _viewText)
                        gridView.Columns[x].Visible = false;
                    if (gridView.Columns[x].HeaderText == "UOM" && _viewQuantity)
                        gridView.Columns[x].Visible = true;
                }
                else
                    gridView.Columns[x].Visible = isVisible;
            }
            catch (System.ArgumentOutOfRangeException aoor)
            {
                string message = aoor.Message;
                Profile.PropertyValues[profileName].PropertyValue = System.Web.Profile.ProfileBase.Properties[profileName].DefaultValue.ToString();
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void WebImageViewer1_ItemBound(object sender, Infragistics.Web.UI.ListControls.ImageItemEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void WebImageViewer2_ItemBound(object sender, Infragistics.Web.UI.ListControls.ImageItemEventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void FetchPopupItemImages(string item)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        //there may not be any images, so hide first.
        tablerowPopupImageViewer.Visible = false;

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        inv.FetchItemImagesOrDocuments("Image", item, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        if (inv.ReferencedItemImageDataSet.ttItemImageDoc.Rows.Count > 0)
        {
            DataView dv = new DataView();
            dv.Table = inv.ReferencedItemImageDataSet.ttItemImageDoc;
            dv.RowFilter = "(image_file like 'http*') OR (image_file like 'https*')";

            WebImageViewer2.DataSource = null;
            WebImageViewer2.DataSourceID = "";

            if (dv.Count > 0)
            {
                tablerowPopupImageViewer.Visible = true;

                WebImageViewer2.DataSource = dv;
                WebImageViewer2.ImageItemBinding.ImageUrlField = "image_file";
                WebImageViewer2.StyleSetName = ConfigurationManager.AppSettings["Theme"].ToString();
            }

            if (dv.Count == 1 && (bool)dv[0]["PRIMARY"] == true)
            {
                tablerowPopupImageViewer.Visible = false;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    public void DeleteEmptyTreenodes()
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (igTree.Nodes.Count > 0)
        {
            List<DataTreeNode> lst = new List<DataTreeNode>();

            for (int ignodes = 0; ignodes < igTree.Nodes.Count; ignodes++)
            {
                if ((igTree.Nodes[ignodes].Text == "") && (igTree.Nodes[ignodes].Value == ""))
                {
                    lst.Add(igTree.Nodes[ignodes]);
                }
            }

            if (lst.Count > 0)
            {
                for (int ilst = 0; ilst < lst.Count(); ilst++)
                {
                    igTree.Nodes.Remove(lst[ilst]);
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void ddlUOM_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DropDownList ddl = sender as DropDownList;
        string itemcode = ddl.DataMember;
        int uompointer = Convert.ToInt32(ddl.SelectedItem.Value);
        FetchAllDataForChangedUOM("list", itemcode, uompointer);

        Session["UOMonInventoryHasJustBeenChanged"] = true;

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Product Results - UOM Changer";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    protected void ddlDetailUOM_SelectedIndexChanged(object sender, EventArgs e)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DropDownList ddl = sender as DropDownList;
        Session["CurrentItemDetailUOM"] = this.lblDetailPartNo.Text.Replace("Part # ", "") + "|" + ddl.SelectedItem.Value;
        string itemcode = ddl.DataMember;
        int uompointer = Convert.ToInt32(ddl.SelectedItem.Value);
        FetchAllDataForChangedUOM("detail", itemcode, uompointer);

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Product Details - UOM Changer";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();


#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private dsInventoryDataSet FetchAllDataForChangedUOM(string source, string itemcode, int uompointer)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        DataManager dm = new DataManager();
        string currentbranch = (string)dm.GetCache("currentBranchID");

        bool showavailable = false;

        if (Session["Show_Available"] != null)
            showavailable = (bool)Session["Show_Available"];

        decimal markupfactor = 0;

        if (Session["MarkupFactor"] != null)
            markupfactor = Convert.ToDecimal(Session["MarkupFactor"]);

        dsInventoryDataSet ds = new dsInventoryDataSet();
        dsInventoryDataSet dsCopy = new dsInventoryDataSet();

        if (Session[this.Mode.ToString() + "CachedInventoryDataSet"] != null)
            ds = (dsInventoryDataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];

        DataRow[] rows = ds.ttinventory.Select("ITEM='" + itemcode + "'");

        if (rows.Length == 1)
        {
            dsCopy.ttinventory.Rows.Add(rows[0].ItemArray);
        }

        DataRow[] uomrows = ds.ttitem_uomconv.Select("item_ptr = " + (Int32)rows[0]["item_ptr"] + " AND uom_ptr = '" + uompointer + "'");

        if (uomrows.Length == 1)
        {
            dsCopy.ttitem_uomconv.Rows.Add(uomrows[0].ItemArray);
        }

        dsCopy.AcceptChanges();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory invdal = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        invdal.FetchUOM(currentbranch, markupfactor, ref dsCopy, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        if (source == "list")
            UpdateListForChangedUOM(itemcode, uompointer, dsCopy);
        else if (source == "detail")
            UpdateDetailForChangedUOM(itemcode, uompointer, dsCopy);

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif

        return dsCopy;
    }

    private void UpdateListForChangedUOM(string itemcode, int uompointer, dsInventoryDataSet dsCopy)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        if (lvProducts.Items.Count > 0)
        {
            for (int x = 0; x < lvProducts.Items.Count; x++)
            {
                if (itemcode == (lvProducts.Items[x].FindControl("hdnItem") as HiddenField).Value)
                {
                    decimal price = 0;

                    if ((string)Session["UserPref_DisplaySetting"] == "Net price")
                        price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["price_disp"]);
                    else
                        price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["retail_price_disp"]);

                    if (_viewInvPrices)
                        (lvProducts.Items[x].FindControl("lblPriceText") as Label).Text = string.Format("{0:C4}", price) + "/" + (string)dsCopy.ttinventory.Rows[0]["price_uom_disp"];
                    else
                        (lvProducts.Items[x].FindControl("lblPriceText") as Label).Visible = false;

                    if (_viewQuantity)
                    {
                        (lvProducts.Items[x].FindControl("lblAvilableCount") as Label).Text = ((decimal)dsCopy.ttinventory.Rows[0]["qty_available_disp"]).ToTrimmedString();
                    }

                    if (_viewInvPrices && _viewInvAvailQty)
                        (lvProducts.Items[x].FindControl("lblAvailUOM") as Label).Text = (string)dsCopy.ttinventory.Rows[0]["qty_uom"];

                    if (_viewText)
                    {
                        (lvProducts.Items[x].FindControl("lblAvilableCount") as Label).Text = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_text"]);
                        (lvProducts.Items[x].FindControl("lblAvailUOM") as Label).Text = "";
                    }

                    if ((decimal)dsCopy.ttinventory.Rows[0]["min_pak_disp"] > 0)
                    {
                        (lvProducts.Items[x].FindControl("lblMinPack") as Label).Text = "Must be ordered in multiples of " + String.Format("{0:#,###0.0000}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);

                        if ((lvProducts.Items[x].FindControl("lblMinPack") as Label).Text.EndsWith(".0000"))
                            (lvProducts.Items[x].FindControl("lblMinPack") as Label).Text = "Must be ordered in multiples of " + String.Format("{0:#,###0}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                    }

                    break;
                }
            }
        }

        if (lvNoImagesProductsList.Items.Count > 0)
        {
            for (int x = 0; x < lvNoImagesProductsList.Items.Count; x++)
            {
                if (itemcode == (lvNoImagesProductsList.Items[x].FindControl("hdnItem") as HiddenField).Value)
                {
                    decimal price = 0;

                    if ((string)Session["UserPref_DisplaySetting"] == "Net price")
                        price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["price_disp"]);
                    else
                        price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["retail_price_disp"]);

                    if (_viewInvPrices)
                        (lvNoImagesProductsList.Items[x].FindControl("lblPriceText") as Label).Text = string.Format("{0:C4}", price) + "/" + (string)dsCopy.ttinventory.Rows[0]["price_uom_disp"];
                    else
                        (lvNoImagesProductsList.Items[x].FindControl("lblPriceText") as Label).Visible = false;

                    if (_viewQuantity)
                    {
                        (lvNoImagesProductsList.Items[x].FindControl("lblAvilableCount") as Label).Text = ((decimal)dsCopy.ttinventory.Rows[0]["qty_available_disp"]).ToTrimmedString();
                    }

                    if (_viewInvPrices && _viewInvAvailQty)
                        (lvNoImagesProductsList.Items[x].FindControl("lblAvailUOM") as Label).Text = (string)dsCopy.ttinventory.Rows[0]["qty_uom"];

                    if (_viewText)
                    {
                        (lvNoImagesProductsList.Items[x].FindControl("lblAvilableCount") as Label).Text = Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_text"]);
                        (lvNoImagesProductsList.Items[x].FindControl("lblAvailUOM") as Label).Text = "";
                    }

                    if ((decimal)dsCopy.ttinventory.Rows[0]["min_pak_disp"] > 0)
                    {
                        (lvNoImagesProductsList.Items[x].FindControl("lblMinPack") as Label).Text = "Must be ordered in multiples of " + String.Format("{0:#,###0.0000}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);

                        if ((lvNoImagesProductsList.Items[x].FindControl("lblMinPack") as Label).Text.EndsWith(".0000"))
                            (lvNoImagesProductsList.Items[x].FindControl("lblMinPack") as Label).Text = "Must be ordered in multiples of " + String.Format("{0:#,###0}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
                    }

                    break;
                }
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void UpdateDetailForChangedUOM(string itemcode, int uompointer, dsInventoryDataSet dsCopy)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        decimal price = 0;

        if ((string)Session["UserPref_DisplaySetting"] == "Net price")
            price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["price_disp"]);
        else
            price = Convert.ToDecimal(dsCopy.ttinventory.Rows[0]["retail_price_disp"]);

        lblDetailPriceAndUom.Text = string.Format("{0:C4}", price) + " / " + (string)dsCopy.ttinventory.Rows[0]["price_uom_disp"];

        if (_viewQuantity)
        {
            lblDetailAvailableQty.Text = "Available qty " + ((decimal)dsCopy.ttinventory.Rows[0]["qty_available_disp"]).ToTrimmedString();
        }

        if (_viewText)
        {
            lblDetailAvailableQty.Text = "Available qty " + Convert.ToString(dsCopy.ttinventory.Rows[0]["qty_text"]);
        }

        if ((decimal)dsCopy.ttinventory.Rows[0]["min_pak_disp"] > 0)
        {
            lblDetailMinPack.Text = "Must be ordered in multiples of " + String.Format("{0:#,###0.0000}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);

            if (lblDetailMinPack.Text.EndsWith(".0000"))
                lblDetailMinPack.Text = "Must be ordered in multiples of " + String.Format("{0:#,###0}", dsCopy.ttinventory.Rows[0]["min_pak_disp"]);
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void PopulateUOMDropDown(DropDownList ddluom, string itempointer, string originalstockinguom, string itemtype)
    {

#if DEBUG
        lock(_hitCounterLock) { _exucutionCounter++;
        _startMethodTime = DateTime.Now; }
#endif

        ddluom.Items.Clear();

        dsInventoryDataSet ds = new dsInventoryDataSet();

        if (Session[this.Mode.ToString() + "CachedInventoryDataSet"] != null)
            ds = (dsInventoryDataSet)Session[this.Mode.ToString() + "CachedInventoryDataSet"];

        if (!(itemtype.ToLower() == "specific length lumber" || itemtype.ToLower() == "sheet good"))
        {
            DataRow[] rows = ds.ttitem_uomconv.Select("item_ptr='" + itempointer + "'");

            foreach (DataRow row in rows)
            {
                string padding = "";

                switch (((string)row["uom_code"]).Length)
                {
                    case 1:
                        padding = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        break;
                    case 2:
                        padding = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        break;
                    case 3:
                        padding = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        break;
                    case 4:
                        padding = "&nbsp;&nbsp;&nbsp;&nbsp;";
                        break;
                }

                ListItem it = new ListItem((string)row["uom_code"] + Server.HtmlDecode(padding) + row["description"], Convert.ToString(row["uom_ptr"]));
                ddluom.Items.Add(it);
            }
        }
        else
        {
            ListItem it = new ListItem(originalstockinguom);
            ddluom.Items.Add(it);
        }

        for (int x = 0; x < ddluom.Items.Count; x++)
        {
            if (ddluom.Items[x].Text.StartsWith(originalstockinguom))
            {
                ddluom.SelectedIndex = x;
                break;
            }
        }

#if DEBUG
        TimeSpan elapsedtime = DateTime.Now - _startMethodTime;
        DebugTraceMethod(elapsedtime, System.Reflection.MethodInfo.GetCurrentMethod().Name, this.Mode.ToString(), igTree.CheckedNodes.Count, this);
#endif
    }

    private void DebugTraceMethod(TimeSpan elapsedtime, string methodname, string mode, int igTreeCheckedNodes, object localThis)
    {
        UserControls_Inventory uci = (UserControls_Inventory)localThis;

        if (mode == "Inventory")
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_exucutionCounter.ToString() + ". " + methodname);
            sb.Append("; " + mode);
            sb.Append("; igTree checked nodes count: " + igTreeCheckedNodes.ToString());
            sb.Append("; " + elapsedtime.ToString() + "\r\n");

            if (methodname.ToLower() == "page_prerender")
                sb.Append("\r\n");

            string path = Server.MapPath("~/Documents");
            File.AppendAllText(Path.Combine(path, "TraceLog.txt"), sb.ToString());
        }
    }

    protected void lnkExtDesc_Click(object sender, EventArgs e)
    {
        if (lnkExtDesc.Text == "show more")
        {
            lnkExtDesc.Text = "show less";
            lblDetailExtDesc.Text = hdnDetailExtDesc.Value + " ";
        }
        else if (lnkExtDesc.Text == "show less")
        {
            lnkExtDesc.Text = "show more";
            lblDetailExtDesc.Text = hdnDetailExtDesc.Value.Substring(0, 100) + "... ";
        }
    }
    public string GetWeSucceedUserID()
    {
        string userID = "0";

        try
        {
            WeSucceedService.ServiceClient client = new WeSucceedService.ServiceClient();

            if (client.Endpoint.Address == null)
                return "0";

            System.ServiceModel.Description.ServiceEndpoint oEndpoint = (System.ServiceModel.Description.ServiceEndpoint)client.Endpoint;
            // MDM - use this for creating an error... oEndpoint.Address = new EndpointAddress("https://" + oEndpoint.Address.Uri.Host + oEndpoint.Address.Uri.LocalPath);
            oEndpoint.Binding.SendTimeout = new TimeSpan(0, 0, 10);    //10 seconds
            oEndpoint.Binding.ReceiveTimeout = new TimeSpan(0, 0, 10); //10 seconds
            userID = client.GetID();
        }

        catch (System.Exception ex)
        {
            System.Exception ex2 = new Exception("WeSucceed Service: " + ex.Message + "; " + ex.StackTrace);

            Dmsi.Agility.EntryNET.EventLog el = new EventLog();
            el.LogError(EventTypeFlag.UnhandledException, ex2);
        }

        return userID;

        //WebRequest request = WebRequest.Create("https://jsonplaceholder.typicode.com/posts/1");

        //// Get the response.
        //HttpWebResponse response = (HttpWebResponse)request.GetResponse();

        //// Get the stream containing content returned by the server.
        //Stream dataStream = response.GetResponseStream();

        //// Open the stream using a StreamReader for easy access.
        //StreamReader reader = new StreamReader(dataStream);

        //// Read the content.
        //string responseFromServer = reader.ReadToEnd();

        //dynamic results = new JavaScriptSerializer().Deserialize<dynamic>(responseFromServer);

        //string title = "";

        //foreach (dynamic result in results)
        //{
        //    if (result.Key == "title")
        //    {
        //        title = result.Value;
        //        break;
        //    }
        //}

        //// Cleanup the streams and the response.
        //reader.Close();
        //dataStream.Close();
        //response.Close();

        //string version = "";

        //HttpWebRequest webRequest = CreateWebRequest("http://restapps.dmsi.com:8980/SessionService/rest/SessionService/GetAgilityVersion");

        //// begin async call to web request.
        //IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

        //// suspend this thread until call is complete. You might want to
        //// do something usefull here like update your UI.
        //asyncResult.AsyncWaitHandle.WaitOne();

        //// get the response from the completed web request.
        //string RESTPostResult;
        //using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
        //{
        //    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
        //    {
        //        RESTPostResult = rd.ReadToEnd();
        //    }
        //}

        //dynamic results = new JavaScriptSerializer().Deserialize<dynamic>(RESTPostResult);

        //foreach (dynamic result in results)
        //{
        //    if (result.Key == "response")
        //    {
        //        foreach (dynamic child in result.Value)
        //        {
        //            if (child.Key == "AgilityVersion")
        //            {
        //                version = child.Value;
        //                break;
        //            }
        //        }
        //    }
        //}

        //return version;
    }

    //private HttpWebRequest CreateWebRequest(string url)
    //{
    //    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
    //    webRequest.ContentType = "application/json";
    //    webRequest.Method = "POST";
    //    return webRequest;
    //}
}