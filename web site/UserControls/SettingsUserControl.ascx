﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SettingsUserControl.ascx.cs" Inherits="UserControls_SettingsUserControl" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.NavigationControls" TagPrefix="ig" %>
<%@ Register Src="~/UserControls/LogView.ascx" TagName="LogView" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/PasswordMaintSettings.ascx" TagName="PasswordMaintSettings" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/MessagesMaint.ascx" TagName="MessagesMaint" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/ChangeTheme.ascx" TagName="ChangeTheme" TagPrefix="uc4" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxcontrol" %>
<style type="text/css">
    .menucell {
        margin-left: 20px;
        margin-right: 20px;
    }

    .menuleftmargin {
        margin-left: 6px;
    }

    .pnlPopUp {
        right: 152px !important;
        margin-right: 0px !important;
    }

    .margintop8 {
        margin-top: 8px !important;
    }
</style>
<script>
   
        </script>
<div class="left-section">
    <div class="lft-cont-sec">
        <div class="search-opt settingsblock">
            <ul class="list">
                <li>
                    <asp:LinkButton runat="server" ID="lnkchangepass" Text="Change Password" OnClick="lnk_Click" />

                      
                </li>
                <li runat="server" id="lilogview">
                    <asp:LinkButton runat="server" ID="lnkLogview" Text="Log Viewer" OnClick="lnk_Click" />
                    <div class="log">
                        <div runat="server" id="adsearchdiv" visible="false">


                            <div>
                                <div class="advsearch" style="width:95%;padding-left:3px;">

                                    <asp:Panel ID="LogViewerFieldPanel" runat="server" DefaultButton="btnFind">
                                        <asp:Label ID="Label3" runat="server" CssClass="alladvsearchlabel" Text="Search by">
                                        </asp:Label>
                                        <br />
                                        <asp:DropDownList ID="ddlTranID" runat="server">
                                            <asp:ListItem Value="remoteuser">Remote User ID</asp:ListItem>
                                            <asp:ListItem Value="authuser">Authenticated User ID</asp:ListItem>
                                            <asp:ListItem Value="remoteip">Remote IP Address</asp:ListItem>
                                            <asp:ListItem Value="type">Event Type</asp:ListItem>
                                            <asp:ListItem Value="baseurl">Base URL</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <asp:TextBox ID="txtTranID" runat="server">
                                        </asp:TextBox><br />
                                        <asp:Label ID="FieldLabel" runat="server" Text="Limit search to" CssClass="alladvsearchlabel"></asp:Label><br />
                                        <asp:DropDownList ID="ddlTimeSpan" runat="server">
                                            <asp:ListItem Selected="True" Value=".02">Last 30 Minutes</asp:ListItem>
                                            <asp:ListItem Value=".0416">Last Hour</asp:ListItem>
                                            <asp:ListItem Value=".5">Last 12 Hours</asp:ListItem>
                                            <asp:ListItem Value="1">Last 24 Hours</asp:ListItem>
                                            <asp:ListItem Value="7">Last 7 days</asp:ListItem>
                                            <asp:ListItem Value="9999">Select All</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Button ID="btnFind" runat="server" CssClass="logviewadvbutton" OnClick="btnFind_Click"
                                            Text="Go" />
                                    </asp:Panel>
                                </div>
                                <ajaxcontrol:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" AutoCollapse="False"
                                    AutoExpand="false" CollapseControlID="PreferencePanel" Collapsed="False" CollapsedImage="~/images/collapse_blue.jpg"
                                    CollapsedSize="0" CollapsedText="Preferences" ExpandControlID="PreferencePanel"
                                    ExpandDirection="Vertical" ExpandedImage="~/images/expand_blue.jpg" ExpandedText="Preferences"
                                    ImageControlID="PreferenceImage" ScrollContents="False" SuppressPostBack="true"
                                    TargetControlID="PreferenceFieldPanel">
                                </ajaxcontrol:CollapsiblePanelExtender>
                                <div>
                                    <asp:Panel ID="PreferencePanel" runat="server">
                                    </asp:Panel>
                                    <asp:Panel ID="PreferenceFieldPanel" runat="server" CssClass="SimplePanelBody">
                                    </asp:Panel>
                                </div>
                                <div id="pnlCustomLogView" class="modal-dialog smlwidth" style="display: none;">

                                    <div id="pnlDragLogView" runat="server">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                
                                                <h4 class="modal-title">Columns to View</h4>
                                            </div>
                                            <div class="modal-body">
                                                <uc5:ColumnSelector ID="csCustomLogView" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </li>
                <li runat="server" id="limessage">
                    <asp:LinkButton runat="server" ID="lnkmessages" Text="Messages" OnClick="lnk_Click" />
                </li>
                <li>
                    <asp:LinkButton runat="server" ID="lnkChange" Text="Change Theme" OnClick="lnk_Click" />
                </li>
            </ul>

        </div>

    </div>



</div>
<div class="right-section">
    <div class="container-sec">
        <h2>
            <asp:Label ID="lblSttingName" runat="server" /> 
            </h2>
        
        
        <br clear="all" />
        <b id="android_msg" style="display: none;float:right;font-size: 12px;line-height: 27px;font-weight:normal;margin-top: -15px;">
            *Perform the functions on this screen from a desktop browser for the best experience.
        </b>
        <div class="custom-grid abc" >
            <uc2:PasswordMaintSettings ID="PasswordMaintSettings" runat="server" Visible="False" />
            <div  runat="server" visible="false" id="LogView1">
                <div class="optional" style="width:350px !important;">
                    <ul class="cust-opt-list">
                        <li>
                            <asp:Label Visible="false" ID="lblLogViewPreference" runat="server" Text="Log Entries" CssClass="FieldLabel" />
                        </li>
                        <li>
                            <asp:Label ID="Label11" runat="server" Text="Show"></asp:Label></li>
                        <li>
                            <asp:DropDownList ID="ddlLogViewRows" runat="server" AutoPostBack="true" Width="50px"
                                OnSelectedIndexChanged="ddlLogViewRows_SelectedIndexChanged"  CssClass="select opt-size">
                                <asp:ListItem Selected="True">10</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>40</asp:ListItem>
                                <asp:ListItem>50</asp:ListItem>
                                <asp:ListItem>75</asp:ListItem>
                                <asp:ListItem>100</asp:ListItem>
                                <asp:ListItem>250</asp:ListItem>
                                <asp:ListItem>500</asp:ListItem>
                                <asp:ListItem>All</asp:ListItem>
                            </asp:DropDownList>
                            </li>
                        <li>
                            <asp:Label runat="server" Text="Rows" ID="lbllogrows" />
                            <ajaxcontrol:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlCustomLogView" PopupDragHandleControlID="pnlDragLogView"
                                TargetControlID="lnkLogViewCustomView">
                            </ajaxcontrol:ModalPopupExtender>
                        </li>
                        <li style="padding-left:9px;">
                            <asp:LinkButton ID="lnkLogViewCustomView" runat="server"  CssClass="optionslink">Select Columns</asp:LinkButton></li>
                    </ul>
                </div>
                <div>
                    <asp:Label ID="lblLogViewError" runat="server" CssClass="ErrorTextRed" Text="Record limit has been exceeded. Add criteria to refine search."
                        Visible="False"></asp:Label>
                </div>
                <br />
                <div class="custom-grid-overflow logviews">
                    <asp:GridView ID="gvLogView" runat="server" AllowPaging="True" AllowSorting="True"
                        OnPageIndexChanging="gvLogView_PageIndexChanging" OnRowCreated="gvLogView_RowCreated"
                        OnRowDataBound="gvLogView_RowDataBound" OnSorting="gvLogView_Sorting" 
                        AutoGenerateColumns="False" HorizontalAlign="Left" CssClass="grid-tabler logoverflow" PagerStyle-CssClass="PagerGridView">
                        <Columns>
                            <asp:BoundField DataField="eventid" HeaderText="Event ID" ReadOnly="True" SortExpression="eventid">
                                <ItemStyle Wrap="False" CssClass="logtextalign" />
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="type" HeaderText="Event Type" ReadOnly="True" SortExpression="type">
                                <ItemStyle Wrap="False" CssClass="logtextalign" />
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="time" HeaderText="Event Timestamp" ReadOnly="True" SortExpression="time">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="remoteip" HeaderText="Remote IP" ReadOnly="True" SortExpression="remoteip">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="remotehost" HeaderText="Remote Host IP" ReadOnly="True"
                                SortExpression="remotehost">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="remoteuser" HeaderText="Remote User ID" ReadOnly="True"
                                SortExpression="remoteuser">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="unmappedremoteuser" HeaderText="Unmapped Remote User"
                                ReadOnly="True" SortExpression="unmappedremoteuser" Visible="False">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="authuser" HeaderText="Authenticated User ID" ReadOnly="True"
                                SortExpression="authuser">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="baseurl" HeaderText="Base URL" ReadOnly="True" SortExpression="baseurl">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="querystring" HeaderText="Query String " ReadOnly="True"
                                SortExpression="querystring">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="http_method" HeaderText="HTTP Method" ReadOnly="True"
                                SortExpression="http_method">
                                <ItemStyle Wrap="False" CssClass="logtextalign" />
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                SortExpression="description">
                                <ItemStyle Wrap="False" CssClass="logtextalign" />
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="innerexception" HeaderText="Inner Exception" ReadOnly="True"
                                SortExpression="innerexception" Visible="False">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="source" HeaderText="Source" ReadOnly="True" SortExpression="source"
                                Visible="False">
                                <ItemStyle Wrap="False" CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="method" HeaderText="Method" ReadOnly="True" SortExpression="method"
                                Visible="False">
                                <ItemStyle Wrap="False"  CssClass="logtextalign"/>
                                <HeaderStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="trace" HeaderText="Trace" ReadOnly="True" SortExpression="trace"
                                Visible="False">
                                <ItemStyle Width="220px" CssClass="logtextalign"/>
                                <HeaderStyle Width="220px" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>

                                </ItemTemplate>

                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                      
                <div class="pagination-sec" style="width:280px">
                        <div class="info" style="width:275px ">
                            <asp:Label ID="lblpageno" runat="server" Text="" />
                        </div>


                    </div>
                </div>

              
                   
            </div>
            <uc3:MessagesMaint ID="MessagesMaint1" runat="server" Visible="False" />
            <uc4:ChangeTheme ID="ChangeTheme1" runat="server" Visible="false" />
            <asp:HiddenField ID="hdnlogview" runat="server" Value="0" />
        </div>
    </div>
</div>

