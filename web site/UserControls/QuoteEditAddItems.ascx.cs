﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Collections.Generic;
using Infragistics.Web.UI.GridControls;
using Infragistics.Web.UI.NavigationControls;
using System.Web.UI.HtmlControls;

public partial class UserControls_QuoteEditAddItems : System.Web.UI.UserControl
{

    public void Initialize(dsQuoteDataSet dsQuote)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["singleview"] == null)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "PrRespFormat();", true);
        }
        else
        {
            if((string)Session["singleview"] == "true")
            {
                 ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormatSingle();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "PrRespFormat();", true);
            }
        }
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
  
        dsQuoteDataSet dsQuote = new dsQuoteDataSet();

        if (Session["QuoteEdit_dsQuote"] != null)
        {
            dsQuote = (dsQuoteDataSet)Session["QuoteEdit_dsQuote"];
            lblQuoteHeader.Text = "Add Items to Quote " + dsQuote.pvttquote_header.Rows[0]["quote_id"].ToString();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.QuoteEditInventory.ClearProductTextboxes();

        UserControls_QuoteEdit qe = Parent.FindControl("QuoteEdit") as UserControls_QuoteEdit;
        qe.UpdateQuoteAfterVisualCafeSave();
        qe.Visible = true;

        this.Visible = false;
        Session["QuoteEditVisible"] = true;
        Session["QuoteEditAddItemsVisible"] = false;
        Session["BlockPopupMenu"] = false;

        if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
        {
            if ((bool)Session["UserAction_AllowRetailPricing"] == true)
            {
                if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                {
                    Page.Master.FindControl("liNetRetail").Visible = true;
                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

                    if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
                    }
                    else
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
                    }
                }
            }
        }

        MenuEnableDisable menuEnable = new MenuEnableDisable();
        menuEnable.EnableMenuItems(this.Page, this.Parent);
    }
}