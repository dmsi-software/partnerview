<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrderTab.ascx.cs" Inherits="UserControls_OrderTab" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/AddInfoSelector.ascx" TagName="AddInfoSelector" TagPrefix="uc8" %>
<%@ Register Src="~/UserControls/Inventory.ascx" TagName="Inventory" TagPrefix="uc9" %>
<%@ Register Src="~/UserControls/Home.ascx" TagName="Home" TagPrefix="uch1" %>
<%@ Register Src="~/UserControls/ShiptoSelector.ascx" TagName="ShiptoSelector" TagPrefix="uc10" %>
<style type="text/css">
    .style3 {
        width: 210px;
    }

    .style4 {
        width: 69px;
    }

    .style5 {
        width: 70px;
    }

    .Field {
        margin-left: 0px;
    }

    .TopMargin1 {
        margin-top: 1px;
    }

    .TopMargin10 {
        margin-top: 10px !important;
    }

    .ErrorTextRedWithTopMargin {
        font-family: Helvatica,Verdana,Tahoma,Arial;
        font-size: 8pt;
        color: Red;
        margin-top: 3px;
    }

    .ErrorTextRedWithTopMarginLarge {
        font-family: Helvatica,Verdana,Tahoma,Arial;
        font-size: 9pt;
        color: Red;
        margin-top: 6px;
        text-align: left;
    }

    .wrapstyle {
        white-space: normal;
    }
</style>
<div class="container-sec form">
    <div class="custom-grid abc">
        <h2 id="SubmitButtons2" runat="server" visible="false" style="padding-bottom: 15px; padding-top: 15px;">
            <asp:Label ID="lblReviewAndSubmit" runat="server" Text="Review and Submit"></asp:Label>
            <div class="col-sm-2 pull-right" style="width: 38%">
                <asp:Button ID="btnSubmitOrder2" runat="server" Text="Submit"
                    CssClass="btn btn-primary" TabIndex="13" Width="100px" UseSubmitBehavior="false" OnClick="btnSubmitOrder_Click" OnClientClick="CartSubmiteHideCancelAndSubmitButtons()" />
                <asp:Button ID="btnEditCart2" runat="server" Text="Return to Cart" OnClick="btnEditCart_Click"
                    CssClass="btn btn-default" TabIndex="12" />
            </div>
        </h2>
        <h2 id="CartButtons2" runat="server" visible="true" style="padding-bottom: 15px; padding-top: 15px;">
            <div>
                <asp:Label ID="lblItemsInCart" runat="server" Text="Items in Cart" />
                <div class="col-sm-2 pull-right cartbannerbutton" style="width: 60%">
                    <asp:Button ID="btnCartCheckout2" runat="server" Text="Proceed to Checkout" OnClick="btnCartCheckout_Click"
                        CssClass="btn btn-primary" UseSubmitBehavior="False" TabIndex="13" />
                    <asp:Button ID="btnContinueShopping2" runat="server" Text="Continue Shopping" CssClass="btn btn-default"
                        UseSubmitBehavior="False" TabIndex="12" OnClick="btnContinueShopping_Click" />
                </div>
            </div>
        </h2>
        <div id="SubmitTop" runat="server" visible="false" class="submit">
            <div>
                <asp:Label ID="lblRequestedDateMessage" runat="server" CssClass="ErrorTextRedWithTopMarginLarge"
                    Text="** Requested delivery date subject to change upon submission."
                    Visible="False"></asp:Label>
            </div>
            <div style="padding-top: 10px">
                <div class="col-sm-6" style="padding-top: 20px;">
                    <ul class="default-list default-list2">
                        <li>
                            <asp:Label ID="Label13" runat="server" CssClass="Editquotelabel" Text="Submit as &nbsp;"></asp:Label>
                            <asp:DropDownList ID="ddlSubmitAs" runat="server" TabIndex="14"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlSubmitAs_SelectedIndexChanged">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="SO">Order</asp:ListItem>
                                <asp:ListItem Value="QU">Quote</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblSubmitAsError" runat="server" CssClass="ErrorTextRed "
                                Text="*"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label12" runat="server" Text="Requested delivery date &nbsp;"
                                CssClass="Editquotelabel wrapEditLabel" />
                            <asp:TextBox ID="txtRequestedDate" runat="server" TabIndex="15" CssClass="TopMargin1"
                                Visible="true" AutoPostBack="True" OnTextChanged="txtRequestedDate_TextChanged"></asp:TextBox>
                            <asp:DropDownList ID="ddlRequestedDate" runat="server"
                                TabIndex="15" Visible="false" AutoPostBack="True" OnSelectedIndexChanged="ddlRequestedDate_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label ID="lblRequestdDateAsterisk" runat="server" CssClass="ErrorTextRed "
                                Text="**" Visible="False"></asp:Label>
                            <asp:Label ID="lblRequestedDateError" runat="server" CssClass="ErrorTextRed" Text="Invalid"
                                Visible="False"></asp:Label>
                        </li>
                        <li class="ordersubmit">
                            <asp:Label ID="Label3" runat="server" CssClass="Editquotelabel" Text="Submitted by"></asp:Label>
                            <asp:TextBox ID="txtOrderedBy" runat="server" TabIndex="16"
                                MaxLength="12"></asp:TextBox>
                            <asp:Label ID="lblOrderedByError" runat="server" CssClass="ErrorTextRed" Text="*"
                                Visible="True" />
                        </li>
                        <li>
                            <asp:Label ID="Label6" runat="server" CssClass="Editquotelabel" Text="PO ID &nbsp;"></asp:Label>
                            <asp:TextBox ID="txtCustomerPO" runat="server" TabIndex="17"
                                MaxLength="16"></asp:TextBox>
                            <asp:Label ID="lblPOError" runat="server" CssClass="ErrorTextRed" Text="*"
                                Visible="False"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label4" runat="server" CssClass="Editquotelabel" Text="Job  &nbsp;"></asp:Label>

                            <asp:TextBox ID="txtJob" runat="server" TabIndex="18"
                                MaxLength="24"></asp:TextBox>
                            <asp:Label ID="lblJobError" runat="server" CssClass="ErrorTextRed" Text="*"
                                Visible="False"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label7" runat="server" CssClass="Editquotelabel" Text="E-mail &nbsp;"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" TabIndex="19"></asp:TextBox>
                            <asp:Label ID="lblEmailError" runat="server" CssClass="ErrorTextRed" Text="*"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label10" runat="server" CssClass="Editquotelabel" Text="Secondary E-mail &nbsp;"></asp:Label>
                            <asp:TextBox ID="txtCcEmail" runat="server" TabIndex="20"></asp:TextBox>
                            <asp:Label ID="lblCcEmailError" runat="server" CssClass="ErrorTextRed" Text="Invalid"
                                Visible="False"></asp:Label>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6" style="padding-top: 20px;">
                    <ul class="default-list2">
                        <li>
                            <asp:Label ID="Label1" runat="server" CssClass="Editquotelabel" Text="Order type &nbsp;"></asp:Label>

                            <asp:DropDownList ID="ddlOrderType" runat="server" TabIndex="22"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlOrderType_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label ID="lblOrderTypeError" runat="server" CssClass="ErrorTextRed" Text="*"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label5" runat="server" CssClass="Editquotelabel" Text="Reference &nbsp;"></asp:Label>

                            <asp:TextBox ID="txtReference" runat="server" TabIndex="23"
                                MaxLength="20"></asp:TextBox>
                            <asp:Label ID="lblReferenceError" runat="server" CssClass="ErrorTextRed" Text="*"
                                Visible="False"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label14" runat="server" CssClass="Editquotelabel" Text="Ship via &nbsp;"></asp:Label>

                            <asp:DropDownList ID="ddlShipVia" runat="server" TabIndex="24"
                                AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:Label ID="lblShipViaError" runat="server" CssClass="ErrorTextRed" Text="*"
                                Visible="False"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="lblShiptoOverride" runat="server" CssClass="Editquotelabel" Text="Shipping address &nbsp;"></asp:Label>
                            <asp:LinkButton runat="server" ID="lnkShiptoOverride" CssClass="SettingsLink" TabIndex="25">Update Address...</asp:LinkButton>
                            <div id="ShiptoModalDiv" runat="server">
                                <cc1:ModalPopupExtender ID="ModalPopupExtenderShipto" runat="server" BackgroundCssClass="ModalBackground"
                                    DropShadow="true" PopupControlID="pnlShiptoCustomView"
                                    TargetControlID="lnkShiptoOverride">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnlShiptoCustomView" runat="server" CssClass="modal-dialog">
                                    <div id="pnlDragShipto" runat="server" class="modal-content" style="text-align: left">
                                        <div class="modal-header" style="margin: 0px 15px; padding: 10px 0px;">
                                            <h4 class="modal-title">Shipping Address</h4>
                                        </div>
                                        <div class="modal-body">
                                            <uc10:ShiptoSelector ID="ShiptoSelector1" runat="server" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </li>
                        <li>
                            <asp:CheckBox ID="cbxShipComplete" runat="server" Text="Ship complete" CssClass="editcheckbox1 editcheckrelease TopMargin10" TabIndex="26" />
                        </li>
                        <li style="padding-top: 8px">
                            <asp:Label ID="Label9" runat="server" CssClass="Editquotelabel" Text="Fax number &nbsp;"></asp:Label>

                            <asp:TextBox ID="txtFax" runat="server" TabIndex="27"></asp:TextBox>
                            <asp:Label ID="lblFaxError" runat="server" CssClass="ErrorTextRed" Text="Invalid"
                                Visible="False"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="lblCustInfo" runat="server" CssClass="Editquotelabel" Text="Retail customer &nbsp;"></asp:Label>
                            <asp:LinkButton runat="server" ID="lnkAddInfo" CssClass="SettingsLink" TabIndex="28">Add Information...</asp:LinkButton>
                            <div id="AddInfoModalDiv" runat="server">
                                <cc1:ModalPopupExtender ID="ModalPopupExtenderAddInfo" runat="server" BackgroundCssClass="ModalBackground"
                                    DropShadow="true" PopupControlID="pnlAddInfoCustomView"
                                    TargetControlID="lnkAddInfo">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnlAddInfoCustomView" runat="server" CssClass="modal-dialog" Width="620px">
                                    <div id="pnlDragMarkup" runat="server" class="modal-content" style="text-align: left">
                                        <div class="modal-header" style="margin: 0px 15px; padding: 10px 0px;">
                                            <h4 class="modal-title">Retail Customer Information</h4>
                                        </div>
                                        <div class="modal-body">
                                            <uc8:AddInfoSelector ID="AddInfoSelector" runat="server" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12">
                    <ul class="default-list">
                        <li>
                            <asp:Label ID="Label2" runat="server" CssClass="Editquotelabel" Text="Message &nbsp;"></asp:Label>
                            <asp:TextBox ID="txtMessage" runat="server" TabIndex="21"
                                MaxLength="800" TextMode="MultiLine"></asp:TextBox>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="CartGridDiv" runat="server" class="itemcart" style="width: 100%; margin: 0px !important;">
            <div class="col-sm-12" style="padding-top: 20px;">
                <div class="pull-left ddl" style="width: 100%">
                    <asp:Label ID="lblshow" runat="server" Text="Show" Font-Size="13px"></asp:Label>
                    <asp:DropDownList ID="ddlOrderHeaderRows" runat="server" AutoPostBack="true" Width="60px"
                        OnSelectedIndexChanged="ddlOrderHeaderRows_SelectedIndexChanged">
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>All</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="Label11" runat="server" Font-Size="13px" Text="Rows"></asp:Label>
                </div>
                <div class="custom-grid" style="width: 100%">
                    <div class="custom-grid-overflow">
                        <asp:GridView ID="gvOrderCart" runat="server" AllowPaging="True" Width="100%" AutoGenerateColumns="False"
                            AllowSorting="True" OnRowCancelingEdit="gvOrderCart_RowCancelingEdit"
                            OnRowEditing="gvOrderCart_RowEditing" OnRowDeleting="gvOrderCart_RowDeleting"
                            OnRowUpdating="gvOrderCart_RowUpdating" EmptyDataText="There are no items in your shopping cart." EmptyDataRowStyle-CssClass=""
                            OnPageIndexChanging="gvOrderCart_PageIndexChanging" OnRowCreated="gvOrderCart_RowCreated"
                            OnRowDataBound="gvOrderCart_RowDataBound" OnSorting="gvOrderCart_Sorting" DataKeyNames="ITEM,thickness,WIDTH,LENGTH,sequence"
                            EnableModelValidation="True" CssClass="grid-tabler">
                            <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                            <Columns>
                                <asp:CommandField ShowCancelButton="true" ControlStyle-Width="20px" ItemStyle-Height="20px" ItemStyle-Width="20px" CancelImageUrl="~/Images/Grid_Cancel.png" ShowDeleteButton="true" DeleteImageUrl="~/Images/delete-icon.png" ShowEditButton="true" EditImageUrl="~/Images/edit-icon.png" UpdateImageUrl="~/Images/Grid_UpdateSave.png" ButtonType="Image" UpdateText="Save" />
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" ID="panel_Configure" Visible="True" Width="88px">
                                            <p>
                                                <asp:LinkButton ID="linkbutton_Configure" 
                                                                runat="server" 
                                                                PostBackUrl='<%# "~/PreConfigurator.aspx" + 
                                                                            "?parent=pv" + 
                                                                            "&mode=update" + 
                                                                            "&item=" + System.Web.HttpUtility.UrlEncode((string)Eval("ITEM"))  + 
                                                                            "&size=" + System.Web.HttpUtility.UrlEncode(((string)Eval("SIZE")).Replace("&", "and").Replace("#", "").Replace("+", "")) + 
                                                                            "&description=" + System.Web.HttpUtility.UrlEncode(((string)Eval("DESCRIPTION")).Replace("&", "and").Replace("#", "").Replace("+", "")) + 
                                                                            "&uom=" + Eval("price_uom") + 
                                                                            "&sequence=" + Eval("sequence") + 
                                                                            "&orderqty=" + Eval("qty") + 
                                                                            "&locref=" + System.Web.HttpUtility.UrlEncode(((string)Eval("location_reference")).Replace("&", "and").Replace("#", "").Replace("+", "")) + 
                                                                            "&universalOptions=" + Eval("has_universal_options") %>'
                                                    Text="Configure/Copy" OnClientClick="newWindowClick('configurator');" Visible="True"
                                                    CssClass="handHover" ForeColor="Black" Width="88px" /><br />
                                        </asp:Panel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="top" Wrap="False" />
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <ControlStyle Width="88px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:Panel runat="server" ID="panel_Duplicate" Visible="True" Width="0px">
                                            <p style="margin-top: -3px;">
                                                <asp:LinkButton ID="linkbutton_Duplicate" runat="server" PostBackUrl='<%# "~/PreConfigurator.aspx?parent=pv&mode=duplicate&item=" + System.Web.HttpUtility.UrlEncode((string)Eval("ITEM"))  + "&size=" + System.Web.HttpUtility.UrlEncode(((string)Eval("SIZE")).Replace("&", "and").Replace("#", "").Replace("+", "")) + "&description=" + System.Web.HttpUtility.UrlEncode(((string)Eval("DESCRIPTION")).Replace("&", "and").Replace("#", "").Replace("+", "")) + "&uom=" + Eval("price_uom") + "&sequence=" + Eval("sequence") + "&orderqty=" + Eval("qty") + "&locref=" + System.Web.HttpUtility.UrlEncode(((string)Eval("location_reference")).Replace("&", "and").Replace("#", "").Replace("+", "")) %>'
                                                    Text="" OnClientClick="newWindowClick('configurator');" Visible="True" ForeColor="Black"
                                                    Width="0px" />
                                            </p>
                                        </asp:Panel>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Height="10px" Wrap="False" />
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <ControlStyle Height="10px" Width="0px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="ITEM" HeaderText="Item" ReadOnly="True" SortExpression="ITEM">
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SIZE" HeaderText="Size" ReadOnly="True" SortExpression="SIZE"
                                    HtmlEncode="False">
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescriptionAndWOPhrase" HeaderText="Description" ReadOnly="True"
                                    SortExpression="DESCRIPTION" ConvertEmptyStringToNull="False">
                                    <ItemStyle Wrap="True" HorizontalAlign="Left" VerticalAlign="Top" CssClass="wrapstyle" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="line_message" HeaderText="Message" SortExpression="line_message">
                                    <ItemStyle HorizontalAlign="Left" Wrap="True" VerticalAlign="Top" CssClass="wrapstyle" />
                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="line_message" Visible="False">
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="location_reference" HeaderText="Location Reference" SortExpression="location_reference">
                                    <ItemStyle HorizontalAlign="Left" Wrap="True" VerticalAlign="Top" CssClass="wrapstyle" />
                                    <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="qty" HeaderText="Qty" SortExpression="qty">
                                    <ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top" CssClass="wrapstyle" />
                                    <HeaderStyle Wrap="True" HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="uom" HeaderText="UOM" ReadOnly="True" SortExpression="uom">
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="price" HeaderText="Price*" ReadOnly="True" SortExpression="price"
                                    DataFormatString="{0:#,##0.0000}" HtmlEncode="False" Visible="False">
                                    <ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="retail_price" HeaderText="Retail Price" ReadOnly="True" SortExpression="retail_price"
                                    DataFormatString="{0:#,##0.0000}" HtmlEncode="False" Visible="False">
                                    <ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="price_uom" HeaderText="UOM " ReadOnly="True" SortExpression="price_uom"
                                    Visible="False">
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="extension" HeaderText="Extension*" ReadOnly="True" SortExpression="extension"
                                    DataFormatString="{0:#,##0.00}" HtmlEncode="False" Visible="False">
                                    <ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="retail_extension" HeaderText="Retail Extension" ReadOnly="True" SortExpression="retail_extension"
                                    DataFormatString="{0:#,##0.00}" HtmlEncode="False" Visible="False">
                                    <ItemStyle Wrap="False" HorizontalAlign="Right" VerticalAlign="Top" />
                                    <HeaderStyle Wrap="False" HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate></ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle Font-Names="helvatica" Font-Overline="False" />
                        </asp:GridView>
                    </div>
                    <div class="pagination-sec">
                        <div class="info">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-sec form submit" style="float: left; padding-bottom: 12px;">
            <p>
                <asp:Label runat="server" ID="lblPriceDisclaimer" Text="* Prices are estimates and may change upon final submission." />
            </p>
            <div id="DeleteButtonDiv" runat="server">
                <br />
                <asp:Button ID="btnDeleteAll" runat="server" Text="Delete All" CssClass="btn btn-default cartbuttondefault releasebuttonalign btnprodsearch"
                    OnClick="btnDeleteAll_Click" TabIndex="28" UseSubmitBehavior="False" />
            </div>
        </div>
        <div style="float: left; padding-bottom: 20px">
            <asp:Label Height="14" ID="lblNegativeQuantityError" runat="server" CssClass="ErrorTextRed"
                Text="Zero or negative quantity not allowed." Visible="False"></asp:Label>
        </div>
        <div style="float: left; padding-bottom: 20px">
            <asp:Label ID="lblQuantityError" runat="server" CssClass="ErrorTextRed" Text="One or more quantities do not conform with the required multiple."
                Visible="False"></asp:Label>
            <asp:Label ID="lblQuantityErrorTable" runat="server" Visible="False"></asp:Label>
        </div>
        <div id="CartTotalDiv" runat="server" visible="false">
            <table class="custom-tabler" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <th>Cart Total
                    </th>
                </tr>
                <tr>
                    <td class="pull-left" style="border-left: 0px !important; border-bottom: 0px !important; font-size: 12px !important"><asp:Label runat="server" ID="lblMinAmount" Text="" ForeColor="Red" /></td>
                    <td class="pull-right">
                        <asp:Label ID="Label8" runat="server" CssClass="FieldLabel" Text="Total"></asp:Label>
                        <asp:Label ID="lblLineTotal" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="EmptyCartDiv" runat="server" style="text-align: left">
            <div id="SubmitButtons" runat="server" visible="false" class="pull-right btnbtm">
                <asp:Button ID="btnSubmitOrder" runat="server" Text="Submit" OnClick="btnSubmitOrder_Click"
                    CssClass="btn btn-primary" TabIndex="30" UseSubmitBehavior="False" OnClientClick="CartSubmiteHideCancelAndSubmitButtons()" />
                <asp:Button ID="btnEditCart" runat="server" Text="Return to Cart" OnClick="btnEditCart_Click"
                    CssClass="btn btn-default" TabIndex="29" UseSubmitBehavior="False" />
            </div>
            <div id="CartButtons" runat="server" class="pull-right btnbtm">
                <asp:Button ID="btnCartCheckout" runat="server" Text="Proceed to Checkout" OnClick="btnCartCheckout_Click"
                    CssClass="btn btn-primary" UseSubmitBehavior="False" TabIndex="31" />
                <asp:Button ID="btnContinueShopping" runat="server" Text="Continue Shopping" CssClass="btn btn-default"
                    UseSubmitBehavior="False" TabIndex="30" OnClick="btnContinueShopping_Click" />
            </div>
        </div>
        <div id="pnlOrderConfirmation" runat="server" cssclass="ModalBackground" style="visibility: hidden; width: 100%; background-color: gray; float: left; position: absolute; top: 0; left: 0; opacity: 0.99; filter: alpha(opacity = 99); z-index: 1050;">
            <div class="BG_popUp">
            </div>
            <div id="pnlinnerorder" runat="server" style="left: 0px; top: 0px; width: 100%; background: grey;"
                class="pnlPopUp">
                <div class="modal-dialog" id="pnlQuoteHeaderDragHeader" runat="server">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p>
                                <asp:Label ID="lblLine1" runat="server" CssClass="Field" />
                                <asp:Label ID="lblLine2" runat="server" CssClass="Field" />
                                <asp:Label ID="lblError" runat="server" CssClass="ErrorTextRed" />
                            </p>
                        </div>
                        <div class="modal-footer" style="width: 100%">
                            <asp:Button ID="btnClose" runat="server" Text="OK" OnClick="btnClose_Click" CssClass="btn btn-primary" />
                        </div>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>
</div>
<cc1:CalendarExtender ID="calRequestedDate" runat="server" PopupButtonID="txtRequestedDate"
    Format="MM/dd/yyyy" TargetControlID="txtRequestedDate" CssClass="cal_Theme1">
</cc1:CalendarExtender>
