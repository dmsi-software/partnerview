﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_SettingsModal : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate(object obj);
    public event OKClickedDelegate OnOKClicked;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (OnOKClicked != null)
            OnOKClicked(null); // will be caught by the parent
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        Session["KeepModalOpen"] = false;
        Session["KeepModalOpen"] = null;

        Label success = this.PasswordMaintenanceSettings.FindControl("lblSuccess") as Label;
        if (success != null)
            success.Text = "";

        Label validationerror = this.PasswordMaintenanceSettings.FindControl("lblValidationError") as Label;
        if (validationerror != null)    
            validationerror.Text = "";
    }
}