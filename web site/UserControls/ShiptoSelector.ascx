<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShiptoSelector.ascx.cs"
    Inherits="UserControls_ShiptoSelector" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style4 {
        height: 23px;
        width: 100px;
    }

    .style5 {
        height: 23px;
        width: 154px;
    }

    .MarginLeft {
        margin-left: 45px;
    }
</style>
<table cellpadding="0" cellspacing="2">
    <tr>
        <td colspan="2" valign="top" style="width: 100%; text-align: left">
            <asp:Panel ID="pnlScroller" runat="server" ScrollBars="None">
                <ul class="retail poupEdit">
                    <li>
                        <asp:Label ID="Label2" runat="server" CssClass="Editquotelabel" Text="Name"></asp:Label>
                        <asp:TextBox ID="txtCustomerName" runat="server" MaxLength="30"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label3" runat="server" CssClass="Editquotelabel" Text="Address 1"></asp:Label>
                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="30"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label5" runat="server" CssClass="Editquotelabel" Text="Address 2"></asp:Label>
                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="30"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label6" runat="server" CssClass="Editquotelabel" Text="Address 3"></asp:Label>
                        <asp:TextBox ID="txtAddress3" runat="server" MaxLength="30"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label7" runat="server" CssClass="Editquotelabel" Text="City"></asp:Label>
                        <asp:TextBox ID="txtCity" runat="server" MaxLength="20"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label12" runat="server" CssClass="Editquotelabel" Text="State"></asp:Label>
                        <asp:TextBox ID="txtState" runat="server" Width="15% " MaxLength="2"></asp:TextBox>
                        <asp:Label ID="Label13" runat="server" CssClass="PopupInfolabel" Text="ZIP"></asp:Label>
                        <asp:TextBox ID="txtZip" runat="server" Width="28% " MaxLength="10"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label8" runat="server" CssClass="Editquotelabel" Text="Country"></asp:Label>
                        <asp:TextBox ID="txtCountry" runat="server" MaxLength="8"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label9" runat="server" CssClass="Editquotelabel" Text="Phone"></asp:Label>
                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="30"></asp:TextBox>
                    </li>
                </ul>
            </asp:Panel>
        </td>
    </tr>
</table>
<div class="modal-footer" style="width: 100%; padding-right: 0px !important;">
    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="btn btnpopup-primary btnpopupleft" OnClick="btnOK_Click" />

    <asp:Button ID="btnReset" runat="server" Text="Delete" OnClick="btnReset_Click" CssClass="btn btnpopup-primary btnpopupleft"
        Visible="false" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btnpopup-default btnpopupleft" OnClick="btnCancel_Click" />
</div>
<cc1:MaskedEditExtender ID="meeShiptoPhone" runat="server" Mask="(999)999-9999" MaskType="Number"
    TargetControlID="txtPhone" ClearMaskOnLostFocus="False" AutoComplete="False">
</cc1:MaskedEditExtender>