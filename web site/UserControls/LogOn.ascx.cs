using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.Profile;

public partial class UserControls_Login : System.Web.UI.UserControl
{
    protected DataManager _dataManager;
    protected ExceptionManager _exceptionManager;
    protected EventLog _eventLog;
    protected SecurityManager _securityManager;

    protected string _errorMessageParam;
    protected string _errorMessageCatch;
    protected int _passwordRC;

    private Progress.Open4GL.Proxy.Connection _ProgressConnection;
    private Dmsi.Agility.EntryNET.EntryNET _ProgressApplicationObject;


    protected Boolean _errorMessage_Boolean;
    protected string _errorMessage_Text;
    protected string _myUserName;
    protected string _validationErrorText;

    [DefaultValue(null)]
    public Progress.Open4GL.Proxy.Connection ProgressConnection
    {
        get { return _ProgressConnection; }
        set { _ProgressConnection = value; }
    }

    /// <summary>
    /// ASP application SHOULD cache this
    /// </summary>
    [DefaultValue(null)]
    public Dmsi.Agility.EntryNET.EntryNET ProgressApplicationObject
    {
        get { return _ProgressApplicationObject; }
        set { _ProgressApplicationObject = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtUserName.Focus();
            // Get cookie from the current request.
            HttpCookie cookie = Request.Cookies.Get("dmsiloginname");

            // Check if cookie exists in the current request.
            if (cookie != null)
            {
                if (cookie.Value != "")
                {
                    txtUserName.Text = cookie.Value.ToString();
                    cbxRememberUser.Checked = true;
                }
            }
        }
        else
        {
            txtForgotUserName.Focus();
        }
        if ((string)Session["GenericLogin"] != "True")
        {

            if ((Session["LoggedIn"] != null) && ((bool)Session["LoggedIn"] == true))
            {
                Session["InventoryRetainpriorLogin_SelectedDetails"] = null;
                Session["QuoteEditAddItemsRetainpriorLogin_SelectedDetails"] = null;
                Session.RemoveAll();
                Session["LoggedIn"] = false;
            }

            _dataManager = new DataManager();
            _exceptionManager = new ExceptionManager();
            _eventLog = new EventLog();
            _securityManager = new SecurityManager();

            lblUserName.Text = "";
            lblPassword.Text = "";

            if (_dataManager.GetCache("ChangePasswordBeforeLoginSuccessful") != null)
            {
                _passwordRC = (int)_dataManager.GetCache("passRC");
                txtUserName.Text = (string)_dataManager.GetCache("UserName");
                txtPassword.Text = (string)_dataManager.GetCache("Password");
                this.btnLogin_Click(null, null);
                _dataManager.RemoveCache("ChangePasswordBeforeLoginSuccessful");
            }
        }
        else
        {
            _dataManager = new DataManager();
            _exceptionManager = new ExceptionManager();
            _eventLog = new EventLog();
            _securityManager = new SecurityManager();
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        lblExtraMessage.Text = "";
        txtUserName.Text = "";
        lblUserName.Text = "";
        txtPassword.Text = "";
        lblPassword.Text = "";
        lblExtraMessage.Text = "";
        lblExtraMessage.Visible = false;
        cbxRememberUser.Checked = false;

        /* Analytics Tracking */
        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Reset Button Click";
        aInput.Label = "";
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

    }

    public void btnLogin_Click(object sender, System.EventArgs e)
    {
        //reset golbals
        Session["UserPref_DisplaySetting"] = null;
        Session["UserPref_Retain"] = null;
        Session["MarkupFactor"] = null;
        Session["MarginFactor"] = null;
        Session["MarkupSetting"] = null;

        if (cbxRememberUser.Checked)
        {
            // Create cookie.
            HttpCookie cookie = new HttpCookie("dmsiloginname");
            // Set value of cookie to current date time.
            cookie.Value = txtUserName.Text;
            // Set cookie to expire in 1 year.
            cookie.Expires = DateTime.Now.AddDays(365d);
            // Insert the cookie in the current HttpResponse.
            Response.Cookies.Add(cookie);
        }
        else
        {
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                if (cookieName == "dmsiloginname")
                {
                    Request.Cookies[i].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(Request.Cookies[i]);
                }
            }
        }

        Session["FromPage"] = "./Default.aspx";

        if ((txtUserName.Text.Length == 0) || (txtPassword.Text.Length == 0))
        {
            if (txtUserName.Text.Length == 0)
            {
                lblUserName.Text = "User name is required.";
            }
            if (txtPassword.Text.Length == 0)
            {
                lblPassword.Text = "Password is required.";
            }
        }
        else
        {

            if (txtUserName.Text.ToLower() != ConfigurationManager.AppSettings["monitorUsername"].ToString().ToLower())
            {
                try
                {
                    string sprevcurrentBranch = (string)_dataManager.GetCache("currentBranchID");
                    string sprevUserName = (string)_dataManager.GetCache("userNameAtLogin");

                    if ((string)Session["GenericLogin"] == "True")
                    {
                        Session["InventoryRetainpriorLogin_SelectedDetails"] = null;
                        Session["QuoteEditAddItemsRetainpriorLogin_SelectedDetails"] = null;
                    }

                    _dataManager.SetCache("userNameAtLogin", txtUserName.Text.ToLower());
                    //Login passes back an integer...
                    _passwordRC = _securityManager.UserLogin("Product=PrtnrView", txtUserName.Text.ToLower(), txtPassword.Text, ref _errorMessageParam, ref _errorMessageCatch);
                    string sCurrSessionContextId = (string)_dataManager.GetCache("opcContextId");
                    //pass = -40;
                    _dataManager.SetCache("haveCalledLogin", true);
                    Session["UserName"] = txtUserName.Text;
                    Session["LoginMessage"] = _errorMessageParam.ToString().Replace("\n", "<br />");
                    Session["passRC"] = _passwordRC;
                    string errorMessage;
                    errorMessage = _errorMessageParam;
                    errorMessage += _errorMessageCatch;
                    _exceptionManager.Message = errorMessage;
                    errorMessage = errorMessage.Replace("\n", "<br />");
                    lblExtraMessage.Text = errorMessage;

                    switch (_passwordRC)
                    {
                        case -50:
                        case 0: // Success

                            Session["Shipto_txtCustomerName"] = null;
                            Session["ShiptoQuote_txtCustomerName"] = null;
                            Session["ShiptoQuoteEdit_txtCustomerName"] = null;

                            string sCurrentBranch = (string)_dataManager.GetCache("currentBranchID");

                            if (sprevcurrentBranch != null)
                            {
                                int iCompare = string.Compare(sprevcurrentBranch.ToLower().Trim(), sCurrentBranch.ToLower().Trim());
                                if (iCompare != 0)
                                {
                                    Session["dspvarDataSet"] = null;
                                    Session["WebHierarchicalDataGridDetail_DataSource"] = null;
                                    Session["InventoryRetainpriorLogin_SelectedDetails"] = null;
                                    Session["QuoteEditAddItemsRetainpriorLogin_SelectedDetails"] = null;
                                    Session["btnReturnToItem_Click"] = null;
                                    Session["priorLoginSearchValue"] = null;
                                    Session["PriorLogin_Group"] = null;
                                    Session["PriorLoginBreadCrumb_GroupDetails"] = null;
                                    Session["CachedInventoryDataSetCopy"] = null;
                                    Session["CachedInventoryDataSet"] = null;
                                    Session["priorLoginSelectedNodeCatGroup"] = null;
                                    Session["priorLoginSelectedImageDetails"] = null;
                                    Session["InventoryRetainPreviousUrl"] = null;
                                    Session["QuoteEditAddItemsRetainPreviousUrl"] = null;

                                    Session["priorLoginCheckedItems"] = null;
                                    Session["InventorypriorLoginCheckedItems"] = null;
                                    Session["QuoteEditAddItemspriorLoginCheckedItems"] = null;

                                    Session["GenericLogin"] = null;
                                    Session["dsReferencedCategoryDataSet"] = null;
                                    Session["priorLoginSelectedImageDetails"] = null;
                                    Session["btnViewQuickList_Click"] = null;
                                    Session["btnViewQuickList_Click_QEAI"] = null;
                                    Session["InventoryImageClicked"] = null;
                                    Session["QuoteEditAddItemsImageClicked"] = null;
                                    Session["priorLoginSelectedNodeCatGroup"] = null;
                                    Session["LastSearchExecuted"] = null;
                                    Session["dsInventoryDataSet"] = null;
                                    Session["LastSearchExecuted"] = null;
                                    Session["LastSearchExecuted_QEAI"] = null;
                                    Session["CachedDetailInventoryRow"] = null;
                                    Session["CachedItemDetailXrefDataSet"] = null;
                                    Session["CachedInventoryDetailSearchCriteria"] = null;
                                    Session["RetainFetchGroupDetails"] = null;
                                    Session["RetainFetchGroupDetails_QEAI"] = null;
                                    Session["ViewInventoryPrices"] = null;
                                    Session["Show_Available"] = null;
                                    Session["ViewInventoryPriceDetail"] = null;
                                    Session["Show_Available"] = null;
                                    Session["Show_OnOrder"] = null;
                                    Session["Show_Available_By_Branch"] = null;
                                    Session["Show_Available_Detail"] = null;
                                    Session["Show_OnOrder_By_Branch"] = null;
                                    Session["Show_OnOrder_Detail"] = null;
                                    Session["UIisOnInventoryDetail"] = null;
                                    Session["apply_payments"] = null;
                                    Session["create_cash_on_acct"] = null;
                                    Session["apply_open_acct_credits_to_invoices"] = null;
                                    Session["ElementHealthCheck"] = null;
                                    Session["view_realtime_invoice"] = null;
                                    Session["disable_edit_shipping_address"] = null;
                                    Session["Shipto_txtCustomerName"] = null;
                                    Session["ShiptoQuote_txtCustomerName"] = null;
                                    Session["ShiptoQuoteEdit_txtCustomerName"] = null;
                                    Session["link_external_website"] = null;
                                    Session["external_link"] = null;
                                    Session["external_url"] = null;
                                    Session["newBranchIDforShiptoFetch"] = null;
                                }
                                else
                                {
                                    Session["dspvarDataSet"] = null;
                                    Session["WebHierarchicalDataGridDetail_DataSource"] = null;
                                    Session["UserPref_DisplaySetting"] = null;
                                    Session["UserPref_Retain"] = null;
                                    Session["MarkupFactor"] = null;
                                    Session["ViewInventoryPrices"] = null;
                                    Session["Show_Available"] = null;
                                    Session["ViewInventoryPriceDetail"] = null;
                                    Session["Show_Available"] = null;
                                    Session["Show_OnOrder"] = null;
                                    Session["Show_Available_By_Branch"] = null;
                                    Session["Show_Available_Detail"] = null;
                                    Session["Show_OnOrder_By_Branch"] = null;
                                    Session["Show_OnOrder_Detail"] = null;
                                    Session["UIisOnInventoryDetail"] = null;
                                    Session["UserPref_Retain"] = null;
                                    Session["MarkupFactor"] = null;
                                    Session["InventoryRetainPreviousUrl"] = null;
                                    Session["QuoteEditAddItemsRetainPreviousUrl"] = null;
                                    Session["InventoryRetainpriorLogin_SelectedDetails"] = null;
                                    Session["QuoteEditAddItemsRetainpriorLogin_SelectedDetails"] = null;
                                    Session["apply_payments"] = null;
                                    Session["create_cash_on_acct"] = null;
                                    Session["apply_open_acct_credits_to_invoices"] = null;
                                    Session["ElementHealthCheck"] = null;
                                    Session["view_realtime_invoice"] = null;
                                    Session["disable_edit_shipping_address"] = null;
                                    Session["Shipto_txtCustomerName"] = null;
                                    Session["ShiptoQuote_txtCustomerName"] = null;
                                    Session["ShiptoQuoteEdit_txtCustomerName"] = null;
                                    Session["link_external_website"] = null;
                                    Session["external_link"] = null;
                                    Session["external_url"] = null;
                                    Session["newBranchIDforShiptoFetch"] = null;
                                }
                            }

                            //Before setting up user, reset to null in case previous login was a single ship-to.
                            Session["SingleShipto"] = null;

                            LoginAccepted_SetupUser();
                            if (Session["GenericLogin"] != null)
                            {
                                if ((string)Session["GenericLogin"].ToString().ToLower() == "true")
                                {
                                    Session["RecallFetchAttributes"] = 1;

                                }
                            }
                            Session["GenericLogin"] = "False";

                            if ((bool)Session["LoggedIn"] == true)
                            {

                                /* Analytics Tracking on sucessfull log in */
                                AnalyticsInput aInput = new AnalyticsInput();

                                aInput.Type = "event";
                                aInput.Action = "Login Button Click";
                                aInput.Label = "";
                                aInput.Value = "0";

                                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                                am.BeginTrackEvent();
                            }

                            break;
                        case -1:  // System-type error or "No Customers"
                        case -10: // Invalid User Name or Password
                        case -20: // Login Failed. Your user account has been disabled.
                            if (_exceptionManager.MessageExists)
                            {
                                //while it comes from  generic login we need to maintain this for changepassword page outside login
                                string genericlogin = "false";
                                string iscustom = "No";
                                if ((string)Session["GenericLogin"] == "True")
                                {
                                    genericlogin = (string)Session["GenericLogin"];
                                    if (Session["IsCustomTheme"] != null)
                                    {
                                        iscustom = (string)Session["IsCustomTheme"];
                                    }
                                }

                                Dmsi.Agility.EntryNET.Connection connection = (Dmsi.Agility.EntryNET.Connection)Session["AgilityLibraryConnection"];

                                if (connection != null)
                                    connection.Disconnect();
                                Session["GenericLogin"] = genericlogin;
                                Session["IsCustomTheme"] = iscustom;
                                throw new Exception(_exceptionManager.Message);
                            }
                            break;
                        case -30: // Your are required to change your password at login.
                        case -40: // Your password has expired and must be changed.
                            GetThemeDetails();
                            Session["UserName"] = txtUserName.Text;
                            Response.Redirect("./ChangePassword.aspx", false);
                            break;

                        default:
                            _exceptionManager.Message = "Fatal Error, no details available...please contact Support.";
                            lblExtraMessage.Text = _exceptionManager.Message.ToString();
                            throw new Exception(_exceptionManager.Message);
                    }
                }
                catch (Exception exc)
                {
                    Dmsi.Agility.EntryNET.Connection connection = (Dmsi.Agility.EntryNET.Connection)Session["AgilityLibraryConnection"];

                    if (connection != null)
                        connection.Disconnect();

                    lblExtraMessage.Text = exc.Message;
                    lblExtraMessage.Visible = true;
                    _eventLog.LogError(EventTypeFlag.DB_Message, exc);
                }
            }
            else
            {
                lblExtraMessage.Text = "Invalid User Name or Password.";
                lblExtraMessage.Visible = true;
            }
        }
    }

    public void Profile_OnMigrateAnonymous(object sender, ProfileMigrateEventArgs args)
    {
    }

    private void LoginAccepted_SetupUser()
    {
        if ((_dataManager.GetCache("haveCalledLogin") != null) || (_dataManager.GetCache("ChangePasswordBeforeLoginSuccessful") != null))
        {
            bool hasDefaultShipto = false;
            PriceModeHelper pmh = new PriceModeHelper(_dataManager);

            _exceptionManager.Message = _errorMessageParam;
            _exceptionManager.Message += _errorMessageCatch;

            AgilityEntryNetUser user = new AgilityEntryNetUser();
            dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");
            dsCustomerDataSet dsCU = (dsCustomerDataSet)_dataManager.GetCache("dsCU");
            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dataManager.GetCache("dsPV");
            dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dataManager.GetCache("dsPVParam");
            _dataManager.SetCache("dsCustomerDataSet", dsCU);

            DataRow[] viewrealtimeinvoiceRow = dsPV.bPV_Action.Select("token_code ='" + "view_realtime_invoice" + "'");
            if (viewrealtimeinvoiceRow.Length == 1)
                Session["view_realtime_invoice"] = true;

            DataTable dtProperties = new DataTable();
            dtProperties.Columns.Add("propertyname");
            dtProperties.Columns.Add("Color");

            DataRow[] drDisplayMinOrdAmt = dsPVParam.ttparam_pv.Select("property_name='" + "display_min_order_hold_amount" + "'");

            if (drDisplayMinOrdAmt.Length > 0)
            {
                Session["display_min_order_hold_amount"] = drDisplayMinOrdAmt[0]["property_value"].ToString();
            }

            DataRow[] drDisplayExtDesc = dsPVParam.ttparam_pv.Select("property_name='" + "display_ext_desc" + "'");

            if (drDisplayExtDesc.Length > 0)
            {
                Session["display_ext_desc"] = drDisplayExtDesc[0]["property_value"].ToString();
            }

            DataRow[] drProductSearchQty = dsPVParam.ttparam_pv.Select("property_name='" + "product_search_qty" + "'");

            if (drProductSearchQty.Length > 0)
            {
                Session["product_search_qty"] = drProductSearchQty[0]["property_value"].ToString();
            }

            DataRow[] drAllowACH = dsPVParam.ttparam_pv.Select("property_name='" + "allow_ach_payments" + "'");

            if (drAllowACH.Length > 0)
            {
                Session["allow_ach_payments"] = drAllowACH[0]["property_value"].ToString();
            }

            DataRow[] drAllowCCDiscounts = dsPVParam.ttparam_pv.Select("property_name='" + "allow_disc_when_cc" + "'");

            if (drAllowCCDiscounts.Length > 0)
            {
                if (((string)drAllowCCDiscounts[0]["property_value"]).ToLower() == "yes")
                    Session["allow_disc_when_cc"] = true;
                else
                    Session["allow_disc_when_cc"] = false;
            }

            DataRow[] drlink_external_website = dsPVParam.ttparam_pv.Select("property_name='" + "link_external_website" + "'");

            if (drlink_external_website.Length > 0 && ((string)drlink_external_website[0]["property_value"]).ToLower() == "yes")
            {
                Session["link_external_website"] = true;

                DataRow[] drexternal_link = dsPVParam.ttparam_pv.Select("property_name='" + "external_link" + "'");
                Session["external_link"] = ((string)drexternal_link[0]["property_value"]).ToString();

                DataRow[] drexternal_url = dsPVParam.ttparam_pv.Select("property_name='" + "external_url" + "'");
                Session["external_url"] = ((string)drexternal_url[0]["property_value"]).ToString();

                DataRow[] drexternal_web_ticket = dsPVParam.ttparam_pv.Select("property_name='" + "external_web_ticket" + "'");
                Session["external_web_ticket"] = ((string)drexternal_web_ticket[0]["property_value"]).ToString();
            }

            DataRow[] drThemeauthentication;
            drThemeauthentication = dsPV.bPV_Action.Select("token_code ='" + "disable_theme_administration" + "'");

            if (drThemeauthentication.Length > 0)
            {
                Session["disable_theme_authentication"] = "true";
            }
            else
            {
                Session["disable_theme_authentication"] = "false";
            }

            DataRow[] drCustomtheme;
            drCustomtheme = dsPVParam.ttparam_pv.Select("property_name='" + "use_custom_theme" + "'");

            if (drCustomtheme.Length > 0)
            {
                Session["IsCustomTheme"] = drCustomtheme[0]["property_value"].ToString();
            }

            if (Session["IsCustomTheme"] != null)
            {
                if (((string)Session["IsCustomTheme"]).ToLower() == "yes")
                {
                    if (!System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                    {

                        DataTable dtDynamictheming = new DataTable();
                        dtDynamictheming.Columns.Add("propertyname");
                        dtDynamictheming.Columns.Add("Color");

                        DataRow[] rows = dsSessionManager.ttSelectedProperty.Select("propertyName='pvthemeActiveTabTextColor' OR " +
                                                                                    "propertyName='pvthemeInactiveButtonTextColor' OR " +
                                                                                    "propertyName='pvthemeGridHeaderTextColor' OR " +
                                                                                    "propertyName='pvthemeActionButtonTextColor' OR " +
                                                                                    "propertyName='pvthemeActionButtonColor' OR " +
                                                                                    "propertyName='pvthemeActiveLinkTextColor' OR " +
                                                                                    "propertyName='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                                    "propertyName='pvthemeCustomerUserInformationTextColor' OR " +
                                                                                    "propertyName='pvthemeGridHeaderColor' OR " +
                                                                                    "propertyName='pvthemeHeaderBarBackGroundColor' OR " +
                                                                                    "propertyName='pvthemeInactiveButtonColor' ");

                        if (rows.Length == 0)
                        {
                            rows = dsPVParam.ttparam_pv.Select("property_name='pvthemeActiveTabTextColor' OR  " +
                                                               "property_name='pvthemeInactiveButtonTextColor' OR " +
                                                               "property_name='pvthemeGridHeaderTextColor' OR " +
                                                               "property_name='pvthemeActionButtonTextColor' OR " +
                                                               "property_name='pvthemeActionButtonColor' OR " +
                                                               "property_name='pvthemeActiveLinkTextColor' OR " +
                                                               "property_name='pvthemeActiveTabColorandRelatedUnderlineBar' OR " +
                                                               "property_name='pvthemeCustomerUserInformationTextColor' OR " +
                                                               "property_name='pvthemeCustomerUserInformationTextColor' OR " +
                                                               "property_name='pvthemeGridHeaderColor' OR " +
                                                               "property_name='pvthemeHeaderBarBackGroundColor' OR " +
                                                               "property_name='pvthemeInactiveButtonColor' ");
                        }

                        if (rows.Length > 0)
                        {
                            for (int i = 0; i < rows.Length; i++)
                            {
                                DataRow drnew;
                                drnew = dtDynamictheming.NewRow();
                                drnew["propertyname"] = rows[i]["propertyName"].ToString();
                                drnew["Color"] = rows[i]["propertyvalue"].ToString();
                                dtDynamictheming.Rows.Add(drnew);
                                dtDynamictheming.AcceptChanges();
                            }
                        }

                        if (dtProperties.Rows.Count > 0)
                        {
                            Session["ChangeTheme"] = dtDynamictheming;
                        }

                        CSSParser css = new CSSParser();

                        css.ReadCSSFile(Server.MapPath(@"~/Documents/DynamicThemes/DefaultTheme.css"), dtDynamictheming);

                        string sparsed;
                        sparsed = css.ToString();

                        if (System.IO.File.Exists(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css"))
                        {
                            System.IO.File.Delete(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css");
                        }

                        System.IO.File.WriteAllText(Server.MapPath(@"~/Documents/DynamicThemes/") + "Custom" + ".css", sparsed.ToString());
                    }
                }
            }

            StoreCustBranchTable(dsCU);

            switch (_passwordRC)
            {
                case 0:
                case -30:
                case -40:
                case -50:

                    ProfileCommon pc = Profile.GetProfile((string)Session["UserName"]);
                    Profile.Initialize((string)Session["UserName"], true);

                    Profile.display_Setting = pc.display_Setting;

                    try
                    {
                        Profile.price_Markup_Factor = Convert.ToDecimal(pc.price_Markup_Factor.ToString());
                    }
                    catch
                    {
                        Profile.price_Markup_Factor = 1;
                    }

                    try
                    {
                        Profile.price_Markup_Factor_List = Convert.ToDecimal(pc.price_Markup_Factor_List.ToString());
                    }
                    catch
                    {
                        Profile.price_Markup_Factor_List = 1;
                    }

                    SetDisplaySettingBasedOnActionAllocations(pc, pmh);

                    foreach (dsPVParamDataSet.ttparam_pvRow paramrow in dsPVParam.ttparam_pv.Rows)
                    {
                        user.AddContext(paramrow.property_name, paramrow.property_value);
                    }

                    foreach (dsSessionMgrDataSet.ttSelectedPropertyRow row in dsSessionManager.ttSelectedProperty.Rows)
                    {
                        // MDM user.AddContext(row.propertyName, row.propertyValue, row.contextID);
                        user.AddContext(row.propertyName, row.propertyValue);
                    }

                    user.AddContext("currentUserPassword", txtPassword.Text);

                    _dataManager.SetCache("currentUserType", user.GetContextValue("UserType"));

                    // cache the user object
                    _dataManager.SetCache("AgilityEntryNetUser", user);

                    // store user specific defaults

                    _dataManager.SetCache("DisplayCanceledSO", user.GetContextValue("display_canceled_so"));

                    _dataManager.SetCache("AllowRetailPricing", user.GetContextValue("allow_retail_pricing"));

                    _dataManager.SetCache("AllowListBasedRetailPrice", user.GetContextValue("allow_list_based_retail_price"));

                    _dataManager.SetCache("DisplayAvailDiscounts", user.GetContextValue("display_avail_disc_in_billing"));

                    _dataManager.SetCache("DisplayCustItemQlist", user.GetContextValue("display_cust_item_qlist"));

                    _dataManager.SetCache("currentBranchID", user.GetContextValue("currentBranchID"));
                    DataRow[] rows = dsCU.ttbranch_cust.Select("branch_id='" + user.GetContextValue("currentBranchID") + "'");
                    if (rows.Length > 0)
                    {
                        _dataManager.SetCache("CurrentBranchRow", rows[0]);
                    }

                    if (Profile.loggedInto453Already == false)
                    {
                        Profile.loggedInto453Already = true;
                        string defaultValue = System.Web.Profile.ProfileBase.Properties["gvInventoryPricing_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvInventoryPricing_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderHeaders_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvSalesOrderHeaders_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderDetail_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvSalesOrderDetail_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvSalesOrderItems_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvSalesOrderItems_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteHeaders_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvQuoteHeaders_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteDetail_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvQuoteDetail_Columns_Visible = defaultValue;

                        defaultValue = System.Web.Profile.ProfileBase.Properties["gvQuoteItems_Columns_Visible"].DefaultValue.ToString();
                        Profile.gvQuoteItems_Columns_Visible = defaultValue;

                        Profile.Save();
                    }

                    break;

                default:
                    lblExtraMessage.Text = _exceptionManager.Message;
                    throw new Exception(_exceptionManager.Message);
            }

            _eventLog.LogEvent(EventTypeFlag.ApplicationEvent, "Successful login, username '" + (string)_dataManager.GetCache("userNameAtLogin").ToString().ToLower() + "'.");

            //TODO: create DataManager.UserConfigUpdate method...
            //Read default.config, [user name].config ...cross-check for new schema, update and write new [user name].config
            //...and cache all user's grid setting for all grids...

            // The following code is being commented out per Mike M.  
            // the interface to Datamanager.UserConfigUpdate does not play well with others 

            string default_cust_key = dsPV.bPV_CoInfo.Rows[0]["default_cust_key"].ToString();
            int default_shipto_seq = (int)dsPV.bPV_CoInfo.Rows[0]["default_shipto_seq"];
            Session["LoggedIn"] = true;

            //MDM - Added cache values for PV/Visual Cafe; view_component_prices and view_incomplete_parent_price

            //Initialize to false on each login; even if in same session...
            Session["VC_view_component_prices"] = false;
            Session["VC_view_incomplete_parent_price"] = false;
            Session["VC_print_extended_price_on_vc_form"] = false;
            Session["UserAction_AllowRetailPricing"] = false;
            Session["UserAction_DisableOnScreenUserPreferences"] = false;

            if (dsPV != null &&
                dsPV.bPV_Action != null &&
                dsPV.bPV_Action.Rows.Count > 0)
            {
                foreach (DataRow row in dsPV.bPV_Action.Rows)
                {
                    if (row["token_code"].ToString() == "disable_edit_shipping_address")
                    {
                        Session["disable_edit_shipping_address"] = true;
                    }

                    if (row["token_code"].ToString() == "apply_payments")
                    {
                        Session["apply_payments"] = true;
                    }

                    if (row["token_code"].ToString() == "create_cash_on_acct")
                    {
                        Session["create_cash_on_acct"] = true;
                    }

                    if (row["token_code"].ToString() == "apply_open_acct_credits_to_invoices")
                    {
                        Session["apply_open_acct_credits_to_invoices"] = true;
                    }

                    if (row["token_code"].ToString() == "view_component_prices")
                    {
                        Session["VC_view_component_prices"] = true;
                    }

                    if (row["token_code"].ToString() == "view_incomplete_parent_price")
                    {
                        Session["VC_view_incomplete_parent_price"] = true;
                    }

                    if (row["token_code"].ToString() == "print_extended_price_on_vc_form")
                    {
                        Session["VC_print_extended_price_on_vc_form"] = true;
                    }

                    if (row["token_code"].ToString() == "enable_retail_price_display")
                    {
                        Session["UserAction_AllowRetailPricing"] = true;

                        if (pmh.OnlyNetModeEnabled()) { Session["UserAction_AllowRetailPricing"] = false; }
                    }

                    if (row["token_code"].ToString() == "disable_on_screen_user_pref_control")
                    {
                        Session["UserAction_DisableOnScreenUserPreferences"] = true;
                    }
                }
            }

            if (user.GetContextValue("allow_retail_pricing").Trim().ToLower() == "no" || (bool)Session["UserAction_AllowRetailPricing"] == false)
            {
                _dataManager.SetCache("AllowRetailPricing", "no");
                Profile.display_Setting = "Net price";
                //Profile.Save();
            }

            if (user.GetContextValue("allow_retail_pricing").Trim().ToLower() == "yes" && (bool)Session["UserAction_AllowRetailPricing"] == true)
            {
                Session["SaveDisplayHasBeenSet"] = true;

                if (Profile.display_Setting == "Net price" || Profile.display_Setting == "Retail price - markup on cost" || Profile.display_Setting == "Retail price")
                    Session["MarkupFactor"] = Profile.price_Markup_Factor;

                if (Profile.display_Setting == "Retail price - discount from list")
                    Session["MarkupFactor"] = Profile.price_Markup_Factor_List;
            }
            else
                Session["MarkupFactor"] = Profile.price_Markup_Factor;

            FormsAuthentication.SetAuthCookie(txtUserName.Text, true);

            int counter = 0;

            retry: try
            {
                SqlDataSourcePVDatabase.SelectParameters["UserName"].DefaultValue = txtUserName.Text.ToLower();
                DataView view = (DataView)SqlDataSourcePVDatabase.Select(DataSourceSelectArguments.Empty);

                if (view == null && (view != null && view.Count == 0))
                {
                    SqlDataSourcePVDatabase.InsertCommand = "INSERT INTO tbl_user (username) VALUES ('" + txtUserName.Text.ToLower() + "')";
                    SqlDataSourcePVDatabase.Insert();
                }
            }
            catch
            {
                counter++;
                if (counter < 4)
                {
                    Thread.Sleep(2000);
                    goto retry;
                }
                else
                    throw;
            }

            string fromPage = Page.Request.Url.ToString().ToLower();

            if (fromPage.ToLower().IndexOf("logon.aspx?source=stocknet", 0) != -1)
            {
                // when coming from LogOn change the URL to point to default
                fromPage = fromPage.Replace("logon", "default");
                Session["FromStockNET"] = fromPage;
                Session["FromPage"] = fromPage;
            }

            if (default_cust_key.Length == 0)
            {
                SendControlTo("./default.aspx?Display=Customer Locator");
                //Response.Redirect(Session["FromPage"].ToString(), false);
                Shipto ship = null;

                int ReturnCode;
                string MessageText;

                ship = new Shipto(out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                DataRow shiptoRow = cust.GetShiptoRow(default_shipto_seq);
                _dataManager.SetCache("ShiptoRowForInformationPanel", shiptoRow);
            }
            else
            {
                if (dsCU.ttCustomer.Rows.Count > 0)
                {
                    // TODO: Put this in the back-end, clear the default customer when not found
                    //       in the current current customer list per current branch
                    //       When there is only one customer for the current branch use that as the default 
                    //       customer.
                    foreach (DataRow row in dsCU.ttCustomer.Rows)
                    {
                        if (row["cust_key"].ToString() == default_cust_key)
                        {
                            _dataManager.SetCache("CustomerRowForInformationPanel", row); // this is the current display

                            int ReturnCode;
                            string MessageText;

                            Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                            {
                                Session["LoggedIn"] = null;
                                Response.Redirect("~/Logon.aspx");
                            }

                            bool isBranchShared = cust.IsBranchSharedForCustomer(_dataManager.GetCache("currentBranchID").ToString(), out ReturnCode, out MessageText);

                            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                            {
                                Session["LoggedIn"] = null;
                                Response.Redirect("~/Logon.aspx");
                            }

                            if (!isBranchShared)
                                _dataManager.RemoveCache("CustomerRowForInformationPanel");
                            else // get the ship-to of cached customer
                            {
                                _dataManager.SetCache("BranchShipTos", cust.BranchShipTos);

                                // get the default shipto, this will drive the tab
                                if (default_shipto_seq > 0)
                                {
                                    // Verify if the currently selected ship-to is valid for the current customer
                                    DataRow shiptoRow = cust.GetShiptoRow(default_shipto_seq);
                                    if (shiptoRow != null)
                                    {
                                        hasDefaultShipto = true;
                                        _dataManager.SetCache("ShiptoRowForInformationPanel", shiptoRow);
                                        shiptoRow = null;
                                    }
                                    else
                                        _dataManager.SetCache("ShiptoRowForInformationPanel", null);
                                }
                            }
                            cust.Dispose();
                            break;
                        }
                    }

                    // Check if this is a customer's customer log on, in some cases have only one customer and ship-to,
                    // Customer tab has to be hidden. Save a session variable for MainTabControl usage
                    if (dsCU.ttCustomer.Rows.Count == 1)
                    {
                        // Verify if the customer is valid for the current branch, "CurrentBranchRow" must be set first
                        _dataManager.SetCache("CustomerRowForInformationPanel", dsCU.ttCustomer.Rows[0]);

                        if (user.GetContextValue("HasMoreCustomers") != "TRUE") // customer from other branches
                            _dataManager.SetCache("SingleCust", dsCU.ttCustomer.Rows[0]);

                        int ReturnCode;
                        string MessageText;

                        Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                        {
                            Session["LoggedIn"] = null;
                            Response.Redirect("~/Logon.aspx");
                        }

                        bool isBranchShared = cust.IsBranchSharedForCustomer(_dataManager.GetCache("currentBranchID").ToString(), out ReturnCode, out MessageText);

                        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                        {
                            Session["LoggedIn"] = null;
                            Response.Redirect("~/Logon.aspx");
                        }

                        if (!isBranchShared)
                            _dataManager.RemoveCache("CustomerRowForInformationPanel");
                        else if (dsCU.Tables["ttShipto"].Rows.Count == 1)
                        {
                            // Check if the shipto is valid for the customer in the current branch
                            DataRow shiptoRow = cust.GetShiptoRow(((Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet.ttShiptoRow)dsCU.Tables["ttShipto"].Rows[0]).seq_num);

                            if (shiptoRow != null)
                            {
                                hasDefaultShipto = true;
                            }

                            // This will allow the program to be reset to the default values
                            // once the use switch to a branch where the values are valid again
                            if (user.GetContextValue("HasMoreCustomers") != "TRUE") // customer from other branches
                                _dataManager.SetCache("SingleShipto", dsCU.ttShipto.Rows[0]);  // this is needed during branch change         
                        }

                        cust.Dispose();
                    }
                }

                // if single shipto is present that means this user can only access one customer, so the locator is not applicable
                if (!hasDefaultShipto && _dataManager.GetCache("SingleShipto") == null)
                    SendControlTo("./default.aspx?Display=Customer Locator");
                else
                    SendControlTo(Session["FromPage"].ToString());


            }
        }
    }

    private void SetDisplaySettingBasedOnActionAllocations(ProfileCommon pc,
                                                           PriceModeHelper pmh)
    {
        Profile.retain_Display = pc.retain_Display;

        if (Profile.display_Setting == "Net Price")
        {
            Profile.display_Setting = pmh.NETPRICE;
        }

        if (Profile.display_Setting == "Retail Price")
        {
            Profile.display_Setting = pmh.RETAILPRICE;
        }

        Profile.Save();

        if (Profile.retain_Display == false)
        {
            Session["UserPref_DisplaySetting"] = pmh.NETPRICE;
        }

        if (!pmh.HaveNetModeEnabled() && !pmh.HaveRetailPricingEnabled())
        {
            throw new Exception(pmh.ERROR_ACTIONALLOCATIONMISMATCH);
        }

        if (pmh.InAModeUserDoesntHaveAccessTo())
        {
            Session["UserPref_DisplaySetting"] = pmh.DefaultUserPreferenceDisplaySetting();
        }
    }

    private void StoreCustBranchTable(dsCustomerDataSet dsCU)
    {
        dsCustomerDataSet.ttbranch_custDataTable table = new dsCustomerDataSet.ttbranch_custDataTable();

        foreach (dsCustomerDataSet.ttbranch_custRow row in dsCU.ttbranch_cust.Rows)
        {
            table.LoadDataRow(row.ItemArray, true);
        }

        Session["ttbranch_cust"] = table;
    }

    protected void SendControlTo(string destinationURL)
    {
        string[] parts = destinationURL.Split(new char[] { '?' });

        Session["IsHome"] = parts.Length == 1;

        switch (_passwordRC)
        {
            case 0:
                Response.Redirect(destinationURL, false);
                break;

            case -30:
            case -40:
            case -50:
                Session["FromPage"] = destinationURL;
                Response.Redirect("./ChangePassword.aspx", false);
                break;
        }
    }

    private void GetSelectedCustDetails()
    {
        if ((_dataManager.GetCache("haveCalledLogin") != null) || (_dataManager.GetCache("ChangePasswordBeforeLoginSuccessful") != null))
        {
            _exceptionManager.Message = _errorMessageParam;
            _exceptionManager.Message += _errorMessageCatch;

            AgilityEntryNetUser user = new AgilityEntryNetUser();
            dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");
            dsCustomerDataSet dsCU = (dsCustomerDataSet)_dataManager.GetCache("dsCU");
            dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dataManager.GetCache("dsPV");
            _dataManager.SetCache("dsCustomerDataSet", dsCU);

            StoreCustBranchTable(dsCU);

            //TODO: create DataManager.UserConfigUpdate method...
            //Read default.config, [user name].config ...cross-check for new schema, update and write new [user name].config
            //...and cache all user's grid setting for all grids...

            // The following code is being commented out per Mike M.  
            // the interface to Datamanager.UserConfigUpdate does not play well with others 
            //myDataManager.UserConfigUpdate((string)myDataManager.GetCache("userNameAtLogin").ToString().ToLower(), Session.SessionID);

            string default_cust_key = dsPV.bPV_CoInfo.Rows[0]["default_cust_key"].ToString();
            int default_shipto_seq = (int)dsPV.bPV_CoInfo.Rows[0]["default_shipto_seq"];
            Session["LoggedIn"] = true;

            //MDM - Added cache values for PV/Visual Cafe; view_component_prices and view_incomplete_parent_price

            //Initialize to false on each login; even if in same session...
            Session["VC_view_component_prices"] = false;
            Session["VC_view_incomplete_parent_price"] = false;
            Session["UserAction_AllowRetailPricing"] = false;

            if (dsCU.ttCustomer.Rows.Count > 0)
            {
                // TODO: Put this in the back-end, clear the default customer when not found
                //       in the current current customer list per current branch
                //       When there is only one customer for the current branch use that as the default 
                //       customer.
                foreach (DataRow row in dsCU.ttCustomer.Rows)
                {
                    _dataManager.SetCache("CustomerRowForInformationPanel", row); // this is the current display

                    int ReturnCode;
                    string MessageText;

                    Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                    if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    bool isBranchShared = cust.IsBranchSharedForCustomer(_dataManager.GetCache("currentBranchID").ToString(), out ReturnCode, out MessageText);

                    if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    if (!isBranchShared)
                        _dataManager.RemoveCache("CustomerRowForInformationPanel");
                    else // get the ship-to of cached customer
                    {
                        _dataManager.SetCache("BranchShipTos", cust.BranchShipTos);

                        // get the default shipto, this will drive the tab
                        if (default_shipto_seq > 0)
                        {
                            // Verify if the currently selected ship-to is valid for the current customer
                            DataRow shiptoRow = cust.GetShiptoRow(default_shipto_seq);
                            if (shiptoRow != null)
                            {
                                _dataManager.SetCache("ShiptoRowForInformationPanel", shiptoRow);
                                shiptoRow = null;
                            }
                        }
                    }

                    cust.Dispose();
                    break;
                }

                // Check if this is a customer's customer log on, in some cases have only one customer and ship-to,
                // Customer tab has to be hidden. Save a session variable for MainTabControl usage
                if (dsCU.ttCustomer.Rows.Count == 1)
                {
                    // Verify if the customer is valid for the current branch, "CurrentBranchRow" must be set first
                    _dataManager.SetCache("CustomerRowForInformationPanel", dsCU.ttCustomer.Rows[0]);

                    if (user.GetContextValue("HasMoreCustomers") != "TRUE") // customer from other branches
                        _dataManager.SetCache("SingleCust", dsCU.ttCustomer.Rows[0]);

                    int ReturnCode;
                    string MessageText;

                    Dmsi.Agility.Data.AgilityCustomer cust = new Dmsi.Agility.Data.AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

                    if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    bool isBranchShared = cust.IsBranchSharedForCustomer(_dataManager.GetCache("currentBranchID").ToString(), out ReturnCode, out MessageText);

                    if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                    {
                        Session["LoggedIn"] = null;
                        Response.Redirect("~/Logon.aspx");
                    }

                    if (!isBranchShared)
                        _dataManager.RemoveCache("CustomerRowForInformationPanel");
                    else if (dsCU.Tables["ttShipto"].Rows.Count == 1)
                    {
                        // Check if the shipto is valid for the customer in the current branch
                        DataRow shiptoRow = cust.GetShiptoRow(((Dmsi.Agility.EntryNET.StrongTypesNS.dsCustomerDataSet.ttShiptoRow)dsCU.Tables["ttShipto"].Rows[0]).seq_num);

                        // This will allow the program to be reset to the default values
                        // once the use switch to a branch where the values are valid again
                        if (user.GetContextValue("HasMoreCustomers") != "TRUE") // customer from other branches
                            _dataManager.SetCache("SingleShipto", dsCU.ttShipto.Rows[0]);  // this is needed during branch change         
                    }

                    cust.Dispose();
                }
            }
        }
    }

    protected void btnResetPassword_Click(object sender, EventArgs e)
    {

        string UserName = txtForgotUserName.Text.ToString();
        Session["UserName"] = UserName;

        int ReturnCode;
        string MessageText;

        _securityManager.ResetPassword(UserName, ref _errorMessage_Boolean, ref _errorMessage_Text, out ReturnCode, out MessageText);

        switch (_errorMessage_Boolean)
        {
            case (true):



                break;

            case (false):
                this.lblError.Text = _errorMessage_Text == null ? "Unknown error has occured." : _errorMessage_Text.ToString();
                txtUserName.Text = "";
                txtForgotUserName.Focus();
                txtForgotUserName.TabIndex = 1;

                ModalPopupExtender1.Show();
                break;
        }

    }
    protected void btnCancelRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("./LogOn.aspx");
    }



    protected void btnCancelRequest_Click1(object sender, EventArgs e)
    {

        txtForgotUserName.Text = "";
        lblError.Text = "";
    }

    protected void btntopclose_Click(object sender, EventArgs e)
    {
        txtForgotUserName.Text = "";
        lblError.Text = "";
    }
    public void GetThemeDetails()
    {
        string _sUsername = ConfigurationManager.AppSettings["ProductSearchUserName"].ToString();
        string _sPassword = ConfigurationManager.AppSettings["ProductSearchPassword"].ToString();

        _dataManager = new DataManager();
        _exceptionManager = new ExceptionManager();
        _eventLog = new EventLog();
        _securityManager = new SecurityManager();
        _passwordRC = _securityManager.UserLogin("Product=PrtnrView", _sUsername.ToLower(), _sPassword, ref _errorMessageParam, ref _errorMessageCatch);
        AgilityEntryNetUser user = new AgilityEntryNetUser();
        dsSessionMgrDataSet dsSessionManager = (dsSessionMgrDataSet)_dataManager.GetCache("dsSessionManager");
        dsCustomerDataSet dsCU = (dsCustomerDataSet)_dataManager.GetCache("dsCU");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)_dataManager.GetCache("dsPV");
        dsPVParamDataSet dsPVParam = (dsPVParamDataSet)_dataManager.GetCache("dsPVParam");

        _dataManager.SetCache("dsCustomerDataSet", dsCU);
        DataTable dtProperties = new DataTable();
        dtProperties.Columns.Add("propertyname");
        dtProperties.Columns.Add("Color");
        DataRow[] drCustomtheme;
        drCustomtheme = dsPVParam.ttparam_pv.Select("property_name='" + "use_custom_theme" + "'");

        DataRow[] drThemeauthentication;
        drThemeauthentication = dsPV.bPV_Action.Select("token_code ='" + "disable_theme_administration" + "'");

        if (drCustomtheme.Length > 0)
        {
            Session["IsCustomTheme"] = drCustomtheme[0]["property_value"].ToString();
        }

        if (drThemeauthentication.Length > 0)
        {
            Session["disable_theme_authentication"] = "true";
        }
        else
        {
            Session["disable_theme_authentication"] = "false";
        }
    }
}