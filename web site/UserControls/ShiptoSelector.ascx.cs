using System;
using System.Data;
using System.Web.UI.WebControls;

using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;
using System.Web.UI;

public partial class UserControls_ShiptoSelector : System.Web.UI.UserControl
{
    public delegate void OKClickedDelegate();
    public event OKClickedDelegate OnOKClicked;
    
    public delegate void ResetClickedDelegate();
    public event ResetClickedDelegate OnResetClicked;

    public delegate void CancelClickedDelegate();
    public event CancelClickedDelegate OnCancelClicked;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Shipto_txtCustomerName"] == null)
        {
            DataManager dm = new DataManager();

            if ((DataRow)dm.GetCache("ShiptoRowForInformationPanel") != null)
            {
                DataRow row = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

                Session["Shipto_txtCustomerName"] = row["shipto_name"];
                Session["Shipto_txtAddress1"] = row["address_1"];
                Session["Shipto_txtAddress2"] = row["address_2"];
                Session["Shipto_txtAddress3"] = row["address_3"];
                Session["Shipto_txtCity"] = row["city"];
                Session["Shipto_txtState"] = row["state"];
                Session["Shipto_txtZip"] = row["zip"];
                Session["Shipto_txtCountry"] = row["country"];
                Session["Shipto_txtPhone"] = row["phone"];
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session["Shipto_txtCustomerName"] != null)
        {
            txtCustomerName.Text = Convert.ToString(Session["Shipto_txtCustomerName"]).Trim();
            txtAddress1.Text = Convert.ToString(Session["Shipto_txtAddress1"]).Trim();
            txtAddress2.Text = Convert.ToString(Session["Shipto_txtAddress2"]).Trim();
            txtAddress3.Text = Convert.ToString(Session["Shipto_txtAddress3"]).Trim();
            txtCity.Text = Convert.ToString(Session["Shipto_txtCity"]).Trim();
            txtState.Text = Convert.ToString(Session["Shipto_txtState"]).Trim();
            txtZip.Text = Convert.ToString(Session["Shipto_txtZip"]).Trim();
            txtCountry.Text = Convert.ToString(Session["Shipto_txtCountry"]).Trim();
            txtPhone.Text = Convert.ToString(Session["Shipto_txtPhone"]).Trim();
        }
        else
        {
            txtCustomerName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
        }

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "ComplementoryPopupHeight();", true);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        Session["Shipto_txtCustomerName"] = txtCustomerName.Text.Trim();
        Session["Shipto_txtAddress1"] = txtAddress1.Text.Trim();
        Session["Shipto_txtAddress2"] = txtAddress2.Text.Trim();
        Session["Shipto_txtAddress3"] = txtAddress3.Text.Trim();
        Session["Shipto_txtCity"] = txtCity.Text.Trim();
        Session["Shipto_txtState"] = txtState.Text.Trim();
        Session["Shipto_txtZip"] = txtZip.Text.Trim();
        Session["Shipto_txtCountry"] = txtCountry.Text.Trim();
        Session["Shipto_txtPhone"] = txtPhone.Text.Trim();

        if (sender != null && e != null)
        {
            /* Analytics Tracking */
            
            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type   = "event";
            aInput.Action = "Cart - Shipping Address Update - OK Button";
            aInput.Label  = "";
            aInput.Value  = "0";

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();
        
        }

            if (OnOKClicked != null)
            OnOKClicked(); // will be caught by the parent
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (OnCancelClicked != null)
            OnCancelClicked(); // will be caught by the parent
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtCustomerName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtZip.Text = "";
        txtCountry.Text = "";
        txtPhone.Text = "";

        Session["Shipto_txtCustomerName"] = null;
        Session["Shipto_txtAddress1"] = null;
        Session["Shipto_txtAddress2"] = null;
        Session["Shipto_txtAddress3"] = null;
        Session["Shipto_txtCity"] = null;
        Session["Shipto_txtState"] = null;
        Session["Shipto_txtZip"] = null;
        Session["Shipto_txtCountry"] = null;
        Session["Shipto_txtPhone"] = null;

        if (OnResetClicked != null)
            OnResetClicked(); // will be caught by the parent
    }
}
