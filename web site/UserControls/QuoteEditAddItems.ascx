﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuoteEditAddItems.ascx.cs" Inherits="UserControls_QuoteEditAddItems" %>
<%@ Register Src="~/UserControls/Inventory.ascx" TagName="Inventory" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/QuoteEdit.ascx" TagName="QuoteEdit" TagPrefix="uc2" %>
<style type="text/css">

</style>
<table cellpadding="0" cellspacing="5" width="100%">
    <tr>
        <td class="RightPane">
            <div style="padding-bottom: 0px" class="GridViewTitleDIV">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td class="SimplePanelHeaderNoBGwithBorder" align="left" style="padding-bottom: 10px;padding-top: 10px">
                            <asp:Label ID="lblQuoteHeader" runat="server" Text="Add Items to Quote "></asp:Label>
                        </td>
                        <td class="SimplePanelHeaderNoBGwithBorder" align="right" style="padding-bottom: 10px;padding-top: 10px">
                            <div id="SubmitButtons2" runat="server" visible="true">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td style="padding-right: 0px">
                                            <asp:Button ID="btnGoBack" runat="server" Text="Done Adding Items"  
                                                CssClass="btn btn-primary" TabIndex="1" OnClick="btnCancel_Click" 
                                                ToolTip="Return to Quote Edit" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc1:Inventory runat="server" ID="QuoteEditInventory" Mode="QuoteEditAddItems" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

