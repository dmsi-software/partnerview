﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Inventory.ascx.cs" Inherits="UserControls_Inventory" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.ListControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.NavigationControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxControl" %>
<%@ Register Src="ColumnSelector.ascx" TagName="ColumnSelector" TagPrefix="uc3" %>
<%@ Register Src="ImageView.ascx" TagName="ImageSelector" TagPrefix="uc4" %>
<style type="text/css">
    *:focus {
        outline: none;
    }

    a {
        color: rgb(0,102,204);
    }

        a:visited {
            color: rgb(0,102,204);
        }

    .LocalHeaderGridView {
        font-style: normal;
        font-family: Verdana;
        font-weight: normal;
    }

    .wrapstyle {
        white-space: normal;
    }

    .BorderedContainer {
    }

    .FieldLabelLocal {
        font-weight: bold;
        font-size: 8pt;
        text-transform: none;
        color: black;
        font-family: Helvatica,Verdana,Tahoma,Arial;
        margin-top: 6px;
    }

    .linkUnderline:visited, .linkUnderline:link, .linkUnderline:active {
        text-decoration: none !important;
        font-size: 12px;
        color: Black;
    }

    .treelink {
        color: Black;
        font-size: 12px;
    }

    .linkUnderline:hover {
        text-decoration: underline !important;
        font-size: 12px;
        color: Black;
        cursor: initial;
    }

    .treelinkHover:hover {
        text-decoration: underline !important;
        font-size: 12px;
        color: Black;
    }

    .listPaging {
        text-decoration: none !important;
        background-color: White;
        font-size: 12px;
    }

    .imgPadding {
        padding: 0px 15px 0px 20px;
        vertical-align: top;
        float: left;
    }

    .lblGrdpartno {
        padding: 0px 0px 0px 5px;
    }

    .pnlVertical {
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }

    .HorizontaltabStyles {
        font-size: 12px;
        border: solid 1px Transparent;
        padding: 6px 30px 6px 30px;
        float: left;
        color: #000FFF;
        border-right-style: solid;
        border-right-width: 2px;
        border-right-color: #B1A491;
    }

    .SortDropDown {
        padding-right: 4px;
    }

    .SortLabel {
        padding-left: 4px;
        border: 0px;
        outline: none !important;
    }

    .DivImagesalign {
        width: auto;
    }

    .DivNoImagesalign {
        width: 180px;
    }

    .FloatingImage {
        display: inline-block;
        vertical-align: top;
        margin: 8px;
        *display: inline;
        *zoom: 1;
        height: 100%;
    }

    .searchddlmargin {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .searchddlmargin2 {
        padding-top: 5px;
        padding-bottom: 5px;
        margin-left: 10px !important;
        height: 35px !important;
    }

    .marginleft10 {
        margin-left: 10px;
        text-decoration: underline !important;
    }

    .marginleft10Only {
        margin-left: 8px !important;
    }

    .marginTopZero {
        margin-top: 0px !important;
    }

    .margintop5 {
        margin-top: 5px !important;
    }

    .margintop10 {
        margin-top: 10px !important;
    }

    .extendedDesc {
        font-size: 12px;
        line-height: 1.3em;
        text-align: left !important;
    }

    .noWrap {
        white-space: nowrap !important;
        line-height: 1.3em !important;
    }

    .smalllabel {
        margin-left: 6px !important;
        margin-top: 12px !important;
    }

    .selectoverride {
        margin-top: 2px !important;
    }

    .floatright {
        float: right !important;
        font-size: 14px !important;
        margin-right: 128px !important;
        margin-top: 8px !important;
        color: black !important;
    }

    .SettingsLink:visited {
        color: #B1A491 !important;
    }

    .EcommDesc {
        text-align: left;
        margin-right: 20px;
        margin-top: 78px;
    }

    .EcommDesc ul {
        width: 100%;
        margin-left: 30px;
    }
</style>
<style type="text/css">
    .igiv_IGImageAreaContainer {
        height: 60px !important;
    }

    .igiv_IGImageHeaderContainer {
        height: 0px !important;
    }
</style>
<script language="javascript" type="text/javascript">
    function GetControlIds(obj) {
        PageMethods.GetControlid('');
    };

    function HideFilterpopupUser() {
        document.getElementById('TargetPagingDetails').style.visibility = 'visible'
        return false
    };
</script>
<asp:HiddenField ID="hdnScrollToTop" runat="server" />
<div runat="server" id="tblInventory" class="left-section pr-left-section" style="">
    <div runat="server" id="trSearch1" style="width: 20%; float: left;">
        <div id="Td1" runat="server">
            <div id="Div1" class="SimplePanelFixedWidthDIV" runat="server">
                <div id="Div2" runat="server" class="LightColor">
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="265px" VerticalStripWidth="2px" BorderWidth="0px" CssClass="Custom">
                        <ajaxToolkit:TabPanel runat="server" ID="tbSearch" Width="100%" TabIndex="0" HeaderText="Search" CssClass="Custom">
                            <ContentTemplate>
                                <asp:Panel runat="server" ID="pnlsearch" CssClass="SimplePanelBody2">
                                    <asp:LinkButton ID="lnkHome" runat="server" Text="All Products" OnClick="lnkHome_Click"
                                        Visible="False" />
                                    <div runat="server" id="divFloatingSearch1">
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 248px">
                                            <tr align="right">
                                                <td align="right" style="padding-top: 10px; padding-bottom: 10px; width: 100%; padding-left: 8px">
                                                    <asp:LinkButton ID="lnkClearAll" runat="server" Text="Clear Selections" OnClick="lnkClearAll_Click" CssClass="CollapsibleAdvSearchPanel margintop10" />
                                                </td>
                                                <td align="right" style="padding-top: 0px; padding-bottom: 10px; width: 100%; padding-left: 8px">
                                                    <asp:Button ID="btnSearch1" runat="server" Text="Apply" OnClick="btnSearch_Click" CssClass="activesbutton btnprodsearch marginleft10Only" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <ig:WebDataTree runat="server" ID="igTree"
                                        ForeColor="Black" CssClass="treelink" OnNodeClick="igTree_NodeClick" Width="266px">
                                        <AutoPostBackFlags NodeClick="On" />
                                        <NodeSettings CssClass="treelink" HoverCssClass="treelinkHover" />
                                    </ig:WebDataTree>
                                    <div runat="server" id="divFloatingSearch2">
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 248px">
                                            <tr align="right">
                                                <td align="right" style="padding-top: 0px; padding-bottom: 10px; width: 100%; padding-left: 8px">
                                                    <asp:LinkButton ID="lnkClearAll2" runat="server" Text="Clear Selections" OnClick="lnkClearAll_Click" CssClass="CollapsibleAdvSearchPanel margintop10" />
                                                </td>
                                                <td align="right" style="padding-top: 0px; padding-bottom: 10px; width: 100%; padding-left: 8px">
                                                    <asp:Button ID="btnSearch2" runat="server" Text="Apply" OnClick="btnSearch_Click" CssClass="activesbutton btnprodsearch marginleft10Only marginTopZero" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="tbAdvSearch" runat="server" BorderWidth="0px" TabIndex="1" 
                                              CssClass="SortLabel" HeaderText="Advanced Search">
                            <ContentTemplate>
                                <asp:Panel ID="InventorySearchFieldPanel" runat="server" CssClass="SimplePanelBodyAdvancedSearch"
                                    DefaultButton="btnFind">
                                    <div class="search-opt">
                                        <div class="advsearch">
                                            <asp:Label ID="Label6" runat="server" Text="Search by" CssClass="alladvsearchlabel"></asp:Label><br />
                                            <ig:WebTextEditor ID="txtKeywordSearchValue" runat="server" NullText=" Keyword"
                                                BorderStyle="Inset" EnableTheming="False" BorderColor="Gray" CssClass="select">
                                            </ig:WebTextEditor>
                                            <asp:HiddenField ID="hdnAddCart" Value="0" runat="server" />
                                            <asp:HiddenField ID="hdnAddCart_QEAI" Value="0" runat="server" />
                                            <ig:WebTextEditor ID="txtInventorySearchValue" runat="server" NullText=" Part number"
                                                BorderStyle="Inset" BorderColor="Gray" CssClass="select" />
                                            <AjaxControl:CascadingDropDown ID="CascadingDropDown1" runat="server" TargetControlID="ddlMajor"
                                                Category="Major" PromptText="All" LoadingText="[Loading Majors...]" ServicePath="~/ProductGroupService.asmx" 
                                                ServiceMethod="GetMajors" SkinID="ddlselect" UseContextKey="true" />
                                            <AjaxControl:CascadingDropDown ID="CascadingDropDown2" runat="server" TargetControlID="ddlMinor"
                                                Category="Minor" PromptText="All" LoadingText="[Loading Minors...]" ServicePath="~/ProductGroupService.asmx"
                                                ServiceMethod="GetMinorsForMajor" ParentControlID="ddlMajor" SkinID="ddlselect" UseContextKey="true"/>
                                            <asp:Label ID="Label13" runat="server" Text="Product group major" CssClass="alladvsearchlabel" />
                                            <asp:DropDownList ID="ddlMajor" runat="server" AppendDataBoundItems="True" CssClass="searchddl" />
                                            <asp:Label ID="Label14" runat="server" CssClass="alladvsearchlabel" Text="Product group minor" />
                                            <asp:DropDownList ID="ddlMinor" runat="server" AppendDataBoundItems="True" CssClass="searchddl" />
                                            <asp:CheckBox ID="cbxOnly" runat="server" Text="Available products only" />
                                            <br class="ClearMe" />
                                            <asp:CheckBox ID="chkQuickList" runat="server" Text="My quicklist items only" Visible="True" />
                                            <br class="ClearMe" />
                                            <asp:Button ID="btnFind" runat="server" Text="Search" OnClick="btnFind_Click" CssClass="activesbutton btnprodsearch" />
                                            <div runat="server" id="OriginalQlistDiv">
                                                <asp:Button ID="btnAdvViewQuickList" runat="server" Text="View Quicklist"
                                                    OnClick="btnViewQuickList_Click" CssClass="inactivesbutton btnprodsearch" />
                                            </div>
                                            <div runat="server" id="NewQlistDiv">
                                                <br />
                                                <br />
                                                <br />
                                                <asp:Label ID="Label12" runat="server" Text="Quicklists" CssClass="alladvsearchlabel"></asp:Label>
                                                <asp:DropDownList ID="ddlQuicklistType" runat="server" />
                                                <asp:Button ID="btnAdvViewQuickList2" runat="server" Text="View Quicklist"
                                                    OnClick="btnViewQuickList_Click" CssClass="inactivesbutton btnprodsearch" />
                                            </div>
                                            <div style="float: right; padding-top: 15px; width: 100%">
                                                <AjaxControl:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                    AutoCollapse="False" AutoExpand="false" CollapseControlID="pnlAdvancedSearch"
                                                    Collapsed="True" CollapsedImage="~/images/collapse_blue.jpg" CollapsedSize="0"
                                                    CollapsedText="Search for" ExpandControlID="pnlAdvancedSearch" ExpandDirection="Vertical"
                                                    ExpandedImage="~/images/expand_blue.jpg" ExpandedText="Search for" ImageControlID=""
                                                    ScrollContents="False" SuppressPostBack="true" TargetControlID="InventorySearchTipsDiv" />
                                                <asp:Panel ID="pnlAdvancedSearch" runat="server" Width="99%" CssClass="CollapsibleAdvSearchPanel">
                                                    <div class="CollapsibleHeaderDIV" style="vertical-align: middle; text-align: right; padding-bottom: 10px">
                                                        Search Tips
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="InventorySearchTipsDiv" runat="server">
                                                    <div id="divContent" runat="server" style="margin-top: 2px; margin-left: 0px; padding-left: 0px; min-height: 260px; width: 200px; line-height: 20px; text-align: left;" />
                                                    <p style="font-size: 6px; margin-top: 0px; margin-bottom: 0px" />
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <asp:Label ID="Label5" runat="server" CssClass="FieldLabel" Text="Sale type" Visible="False" />
                                        <asp:Label ID="lblSaleType" runat="server" Visible="False"></asp:Label>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                       </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </div>
            </div>
        </div>
    </div>
</div>
<div runat="server" id="Td2" class="right-section pr-right-section">
    <div class="container-sec">
        <div class="custom-grid abc inventorygrid" id="trSearch" runat="server">
            <div id="Div3" class="SimplePanelFullWidthDIV" runat="server">
                <div>
                    <asp:HyperLink runat="server" ID="hypNavigateExternalSite" CssClass="floatright" /> <%--Target="_blank"--%>
                </div>
                <div width="100%" id="tblbreadCrums"
                    runat="server">
                    <asp:Panel ID="pnlAddbreadCrumbs" runat="server" class="brd-crumb">
                        <ul>
                            <li style="margin-left: 7px">
                                <asp:LinkButton runat="server" ID="lnkBreadCrumb" Text="All Products" OnClick="lnkHome_Click"
                                    CssClass="linkUnderline"></asp:LinkButton>
                                <asp:Label runat="server" Visible="false" Text=">" ID="lblBreadCatSep" />
                            </li>
                            <li>
                                <asp:LinkButton runat="server" ID="lnkBreadCategory" Visible="false" OnClick="lnkCategory_Click"
                                    CssClass="linkUnderline"></asp:LinkButton>
                                <asp:Label runat="server" Visible="false" Text=">" ID="lblBreadGroupSep" />
                            </li>
                            <li>
                                <asp:LinkButton runat="server" ID="lnkBreadgroup" Visible="false" OnClick="lnkBreadgroup_Click"
                                    CssClass="linkUnderline" />
                            </li>
                            <li>
                                <asp:LinkButton runat="server" ID="lnkBreadCatValue" Visible="false" OnClick="lnkBreadCatVal_Click"
                                    CssClass="linkUnderline" />
                                <asp:Label ID="lblAdvSearchAllProd" runat="server" Visible="False" Text="All Products"
                                    Font-Size="12px" />
                            </li>
                        </ul>
                    </asp:Panel>
                </div>
            </div>
            <div class="SimplePanelFullWidthDIV" runat="server" id="dvImageBanners" style="padding: 0px">
                <div style="clear: both"></div>
                <asp:Panel ID="Panel10" runat="server">
                    <asp:Panel ID="plAddImages" runat="server" ScrollBars="None" CssClass="container-fluid pnlVertical" HorizontalAlign="Left" ViewStateMode="Enabled">
                    </asp:Panel>
                </asp:Panel>
            </div>
            <div class="SimplePanelFullWidthDIV" runat="server" id="dvProdListView" visible="false">
                <asp:Panel ID="Panel1" runat="server" CssClass="product-detail-view prodviewauto">
                    <div class="sorting-section" style="width: 97%">
                        <ul class="sort" style="float: left; width: 100%">
                            <li>
                                <div align="right" id="dvImgQuickList" runat="server">
                                </div>
                            </li>
                            <li>
                                <div style="white-space: nowrap; padding-top: 5px;">
                                    <asp:Label ID="Label1" runat="server" Text="Show" Font-Size="13px" CssClass="SortLabel" />
                                    <asp:DropDownList ID="ddlHeader" runat="server" Width="60px" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlHeader_SelectedIndexChanged">
                                        <asp:ListItem>5</asp:ListItem>
                                        <asp:ListItem>10</asp:ListItem>
                                        <asp:ListItem>15</asp:ListItem>
                                        <asp:ListItem>20</asp:ListItem>
                                        <asp:ListItem>25</asp:ListItem>
                                        <asp:ListItem>30</asp:ListItem>
                                        <asp:ListItem>40</asp:ListItem>
                                        <asp:ListItem>50</asp:ListItem>
                                        <asp:ListItem>75</asp:ListItem>
                                        <asp:ListItem>100</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="Label2" runat="server" Font-Size="13px" Text="Rows" CssClass="SortLabel" />
                                </div>
                            </li>
                            <li>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lblSort" runat="server" Text="Sort by" Font-Size="13px" />
                                    <asp:DropDownList ID="ddlProductSorting" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductSorting_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Selected="True">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Part No (A-Z) </asp:ListItem>
                                        <asp:ListItem Value="2">Part No (Z-A)</asp:ListItem>
                                        <asp:ListItem Value="3">Product Desc. (A-Z)</asp:ListItem>
                                        <asp:ListItem Value="4">Product Desc. (Z-A)</asp:ListItem>
                                        <asp:ListItem Value="5">Available Qty (High to Low) </asp:ListItem>
                                        <asp:ListItem Value="6">Available Qty (Low to High)</asp:ListItem>
                                        <asp:ListItem Value="7">Price (High to Low)  </asp:ListItem>
                                        <asp:ListItem Value="8">Price (Low to High)</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </li>
                            <li>
                                <div align="right" id="Div4" runat="server">
                                </div>
                            </li>
                            <li style="float: right;">
                                <div class="prodsearchsortbutton">
                                    <asp:Button ID="btnViewQuickList" runat="server" Text="View Quicklist"
                                        OnClick="btnViewQuickList_Click" CssClass="btn btn-default addtocart btn_prdtl" />
                                    <asp:Button Visible="false" ID="btnAddAll2" runat="server" Text="Add to Cart" OnClick="btnAddAll_Click"
                                        CssClass="btn btn-primary addtocart" />
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div style="width: 97%; float: left; text-align: left;">
                        <asp:Label ID="lblHeaderError" runat="server" Text="Your search returned too many records to be displayed. Consider refining your search criteria. For help click Search Tips."
                            Visible="False" CssClass="ErrorTextRed"></asp:Label>
                        <asp:Label ID="lblErrorMessage" runat="server" ForeColor="red" />
                    </div>
                    <div runat="server" id="dvProductRow" class="prodcut-row">
                        <asp:ListView ID="lvProducts" runat="server" ItemPlaceholderID="itemPlaceHolder"
                            OnItemDataBound="lvProducts_ItemDataBound" OnItemCommand="lvProducts_ItemCommand"
                            DataKeyNames="ITEM,thickness,WIDTH,LENGTH,Size,qty_uom,qty_uom_conv_ptr,qty_uom_conv_ptr_sysid">
                            <LayoutTemplate>
                                <table id="tblProducts" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr style="visibility: collapse; display: none;">
                                            <th style="width: 120px;">
                                                <asp:Label ID="lblProductImage" runat="server" Text="Product Image" Font-Bold="true"
                                                    ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <th style="width: 25px;">&nbsp;
                                            </th>
                                            <th style="width: 450px;">
                                                <asp:Label ID="lblProductDetails" runat="server" Text="Product Details" Font-Bold="true"
                                                    ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <th style="width: 25px;">&nbsp;
                                            </th>
                                            <th style="width: 250px;">
                                                <asp:Label ID="lblProductPriceDetails" runat="server" Text="Product Price Details"
                                                    Font-Bold="true" ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <div id="itemPlaceHolder" runat="server">
                                            </div>
                                        </tr>
                                    </thead>
                                    <tbody id="groupplaceholder" runat="server">
                                    </tbody>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr id="Tr1" class="DarkBottomBorder" style="height: 120px;" runat="server">
                                    <td align="center" style="width: 120px; height: 100px; text-align: center;">
                                        <asp:ImageButton ID="imgProduct" runat="server" ImageAlign="Middle" Height="100px"
                                            CommandName="Image" />
                                        <asp:HiddenField ID="hfID" runat="server" />
                                    </td>
                                    <td style="width: 25px;">&nbsp;
                                    </td>
                                    <td style="width: 725px; height: 120px; vertical-align: top;">
                                        <table>
                                            <tr id="trItemTemp1" style="height: 120px;" runat="server">
                                                <td style="width: 725px; height: 120px; vertical-align: top;">
                                                    <table style="vertical-align: top;">
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <h3 class="pd-Title">
                                                                    <asp:LinkButton ID="lbtProdDesc" runat="server" Text="Product Description" OnClick="ProdDetails_Click"
                                                                        CommandName="Product" Font-Size="Large"></asp:LinkButton>
                                                                    <asp:Label ID="lblproddesc" runat="server" Font-Size="Large" Visible="false" ForeColor="#000000" />
                                                                </h3>
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px; vertical-align: middle;">
                                                                <asp:Label ID="lblPrice" runat="server" Text="  "></asp:Label>
                                                                <asp:Label ID="lblPriceText" runat="server" Text="" DataFormatString="{0:#,##0.00}"
                                                                    CssClass="lblGrdpartno"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="trExtendedDescription">
                                                            <td colspan="2" style="width: 450px;">
                                                                <asp:Label runat="server" ID="lblExtDesc"
                                                                    Text="" CssClass="extendedDesc">
                                                                </asp:Label><asp:LinkButton CssClass="CollapsibleAdvSearchPanel noWrap" runat="server" ID="lnkExtDesc" Text="show more" CommandName="ExtDesc" Height="13px" />
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <asp:Label ID="lblAvailable" runat="server" Text="Available " Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblAvilableCount" runat="server" Text="" Font-Bold="true" CssClass="lblGrdpartno"></asp:Label>
                                                                <asp:Label ID="lblAvilableCountText" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblAvailUOM" runat="server" Text="" Font-Bold="true" />
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px;">
                                                                <div style="vertical-align: bottom; width: 250px;">
                                                                    <asp:Panel runat="server" ID="panel_Configure" Visible="False">
                                                                        <asp:LinkButton ID="linkbutton_Configure" runat="server" PostBackUrl='<%# "~/PreConfigurator.aspx?parent=pv&mode=create&item=" + System.Web.HttpUtility.UrlEncode((string)Eval("ITEM"))  + "&size=" + System.Web.HttpUtility.UrlEncode(((string)Eval("SIZE")).Replace("&", "and").Replace("#", "").Replace("+", "")) + "&description=" + System.Web.HttpUtility.UrlEncode(((string)Eval("DESCRIPTION")).Replace("&", "and").Replace("#", "").Replace("+", "")) + "&uom=" + System.Web.HttpUtility.HtmlEncode((string)Eval("stocking_uom")) %>'
                                                                            Text="Configure" OnClientClick="newWindowClick('configurator');" Visible="True" CssClass="SettingsLink productsconfigure" />
                                                                    </asp:Panel>
                                                                    <asp:Label ID="lblOrder" runat="server" Text="Qty " Font-Bold="true"></asp:Label>
                                                                    <asp:TextBox ID="txtOrder" runat="server" CssClass="qty-txt qtymargin" onkeydown="return (event.keyCode!=13);" MaxLength="14"></asp:TextBox>
                                                                    <asp:DropDownList onChange="javascript:ddlClick();" OnSelectedIndexChanged="ddlUOM_SelectedIndexChanged" DataMember='<%# Eval("ITEM") %>' Visible="true" ID="ddlUOM" runat="server" Width="80px" Height="30px" CssClass="searchddl qtymargin searchddlmargin" AutoPostBack="True" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <asp:Label ID="lblPartNumber" runat="server" Text="Part # "></asp:Label>
                                                                <asp:Label ID="lblPartNo" runat="server" Text="" CssClass="lblGrdpartno"></asp:Label>
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px; padding-bottom: 2px;">
                                                                <asp:Label ID="lblMinPack" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <asp:Label ID="lblProductGroup" runat="server" Text="Product Group: "></asp:Label>
                                                                <asp:Label ID="lblMajorMinor" runat="server" Text="Major / Minor " CssClass="lblGrdpartno"></asp:Label>
                                                                <asp:HiddenField ID="hdnStockinguom" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnminpackDetails" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnRetailprice" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnsize" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnItem" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnItemPtr" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnItemType" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnPrice" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnExtDesc" runat="server" Visible="false" />
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px; padding-bottom: 4px;">
                                                                <asp:LinkButton ID="lbtnQuicklist" runat="server" Text="+ Add to my quicklist" CommandName="QuickList" CssClass="pd-quick-btn"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                        <asp:ListView ID="lvNoImagesProductsList" runat="server" ItemPlaceholderID="itemPlaceHolder"
                            OnItemDataBound="lvNoImagesProductsList_ItemDataBound" OnItemCommand="lvNoImagesProductsList_ItemCommand"
                            DataKeyNames="ITEM,thickness,WIDTH,LENGTH,Size,qty_uom,qty_uom_conv_ptr,qty_uom_conv_ptr_sysid">
                            <LayoutTemplate>
                                <table id="tblProducts" cellpadding="0" cellspacing="0" border="0">
                                    <thead>
                                        <tr style="visibility: collapse; display: none;">
                                            <th style="width: 25px;">&nbsp;
                                            </th>
                                            <th style="width: 450px;">
                                                <asp:Label ID="lblProductDetails" runat="server" Text="Product Details" Font-Bold="true"
                                                    ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <th style="width: 25px;">&nbsp;
                                            </th>
                                            <th style="width: 250px;">
                                                <asp:Label ID="lblProductPriceDetails" runat="server" Text="Product Price Details"
                                                    Font-Bold="true" ForeColor="DarkBlue" Font-Underline="true" Font-Size="Large"></asp:Label>
                                            </th>
                                            <div id="itemPlaceHolder" runat="server">
                                            </div>
                                        </tr>
                                    </thead>
                                    <tbody id="groupplaceholder" runat="server">
                                    </tbody>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr id="Tr1" style="height: 120px;" runat="server">
                                    <td class="DarkBottomBorder" style="width: 25px; text-align: center">&nbsp;
                                    </td>
                                    <td class="DarkBottomBorder" style="width: 725px; height: 120px; vertical-align: top;">
                                        <table>
                                            <tr id="trItemTemp2" style="height: 120px;" runat="server">
                                                <td style="width: 725px; height: 120px; vertical-align: top;">
                                                    <table style="vertical-align: top;">
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <h3 class="pd-Title">
                                                                    <asp:LinkButton ID="lbtProdDesc" runat="server" Text="Product Description" OnClick="ProdDetails_Click"
                                                                        CommandName="Product"></asp:LinkButton>
                                                                    <asp:Label ID="lblproddesc" runat="server" Font-Size="Large" Visible="false" ForeColor="#000000" />
                                                                </h3>
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px; vertical-align: middle;">
                                                                <asp:Label ID="lblPrice" runat="server" Text="  "></asp:Label>
                                                                <asp:Label ID="lblPriceText" runat="server" Text="" DataFormatString="{0:#,##0.00}"
                                                                    CssClass="lblGrdpartno"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="trExtendedDescription">
                                                            <td colspan="2" style="width: 450px;">
                                                                <asp:Label runat="server" ID="lblExtDesc"
                                                                    Text="" CssClass="extendedDesc">
                                                                </asp:Label><asp:LinkButton CssClass="CollapsibleAdvSearchPanel noWrap" runat="server" ID="lnkExtDesc" Text="show more" CommandName="ExtDesc" Height="13px" />
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <asp:Label ID="lblAvailable" runat="server" Text="Available " Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblAvilableCount" runat="server" Text="" Font-Bold="true" CssClass="lblGrdpartno"></asp:Label>
                                                                <asp:Label ID="lblAvilableCountText" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <asp:Label ID="lblAvailUOM" runat="server" Text="" Font-Bold="true" />
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px;">
                                                                <div style="vertical-align: bottom; width: 250px">
                                                                    <asp:Panel runat="server" ID="panel_Configure" Visible="False">
                                                                        <asp:LinkButton ID="linkbutton_Configure" runat="server" PostBackUrl='<%# "~/PreConfigurator.aspx?parent=pv&mode=create&item=" + System.Web.HttpUtility.UrlEncode((string)Eval("ITEM"))  + "&size=" + System.Web.HttpUtility.UrlEncode(((string)Eval("SIZE")).Replace("&", "and").Replace("#", "").Replace("+", "")) + "&description=" + System.Web.HttpUtility.UrlEncode(((string)Eval("DESCRIPTION")).Replace("&", "and").Replace("#", "").Replace("+", "")) + "&uom=" + System.Web.HttpUtility.HtmlEncode((string)Eval("stocking_uom")) %>'
                                                                            Text="Configure" OnClientClick="newWindowClick('configurator');" Visible="True"
                                                                            CssClass="SettingsLink productsconfigure" />
                                                                    </asp:Panel>
                                                                    <asp:Label ID="lblOrder" runat="server" Text="Qty " Font-Bold="true"></asp:Label>
                                                                    <asp:TextBox ID="txtOrder" runat="server" CssClass="qty-txt qtymargin" onkeydown="return (event.keyCode!=13);" MaxLength="14"></asp:TextBox>
                                                                    <asp:DropDownList onChange="javascript:ddlClick();" OnSelectedIndexChanged="ddlUOM_SelectedIndexChanged" DataMember='<%# Eval("ITEM") %>' Visible="true" ID="ddlUOM2" runat="server" Width="80px" Height="30px" CssClass="searchddl qtymargin searchddlmargin" AutoPostBack="True" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <asp:Label ID="lblPartNumber" runat="server" Text="Part # "></asp:Label>
                                                                <asp:Label ID="lblPartNo" runat="server" Text="" CssClass="lblGrdpartno"></asp:Label>
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px; padding-top: 2px;">
                                                                <asp:Label ID="lblMinPack" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 30px; vertical-align: bottom;">
                                                            <td style="width: 450px;">
                                                                <asp:Label ID="lblProductGroup" runat="server" Text="Product Group: "></asp:Label>
                                                                <asp:Label ID="lblMajorMinor" runat="server" Text="Major / Minor " CssClass="lblGrdpartno"></asp:Label>
                                                                <asp:HiddenField ID="hdnStockinguom" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnminpackDetails" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnRetailprice" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnsize" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnItem" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnItemPtr" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnItemType" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnPrice" runat="server" Visible="false" />
                                                                <asp:HiddenField ID="hdnExtDesc" runat="server" Visible="false" />
                                                            </td>
                                                            <td style="width: 25px;">&nbsp;
                                                            </td>
                                                            <td style="width: 250px; padding-bottom: 4px;">
                                                                <asp:LinkButton ID="lbtnQuicklist" CssClass="pd-quick-btn" runat="server" Text="+ Add to my quicklist" CommandName="QuickList"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <table cellpadding="0" cellspacing="0" border="0" width="720px">
                        <tr>
                            <td style="width: 212px; padding-left: 0px;" align="left"></td>
                            <td align="left" style="width: 488px;"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:ListView runat="server" ID="lvPaging" GroupItemCount="1" OnItemDataBound="lvPaging_ItemDataBound"
                                    OnItemCommand="lvPaging_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnknumber" runat="server" CssClass="pd-page-link" Width="32px" />
                                    </ItemTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                    </table>
                    <div class="pagination-sec">
                        <div class="info">
                            <asp:Label ID="lblpageno" runat="server" Text="" Font-Size="13px" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div>
        </div>
    </div>
</div>
<div runat="server" id="trDetails" visible="false" class="productdetails container-sec form">
    <div class="custom-grid abc">
        <p>
            <asp:Label runat="server" ID="lblPGMajorMinor" Text="" />
            <asp:Button runat="server" ID="btnReturnToItem" Text="Back to Product List" OnClick="btnReturnToItem_Click" CssClass="pull-right btn btn-default" />
        </p>
        <div class="col-sm-12" id="trinnerdetails" runat="server">
            <div class="col-sm-3 trimgitemdetails" runat="server" id="tdimageItemDetail">
                <a runat="server" id="actionImageClick" onclick="ctl00_ImgLarge_ImageClick()">
                    <div class="zoomPad" style="height: 100px; text-align: center">
                        <asp:Image runat="server" ID="imageItemDetail" ImageAlign="Middle" />
                    </div>
                </a>
                <br clear="all" />
                <div runat="server" id="tablerowImageViewer" align="center" class="scrollingdiv" style="float: right; padding-bottom: 15px">
                    <ig:WebImageViewer ID="WebImageViewer1" runat="server"
                        Height="64px" EnableInitialFadeAnimation="False"
                        EnableDragScrolling="False" OnItemBound="WebImageViewer1_ItemBound"
                        WrapAround="False" CssClass="thumb-view">
                        <ScrollAnimations Type="Page" Page-EquationType="Linear" Page-Duration="0">
                        </ScrollAnimations>
                        <ClientEvents ImageClick="ctl00_WebImageViewer1_ImageClick" />
                    </ig:WebImageViewer>
                </div>
                <AjaxControl:ModalPopupExtender ID="mdlimageselector" runat="server" BackgroundCssClass="ModalBackground"
                    DropShadow="true" PopupControlID="pnlimageselectpopup"
                    TargetControlID="actionImageClick">
                </AjaxControl:ModalPopupExtender>
                <asp:Panel ID="pnlimageselectpopup" runat="server" CssClass="modal-dialog mdlpopup">
                    <asp:Panel ID="pnlimageselectdrag" runat="server" CssClass="modal-content">
                        <div class="modal-header" style="padding: 5px !important; height: 30px;">
                            <h4 class="modal-title">
                                <asp:Label ID="imgproddiscription" runat="server" />
                                <asp:Button runat="server" ID="btnclose" Text="X" CssClass="close" />
                            </h4>
                        </div>
                        <div class="modal-body" style="min-height: 500px; margin-left: -17px">
                            <div class="clearfix mdlpopup" style="text-align: center;">
                                <ul id="lightSlider" class="gallery list-unstyled cS-hidden mdlpopup">
                                    <li style="align-content: center; text-align: center; width: 100%">
                                        <asp:Image ID="imgLarge" runat="server" Height="400px" />
                                    </li>
                                    <li runat="server" id="tablerowPopupImageViewer" style="align-content: center; text-align: center; width: 100%">
                                        <ig:WebImageViewer ID="WebImageViewer2" runat="server" Width="540px" Height="63px" EnableInitialFadeAnimation="False" EnableDragScrolling="False" WrapAround="False"
                                            OnItemBound="WebImageViewer2_ItemBound">
                                            <ScrollAnimations Page-Duration="0" Page-EquationType="Linear" Type="Page">
                                            </ScrollAnimations>
                                            <ClientEvents ImageClick="ctl00_WebImageViewer2_ImageClick" />
                                        </ig:WebImageViewer>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </div>
            <div class="col-sm-9 dvproddesc" style="text-align: left !important">
                <h4>
                    <asp:Label runat="server" ID="lblSizePlusDescription" />
                </h4>
                <asp:Label runat="server" CssClass="extendedDesc" ID="lblDetailExtDesc" Visible="false" /><asp:LinkButton CssClass="CollapsibleAdvSearchPanel noWrap" runat="server" ID="lnkExtDesc" Text="show more" OnClick="lnkExtDesc_Click" /><asp:HiddenField runat="server" ID="hdnDetailExtDesc" Visible="false" />
                <hr />
                <div class="col-sm-12 product-desc">
                    <div class="col-sm-9 ">
                        <p>
                            <asp:Label runat="server" ID="lblDetailPartNo" />
                        </p>
                        <h3>
                            <asp:Label runat="server" ID="lblDetailPriceAndUom" />
                        </h3>
                        <p>
                            <asp:Label runat="server" ID="lblDetailAvailableQty" />
                        </p>
                        <div id="divExtendedDescription" runat="server" class="EcommDesc"></div>
                    </div>
                    <div class="col-sm-3" style="text-align: right;">
                        <div style="float: right; text-align: left;" class="addtoCartwrap">
                            <div runat="server" id="trDetailMessage" style="float: left; margin-top: -45px;">
                                <asp:Label runat="server" ID="lblDetailMessage" ForeColor="Red" Text="&nbsp;<br/>&nbsp;" />
                            </div>
                            <div runat="server" id="trDetailQty" style="float: left; width: 240px; margin-bottom: 8px">
                                <asp:Label ID="lbltrdetailqty" runat="server" Text="Qty &nbsp  " Font-Bold="true" CssClass="qty-wid margintop5" />
                                <ig:WebTextEditor ID="txtDetailItemQty" runat="server" CssClass="qty-prdtxt prdtxtdetail"
                                    FocusOnInitialization="True" HideEnterKey="True" MaxLength="14" />
                                <asp:DropDownList OnSelectedIndexChanged="ddlDetailUOM_SelectedIndexChanged" Visible="true" ID="ddlDetailUOM" runat="server" Width="80px" Height="36px" CssClass="searchddl qtymargin searchddlmargin2" AutoPostBack="True" />
                            </div>
                            <div runat="server" id="divDetailMinPack">
                                <asp:Label runat="server" ID="lblDetailMinPack" Style="line-height: 16px; float: left; margin-bottom: 10px" />
                            </div>
                            <div>
                                <asp:Button ID="btnDetailAddToCart" runat="server" Text="Add to Cart"
                                    OnClick="btnDetailAddToCart_Click" class="addtocart btn btn-primary" />
                            </div>
                            <br />
                            <asp:Button ID="btnAddToQList" runat="server" Text="+ Add to my quicklist" CssClass="prodaddqlist addtolit" OnClick="btnAddToQList_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="bs-example">
                <div>
                    <table style="padding-left: 12px; padding-right: 12px" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="MediumColor" style="width: 100%">
                            <td class="MediumColor" width="100%">
                                <div class="bs-example ig pd-tab-list">
                                    <ig:WebDataMenu ID="WebDataMenu1" runat="server" OnItemClick="WebDataMenu1_ItemClick"
                                        ActivateOnHover="False"
                                        ViewStateMode="Disabled" Width="100%" EnableScrolling="False">
                                        <GroupSettings Orientation="Horizontal" />
                                        <Items>
                                            <ig:DataMenuItem Text="Pricing">
                                            </ig:DataMenuItem>
                                            <ig:DataMenuItem Text="Available">
                                            </ig:DataMenuItem>
                                            <ig:DataMenuItem Text="On Order">
                                            </ig:DataMenuItem>
                                            <ig:DataMenuItem Text="Part Numbers">
                                            </ig:DataMenuItem>
                                            <ig:DataMenuItem Text="Documents">
                                            </ig:DataMenuItem>
                                        </Items>
                                    </ig:WebDataMenu>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div runat="server" id="pnlContentContainer" visible="True">
                    <div runat="server" id="tableDocuments" border="0" cellpadding="0" cellspacing="0"
                        width="100%" class="custom-grid">
                        <asp:GridView ID="gvItemDocuments" runat="server" AllowPaging="False" CellPadding="4"
                            ForeColor="#333333" GridLines="None" DataKeyNames="image_id" EmptyDataRowStyle-BorderStyle="None"
                            AutoGenerateColumns="False" AllowSorting="False" EmptyDataText="No documents found."
                            OnRowDataBound="gvItemDocuments_RowDataBound" EnableModelValidation="True"
                            HeaderStyle-Height="0px" ShowHeader="False" CssClass="grid-tabler partnoborder">
                            <RowStyle ForeColor="Black" Wrap="False" />
                            <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                            <EditRowStyle BackColor="#999999" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <Columns>
                                <asp:BoundField ConvertEmptyStringToNull="False"
                                    DataField="display_text" ReadOnly="True">
                                    <ItemStyle Wrap="False" Width="150px" />
                                    <ControlStyle Width="150px" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <table runat="server" id="tableXrefs" border="0" cellpadding="0" cellspacing="0"
                        width="100%">
                        <tr style="width: 100%">
                            <td colspan="2" align="left">
                                <asp:GridView ID="gvItemXrefs" runat="server" AllowPaging="True" CellPadding="4" EmptyDataRowStyle-BorderStyle="None"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler partnoborder" DataKeyNames="xref_num"
                                    PageSize="6" AutoGenerateColumns="False" AllowSorting="True" EmptyDataText="No part numbers found."
                                    OnSorting="gvItemXrefs_Sorting" OnPageIndexChanging="gvItemXrefs_PageIndexChanging"
                                    OnRowCreated="gvItemXrefs_RowCreated" OnRowDataBound="gvItemXrefs_RowDataBound"
                                    EnableModelValidation="True" OnRowCommand="gvItemXrefs_RowCommand" OnRowDeleting="gvItemXrefs_RowDeleting">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ShowDeleteButton="true" DeleteImageUrl="~/Images/delete-icon.png" />
                                        <asp:BoundField AccessibleHeaderText="Number" ConvertEmptyStringToNull="False" DataField="xref_num"
                                            HeaderText="Number" ReadOnly="True" SortExpression="xref_num">
                                            <ItemStyle Wrap="True" Width="150px" CssClass="wrapstyle" />
                                            <ControlStyle Width="150px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Dimension" ConvertEmptyStringToNull="False"
                                            DataField="dimension" HeaderText="Dimension" ReadOnly="True" SortExpression="dimension">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <ControlStyle Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Type" ConvertEmptyStringToNull="False" DataField="xref_purpose"
                                            HeaderText="Type" ReadOnly="True" SortExpression="xref_purpose" HtmlEncode="False">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Source" ConvertEmptyStringToNull="False" DataField="xref_name"
                                            HeaderText="Source" ReadOnly="True" SortExpression="xref_name" HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="150px" />
                                            <ControlStyle Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td align="right" style="width: 100%">
                                <asp:Label runat="server" ID="lblAddXrefMessage" ForeColor="Red" />
                            </td>
                            <td align="right" style="padding: 4px">
                                <asp:Button runat="server" Text="Add" ID="btnAddXref" CssClass="btn btn-default addnum" />
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td colspan="2">
                                <AjaxControl:ModalPopupExtender ID="ModalPopupExtender10" runat="server" BackgroundCssClass="ModalBackground"
                                    DropShadow="true" PopupControlID="pnlAddXref"
                                    TargetControlID="btnAddXref">
                                </AjaxControl:ModalPopupExtender>
                                <asp:Panel ID="pnlAddXref" runat="server" CssClass="modal-dialog" Style="display: none">
                                    <asp:Panel ID="pnlDragXref" runat="server" CssClass="modal-content">
                                        <div class="modal-header" style="height: 45px !important;">
                                            <h4 class="modal-title" id="myModalLabel" style="width: 95% !important;">Create part number</h4>
                                        </div>
                                        <div class="modal-body" style="min-height: 200px !important">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" class="LightColor" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" style="padding-left: 16px; padding-right: 16px; padding-top: 10px; padding-bottom: 5px">
                                                                    <asp:Label runat="server" ID="lblXrefPopUpSizeDesc" Font-Size="16px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="padding-left: 16px; padding-top: 5px; padding-bottom: 10px">
                                                                    <asp:Label runat="server" ID="lblXrefPopUpItem" Font-Size="14px" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 16px; padding-top: 6px" align="left">
                                                        <asp:Label ID="Label3" runat="server" Text="Part no" CssClass="partn" />
                                                    </td>

                                                    <td style="padding-top: 4px" align="center">
                                                        <ig:WebTextEditor ID="txtInputXref" runat="server" Width="314px" BorderStyle="Inset"
                                                            BorderColor="Gray" FocusOnInitialization="True" HideEnterKey="True" MaxLength="50" CssClass="partnoText">
                                                        </ig:WebTextEditor>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="modal-footer" style="width: 100%">
                                            <div style="float: right">
                                                <asp:Button runat="server" ID="Button1" Text="Cancel" CssClass="btn btnpopup-default" UseSubmitBehavior="False" />

                                                <asp:Button runat="server" ID="btnSaveXref" Text="Save" CssClass="btn btnpopup-primary" OnClick="btnSaveXref_Click" />
                                            </div>
                                        </div>
                                        <div style="clear: both"></div>
                                    </asp:Panel>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <div id="PricingParentDiv" runat="server" align="left" class="tab-content">
                        <div class="tab-pane fade active in">
                            <div class="col-sm-3 pull-left ddl" style="width: 320px;">
                                <asp:Label ID="Label4" runat="server" Text="Show" Font-Size="13px" Width="32px"></asp:Label>
                                <asp:DropDownList ID="ddlPricing" runat="server" Width="60px" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlPricing_SelectedIndexChanged">
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>40</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>75</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>250</asp:ListItem>
                                    <asp:ListItem>500</asp:ListItem>
                                    <asp:ListItem>All</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label9" runat="server" Text="Rows" Font-Size="13px" CssClass="inventorygridrow"></asp:Label>
                                <asp:LinkButton ID="lnkPriceCustomView" runat="server" CssClass="SettingsLink optionslink">Select Columns</asp:LinkButton>
                            </div>
                            <div class="pull-right ">
                            </div>
                            <div style="width: 100%; float: left;">
                                <asp:GridView ID="gvInventoryPricing" runat="server" AllowPaging="True" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler" DataKeyNames="ITEM"
                                    PageSize="4" AutoGenerateColumns="False" AllowSorting="True" EmptyDataText="No Inventory Items"
                                    OnSorting="gvInventoryPricing_Sorting" OnPageIndexChanging="gvInventoryPricing_PageIndexChanging"
                                    OnRowCreated="gvInventoryPricing_RowCreated" OnRowDataBound="gvInventoryPricing_RowDataBound">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:BoundField AccessibleHeaderText="Sale Type" ConvertEmptyStringToNull="False"
                                            DataField="saleType" HeaderText="Sale Type" ReadOnly="True" SortExpression="saleType"
                                            HtmlEncode="False">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Price/Disc Type" ConvertEmptyStringToNull="False"
                                            DataField="price_type" HeaderText="Price/Disc Type" ReadOnly="True" SortExpression="price_type">
                                            <ItemStyle Wrap="False" Width="170px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Dimension" ConvertEmptyStringToNull="False"
                                            DataField="dimensions" HeaderText="Dimension" ReadOnly="True" SortExpression="dimensions"
                                            HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="76px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Price" ConvertEmptyStringToNull="False" DataField="disp_price"
                                            HeaderText="Price" ReadOnly="True" SortExpression="disp_price" DataFormatString="{0:c}"
                                            HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="84px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Retail Price" ConvertEmptyStringToNull="False"
                                            DataField="retail_disp_price" HeaderText="Retail Price" ReadOnly="True" SortExpression="retail_disp_price"
                                            DataFormatString="{0:c}" HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="84px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Price UOM" ConvertEmptyStringToNull="False"
                                            DataField="disp_price_uom" HeaderText="Price UOM" ReadOnly="True" SortExpression="disp_price_uom">
                                            <ItemStyle Wrap="False" Width="82px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Disc Qty" ConvertEmptyStringToNull="False"
                                            DataField="discQty" HeaderText="Disc Qty" ReadOnly="True" SortExpression="discQty">
                                            <ItemStyle Wrap="False" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Discount" ConvertEmptyStringToNull="False"
                                            DataField="discount" HeaderText="Discount" ReadOnly="True" SortExpression="discount"
                                            DataFormatString="{0:c}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="70px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Disc UOM" ConvertEmptyStringToNull="False"
                                            DataField="disc_uom" HeaderText="Disc UOM" ReadOnly="True" SortExpression="disc_uom">
                                            <ItemStyle Wrap="False" Width="75px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Effective Dates" ConvertEmptyStringToNull="False"
                                            DataField="date_range" HeaderText="Effective Dates" ReadOnly="True" SortExpression="date_range">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <div id="PricingModalDiv" runat="server" style="top: 0px!important">
                                    <AjaxControl:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="ModalBackground"
                                        DropShadow="true" PopupControlID="pnlPriceCustomView"
                                        TargetControlID="lnkPriceCustomView">
                                    </AjaxControl:ModalPopupExtender>
                                    <asp:Panel ID="pnlPriceCustomView" runat="server" class="modal-dialog smlwidth">
                                        <div id="pnlDragPrice" runat="server" class="modal-content">
                                            <div class="modal-header" style="height: 45px !important;">
                                                <h4 class="modal-title" style="width: 95% !important;">Columns to View</h4>
                                            </div>
                                            <div class="modal-body">
                                                <uc3:ColumnSelector ID="ColumnSelectorPrice" runat="server" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="AvailableParentDiv" runat="server" align="left" class="tab-content">
                        <div class="tab-pane fade active in">
                            <div class="col-sm-6 pull-left ddl" style="padding-left: 0px">
                                <asp:Label ID="lblLevelAvailable" runat="server" Font-Size="13px">Available</asp:Label>
                                <asp:DropDownList
                                    ID="ddlLevelAvailable" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLevelAvailable_SelectedIndexChanged"
                                    class="tab-content select productsavilabledropdown">
                                    <asp:ListItem>Totals</asp:ListItem>
                                    <asp:ListItem>Totals By Branch</asp:ListItem>
                                    <asp:ListItem>Inventory Details</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label7" runat="server" Width="32px" Font-Size="13px" Text="Show"></asp:Label>
                                <asp:DropDownList ID="ddlAvailable" runat="server" AutoPostBack="true" Width="60px"
                                    OnSelectedIndexChanged="ddlAvailable_SelectedIndexChanged">
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>40</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>75</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>250</asp:ListItem>
                                    <asp:ListItem>500</asp:ListItem>
                                    <asp:ListItem>All</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label10" runat="server" Font-Size="13px" Width="35px" Text="Rows"></asp:Label>
                            </div>
                            <div class="pull-right ddl">
                            </div>
                            <div style="width: 100%; float: left">
                                <asp:GridView ID="gvInventoryAvailableTotal" runat="server" AllowPaging="True" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                    Width="250px" EmptyDataText="No Inventory Items" OnPageIndexChanging="gvInventoryAvailableTotal_PageIndexChanging"
                                    OnRowCreated="gvInventoryAvailableTotal_RowCreated" OnRowDataBound="gvInventoryAvailableTotal_RowDataBound"
                                    OnSorting="gvInventoryAvailableTotal_Sorting" OnLoad="gvInventoryAvailableTotal_Load">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:BoundField AccessibleHeaderText="Available" ConvertEmptyStringToNull="False"
                                            DataField="qty_text" HeaderText="Available" ReadOnly="True" SortExpression="qty_text"
                                            HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="75px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                            DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="150px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="uom"
                                            HeaderText="UOM" ReadOnly="True" SortExpression="uom">
                                            <ItemStyle Wrap="False" Width="100px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:GridView ID="gvInventoryAvailableBranch" runat="server" AllowPaging="True" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                    Width="400px" AllowSorting="True" EmptyDataText="No Inventory Items" Visible="False"
                                    OnPageIndexChanging="gvInventoryAvailableBranch_PageIndexChanging" OnRowCreated="gvInventoryAvailableBranch_RowCreated"
                                    OnRowDataBound="gvInventoryAvailableBranch_RowDataBound" OnSorting="gvInventoryAvailableBranch_Sorting"
                                    OnLoad="gvInventoryAvailableBranch_Load">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:BoundField AccessibleHeaderText="Available" ConvertEmptyStringToNull="False"
                                            DataField="qty_text" HeaderText="Available" ReadOnly="True" SortExpression="qty_text"
                                            HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="left" Width="75px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                            DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="150px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />

                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="uom"
                                            HeaderText="UOM" ReadOnly="True" SortExpression="uom">
                                            <ItemStyle Wrap="False" Width="100px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Branch" ConvertEmptyStringToNull="False" DataField="branch_id"
                                            HeaderText="Branch" ReadOnly="True" SortExpression="branch_id">
                                            <ItemStyle Wrap="False" Width="150px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:GridView ID="gvInventoryAvailableTally" runat="server" AllowPaging="True" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                    AllowSorting="True" EmptyDataText="No Inventory Items"
                                    Visible="False" OnPageIndexChanging="gvInventoryAvailableTally_PageIndexChanging"
                                    OnRowCreated="gvInventoryAvailableTally_RowCreated" OnRowDataBound="gvInventoryAvailableTally_RowDataBound"
                                    OnSorting="gvInventoryAvailableTally_Sorting" OnSelectedIndexChanged="gvInventoryAvailableTally_SelectedIndexChanged"
                                    DataKeyNames="tag" OnLoad="gvInventoryAvailableTally_Load">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView gridborderbottom" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" ButtonType="Link" ItemStyle-CssClass="selectlinkUnderline" />
                                        <asp:BoundField AccessibleHeaderText="Available" ConvertEmptyStringToNull="False"
                                            DataField="qty_text" HeaderText="Available" ReadOnly="True" SortExpression="qty_text"
                                            HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="75px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                            DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="stocking_uom"
                                            HeaderText="UOM" ReadOnly="True" SortExpression="stocking_uom">
                                            <ItemStyle Wrap="False" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Piece Count" ConvertEmptyStringToNull="False"
                                            DataField="char-pc-cnt" HeaderText="Piece Count" ReadOnly="True" SortExpression="char-pc-cnt">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Location" ConvertEmptyStringToNull="False"
                                            DataField="location" HeaderText="Location" ReadOnly="True" SortExpression="location"
                                            Visible="False">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Lot" ConvertEmptyStringToNull="False" DataField="lot"
                                            HeaderText="Lot" ReadOnly="True" SortExpression="lot" Visible="False">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Tag" ConvertEmptyStringToNull="False" DataField="tag"
                                            HeaderText="Tag" ReadOnly="True" SortExpression="tag">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Branch" ConvertEmptyStringToNull="False" DataField="branch_id"
                                            HeaderText="Branch" ReadOnly="True" SortExpression="branch_id">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <div style="margin-left: 20px; margin-top: 10px" align="left">
                                    <asp:GridView ID="gvInventoryAvailableTallyDetail" runat="server" CellPadding="4"
                                        ForeColor="#333333" GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                        AllowSorting="True" EmptyDataText="No Inventory Items" Visible="False" OnRowCreated="gvInventoryAvailableTallyDetail_RowCreated"
                                        OnRowDataBound="gvInventoryAvailableTallyDetail_RowDataBound" OnSorting="gvInventoryAvailableTallyDetail_Sorting"
                                        OnPageIndexChanging="gvInventoryAvailableTallyDetail_PageIndexChanging" OnLoad="gvInventoryAvailableTallyDetail_Load">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                        <RowStyle ForeColor="Black" Wrap="False" />
                                        <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                        <EditRowStyle BackColor="#999999" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle CssClass="PagerGridView " HorizontalAlign="Left" Height="10px" />
                                        <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                            Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                            ForeColor="White" />
                                        <Columns>
                                            <asp:BoundField AccessibleHeaderText="Available" ConvertEmptyStringToNull="False"
                                                DataField="qty_text" HeaderText="Available" ReadOnly="True" SortExpression="qty_text"
                                                HtmlEncode="False">
                                                <ItemStyle Wrap="False" HorizontalAlign="Left" Width="75px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                                DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                                <ItemStyle Wrap="False" HorizontalAlign="Right" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Right" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="stocking_uom"
                                                HeaderText="UOM" ReadOnly="True" SortExpression="stocking_uom">
                                                <ItemStyle Wrap="False" Width="50px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Thickness" ConvertEmptyStringToNull="False"
                                                DataField="thickness" HeaderText="Thickness" ReadOnly="True" SortExpression="thickness">
                                                <ItemStyle Wrap="False" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Width" ConvertEmptyStringToNull="False" DataField="WIDTH"
                                                HeaderText="Width" ReadOnly="True" SortExpression="WIDTH">
                                                <ItemStyle Wrap="False" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Length" ConvertEmptyStringToNull="False" DataField="LENGTH"
                                                HeaderText="Length" ReadOnly="True" SortExpression="LENGTH">
                                                <ItemStyle Wrap="False" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Piece Count" ConvertEmptyStringToNull="False" HeaderStyle-CssClass="gridborderleft" ItemStyle-CssClass="gridborderleft"
                                                DataField="char-pc-cnt" HeaderText="Piece Count" ReadOnly="True" SortExpression="char-pc-cnt">
                                                <ItemStyle Wrap="False" Width="90px" CssClass="gridborderleft gridborderbottom" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="gridborderleft" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="OnOrderParentDiv" runat="server" align="left" class="tab-content">
                        <div class="tab-pane fade active in">
                            <div class="col-sm-6 pull-left ddl" style="padding-left: 0px">
                                <asp:Label ID="lblLevelOnOrder" runat="server" Font-Size="13px">On Order</asp:Label>
                                <asp:DropDownList ID="ddlLevelOnOrder" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLevelOnOrder_SelectedIndexChanged"
                                    Width="150px" CssClass="tab-content select productsavilabledropdown">
                                    <asp:ListItem>Totals</asp:ListItem>
                                    <asp:ListItem>Totals By Branch</asp:ListItem>
                                    <asp:ListItem>Transaction Details</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label8" runat="server" Text="Show" Font-Size="13px" Width="32px"></asp:Label>
                                <asp:DropDownList ID="ddlOnOrder" runat="server" AutoPostBack="true" Width="60px"
                                    OnSelectedIndexChanged="ddlOnOrder_SelectedIndexChanged">
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>40</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>75</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>250</asp:ListItem>
                                    <asp:ListItem>500</asp:ListItem>
                                    <asp:ListItem>All</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label11" runat="server" Text="Rows" Font-Size="13px" Width="37px"></asp:Label>
                            </div>
                            <div class="pull-right ddl">
                            </div>
                            <div style="width: 100%; float: left;">
                                <asp:GridView ID="gvOnOrderTotal" runat="server" AllowPaging="True" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                    Width="250px" EmptyDataText="No Inventory Items" OnPageIndexChanging="gvInventoryOnOrderTotal_PageIndexChanging"
                                    OnRowCreated="gvInventoryOnOrderTotal_RowCreated" OnRowDataBound="gvInventoryOnOrderTotal_RowDataBound"
                                    OnSorting="gvInventoryOnOrderTotal_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                            DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                            <ItemStyle Wrap="True" HorizontalAlign="Right" Width="150px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" Width="150px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="uom"
                                            HeaderText="UOM" ReadOnly="True" SortExpression="uom">
                                            <ItemStyle Wrap="False" Width="100px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="100px" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:GridView ID="gvOnOrderBranch" runat="server" AllowPaging="True" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                    AllowSorting="True" EmptyDataText="No Inventory Items" Visible="False" OnPageIndexChanging="gvOnOrderBranch_PageIndexChanging"
                                    OnRowCreated="gvOnOrderBranch_RowCreated" OnRowDataBound="gvOnOrderBranch_RowDataBound"
                                    OnSorting="gvOnOrderBranch_Sorting">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                            DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="150px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" Width="150px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="uom"
                                            HeaderText="UOM" ReadOnly="True" SortExpression="uom">
                                            <ItemStyle Wrap="False" Width="100px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Branch" ConvertEmptyStringToNull="False" DataField="branch_id"
                                            HeaderText="Branch" ReadOnly="True" SortExpression="branch_id">
                                            <ItemStyle Wrap="False" Width="150px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="150px" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:GridView ID="gvOnOrderTran" runat="server" AllowPaging="True" CellPadding="4"
                                    ForeColor="#333333" GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                    AllowSorting="True" EmptyDataText="No Inventory Items"
                                    Visible="False" OnPageIndexChanging="gvOnOrderTran_PageIndexChanging" OnRowCreated="gvOnOrderTran_RowCreated"
                                    OnRowDataBound="gvOnOrderTran_RowDataBound" OnSorting="gvOnOrderTran_Sorting"
                                    OnSelectedIndexChanged="gvOnOrderTran_SelectedIndexChanged" DataKeyNames="PARENT_key">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                    <RowStyle ForeColor="Black" Wrap="False" />
                                    <AlternatingRowStyle ForeColor="#284775" BackColor="White" Wrap="False" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle CssClass="PagerGridView gridborderbottom" HorizontalAlign="Left" Height="10px" />
                                    <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                        ForeColor="White" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" ButtonType="Link" ItemStyle-CssClass="selectlinkUnderline" />
                                        <asp:BoundField AccessibleHeaderText="Parent Key" ConvertEmptyStringToNull="False"
                                            DataField="PARENT_key" HeaderText="Parent Key" ReadOnly="True" SortExpression="PARENT_key"
                                            HtmlEncode="False" Visible="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                            DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                            <ItemStyle Wrap="False" HorizontalAlign="Right" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="False" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="stocking_uom"
                                            HeaderText="UOM" ReadOnly="True" SortExpression="stocking_uom">
                                            <ItemStyle Wrap="False" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="50px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Tran ID" ConvertEmptyStringToNull="False" DataField="tran_id"
                                            HeaderText="Tran ID" ReadOnly="True" SortExpression="tran_id">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Tran Type" ConvertEmptyStringToNull="False"
                                            DataField="tran_type" HeaderText="Tran Type" ReadOnly="True" SortExpression="tran_type">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Exp. Ship Date" ConvertEmptyStringToNull="False"
                                            DataField="expect_date" HeaderText="Exp. Ship Date" ReadOnly="True" SortExpression="expect_date"
                                            DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Exp. Receipt Date" ConvertEmptyStringToNull="False"
                                            DataField="due_date" HeaderText="Exp. Receipt Date" ReadOnly="True" SortExpression="due_date"
                                            DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Status" ConvertEmptyStringToNull="False" DataField="po_status"
                                            HeaderText="Status" ReadOnly="True" SortExpression="po_status">
                                            <ItemStyle Wrap="False" Width="60px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="60px" />
                                        </asp:BoundField>
                                        <asp:BoundField AccessibleHeaderText="Branch" ConvertEmptyStringToNull="False" DataField="branch_id"
                                            HeaderText="Branch" ReadOnly="True" SortExpression="branch_id">
                                            <ItemStyle Wrap="False" Width="90px" />
                                            <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate></ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <div style="margin-left: 20px; margin-top: 15px" align="left">
                                    <asp:GridView ID="gvOnOrderTranDetail" runat="server" CellPadding="4" ForeColor="#333333"
                                        GridLines="None" CssClass="grid-tabler" PageSize="4" AutoGenerateColumns="False"
                                        AllowSorting="True" EmptyDataText="No Inventory Items" Visible="False" OnRowCreated="gvOnOrderTranDetail_RowCreated"
                                        OnRowDataBound="gvOnOrderTranDetail_RowDataBound" OnSorting="gvOnOrderTranDetail_Sorting"
                                        OnPageIndexChanging="gvOnOrderTranDetail_PageIndexChanging">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <FooterStyle Font-Bold="False" CssClass="accordionLink" Height="10px" />
                                        <HeaderStyle Font-Bold="False" Height="10px" CssClass="HeaderGridView" Font-Italic="False"
                                            Font-Overline="False" Font-Strikeout="False" HorizontalAlign="Left" Wrap="False"
                                            ForeColor="White" />
                                        <Columns>
                                            <asp:BoundField AccessibleHeaderText="Quantity" ConvertEmptyStringToNull="False"
                                                DataField="qty" HeaderText="Quantity" ReadOnly="True" SortExpression="qty" HtmlEncode="False">
                                                <ItemStyle Wrap="False" HorizontalAlign="Right" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Right" Wrap="False" Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="UOM" ConvertEmptyStringToNull="False" DataField="stocking_uom"
                                                HeaderText="UOM" ReadOnly="True" SortExpression="stocking_uom">
                                                <ItemStyle Wrap="False" Width="50px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="50px" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Thickness" ConvertEmptyStringToNull="False"
                                                DataField="thickness" HeaderText="Thickness" ReadOnly="True" SortExpression="thickness">
                                                <ItemStyle Wrap="False" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Width" ConvertEmptyStringToNull="False" DataField="WIDTH"
                                                HeaderText="Width" ReadOnly="True" SortExpression="WIDTH">
                                                <ItemStyle Wrap="False" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField AccessibleHeaderText="Length" ConvertEmptyStringToNull="False" DataField="LENGTH"
                                                HeaderText="Length" ReadOnly="True" SortExpression="LENGTH">
                                                <ItemStyle Wrap="False" Width="90px" />
                                                <HeaderStyle HorizontalAlign="Left" Wrap="False" Width="90px" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate></ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-right btnbtm">
            <asp:Button runat="server" ID="btnReturnToItem2" Text="Back to Product List" OnClick="btnReturnToItem_Click" CssClass="pull-right btn btn-default" />
        </div>
    </div>
</div>
<asp:HiddenField runat="server" ID="hdnBannerId" />
