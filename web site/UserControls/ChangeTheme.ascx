﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeTheme.ascx.cs" Inherits="UserControls_ChangeTheme" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">
    function Themepicker() {


        $(".pick-a-color").pickAColor({
            showSpectrum: true,
            showSavedColors: true,
            saveColorsPerElement: true,
            fadeMenuToggle: true,
            showAdvanced: true,
            showBasicColors: true,
            showHexInput: true,
            allowBlank: true,
            inlineDropdown: true
        });


    }
</script>

<div class=" col-lg-12 opt-margin-dev">

    <div class="theme-row" style="margin-left: 15px; margin-right: 15px; width: 96%;">

        <h5>Header Bar Background Color</h5>
        <div class="theme-row-spilt">

            <div class="theme-row-spilt-left">
               

               
                    <input type="text" onChange="headerstyle(this.value)" name="border-color" class="pick-a-color form-control" id="txtHeaderBar" clientidmode="Static" value="#42748d" runat="server" tabindex="12" />
               


            </div>

            <div class="theme-row-spilt-right">
                <div id="headbrbg" runat="server" style="background-color: #222; width: 155px; border-radius: 7px; padding: 5px; text-align: center;">&nbsp; </div>
            </div>

        </div>
        <h5>Header Bar Text Color</h5>

        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
               

                
                    <input type="text" onChange="headercolor(this.value)" name="border-color" class="pick-a-color form-control" id="txtuserinformation" clientidmode="Static" value="#92cddc" runat="server" tabindex="14" />
                
               



            </div>

            <div class="theme-row-spilt-right">
                <div id="dvuserinformation" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span style="color: #f87744;">&nbsp; </span>
                </div>
            </div>

        </div>

        <h5>Active/Link Text Color</h5>

        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
                    <input type="text"  onChange="linktext(this.value)" name="border-color" class="pick-a-color form-control" id="txtActiveLink" clientidmode="Static" value="#92cddc" runat="server" tabindex="16" />
            </div>
            <div class="theme-row-spilt-right">
                <div id="actlink" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span style="color: #c8b90f;">&nbsp;</span>
                </div>
            </div>

        </div>
        <h5>Primary Button Color</h5>

        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
                    <input type="text" name="border-color" onChange="primarybackground(this.value)" class="pick-a-color form-control" id="txtbtnStyle" clientidmode="Static" value="#42748d" runat="server" tabindex="18" />
            </div>

            <div class="theme-row-spilt-right">


                <div id="highlightcolorch" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span>&nbsp;</span>
                </div>
            </div>

        </div>

        <h5>Primary Button Text Color</h5>
        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
                

                  
                    <input type="text" name="border-color" onChange="primarycolor(this.value)" class="pick-a-color form-control" id="txtactivebuttext" clientidmode="Static" value="#ffffff" runat="server" tabindex="20" />
               
               
            </div>

            <div class="theme-row-spilt-right">
                <div id="dvprimarytext" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span style="color: #f87744;">&nbsp;</span>
                </div>
            </div>

        </div>

        <h5>Secondary Button Color</h5>


        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
                
               
                    <input type="text" onChange="secondarybackground(this.value)" name="border-color" class="pick-a-color form-control" id="txtInactivebutton" clientidmode="Static" value="#dbd9d6" runat="server" tabindex="22" />
                
            </div>

            <div class="theme-row-spilt-right">

                <div id="highlightcolorch2" runat="server" class="theme-btn-de-act" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span>&nbsp;</span>
                </div>
            </div>

        </div>


        <h5>Secondary Button Text Color</h5>
        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">

                

                
                    <input type="text" onChange="secondarytext(this.value)" name="border-color" class="pick-a-color form-control" id="txtinactivebuttext" clientidmode="Static" value="#565759" runat="server" tabindex="24" />
               


            </div>

            <div class="theme-row-spilt-right">
                <div id="dvInActbuttext" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span style="color: #f87744;">&nbsp;</span>
                </div>
            </div>

        </div>
        <h5>Active Tab  and Related Underline Bar Color </h5>

        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
               

               
                    <input type="text" onChange="activetab(this.value)" name="border-color" class="pick-a-color form-control" id="txtActivetab" clientidmode="Static" value="#42748d" runat="server" tabindex="26" />
               





            </div>

            <div class="theme-row-spilt-right">

                <div id="dvacttab" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span style="color: #f87744;">&nbsp;</span>
                </div>
            </div>

        </div>

        <h5>Active Tab Text Color</h5>
        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">

             


              
                    <input type="text" onChange="activetabtext(this.value)" name="border-color" class="pick-a-color form-control" id="txtactivetabtext" clientidmode="Static" value="#ffffff" runat="server" tabindex="28" />
               



            </div>

            <div class="theme-row-spilt-right">
                <div id="dvActtabtext" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">
                    <span style="color: #f87744;">&nbsp;</span>
                </div>
            </div>

        </div>

        <h5>Grid Header Color</h5>
        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
                

                
                    <input type="text" onChange="gridheaderback(this.value)" name="border-color" class="pick-a-color form-control" id="txtGrids" clientidmode="Static" value="#42748d" runat="server" tabindex="28" />
               



            </div>

            <div class="theme-row-spilt-right">
                <div id="dvgridbg" runat="server" style="color: #FFF; background: #42748d; border-radius: 7px; width: 155px; padding: 5px; text-align: center;">&nbsp;</div>
            </div>

        </div>

        <h5>Grid Header Text Color</h5>
        <div class="theme-row-spilt">
            <div class="theme-row-spilt-left">
                

                
                    <input type="text" onChange="gridheadertext(this.value)" name="border-color" class="pick-a-color form-control" id="txtGridtextcolor" clientidmode="Static" value="#ffffff" runat="server" tabindex="30" />
               
            </div>

            <div class="theme-row-spilt-right">
                <div id="dvGridtext" runat="server" style="width: 155px; border-radius: 7px; padding: 5px; text-align: center;">

                    <span style="color: #f87744;">&nbsp;</span>
                </div>
            </div>

        </div>








    </div>
    <span class="color-preview current-color"></span>


    <div style="float: right; padding-bottom: 20px; padding-right: 24px;">

         <asp:Button ID="btndefaulttheme" runat="server" Text="Default Theme" class="btn btn-default" TabIndex="35" />
        <asp:Button ID="UpdateTheme" runat="server" Text="Change Theme" class="btn btn-primary" TabIndex="34" />


       

        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalBackground"
            DropShadow="true" PopupControlID="pnlchangetheme"
            TargetControlID="UpdateTheme">
        </cc1:ModalPopupExtender>
        <cc1:ModalPopupExtender ID="mdefaulttheme" runat="server" BackgroundCssClass="ModalBackground"
            DropShadow="true" PopupControlID="pnlUpdatetheme"
            TargetControlID="btndefaulttheme">
        </cc1:ModalPopupExtender>


        <asp:Panel ID="pnlchangetheme" runat="server">
            <div id="dvchangethemeinner" runat="server" class="modal-dialog smlwidth">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Change Theme
                        </h4>
                    </div>
                    <div class="modal-body" style="min-height: 110px !important;">
                        <p style="font-size: 13px;">Do you want to change to or update the custom theme?</p>
                    </div>
                    <div class="modal-footer" style="width: 100%">
                        <div style="float: right">
                            <asp:Button runat="server" CssClass="btn btnpopup-default" Text="No" ></asp:Button>

                            <asp:Button CssClass="btn btnpopup-primary" runat="server" ID="btnchangethemeSave" Text="Yes" OnClick="btnSave_Click"></asp:Button>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlUpdatetheme" runat="server">
            <div id="dvinnerupdtheme" runat="server" class="modal-dialog smlwidth">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Default Theme
                        </h4>
                    </div>
                    <div class="modal-body" style="min-height: 110px !important;">
                        <p style="font-size: 13px;">Do you want change to default theme?</p>
                    </div>
                    <div class="modal-footer" style="width: 100%">
                        <div style="float: right">
                             <asp:Button runat="server" CssClass="btn btnpopup-primary" Text="No"></asp:Button>
                            <asp:Button CssClass="btn btnpopup-default" runat="server" ID="btndefaulthemechange" Text="Yes" OnClick="btndefaulttheme_Click"></asp:Button>
                           
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </asp:Panel>
    </div>

</div>

