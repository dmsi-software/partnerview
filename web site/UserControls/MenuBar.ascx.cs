using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_MenuBar : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DrawMenuBar();
    }

    public void RedrawMenuBar()
    {
        this.Controls.Clear();
        DrawMenuBar();
    }

    private void DrawMenuBar()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");
        bool isHome = Session["IsHome"] == null ? false : (bool)Session["IsHome"];

        string helpPage = ConfigurationManager.AppSettings["HelpFile"];

        Label body = new Label();
        body.Text = "<div id=\"MenuBar\" class=\"MenuBarDIV\" style=\"width: 100%\">";
        body.Text += "<table cellpadding=0 cellspacing=2><tr>";
        body.Text += "<td style=\"white-space: nowrap\"><a style=\"text-decoration: none;\" href=\"./LogOn.aspx\"><div class=\"MenuBarLink\" onmouseover=\"this.className='MenuBarLinkHover'\" onmouseout=\"this.className='MenuBarLink'\">&nbsp;&nbsp;Log Out&nbsp;&nbsp;</div></a></td>";
        body.Text += "<td style=\"white-space: nowrap\"><a style=\"text-decoration: none;\" href=\"./ChangePassword.aspx\"><div class=\"MenuBarLink\" onmouseover=\"this.className='MenuBarLinkHover'\" onmouseout=\"this.className='MenuBarLink'\">&nbsp;&nbsp;Change Password&nbsp;&nbsp;</div></a></td>";
        body.Text += "<td><a style=\"text-decoration: none;\" href=\"" + helpPage + "\" target=\"_blank\"><div class=\"MenuBarLink\" onmouseover=\"this.className='MenuBarLinkHover'\" onmouseout=\"this.className='MenuBarLink'\">&nbsp;&nbsp;Help&nbsp;&nbsp;</div></a></td>";

        if (user != null && user.GetContextValue("currentUseDebug") != null && user.GetContextValue("currentUseDebug").ToLower().Equals("yes"))
            body.Text += "<td style=\"white-space: nowrap\"><a style=\"text-decoration: none;\" href=\"./LogViewer.aspx\" target=\"_blank\"><div class=\"MenuBarLink\" onmouseover=\"this.className='MenuBarLinkHover'\" onmouseout=\"this.className='MenuBarLink'\">&nbsp;&nbsp;Log Viewer&nbsp;&nbsp;</div></a></td>";

        if (isHome &&
            dsPV != null &&
            dsPV.bPV_Action != null &&
            dsPV.bPV_Action.Rows.Count > 0 &&
            user.GetContextValue("UserType").ToLower() == "internal")
        {
            bool readOnly = false;

            foreach (DataRow row in dsPV.bPV_Action.Rows)
            {
                if (row["token_code"].ToString() == "read_only_content_msgs")
                {
                    readOnly = true;
                    break;
                }
            }

            if (!readOnly)
                body.Text += "<td style=\"white-space: nowrap\"><a style=\"text-decoration: none;\" href=\"./UpdateMessages.aspx\"><div class=\"MenuBarLink\" onmouseover=\"this.className='MenuBarLinkHover'\" onmouseout=\"this.className='MenuBarLink'\">&nbsp;&nbsp;Messages&nbsp;&nbsp;</div></a></td>";

        }

        body.Text += "</tr></table></div>";

        this.Controls.Add(body);
    }
}
