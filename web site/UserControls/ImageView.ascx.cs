﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserControls_ImageViewl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.WebImageViewer1.StyleSetName = System.Configuration.ConfigurationManager.AppSettings["Theme"];

        string tempImageURL = "";

        if (Request.QueryString["image"].ToString().ToLower().StartsWith("http://images.dmsi.com") || Request.QueryString["image"].ToString().ToLower().StartsWith("https://images.dmsi.com"))
        {
            tempImageURL = Request.QueryString["image"].ToString();
            tempImageURL = tempImageURL.Replace("__large", "");
            tempImageURL = tempImageURL.Replace("__small", "");
            tempImageURL = tempImageURL.Replace(".jpg", "__large.jpg");
            tempImageURL = tempImageURL.Replace(".JPG", "__large.jpg");
        }
        else
            tempImageURL = Request.QueryString["image"].ToString();

        this.imgLarge.ImageUrl = tempImageURL;
        Parent.Page.Title = "Part No: " + Request.QueryString["item"].ToString();

        FetchItemImages(Request.QueryString["item"].ToString());
    }


    protected void WebImageViewer1_ItemBound(object sender, Infragistics.Web.UI.ListControls.ImageItemEventArgs e)
    {
        
    }

    public void FetchItemImages(string item)
    {
        //there may not be any images, so hide first.
        tablerowImageViewer.Visible = false;

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Inventory inv = new Dmsi.Agility.Data.Inventory(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        inv.FetchItemImagesOrDocuments("Image", item, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        } 

        if (inv.ReferencedItemImageDataSet.ttItemImageDoc.Rows.Count > 0)
        {
            DataView dv = new DataView();
            dv.Table = inv.ReferencedItemImageDataSet.ttItemImageDoc;
            dv.RowFilter = "(image_file like 'http*') OR (image_file like 'https*')";

            if (dv.Count > 0)
            {
                tablerowImageViewer.Visible = true;
                WebImageViewer1.DataSource = dv;
                WebImageViewer1.ImageItemBinding.ImageUrlField = "image_file";
            }

            if (dv.Count == 1 && (bool)dv[0]["PRIMARY"] == true)
                tablerowImageViewer.Visible = false;
        }
    }
}