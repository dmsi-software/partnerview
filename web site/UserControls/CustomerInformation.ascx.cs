using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Dmsi.Agility.Data;

public partial class UserControls_CustomerInformation : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // This should pick up the initial value of the customer 
        if (!this.IsPostBack)
            this.Refresh();
    }

    public void Refresh()
    {
        int ReturnCode;
        string MessageText;

        AgilityCustomer cust = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Shipto shipto = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        this.lblCustomerName.Text = cust.CustomerName;
        this.lblCustomerCode.Text = cust.CustomerCode;

        if (shipto.ReferencedRow != null)
        {
            this.lblShiptoName.Text = shipto.ShiptoName;
            this.lblShiptoSequence.Text = shipto.ShiptoSequence.ToString();

            if (shipto.Nonsalable)
                this.lblShiptoSequence.Text += " (Nonsalable)"; 

            this.lblAddress1.Text = shipto.Address1;
            this.lblAddress2.Text = shipto.Address2;
            this.lblAddress3.Text = shipto.Address3;
            this.lblCityStateZip.Text = shipto.City + ", " + shipto.State + " " + shipto.Zip;
        }
        else
        {
            if (cust.ReferencedRow != null)
                this.lblShiptoSequence.Text = "None";
            else
                this.lblShiptoSequence.Text = "";

            this.lblShiptoName.Text = "";
            this.lblAddress1.Text = cust.Address1;
            this.lblAddress2.Text = cust.Address2;
            this.lblAddress3.Text = cust.Address3;
            this.lblCityStateZip.Text = cust.City + ", " + cust.State + " " + cust.Zip;
        }

        if (this.lblCityStateZip.Text.Trim().Equals(","))
            this.lblCityStateZip.Text = "";

        cust.Dispose();
        shipto.Dispose();
    }
}
