﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuoteRelease.ascx.cs"
    Inherits="UserControls_QuoteRelease" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.GridControls" TagPrefix="ig" %>
<%@ Register Src="CustomerInformation.ascx" TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/AddInfoQuoteSelector.ascx" TagName="AddInfoQuoteSelector" TagPrefix="uc8" %>
<%@ Register Src="~/UserControls/ShiptoQuoteSelector.ascx" TagName="ShiptoQuoteSelector" TagPrefix="uc10" %>
<style type="text/css">
    .gridHeader {
        white-space: nowrap;
    }

    .style3 {
        width: 210px;
    }

    .style5 {
        width: 110px;
    }

    .Field {
        margin-left: 0px;
    }

    .TopMargin1 {
        margin-top: 1px;
    }

    .TopMargin10 {
        margin-top: 10px !important;
    }

    .ErrorTextRedWithTopMargin {
        font-family: Helvatica,Verdana,Tahoma,Arial;
        font-size: 8pt;
        color: Red;
        margin-top: 3px;
    }

    .ErrorTextRedWithTopMarginLarge {
        font-family: Helvatica,Verdana,Tahoma,Arial;
        font-size: 9pt;
        color: Red;
        margin-top: 3px;
        text-align: left;
    }

    .style7 {
        height: 23px;
        width: 160px;
    }

    .style8 {
        width: 165px;
    }

    .auto-style1 {
        width: 210px;
        height: 23px;
    }

    .auto-style2 {
        width: 154px;
        height: 23px;
    }

    .auto-style3 {
        height: 23px;
    }
</style>
<div class="container-sec form submit" id="dvquoterelease" runat="server">
    <div class="custom-grid">
        <h2 style="padding-bottom: 15px">
            <asp:Label ID="lblSalesOrderHeader" runat="server" Text="Review and Submit Quote ">
            </asp:Label><asp:Label ID="txtQuoteID" runat="server" TabIndex="1" />
            <div id="SubmitButtons2" runat="server" visible="true" class="col-sm-2 pull-right" style="width: 38%">
                <asp:Button ID="btnSubmitOrder2" runat="server" Text="Submit"
                    TabIndex="12" UseSubmitBehavior="False" OnClick="btnSubmitOrder_Click" CssClass="btn btn-primary Quoterelease_Submit" OnClientClick="QuoteReleaseHideCancelAndSubmitButtons()" />
                    &nbsp;&nbsp;
                <asp:Button ID="btnEditCart2" runat="server" Text="Cancel" CssClass="btn btn-default" TabIndex="13" OnClick="btnCancel_Click" />
            </div>
        </h2>
        <div id="OrderConfirmation" runat="server" cssclass="ModalBackground" style="visibility: hidden; width: 100%; background-color: gray; float: left; position: absolute; top: 0; left: 0; opacity: 0.98; filter: alpha(opacity = 98); z-index: 1050;">
            <div class="BG_popUp">
            </div>
            <asp:Panel Height="500px" ID="pnlinnerorder" runat="server" Style="left: 0px; top: 0px; width: 100%; background: grey; opacity: 0.99; filter: alpha(opacity = 99); position: absolute; top: 0; left: 0; z-index: 1050;"
                class="pnlPopUp">
                <div class="modal-dialog" id="pnlQuoteHeaderDragHeader" runat="server">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p>
                                <asp:Label ID="lblLine1" runat="server" CssClass="Field" />
                                <asp:Label ID="lblLine2" runat="server" CssClass="Field" />
                                <asp:Label ID="lblError" runat="server" CssClass="ErrorTextRed" />
                            </p>
                        </div>
                        <div class="modal-footer" style="width: 100%">
                            <asp:Button Width="68px" ID="btnClose" runat="server" Text="OK" OnClick="btnClose_Click" CssClass="btn btnpopup-primary quotereleasealignright" />
                        </div>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div id="SubmitTop" runat="server" visible="true" style="padding-top: 20px;">
            <div>
                <table>
                    <tr>
                        <td valign="top" align="left" style="padding-left: 9px; white-space: nowrap">
                            <asp:Label ID="lblRequiredFieldMessage" Text="* Required field" runat="server" CssClass="ErrorTextRedWithTopMarginLarge" Width="240px" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left" style="white-space: nowrap">
                            <asp:Label ID="lblRequestedDateMessage" runat="server" CssClass="ErrorTextRedWithTopMarginLarge"
                                Text="** Requested delivery date subject<br/>&nbsp;&nbsp;&nbsp;&nbsp; to change upon submission."
                                Visible="False" Width="240px"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6" style="padding-top: 20px;">
                <ul class="default-list">
                    <li>
                        <asp:Label ID="Label16" runat="server" Text="Requested delivery date &nbsp;"
                            CssClass="Editquotelabel wrapEditLabel"></asp:Label>
                        <asp:TextBox ID="txtRequestedDate" runat="server" TabIndex="14" CssClass="TopMargin1"
                            OnTextChanged="txtRequestedDate_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <asp:DropDownList ID="ddlRequestedDate" runat="server"
                            TabIndex="14" CssClass="TopMargin1" Visible="false" OnSelectedIndexChanged="ddlRequestedDate_SelectedIndexChanged"
                            AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:Label ID="lblRequestdDateAsterisk" runat="server" CssClass="ErrorTextRed" Text="**"
                            Visible="False"></asp:Label>
                        <asp:Label ID="lblRequestedDateError" runat="server" CssClass="ErrorTextRed" Text="Invalid"
                            Visible="False"></asp:Label>
                    </li>
                    <li class="clear">
                        <asp:Label ID="Label19" runat="server" CssClass="Editquotelabel" Text="Submitted by&nbsp;"></asp:Label>
                        <asp:TextBox ID="txtOrderedBy" runat="server" TabIndex="15"
                            MaxLength="12"></asp:TextBox>
                        <asp:Label ID="lblOrderedByError" runat="server" CssClass="ErrorTextRed" Text="*"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="Label22" runat="server" CssClass="Editquotelabel" Text="PO ID &nbsp;"></asp:Label>
                        <asp:TextBox ID="txtCustomerPO" runat="server" TabIndex="16"
                            MaxLength="16"></asp:TextBox>
                        <asp:Label ID="lblPOError" runat="server" CssClass="ErrorTextRed" Text="*"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="Label27" runat="server" CssClass="Editquotelabel" Text="Job  &nbsp;"></asp:Label>
                        <asp:TextBox ID="txtJob" runat="server" TabIndex="17"
                            MaxLength="15"></asp:TextBox>
                        <asp:Label ID="lblJobError" runat="server" CssClass="ErrorTextRed" Text="*"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="Label11" runat="server" CssClass="Editquotelabel" Text="E-mail &nbsp;" />
                        <asp:TextBox ID="txtEmail" runat="server" TabIndex="18"></asp:TextBox>
                        <asp:Label ID="lblEmailError" runat="server" CssClass="ErrorTextRed" Text="*"
                            Visible="True" />
                    </li>
                    <li>
                        <asp:Label ID="Label25" runat="server" CssClass="Editquotelabel" Text="Secondary E-mail &nbsp;" />
                        <asp:TextBox ID="txtCcEmail" runat="server" TabIndex="19" />
                        <asp:Label ID="lblCcEmailError" runat="server" CssClass="ErrorTextRed" Text="Invalid"
                            Visible="False"></asp:Label>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6" style="padding-top: 20px;">
                <ul class="default-list2">
                    <li>
                        <asp:Label ID="Label18" runat="server" CssClass="Editquotelabel" Text="Order type &nbsp;" />
                        <asp:TextBox ID="txtOrderType" runat="server" TabIndex="21"
                            Enabled="false" />
                    </li>
                    <li>
                        <asp:Label ID="Label3" runat="server" CssClass="Editquotelabel" Text="Reference &nbsp" />
                        <asp:TextBox ID="txtReference" runat="server" TabIndex="22"
                            MaxLength="20" />
                        <asp:Label ID="lblReferenceError" runat="server" CssClass="ErrorTextRed" Text="*" />
                    </li>
                    <li>
                        <asp:Label ID="Label24" runat="server" CssClass="Editquotelabel" Text="Ship via &nbsp;" />

                        <asp:DropDownList ID="ddlShipVia" runat="server" TabIndex="23"
                            AutoPostBack="True" />
                        <asp:Label ID="lblShipViaError" runat="server" CssClass="ErrorTextRed" Text="*"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="lblShiptoQuote" runat="server" CssClass="Editquotelabel" Text="Shipping address &nbsp;"></asp:Label>
                        <asp:LinkButton runat="server" ID="lnkShiptoQuote" CssClass="SettingsLink" TabIndex="24">Update Address...</asp:LinkButton>
                        <div id="ShiptoQuoteModalDiv" runat="server">
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderShiptoQuote" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlShiptoQuoteCustomView"
                                TargetControlID="lnkShiptoQuote">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlShiptoQuoteCustomView" runat="server" CssClass="modal-dialog">
                                <div id="Div2" runat="server" class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="lblshiptoquote">Shipping Address</h4>
                                    </div>
                                    <div class="modal-body">
                                        <uc10:ShiptoQuoteSelector ID="ShiptoQuoteSelector1" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </li>
                    <li>
                        <asp:CheckBox ID="cbxShipComplete" runat="server" TabIndex="25" Text="Ship complete" CssClass="editcheckbox1 editcheckrelease TopMargin10" />
                    </li>
                    <li style="padding-top: 10px;">
                        <asp:Label Height="19px" ID="Label28" runat="server" CssClass="Editquotelabel" Text="Fax  &nbsp;" />

                        <asp:TextBox ID="txtFax" runat="server" TabIndex="26" />
                        <asp:Label ID="lblFaxError" runat="server" CssClass="ErrorTextRed" Text="Invalid"
                            Visible="False" />
                    </li>
                    <li>
                        <asp:Label ID="lblCustInfo" runat="server" CssClass="Editquotelabel" Text="Retail customer &nbsp;"></asp:Label>
                        <asp:LinkButton runat="server" ID="lnkAddInfo" CssClass="SettingsLink" TabIndex="27">Add Information...</asp:LinkButton>
                        <div id="AddInfoModalDiv" runat="server">
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddInfo" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlAddInfoCustomView"
                                TargetControlID="lnkAddInfo">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlAddInfoCustomView" runat="server" CssClass="modal-dialog" Width="620px">
                                <div id="pnlDragMarkup" runat="server" class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="lbleditretail">Retail Customer Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <uc8:AddInfoQuoteSelector ID="AddInfoQuoteSelector" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </li>
                </ul>
            </div>
            <div style="float: left; width: 98%; margin-left: 15px">
                <ul class="default-list">
                    <li>
                        <asp:Label ID="Label14" runat="server" CssClass="Editquotelabel" Style="float: left" Text="Message &nbsp;" />
                        <asp:TextBox ID="txtMessage" runat="server" TabIndex="20" Columns="153" Rows="3"
                            MaxLength="800" TextMode="MultiLine" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
        <div id="MessageDiv" runat="server" class="container-sec form">
            <asp:Button runat="server" ID="btnRelease" Text="Edit Qty" Visible="false" OnClick="btnRelease_Click"
                class="btn btn-default dall releasebuttonalign" TabIndex="28" />
            <asp:Label runat="server" ID="lblGridMessage" ForeColor="red" Text="&nbsp;" CssClass="quoterelease_errmsg"></asp:Label>
            <asp:Panel ID="QuoteDetailGridDiv" runat="server">
                <div style="margin-right: 16px; padding-right: 16px" class="custom-grid">
                    <ig:WebDataGrid ID="wdgQuoteDetails" runat="server" Width="100%" AutoGenerateColumns="False"
                        DataKeyFields="sequence" EnableDataViewState="True" StyleSetName="Office2007Silver">
                        <Columns>
                            <ig:BoundDataField DataFieldName="sequence" DataType="System.Int32" Key="sequence"
                                Width="45px">
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="qty_to_release" Key="readonly_qty_to_release" Width="60px">
                                <Header Text="Qty" />
                            </ig:BoundDataField>
                            <ig:TemplateDataField Hidden="true" Key="qty_to_release" Width="70px">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtQtyToRelease" Text='<%# DataBinder.Eval(((Infragistics.Web.UI.TemplateContainer)Container).DataItem, "qty_to_release")%>'
                                        Width="57px" Height="30px" CssClass="wrapstyledecimal" ViewStateMode="Enabled"></asp:TextBox>
                                </ItemTemplate>
                                <Header Text="Qty" />
                            </ig:TemplateDataField>
                            <ig:BoundDataField DataFieldName="uom" Key="uom" Width="50px">
                                <Header Text="UOM" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="ITEM" Key="ITEM" Width="75px">
                                <Header Text="Item" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="SIZE" Key="SIZE" Width="85px">
                                <Header Text="Size" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="quote_desc" Key="quote_desc" Width="150px">
                                <Header Text="Description" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="wo_phrase" Key="wo_phrase" CssClass="igwophase" Header-CssClass="igwophase">
                                <Header Text="WO Phrase/Quoted Tally" CssClass="igwophase" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="location_reference" Key="location_reference" Width="135px">
                                <Header Text="Location Reference" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="net_price" Key="price" DataFormatString="{0:C}" DataType="System.Decimal" Width="86px">
                                <Header Text="Price*" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="price_uom_code" Key="price_uom_code" Width="50px">
                                <Header Text="UOM" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="extended_price" Key="extended_price" Width="96px"
                                DataFormatString="{0:C}" DataType="System.Decimal" Header-CssClass="gridHeader">
                                <Header Text="Ext. Price*" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="retail_price" Key="retail_price" DataFormatString="{0:C}" Width="100px"
                                DataType="System.Decimal" Header-CssClass="gridHeader">
                                <Header Text="Retail Price" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="retail_price_uom_code" Key="retail_price_uom_code" Header-CssClass="gridHeader" Width="100px">
                                <Header Text="Retail UOM" />
                            </ig:BoundDataField>
                            <ig:BoundDataField DataFieldName="retail_extended_price" Key="retail_extended_price" Width="114px"
                                DataFormatString="{0:C}" DataType="System.Decimal" Header-CssClass="gridHeader">
                                <Header Text="Retail Ext. Price" />
                            </ig:BoundDataField>
                        </Columns>
                        <EditorProviders>
                            <ig:NumericEditorProvider ID="wdgQuoteDetails_NumericEditorProvider1" EditorControl-MaxDecimalPlaces="4"
                                EditorControl-BackColor="#FFFF99" EditorControl-MinDecimalPlaces="0" EditorControl-NullValue="0"
                                EditorControl-Nullable="False">
                                <EditorControl MaxDecimalPlaces="4" BackColor="#FFFF99" MinDecimalPlaces="0" NullValue="0"
                                    Nullable="False" MinValue="0">
                                </EditorControl>
                            </ig:NumericEditorProvider>
                        </EditorProviders>
                        <Behaviors>
                            <ig:ColumnResizing>
                            </ig:ColumnResizing>
                            <ig:EditingCore>
                                <Behaviors>
                                    <ig:CellEditing>
                                        <EditModeActions EnableF2="true" EnableOnActive="true" MouseClick="Single" />
                                        <ColumnSettings>
                                            <ig:EditingColumnSetting ColumnKey="readonly_qty_to_release" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="sequence" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="ITEM" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="SIZE" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="wo_phrase" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="location_reference" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="quote_desc" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="qty_ordered" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="qty_released" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="qty_to_release" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="uom" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="price" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="retail_price" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="price_uom_code" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="retail_price_uom_code" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="extended_price" ReadOnly="True" />
                                            <ig:EditingColumnSetting ColumnKey="retail_extended_price" ReadOnly="True" />
                                        </ColumnSettings>
                                    </ig:CellEditing>
                                </Behaviors>
                            </ig:EditingCore>
                        </Behaviors>
                    </ig:WebDataGrid>
                </div>
                <table cellpadding="0" cellspacing="5" width="100%">
                    <tr>
                        <td class="RightPane" align="left" valign="top">
                            <div class="GridViewTitleDIV">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="left" style="vertical-align: top; text-align: left; padding-right: 3px"></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label runat="server" ID="lblPriceDisclaimer" CssClass="Field" Text="* Prices are estimates and may change upon final submission." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblQuantityError" runat="server" CssClass="ErrorTextRed" Text="One or more quantities do not conform with the required multiple."
                                                Visible="False"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblQuantityErrorTable" runat="server" Visible="False"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="CartTotalDiv" runat="server" visible="false">
                                                <table cellpadding="2" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left" class="GeneralTableHeading" colspan="2" nowrap="nowrap">Shopping Cart Total</td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="height: 21px; text-align: right">
                                                            <asp:Label ID="Label8" runat="server" CssClass="FieldLabel" Text="Total:"></asp:Label>
                                                        </td>
                                                        <td nowrap style="width: 150px; text-align: right">
                                                            <asp:Label ID="lblLineTotal" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div id="EmptyCartDiv" runat="server" style="text-align: left">
            <div class="pull-right btnbtm" id="SubmitButtons" runat="server" visible="true">
                <asp:Button ID="btnSubmitOrder" runat="server" Text="Submit"
                    CssClass="btn btn-primary Quoterelease_Submit" TabIndex="29" UseSubmitBehavior="False" OnClick="btnSubmitOrder_Click" OnClientClick="QuoteReleaseHideCancelAndSubmitButtons()" />
                <asp:Button ID="btnEditCart" runat="server" Text="Cancel"
                    CssClass="btn btn-default" TabIndex="28" UseSubmitBehavior="False" OnClick="btnCancel_Click" />
            </div>
        </div>
    </div>
    <cc1:MaskedEditExtender ID="meeFaxNumber" runat="server" Mask="(999) 999-9999" MaskType="Number"
        TargetControlID="txtFax" ClearMaskOnLostFocus="False" AutoComplete="False">
    </cc1:MaskedEditExtender>
    <cc1:CalendarExtender ID="calRequestedDate" runat="server" PopupButtonID="calRequestedDate"
        Format="MM/dd/yyyy" TargetControlID="txtRequestedDate" CssClass="cal_Theme1">
    </cc1:CalendarExtender>
</div>
