﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuoteEdit.ascx.cs" Inherits="UserControls_QuoteEdit" %>
<%@ Register Assembly="Infragistics.Web" Namespace="Infragistics.Web.UI.GridControls" TagPrefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/AddInfoQuoteEditSelector.ascx" TagName="AddInfoQuoteEditSelector" TagPrefix="uc8" %>
<%@ Register Src="~/UserControls/MultiplierSelector.ascx" TagName="MultiplierSelector"  TagPrefix="uc9" %>
<%@ Register Src="~/UserControls/Inventory.ascx" TagName="Inventory"  TagPrefix="uc10" %>
<%@ Register Src="~/UserControls/ShiptoQuoteEditSelector.ascx" TagName="ShiptoQuoteEditSelector" TagPrefix="uc11" %>
<style type="text/css">
    .wrapItem {
        white-space: normal;
    }

    .gridHeader {
        white-space: nowrap;
    }

    .style3 {
        width: 210px;
    }

    .style4 {
        width: 69px;
    }

    .style5 {
        width: 90px;
    }

    .Field {
        margin-left: 0px;
    }

    .TopMargin1 {
        margin-top: 1px;
    }

    .TopMargin10 {
        margin-top: 10px !important;
    }

    .ErrorTextRedWithTopMargin {
        font-family: Helvatica,Verdana,Tahoma,Arial;
        font-size: 8pt;
        color: Red;
        margin-top: 3px;
    }

    .BtnTextPlusMarginBottom {
        margin-bottom: 3px;
        margin-left: 8px;
    }

    .LinkButtonPlusMarginTop {
        margin-left: 8px;
        margin-top: 8px;
    }

    .SettingsLinkMarginTop {
        text-decoration: none;
        cursor: pointer;
        color: Blue;
        margin-left: 8px;
        margin-right: 6px;
        margin-top: 6px;
    }

    .style7 {
        height: 23px;
        width: 152px;
    }

    .style8 {
        width: 152px;
    }
</style>
<div class="container-sec form">
    <div class="custom-grid">
        <h2 style="padding-bottom: 20px">
            <asp:Label ID="lblQuoteHeader" runat="server" Text="Edit Quote"></asp:Label>
            <asp:Button ID="btnGoBack" runat="server" Text="Return to Quotes"
                CssClass="pull-right btn btn-default btnquoterelease returntoQuote" TabIndex="12" OnClick="btnCancel_Click"
                ToolTip="Cancel unsaved changes; return to Quotes" />
        </h2>
        <div>
            <div class="col-sm-6 submit" style="padding-top: 20px;">
                <ul class="default-list">
                    <li>
                        <asp:Label ID="Label16" runat="server" Text="Requested delivery date &nbsp;"
                            CssClass="Editquotelabel wrapEditLabel"></asp:Label>
                        <asp:TextBox ID="txtRequestedDate" runat="server" TabIndex="13" CssClass="TopMargin1"></asp:TextBox>
                        <asp:DropDownList ID="ddlRequestedDate" runat="server"
                            TabIndex="13" CssClass="TopMargin1" Visible="false">
                        </asp:DropDownList>
                        <asp:Label ID="lblRequestedDateError" runat="server" CssClass="ErrorTextRed" Text="Invalid"
                            Visible="False"></asp:Label>
                    </li>
                    <li class="clear">
                        <asp:Label ID="Label19" runat="server" CssClass="Editquotelabel" Text="Submitted by &nbsp;"></asp:Label>
                        <asp:TextBox ID="txtOrderedBy" runat="server" TabIndex="14"
                            MaxLength="12" CssClass="EditquoteText"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label22" runat="server" CssClass="Editquotelabel" Text="PO ID &nbsp;"></asp:Label>
                        <asp:TextBox ID="txtCustomerPO" runat="server" TabIndex="15"
                            MaxLength="16" CssClass="EditquoteText"></asp:TextBox>
                        <asp:Label ID="lblPOError" runat="server" CssClass="ErrorTextRed" Text="Required"
                            Visible="False"></asp:Label>
                    </li>
                    <li>
                        <asp:Label ID="Label27" runat="server" CssClass="Editquotelabel" Text="Job  &nbsp;"></asp:Label>
                        <asp:TextBox ID="txtJob" runat="server" TabIndex="16"
                            MaxLength="24" CssClass="EditquoteText"></asp:TextBox>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 submit" style="padding-top: 20px;">
                <ul class="default-list2">
                    <li>
                        <asp:Label ID="Label18" runat="server" CssClass="Editquotelabel" Text="Order type &nbsp;"></asp:Label>
                        <asp:TextBox ID="txtOrderType" runat="server" CssClass="EditquoteText"
                            Enabled="false"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label3" runat="server" CssClass="Editquotelabel" Text="Reference  &nbsp"></asp:Label>
                        <asp:TextBox ID="txtReference" runat="server" TabIndex="18"
                            MaxLength="20" CssClass="EditquoteText"></asp:TextBox>
                    </li>
                    <li>
                        <asp:Label ID="Label24" runat="server" CssClass="Editquotelabel" Text="Ship via &nbsp;"></asp:Label>
                        <asp:DropDownList ID="ddlShipVia" runat="server" TabIndex="19"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </li>
                    <li>
                        <asp:Label ID="lblShiptoQuoteEdit" runat="server" CssClass="Editquotelabel" Text="Shipping address &nbsp;"></asp:Label>
                        <asp:LinkButton runat="server" ID="lnkShiptoQuoteEdit" CssClass="SettingsLink quoteEditaddinfo" TabIndex="20">Update Information...</asp:LinkButton>
                        <div id="ShiptoQuoteEditModalDiv" runat="server">
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderShiptoQuoteEdit" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlShiptoQuoteEditCustomView"
                                TargetControlID="lnkShiptoQuoteEdit">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlShiptoQuoteEditCustomView" runat="server" CssClass="modal-dialog">
                                <div id="Div2" runat="server" class="modal-content">
                                    <div class="modal-header" style="margin: 0px 15px; padding: 5px 0px;">
                                        <h4 class="modal-title" id="ShiptoQuoteEditModalLabel">Shipping Address</h4>
                                    </div>
                                    <div class="modal-body">
                                        <uc11:ShiptoQuoteEditSelector ID="ShiptoQuoteEditSelector1" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </li>
                    <li>
                        <asp:CheckBox ID="cbxShipComplete" runat="server" TabIndex="21" Text="Ship complete" CssClass="editcheckbox1 editcheckrelease TopMargin10" />
                    </li>
                    <li>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                    <asp:Label ID="lblCustInfo" runat="server" CssClass="Editquotelabel TopMargin10" Text="Retail customer &nbsp;"></asp:Label>
                                </td>
                                <td valign="bottom">
                                    <asp:LinkButton runat="server" ID="lnkAddInfo" CssClass="SettingsLink quoteEditaddinfo" TabIndex="22">Add Information...</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <div id="AddInfoModalDiv" runat="server">
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddInfo" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlAddInfoCustomView"
                                TargetControlID="lnkAddInfo">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlAddInfoCustomView" runat="server" CssClass="modal-dialog" Width="620px">
                                <div id="pnlDragMarkup" runat="server" class="modal-content">
                                    <div class="modal-header" style="margin: 0px 15px; padding: 5px 0px;">
                                        <h4 class="modal-title" id="myModalLabel">Retail Customer Information</h4>
                                    </div>
                                    <div class="modal-body">
                                        <uc8:AddInfoQuoteEditSelector ID="AddInfoQuoteEditSelector" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </li>
                </ul>
            </div>
            <div style="float: left; width: 98%; margin-left: 15px">
                <ul class="default-list">
                    <li>
                        <asp:Label Style="float: left" ID="Label14" runat="server" CssClass="Editquotelabel" Text="Message&nbsp;"></asp:Label>
                        <asp:TextBox Wrap="true" CssClass="messagetextarea" ID="txtMessage" runat="server" TabIndex="17" Rows="3" Columns="153"
                            MaxLength="800" TextMode="MultiLine"></asp:TextBox>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnSaveChanges" runat="server" Text="Save Changes"
                    CssClass="pull-right btn btn-primary returntoQuote" TabIndex="23" UseSubmitBehavior="False"
                    OnClick="btnSaveChanges_Click" ToolTip="Save changes in this section" />
            </div>
            <br class="clear" />
            <hr />
            <div class="pull-left">
                <asp:Button runat="server" ID="btnAddItems" Text="Add Items"
                    CssClass="pull-right btn btn-primary btnquotedit releasebuttonalign" TabIndex="24" UseSubmitBehavior="False"
                    ToolTip="Add items to this quote" OnClick="btnAddItems_Click" />
            </div>
        </div>
        <div style="float: left; padding-top: 7px;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td></td>
                    <td>
                        <asp:Label runat="server" ID="lblGridMessage" ForeColor="red" CssClass="BtnTextPlusMarginBottom" />
                    </td>
                    <td>
                        <asp:LinkButton runat="server" TabIndex="25" ID="btnChangeMarkup" CssClass="SettingsLink" Text="Apply New Multiplier" EnableViewState="True" />
                        <div id="MultiplierModalDiv" runat="server">
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderMultiplier" runat="server" BackgroundCssClass="ModalBackground"
                                DropShadow="true" PopupControlID="pnlMultiplierCustomView"
                                TargetControlID="btnChangeMarkup">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlMultiplierCustomView" runat="server" CssClass="modal-dialog"
                                Width="300px">
                                <div id="pnlDragMultiplier" runat="server" class="modal-content" style="width: 295px; text-align: left">
                                    <div class="modal-header" style="margin: 0px 15px; padding: 10px 0px;">
                                        <h4 class="modal-title" id="lbleditretail" runat="server">Edit Retail Price Multiplier</h4>
                                    </div>
                                    <div class="modal-body">
                                        <uc9:MultiplierSelector ID="MultiplierSelector" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="custom-grid" style="overflow-x: auto">
            <asp:Panel ID="QuoteDetailGridDiv" runat="server">
                <asp:GridView runat="server" ID="gvQuoteDetails" Width="99%"
                    AutoGenerateColumns="False" DataKeyNames="sequence,min_pak_disp,location_reference"
                    OnRowCancelingEdit="gvQuoteDetails_RowCancelingEdit"
                    OnRowDataBound="gvQuoteDetails_RowDataBound"
                    OnRowDeleting="gvQuoteDetails_RowDeleting"
                    OnRowEditing="gvQuoteDetails_RowEditing"
                    OnRowUpdating="gvQuoteDetails_RowUpdating"
                    OnDataBound="gvQuoteDetails_DataBound"
                    CssClass="custom-tabler">
                    <Columns>
                        <asp:CommandField ShowEditButton="true" ShowDeleteButton="true" HeaderText="Actions" DeleteImageUrl="~/Images/delete-icon.png" EditImageUrl="~/Images/edit-icon.png" CancelImageUrl="~/Images/Grid_Cancel.png" UpdateImageUrl="~/Images/Grid_UpdateSave.png" ButtonType="Image" UpdateText="Save" />
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Panel runat="server" ID="panel_Configure" Visible="True" Width="88px">
                                        <asp:LinkButton ID="linkbutton_Configure" 
                                                        runat="server" 
                                                        PostBackUrl='<%# "~/PreConfigurator.aspx" + 
                                                                    "?parent=pv" + 
                                                                    "&source=pvquote" + 
                                                                    "&mode=update" + 
                                                                    "&item=" + System.Web.HttpUtility.UrlEncode((string)Eval("ITEM"))  +
                                                                    "&size=" + System.Web.HttpUtility.UrlEncode(((string)Eval("SIZE")).Replace("&", "and").Replace("#", "").Replace("+", "")) +
                                                                    "&description=" + System.Web.HttpUtility.UrlEncode(((string)Eval("quote_desc")).Replace("&", "and").Replace("#", "").Replace("+", "")) +
                                                                    "&uom=" + Eval("price_uom_code") + 
                                                                    "&retailuom=" + Eval("retail_price_uom_code") + 
                                                                    "&quote_id=" + Eval("quote_id") + 
                                                                    "&sequence=" + Eval("sequence") + 
                                                                    "&orderqty=" + Eval("qty_ordered")  + 
                                                                    "&universalOptions=" + Eval("has_universal_options") %>'
                                                       Text="Configure/Copy" 
                                                       OnClientClick="newWindowClick('configurator');" 
                                                       Visible="True"
                                                       CssClass="handHover" 
                                                       ForeColor="Black" 
                                                       Width="88px" />
                                        
                                        </asp:Panel>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                            <ControlStyle Width="88px" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Sequence" DataField="sequence" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Item" DataField="ITEM" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Size" DataField="SIZE" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Description" DataField="quote_desc" HtmlEncode="false" ReadOnly="true" ItemStyle-CssClass="wrapItem">
                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            <ItemStyle Wrap="True" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapItem" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="WO Phrase/Quoted Tally" DataField="wo_phrase" HtmlEncode="false" ReadOnly="true" ItemStyle-CssClass="wrapItem">
                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            <ItemStyle Wrap="True" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapItem" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Location Reference" DataField="location_reference" HtmlEncode="false" ReadOnly="false" ItemStyle-CssClass="wrapItem">
                            <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                            <ItemStyle Wrap="True" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapstyledecimal" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Order Qty" DataField="qty_ordered" HtmlEncode="false" ReadOnly="false">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapstyledecimal" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Min Pack" DataField="min_pak_disp" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Previously Released" DataField="qty_released" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="UOM" DataField="uom" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Price*" DataField="net_price" HtmlEncode="false" ReadOnly="true" DataFormatString="{0:C}">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapstyledecimal" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="UOM" DataField="price_uom_code" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:CheckBoxField HeaderText="Price Override" DataField="override_price" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:CheckBoxField>
                        <asp:BoundField HeaderText="Extension*" DataField="extended_price" HtmlEncode="false" ReadOnly="true" DataFormatString="{0:C}">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Retail Price" DataField="retail_price" HtmlEncode="false" ReadOnly="true" DataFormatString="{0:C}">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" CssClass="wrapstyledecimal" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Retail UOM" DataField="retail_price_uom_code" HtmlEncode="false" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:CheckBoxField HeaderText="Retail Price Override" DataField="retail_override_price" ReadOnly="true">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:CheckBoxField>
                        <asp:BoundField HeaderText="Retail Extension" DataField="retail_extended_price" HtmlEncode="false" ReadOnly="true" DataFormatString="{0:C}">
                            <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                            <ItemStyle Wrap="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <p>
                    <asp:Label runat="server" ID="lblPriceDisclaimer" Text="* Prices are estimates and may change upon final submission." />
                </p>
            </asp:Panel>
        </div>
    </div>
    <cc1:CalendarExtender ID="calRequestedDate" runat="server" PopupButtonID="txtRequestedDate"
        Format="MM/dd/yyyy" TargetControlID="txtRequestedDate" CssClass="cal_Theme1">
    </cc1:CalendarExtender>
</div>

