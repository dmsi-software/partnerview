using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Dmsi.Agility.Data;
using Dmsi.Agility.EntryNET;
using Dmsi.Agility.EntryNET.StrongTypesNS;

public partial class UserControls_OrderTab : System.Web.UI.UserControl
{
    private bool _viewInventoryPrices = false;

    public delegate void PublishCartDelegate(int itemCount, string cartTotal);
    public event PublishCartDelegate OnPublishCart;

    public delegate void SaleTypeChangedDelegate(string newSaleType, string newSaleTypeDescription);
    public event SaleTypeChangedDelegate OnSaleTypeChanged;

    public delegate void CartRowEditModeDelegate(bool isInEditMode);
    public event CartRowEditModeDelegate OnCartRowEditModeChanged;
    int configureexist = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        //reset error label from last pass...
        lblNegativeQuantityError.Visible = false;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        //if menu security removes Inventory Items, then the Continue Shopping buttons have to be removed from view...
        this.btnContinueShopping.Visible = false;
        this.btnContinueShopping2.Visible = false;

        foreach (DataRow row in dsPV.bPV_userMenus.Rows)
        {
            if (row["menu_item_description"].ToString() == "Inventory Items")
            {
                this.btnContinueShopping.Visible = true;
                this.btnContinueShopping2.Visible = true;
            }
        }
        //end of if menu security removes Inventory Items

        if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
        {
            DataRow[] rows = dsPV.bPV_Action.Select("token_code='view_inv_prices'");

            if (rows.Length > 0)
            {
                foreach (DataControlField col in this.gvOrderCart.Columns)
                {
                    if (col.HeaderText == "Price*" ||
                        col.HeaderText == "Extension*" ||
                        col.HeaderText == "Retail Price" ||
                        col.HeaderText == "Retail Extension" ||
                        col.HeaderText == "UOM ") //Do not remove this space. It's there to separate the two UOM columns...
                    {
                        col.Visible = true;
                    }
                }

                _viewInventoryPrices = true;

                ApplyUserPrefPriceColumnSelection();
            }

            rows = null;

            //check for submit_quotes and submit_orders actions allocations...
            rows = dsPV.bPV_Action.Select("token_code='submit_orders'");

            if (rows.Length == 0) //remove ComboBox Item for Orders; no allocation
            {
                for (int x = 0; x < ddlSubmitAs.Items.Count; x++)
                {
                    if (ddlSubmitAs.Items[x].Value == "SO")
                        ddlSubmitAs.Items.RemoveAt(x);
                }
            }

            rows = null;
            rows = dsPV.bPV_Action.Select("token_code='submit_quotes'");

            if (rows.Length == 0) //remove ComboBox Item for Quotes; no allocation
            {
                for (int x = 0; x < ddlSubmitAs.Items.Count; x++)
                {
                    if (ddlSubmitAs.Items[x].Value == "QU")
                        ddlSubmitAs.Items.RemoveAt(x);
                }
            }

            rows = null;
        }

        // If initial load, initiate the binding since customer and shipto might be already present
        if (!this.IsPostBack &&
            dm.GetCache("currentBranchID") != null &&
            dm.GetCache("ShiptoRowForInformationPanel") != null)
            this.Reset((string)dm.GetCache("currentBranchID"), "");

        if (!IsPostBack)
        {
            //get default_submit_cart_as value to set Selected on ddlSubmitAs Item.
            if (dsPV.bPV_CoInfo.Rows[0]["default_submit_cart_as"].ToString() != "")
                ddlSubmitAs.Items.RemoveAt(0);

            for (int x = 0; x < ddlSubmitAs.Items.Count; x++)
            {
                if (dsPV.bPV_CoInfo.Rows[0]["default_submit_cart_as"].ToString() == "" && ddlSubmitAs.Items.Count == 2)
                {
                    ddlSubmitAs.Items.RemoveAt(0);
                    ddlSubmitAs.Items[0].Selected = true;
                    break;
                }
                else if (dsPV.bPV_CoInfo.Rows[0]["default_submit_cart_as"].ToString() == ddlSubmitAs.Items[x].Text)
                {
                    ddlSubmitAs.Items[x].Selected = true;
                    break;
                }
            }

            this.CartTotalDiv.Visible = _viewInventoryPrices;

            this.ddlOrderHeaderRows.SelectedIndex = Profile.ddlOrderHeaderRows_SelectedIndex;

            int pageSize;
            if (ddlOrderHeaderRows.SelectedValue == "All")
                pageSize = 32000;
            else
                pageSize = Convert.ToInt32(ddlOrderHeaderRows.SelectedValue);

            this.gvOrderCart.PageSize = pageSize;
            gvOrderCart.DataBind();

            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
            txtEmail.Text = user.GetContextValue("currentUserEmail");

            if (user.GetContextValue("currentUserName").Length > 12)
                txtOrderedBy.Text = user.GetContextValue("currentUserName").Substring(0, 12);
            else
                txtOrderedBy.Text = user.GetContextValue("currentUserName");
        }
        else
        {
            UpdateButtonStatus(gvOrderCart.EditIndex == -1);

            if (btnCartCheckout2.Enabled)
                this.btnSubmitOrder2.Focus();
        }

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            AddInfoModalDiv.Visible = true;
            this.lnkAddInfo.Visible = true;
            this.lblCustInfo.Visible = true;
        }
        else
        {
            AddInfoModalDiv.Visible = false;
            this.lnkAddInfo.Visible = false;
            this.lblCustInfo.Visible = false;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        DataManager dm = new DataManager();

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            AddInfoModalDiv.Visible = true;
            this.lnkAddInfo.Visible = true;
            this.lblCustInfo.Visible = true;
        }
        else
        {
            AddInfoModalDiv.Visible = false;
            this.lnkAddInfo.Visible = false;
            this.lblCustInfo.Visible = false;
        }

        if (Session["disable_edit_shipping_address"] != null && (bool)Session["disable_edit_shipping_address"] == true)
        {
            lblShiptoOverride.Visible = false;
            lnkShiptoOverride.Visible = false;
            ShiptoModalDiv.Visible = false;
            cbxShipComplete.CssClass = "editcheckbox1 editcheckrelease";
        }
        else
        {
            lblShiptoOverride.Visible = true;
            lnkShiptoOverride.Visible = true;
            ShiptoModalDiv.Visible = true;
            cbxShipComplete.CssClass = "editcheckbox1 editcheckrelease TopMargin10";
        }

        if (ddlSubmitAs.Text == "SO" && dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            DataRow row = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

            int shipToSequence = (int)row["seq_num"];

            DataRow[] rows = ((dsCustomerDataSet.ttShiptoDataTable)dm.GetCache("AllCurrentCustomerShiptos")).Select("seq_num = " + shipToSequence.ToString());
            if (rows.Length == 1)
            {
                row = rows[0];
            }

            if ((bool)row["cust_po_required"])
            {
                lblPOError.Visible = true;
            }
            else
                lblPOError.Visible = false;

            if ((bool)row["job_number_required"])
            {
                lblJobError.Visible = true;
            }
            else
                lblJobError.Visible = false;

            if ((bool)row["reference_required"])
            {
                lblReferenceError.Visible = true;
            }
            else
                lblReferenceError.Visible = false;

            if ((bool)row["ship_via_required"])
            {
                lblShipViaError.Visible = true;
            }
            else
                lblShipViaError.Visible = false;
        }

        if (dm.GetCache("ShiptoRowForInformationPanel") != null &&
            Session["UserPref_DisplaySetting"] != null &&
            ((string)Session["UserPref_DisplaySetting"] == "Net" || (string)Session["UserPref_DisplaySetting"] == "Net price") &&
            Session["display_min_order_hold_amount"] != null && ((string)Session["display_min_order_hold_amount"]).ToLower() == "yes")
        {
            DataRow row = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

            DataTable dt = (DataTable)Session["AllCurrentCustomerShiptos"];

            DataRow[] rows = dt.Select("seq_num=" + Convert.ToInt32(row["seq_num"]));

            Decimal minOrderAmount = 0;

            if (rows.Length == 1)
                minOrderAmount = Convert.ToDecimal(rows[0]["min_ord_hold_amount"]);
            else
                minOrderAmount = Convert.ToDecimal(row["min_ord_hold_amount"]);

            if (minOrderAmount > 0)
            {
                lblMinAmount.Text = "Ship-to total minimum order amount for all open orders: " + minOrderAmount.ToString("C2");
                lblMinAmount.Visible = true;
            }
            else
            {
                lblMinAmount.Visible = false;
            }
        }
        else
            lblMinAmount.Visible = false;

        if (gvOrderCart.Rows.Count == 0)
            lblLineTotal.Text = "";

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Theme", "RespFormatSingle();ComplementoryPopupHeight();", true);
    }

    private void UpdateButtonStatus(bool state)
    {
        btnCartCheckout.Enabled = state;
        btnCartCheckout2.Enabled = state;
        btnEditCart.Enabled = state;
        btnEditCart2.Enabled = state;
        btnSubmitOrder.Enabled = state && (Session["MinPackViolation"] == null || (Session["MinPackViolation"] != null && (bool)Session["MinPackViolation"]));
        btnSubmitOrder2.Enabled = state && (Session["MinPackViolation"] == null || (Session["MinPackViolation"] != null && (bool)Session["MinPackViolation"]));
        btnDeleteAll.Enabled = state;

        btnContinueShopping.Enabled = state;
        btnContinueShopping2.Enabled = state;
    }

    #region Public Methods

    public void ApplyUserPrefPriceColumnSelection()
    {
        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            foreach (DataControlField col in this.gvOrderCart.Columns)
            {
                if (col.HeaderText == "Retail Price" ||
                    col.HeaderText == "Retail Extension")
                {
                    if (_viewInventoryPrices)
                        col.Visible = true;
                    else
                        col.Visible = false;
                }

                if (col.HeaderText == "Price*" ||
                    col.HeaderText == "Extension*")
                {
                    col.Visible = false;
                }
            }
        }
        else
        {
            foreach (DataControlField col in this.gvOrderCart.Columns)
            {
                if (col.HeaderText == "Retail Price" ||
                    col.HeaderText == "Retail Extension")
                {
                    col.Visible = false;
                }

                if (col.HeaderText == "Price*" ||
                    col.HeaderText == "Extension*")
                {
                    if (_viewInventoryPrices)
                        col.Visible = true;
                    else
                        col.Visible = false;
                }
            }
        }
    }

    public void SetPricingView()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        //if menu security removes Inventory Items, then the Continue Shopping buttons have to be removed from view...
        this.btnContinueShopping.Visible = false;
        this.btnContinueShopping2.Visible = false;

        foreach (DataRow row in dsPV.bPV_userMenus.Rows)
        {
            if (row["menu_item_description"].ToString() == "Inventory Items")
            {
                this.btnContinueShopping.Visible = true;
                this.btnContinueShopping2.Visible = true;
            }
        }
        //end of if menu security removes Inventory Items

        if (dsPV.bPV_Action != null && dsPV.bPV_Action.Rows.Count > 0)
        {
            DataRow[] rows = dsPV.bPV_Action.Select("token_code='view_inv_prices'");

            if (rows.Length > 0)
            {
                foreach (DataControlField col in this.gvOrderCart.Columns)
                {
                    if (col.HeaderText == "Price*" ||
                        col.HeaderText == "Extension*" ||
                        col.HeaderText == "Retail Price" ||
                        col.HeaderText == "Retail Extension" ||
                        col.HeaderText == "UOM ") //Do not remove this space. It's there to separate the to UOM columns...
                    {
                        col.Visible = true;
                    }
                }

                _viewInventoryPrices = true;
            }

            rows = null;
            //check for submit_quotes and submit_orders actions allocations...
            rows = dsPV.bPV_Action.Select("token_code='submit_orders'");

            if (rows.Length == 0) //remove ComboBox Item for Orders; no allocation
            {
                for (int x = 0; x < ddlSubmitAs.Items.Count; x++)
                {
                    if (ddlSubmitAs.Items[x].Value == "SO")
                        ddlSubmitAs.Items.RemoveAt(x);
                }
            }

            rows = null;
            rows = dsPV.bPV_Action.Select("token_code='submit_quotes'");

            if (rows.Length == 0) //remove ComboBox Item for Quotes; no allocation
            {
                for (int x = 0; x < ddlSubmitAs.Items.Count; x++)
                {
                    if (ddlSubmitAs.Items[x].Value == "QU")
                        ddlSubmitAs.Items.RemoveAt(x);
                }
            }

            rows = null;
        }

        // If initial load, initiate the binding since customer and shipto might be already present
        if (!this.IsPostBack &&
            dm.GetCache("currentBranchID") != null &&
            dm.GetCache("ShiptoRowForInformationPanel") != null)
            this.Reset((string)dm.GetCache("currentBranchID"), "");

        if (!IsPostBack)
        {
            //get default_submit_cart_as value to set Selected on ddlSubmitAs Item.
            if (dsPV.bPV_CoInfo.Rows[0]["default_submit_cart_as"].ToString() != "")
                ddlSubmitAs.Items.RemoveAt(0);

            for (int x = 0; x < ddlSubmitAs.Items.Count; x++)
            {
                if (dsPV.bPV_CoInfo.Rows[0]["default_submit_cart_as"].ToString() == "" && ddlSubmitAs.Items.Count == 2)
                {
                    ddlSubmitAs.Items.RemoveAt(0);
                    ddlSubmitAs.Items[0].Selected = true;
                    break;
                }
                else if (dsPV.bPV_CoInfo.Rows[0]["default_submit_cart_as"].ToString() == ddlSubmitAs.Items[x].Text)
                {
                    ddlSubmitAs.Items[x].Selected = true;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Call this to clear bound data
    /// </summary>
    /// <param name="branchId">Current Branch ID</param>
    public void Reset(string branchID)
    {
        SubmitTop.Visible = false;
        SubmitButtons.Visible = false;
        SubmitButtons2.Visible = false;
        CartButtons.Visible = true;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.Shipto currentShipto = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        } 

        Dmsi.Agility.Data.OrderCart orderCart;
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");

        decimal markupFactorValue = 1;
        if (Session["MarkupFactor"] != null)
        {
            markupFactorValue = Convert.ToDecimal(Session["MarkupFactor"]);
        }

        //Reset grids and rebind datasource
        dm.RemoveCache("SaleType");
        dm.RemoveCache("dsOrderCartDataSet");
        dm.RemoveCache("OrderCart");

        orderCart = new OrderCart(branchID, user.GetContextValue("currentUserLogin"), currentShipto.ShiptoObj.ToString(), "<All>", markupFactorValue, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        dm.SetCache("OrderCart", orderCart);
        dm = null;

        gvOrderCart.SelectedIndex = -1;
        currentShipto.Dispose();

        lblSubmitAsError.Text = "*";
        lblOrderTypeError.Text = "*";
        lblPOError.Text = "*";
        lblOrderedByError.Text = "*";
        lblJobError.Text = "*";
        lblReferenceError.Text = "*";
        lblShipViaError.Text = "*";
        lblEmailError.Text = "*";

        this.btnEditCart_Click(null, null);
    }

    /// <summary>
    /// Call this to clear bound data when the branch or customer has changed
    /// </summary>
    /// <param name="branchID">Current Branch ID</param>
    /// <param name="shiptoObj">Current Shipto Object</param>
    public void Reset(string branchID, string shiptoObj)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        this.Reset(branchID);

        Dmsi.Agility.Data.OrderCart orderCart = (OrderCart)dm.GetCache("OrderCart");

        if (orderCart != null && orderCart.ReferencedDataSet != null)
        {
            //handle populating shipvia drop-down here...
            ddlShipVia.Items.Clear();
            ddlShipVia.Enabled = true;

            foreach (DataRowView row in orderCart.ReferencedDataSet.Tables["ttship_via"].DefaultView)
            {
                ListItem li = new ListItem();
                li.Text = ((string)row["ship_via"]).ToUpper();
                li.Value = ((string)row["ship_via"]).ToUpper();
                ddlShipVia.Items.Add(li);
            }

            int ReturnCode;
            string MessageText;

            Shipto shipto = new Shipto(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            if (shipto.ReferencedRow != null)
            {
                string shipviaFromShipto = shipto.ReferencedRow["ship_via"].ToString().ToUpper();

                ListItem lisearch = new ListItem();
                lisearch.Text = shipviaFromShipto;
                lisearch.Value = shipviaFromShipto;

                if (ddlShipVia.Items.Contains(lisearch))
                {
                    for (int x = 0; x < ddlShipVia.Items.Count; x++)
                    {
                        if (ddlShipVia.Items[x].Value == shipviaFromShipto)
                        {
                            ddlShipVia.SelectedIndex = x;
                            break;
                        }
                    }
                }
                else
                {
                    ddlShipVia.SelectedIndex = 0;
                }
            }

            dm.SetCache("SaleType", orderCart.DefaultSaleType);

            if (this.OnSaleTypeChanged != null)
                this.OnSaleTypeChanged(orderCart.DefaultSaleType, orderCart.SaleTypeDescription);
        }
        else
        {
            dm.SetCache("SaleType", "<All>");
            if (this.OnSaleTypeChanged != null)
                this.OnSaleTypeChanged("<All>", "All Sale Types");
        }

        dm = null;
    }

    public void SwitchCartToEditMode(dsOrderCartDataSet.ttorder_cartRow row)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dm.RemoveCache("dsOrderCartDataSet");
        dm.RemoveCache("OrderCart");
        AssignDataSource(false);


        this.CartGridDiv.Visible = true;
        this.btnEditCart_Click(null, null);
    }

    public void ForceEmptyCartClientOnly()
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        OrderCart cart = (OrderCart)dm.GetCache("OrderCart");
        cart.ReferencedDataSet.Tables["ttorder_cart"].Rows.Clear();
        cart.ReferencedDataSet.AcceptChanges();

        string saleType = "<all>";
        string filter = "";
        string sort = (string)dm.GetCache("gvOrderCartSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "";
        else
            sort += dm.GetCache("gvOrderCartSortDirection");

        if (dm.GetCache("SaleType") == null)
        {
            dm.SetCache("SaleType", cart.DefaultSaleType);

            if (this.OnSaleTypeChanged != null)
                this.OnSaleTypeChanged(cart.DefaultSaleType, cart.SaleTypeDescription);
        }

        saleType = (string)dm.GetCache("SaleType");
        DataSet cartDS = cart.ReferencedDataSet;
        DataView dv = new DataView(cartDS.Tables["ttorder_cart"], filter, sort, DataViewRowState.CurrentRows);

        DeleteButtonDiv.Visible = false;
        EmptyCartDiv.Visible = false;
        //this.CartTotalDiv.Visible = false;
        if (dv.ToTable().Rows.Count > 0)
        {
            DataRow[] rows = dv.ToTable().Select("configurable='True'");
            if (rows.Length > 0)
            {
                configureexist = 1;
            }
            else
            {
                configureexist = 0;
            }
        }
        gvOrderCart.DataSource = dv;
        gvOrderCart.DataBind();
        int ipageindex = gvOrderCart.PageIndex;
        int igridcount = gvOrderCart.Rows.Count;
        int itotalrecords = dv.ToTable().Rows.Count;
        int iNoofrows = Convert.ToInt32(ddlOrderHeaderRows.SelectedValue);
        int iFirstrecordno = ipageindex * (iNoofrows);
        iFirstrecordno = iFirstrecordno + 1;
        int iLastRecord = (iFirstrecordno - 1) + igridcount;
        if (itotalrecords > 0)
        {
        }
        if (cartDS.Tables["ttorder_cart"].Rows.Count == 0)
            lblPriceDisclaimer.Visible = false;
        else if (Session["UserPref_DisplaySetting"] != null &&
                ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                 (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                 (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            lblPriceDisclaimer.Visible = false;
        }
        else
            lblPriceDisclaimer.Visible = true;

        decimal cartTotal;
        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            cartTotal = cart.RetailCartTotal;
        }
        else
            cartTotal = cart.CartTotal;

        if (this.OnPublishCart != null)
            this.OnPublishCart(cartDS.Tables["ttorder_cart"].Rows.Count, String.Format("{0:C}", cartTotal));
    }

    public void AssignDataSource(bool ForceCartFetch)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        if (ForceCartFetch)
            dm.SetCache("OrderCart", null);

        string saleType = "<all>";
        string filter = "";
        string sort = (string)dm.GetCache("gvOrderCartSortColumn");

        if (sort == null || sort.Length == 0)
            sort = "";
        else
            sort += dm.GetCache("gvOrderCartSortDirection");

        OrderCart orderCart = null;

        if (dm.GetCache("OrderCart") != null)
            orderCart = (OrderCart)dm.GetCache("OrderCart");
        else
        {
            int ReturnCode;
            string MessageText;

            Dmsi.Agility.Data.Shipto currentShipto = new Shipto(out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            } 

            AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
            Branch branch = new Branch();

            if (dm.GetCache("SaleType") == null)
                saleType = "default"; // the back-end will try to use the default sale type to fetch prices else <all>
            else
                saleType = (string)dm.GetCache("SaleType");

            decimal markupFactorValue = 1;
            if (Session["MarkupFactor"] != null)
            {
                markupFactorValue = Convert.ToDecimal(Session["MarkupFactor"]);
            }

            orderCart = new OrderCart(branch.BranchId, user.GetContextValue("currentUserLogin"), currentShipto.ShiptoObj.ToString(), saleType, markupFactorValue, out ReturnCode, out MessageText);

            if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
            {
                Session["LoggedIn"] = null;
                Response.Redirect("~/Logon.aspx");
            }

            dm.SetCache("OrderCart", orderCart);

            currentShipto.Dispose();
            branch.Dispose();
        }

        if (dm.GetCache("SaleType") == null)
        {
            dm.SetCache("SaleType", orderCart.DefaultSaleType);

            if (this.OnSaleTypeChanged != null)
                this.OnSaleTypeChanged(orderCart.DefaultSaleType, orderCart.SaleTypeDescription);
        }

        saleType = (string)dm.GetCache("SaleType");
        DataSet cartDS = orderCart.ReferencedDataSet;
        DataView dv = new DataView(cartDS.Tables["ttorder_cart"], filter, sort, DataViewRowState.CurrentRows);

        if (cartDS.Tables["ttorder_cart"].Rows.Count == 0)
        {
            DeleteButtonDiv.Visible = false;
            EmptyCartDiv.Visible = false;
            //this.CartTotalDiv.Visible = false;
            SubmitButtons2.Visible = false;

            if (!ForceCartFetch)
            {
                btnCartCheckout2.Visible = false;
                lblItemsInCart.Visible = false;
                CartButtons2.Visible = false;
            }
        }
        else
        {
            decimal cartTotal;

            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                cartTotal = orderCart.RetailCartTotal;
            }
            else
                cartTotal = orderCart.CartTotal;

            DeleteButtonDiv.Visible = true;
            EmptyCartDiv.Visible = true;
            this.lblLineTotal.Text = String.Format("{0:#,0.00}", cartTotal);
            this.CartTotalDiv.Visible = _viewInventoryPrices;
        }

        if (cartDS.Tables["ttorder_cart"].Rows.Count > 0 && ForceCartFetch)
        {
            btnCartCheckout2.Visible = true;
            lblItemsInCart.Visible = true;
            CartButtons2.Visible = true;
        }
        if (dv.ToTable().Rows.Count > 0)
        {
            DataRow[] rows = dv.ToTable().Select("configurable='True'");
            if(rows.Length>0)
            {
                configureexist = 1;
            }
            else
            {
                configureexist = 0;
            }
        }
        gvOrderCart.DataSource = dv;
        gvOrderCart.DataBind();
        
        int ipageindex = gvOrderCart.PageIndex;
        int igridcount = gvOrderCart.Rows.Count;
        int itotalrecords = dv.ToTable().Rows.Count;
        int iNoofrows;
        this.ddlOrderHeaderRows.SelectedIndex = Profile.ddlOrderHeaderRows_SelectedIndex;
        if (ddlOrderHeaderRows.SelectedValue == "All")
        {
            iNoofrows = 32000;
            igridcount = itotalrecords;
        }
        else
        {
            iNoofrows = Convert.ToInt32(ddlOrderHeaderRows.SelectedValue);
        }
        int iFirstrecordno = ipageindex * (iNoofrows);
        iFirstrecordno = iFirstrecordno + 1;
        int iLastRecord = (iFirstrecordno - 1) + igridcount;
        if (itotalrecords > 0)
        {
        }
        //Logic to populate Order Type ddl...
        ddlOrderType.Items.Clear();
        cartDS.Tables["ttsales_type"].DefaultView.Sort = "description ASC";

        int j = 0, selectedSaleType = -1;

        foreach (DataRowView row in cartDS.Tables["ttsales_type"].DefaultView)
        {
            ListItem li = new ListItem();
            li.Text = (string)row["description"];
            li.Value = (string)row["sale_type"];
            ddlOrderType.Items.Add(li);

            if (saleType.ToLower() == li.Value.ToLower())
                selectedSaleType = j;
            else
                j++;
        }

        if (selectedSaleType == -1 &&
            cartDS.Tables["ttcust_default"].Rows.Count > 0)
            selectedSaleType = ddlOrderType.Items.IndexOf(ddlOrderType.Items.FindByValue(cartDS.Tables["ttcust_default"].Rows[0]["sale_type"].ToString()));

        ddlOrderType.SelectedIndex = selectedSaleType;

        //TODO: For now call, Customer FetchDeliveryList here...
        if (dm.GetCache("ShiptoRowForInformationPanel") != null)
        {
            DataRow shiptoRow = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");
            string inputCustKey = shiptoRow["cust_key"].ToString();
            int inputShipto = Convert.ToInt32(shiptoRow["seq_num"].ToString());
            string inputSaleType = cartDS.Tables["ttcust_default"].Rows[0]["sale_type"].ToString();

            if (dm.GetCache("SaleType") != null)
                inputSaleType = (string)dm.GetCache("SaleType");

            List<string> dateStrings = new List<string>();

            if (dm.GetCache("use_route_dates") != null && (string)dm.GetCache("use_route_dates") == "Select date from available routes")
            {
                int ReturnCode;
                string MessageText;

                Dmsi.Agility.Data.Customers custs = new Customers(out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                } 

                dateStrings = custs.FetchDeliveryList(inputCustKey, inputShipto, inputSaleType, true, out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                } 
            }

            if (dateStrings.Count > 0)
            {
                ddlRequestedDate.Visible = true;
                txtRequestedDate.Visible = false;


                if (Session["orderDateStrings"] == null)
                {
                    ddlRequestedDate.DataSource = dateStrings;
                    ddlRequestedDate.DataBind();
                    Session["orderDateStrings"] = dateStrings;

                    if (Session["LastChosenRequestedDeliveryDate"] != null)
                    {
                        foreach (ListItem item in ddlRequestedDate.Items)
                        {
                            if ((string)Session["LastChosenRequestedDeliveryDate"] == item.Text)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                }

                if (ddlRequestedDate.SelectedItem != null && ddlRequestedDate.SelectedItem.Text == "Next available")
                {
                    lblRequestdDateAsterisk.Visible = false;
                    lblRequestedDateMessage.Visible = false;
                }

                if (ddlRequestedDate.SelectedItem == null)
                {
                    ddlRequestedDate.DataSource = dateStrings;
                    ddlRequestedDate.DataBind();
                    Session["orderDateStrings"] = dateStrings;

                    if (Session["LastChosenRequestedDeliveryDate"] != null)
                    {
                        foreach (ListItem item in ddlRequestedDate.Items)
                        {
                            if ((string)Session["LastChosenRequestedDeliveryDate"] == item.Text)
                            {
                                item.Selected = true;
                                lblRequestdDateAsterisk.Visible = true;
                                lblRequestedDateMessage.Visible = true;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                ddlRequestedDate.Visible = false;
                txtRequestedDate.Visible = true;



                lblRequestdDateAsterisk.Visible = false;
                lblRequestedDateMessage.Visible = false;

                if (txtRequestedDate.Text.Length > 0)
                {
                    lblRequestdDateAsterisk.Visible = true;
                    lblRequestedDateMessage.Visible = true;
                }

                ddlRequestedDate.DataSource = null;
                ddlRequestedDate.DataBind();
                if (Session["orderDateStrings"] != null)
                    Session["orderDateStrings"] = null;
            }
        }

        Session["MinPackViolation"] = DisplayMinPackViolation(orderCart);

        if (cartDS.Tables["ttorder_cart"].Rows.Count == 0)
            lblPriceDisclaimer.Visible = false;
        else if (Session["UserPref_DisplaySetting"] != null &&
                ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                 (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                 (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            lblPriceDisclaimer.Visible = false;
        }
        else
            lblPriceDisclaimer.Visible = true;

        if (this.OnPublishCart != null)
        {
            decimal cartTotal;

            if (Session["UserPref_DisplaySetting"] != null &&
               ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
            {
                cartTotal = orderCart.RetailCartTotal;
            }
            else
                cartTotal = orderCart.CartTotal;

            if (cartDS.Tables["ttorder_cart"].Rows.Count == 0)
                this.OnPublishCart(cartDS.Tables["ttorder_cart"].Rows.Count, String.Format("{0:C}", 0));
            else
                this.OnPublishCart(cartDS.Tables["ttorder_cart"].Rows.Count, String.Format("{0:C}", cartTotal));
        }
    }

    #endregion

    #region Private Methods

    private void DisplayOrderMessage(string message)
    {

        SubmitButtons2.Visible = false;

        lblLine1.Visible = true;
        lblLine1.Text = message;

        lblLine2.Visible = false;
        lblError.Visible = false;

        if (message.Contains("has been submitted"))
        {
            lblError.Visible = false;
            lblLine2.Visible = true;

            if (txtCcEmail.Text.Length > 0)
                lblLine2.Text = "<br />An email confirmation will be sent to " + txtEmail.Text + " and " + txtCcEmail.Text + ".<br />";
            else
                lblLine2.Text = "<br />An email confirmation will be sent to " + txtEmail.Text + ".<br />";

            lblLine2.Text += "<br />If the confirmation is not received, please contact our office.";
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = message;

            lblLine1.Visible = false;
            lblLine2.Visible = false;
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Popup", "ShowPopup('ctl00_cphMaster1_MainTabControl1_OrderTab1_pnlOrderConfirmation','ctl00_cphMaster1_MainTabControl1_OrderTab1_pnlinnerorder');", true);
    }

    #endregion

    protected void btnCartCheckout_Click(object sender, EventArgs e)
    {
        if (sender != null && e != null)
        {
            /* Analytics Tracking */

            Button bt = sender as Button;

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Label = "";
            aInput.Value = "0";

            if (bt.ID == "btnCartCheckout2")
            {
                aInput.Action = "Cart - Proceed to Checkout - Top";
            }
            else
            {
                aInput.Action = "Cart - Proceed to Checkout - Bottom";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        //Each time, set ship-complete to default on cust ship-to...

        int ReturnCode;
        string MessageText;

        Shipto shipto = new Shipto(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        Dmsi.Agility.Data.AgilityCustomer cust = new AgilityCustomer(true, "CustomerRowForInformationPanel", out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        } 

        DataRow row = cust.GetShiptoRow(shipto.ShiptoSequence);
        bool shipcompleteFromShipto = Convert.ToBoolean(row["ship_complete"].ToString()); 
             
        if (shipcompleteFromShipto)
            cbxShipComplete.Checked = true;
        else
            cbxShipComplete.Checked = false;

        SubmitTop.Visible = true;
        SubmitButtons.Visible = true;
        SubmitButtons2.Visible = true;
        CartButtons.Visible = false;
        btnCartCheckout2.Visible = false;
        lblItemsInCart.Visible = false;
        CartButtons2.Visible = false;
        this.AssignDataSource(false);

        if (OnCartRowEditModeChanged != null)
        {
            Session["UIisOnCheckout"] = true;
            OnCartRowEditModeChanged(true);
            this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
        }
        HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
        hypUserPreferences.Visible = false;

        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
    }

    protected void btnEditCart_Click(object sender, EventArgs e)
    {

        /* Analytics Tracking */

        Button bt = sender as Button;

        if (sender != null && e != null && bt != null)
        {
            
            if (bt.ID == "btnEditCart2")
            {

                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Label = "";
                aInput.Value = "0";
                aInput.Action = "Cart - Return to Cart - Top";

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();

            }
            else if (bt.ID == "btnEditCart")
            {
                AnalyticsInput aInput = new AnalyticsInput();

                aInput.Type = "event";
                aInput.Label = "";
                aInput.Value = "0";
                aInput.Action = "Cart - Return to Cart - Bottom";

                AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                am.BeginTrackEvent();
            }
          
        }

        lblRequestedDateError.Visible = false;
        lblFaxError.Visible = false;
        lblPOError.Visible = false;


        CartGridDiv.Visible = true;
        SubmitTop.Visible = false;
        SubmitButtons.Visible = false;
        SubmitButtons2.Visible = false;
        CartButtons.Visible = true;
        btnCartCheckout2.Visible = true;
        lblItemsInCart.Visible = true;
        CartButtons2.Visible = true;
        this.AssignDataSource(false);

        if (OnCartRowEditModeChanged != null)
        {
            Session["UIisOnCheckout"] = false;
            OnCartRowEditModeChanged(false);
        }
    }

    protected void gvOrderCart_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvOrderCart.Columns[0].Visible = false;
        gvOrderCart.Columns[1].Visible = false;
        gvOrderCart.Columns[2].Visible = false;
        Panel p = (Panel)gvOrderCart.Rows[e.NewEditIndex].Cells[0].FindControl("panel_Configure");
        if (p != null && p.Visible == true)
        {
            BoundField bf = gvOrderCart.Columns[9] as BoundField;
            bf.ReadOnly = true;

            bf = gvOrderCart.Columns[8] as BoundField;
            bf.ReadOnly = false;
        }
        else if (p != null && p.Visible == false)
        {
            BoundField bf = gvOrderCart.Columns[9] as BoundField;
            bf.ReadOnly = false;

            bf = gvOrderCart.Columns[8] as BoundField;
            bf.ReadOnly = true;
        }

        gvOrderCart.EditIndex = e.NewEditIndex;
        this.AssignDataSource(false);

        UpdateButtonStatus(false);

        if (OnCartRowEditModeChanged != null)
        {
            OnCartRowEditModeChanged(true);
            EditCartDetails(true);
        }
        System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(gvOrderCart.Rows[e.NewEditIndex].Cells[0].Controls[0]));
        System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(gvOrderCart.Rows[e.NewEditIndex].Cells[0].Controls[2]));

        imgedit.ToolTip = "Save";
        imgDelete.ToolTip = "Cancel";
        gvOrderCart.Columns[0].Visible = true;
        gvOrderCart.Columns[1].Visible = true;
        this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
    }

    protected void gvOrderCart_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvOrderCart.Columns[0].Visible = true;
        gvOrderCart.Columns[1].Visible = true;

        gvOrderCart.EditIndex = -1;
        this.AssignDataSource(false);

        UpdateButtonStatus(true);

        if (OnCartRowEditModeChanged != null)
            OnCartRowEditModeChanged(false);

        EditCartDetails(false);
    }

    private void EditCartDetails(bool isInEditMode)
    {
        if (isInEditMode)
        {
            Session["DisableUserPrefs"] = isInEditMode;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                    hpl.Visible = false;

                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                }
            }
        }
        else if ((bool)Session["UIisOnCheckout"] == false)
        {
            Session["DisableUserPrefs"] = isInEditMode;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                    hpl.Visible = true;

                    if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                    {
                        Page.Master.FindControl("liNetRetail").Visible = true;
                        ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

                        if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                        {
                            ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
                        }
                        else
                        {
                            ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
                        }
                    }
                }
            }
            else
            {
                HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                hpl.Visible = false;

                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
            }
        }

        if (Session["UIisOnInventoryDetail"] != null && (bool)Session["UIisOnInventoryDetail"] == true)
        {
            Session["DisableUserPrefs"] = true;

            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                if ((bool)Session["UserAction_AllowRetailPricing"] == true)
                {
                    HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                    hpl.Visible = false;

                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
                }
            }
            else
            {
                HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                hpl.Visible = false;

                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
            }
        }
    }

    protected void gvOrderCart_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DataControlFieldCell cell = gvOrderCart.Rows[e.RowIndex].Cells[2] as DataControlFieldCell;

        gvOrderCart.Columns[2].ExtractValuesFromCell(
            e.Keys,
            cell,
            DataControlRowState.Normal,
            true);

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.OrderCart orderCart = new OrderCart(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        orderCart.DeleteCartEntry(gvOrderCart.DataKeys[e.RowIndex]["ITEM"].ToString(), (decimal)gvOrderCart.DataKeys[e.RowIndex]["thickness"], (decimal)gvOrderCart.DataKeys[e.RowIndex]["WIDTH"], (decimal)gvOrderCart.DataKeys[e.RowIndex]["LENGTH"], (int)gvOrderCart.DataKeys[e.RowIndex]["sequence"], out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }        
        
        gvOrderCart.EditIndex = -1;
        this.AssignDataSource(false);

        /* Analytics Tracking */

        AnalyticsInput aInput = new AnalyticsInput();

        string analyticsLabel = "Mode = " + "Net";

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            analyticsLabel = "Mode = " + "Retail";
        }
         
        aInput.Type = "event";
        aInput.Action = "Cart - Item Edit - Delete Icon";
        aInput.Label = analyticsLabel;
        aInput.Value = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

        decimal cartTotal;

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            cartTotal = orderCart.RetailCartTotal;
        }
        else
            cartTotal = orderCart.CartTotal;

        this.lblLineTotal.Text = String.Format("{0:#,0.00}", cartTotal);

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dm.SetCache("OrderCart", orderCart);

        UpdateButtonStatus(true);

        if (orderCart.ReferencedDataSet.Tables["ttorder_cart"].Rows.Count == 0)
        {
            this.lblLineTotal.Text = String.Format("{0:#,0.00}", 0.00);
            orderCart.DeleteAllItems();
            btnEditCart_Click(sender, e);
       
            AssignDataSource(false);
        }

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            cartTotal = orderCart.RetailCartTotal;
        }
        else
            cartTotal = orderCart.CartTotal;

        if (this.OnPublishCart != null)
            this.OnPublishCart(orderCart.ReferencedDataSet.Tables["ttorder_cart"].Rows.Count, String.Format("{0:C}", cartTotal));
    }

    protected void gvOrderCart_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        gvOrderCart.Columns[0].Visible = true;
        gvOrderCart.Columns[1].Visible = true;

        DataControlFieldCell cell = gvOrderCart.Rows[e.RowIndex].Cells[3] as DataControlFieldCell;

        gvOrderCart.Columns[2].ExtractValuesFromCell(
            e.Keys,
            cell,
            DataControlRowState.Normal,
            true);

        cell = gvOrderCart.Rows[e.RowIndex].Cells[9] as DataControlFieldCell;

        gvOrderCart.Columns[9].ExtractValuesFromCell(
            e.NewValues,
            cell,
            DataControlRowState.Edit,
            true);

        cell = gvOrderCart.Rows[e.RowIndex].Cells[8] as DataControlFieldCell;

        gvOrderCart.Columns[8].ExtractValuesFromCell(
            e.NewValues,
            cell,
            DataControlRowState.Edit,
            true);

        string newQty = e.NewValues["qty"] == null ? "" : e.NewValues["qty"].ToString();
        string newMessage = e.NewValues["line_message"] == null ? "" : e.NewValues["line_message"].ToString();
        string newLocationReference = e.NewValues["location_reference"] == null ? "" : e.NewValues["location_reference"].ToString();

        bool parsed = false;
        decimal quantity = (decimal)0;
        parsed = Decimal.TryParse(newQty, out quantity);

        if (!parsed || quantity <= 0)
        {
            e.Cancel = true;
            lblNegativeQuantityError.Text = "Zero or negative quantity not allowed.";
            lblNegativeQuantityError.Visible = true;
            return;
        }

        if (quantity.ToString().Trim().Length > 14)
        {
            e.Cancel = true;
            lblNegativeQuantityError.Text = "Quantity entered is not valid.";
            lblNegativeQuantityError.Visible = true;
            return;
        }

        int ReturnCode;
        string MessageText;

        Dmsi.Agility.Data.OrderCart orderCart = new OrderCart(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }
        
        orderCart.UpdateCart(gvOrderCart.DataKeys[e.RowIndex]["ITEM"].ToString(), (decimal)gvOrderCart.DataKeys[e.RowIndex]["thickness"], (decimal)gvOrderCart.DataKeys[e.RowIndex]["WIDTH"], (decimal)gvOrderCart.DataKeys[e.RowIndex]["LENGTH"], (int)gvOrderCart.DataKeys[e.RowIndex]["sequence"], newQty, newMessage, newLocationReference, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }
        
        gvOrderCart.EditIndex = -1;

        // Force to refresh the cart from the back, to have the pricing correctly reflect the qty if applicable
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();

        dm.RemoveCache("dsOrderCartDataSet");
        dm.RemoveCache("OrderCart");

        this.AssignDataSource(false);

        orderCart = new OrderCart(out ReturnCode, out MessageText); // Now get the order returned from the back-end

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        /* Analytics Tracking */

        AnalyticsInput aInput = new AnalyticsInput();

        string analyticsLabel = "Mode = " +  "Net";

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            analyticsLabel = "Mode = " + "Retail";
        }

        analyticsLabel = analyticsLabel + ", " + "Message = " + newMessage + ", " + " Location Ref = " + newLocationReference;

        aInput.Type   = "event";
        aInput.Action = "Cart - Item Edit - Save Icon";
        aInput.Label  = analyticsLabel;
        aInput.Value  = "0";

        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();

        dm.SetCache("OrderCart", orderCart);

        decimal cartTotal;

        if (Session["UserPref_DisplaySetting"] != null &&
           ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
            (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
        {
            cartTotal = orderCart.RetailCartTotal;
        }
        else
            cartTotal = orderCart.CartTotal;

        this.lblLineTotal.Text = String.Format("{0:#,0.00}", cartTotal);

        if (this.OnPublishCart != null)
            this.OnPublishCart(orderCart.ReferencedDataSet.Tables["ttorder_cart"].Rows.Count, String.Format("{0:C}", cartTotal));

        UpdateButtonStatus(true);

        if (OnCartRowEditModeChanged != null)
        {
            OnCartRowEditModeChanged(false);
            EditCartDetails(false);
        }
    }

    protected void btnSubmitOrder_Click(object sender, EventArgs e)
    {
        bool ok = true;

        try
        {
            if (ddlOrderType.SelectedValue == "")
            {
                ok = false;

                if (ddlOrderType.Items.Count == 1)
                    DisplayOrderMessage("Unable to process the order.");
                else
                    lblOrderTypeError.Text = "*&nbsp;Required";
            }
            else
            {
                lblOrderTypeError.Text = "*";
            }

            if (Page.Request.Url.ToString().Contains("bc.com"))
            {
                if (txtCustomerPO.Text.Trim().Length == 0)
                {
                    ok = false;
                    lblPOError.Visible = true;
                    lblPOError.Text = "*&nbsp;Required";
                }
                else
                {
                    lblPOError.Visible = false;
                    lblPOError.Text = "*";
                }
            }

            if (ddlSubmitAs.Text.Length == 0)
            {
                ok = false;
                lblSubmitAsError.Text = "*&nbsp;Required";
            }
            else
            {
                lblSubmitAsError.Text = "*";
            }

            DataManager dm = new DataManager();
            DataRow row = (DataRow)dm.GetCache("ShiptoRowForInformationPanel");

            if (txtOrderedBy.Text.Length == 0)
            {
                ok = false;
                lblOrderedByError.Text = "*&nbsp;Required";
            }

            if (txtOrderedBy.Text.Length > 0)
            {
                lblOrderedByError.Text = "*";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["cust_po_required"] && txtCustomerPO.Text.Length == 0)
            {
                ok = false;
                lblPOError.Text = "*&nbsp;Required";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["cust_po_required"] && txtCustomerPO.Text.Length > 0)
            {
                lblPOError.Text = "*";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["job_number_required"] && txtJob.Text.Length == 0)
            {
                ok = false;
                lblJobError.Text = "*&nbsp;Required";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["job_number_required"] && txtJob.Text.Length > 0)
            {
                lblJobError.Text = "*";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["reference_required"] && txtReference.Text.Length == 0)
            {
                ok = false;
                lblReferenceError.Text = "*&nbsp;Required";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["reference_required"] && txtReference.Text.Length > 0)
            {
                lblReferenceError.Text = "*";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["ship_via_required"] && ddlShipVia.Text.Length == 0)
            {
                ok = false;
                lblShipViaError.Text = "*&nbsp;Required";
            }

            if (ddlSubmitAs.Text == "SO" && (bool)row["ship_via_required"] && ddlShipVia.Text.Length > 0)
            {
                lblShipViaError.Text = "*";
            }

            lblCcEmailError.Visible = false;
            if (txtCcEmail.Text.Length > 0)
            {
                RegexStringValidator val = new RegexStringValidator(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                try
                {
                    val.Validate(txtCcEmail.Text);
                }
                catch
                {
                    ok = false;
                    lblCcEmailError.Visible = true;
                    lblCcEmailError.Text = "Invalid";
                }
            }

            if (txtEmail.Text.Length == 0)
            {
                ok = false;
                lblEmailError.Text = "*&nbsp;Required";
            }
            else
            {
                lblEmailError.Text = "*";
                RegexStringValidator val = new RegexStringValidator(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

                try
                {
                    val.Validate(txtEmail.Text);
                }
                catch
                {
                    ok = false;
                    lblEmailError.Text = "*&nbsp;Invalid";
                }
            }

            if (ddlRequestedDate.Visible)
            {
                if (ddlRequestedDate.SelectedValue != null && ddlRequestedDate.SelectedValue.Length > 0 && ddlRequestedDate.SelectedValue != "Next available")
                {
                    try
                    {
                        DateTime requestedDate = DateTime.Parse(ddlRequestedDate.SelectedValue);

                        if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                        {
                            ok = false;
                            lblRequestedDateError.Visible = true;
                        }
                        else
                            lblRequestedDateError.Visible = false;
                    }
                    catch
                    {
                        ok = false;
                        lblRequestedDateError.Visible = true;
                    }
                }
            }
            else
            {
                if (txtRequestedDate.Text.Trim() != "")
                {
                    try
                    {
                        DateTime requestedDate = DateTime.Parse(txtRequestedDate.Text);

                        if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                        {
                            ok = false;
                            lblRequestedDateError.Visible = true;
                        }
                        else
                            lblRequestedDateError.Visible = false;
                    }
                    catch
                    {
                        ok = false;
                        lblRequestedDateError.Visible = true;
                    }
                }
            }

            if (txtFax.Text == "(___) ___-____")
            {
                lblFaxError.Visible = false;
            }
            else if (txtFax.Text.Contains("_"))
            {
                ok = false;
                lblFaxError.Visible = true;
            }

            if (ok)
            {
                int ReturnCode;
                string MessageText;

                OrderCart cart = new OrderCart(out ReturnCode, out MessageText);

                if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
                {
                    Session["LoggedIn"] = null;
                    Response.Redirect("~/Logon.aspx");
                }

                ok = DisplayMinPackViolation(cart);

                if (ok)
                {
                    //We want the call to timeout in 2 seconds...
                    this.Page.AsyncTimeout = new TimeSpan(0, 0, 2);

                    // create the asynchronous task instance
                    PageAsyncTask asyncTask = new PageAsyncTask(
                      new BeginEventHandler(this.beginAsyncRequest),
                      new EndEventHandler(this.endAsyncRequest),
                      new EndEventHandler(this.asyncRequestTimeout),
                      cart, true);

                    // register the asynchronous task instance with the page
                    this.Page.RegisterAsyncTask(asyncTask);

                    this.Page.ExecuteRegisteredAsyncTasks();
                }

                lblFaxError.Visible = false;

                SubmitTop.Visible = false;
                btnCartCheckout2.Visible = false;
                lblItemsInCart.Visible = false;
                CartButtons2.Visible = false;
               
                EmptyCartDiv.Visible = false;
                DeleteButtonDiv.Visible = false;
                CartGridDiv.Visible = false;
                try
                {
                    this.ForceEmptyCartClientOnly();
                }
                catch (Exception ex)
                {
                    DisplayOrderMessage(ex.Message.ToString());
                }
                finally
                {
                    
                }
                
                
                int showpopup = 1;
                if (ddlSubmitAs.SelectedValue == "SO")
                {
                    DisplayOrderMessage("Your order has been submitted.");
                    showpopup = 0;
                }
                if (ddlSubmitAs.SelectedValue == "QU")
                {
                    DisplayOrderMessage("Your quote has been submitted.");
                    showpopup = 0;
                }
                if (showpopup == 1)
                {
                    SubmitButtons2.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Popup", "ShowPopup('ctl00_cphMaster1_MainTabControl1_OrderTab1_pnlOrderConfirmation','ctl00_cphMaster1_MainTabControl1_OrderTab1_pnlinnerorder');", true);

                }
                else if (sender != null && e != null)
                {
                     /* Analytics Tracking */

                     Button bt = sender as Button;

                     AnalyticsInput aInput = new AnalyticsInput();

                     aInput.Type = "event";
                     aInput.Value = "0";
                     string analyticsLabel = "Mode = Net";

                    if (Session["UserPref_DisplaySetting"] != null &&
                       ((string)Session["UserPref_DisplaySetting"] == "Retail price" ||
                       (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" ||
                       (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list"))
                    {
                        analyticsLabel = "Mode = Retail";
                    }

                        if (ddlSubmitAs.SelectedValue == "QU")
                     {
                        aInput.Label = analyticsLabel + ", " + "Quote Submitted";
                     }
                     else
                     {
                        aInput.Label = analyticsLabel + ", " + "Order Submitted"; ;
                     }

                     if (bt.ID == "btnSubmitOrder2")
                     {
                        aInput.Action = "Cart - Submit- Top";
                     }
                     else
                     {
                        aInput.Action = "Cart - Submit- Bottom";
                     }

                     AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
                     am.BeginTrackEvent();
                   
                }

                txtJob.Text = "";
                txtReference.Text = "";
                txtCustomerPO.Text = "";
                txtMessage.Text = "";
                txtCcEmail.Text = "";
                txtFax.Text = "";
                this.lblLineTotal.Text = String.Format("{0:#,0.00}", 0.00);

                if (this.OnPublishCart != null)
                    this.OnPublishCart(0, String.Format("{0:C}", 0));

                if (OnCartRowEditModeChanged != null)
                {
                    Session["UIisOnCheckout"] = false;
                    OnCartRowEditModeChanged(false);
                }

            }
            if (ok == true)
            {

              
                
            }
            else
            {
               

            }
        }
        catch
        {
           

        }
    }

    private void endAsyncRequest(IAsyncResult result)
    {
        //Do Nothing...
    }

    void asyncRequestTimeout(IAsyncResult result)
    {
        //Do Nothing...
    }

    private IAsyncResult beginAsyncRequest(object sender, EventArgs e, AsyncCallback callback, object state)
    {
        OrderCart cart = state as OrderCart;

        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        AgilityEntryNetUser user = (AgilityEntryNetUser)dm.GetCache("AgilityEntryNetUser");
        dsOrderCartDataSet dsCart = (dsOrderCartDataSet)cart.ReferencedDataSet;
        dsSalesOrderDataSet dsSO = new dsSalesOrderDataSet();
        dsSO.ttso_header.expect_dateColumn.AllowDBNull = true;
        dsSalesOrderDataSet.ttso_headerRow soHeader = dsSO.ttso_header.Newttso_headerRow();

        int ReturnCode;
        string MessageText;
        
        AgilityCustomer cust = new AgilityCustomer("CustomerRowForInformationPanel", out ReturnCode, out MessageText);
        Shipto shipto = new Shipto(out ReturnCode, out MessageText);

        for (int i = 0; i < soHeader.ItemArray.Length; i++)
        {
            try
            {
                soHeader[i] = "";
            }
            catch
            {
                try
                {
                    soHeader[i] = DateTime.Now;
                }
                catch
                {
                    try
                    {
                        soHeader[i] = 0;
                    }
                    catch
                    {
                        soHeader[i] = false;
                    }
                }
            }
        }

        soHeader.TYPE = ddlSubmitAs.SelectedValue;
        soHeader.cust_code = cust.CustomerCode;
        soHeader.cust_key = cust.CustomerKey;
        soHeader.cust_key_sysid = cust.CustomerSystemID;
        soHeader.shipto_seq_num = shipto.ShiptoSequence;
        soHeader.ship_to_seq_num_sysid = shipto.ShiptoSystemID;
        soHeader.user_email = txtEmail.Text;
        soHeader.user_email2 = txtCcEmail.Text;
        soHeader.user_fax = txtFax.Text.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
        soHeader.ordered_by = txtOrderedBy.Text;
        soHeader.job = txtJob.Text;
        soHeader.reference = txtReference.Text;
        soHeader.cust_po = txtCustomerPO.Text;
        soHeader.order_message = txtMessage.Text;
        soHeader.sale_type = ddlOrderType.SelectedValue.ToString();
        soHeader.ship_via = ddlShipVia.SelectedValue.ToString();
        soHeader.ship_complete = cbxShipComplete.Checked;

        //MDM - Added as Ship-to overrides from popup control
        if (Session["Shipto_txtCustomerName"] != null)
        {
            soHeader.shiptoname = (string)Session["Shipto_txtCustomerName"];
            soHeader.shipToAddr1 = (string)Session["Shipto_txtAddress1"];
            soHeader.shipToAddr2 = (string)Session["Shipto_txtAddress2"];
            soHeader.shipToAddr3 = (string)Session["Shipto_txtAddress3"];
            soHeader.shipToCity = (string)Session["Shipto_txtCity"];
            soHeader.shipToState = (string)Session["Shipto_txtState"];
            soHeader.shipToZip = (string)Session["Shipto_txtZip"];
            soHeader.shipToCountry = (string)Session["Shipto_txtCountry"];
            soHeader.shipToPhone = (string)Session["Shipto_txtPhone"];
        }

        if (txtRequestedDate.Visible)
        {
            if (txtRequestedDate.Text.Trim() != "")
            {
                soHeader.expect_date = DateTime.Parse(txtRequestedDate.Text);
            }
            else
                soHeader["expect_date"] = DBNull.Value;
        }
        else
        {
            if (ddlRequestedDate.SelectedValue != null && ddlRequestedDate.SelectedValue.Length > 0 && ddlRequestedDate.SelectedValue != "Next available")
                soHeader.expect_date = DateTime.Parse(ddlRequestedDate.SelectedValue);
            else
                soHeader["expect_date"] = DBNull.Value;
        }

        Session["orderDateStrings"] = null;
        Session["LastChosenRequestedDeliveryDate"] = null;

        SalesOrder salesOrder = new SalesOrder((string)dm.GetCache("currentBranchID"), soHeader, dsCart);
        salesOrder.BeginSaveSalesOrder();

        //string NetOrRetail = "Net";

        //if (dm.GetCache("UserPref_DisplaySetting") != null &&
        //   ((string)dm.GetCache("UserPref_DisplaySetting") == "Retail price" ||
        //    (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - markup on cost" ||
        //    (string)dm.GetCache("UserPref_DisplaySetting") == "Retail price - discount from list"))
        //{
        //    NetOrRetail = "Retail";
        //}

        //string pageDomain = this.Page.Request.Url.ToString().Substring(this.Page.Request.Url.ToString().IndexOf("//") + 2);
        //pageDomain = pageDomain.Substring(0, pageDomain.IndexOf("/"));

        //AnalyticsInput aInput = new AnalyticsInput();
        //aInput.Type = "event";
        //aInput.Category = pageDomain;
        //aInput.Action = "Cart_Create_" + soHeader.TYPE;
        //aInput.Label = NetOrRetail;
        //aInput.Value = "0";
        //aInput.Host = this.Page.Request.Url.ToString();
        //aInput.IPAddress = this.Page.Request.UserHostAddress;

        //AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        //am.BeginTrackEvent();

        Thread.Sleep(2000);

        shipto.Dispose();
        cust.Dispose();

        // Clear Additional Information fields from the popup control...
        Session["AddInfo_txtCustomerId"] = null;
        Session["AddInfo_txtShipTo"] = null;
        Session["AddInfo_txtCustomerName"] = null;
        Session["AddInfo_txtAddress1"] = null;
        Session["AddInfo_txtAddress2"] = null;
        Session["AddInfo_txtAddress3"] = null;
        Session["AddInfo_txtCity"] = null;
        Session["AddInfo_txtState"] = null;
        Session["AddInfo_txtZip"] = null;
        Session["AddInfo_txtCountry"] = null;
        Session["AddInfo_txtPhone"] = null;
        Session["AddInfo_txtEmail"] = null;

        Session["Shipto_txtCustomerName"] = null;
        Session["Shipto_txtAddress1"] = null;
        Session["Shipto_txtAddress2"] = null;
        Session["Shipto_txtAddress3"] = null;
        Session["Shipto_txtCity"] = null;
        Session["Shipto_txtState"] = null;
        Session["Shipto_txtZip"] = null;
        Session["Shipto_txtCountry"] = null;
        Session["Shipto_txtPhone"] = null;

        return new MyEmtpyIAsyncResult();
    }

    private bool DisplayMinPackViolation(OrderCart cart)
    {
        bool ok = true;
        cart.CheckQuantity();

        if (cart.Violations != null && cart.Violations.Count > 0)
        {
            this.lblQuantityError.Visible = true;
            this.lblQuantityErrorTable.Visible = true;
            this.lblQuantityError.Text = "One or more items do not meet the required multiple.";
            this.lblQuantityErrorTable.Text = "<table class=\"carttable\" width=\"100%\" cellspacing=\"0\" cellpadding=\"2\">";

            this.lblQuantityErrorTable.Text += "<tr>";
            this.lblQuantityErrorTable.Text += "<th align=\"left\" >Item</th>";
            this.lblQuantityErrorTable.Text += "<th align=\"right\" >Required Multiple</th>";
            this.lblQuantityErrorTable.Text += "<th align=\"right\" >Current Qty</th>";
            this.lblQuantityErrorTable.Text += "<th align=\"right\" >Suggested Qty</th>";
            this.lblQuantityErrorTable.Text += "</tr>";

            foreach (Dmsi.Agility.Data.Violation item in cart.Violations)
            {
                this.lblQuantityErrorTable.Text += "<tr>";
                this.lblQuantityErrorTable.Text += "<td align=\"left\">" + item.Item + "</td>";
                this.lblQuantityErrorTable.Text += "<td align=\"right\">" + String.Format("{0:#,0.0000}", item.Multiple) + "</td>";
                this.lblQuantityErrorTable.Text += "<td align=\"right\">" + String.Format("{0:#,0.0000}", item.CurrentValue) + "</td>";
                this.lblQuantityErrorTable.Text += "<td align=\"right\">" + String.Format("{0:#,0.0000}", item.Suggested) + "</td>";
                this.lblQuantityErrorTable.Text += "</tr>";
            }

            this.lblQuantityErrorTable.Text += "</table><br />";
            ok = false;
        }
        else
        {
            this.lblQuantityError.Visible = false;
            this.lblQuantityErrorTable.Visible = false;
        }

        return ok;
    }

    public void btnClose_Click(object sender, EventArgs e)
    {

        CartGridDiv.Visible = true;
        btnEditCart_Click(sender, e);

        Button senderButton = sender as Button;
        if (senderButton != null && senderButton.Text != "")
        {
            btnCartCheckout2.Visible = false;
            lblItemsInCart.Visible = false;
            CartButtons2.Visible = false;
        }

        if (Session["AllowRetailPricing"] != null)
        {
            if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
            {
                this.Page.Master.FindControl("OptionsModalDiv").Visible = true;
                HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                hypUserPreferences.Visible = true;

                if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                {
                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

                    if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
                    }
                    else
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
                    }
                }
            }
            else
            {
                this.Page.Master.FindControl("OptionsModalDiv").Visible = false;
                HyperLink hypUserPreferences = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                hypUserPreferences.Visible = false;

                ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = false;
            }
        }
       
    }

    protected void gvOrderCart_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvOrderCart.PageIndex = e.NewPageIndex;
        AssignDataSource(false);
    }

    protected void ddlOrderHeaderRows_SelectedIndexChanged(object sender, EventArgs e)
    {
        Profile.ddlOrderHeaderRows_SelectedIndex = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedIndex);

        int pageSize;
        if (((System.Web.UI.WebControls.DropDownList)sender).SelectedValue == "All")
        {
            pageSize = 32000;
        }
        else
        {
            pageSize = Convert.ToInt32(((System.Web.UI.WebControls.DropDownList)sender).SelectedValue);
        }

        gvOrderCart.PageSize = pageSize;
        AssignDataSource(false);

        /* Analytics Tracking */

        AnalyticsInput aInput = new AnalyticsInput();

        aInput.Type = "event";
        aInput.Action = "Cart - Show Rows Selection";
        aInput.Label = "Show Rows selection = " + ((System.Web.UI.WebControls.DropDownList)sender).SelectedValue;
        aInput.Value = "0";
        
        AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
        am.BeginTrackEvent();
    }

    protected void gvOrderCart_Sorting(object sender, GridViewSortEventArgs e)
    {
        if ((string)Session["gvOrderCartSortColumn"] != e.SortExpression)
            Session["gvOrderCartSortDirection"] = " ASC";
        else
        {
            if ((string)Session["gvOrderCartSortDirection"] == " ASC")
                Session["gvOrderCartSortDirection"] = " DESC";
            else
                Session["gvOrderCartSortDirection"] = " ASC";
        }

        gvOrderCart.PageIndex = 0;
        Session["gvOrderCartSortColumn"] = e.SortExpression;

        AssignDataSource(false);

        // Save current order
        int ReturnCode;
        string MessageText;

        OrderCart cart = new OrderCart(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        cart.SaveCart(0, out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }
    }

    protected void gvOrderCart_RowCreated(object sender, GridViewRowEventArgs e)
    {
        // Use the RowType property to determine whether the 
        // row being created is the header row. 
        if (e.Row.RowType == DataControlRowType.Header)
            this.AssignSortImageToRow(this.gvOrderCart, e.Row, "gvOrderCartSortColumn", "gvOrderCartSortDirection", 0);
    }

    /// <summary>
    /// Insert the sort image to the column header
    /// </summary>
    /// <param name="gv"></param>
    /// <param name="row"></param>
    /// <param name="sessionSortColKey"></param>
    /// <param name="sessionDirectionKey"></param>
    /// <param name="offSet">Refers to the number non-boundable columns before boundable columns</param>
    private void AssignSortImageToRow(GridView gv, GridViewRow row, string sessionSortColKey, string sessionDirectionKey, int offSet)
    {
        // Call the GetSortColumnIndex helper method to determine
        // the index of the column being sorted.
        int sortColumnIndex = GetSortColumnIndex(gv, sessionSortColKey, offSet);

        if (sortColumnIndex != -1)
        {
            // Call the AddSortImage helper method to add
            // a sort direction image to the appropriate
            // column header. 
            AddSortImageAndText(sortColumnIndex, row, sessionSortColKey, sessionDirectionKey);
        }
    }

    // This is a helper method used to determine the index of the
    // column being sorted. If no column is being sorted, -1 is returned.
    private int GetSortColumnIndex(GridView gv, string sessionSortColKey, int offSet)
    {
        // Iterate through the Columns collection to determine the index
        // of the column being sorted.
        foreach (DataControlField field in gv.Columns)
        {
            if (field.SortExpression == (string)Session[sessionSortColKey])
            {
                return gv.Columns.IndexOf(field) + offSet;
            }
        }

        return -1;
    }

    // This is a helper method used to add a sort direction
    // image to the header of the column being sorted.
    private void AddSortImageAndText(int columnIndex, GridViewRow headerRow, string sessionSortColKey, string sessionDirectionKey)
    {
        // Create the sorting image based on the sort direction.
        LinkButton imageLink = new LinkButton();
        Image sortImage = new Image();

        if ((string)Session[sessionDirectionKey] == " ASC")
        {
            sortImage.ImageUrl = "~/Images/sort_asc_arrow.gif";
            sortImage.AlternateText = "Ascending Order";
        }
        else
        {
            sortImage.ImageUrl = "~/Images/sort_desc_arrow.gif";
            sortImage.AlternateText = "Descending Order";
        }

        // Add hover text to the corresponding column header link
        string hoverText = ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Text + " - " + sortImage.AlternateText;
        ((LinkButton)headerRow.Cells[columnIndex].Controls[0]).Attributes.Add("title", hoverText);

        // Add the image to the appropriate header cell.
        imageLink.CommandName = "Sort";
        imageLink.CommandArgument = (string)Session[sessionSortColKey];
        imageLink.Controls.Add(sortImage);
        headerRow.Cells[columnIndex].Controls.Add(imageLink);

        imageLink.Dispose();
        sortImage.Dispose();
    }

    protected void gvOrderCart_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRow currRow = ((DataRowView)e.Row.DataItem).Row;
            System.Web.UI.WebControls.ImageButton imgedit = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[0]));
            System.Web.UI.WebControls.ImageButton imgDelete = ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[0].Controls[2]));

            imgedit.ToolTip = "Edit";
            imgDelete.ToolTip = "Delete";
            if (e.Row.RowState == DataControlRowState.Edit)
            {

                imgedit.ToolTip = "Save";
                imgDelete.ToolTip = "Cancel";
            }

            //Set the boolean to false for release code...
            bool configurable = false;
            if (currRow["configurable"] != null && currRow["configurable"].ToString().Length > 0)
                configurable = Convert.ToBoolean(currRow["configurable"]);

            Panel panelConfigure = (Panel)e.Row.FindControl("panel_Configure");
            if (panelConfigure != null)
            {
                if (configurable)
                {
                    panelConfigure.Width = new Unit(88, UnitType.Pixel);
                    panelConfigure.Visible = true;
                    gvOrderCart.Columns[1].Visible = true;
                    
                }
                else
                {
                    panelConfigure.Width = new Unit(0);
                    panelConfigure.Visible = false;
                    if (configureexist == 0)
                    {
                        gvOrderCart.Columns[1].Visible = false;
                    }
                    else
                    {
                        gvOrderCart.Columns[1].Visible = true;

                    }
                }
            }

            Panel panelDuplicate = (Panel)e.Row.FindControl("panel_Duplicate");
            if (panelDuplicate != null)
            {
                if (configurable)
                {
                    panelDuplicate.Width = new Unit(0, UnitType.Pixel);
                    panelDuplicate.Visible = true;
                    gvOrderCart.Columns[2].Visible = false;
                }
                else
                {
                    panelDuplicate.Width = new Unit(0);
                    panelDuplicate.Visible = false;
                    gvOrderCart.Columns[2].Visible = false;
                }
            }

            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (e.Row.Cells[i] != null && e.Row.Cells[i].Text.StartsWith(" ", StringComparison.OrdinalIgnoreCase))
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(" ", "&nbsp;");
                }
            }

            e.Row.Attributes.Add("onmouseover", "this.className='GridViewRowMouseHover'");
            e.Row.Attributes.Add("onmouseout", "this.className='GridViewRowMouseOut'");

            // NOTE: a good sample of converting a cell to the databound field
            if (e.Row.RowIndex == gvOrderCart.EditIndex)
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    BoundField fld = ((DataControlFieldCell)cell).ContainingField as BoundField;
                    if (fld != null && fld.DataField == "line_message")
                    {
                        foreach (Control ctl in cell.Controls)
                        {
                            TextBox txt = ctl as TextBox;
                            if (txt != null)
                                txt.MaxLength = 1000;
                        }
                    }

                    if (fld != null && fld.DataField == "location_reference")
                    {
                        foreach (Control ctl in cell.Controls)
                        {
                            TextBox txt = ctl as TextBox;
                            if (txt != null)
                                txt.MaxLength = 40;
                        }
                    }
                }
            }
        }
    }

    protected void btnDeleteAll_Click(object sender, EventArgs e)
    {
        this.lblLineTotal.Text = String.Format("{0:#,0.00}", 0.00);

        int ReturnCode;
        string MessageText;

        OrderCart cart = new OrderCart(out ReturnCode, out MessageText);

        if (ReturnCode == 2 && MessageText.ToLower().Contains("979"))
        {
            Session["LoggedIn"] = null;
            Response.Redirect("~/Logon.aspx");
        }

        cart.DeleteAllItems();

        if (sender != null && e != null)
        {

            /* Analytics Tracking */

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Action = "Cart - Delete All Button";
            aInput.Label = "Net";
            aInput.Value = "0";

            if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
            {
                aInput.Label = "Retail";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }
        
        btnEditCart_Click(sender, e);
        AssignDataSource(false);
        btnCartCheckout2.Visible = false;
        lblItemsInCart.Visible = false;
        CartButtons2.Visible = false;

        if (((string)Session["AllowRetailPricing"]).ToLower() == "yes")
        {
            if ((bool)Session["UserAction_AllowRetailPricing"] == true)
            {
                if ((bool)Session["UserAction_DisableOnScreenUserPreferences"] == false)
                {
                    HyperLink hpl = (HyperLink)this.Page.Master.FindControl("hypUserPreferences");
                    hpl.Visible = true;

                    Page.Master.FindControl("liNetRetail").Visible = true;
                    ((LinkButton)Page.Master.FindControl("hypNetRetail")).Enabled = true;

                    if ((string)Session["UserPref_DisplaySetting"] == "Retail price" || (string)Session["UserPref_DisplaySetting"] == "Retail price - markup on cost" || (string)Session["UserPref_DisplaySetting"] == "Retail price - discount from list")
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Retail";
                    }
                    else
                    {
                        ((LinkButton)this.Page.Master.FindControl("hypNetRetail")).Text = "Net";
                    }
                }
            }
        }
    }

    protected void ddlOrderType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dm.SetCache("SaleType", this.ddlOrderType.SelectedValue);
        dm.RemoveCache("dsOrderCartDataSet");
        dm.RemoveCache("OrderCart");
        Session["orderDateStrings"] = null;
        this.AssignDataSource(false);

        if (this.OnSaleTypeChanged != null)
        {
            if (this.ddlOrderType.SelectedValue.Length > 0)
                this.OnSaleTypeChanged(this.ddlOrderType.SelectedValue, this.ddlOrderType.SelectedItem.Text);
            else
                this.OnSaleTypeChanged("", "All Sale Types");
        }
    }

    protected void ddlSubmitAs_SelectedIndexChanged(object sender, EventArgs e)
    {
        Dmsi.Agility.EntryNET.DataManager dm = new DataManager();
        dsPartnerVuDataSet dsPV = (dsPartnerVuDataSet)dm.GetCache("dsPV");

        if (dsPV.bPV_CoInfo.Rows[0]["default_submit_cart_as"].ToString() == "" && ddlSubmitAs.Items.Count == 3)
        {
            ddlSubmitAs.Items.RemoveAt(0);
        }

        if (ddlSubmitAs.SelectedValue.Length == 0)
            lblSubmitAsError.Visible = true;

        if (ddlSubmitAs.Text == "QU")
        {
            //We have just changed to QU from SO, so need to remove visibility of *'s
            lblPOError.Visible = false;
            lblPOError.Text = "*";

            lblJobError.Visible = false;
            lblJobError.Text = "*";

            lblOrderedByError.Text = "*";

            lblReferenceError.Visible = false;
            lblReferenceError.Text = "*";

            lblShipViaError.Visible = false;
            lblShipViaError.Text = "*";
        }
    }

    protected void btnContinueShopping_Click(object sender, EventArgs e)
    {

        if (sender != null && e != null)
        {
            /* Analytics Tracking */

            Button bt = sender as Button;

            AnalyticsInput aInput = new AnalyticsInput();

            aInput.Type = "event";
            aInput.Label = "";
            aInput.Value = "0";

            if (bt.ID == "btnContinueShopping2")
            {
                aInput.Action = "Cart - Continue Shopping - Top";
            }
            else
            {
                aInput.Action = "Cart - Continue Shopping - Bottom";
            }

            AnalyticsManager am = new AnalyticsManager(this.Page, aInput);
            am.BeginTrackEvent();

        }

        Session["UIisOnCheckout"] = false;

        UserControls_Inventory defaultInventory = Parent.FindControl("Inventory1") as UserControls_Inventory;
        UserControls_Home home = Parent.FindControl("Home1") as UserControls_Home;
        defaultInventory.Visible = true;

        this.Visible = false;
    }

    protected void txtRequestedDate_TextChanged(object sender, EventArgs e)
    {
        if (txtRequestedDate.Text.Length == 0)
        {
            lblRequestdDateAsterisk.Visible = false;
            lblRequestedDateMessage.Visible = false;

            if (lblRequestedDateError.Visible)
                lblRequestedDateError.Visible = false;
        }
        else
        {
            lblRequestdDateAsterisk.Visible = true;
            lblRequestedDateMessage.Visible = true;
        }

        try
        {
            if (txtRequestedDate.Text.Length > 0)
            {
                DateTime requestedDate = DateTime.Parse(txtRequestedDate.Text);

                if (requestedDate < DateTime.Now && requestedDate.ToShortDateString() != DateTime.Now.ToShortDateString())
                {
                    lblRequestedDateError.Visible = true;
                }
                else
                    lblRequestedDateError.Visible = false;
            }
        }
        catch
        {
            lblRequestedDateError.Visible = true;
        }
    }

    protected void ddlRequestedDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LastChosenRequestedDeliveryDate"] = ddlRequestedDate.SelectedItem.Text;

        if (ddlRequestedDate.SelectedItem != null && ddlRequestedDate.SelectedItem.Text == "Next available")
        {
            lblRequestdDateAsterisk.Visible = false;
            lblRequestedDateMessage.Visible = false;

            if (lblRequestedDateError.Visible)
                lblRequestedDateError.Visible = false;
        }
        else
        {
            lblRequestdDateAsterisk.Visible = true;
            lblRequestedDateMessage.Visible = true;
        }
    }
}

public class MyEmtpyIAsyncResult : IAsyncResult
{
    public MyEmtpyIAsyncResult() //Default ctor...
    {
    }

    public object AsyncState
    {
        get { return new object(); }
    }

    public System.Threading.WaitHandle AsyncWaitHandle
    {
        get { return null; }
    }

    public bool CompletedSynchronously
    {
        get { return true; }
    }

    public bool IsCompleted
    {
        get { return true; }
    }
}
