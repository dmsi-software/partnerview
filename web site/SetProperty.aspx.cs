﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;

public partial class SetProperty : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Page.ClientTarget = "uplevel";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["connection"] != null)
            {
                XmlDocument webConfig = new XmlDocument();
                webConfig.Load(Server.MapPath("~/") + "/Web.config");

                XmlNodeList nodes = webConfig.SelectNodes("/configuration/appSettings/add");
                foreach (XmlNode childNode in nodes)
                {
                    if (childNode.Attributes["key"].Value.ToLower() == "agilitylibraryconnection")
                    {
                        childNode.Attributes["value"].Value = Request.QueryString["connection"].ToString();
                        break;
                    }
                }

                webConfig.Save(Server.MapPath("~/") + "/Web.config");
            }

            if (Request.QueryString["theme"] != null)
            {
                XmlDocument webConfig = new XmlDocument();
                webConfig.Load(Server.MapPath("~/") + "/Web.config");

                XmlNodeList nodes = webConfig.SelectNodes("/configuration/appSettings/add");
                foreach (XmlNode childNode in nodes)
                {
                    if (childNode.Attributes["key"].Value.ToLower() == "theme")
                    {
                        childNode.Attributes["value"].Value = Request.QueryString["theme"].ToString();
                        break;
                    }
                }

                webConfig.Save(Server.MapPath("~/") + "/Web.config");

                File.Copy(Server.MapPath("~/") + "/App_Themes/" + Request.QueryString["theme"].ToString() + "MasterPage.css", Server.MapPath("~/") + "/App_Themes/MasterPage.css", true);
            }
        }
    }
}